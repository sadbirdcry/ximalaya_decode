// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.umeng.analytics.MobclickAgent;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.a.c;
import com.ximalaya.ting.android.a.d;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.setting.WakeUpSettingActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.broadcast.AlarmReceiver2;
import com.ximalaya.ting.android.fragment.play.PlayerFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.setting.DaysOfWeek;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.thirdBind.ThirdPartyUserInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;

// Referenced classes of package com.ximalaya.ting.android.util:
//            TimeHelper, Logger, Utilities, bc, 
//            MD5, bd, az, ba

public class ToolUtil
{

    public static final int ONE_HOUR = 0x36ee80;
    public static final int ONE_MINUTE = 60000;
    public static final int ONE_SECOND = 1000;
    private static final int PHONES_DISPLAY_NAME_INDEX = 0;
    private static final int PHONES_NUMBER_INDEX = 1;
    private static final String PHONES_PROJECTION[] = {
        "display_name", "data1"
    };
    private static final String SECURETY_KEY = "e1996c7d6e0ff0664b28af93a2eeff8f8cae84b2402d158f7bb115b735a1663d";
    public static final String TAG = com/ximalaya/ting/android/util/ToolUtil.getSimpleName();
    private static int contentViewHeight = 0;
    private static long lastClickTime;
    private static int screenHeight = 0;
    private static int screenWidth = 0;
    private static int statusBarHeight = 0;

    public ToolUtil()
    {
    }

    public static String addFilePrefix(String s)
    {
        String s1 = s;
        if (s != null)
        {
            s1 = s;
            if (s.length() > 0)
            {
                s1 = s;
                if (!s.startsWith("file://"))
                {
                    s1 = (new StringBuilder()).append("file://").append(s).toString();
                }
            }
        }
        return s1;
    }

    public static long calculateAlarm(int i, int j, DaysOfWeek daysofweek)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int k = calendar.get(11);
        int l = calendar.get(12);
        if (i < k || i == k && j <= l)
        {
            calendar.add(6, 1);
        }
        calendar.set(11, i);
        calendar.set(12, j);
        calendar.set(13, 0);
        calendar.set(14, 0);
        i = daysofweek.getNextAlarm(calendar);
        if (i > 0)
        {
            calendar.add(7, i);
        }
        return calendar.getTimeInMillis();
    }

    public static void cancelNotification(Context context, int i)
    {
        ((NotificationManager)context.getSystemService("notification")).cancel(i);
    }

    public static void changeLight(ImageView imageview, int i)
    {
        ColorMatrix colormatrix = new ColorMatrix();
        colormatrix.set(new float[] {
            1.0F, 0.0F, 0.0F, 0.0F, (float)i, 0.0F, 1.0F, 0.0F, 0.0F, (float)i, 
            0.0F, 0.0F, 1.0F, 0.0F, (float)i, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F
        });
        imageview.setColorFilter(new ColorMatrixColorFilter(colormatrix));
    }

    public static boolean checkApkExist(Context context, String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return false;
        }
        try
        {
            context.getPackageManager().getApplicationInfo(s, 8192);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        return true;
    }

    public static boolean checkSdcard()
    {
        return "mounted".equals(Environment.getExternalStorageState()) && Environment.getExternalStorageDirectory().canWrite();
    }

    public static String convertTime(long l)
    {
        return convertTimeForFeed(l);
    }

    public static String convertTimeForFeed(long l)
    {
        if (l == 0L)
        {
            return "\u65E0";
        }
        long l1 = (System.currentTimeMillis() - l) / 1000L;
        if (l1 < 0L)
        {
            return TimeHelper.countTime2(l);
        }
        l = l1 / 0x1da9c00L;
        long l2 = l1 / 0x278d00L;
        long l3 = l1 / 0x15180L;
        long l4 = (l1 % 0x15180L) / 3600L;
        long l5 = (l1 % 3600L) / 60L;
        l1 = (l1 % 60L) / 60L;
        StringBuffer stringbuffer = new StringBuffer();
        if (l != 0L)
        {
            stringbuffer.append((new StringBuilder()).append(l).append("\u5E74").toString());
            return (new StringBuilder()).append(stringbuffer.toString()).append("\u524D").toString();
        }
        if (l2 != 0L)
        {
            stringbuffer.append((new StringBuilder()).append(l2).append("\u4E2A\u6708").toString());
            return (new StringBuilder()).append(stringbuffer.toString()).append("\u524D").toString();
        }
        if (l3 != 0L)
        {
            stringbuffer.append((new StringBuilder()).append(l3).append("\u5929").toString());
            return (new StringBuilder()).append(stringbuffer.toString()).append("\u524D").toString();
        }
        if (l4 != 0L)
        {
            stringbuffer.append((new StringBuilder()).append(l4).append("\u5C0F\u65F6").toString());
            return (new StringBuilder()).append(stringbuffer.toString()).append("\u524D").toString();
        }
        if (l5 != 0L)
        {
            stringbuffer.append((new StringBuilder()).append(l5).append("\u5206\u949F").toString());
            return (new StringBuilder()).append(stringbuffer.toString()).append("\u524D").toString();
        }
        if (l1 != 0L)
        {
            stringbuffer.append((new StringBuilder()).append(l1).append("\u79D2").toString());
            return (new StringBuilder()).append(stringbuffer.toString()).append("\u524D").toString();
        } else
        {
            return "\u521A\u521A";
        }
    }

    public static void createAppDirectory()
    {
        Throwable throwable;
        createDirectory(a.ac);
        File file = new File(a.aj);
        if (!file.exists() && !file.mkdirs())
        {
            throw new Exception((new StringBuilder()).append("\u76EE\u5F55\u4E0D\u5B58\u5728\uFF0C\u521B\u5EFA\u5931\u8D25\uFF01").append(a.aj).toString());
        }
        file = new File(a.ah);
        if (!file.exists() && !file.mkdirs())
        {
            throw new Exception((new StringBuilder()).append("\u76EE\u5F55\u4E0D\u5B58\u5728\uFF0C\u521B\u5EFA\u5931\u8D25\uFF01").append(a.ah).toString());
        }
        try
        {
            File file1 = new File(a.ai);
            if (!file1.exists() && !file1.mkdirs())
            {
                throw new Exception((new StringBuilder()).append("\u76EE\u5F55\u4E0D\u5B58\u5728\uFF0C\u521B\u5EFA\u5931\u8D25\uFF01").append(a.ai).toString());
            }
            file1 = new File(a.al);
            if (!file1.exists() && !file1.mkdirs())
            {
                throw new Exception((new StringBuilder()).append("\u76EE\u5F55\u4E0D\u5B58\u5728\uFF0C\u521B\u5EFA\u5931\u8D25\uFF01").append(a.al).toString());
            }
            file1 = new File(a.am);
            if (!file1.exists() && !file1.mkdirs())
            {
                throw new Exception((new StringBuilder()).append("\u76EE\u5F55\u4E0D\u5B58\u5728\uFF0C\u521B\u5EFA\u5931\u8D25\uFF01").append(a.am).toString());
            }
            file1 = new File(a.an);
            if (!file1.exists() && !file1.mkdirs())
            {
                throw new Exception((new StringBuilder()).append("\u76EE\u5F55\u4E0D\u5B58\u5728\uFF0C\u521B\u5EFA\u5931\u8D25\uFF01").append(a.an).toString());
            }
            file1 = new File(a.ao);
            if (!file1.exists() && !file1.mkdirs())
            {
                throw new Exception((new StringBuilder()).append("\u76EE\u5F55\u4E0D\u5B58\u5728\uFF0C\u521B\u5EFA\u5931\u8D25\uFF01").append(a.ao).toString());
            }
            file1 = new File(a.ak);
            if (!file1.exists() && !file1.mkdirs())
            {
                throw new Exception((new StringBuilder()).append("\u76EE\u5F55\u4E0D\u5B58\u5728\uFF0C\u521B\u5EFA\u5931\u8D25\uFF01").append(a.ak).toString());
            }
        }
        catch (Exception exception) { }
        // Misplaced declaration of an exception variable
        catch (Throwable throwable)
        {
            Logger.log("createAppDirectory", throwable.getMessage(), true);
            return;
        }
        return;
    }

    public static boolean createDirectory(String s)
    {
        if (!Utilities.isBlank(s)) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        boolean flag;
        try
        {
            s = new File(s);
            if (s.exists())
            {
                break; /* Loop/switch isn't completed */
            }
            flag = s.mkdirs();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return false;
        }
        if (!flag) goto _L1; else goto _L3
_L3:
        return true;
    }

    public static Notification createNotification(Context context, String s, String s1, String s2, PendingIntent pendingintent)
    {
        int i = android.os.Build.VERSION.SDK_INT;
        String s3 = s;
        if (TextUtils.isEmpty(s))
        {
            s3 = context.getResources().getString(0x7f090000);
        }
        if (i < 16)
        {
            s = new Notification();
            s.icon = 0x7f020598;
            s.defaults = ((Notification) (s)).defaults | 4;
            s.defaults = ((Notification) (s)).defaults | 1;
            s.flags = ((Notification) (s)).flags | 0x10;
            s.when = System.currentTimeMillis();
            s.tickerText = s1;
            s.setLatestEventInfo(context, s3, s2, pendingintent);
            return s;
        } else
        {
            context = (new android.support.v4.app.NotificationCompat.Builder(context)).setWhen(System.currentTimeMillis()).setSmallIcon(0x7f020598).setTicker(s1).setContentTitle(s3).setContentText(s2).setContentIntent(pendingintent).setPriority(1).setAutoCancel(true).setDefaults(-1).setStyle((new android.support.v4.app.NotificationCompat.BigTextStyle()).bigText(s2)).build();
            context.when = System.currentTimeMillis();
            return context;
        }
    }

    public static ProgressDialog createProgressDialog(Context context, int i, boolean flag, boolean flag1)
    {
        context = new MyProgressDialog(context);
        context.setIndeterminate(flag);
        context.setCancelable(flag1);
        return context;
    }

    private static void doUploadContacts(Context context, List list)
    {
        (new bc(list)).execute(new Object[] {
            context
        });
    }

    public static int dp2px(Context context, float f1)
    {
        return dp2px(context.getResources(), f1);
    }

    public static int dp2px(Resources resources, float f1)
    {
        return (int)(resources.getDisplayMetrics().density * f1 + 0.5F);
    }

    public static String formatFileSize(long l)
    {
        return (new StringBuilder()).append(String.format("%.2f", new Object[] {
            Float.valueOf((float)l / 1024F / 1024F)
        })).append("MB").toString();
    }

    public static String formatFriendlyFileSize(double d1)
    {
        if (d1 < 1024D)
        {
            return (new StringBuilder()).append(String.format("%.2f", new Object[] {
                Double.valueOf(d1)
            })).append("B").toString();
        }
        d1 /= 1024D;
        if (d1 < 1024D)
        {
            return (new StringBuilder()).append(String.format("%.2f", new Object[] {
                Double.valueOf(d1)
            })).append("KB").toString();
        }
        d1 /= 1024D;
        if (d1 < 1024D)
        {
            return (new StringBuilder()).append(String.format("%.2f", new Object[] {
                Double.valueOf(d1)
            })).append("MB").toString();
        } else
        {
            d1 /= 1024D;
            return (new StringBuilder()).append(String.format("%.2f", new Object[] {
                Double.valueOf(d1)
            })).append("G").toString();
        }
    }

    public static double formatNumber(int i, double d1)
    {
        return (new BigDecimal(d1)).setScale(i, 4).doubleValue();
    }

    public static String formatTime(long l)
    {
        long l1 = l;
        if (l <= 0L)
        {
            l1 = System.currentTimeMillis();
        }
        return (new SimpleDateFormat("yyyy\u5E74MM\u6708dd\u65E5 HH:mm:ss")).format(new Date(l1));
    }

    public static String formatTime(long l, String s)
    {
        long l1 = l;
        if (l <= 0L)
        {
            l1 = System.currentTimeMillis();
        }
        return (new SimpleDateFormat(s)).format(new Date(l1));
    }

    public static String formatTimeNew(long l, String s)
    {
        return (new SimpleDateFormat(s)).format(new Date(l));
    }

    public static String geDeviceInfo(Context context)
    {
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append(getPhoneVersion()).append("/");
        stringbuffer.append(getSystemVersion()).append("/");
        stringbuffer.append(((MyApplication)context).m());
        return stringbuffer.toString();
    }

    public static String genSignature(Context context, Map map)
    {
        String s;
        TreeMap treemap;
        treemap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        s = null;
        String s3 = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("UMENG_CHANNEL");
        s = s3;
_L2:
        if (!TextUtils.isEmpty(s))
        {
            treemap.put("channel", s);
        }
        if (UserInfoMannage.hasLogined())
        {
            long l = UserInfoMannage.getInstance().getUser().uid;
            String s1 = UserInfoMannage.getInstance().getUser().token;
            treemap.put("uid", String.valueOf(l));
            treemap.put("token", s1);
        }
        treemap.put("device", ((MyApplication)(MyApplication)context.getApplicationContext()).h());
        treemap.put("deviceId", ((MyApplication)(MyApplication)context.getApplicationContext()).i());
        treemap.put("version", ((MyApplication)(MyApplication)context.getApplicationContext()).m());
        treemap.put("impl", context.getPackageName());
        for (context = map.entrySet().iterator(); context.hasNext(); treemap.put((String)map.getKey(), (String)map.getValue()))
        {
            map = (java.util.Map.Entry)context.next();
        }

        break; /* Loop/switch isn't completed */
        Exception exception;
        exception;
        com.ximalaya.ting.android.util.Logger.d("PushStat", "No channel id.");
        if (true) goto _L2; else goto _L1
_L1:
        context = new StringBuffer();
        map = treemap.entrySet().iterator();
        do
        {
            if (!map.hasNext())
            {
                break;
            }
            Object obj = (java.util.Map.Entry)map.next();
            if (!TextUtils.isEmpty(context))
            {
                context.append("&");
            }
            String s2 = (String)((java.util.Map.Entry) (obj)).getKey();
            obj = (String)((java.util.Map.Entry) (obj)).getValue();
            if (!TextUtils.isEmpty(((CharSequence) (obj))))
            {
                context.append(s2).append("=").append(((String) (obj)));
            }
        } while (true);
        context.append("&").append("e1996c7d6e0ff0664b28af93a2eeff8f8cae84b2402d158f7bb115b735a1663d");
        return md5(context.toString().toLowerCase());
    }

    public static String getAndroidId(Context context)
    {
        if (context == null)
        {
            return null;
        } else
        {
            return android.provider.Settings.Secure.getString(context.getContentResolver(), "android_id");
        }
    }

    public static int getAndroidSDKVersion()
    {
        int i;
        try
        {
            i = Integer.valueOf(android.os.Build.VERSION.SDK).intValue();
        }
        catch (NumberFormatException numberformatexception)
        {
            return -1;
        }
        return i;
    }

    public static String getAppVersion(Context context)
    {
        try
        {
            context = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            return "";
        }
        return context;
    }

    public static float getCachesSize()
    {
        float f1;
        float f2;
        float f3;
        String s;
        boolean flag;
        s = null;
        flag = false;
        f3 = 0.0F;
        f1 = 0.0F;
        f2 = 0.0F;
        if (!Environment.getExternalStorageState().equals("mounted")) goto _L2; else goto _L1
_L1:
        File file1;
        File file = new File("/ting/images");
        f1 = f3;
        if (file != null)
        {
            f1 = f3;
            if (file.isDirectory())
            {
                File afile[] = file.listFiles();
                int k = afile.length;
                int i = 0;
                do
                {
                    f1 = f2;
                    if (i >= k)
                    {
                        break;
                    }
                    f1 = afile[i].length();
                    i++;
                    f2 = f1 + f2;
                } while (true);
            }
        }
        file1 = new File(c.d);
        f2 = f1;
        if (file1 == null) goto _L4; else goto _L3
_L3:
        f2 = f1;
        if (!file1.isDirectory()) goto _L4; else goto _L5
_L5:
        Object obj = LocalMediaService.getInstance();
        if (obj == null) goto _L7; else goto _L6
_L6:
        Object obj1 = getIncomPlayingAudioFilename(((LocalMediaService) (obj)));
        if (!Utilities.isNotBlank(((String) (obj1)))) goto _L7; else goto _L8
_L8:
        obj = (new StringBuilder()).append(((String) (obj1))).append(".chunk").toString();
        s = (new StringBuilder()).append(((String) (obj1))).append(".index").toString();
_L17:
        File afile1[];
        int j;
        int l;
        afile1 = file1.listFiles();
        l = afile1.length;
        j = ((flag) ? 1 : 0);
_L12:
        f2 = f1;
        if (j >= l) goto _L4; else goto _L9
_L9:
        obj1 = afile1[j];
        if (obj == null || !((String) (obj)).equalsIgnoreCase(((File) (obj1)).getName())) goto _L11; else goto _L10
_L10:
        f2 = f1;
_L15:
        j++;
        f1 = f2;
          goto _L12
_L11:
        if (s == null) goto _L14; else goto _L13
_L13:
        f2 = f1;
        if (s.equalsIgnoreCase(((File) (obj1)).getName())) goto _L15; else goto _L14
_L14:
        f2 = f1 + (float)((File) (obj1)).length();
          goto _L15
_L4:
        f1 = f2 / 1024F / 1024F;
_L2:
        return f1;
_L7:
        obj = null;
        if (true) goto _L17; else goto _L16
_L16:
    }

    public static String getChannel(Context context)
    {
        try
        {
            context = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("UMENG_CHANNEL");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return "";
        }
        return context;
    }

    public static int getContentViewHeight(Activity activity)
    {
        if (contentViewHeight == 0)
        {
            activity = activity.getWindow().findViewById(0x1020002);
            contentViewHeight = activity.getBottom() - activity.getTop();
        }
        return contentViewHeight;
    }

    public static String getDeviceToken(Context context)
    {
        if (context == null)
        {
            return "";
        }
        Object obj = context.getSharedPreferences("client_preferences", 0);
        if (obj == null)
        {
            return "";
        }
        Object obj1 = (TelephonyManager)context.getSystemService("phone");
        String s = ((SharedPreferences) (obj)).getString("DEVICE_TOKEN", null);
        if (s != null && s.trim().length() != 0 && !s.matches("0+"))
        {
            return ((SharedPreferences) (obj)).getString("DEVICE_TOKEN", null);
        } else
        {
            String s1 = (new StringBuilder()).append("").append(((TelephonyManager) (obj1)).getDeviceId()).toString();
            obj1 = (new StringBuilder()).append("").append(((TelephonyManager) (obj1)).getSimSerialNumber()).toString();
            context = (new UUID((new StringBuilder()).append("").append(android.provider.Settings.Secure.getString(context.getContentResolver(), "android_id")).toString().hashCode(), (long)s1.hashCode() << 32 | (long)((String) (obj1)).hashCode() | (long)"ting".hashCode())).toString();
            obj = ((SharedPreferences) (obj)).edit();
            ((android.content.SharedPreferences.Editor) (obj)).putString("DEVICE_TOKEN", context);
            ((android.content.SharedPreferences.Editor) (obj)).commit();
            return context;
        }
    }

    public static float getDownloadSize()
    {
        float f4 = 0.0F;
        float f1 = 0.0F;
        float f3 = 0.0F;
        if (Environment.getExternalStorageState().equals("mounted"))
        {
            File file = new File(c.a);
            float f2 = f4;
            if (file != null)
            {
                f2 = f4;
                if (file.isDirectory())
                {
                    File afile[] = file.listFiles();
                    int j = afile.length;
                    int i = 0;
                    f1 = f3;
                    do
                    {
                        f2 = f1;
                        if (i >= j)
                        {
                            break;
                        }
                        f2 = afile[i].length();
                        i++;
                        f1 = f2 + f1;
                    } while (true);
                }
            }
            f1 = f2 / 1024F / 1024F;
        }
        return f1;
    }

    private static String getHostName(String s)
    {
        if (s != null && !s.trim().equals(""))
        {
            if ((s = Pattern.compile("(?<=//|)((\\w)+\\.)+\\w+").matcher(s)).find())
            {
                return s.group();
            }
        }
        return "";
    }

    static String getIncomPlayingAudioFilename(Context context)
    {
        context.startService(new Intent(context, com/ximalaya/ting/android/service/play/LocalMediaService));
        context = LocalMediaService.getInstance();
        if (context != null)
        {
            context = context.getCurrentUrl();
            if (context != null && context.contains("http"))
            {
                return md5(context);
            }
        }
        return null;
    }

    public static String getIncomPlayingAudioFilename(LocalMediaService localmediaservice)
    {
        if (localmediaservice != null)
        {
            localmediaservice = localmediaservice.getCurrentUrl();
            if (localmediaservice != null && localmediaservice.contains("http"))
            {
                return md5(localmediaservice);
            }
        }
        return null;
    }

    public static String getLocalMacAddress(Context context)
    {
        if (context == null)
        {
            return "";
        }
        context = (WifiManager)context.getSystemService("wifi");
        if (context == null)
        {
            return "";
        }
        try
        {
            context = context.getConnectionInfo();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Logger.e(context);
            context = null;
        }
        if (context == null)
        {
            return "";
        } else
        {
            return context.getMacAddress();
        }
    }

    public static String getMobileOperatorName(Context context)
    {
        if (context == null)
        {
            return null;
        }
        context = (TelephonyManager)context.getSystemService("phone");
        if (context == null)
        {
            return null;
        }
        context = context.getSimOperator();
        if ("46001".equals(context))
        {
            return "\u4E2D\u56FD\u8054\u901A";
        }
        if ("46002".equals(context))
        {
            return "\u4E2D\u56FD\u79FB\u52A8";
        }
        if ("46003".equals(context))
        {
            return "\u4E2D\u56FD\u7535\u4FE1";
        } else
        {
            return "\u672A\u77E5";
        }
    }

    public static String getPackageMD5()
    {
        if (a.e)
        {
            return "4faced78325e7f39f161dbc35241da22";
        } else
        {
            return "087d52913f1a966b4b3cfa0021377103";
        }
    }

    public static String getPackageName(Context context)
    {
        String s = context.getPackageName();
        context = s;
        if (s == null)
        {
            context = "";
        }
        return context;
    }

    public static String getPhoneVersion()
    {
        return Build.MODEL;
    }

    public static int getPlanStopPlayerDelayMinutes(int i)
    {
        switch (i)
        {
        default:
            return 0;

        case 0: // '\0'
            return 10;

        case 1: // '\001'
            return 20;

        case 2: // '\002'
            return 30;

        case 3: // '\003'
            return 60;

        case 4: // '\004'
            return 90;
        }
    }

    public static Map getQueryMap(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return null;
        }
        String as1[] = s.split("&");
        HashMap hashmap = new HashMap();
        int j = as1.length;
        int i = 0;
        while (i < j) 
        {
            String as[] = as1[i].split("=");
            String s1;
            if (as.length > 0)
            {
                s = as[0];
            } else
            {
                s = "";
            }
            if (as.length > 1)
            {
                s1 = as[1];
            } else
            {
                s1 = "";
            }
            if (!TextUtils.isEmpty(s))
            {
                hashmap.put(s, s1);
            }
            i++;
        }
        return hashmap;
    }

    public static int getScreenHeight(Context context)
    {
        if (screenHeight == 0)
        {
            if (context == null)
            {
                return 0;
            }
            screenHeight = ((WindowManager)context.getSystemService("window")).getDefaultDisplay().getHeight();
        }
        return screenHeight;
    }

    public static int getScreenWidth(Context context)
    {
        if (screenWidth == 0)
        {
            if (context == null)
            {
                return 0;
            }
            screenWidth = ((WindowManager)context.getSystemService("window")).getDefaultDisplay().getWidth();
        }
        return screenWidth;
    }

    public static int getShareContentLength(String s)
    {
        int i = 0;
        int j = 0;
        int k = 0;
        while (i < s.length()) 
        {
            if (isChineseChar(s.charAt(i)))
            {
                j++;
            } else
            {
                k++;
            }
            i++;
        }
        return k / 2 + j;
    }

    public static List getSimContacts(Context context)
    {
        Cursor cursor;
        Object obj;
        ArrayList arraylist;
        obj = null;
        cursor = null;
        arraylist = new ArrayList();
        Cursor cursor1;
        Object obj1;
        obj1 = context.getContentResolver();
        cursor1 = ((ContentResolver) (obj1)).query(android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PHONES_PROJECTION, null, null, null);
        cursor = cursor1;
        if (cursor == null) goto _L2; else goto _L1
_L1:
        do
        {
            if (!cursor.moveToNext())
            {
                break;
            }
            String s = cursor.getString(1);
            if (!Utilities.isEmpty(s))
            {
                String s1 = s.replaceAll("[-\u2014\u2014_ \\*\\+]", "");
                s = cursor.getString(0);
                ThirdPartyUserInfo thirdpartyuserinfo1 = new ThirdPartyUserInfo();
                thirdpartyuserinfo1.nickname = s;
                thirdpartyuserinfo1.identity = s1;
                arraylist.add(thirdpartyuserinfo1);
            }
        } while (true);
          goto _L2
_L4:
        if (cursor == null)
        {
            break MISSING_BLOCK_LABEL_132;
        }
        cursor.close();
        boolean flag;
        if (context != null)
        {
            try
            {
                context.close();
            }
            // Misplaced declaration of an exception variable
            catch (Context context) { }
        }
        return arraylist;
_L2:
        flag = "1".equals(isSimExist(context));
        if (flag)
        {
            break MISSING_BLOCK_LABEL_187;
        }
        if (cursor == null)
        {
            break MISSING_BLOCK_LABEL_172;
        }
        cursor.close();
        ThirdPartyUserInfo thirdpartyuserinfo;
        if (false)
        {
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (Context context) { }
        }
        return arraylist;
        try
        {
            context = ((ContentResolver) (obj1)).query(Uri.parse("content://icc/adn"), PHONES_PROJECTION, null, null, null);
            break MISSING_BLOCK_LABEL_205;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context = null;
        }
        finally { }
        if (true) goto _L4; else goto _L3
_L3:
        context;
        cursor = null;
        if (cursor == null)
        {
            break MISSING_BLOCK_LABEL_291;
        }
        cursor.close();
        if (obj != null)
        {
            try
            {
                ((Cursor) (obj)).close();
            }
            catch (Exception exception) { }
        }
        throw context;
        if (context != null)
        {
            try
            {
                do
                {
                    if (!context.moveToNext())
                    {
                        break;
                    }
                    obj1 = context.getString(1);
                    if (!Utilities.isEmpty(((String) (obj1))))
                    {
                        obj = context.getString(0);
                        thirdpartyuserinfo = new ThirdPartyUserInfo();
                        thirdpartyuserinfo.nickname = ((String) (obj));
                        thirdpartyuserinfo.identity = ((String) (obj1));
                        arraylist.add(thirdpartyuserinfo);
                    }
                } while (true);
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                continue; /* Loop/switch isn't completed */
            }
            finally
            {
                obj = context;
                context = exception1;
            }
        }
        if (cursor == null)
        {
            break MISSING_BLOCK_LABEL_313;
        }
        cursor.close();
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_142;
        }
        context.close();
        break MISSING_BLOCK_LABEL_142;
        context;
        context = null;
        if (true) goto _L4; else goto _L5
_L5:
    }

    public static String getSimpleSystemVersion()
    {
        String s = android.os.Build.VERSION.SDK;
        return (new StringBuilder()).append("Android").append(s).toString();
    }

    public static String getSingInfoMd5(Context context)
    {
        String s = "com.ximalaya.ting.android";
        if (a.e)
        {
            s = "com.ximalaya.tinghd.android";
        }
        try
        {
            context = MD5.md5(context.getPackageManager().getPackageInfo(s, 64).signatures[0].toByteArray());
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return "";
        }
        return context;
    }

    public static int getStatusBarHeight(Activity activity)
    {
        if (statusBarHeight == 0)
        {
            Rect rect = new Rect();
            activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
            statusBarHeight = rect.top;
        }
        return statusBarHeight;
    }

    public static int getStatusBarHeight(Context context)
    {
        if (statusBarHeight == 0)
        {
            int i;
            try
            {
                Class class1 = Class.forName("com.android.internal.R$dimen");
                Object obj = class1.newInstance();
                i = Integer.parseInt(class1.getField("status_bar_height").get(obj).toString());
                statusBarHeight = dp2px(context, context.getResources().getDimensionPixelSize(i));
                i = statusBarHeight;
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
                return dp2px(context, 50F);
            }
            return i;
        } else
        {
            return statusBarHeight;
        }
    }

    public static String getSystemVersion()
    {
        String s = android.os.Build.VERSION.SDK;
        return (new StringBuilder()).append("Android-").append(s).toString();
    }

    public static float getTotalSpaceSize()
    {
        return (new BigDecimal(getDownloadSize() + getCachesSize())).setScale(1, 4).floatValue();
    }

    public static String getUrlIp(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return null;
        }
        try
        {
            s = InetAddress.getByName(getHostName(s)).getHostAddress();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return null;
        }
        return s;
    }

    public static String hexString(byte abyte0[])
    {
        return String.format((new StringBuilder()).append("%0").append(abyte0.length << 1).append("x").toString(), new Object[] {
            new BigInteger(1, abyte0)
        });
    }

    public static byte[] imageZoom32(Bitmap bitmap)
    {
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
        bitmap.compress(android.graphics.Bitmap.CompressFormat.JPEG, 100, bytearrayoutputstream);
        byte abyte0[] = bytearrayoutputstream.toByteArray();
        double d1 = abyte0.length / 1024;
        if (d1 > 32D)
        {
            d1 /= 32D;
            float f1 = (float)((double)bitmap.getWidth() / Math.sqrt(d1));
            float f2 = (float)((double)(float)bitmap.getHeight() / Math.sqrt(d1));
            float f3 = bitmap.getWidth();
            float f4 = bitmap.getHeight();
            abyte0 = new Matrix();
            abyte0.postScale(f1 / f3, f2 / f4);
            Bitmap.createBitmap(bitmap, 0, 0, (int)f3, (int)f4, abyte0, true).compress(android.graphics.Bitmap.CompressFormat.JPEG, 100, bytearrayoutputstream);
            bitmap = bytearrayoutputstream.toByteArray();
            try
            {
                bytearrayoutputstream.close();
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
            Logger.log((new StringBuilder()).append("\u538B\u7F29\u540E\u56FE\u7247\u5927\u5C0Fbitmap.compress\uFF1A").append(bitmap.length).toString());
            return bitmap;
        }
        try
        {
            bytearrayoutputstream.close();
        }
        // Misplaced declaration of an exception variable
        catch (Bitmap bitmap)
        {
            bitmap.printStackTrace();
            return abyte0;
        }
        return abyte0;
    }

    public static byte[] imageZoom32(byte abyte0[])
    {
        if (abyte0 == null || abyte0.length == 0)
        {
            return abyte0;
        }
        ByteArrayOutputStream bytearrayoutputstream;
        byte abyte1[];
        try
        {
            Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeByteArray(abyte0, 0, abyte0.length), 150, 150, 2);
            bytearrayoutputstream = new ByteArrayOutputStream();
            bitmap.compress(android.graphics.Bitmap.CompressFormat.JPEG, 70, bytearrayoutputstream);
            abyte1 = bytearrayoutputstream.toByteArray();
        }
        catch (Exception exception)
        {
            return abyte0;
        }
        try
        {
            Logger.log((new StringBuilder()).append("\u538B\u7F29\u540E\u56FE\u7247\u5927\u5C0Fbitmap.compress\uFF1A").append(abyte1.length / 1024).toString());
            bytearrayoutputstream.close();
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            return abyte1;
        }
        return abyte1;
    }

    public static String inStream2String(InputStream inputstream)
        throws Exception
    {
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
        byte abyte0[] = new byte[1024];
        do
        {
            int i = inputstream.read(abyte0);
            if (i != -1)
            {
                bytearrayoutputstream.write(abyte0, 0, i);
            } else
            {
                return new String(bytearrayoutputstream.toByteArray());
            }
        } while (true);
    }

    public static boolean isChineseChar(char c1)
    {
        return String.valueOf(c1).getBytes().length > 1;
    }

    public static boolean isConnectToNetwork(Context context)
    {
        if (context == null)
        {
            return false;
        }
        context = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        return context != null && context.isAvailable() && context.isConnected();
    }

    public static boolean isFastClick()
    {
        com/ximalaya/ting/android/util/ToolUtil;
        JVM INSTR monitorenter ;
        long l;
        long l1;
        l = System.currentTimeMillis();
        l1 = lastClickTime;
        if (l - l1 >= 500L) goto _L2; else goto _L1
_L1:
        boolean flag = true;
_L4:
        com/ximalaya/ting/android/util/ToolUtil;
        JVM INSTR monitorexit ;
        return flag;
_L2:
        lastClickTime = l;
        flag = false;
        if (true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    public static boolean isFirstLaunch(Context context)
    {
label0:
        {
            boolean flag = false;
            PackageManager packagemanager = context.getPackageManager();
            try
            {
                int i = packagemanager.getPackageInfo(context.getPackageName(), 0).versionCode;
                context = SharedPreferencesUtil.getInstance(context);
                if (context.getInt("launched_version_code", -1) == i)
                {
                    break label0;
                }
                context.saveInt("launched_version_code", i);
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                context.printStackTrace();
                return false;
            }
            flag = true;
        }
        return flag;
    }

    public static boolean isFirstLaunchRadio(Context context)
    {
        return SharedPreferencesUtil.getInstance(context).getBoolean("first_launch_radio", true);
    }

    public static boolean isLivePlaying(int i)
    {
        return PlayListControl.getPlayListManager().getCurSound() != null && i > 0 && i == PlayListControl.getPlayListManager().getCurSound().radioId;
    }

    public static boolean isLiveSoundInValidDate(Date date)
    {
        if (date != null)
        {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            date = Calendar.getInstance();
            if (calendar.get(1) == date.get(1) && calendar.get(2) == date.get(2) && calendar.get(5) == date.get(5))
            {
                return true;
            }
        }
        return false;
    }

    public static String isSimExist(Context context)
    {
        switch (((TelephonyManager)context.getSystemService("phone")).getSimState())
        {
        default:
            return "";

        case 1: // '\001'
            return "\u68C0\u6D4B\u4E0D\u5230SIM\u5361";

        case 4: // '\004'
            return "\u9700\u8981NetworkPIN\u89E3\u9501";

        case 2: // '\002'
            return "\u9700\u8981PIN\u89E3\u9501";

        case 3: // '\003'
            return "\u9700\u8981PUN\u89E3\u9501";

        case 5: // '\005'
            return "1";

        case 0: // '\0'
            return "\u672A\u77E5\u72B6\u6001";
        }
    }

    public static boolean isSuccess(String s)
    {
        if (!TextUtils.isEmpty(s))
        {
            int i;
            try
            {
                i = JSON.parseObject(s).getInteger("ret").intValue();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return false;
            }
            if (i == 0)
            {
                return true;
            }
        }
        return false;
    }

    public static boolean isTrue(String s)
    {
        return s != null && !s.equals("") && !s.equals("null") && !s.equals("false");
    }

    public static boolean isUseSmartbarAsTab()
    {
        return false;
    }

    public static byte[] loadByteArrayFromNetwork(String s)
    {
        MyApplication myapplication;
        try
        {
            s = new HttpGet(s);
            myapplication = (MyApplication)MyApplication.b();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return null;
        }
        if (myapplication == null)
        {
            return null;
        }
        s = EntityUtils.toByteArray(myapplication.j().execute(s).getEntity());
        return s;
    }

    public static void makeAlarmLaterNotification(Context context)
    {
        NotificationManager notificationmanager = (NotificationManager)context.getSystemService("notification");
        android.support.v4.app.NotificationCompat.Builder builder = new android.support.v4.app.NotificationCompat.Builder(context);
        RemoteViews remoteviews = new RemoteViews(context.getPackageName(), 0x7f030180);
        remoteviews.setTextViewText(0x7f0a0175, context.getString(0x7f0901b5));
        remoteviews.setTextViewText(0x7f0a0021, context.getString(0x7f0901b6));
        remoteviews.setTextViewText(0x7f0a05d4, context.getString(0x7f0901b7));
        remoteviews.setImageViewResource(0x7f0a01ab, 0x7f020598);
        Object obj = new Intent(context, com/ximalaya/ting/android/broadcast/AlarmReceiver2);
        ((Intent) (obj)).setAction("com.ximalaya.ting.android.activity.alarm.Cancel_Alarm");
        obj = PendingIntent.getBroadcast(context, 11, ((Intent) (obj)), 0);
        remoteviews.setOnClickPendingIntent(0x7f0a05d4, ((PendingIntent) (obj)));
        remoteviews.setOnClickPendingIntent(0x7f0a05d4, ((PendingIntent) (obj)));
        obj = new Intent(context, com/ximalaya/ting/android/activity/setting/WakeUpSettingActivity);
        ((Intent) (obj)).setFlags(0x20000000);
        obj = PendingIntent.getActivity(context, 11, ((Intent) (obj)), 0);
        context = builder.setContent(remoteviews).setWhen(System.currentTimeMillis()).setSmallIcon(0x7f020598).setTicker(context.getString(0x7f0901b5)).setContentIntent(((PendingIntent) (obj))).build();
        context.flags = 32;
        notificationmanager.notify(6, context);
    }

    public static void makeAlarmNotification(Context context, String s, String s1, String s2)
    {
        NotificationManager notificationmanager = (NotificationManager)context.getSystemService("notification");
        Notification notification = new Notification();
        notification.icon = 0x7f020598;
        notification.tickerText = s;
        notification.when = System.currentTimeMillis();
        s = new Intent(context, com/ximalaya/ting/android/activity/setting/WakeUpSettingActivity);
        s.putExtra("tab_index", 0);
        s.putExtra("soundsIndex", 0);
        s = PendingIntent.getActivity(context, 0, s, 0);
        notification.defaults = notification.defaults | 1;
        notification.defaults = notification.defaults | 2;
        notification.flags = notification.flags | 0x10;
        notification.setLatestEventInfo(context, s1, s2, s);
        notificationmanager.notify(1, notification);
    }

    public static void makeDownloadNotification(Context context, String s, String s1, String s2, int i)
    {
        while (context == null || MyApplication.b() == null) 
        {
            return;
        }
        NotificationManager notificationmanager = (NotificationManager)MyApplication.b().getSystemService("notification");
        Notification notification = new Notification();
        notification.icon = 0x7f020598;
        notification.tickerText = s;
        notification.when = System.currentTimeMillis();
        s = PendingIntent.getActivity(context, 0, new Intent(), 0);
        notification.defaults = notification.defaults | 1;
        notification.defaults = notification.defaults | 2;
        notification.flags = notification.flags | 0x10;
        notification.setLatestEventInfo(context, s1, s2, s);
        notificationmanager.notify(i, notification);
    }

    public static void makePlayNotification(Context context, String s, String s1, String s2)
    {
        if (context == null)
        {
            return;
        } else
        {
            NotificationManager notificationmanager = (NotificationManager)context.getSystemService("notification");
            Notification notification = new Notification();
            notification.icon = 0x7f020598;
            notification.tickerText = s;
            notification.when = System.currentTimeMillis();
            s = new Intent(context, com/ximalaya/ting/android/activity/MainTabActivity2);
            s.putExtra(a.t, com/ximalaya/ting/android/fragment/play/PlayerFragment);
            s.addFlags(0x20000);
            s = PendingIntent.getActivity(context, 0, s, 0);
            notification.defaults = notification.defaults | 1;
            notification.defaults = notification.defaults | 2;
            notification.flags = notification.flags | 0x10;
            notification.setLatestEventInfo(context, s1, s2, s);
            notificationmanager.notify(2, notification);
            return;
        }
    }

    public static String md5(InputStream inputstream)
    {
        MessageDigest messagedigest;
        byte abyte0[];
        messagedigest = MessageDigest.getInstance("MD5");
        abyte0 = new byte[8192];
_L1:
        int i = inputstream.read(abyte0);
label0:
        {
            if (i <= 0)
            {
                break label0;
            }
            try
            {
                messagedigest.update(abyte0, 0, i);
            }
            // Misplaced declaration of an exception variable
            catch (InputStream inputstream)
            {
                throw new RuntimeException(inputstream);
            }
            // Misplaced declaration of an exception variable
            catch (InputStream inputstream)
            {
                return "";
            }
        }
          goto _L1
        inputstream = hexString(messagedigest.digest());
        return inputstream;
    }

    public static String md5(String s)
    {
        if (s == null || "".equalsIgnoreCase(s))
        {
            return "playUrl is null";
        } else
        {
            return md5(((InputStream) (new ByteArrayInputStream(s.getBytes()))));
        }
    }

    public static String md5(byte abyte0[], int i)
        throws NoSuchAlgorithmException
    {
        Object obj = MessageDigest.getInstance("MD5");
        ((MessageDigest) (obj)).update(abyte0);
        abyte0 = ((MessageDigest) (obj)).digest();
        obj = new StringBuffer();
        for (int j = 0; j < abyte0.length; j++)
        {
            byte byte0 = abyte0[j];
            int k = byte0;
            if (byte0 < 0)
            {
                k = byte0 + 256;
            }
            if (k < 16)
            {
                ((StringBuffer) (obj)).append("0");
            }
            ((StringBuffer) (obj)).append(Integer.toHexString(k));
        }

        if (i == 16)
        {
            return ((StringBuffer) (obj)).toString().substring(8, 24);
        } else
        {
            return ((StringBuffer) (obj)).toString();
        }
    }

    public static String md516(String s)
    {
        try
        {
            s = md5(s.getBytes("UTF-8"), 16);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return null;
        }
        return s;
    }

    public static void mobileResume()
    {
        if (UserInfoMannage.hasLogined())
        {
            f.a().b("mobile/resume", null, null, null);
        }
    }

    public static void onEvent(Context context, String s)
    {
        MobclickAgent.onEvent(context, s);
        com.ximalaya.ting.android.util.Logger.d(TAG, (new StringBuilder()).append("\u4E8B\u4EF6\u7EDF\u8BA1").append(s).toString());
    }

    public static void onEvent(Context context, String s, String s1)
    {
        MobclickAgent.onEvent(context, s, s1);
        com.ximalaya.ting.android.util.Logger.d(TAG, (new StringBuilder()).append(s).append(":").append(s1).toString());
    }

    public static int px2dp(Context context, float f1)
    {
        return (int)(f1 / context.getResources().getDisplayMetrics().density + 0.5F);
    }

    public static void setActionBarTabsShowAtBottom(ActionBar actionbar, boolean flag)
    {
        Method method = Class.forName("android.app.ActionBar").getMethod("setTabsShowAtBottom", new Class[] {
            Boolean.TYPE
        });
        method.invoke(actionbar, new Object[] {
            Boolean.valueOf(flag)
        });
        return;
        actionbar;
        try
        {
            actionbar.printStackTrace();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (ActionBar actionbar)
        {
            actionbar.printStackTrace();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (ActionBar actionbar)
        {
            actionbar.printStackTrace();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (ActionBar actionbar)
        {
            actionbar.printStackTrace();
        }
        break MISSING_BLOCK_LABEL_76;
        actionbar;
        actionbar.printStackTrace();
        return;
        actionbar;
        actionbar.printStackTrace();
        return;
    }

    public static void setIsntFirstLaunchRadio(Context context)
    {
        SharedPreferencesUtil.getInstance(context).saveBoolean("first_launch_radio", false);
    }

    public static void setNextAlarm(Context context)
    {
        AlarmManager alarmmanager = (AlarmManager)context.getSystemService("alarm");
        SharedPreferences sharedpreferences = context.getSharedPreferences("alarm_setting", 0);
        PendingIntent pendingintent = PendingIntent.getBroadcast(context, 0, new Intent(context, com/ximalaya/ting/android/broadcast/AlarmReceiver2), 0);
        alarmmanager.cancel(pendingintent);
        int i = sharedpreferences.getInt("alarm_hour", -1);
        int j = sharedpreferences.getInt("alarm_minute", -1);
        int k = sharedpreferences.getInt("repeat_week_days", 0);
        if (i < 0 || j < 0)
        {
            return;
        } else
        {
            alarmmanager.set(0, calculateAlarm(i, j, new DaysOfWeek(k)), pendingintent);
            Toast.makeText(context, "\u4E0B\u4E00\u6B21\u95F9\u949F\u8BBE\u7F6E\u6210\u529F", 0).show();
            return;
        }
    }

    public static void showToast(String s)
    {
        if (!MyApplication.c())
        {
            return;
        } else
        {
            MyApplication.f.runOnUiThread(new bd(s));
            return;
        }
    }

    public static void showUploadPhoneNumDialog(Activity activity)
    {
        (new DialogBuilder(activity)).setTitle("\u63D0\u793A").setOkBtn("\u786E\u5B9A", new az(activity)).setCancelBtn("\u53D6\u6D88").setMessage("\u770B\u770B\u624B\u673A\u901A\u8BAF\u5F55\u91CC\u8C01\u5728\u4F7F\u7528\u559C\u9A6C\u62C9\u96C5FM\uFF1F").showConfirm();
    }

    public static void startMainAct(Context context)
    {
        if (MyApplication.f != null && (MyApplication.f instanceof MainTabActivity2))
        {
            return;
        } else
        {
            Intent intent = new Intent();
            intent.setClass(context, com/ximalaya/ting/android/activity/MainTabActivity2);
            intent.setAction("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.setFlags(0x14000000);
            context.startActivity(intent);
            return;
        }
    }

    public static void startMainApp(Context context)
    {
        if (MainTabActivity2.mainTabActivity != null)
        {
            return;
        } else
        {
            Intent intent = new Intent();
            intent.setClass(context, com/ximalaya/ting/android/activity/MainTabActivity2);
            intent.setAction("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.setFlags(0x10000000);
            context.startActivity(intent);
            return;
        }
    }

    public static void switchOnline(boolean flag)
    {
        if (flag)
        {
            a.u = "http://mobile.ximalaya.com/";
            a.Q = "http://ad.ximalaya.com/";
            a.S = "http://activity.ximalaya.com/";
            a.v = "http://m.ximalaya.com/";
            a.d = 1;
            d.a = "dmmqQ71hE2vniB2u2TXQ19PP";
            d.b = "pKWDuh1n2013_!^%$";
            a.B = "http://hybrid.ximalaya.com/pages/addv_intro.html";
            a.C = "http://hybrid.ximalaya.com/pages/zone/zone_create.html";
            a.E = "http://hybrid.ximalaya.com/";
            a.A = "http://ifm.ximalaya.com/";
            a.F = "http://upload.ximalaya.com/";
            a.y = "http://xzone.ximalaya.com/x-zone-post/";
            a.z = "http://xzone.ximalaya.com/x-zone-management/";
            a.F = "http://upload.ximalaya.com/";
            a.x = "http://fdfs.ximalaya.com/";
            a.G = "http://ar.ximalaya.com/";
            a.H = "http://pns.ximalaya.com/";
            a.O = "http://live.ximalaya.com/live-web/v1/";
            a.P = "http://search.ximalaya.com/";
            a.I = "http://adse.ximalaya.com/";
            a.J = "http://play.ximalaya.com/";
            a.K = "http://xdcs-collector.ximalaya.com/";
            a.M = "http://mobile.ximalaya.com/";
            a.R = "http://ad.ximalaya.com/";
            a.T = "http://e.ximalaya.com/";
            a.U = "http://hybrid.ximalaya.com/";
            a.V = "http://ma.ximalaya.com/";
            a.W = "http://adweb.ximalaya.com/broadcaster/getAdTipPage";
            return;
        } else
        {
            a.u = "http://mobile.test.ximalaya.com/";
            a.v = "http://m.test.ximalaya.com/";
            a.Q = "http://ad.test.ximalaya.com/";
            a.S = "http://activity.test.ximalaya.com/";
            a.E = "http://hybrid.test.ximalaya.com/";
            a.d = 4;
            d.a = "vESkrDQF1lc8D8PLG30oOydp";
            d.b = "_umadijk2013om!^%$";
            a.B = "http://hybrid.test.ximalaya.com/pages/addv_intro.html";
            a.C = "http://hybrid.test.ximalaya.com/pages/zone/zone_create.html";
            a.A = "http://ifm.test.ximalaya.com/";
            a.F = "http://upload.test.ximalaya.com/";
            a.y = "http://xzone.test.ximalaya.com/x-zone-post/";
            a.z = "http://xzone.test.ximalaya.com/x-zone-management/";
            a.F = "http://upload.test.ximalaya.com/";
            a.x = "http://fdfs.test.ximalaya.com/";
            a.G = "http://192.168.1.173:92/";
            a.H = "http://pns.test.ximalaya.com/";
            a.O = "http://live.test.ximalaya.com/live-web/v1/";
            a.P = "http://search.test.ximalaya.com/";
            a.I = "http://adse.test.ximalaya.com/";
            a.J = "http://play.test.ximalaya.com/";
            a.K = "http://xdcs-collector.test.ximalaya.com/";
            a.M = "http://test.ximalaya.com/";
            a.R = "http://ad.test.ximalaya.com/";
            a.T = "http://e.test.ximalaya.com/";
            a.U = "http://hybrid.test.ximalaya.com/";
            a.V = "http://ma.test.ximalaya.com/";
            a.W = "http://adweb.test.ximalaya.com/broadcaster/getAdTipPage";
            return;
        }
    }

    public static final double toMBFormat(double d1)
    {
        return formatNumber(2, d1 / 1024D / 1024D);
    }

    public static final String toMBFormatString(double d1)
    {
        return String.format("%.2f", new Object[] {
            Double.valueOf(d1 / 1024D / 1024D)
        });
    }

    public static String toTime(double d1)
    {
        if (d1 >= 3600D)
        {
            int i = (int)(d1 / 3600D);
            int i1 = (int)(d1 % 3600D);
            if (i1 >= 60)
            {
                int k = i1 / 60;
                i1 %= 60;
                StringBuilder stringbuilder = new StringBuilder();
                String s;
                if (i < 10)
                {
                    s = (new StringBuilder()).append("0").append(i).toString();
                } else
                {
                    s = (new StringBuilder()).append("").append(i).toString();
                }
                stringbuilder = stringbuilder.append(s).append(":");
                if (k < 10)
                {
                    s = (new StringBuilder()).append("0").append(k).toString();
                } else
                {
                    s = (new StringBuilder()).append("").append(k).toString();
                }
                stringbuilder = stringbuilder.append(s).append(":");
                if (i1 < 10)
                {
                    s = (new StringBuilder()).append("0").append(i1).toString();
                } else
                {
                    s = (new StringBuilder()).append("").append(i1).toString();
                }
                return stringbuilder.append(s).toString();
            }
            StringBuilder stringbuilder1 = new StringBuilder();
            String s1;
            if (i < 10)
            {
                s1 = (new StringBuilder()).append("0").append(i).toString();
            } else
            {
                s1 = (new StringBuilder()).append("").append(i).toString();
            }
            stringbuilder1 = stringbuilder1.append(s1).append(":");
            if (i1 < 10)
            {
                s1 = (new StringBuilder()).append("00:0").append(i1).toString();
            } else
            {
                s1 = (new StringBuilder()).append("00:").append(i1).toString();
            }
            return stringbuilder1.append(s1).toString();
        }
        if (d1 >= 60D)
        {
            int j = (int)(d1 / 60D);
            int l = (int)(d1 % 60D);
            StringBuilder stringbuilder2 = new StringBuilder();
            String s2;
            if (j < 10)
            {
                s2 = (new StringBuilder()).append("0").append(j).toString();
            } else
            {
                s2 = (new StringBuilder()).append("").append(j).toString();
            }
            stringbuilder2 = stringbuilder2.append(s2).append(":");
            if (l < 10)
            {
                s2 = (new StringBuilder()).append("0").append(l).toString();
            } else
            {
                s2 = (new StringBuilder()).append("").append(l).toString();
            }
            return stringbuilder2.append(s2).toString();
        }
        if (d1 < 10D)
        {
            return (new StringBuilder()).append("00:0").append((int)d1).toString();
        } else
        {
            return (new StringBuilder()).append("00:").append((int)d1).toString();
        }
    }

    public static String toTimeForHistory(double d1, double d2, boolean flag)
    {
        if (d1 + 10D >= d2)
        {
            return "\u5DF2\u64AD\u5B8C";
        }
        if (d1 >= 3600D)
        {
            int i = (int)(d1 / 3600D);
            int i1 = (int)(d1 % 3600D);
            if (i1 >= 60)
            {
                int k = i1 / 60;
                i1 %= 60;
                StringBuilder stringbuilder = new StringBuilder();
                String s;
                if (flag)
                {
                    s = "\u5DF2\u64AD\u653E\u5230";
                } else
                {
                    s = "";
                }
                stringbuilder = stringbuilder.append(s);
                if (i < 10)
                {
                    s = (new StringBuilder()).append("0").append(i).toString();
                } else
                {
                    s = (new StringBuilder()).append("").append(i).toString();
                }
                stringbuilder = stringbuilder.append(s).append("\u65F6");
                if (k < 10)
                {
                    s = (new StringBuilder()).append("0").append(k).toString();
                } else
                {
                    s = (new StringBuilder()).append("").append(k).toString();
                }
                stringbuilder = stringbuilder.append(s).append("\u5206");
                if (i1 < 10)
                {
                    s = (new StringBuilder()).append("0").append(i1).toString();
                } else
                {
                    s = (new StringBuilder()).append("").append(i1).toString();
                }
                return stringbuilder.append(s).append("\u79D2").toString();
            }
            StringBuilder stringbuilder1 = new StringBuilder();
            String s1;
            if (flag)
            {
                s1 = "\u5DF2\u64AD\u653E\u5230";
            } else
            {
                s1 = "";
            }
            stringbuilder1 = stringbuilder1.append(s1);
            if (i < 10)
            {
                s1 = (new StringBuilder()).append("0").append(i).toString();
            } else
            {
                s1 = (new StringBuilder()).append("").append(i).toString();
            }
            stringbuilder1 = stringbuilder1.append(s1).append("\u65F6");
            if (i1 < 10)
            {
                s1 = (new StringBuilder()).append("00\u52060").append(i1).toString();
            } else
            {
                s1 = (new StringBuilder()).append("00\u5206").append(i1).toString();
            }
            return stringbuilder1.append(s1).append("\u79D2").toString();
        }
        if (d1 >= 60D)
        {
            int j = (int)(d1 / 60D);
            int l = (int)(d1 % 60D);
            StringBuilder stringbuilder2 = new StringBuilder();
            String s2;
            if (flag)
            {
                s2 = "\u5DF2\u64AD\u653E\u5230";
            } else
            {
                s2 = "";
            }
            stringbuilder2 = stringbuilder2.append(s2);
            if (j < 10)
            {
                s2 = (new StringBuilder()).append("0").append(j).toString();
            } else
            {
                s2 = (new StringBuilder()).append("").append(j).toString();
            }
            stringbuilder2 = stringbuilder2.append(s2).append("\u5206");
            if (l < 10)
            {
                s2 = (new StringBuilder()).append("0").append(l).toString();
            } else
            {
                s2 = (new StringBuilder()).append("").append(l).toString();
            }
            return stringbuilder2.append(s2).append("\u79D2").toString();
        }
        StringBuilder stringbuilder3 = new StringBuilder();
        String s3;
        if (flag)
        {
            s3 = "\u5DF2\u64AD\u653E\u5230";
        } else
        {
            s3 = "";
        }
        stringbuilder3 = stringbuilder3.append(s3);
        if (d1 < 10D)
        {
            s3 = (new StringBuilder()).append("00\u52060").append((int)d1).append("\u79D2").toString();
        } else
        {
            s3 = (new StringBuilder()).append("00\u5206").append((int)d1).append("\u79D2").toString();
        }
        return stringbuilder3.append(s3).toString();
    }

    public static void uploadAllPhoneNum(Activity activity)
    {
        String s = SharedPreferencesUtil.getInstance(activity).getString("last_upload_phone_num_time");
        if (!TextUtils.isEmpty(s) && (System.currentTimeMillis() - Long.valueOf(s).longValue()) / 0x5265c00L < 15L)
        {
            return;
        } else
        {
            SharedPreferencesUtil.getInstance(activity).saveString("last_upload_phone_num_time", (new StringBuilder()).append(System.currentTimeMillis()).append("").toString());
            (new ba(activity)).execute(new Object[] {
                activity
            });
            return;
        }
    }

    public static boolean verifiEmail(String s)
    {
        s = Pattern.compile("^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$").matcher(s);
        s.matches();
        return s.matches();
    }

    public static boolean verifiNickName(String s)
    {
        s = Pattern.compile("^[_A-za-z0-9|\u4E00-\u9FA5]+$").matcher(s);
        s.matches();
        return s.matches();
    }

    public static boolean verifiPhone(String s)
    {
        return Pattern.compile("^(1)\\d{10}$").matcher(s).matches();
    }


}
