// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.view.View;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;

public class CommonRequestNew
{

    public CommonRequestNew()
    {
    }

    public static void subjectClickCount(long l, long l1, View view)
    {
        if (l == 0L || l1 == 0L)
        {
            return;
        } else
        {
            view = (new StringBuilder()).append("m/subjects/").append(l).append("/contents/").append(l1).append("/clicked").toString();
            f.a().a(view, new RequestParams(), null, null);
            return;
        }
    }

    public static void subjectClickCount(long l, long l1, View view, String s, int i)
    {
        view = new RequestParams();
        if (l == 0L || l1 == 0L)
        {
            return;
        }
        if (s != null && i >= 0)
        {
            view.add("position", (new StringBuilder()).append(i).append("").toString());
            view.add("title", s);
        }
        s = (new StringBuilder()).append("m/subjects/").append(l).append("/contents/").append(l1).append("/clicked").toString();
        f.a().a(s, view, null, null);
    }
}
