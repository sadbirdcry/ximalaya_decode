// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Set;

public class BuildProperties
{

    private final Properties properties = new Properties();

    private BuildProperties()
        throws IOException
    {
        properties.load(new FileInputStream(new File(Environment.getRootDirectory(), "build.prop")));
    }

    public static BuildProperties newInstance()
        throws IOException
    {
        return new BuildProperties();
    }

    public boolean containsKey(Object obj)
    {
        return properties.containsKey(obj);
    }

    public boolean containsValue(Object obj)
    {
        return properties.containsValue(obj);
    }

    public Set entrySet()
    {
        return properties.entrySet();
    }

    public String getProperty(String s)
    {
        return properties.getProperty(s);
    }

    public String getProperty(String s, String s1)
    {
        return properties.getProperty(s, s1);
    }

    public boolean isEmpty()
    {
        return properties.isEmpty();
    }

    public Set keySet()
    {
        return properties.keySet();
    }

    public Enumeration keys()
    {
        return properties.keys();
    }

    public int size()
    {
        return properties.size();
    }

    public Collection values()
    {
        return properties.values();
    }
}
