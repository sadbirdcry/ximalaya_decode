// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.ximalaya.ting.android.util:
//            n

public class DiskLruCache
{

    private static final String CACHE_FILENAME_PREFIX = "cache_";
    private static final int INITIAL_CAPACITY = 32;
    private static final float LOAD_FACTOR = 0.75F;
    private static final int MAX_REMOVALS = 4;
    private static final FilenameFilter cacheFileFilter = new n();
    private final int IO_BUFFER_SIZE = 4096;
    private int cacheByteSize;
    private int cacheSize;
    private final File mCacheDir;
    private android.graphics.Bitmap.CompressFormat mCompressFormatJPG;
    private android.graphics.Bitmap.CompressFormat mCompressFormatPNG;
    private int mCompressQuality;
    private final Map mLinkedHashMap = Collections.synchronizedMap(new LinkedHashMap(32, 0.75F, true));
    private long maxCacheByteSize;
    private final int maxCacheItemSize = 64;

    private DiskLruCache(File file, long l)
    {
        cacheSize = 0;
        cacheByteSize = 0;
        maxCacheByteSize = 0x500000L;
        mCompressFormatJPG = android.graphics.Bitmap.CompressFormat.JPEG;
        mCompressFormatPNG = android.graphics.Bitmap.CompressFormat.PNG;
        mCompressQuality = 70;
        mCacheDir = file;
        maxCacheByteSize = l;
    }

    public static void clearCache(Context context, String s)
    {
        clearCache(getDiskCacheDir(context, s));
    }

    public static void clearCache(File file)
    {
        file = file.listFiles(cacheFileFilter);
        for (int i = 0; i < file.length; i++)
        {
            file[i].delete();
        }

    }

    private String createFilePath(File file, String s)
    {
        try
        {
            file = (new StringBuilder()).append(file.getAbsolutePath()).append(File.separator).append("cache_").append(URLEncoder.encode(s.replace("*", "").replaceAll("\\.", "--"), "UTF-8")).toString();
        }
        // Misplaced declaration of an exception variable
        catch (File file)
        {
            return null;
        }
        return file;
    }

    private void flushCache()
    {
        for (int i = 0; i < 4 && (cacheSize > 64 || (long)cacheByteSize > maxCacheByteSize); i++)
        {
            java.util.Map.Entry entry = (java.util.Map.Entry)mLinkedHashMap.entrySet().iterator().next();
            File file = new File((String)entry.getValue());
            long l = file.length();
            mLinkedHashMap.remove(entry.getKey());
            file.delete();
            cacheSize = mLinkedHashMap.size();
            cacheByteSize = (int)((long)cacheByteSize - l);
        }

    }

    public static File getDiskCacheDir(Context context, String s)
    {
        context = context.getCacheDir().getPath();
        return new File((new StringBuilder()).append(context).append(File.separator).append(s).toString());
    }

    public static DiskLruCache openCache(Context context, File file, long l)
    {
        if (!file.exists())
        {
            file.mkdir();
        }
        if (file.isDirectory() && file.canWrite())
        {
            return new DiskLruCache(file, l);
        } else
        {
            return null;
        }
    }

    private void put(String s, String s1)
    {
        mLinkedHashMap.put(s, s1);
        cacheSize = mLinkedHashMap.size();
        cacheByteSize = (int)((long)cacheByteSize + (new File(s1)).length());
    }

    public void clearCache()
    {
        clearCache(mCacheDir);
    }

    public boolean containsKey(String s)
    {
        if (mLinkedHashMap.containsKey(s))
        {
            return true;
        }
        String s1 = createFilePath(mCacheDir, s);
        if ((new File(s1)).exists())
        {
            put(s, s1);
            return true;
        } else
        {
            return false;
        }
    }

    public String createFilePath(String s)
    {
        return createFilePath(mCacheDir, s);
    }

    public Bitmap get(String s)
    {
        Map map = mLinkedHashMap;
        map;
        JVM INSTR monitorenter ;
        String s1 = (String)mLinkedHashMap.get(s);
        if (s1 == null)
        {
            break MISSING_BLOCK_LABEL_34;
        }
        s = BitmapFactory.decodeFile(s1);
        map;
        JVM INSTR monitorexit ;
        return s;
        String s2 = createFilePath(mCacheDir, s);
        if (!(new File(s2)).exists())
        {
            break MISSING_BLOCK_LABEL_79;
        }
        put(s, s2);
        s = BitmapFactory.decodeFile(s2);
        return s;
        s;
        map;
        JVM INSTR monitorexit ;
        throw s;
        s;
        map;
        JVM INSTR monitorexit ;
        return null;
    }

    public void put(String s, Bitmap bitmap)
    {
        Map map = mLinkedHashMap;
        map;
        JVM INSTR monitorenter ;
        Object obj = mLinkedHashMap.get(s);
        if (obj != null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        try
        {
            String s1 = createFilePath(mCacheDir, s);
            if (writeBitmapToFile(bitmap, s1, s))
            {
                put(s, s1);
                flushCache();
            }
        }
        // Misplaced declaration of an exception variable
        catch (String s) { }
        // Misplaced declaration of an exception variable
        catch (String s) { }
        map;
        JVM INSTR monitorexit ;
        return;
        s;
        map;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void setCompressParams(android.graphics.Bitmap.CompressFormat compressformat, int i)
    {
        mCompressFormatJPG = compressformat;
        mCompressQuality = i;
    }

    public boolean writeBitmapToFile(Bitmap bitmap, String s, String s1)
        throws IOException, FileNotFoundException
    {
        s = new BufferedOutputStream(new FileOutputStream(s), 4096);
        if (!s1.contains(".png")) goto _L2; else goto _L1
_L1:
        boolean flag1 = bitmap.compress(mCompressFormatPNG, mCompressQuality, s);
        boolean flag;
        flag = flag1;
        if (s != null)
        {
            s.close();
            flag = flag1;
        }
_L4:
        return flag;
_L2:
        flag1 = bitmap.compress(mCompressFormatJPG, mCompressQuality, s);
        flag = flag1;
        if (s == null) goto _L4; else goto _L3
_L3:
        s.close();
        return flag1;
        bitmap;
        s = null;
_L6:
        if (s != null)
        {
            s.close();
        }
        throw bitmap;
        bitmap;
        if (true) goto _L6; else goto _L5
_L5:
    }

}
