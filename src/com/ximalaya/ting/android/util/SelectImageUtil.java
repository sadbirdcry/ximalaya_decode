// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.util:
//            as

public class SelectImageUtil
{

    public static final String CONTENT_TYPE = "image/*";
    public static final int GET_IMAGE_FROM_CAMERA = 161;
    public static final int GET_IMAGE_FROM_DCIM = 4066;
    public static final int PHOTO_CROP = 2803;
    private static MenuDialog mDialog;
    private static List sTitles;

    public SelectImageUtil()
    {
    }

    public static int cropImage(Activity activity, Uri uri, String s, int i, int j, int k, int l)
    {
        try
        {
            Intent intent = new Intent("com.android.camera.action.CROP");
            intent.setDataAndType(uri, "image/*");
            intent.putExtra("crop", "true");
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("outputX", 640);
            intent.putExtra("outputY", 640);
            intent.putExtra("scale", true);
            intent.putExtra("output", Uri.fromFile(new File(s)));
            intent.putExtra("return-data", false);
            intent.putExtra("outputFormat", android.graphics.Bitmap.CompressFormat.JPEG.toString());
            intent.putExtra("noFaceDetection", true);
            activity.startActivityForResult(intent, 2803);
        }
        // Misplaced declaration of an exception variable
        catch (Activity activity)
        {
            activity.printStackTrace();
            return -1;
        }
        return 0;
    }

    public static int getImageFromCamera(Activity activity, String s)
    {
        try
        {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            intent.putExtra("output", Uri.fromFile(new File(s)));
            activity.startActivityForResult(intent, 161);
        }
        // Misplaced declaration of an exception variable
        catch (Activity activity)
        {
            activity.printStackTrace();
            return -1;
        }
        return 0;
    }

    public static int getImageFromDICM(Activity activity)
    {
        try
        {
            Intent intent = new Intent("android.intent.action.PICK", null);
            intent.setDataAndType(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
            activity.startActivityForResult(intent, 4066);
        }
        // Misplaced declaration of an exception variable
        catch (Activity activity)
        {
            activity.printStackTrace();
            return 1;
        }
        return 0;
    }

    private static MenuDialog initDialog(Activity activity, String s)
    {
        if (sTitles == null)
        {
            sTitles = new ArrayList();
            sTitles.add("\u4ECE\u76F8\u518C\u9009\u62E9");
            sTitles.add("\u62CD\u7167");
        }
        s = new as(activity, s);
        mDialog = new MenuDialog(activity, sTitles);
        mDialog.setOnItemClickListener(s);
        return mDialog;
    }

    public static void showDialog(Activity activity, String s)
    {
        s = initDialog(activity, s);
        if (activity != null && !activity.isFinishing())
        {
            s.show();
        }
    }

}
