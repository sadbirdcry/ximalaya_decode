// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.ximalaya.ting.android.a;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.util:
//            a, b

public class BitmapUtils
{
    public static interface CompressCallback
    {

        public abstract void onFinished(Uri uri, boolean flag);
    }

    public static interface CompressCallback2
    {

        public abstract void onFinished(Map map, boolean flag);
    }


    private static final int IO_BUFFER_SIZE = 4096;
    private static android.graphics.Bitmap.CompressFormat mCompressFormatJPG;
    private static android.graphics.Bitmap.CompressFormat mCompressFormatPNG;
    private static int mCompressQuality = 70;

    public BitmapUtils()
    {
    }

    public static Drawable bitmap2drawable(Resources resources, Bitmap bitmap)
    {
        if (bitmap == null)
        {
            return null;
        } else
        {
            return new BitmapDrawable(resources, bitmap);
        }
    }

    public static Bitmap clone(Bitmap bitmap)
    {
        if (bitmap == null)
        {
            return null;
        } else
        {
            return bitmap.copy(bitmap.getConfig(), true);
        }
    }

    public static Drawable clone(Resources resources, Drawable drawable)
    {
        if (drawable == null)
        {
            return null;
        } else
        {
            return drawable.getConstantState().newDrawable(resources);
        }
    }

    public static void compressImage(Uri uri, boolean flag, CompressCallback compresscallback)
    {
        (new com.ximalaya.ting.android.util.a(uri, compresscallback, flag)).start();
    }

    public static void compressImages(List list, boolean flag, CompressCallback2 compresscallback2)
    {
        (new b(list, flag, compresscallback2)).start();
    }

    public static Bitmap drawable2bitmap(Drawable drawable)
    {
        if (drawable == null)
        {
            return null;
        }
        int i = drawable.getIntrinsicWidth();
        int j = drawable.getIntrinsicHeight();
        Object obj;
        Canvas canvas;
        if (drawable.getOpacity() != -1)
        {
            obj = android.graphics.Bitmap.Config.ARGB_8888;
        } else
        {
            obj = android.graphics.Bitmap.Config.RGB_565;
        }
        obj = Bitmap.createBitmap(i, j, ((android.graphics.Bitmap.Config) (obj)));
        canvas = new Canvas(((Bitmap) (obj)));
        drawable.setBounds(0, 0, i, j);
        drawable.draw(canvas);
        return ((Bitmap) (obj));
    }

    public static Bitmap extractBitmap(Bitmap bitmap, int i, int j)
    {
        int i1;
        int j1;
        while (bitmap == null || i == 0 && j == 0) 
        {
            return bitmap;
        }
        i1 = bitmap.getWidth();
        j1 = bitmap.getHeight();
        if (i != 0) goto _L2; else goto _L1
_L1:
        int k;
        int l;
        k = (int)((float)j * ((float)i1 / (float)j1));
        l = j;
_L4:
        float f = (float)k / (float)i1;
        float f1 = (float)l / (float)j1;
        Matrix matrix = new Matrix();
        matrix.postScale(f, f1);
        return Bitmap.createBitmap(bitmap, 0, 0, i1, j1, matrix, true);
_L2:
        k = i;
        l = j;
        if (j == 0)
        {
            l = (int)((float)i * ((float)j1 / (float)i1));
            k = i;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private static String genCopyFile(String s)
        throws IOException
    {
        int i = s.lastIndexOf(".");
        String s1 = s.substring(s.lastIndexOf(File.separator), i);
        Object obj = s.substring(i, s.length());
        s1 = (new StringBuilder()).append(a.af).append(s1).append("_copy").append(((String) (obj))).toString();
        obj = new File(s1);
        if (((File) (obj)).exists() || ((File) (obj)).createNewFile())
        {
            s = s1;
        }
        return s;
    }

    public static Bitmap getBitmap(Resources resources, int i, int j, int k)
    {
        if (resources == null || i == 0)
        {
            return null;
        } else
        {
            return getBitmap(resources.getDrawable(i), j, k);
        }
    }

    public static Bitmap getBitmap(Drawable drawable, int i, int j)
    {
        int k;
        int l;
        int i1;
label0:
        {
            if (drawable == null)
            {
                return null;
            }
            drawable = ((BitmapDrawable)drawable).getBitmap();
            l = drawable.getWidth();
            i1 = drawable.getHeight();
            if (i >= 0)
            {
                k = i;
                i = j;
                if (j >= 0)
                {
                    break label0;
                }
            }
            i = i1;
            j = l;
            k = j;
        }
        float f = (float)i / (float)i1;
        float f1 = (float)k / (float)l;
        Matrix matrix = new Matrix();
        matrix.postScale(f, f1);
        return Bitmap.createBitmap(drawable, 0, 0, l, i1, matrix, true);
    }

    public static byte[] getBitmapByte(Bitmap bitmap)
    {
        if (bitmap == null)
        {
            return null;
        }
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
        bitmap.compress(android.graphics.Bitmap.CompressFormat.PNG, 100, bytearrayoutputstream);
        try
        {
            bytearrayoutputstream.flush();
            bytearrayoutputstream.close();
        }
        // Misplaced declaration of an exception variable
        catch (Bitmap bitmap)
        {
            bitmap.printStackTrace();
        }
        return bytearrayoutputstream.toByteArray();
    }

    public static byte[] getBitmapBytes(Resources resources, int i)
    {
        return getBitmapByte(BitmapFactory.decodeResource(resources, i));
    }

    public static Bitmap getBitmapFromByte(byte abyte0[])
    {
        if (abyte0 != null)
        {
            return BitmapFactory.decodeByteArray(abyte0, 0, abyte0.length);
        } else
        {
            return null;
        }
    }

    public static Bitmap getRoundCornerBitmap(Bitmap bitmap, float f)
    {
        if (bitmap == null)
        {
            return null;
        } else
        {
            int i = bitmap.getWidth();
            int j = bitmap.getHeight();
            Bitmap bitmap1 = Bitmap.createBitmap(i, j, android.graphics.Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap1);
            Paint paint = new Paint();
            Rect rect = new Rect(0, 0, i, j);
            RectF rectf = new RectF(rect);
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            canvas.drawRoundRect(rectf, f, f, paint);
            paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            return bitmap1;
        }
    }

    public static boolean writeBitmapToFile(Bitmap bitmap, String s, String s1)
        throws IOException, FileNotFoundException
    {
        s = new BufferedOutputStream(new FileOutputStream(s), 4096);
        if (!s1.contains(".png")) goto _L2; else goto _L1
_L1:
        boolean flag1 = bitmap.compress(mCompressFormatPNG, mCompressQuality, s);
        boolean flag = flag1;
        if (s == null) goto _L4; else goto _L3
_L3:
        flag = flag1;
_L7:
        s.close();
_L4:
        return flag;
_L2:
        flag1 = bitmap.compress(mCompressFormatJPG, mCompressQuality, s);
        flag = flag1;
        if (s == null) goto _L4; else goto _L5
_L5:
        flag = flag1;
        if (true) goto _L7; else goto _L6
_L6:
        bitmap;
        s = null;
_L9:
        if (s != null)
        {
            s.close();
        }
        throw bitmap;
        bitmap;
        if (true) goto _L9; else goto _L8
_L8:
    }

    static 
    {
        mCompressFormatJPG = android.graphics.Bitmap.CompressFormat.JPEG;
        mCompressFormatPNG = android.graphics.Bitmap.CompressFormat.PNG;
    }

}
