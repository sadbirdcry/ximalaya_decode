// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;

public class PlanTerminateUtil
{

    public PlanTerminateUtil()
    {
    }

    public static void clearAutoFinishData(Context context)
    {
        context = SharedPreferencesUtil.getInstance(context);
        context.saveBoolean("isOnForPlan", false);
        context.saveInt("delay_minutes_index", -1);
        context.saveLong("plan_play_stop_time", 0L);
    }

    public static boolean isStopPlayWhenCurrSoundFinish(Context context)
    {
        context = SharedPreferencesUtil.getInstance(context);
        boolean flag = context.getBoolean("isOnForPlan", false);
        int i = context.getInt("delay_minutes_index", -1);
        return flag && i == 1;
    }
}
