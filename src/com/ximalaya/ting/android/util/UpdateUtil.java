// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.check_version.CheckVersionResult;
import com.ximalaya.ting.android.model.check_version.MsgCarry;

// Referenced classes of package com.ximalaya.ting.android.util:
//            be, bf, MyAsyncTask, Utilities

public class UpdateUtil
{
    private class a extends MyAsyncTask
    {

        ProgressDialog a;
        final UpdateUtil b;

        protected transient CheckVersionResult a(Void avoid[])
        {
            avoid = e.a;
            avoid = f.a().a(avoid, null, null, null);
            if (avoid == null)
            {
                break MISSING_BLOCK_LABEL_45;
            }
            int i;
            avoid = (CheckVersionResult)JSON.parseObject(avoid, com/ximalaya/ting/android/model/check_version/CheckVersionResult);
            i = ((CheckVersionResult) (avoid)).ret;
            if (i == 0)
            {
                return avoid;
            }
            break MISSING_BLOCK_LABEL_45;
            avoid;
            avoid.printStackTrace();
            return null;
        }

        protected void a(CheckVersionResult checkversionresult)
        {
            if (b.mActivity == null)
            {
                return;
            }
            if (a != null && a.isShowing())
            {
                a.cancel();
            }
            Message message = b.mUiHandler.obtainMessage();
            if (checkversionresult != null && !Utilities.isBlank(checkversionresult.version))
            {
                if (checkversionresult.forceUpdate)
                {
                    message.what = 1;
                } else
                {
                    message.what = 0;
                }
                message.obj = new MsgCarry(checkversionresult.version, checkversionresult.download, 0);
            } else
            {
                message.what = 163;
            }
            message.sendToTarget();
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((CheckVersionResult)obj);
        }

        protected void onPreExecute()
        {
            if (b.mBlock)
            {
                a = new MyProgressDialog(b.mActivity);
                a.setTitle("\u5347\u7EA7");
                a.setMessage("\u6B63\u5728\u8054\u7F51\u68C0\u6D4B\u7248\u672C\uFF0C\u8BF7\u7A0D\u5019...");
                a.setCancelable(false);
                a.show();
            }
        }

        private a()
        {
            b = UpdateUtil.this;
            super();
        }

        a(be be1)
        {
            this();
        }
    }

    private class b extends Handler
    {

        final UpdateUtil a;

        public void handleMessage(Message message)
        {
            MsgCarry msgcarry = (MsgCarry)message.obj;
            switch (message.what)
            {
            default:
                return;

            case 0: // '\0'
                a.askVersionUpdate(msgcarry.getServerVersion(), msgcarry.getDownloadUrl(), false);
                return;

            case 1: // '\001'
                a.askVersionUpdate(msgcarry.getServerVersion(), msgcarry.getDownloadUrl(), true);
                return;

            case 163: 
                Toast.makeText(a.mActivity, "\u5F53\u524D\u5DF2\u662F\u6700\u65B0\u7248\u672C", 1).show();
                break;
            }
        }

        public b(Looper looper)
        {
            a = UpdateUtil.this;
            super(looper);
        }
    }


    public static final int FORCE_UPDATE = 2;
    public static final int NORMAL_UPDATE = 1;
    public static final int NO_UPDATE = 3;
    private static byte sLock[] = new byte[0];
    private static UpdateUtil sUtil = null;
    private Context mActivity;
    private boolean mBlock;
    private a mCheckUpdateTask;
    private b mUiHandler;

    private UpdateUtil()
    {
    }

    private void askVersionUpdate(String s, String s1, boolean flag)
    {
        String s2 = getApkName(s);
        DialogBuilder dialogbuilder = new DialogBuilder(mActivity);
        dialogbuilder.setTitle("\u8F6F\u4EF6\u5347\u7EA7");
        if (!flag)
        {
            dialogbuilder.setMessage((new StringBuilder()).append("\u559C\u9A6C\u62C9\u96C5").append(s).append("\u7248\u5DF2\u7ECF\u53D1\u5E03,\u5FEB\u53BB\u4F53\u9A8C\u65B0\u529F\u80FD\u5427.").toString());
        } else
        {
            dialogbuilder.setMessage((new StringBuilder()).append("\u559C\u9A6C\u62C9\u96C5").append(s).append("\u7248\u5DF2\u6709\u8F83\u5927\u6539\u53D8,\n\u4E3A\u907F\u514D\u5F71\u54CD\u60A8\u7684\u4F7F\u7528,\u8BF7\u70B9\u51FB\u5347\u7EA7\u65B0\u7248\u6309\u94AE\u5F00\u59CB\u5347\u7EA7!").toString());
            dialogbuilder.setCancelable(false);
        }
        dialogbuilder.setOkBtn("\u5347\u7EA7\u65B0\u7248", new be(this, s2, s1));
        if (!flag)
        {
            dialogbuilder.setCancelBtn("\u4E0B\u6B21\u518D\u8BF4", new bf(this));
        }
        dialogbuilder.showConfirm();
    }

    private String getApkName(String s)
    {
        return (new StringBuilder()).append(mActivity.getString(0x7f09009e)).append(s).toString();
    }

    public static UpdateUtil getInstance()
    {
        if (sUtil == null)
        {
            synchronized (sLock)
            {
                if (sUtil == null)
                {
                    sUtil = new UpdateUtil();
                }
            }
        }
        return sUtil;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private void release()
    {
        mUiHandler.removeCallbacksAndMessages(null);
        mUiHandler = null;
        mActivity = null;
    }

    public void update(Context context, boolean flag)
    {
        if (mCheckUpdateTask == null || mCheckUpdateTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
        {
            mActivity = context;
            mUiHandler = new b(mActivity.getMainLooper());
            mCheckUpdateTask = new a(null);
            mCheckUpdateTask.myexec(new Void[0]);
            return;
        } else
        {
            Toast.makeText(mActivity, "\u6B63\u5728\u8054\u7F51\u5347\u7EA7\uFF0C\u8BF7\u7A0D\u5019\uFF01", 1).show();
            return;
        }
    }






}
