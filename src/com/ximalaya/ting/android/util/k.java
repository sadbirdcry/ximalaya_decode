// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.xdcs.CdnCollectData;
import com.ximalaya.ting.android.model.xdcs.CdnEvent;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.entity.StringEntity;

// Referenced classes of package com.ximalaya.ting.android.util:
//            DataCollectUtil

class k
    implements Runnable
{

    final CdnCollectData a;
    final DataCollectUtil b;

    k(DataCollectUtil datacollectutil, CdnCollectData cdncollectdata)
    {
        b = datacollectutil;
        a = cdncollectdata;
        super();
    }

    public void run()
    {
        try
        {
            Object obj1 = new CdnEvent();
            ((CdnEvent) (obj1)).setType("CDN");
            ((CdnEvent) (obj1)).setProps(a);
            ((CdnEvent) (obj1)).setTs(System.currentTimeMillis());
            Object obj = new ArrayList();
            ((List) (obj)).add(obj1);
            obj1 = new DataCollectUtil.EventRecord();
            obj1.events = ((List) (obj));
            obj = JSON.toJSONString(obj1);
            MyLogger.getLogger().d((new StringBuilder()).append("CdnCollectData=").append(((String) (obj))).toString());
            obj = new StringEntity(((String) (obj)), "UTF-8");
            obj = f.a().a(DataCollectUtil.access$100(b), e.an, null, ((org.apache.http.HttpEntity) (obj)), "application/json");
            MyLogger.getLogger().d((new StringBuilder()).append("cdn response=").append(((com.ximalaya.ting.android.b.n.a) (obj)).a).toString());
            return;
        }
        catch (Exception exception)
        {
            MyLogger.getLogger().d((new StringBuilder()).append("Cdn exception=").append(exception.getMessage()).toString());
        }
    }
}
