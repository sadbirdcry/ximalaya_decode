// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;
import com.ximalaya.ting.android.MyApplication;

public class CustomToast
{
    private static class a
        implements Runnable
    {

        private Context a;
        private String b;
        private int c;

        public void run()
        {
            if (a != null)
            {
                Toast.makeText(a, b, c).show();
            }
        }

        public a(Context context, String s, int i)
        {
            a = context;
            b = s;
            if (i == 0)
            {
                c = 0;
                return;
            } else
            {
                c = 1;
                return;
            }
        }
    }


    public static final int SHOW_TIME_LONG = 2000;
    public static final int SHOW_TIME_NORMAL = 1000;
    public static final int SHOW_TIME_SHORT = 200;
    private static Handler mHandler = new Handler(Looper.getMainLooper());

    public CustomToast()
    {
    }

    public static void showToast(Context context, int i, int j)
    {
        showToast(context, context.getResources().getString(i), j);
    }

    public static void showToast(Context context, String s, int i)
    {
        Context context1 = context;
        if (context == null)
        {
            context1 = MyApplication.b();
        }
        if (context1 == null)
        {
            return;
        } else
        {
            mHandler.post(new a(context1, s, i));
            return;
        }
    }

}
