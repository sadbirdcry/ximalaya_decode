// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.content.SharedPreferences;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.MyApplication;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class SharedPreferencesUserUtil
{

    private static SharedPreferencesUserUtil instance;
    private static String user;
    private SharedPreferences settings;

    private SharedPreferencesUserUtil(Context context)
    {
        settings = context.getSharedPreferences("user_data", 0);
    }

    public static SharedPreferencesUserUtil getInstance(Context context, String s)
    {
        user = s;
        s = context;
        if (context == null)
        {
            s = MyApplication.b();
        }
        if (instance != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/util/SharedPreferencesUserUtil;
        JVM INSTR monitorenter ;
        instance = new SharedPreferencesUserUtil(s.getApplicationContext());
        com/ximalaya/ting/android/util/SharedPreferencesUserUtil;
        JVM INSTR monitorexit ;
_L2:
        return instance;
        context;
        com/ximalaya/ting/android/util/SharedPreferencesUserUtil;
        JVM INSTR monitorexit ;
        throw context;
    }

    public ArrayList getArrayList(String s)
    {
        ArrayList arraylist = new ArrayList();
        SharedPreferences sharedpreferences = settings;
        sharedpreferences;
        JVM INSTR monitorenter ;
        s = settings.getString((new StringBuilder()).append(s).append(user).toString(), "{}");
        s = new JSONArray(s);
        if (s == null) goto _L2; else goto _L1
_L1:
        int i = 0;
_L3:
        if (i >= s.length())
        {
            break; /* Loop/switch isn't completed */
        }
        arraylist.add(s.optString(i));
        i++;
        if (true) goto _L3; else goto _L2
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        return arraylist;
_L2:
        sharedpreferences;
        JVM INSTR monitorexit ;
        return arraylist;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        throw s;
    }

    public boolean getBoolean(String s)
    {
        boolean flag;
        synchronized (settings)
        {
            flag = settings.getBoolean((new StringBuilder()).append(s).append(user).toString(), false);
        }
        return flag;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        throw s;
    }

    public boolean getBoolean(String s, boolean flag)
    {
        synchronized (settings)
        {
            flag = settings.getBoolean((new StringBuilder()).append(s).append(user).toString(), flag);
        }
        return flag;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        throw s;
    }

    public Double getDouble(String s)
    {
        SharedPreferences sharedpreferences = settings;
        sharedpreferences;
        JVM INSTR monitorenter ;
        s = settings.getString((new StringBuilder()).append(s).append(user).toString(), null);
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        double d = Double.parseDouble(s);
        sharedpreferences;
        JVM INSTR monitorexit ;
        return Double.valueOf(d);
        sharedpreferences;
        JVM INSTR monitorexit ;
        return null;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        throw s;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        return null;
    }

    public HashMap getHashMapByKey(String s)
    {
        HashMap hashmap = new HashMap();
        SharedPreferences sharedpreferences = settings;
        sharedpreferences;
        JVM INSTR monitorenter ;
        s = settings.getString((new StringBuilder()).append(s).append(user).toString(), "{}");
        s = new JSONObject(s);
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_114;
        }
        String s1;
        for (Iterator iterator = s.keys(); iterator.hasNext(); hashmap.put(s1, s.optString(s1)))
        {
            s1 = (String)iterator.next();
        }

        break MISSING_BLOCK_LABEL_114;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        throw s;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        return hashmap;
        sharedpreferences;
        JVM INSTR monitorexit ;
        return hashmap;
    }

    public int getInt(String s, int i)
    {
        synchronized (settings)
        {
            i = settings.getInt((new StringBuilder()).append(s).append(user).toString(), i);
        }
        return i;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        throw s;
    }

    public Boolean getOptBoolean(String s)
    {
        Object obj = null;
        SharedPreferences sharedpreferences = settings;
        sharedpreferences;
        JVM INSTR monitorenter ;
        s = settings.getString((new StringBuilder()).append(s).append(user).toString(), null);
        boolean flag = Boolean.parseBoolean(s);
        s = Boolean.valueOf(flag);
_L2:
        sharedpreferences;
        JVM INSTR monitorexit ;
        return s;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        throw s;
        s;
        s = obj;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public Double getOptDouble(String s)
    {
        Object obj = null;
        SharedPreferences sharedpreferences = settings;
        sharedpreferences;
        JVM INSTR monitorenter ;
        s = settings.getString((new StringBuilder()).append(s).append(user).toString(), null);
        double d = Double.parseDouble(s);
        s = Double.valueOf(d);
_L2:
        sharedpreferences;
        JVM INSTR monitorexit ;
        return s;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        throw s;
        s;
        s = obj;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public String getString(String s)
    {
        synchronized (settings)
        {
            s = settings.getString((new StringBuilder()).append(s).append(user).toString(), "");
        }
        return s;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void removeByKey(String s)
    {
        settings.edit().remove((new StringBuilder()).append(s).append(user).toString()).commit();
    }

    public void saveArrayList(String s, ArrayList arraylist)
    {
        synchronized (settings)
        {
            settings.edit().putString((new StringBuilder()).append(s).append(user).toString(), JSON.toJSONString(arraylist)).commit();
        }
        return;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void saveBoolean(String s, boolean flag)
    {
        synchronized (settings)
        {
            settings.edit().putBoolean((new StringBuilder()).append(s).append(user).toString(), flag).commit();
        }
        return;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void saveHashMap(String s, HashMap hashmap)
    {
        JSONObject jsonobject = new JSONObject(hashmap);
        synchronized (settings)
        {
            settings.edit().putString((new StringBuilder()).append(s).append(user).toString(), jsonobject.toString()).commit();
        }
        return;
        s;
        hashmap;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void saveInt(String s, int i)
    {
        synchronized (settings)
        {
            settings.edit().putInt((new StringBuilder()).append(s).append(user).toString(), i).commit();
        }
        return;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void saveString(String s, String s1)
    {
        synchronized (settings)
        {
            settings.edit().putString((new StringBuilder()).append(s).append(user).toString(), s1).commit();
        }
        return;
        s;
        sharedpreferences;
        JVM INSTR monitorexit ;
        throw s;
    }
}
