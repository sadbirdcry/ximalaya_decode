// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.SyncHttpClient;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.login.WelcomeActivity;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.FreeFlowUtilBase;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.free_flow.PhoneUserModel;
import com.ximalaya.ting.android.player.XMediaplayerImpl;
import com.ximalaya.ting.android.service.play.TingMediaPlayer;
import com.ximalaya.ting.android.transaction.a.b;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import sun.misc.BASE64Encoder;

// Referenced classes of package com.ximalaya.ting.android.util:
//            DESUtil, Logger, NetworkUtils, ToolUtil, 
//            z, y, s, x, 
//            p, r, q, t, 
//            ImageManager2, v, w, o, 
//            u, MyAsyncTask, Utilities

public class FreeFlowUtil extends FreeFlowUtilBase
{
    public static interface FlowProxyListener
    {

        public abstract void stopHandleFreeFlow();
    }

    public static interface InformFreeFlowListener
    {

        public abstract void normalOperation();
    }

    class a extends MyAsyncTask
    {

        final FreeFlowUtil a;
        private volatile int b;

        public transient String a(Integer ainteger[])
        {
            b = ainteger[0].intValue();
            if (b > 3)
            {
                return null;
            } else
            {
                ainteger = a.getPhoneUserInfo();
                Logger.log((new StringBuilder()).append("FreeFlowUtil getPhoneUserInfo response = ").append(ainteger).toString());
                Logger.logToSd((new StringBuilder()).append("FreeFlowUtil getPhoneUserInfo response = ").append(ainteger).toString());
                return ainteger;
            }
        }

        public void a(String s1)
        {
            if (!Utilities.isNotEmpty(s1))
            {
                break MISSING_BLOCK_LABEL_297;
            }
            if (s1.equals("timeout"))
            {
                (a. new a()).myexec(new Integer[] {
                    Integer.valueOf(b + 1)
                });
                return;
            }
            String s2;
            try
            {
                s1 = (PhoneUserModel)JSON.parseObject(s1, com/ximalaya/ting/android/model/free_flow/PhoneUserModel);
            }
            // Misplaced declaration of an exception variable
            catch (String s1)
            {
                Logger.log((new StringBuilder()).append("FreeFlowUtil GetPhoneUserInfoFromUnicomServer exception: ").append(s1.getMessage()).toString());
                Logger.logToSd((new StringBuilder()).append("FreeFlowUtil GetPhoneUserInfoFromUnicomServer exception: ").append(s1.getMessage()).toString());
                a.stopHandleFreeFlow();
                return;
            }
            if (s1 == null)
            {
                break MISSING_BLOCK_LABEL_265;
            }
            if (!"000000".equals(s1.getReturnCode()))
            {
                break MISSING_BLOCK_LABEL_265;
            }
            Logger.log((new StringBuilder()).append("FreeFlowUtil saved number = ").append(s1.getMobile()).toString());
            Logger.logToSd((new StringBuilder()).append("FreeFlowUtil saved number = ").append(s1.getMobile()).toString());
            s2 = ((TelephonyManager)FreeFlowUtilBase.mContext.getSystemService("phone")).getSubscriberId();
            if (TextUtils.isEmpty(s1.getImsi()) || s1.getImsi() != null && s1.getImsi().equals(s2))
            {
                a.savePhoneNumber(s1.getMobile());
                a.saveIMSI(s2);
                a.requestUserOrderStatus(s1.getMobile());
                return;
            }
            a.stopHandleFreeFlow();
            return;
            (a. new a()).myexec(new Integer[] {
                Integer.valueOf(b + 1)
            });
            return;
            s1 = new Date();
            s1 = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss aa")).format(s1);
            Logger.log((new StringBuilder()).append("FreeFlowUtil ").append(s1).append(" \u51FA\u73B0\u53D6\u53F7\u5931\u8D25\u73B0\u8C61").toString());
            Logger.logToSd((new StringBuilder()).append("FreeFlowUtil ").append(s1).append(" \u51FA\u73B0\u53D6\u53F7\u5931\u8D25\u73B0\u8C61").toString());
            a.stopHandleFreeFlow();
            return;
        }

        public Object doInBackground(Object aobj[])
        {
            return a((Integer[])aobj);
        }

        public void onPostExecute(Object obj)
        {
            a((String)obj);
        }

        a()
        {
            a = FreeFlowUtil.this;
            super();
        }
    }

    class b extends MyAsyncTask
    {

        final FreeFlowUtil a;

        public transient String a(Void avoid[])
        {
            avoid = a.getSavedPhoneNumber();
            avoid = a.getProxyServerInfo(avoid);
            Logger.log((new StringBuilder()).append("ProxyServer info = ").append(avoid).toString());
            return avoid;
        }

        public void a(String s1)
        {
            if (Utilities.isNotEmpty(s1) && !s1.equals("timeout"))
            {
                try
                {
                    Logger.log((new StringBuilder()).append("FreeFlowUtil ProxyServerModel =").append(s1).toString());
                    a.saveProxyServer(s1);
                }
                // Misplaced declaration of an exception variable
                catch (String s1) { }
            }
            a.initProxyServer();
            a.setProxy();
            a.stopHandleFreeFlow();
        }

        public Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        public void onPostExecute(Object obj)
        {
            a((String)obj);
        }

        b()
        {
            a = FreeFlowUtil.this;
            super();
        }
    }


    private static final String CHECK_ORDER_STATUS_URL = "http://pv.p10155.cn/Xpro/checkVAC.aspx?";
    private static int CheckOrderStatusTimes = 0;
    private static final String GET_PHONE_NUMBER_URL = "http://pv.p10155.cn/Xpro/getuserinfo.aspx?";
    private static final String GET_PROXY_SERVER_INFO_URL = "http://test2.p10155.cn:8075/Xpro/checkProxyUser.aspx?";
    private static final String NEVER_INFORM = "new_never_inform";
    public static final int NO_ORDER_STATUS = 0;
    public static final int ORDERED_STATUS = 1;
    public static final int ORDER_PAGE = 9527;
    public static final String ORDER_PAGE_URL = "http://pv.p10155.cn/Index.aspx?";
    private static final String ORDER_STATUS = "order_status";
    private static final String PHONE_IMSI = "phone_imsi";
    private static final String PHONE_NUMBER = "phone_number";
    private static final String PROXY_SERVER = "proxy_server";
    public static final int REORDER_STATUS = 2;
    private static final String UNICOM_URL = "http://pv.p10155.cn";
    public static final int UNKNOWN_STATUS = -1;
    private static final String UPDATE_ORDER_STATUS = "update_order_status";
    private static FreeFlowUtil mInstance;
    public static String unicomShow = "true";
    private String key;
    private FlowProxyListener listener;
    private MyApplication mMyApplication;
    private int mOrderStatus;
    private int usedProxyType;

    private FreeFlowUtil()
    {
        key = "p10155cn";
        usedProxyType = 0;
        listener = null;
        mOrderStatus = -1;
    }

    public static FreeFlowUtil getInstance()
    {
        if (mInstance != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/util/FreeFlowUtil;
        JVM INSTR monitorenter ;
        if (mInstance == null)
        {
            mInstance = new FreeFlowUtil();
        }
        com/ximalaya/ting/android/util/FreeFlowUtil;
        JVM INSTR monitorexit ;
_L2:
        return mInstance;
        Exception exception;
        exception;
        com/ximalaya/ting/android/util/FreeFlowUtil;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private String getPhoneUserInfo()
    {
        String s1;
        String s2;
        s2 = (new StringBuilder()).append("key=").append(key).append("&timestamp=").append(getTimeStamp()).toString();
        s1 = "http://pv.p10155.cn/Xpro/getuserinfo.aspx?";
        s2 = (new StringBuilder()).append("http://pv.p10155.cn/Xpro/getuserinfo.aspx?").append("phonenum=").append(DESUtil.desEncrypt(s2, key)).toString();
        s1 = s2;
_L2:
        Logger.log((new StringBuilder()).append("FreeFlowUtil getPhoneUserInfo url=").append(s1).toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil getPhoneUserInfo url=").append(s1).toString());
        return f.a().a(s1, null, null, null);
        Exception exception;
        exception;
        Logger.log((new StringBuilder()).append("FreeFlowUtil getPhoneUserInfo exception=").append(exception.getMessage()).toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil getPhoneUserInfo exception=").append(exception.getMessage()).toString());
        if (true) goto _L2; else goto _L1
_L1:
    }

    private String getProxyServerInfo(String s1)
    {
        String s2;
        s2 = (new StringBuilder()).append("key=").append(key).append("&timestamp=").append(getTimeStamp()).toString();
        s1 = "http://test2.p10155.cn:8075/Xpro/checkProxyUser.aspx?";
        s2 = (new StringBuilder()).append("http://test2.p10155.cn:8075/Xpro/checkProxyUser.aspx?").append("phonenum=").append(DESUtil.desEncrypt(s2, key)).toString();
        s1 = s2;
_L2:
        Logger.log((new StringBuilder()).append("FreeFlowUtil getProxyServerInfo url=").append(s1).toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil getProxyServerInfo url=").append(s1).toString());
        return f.a().a(s1, null, null, null);
        Exception exception;
        exception;
        Logger.log((new StringBuilder()).append("FreeFlowUtil getProxyServerInfo exception=").append(exception.getMessage()).toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil getProxyServerInfo exception=").append(exception.getMessage()).toString());
        if (true) goto _L2; else goto _L1
_L1:
    }

    private String getSavedProxyServer()
    {
        return SharedPreferencesUtil.getInstance(mContext).getString("proxy_server");
    }

    private String getSavedUpdateOrderStatusDate()
    {
        return SharedPreferencesUtil.getInstance(mContext).getString("update_order_status");
    }

    private String getTimeStamp()
    {
        Date date = new Date();
        return (new SimpleDateFormat("yyyyMMddHHmmss")).format(date);
    }

    private String getUserOrderStatus(String s1)
    {
        String s2;
        CheckOrderStatusTimes++;
        s2 = (new StringBuilder()).append("phonenum=").append(s1).append("&key=").append(key).append("&timestamp=").append(getTimeStamp()).toString();
        s1 = "http://pv.p10155.cn/Xpro/checkVAC.aspx?";
        Logger.log((new StringBuilder()).append("FreeFlowUtil getUserOrderStatus temp=").append(s2).toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil getUserOrderStatus temp=").append(s2).toString());
        s2 = (new StringBuilder()).append("http://pv.p10155.cn/Xpro/checkVAC.aspx?").append("phonenum=").append(DESUtil.desEncrypt(s2, key)).toString();
        s1 = s2;
_L2:
        Logger.log((new StringBuilder()).append("FreeFlowUtil getUserOrderStatus url=").append(s1).toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil getUserOrderStatus url=").append(s1).toString());
        return f.a().a(s1, null, null, null);
        Exception exception;
        exception;
        Logger.log((new StringBuilder()).append("FreeFlowUtil getUserOrderStatus exception=").append(exception.getMessage()).toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil getUserOrderStatus exception=").append(exception.getMessage()).toString());
        if (true) goto _L2; else goto _L1
_L1:
    }

    private void gotoOrderPage(boolean flag)
    {
        Activity activity = MyApplication.a();
        if (activity == null || activity.isFinishing())
        {
            return;
        }
        String s1 = getSavedPhoneNumber();
        Object obj;
        String s2;
        int i;
        if (!TextUtils.isEmpty(s1))
        {
            s1 = (new StringBuilder()).append("http://pv.p10155.cn/Index.aspx?").append("phonenum=").append(s1).append("&").toString();
        } else
        {
            s1 = "http://pv.p10155.cn/Index.aspx?";
        }
        s2 = ((TelephonyManager)mContext.getSystemService("phone")).getSubscriberId();
        obj = s1;
        if (!TextUtils.isEmpty(s2))
        {
            obj = (new StringBuilder()).append(s1).append("IMSI=").append(s2).append("&").toString();
        }
        i = 0;
        if (NetworkUtils.getNetType(mContext) == 0)
        {
            i = 1;
        }
        s1 = (new StringBuilder()).append(((String) (obj))).append("networkType=").append(i).append("&").toString();
        obj = (new StringBuilder()).append(s1).append("extensions=").append(ToolUtil.getChannel(mContext)).append(",").append(mMyApplication.m()).append(",").append(mMyApplication.i()).toString();
        s1 = ((String) (obj));
        if (flag)
        {
            s1 = (new StringBuilder()).append(((String) (obj))).append("&ac=ad_ximaH1").toString();
        }
        Logger.log((new StringBuilder()).append("FreeFlowUtil orderpage url =").append(s1).toString());
        obj = new Intent(activity, com/ximalaya/ting/android/activity/web/WebActivityNew);
        ((Intent) (obj)).putExtra("ExtraUrl", s1);
        activity.startActivityForResult(((Intent) (obj)), 9527);
        removeUpdateOrderStatusDate();
    }

    private void informCheckOrderStatusFailure(String s1)
    {
label0:
        {
            Activity activity = MyApplication.a();
            Object obj = activity;
            if (activity == null)
            {
                if (WelcomeActivity.welcomeActivity == null || WelcomeActivity.welcomeActivity.isFinishing())
                {
                    break label0;
                }
                obj = WelcomeActivity.welcomeActivity;
            }
            (new DialogBuilder(((Context) (obj)))).setMessage("\u514D\u6D41\u91CF\u670D\u52A1\u8BF7\u6C42\u5931\u8D25\uFF0C\u662F\u5426\u91CD\u8BD5\uFF1F").setCancelable(false).setOkBtn("\u91CD\u8BD5", new z(this, s1)).setCancelBtn(new y(this)).showConfirm();
            return;
        }
        stopHandleFreeFlow();
    }

    private void initProxyServer()
    {
        Object obj = getSavedProxyServer();
        Logger.log((new StringBuilder()).append("FreeFlowUtil info=").append(((String) (obj))).toString());
        if (!TextUtils.isEmpty(((CharSequence) (obj))))
        {
            obj = JSON.parseObject(((String) (obj)));
            if (obj != null)
            {
                if (!TextUtils.isEmpty(((JSONObject) (obj)).getString("NetIP")))
                {
                    NET_PROXY_HOST = ((JSONObject) (obj)).getString("NetIP");
                }
                if (!TextUtils.isEmpty(((JSONObject) (obj)).getString("WapIP")))
                {
                    WAP_PROXY_HOST = ((JSONObject) (obj)).getString("WapIP");
                }
            }
        }
    }

    private boolean isUpdateOrderStatusToday()
    {
        Object obj = Calendar.getInstance();
        int i = ((Calendar) (obj)).get(1);
        int j = ((Calendar) (obj)).get(2);
        int k = ((Calendar) (obj)).get(5);
        obj = (new StringBuilder()).append("").append(i).append(j + 1).append(k).toString();
        String s1 = getSavedUpdateOrderStatusDate();
        Logger.log((new StringBuilder()).append("FreeFlowUtil currentDate = ").append(((String) (obj))).append(", savedDate=").append(s1).toString());
        return ((String) (obj)).equals(s1);
    }

    private void removeOrderStatus()
    {
        SharedPreferencesUtil.getInstance(mContext).removeByKey("order_status");
    }

    private void removeUpdateOrderStatusDate()
    {
        SharedPreferencesUtil.getInstance(mContext).removeByKey("update_order_status");
    }

    private void requestProxyServerInfo()
    {
        (new b()).myexec(new Void[0]);
    }

    private void requestUserOrderStatus(String s1)
    {
        if (!isUpdateOrderStatusToday())
        {
            verifyUserOrderStatus(s1);
        }
    }

    private void saveProxyServer(String s1)
    {
        if (TextUtils.isEmpty(s1))
        {
            return;
        } else
        {
            SharedPreferencesUtil.getInstance(mContext).saveString("proxy_server", s1);
            return;
        }
    }

    private void saveUpdateOrderStatusDate()
    {
        Object obj = Calendar.getInstance();
        int i = ((Calendar) (obj)).get(1);
        int j = ((Calendar) (obj)).get(2);
        int k = ((Calendar) (obj)).get(5);
        obj = (new StringBuilder()).append("").append(i).append(j + 1).append(k).toString();
        SharedPreferencesUtil.getInstance(mContext).saveString("update_order_status", ((String) (obj)));
    }

    private void setNetProxyForAsyncHttpClient()
    {
        f.a().b().setProxy(NET_PROXY_HOST, 8080);
        f.a().c().setProxy(NET_PROXY_HOST, 8080);
    }

    private void setNetProxyForHttpClient()
    {
        HttpClient httpclient = mMyApplication.j();
        HttpHost httphost = new HttpHost(NET_PROXY_HOST, 8080);
        httpclient.getParams().setParameter("http.route.default-proxy", httphost);
    }

    public static void setProxyForHttpClient(HttpClient httpclient)
    {
        if (!getInstance().isNeedFreeFlowProxy())
        {
            return;
        }
        String s1;
        if (getNetType() == 2)
        {
            HttpHost httphost = new HttpHost(NET_PROXY_HOST, 8080);
            httpclient.getParams().setParameter("http.route.default-proxy", httphost);
        } else
        {
            HttpHost httphost1 = new HttpHost(WAP_PROXY_HOST, 8080);
            httpclient.getParams().setParameter("http.route.default-proxy", httphost1);
        }
        httpclient = (DefaultHttpClient)httpclient;
        s1 = (new StringBuilder()).append(APPKEY).append(":").append(KEY).toString();
        httpclient.addRequestInterceptor(new s((new BASE64Encoder()).encode(s1.getBytes())));
    }

    private void setWapProxyForAsyncHttpClient()
    {
        f.a().b().setProxy(WAP_PROXY_HOST, 8080);
        f.a().c().setProxy(WAP_PROXY_HOST, 8080);
    }

    private void setWapProxyForHttpClient()
    {
        HttpClient httpclient = mMyApplication.j();
        HttpHost httphost = new HttpHost(WAP_PROXY_HOST, 8080);
        httpclient.getParams().setParameter("http.route.default-proxy", httphost);
    }

    private void stopHandleFreeFlow()
    {
        Logger.log("FreeFlowUtil stopHandleFreeFlow");
        if (listener != null)
        {
            listener.stopHandleFreeFlow();
        }
    }

    public static boolean unicomFreeFlowSwitchOn()
    {
        return true;
    }

    private void updateOrderStatus(int i)
    {
        mOrderStatus = i;
        SharedPreferencesUtil.getInstance(mContext).saveInt("order_status", i);
        saveUpdateOrderStatusDate();
    }

    private void verifyUserOrderStatus(String s1)
    {
        (new x(this, s1)).myexec(new String[] {
            s1
        });
    }

    public void destroy()
    {
        Logger.log((new StringBuilder()).append("FreeFlowUtil \u672C\u6B21\u542F\u52A8\u5171\u8C03\u7528\u67E5\u8BE2\u63A5\u53E3").append(CheckOrderStatusTimes).append("\u6B21").toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil \u672C\u6B21\u542F\u52A8\u5171\u8C03\u7528\u67E5\u8BE2\u63A5\u53E3").append(CheckOrderStatusTimes).append("\u6B21").toString());
        CheckOrderStatusTimes = 0;
        isNeedFreeFlowProxy = false;
    }

    public int getOrderStatus()
    {
        return SharedPreferencesUtil.getInstance(mContext).getInt("order_status", -1);
    }

    public String getSavedIMSI()
    {
        return SharedPreferencesUtil.getInstance(mContext).getString("phone_imsi");
    }

    public String getSavedPhoneNumber()
    {
        return SharedPreferencesUtil.getInstance(mContext).getString("phone_number");
    }

    public void gotoOrderPage()
    {
        gotoOrderPage(false);
    }

    public void informFreeFlow(InformFreeFlowListener informfreeflowlistener, com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback dialogcallback, com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback dialogcallback1, com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback dialogcallback2)
    {
        Activity activity = MyApplication.a();
        if (activity == null || activity.isFinishing())
        {
            informfreeflowlistener.normalOperation();
            return;
        }
        if (!unicomFreeFlowSwitchOn())
        {
            informfreeflowlistener.normalOperation();
            return;
        }
        if (SharedPreferencesUtil.getInstance(mContext).getBoolean("new_never_inform") || NetworkUtils.getNetType(mContext) != 0 || !isChinaUnicomUser() || isNeedFreeFlowProxy())
        {
            informfreeflowlistener.normalOperation();
            return;
        } else
        {
            ToolUtil.onEvent(activity.getApplicationContext(), "NetworkSwitch_Ads");
            informfreeflowlistener = new SpannableString("\u5F53\u524D\u6CA1\u6709WiFi\uFF0C\u5141\u8BB8\u7528\u6D41\u91CF\u64AD\u653E\u6216\u4E0B\u8F7D\u5417\uFF1F\uFF08\u5EFA\u8BAE\u60A8\u5148\u5F00\u901A\u514D\u6D41\u91CF\u670D\u52A1\uFF09");
            p p1 = new p(this, activity);
            ForegroundColorSpan foregroundcolorspan = new ForegroundColorSpan(activity.getResources().getColor(0x7f070005));
            informfreeflowlistener.setSpan(p1, informfreeflowlistener.length() - 6, informfreeflowlistener.length() - 1, 17);
            informfreeflowlistener.setSpan(foregroundcolorspan, informfreeflowlistener.length() - 6, informfreeflowlistener.length() - 1, 17);
            (new DialogBuilder(activity, false)).setTitle("\u6D41\u91CF\u63D0\u9192").setMessage(informfreeflowlistener).setTextClickable().setOkBtn("\u603B\u662F\u5141\u8BB8", new r(this, activity, dialogcallback)).setNeutralBtn("\u5141\u8BB8\u672C\u6B21", dialogcallback1).setCancelBtn(new q(this, activity, dialogcallback2)).showMultiButton();
            return;
        }
    }

    public void init(Context context)
    {
        mContext = context;
        if (context != null)
        {
            mMyApplication = (MyApplication)context.getApplicationContext();
            return;
        } else
        {
            mMyApplication = (MyApplication)MyApplication.b();
            return;
        }
    }

    public boolean isChinaUnicomUser()
    {
        if (mContext == null)
        {
            return false;
        }
        Object obj = (TelephonyManager)mContext.getSystemService("phone");
        if (obj == null)
        {
            return false;
        }
        obj = ((TelephonyManager) (obj)).getSubscriberId();
        if (obj == null)
        {
            return false;
        }
        return !((String) (obj)).startsWith("46000") && !((String) (obj)).startsWith("46002") && !((String) (obj)).startsWith("46007") && !((String) (obj)).startsWith("46003") && !((String) (obj)).startsWith("46005");
    }

    public boolean isNeedFreeFlowProxy()
    {
        if (NetworkUtils.getNetType(mContext) == 0)
        {
            return isOrderFlowPackage();
        } else
        {
            return false;
        }
    }

    public boolean isOrderFlowPackage()
    {
        if (mOrderStatus == -1)
        {
            mOrderStatus = getOrderStatus();
        }
        return mOrderStatus == 1 || mOrderStatus == 2;
    }

    public void removeFlowProxyListener()
    {
        listener = null;
    }

    public void removeProxy()
    {
        if (!unicomFreeFlowSwitchOn())
        {
            return;
        }
        Logger.log("FreeFlowUtil removeProxy");
        Logger.logToSd("FreeFlowUtil removeProxy");
        isNeedFreeFlowProxy = false;
        usedProxyType = 0;
        Object obj = f.a().b();
        ((AsyncHttpClient) (obj)).removeHeader("Authorization");
        ((AsyncHttpClient) (obj)).getHttpClient().getParams().removeParameter("http.route.default-proxy");
        obj = f.a().c();
        ((SyncHttpClient) (obj)).removeHeader("Authorization");
        ((SyncHttpClient) (obj)).getHttpClient().getParams().removeParameter("http.route.default-proxy");
        if (mMyApplication != null)
        {
            HttpClient httpclient = mMyApplication.j();
            httpclient.getParams().removeParameter("http.route.default-proxy");
            ((DefaultHttpClient)httpclient).addRequestInterceptor(new t(this));
        }
        com.ximalaya.ting.android.transaction.a.b.a().c();
        ImageManager2.from(mContext).reSetForProxy(mContext);
        TingMediaPlayer.getTingMediaPlayer(mContext).removeProxy();
    }

    public void removeProxyForGlobalHttpClient()
    {
        HttpClient httpclient = mMyApplication.j();
        httpclient.getParams().removeParameter("http.route.default-proxy");
        ((DefaultHttpClient)httpclient).addRequestInterceptor(new v(this));
    }

    public void removeProxyForHttpClient(HttpClient httpclient)
    {
        httpclient.getParams().removeParameter("http.route.default-proxy");
        ((DefaultHttpClient)httpclient).addRequestInterceptor(new w(this));
    }

    public void removeSavedIMSI()
    {
        SharedPreferencesUtil.getInstance(mContext).removeByKey("phone_imsi");
    }

    public void removeSavedPhoneNumber()
    {
        SharedPreferencesUtil.getInstance(mContext).removeByKey("phone_number");
    }

    public void saveIMSI(String s1)
    {
        if (TextUtils.isEmpty(s1))
        {
            return;
        } else
        {
            SharedPreferencesUtil.getInstance(mContext).saveString("phone_imsi", s1);
            return;
        }
    }

    public void savePhoneNumber(String s1)
    {
        if (TextUtils.isEmpty(s1))
        {
            return;
        } else
        {
            SharedPreferencesUtil.getInstance(mContext).saveString("phone_number", s1);
            return;
        }
    }

    public void setFlowProxyListener(FlowProxyListener flowproxylistener)
    {
        listener = flowproxylistener;
    }

    public void setProxy()
    {
        Logger.log("FreeFlowUtil setProxy");
        Logger.logToSd("FreeFlowUtil setProxy");
        Object obj;
        if (getNetType() == 2)
        {
            usedProxyType = 2;
            setNetProxyForAsyncHttpClient();
            setNetProxyForHttpClient();
        } else
        {
            usedProxyType = 1;
            setWapProxyForAsyncHttpClient();
            setWapProxyForHttpClient();
        }
        obj = (new StringBuilder()).append(APPKEY).append(":").append(KEY).toString();
        obj = (new BASE64Encoder()).encode(((String) (obj)).getBytes());
        f.a().a("Authorization", (new StringBuilder()).append("Basic ").append(((String) (obj))).toString());
        ((DefaultHttpClient)mMyApplication.j()).addRequestInterceptor(new o(this, ((String) (obj))));
        isNeedFreeFlowProxy = true;
        com.ximalaya.ting.android.transaction.a.b.a().b();
        ImageManager2.from(mContext).reSetForProxy(mContext);
        TingMediaPlayer.getTingMediaPlayer(mContext).setProxy();
        obj = new Date();
        obj = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss aa")).format(((Date) (obj)));
        Logger.log((new StringBuilder()).append("FreeFlowUtil ").append(((String) (obj))).append(" \u8BBE\u7F6E\u4E86\u4EE3\u7406").toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil ").append(((String) (obj))).append(" \u8BBE\u7F6E\u4E86\u4EE3\u7406").toString());
    }

    public void setProxyForGlobalHttpClient()
    {
        Object obj = mMyApplication.j();
        String s1;
        if (getNetType() == 2)
        {
            HttpHost httphost = new HttpHost(NET_PROXY_HOST, 8080);
            ((HttpClient) (obj)).getParams().setParameter("http.route.default-proxy", httphost);
        } else
        {
            HttpHost httphost1 = new HttpHost(WAP_PROXY_HOST, 8080);
            ((HttpClient) (obj)).getParams().setParameter("http.route.default-proxy", httphost1);
        }
        obj = (DefaultHttpClient)obj;
        s1 = (new StringBuilder()).append(APPKEY).append(":").append(KEY).toString();
        ((DefaultHttpClient) (obj)).addRequestInterceptor(new u(this, (new BASE64Encoder()).encode(s1.getBytes())));
    }

    public void setProxyForXMediaPlayer(XMediaplayerImpl xmediaplayerimpl)
    {
        int i = getNetType();
        String s1 = (new StringBuilder()).append(APPKEY).append(":").append(KEY).toString();
        s1 = (new BASE64Encoder()).encode(s1.getBytes());
        if (i == 2)
        {
            xmediaplayerimpl.setProxy(NET_PROXY_HOST, 8080, (new StringBuilder()).append("Basic ").append(s1).toString());
            return;
        } else
        {
            xmediaplayerimpl.setProxy(WAP_PROXY_HOST, 8080, (new StringBuilder()).append("Basic ").append(s1).toString());
            return;
        }
    }

    public void useFreeFlow()
    {
        useFreeFlow(false);
    }

    public void useFreeFlow(boolean flag)
    {
        useFreeFlow(flag, 0);
    }

    public void useFreeFlow(boolean flag, int i)
    {
        if (!unicomFreeFlowSwitchOn())
        {
            stopHandleFreeFlow();
            return;
        }
        Logger.log((new StringBuilder()).append("FreeFlowUtil useFreeFlow isStart=").append(flag).append(", netChange=").append(i).toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil useFreeFlow isStart=").append(flag).append(", netChange=").append(i).toString());
        if (isNeedFreeFlowProxy && i > 0)
        {
            if (usedProxyType != 0 && getNetType() != usedProxyType)
            {
                Logger.log("FreeFlowUtil net or wap change");
                Logger.logToSd("FreeFlowUtil net or wap change");
                removeProxy();
                setProxy();
            }
            stopHandleFreeFlow();
            return;
        }
        if (!isChinaUnicomUser())
        {
            break MISSING_BLOCK_LABEL_311;
        }
        String s1 = getSavedPhoneNumber();
        String s3 = getSavedIMSI();
        if (!TextUtils.isEmpty(s1) && !s3.equals(((TelephonyManager)mContext.getSystemService("phone")).getSubscriberId()))
        {
            removeSavedPhoneNumber();
            removeSavedIMSI();
            removeOrderStatus();
            removeUpdateOrderStatusDate();
        }
        if (NetworkUtils.getNetType(mContext) != 0) goto _L2; else goto _L1
_L1:
        String s2;
        s2 = getSavedPhoneNumber();
        if (TextUtils.isEmpty(s2))
        {
            (new a()).myexec(new Integer[] {
                Integer.valueOf(1)
            });
            return;
        }
        if (!flag) goto _L4; else goto _L3
_L3:
        i = getOrderStatus();
        if (i != 1 && i != 2) goto _L6; else goto _L5
_L5:
        requestProxyServerInfo();
        stopHandleFreeFlow();
        Logger.log("FreeFlowUtil orderStatus 1");
        Logger.logToSd("FreeFlowUtil orderStatus 1");
_L4:
        requestUserOrderStatus(s2);
        return;
_L6:
        if (i == 0)
        {
            stopHandleFreeFlow();
            Logger.log("FreeFlowUtil orderStatus 2");
            Logger.logToSd("FreeFlowUtil orderStatus 2");
        }
        if (true) goto _L4; else goto _L2
_L2:
        stopHandleFreeFlow();
        return;
        stopHandleFreeFlow();
        return;
    }

    static 
    {
        CheckOrderStatusTimes = 0;
    }












}
