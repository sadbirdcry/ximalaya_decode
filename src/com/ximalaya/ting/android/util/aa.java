// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.net.Uri;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import java.io.IOException;
import java.net.HttpURLConnection;
import sun.misc.BASE64Encoder;

// Referenced classes of package com.ximalaya.ting.android.util:
//            FreeFlowUtil

final class aa extends OkHttpDownloader
{

    aa(OkHttpClient okhttpclient)
    {
        super(okhttpclient);
    }

    protected HttpURLConnection openConnection(Uri uri)
        throws IOException
    {
        if (FreeFlowUtil.getInstance().isNeedFreeFlowProxy())
        {
            uri = super.openConnection(uri);
            String s = (new StringBuilder()).append(FreeFlowUtil.APPKEY).append(":").append(FreeFlowUtil.KEY).toString();
            s = (new BASE64Encoder()).encode(s.getBytes());
            uri.setRequestProperty("Authorization", (new StringBuilder()).append("Basic ").append(s).toString());
            return uri;
        } else
        {
            return super.openConnection(uri);
        }
    }
}
