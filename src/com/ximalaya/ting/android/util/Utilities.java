// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.content.res.Resources;
import android.os.Environment;
import android.util.DisplayMetrics;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

// Referenced classes of package com.ximalaya.ting.android.util:
//            Logger

public class Utilities
{

    public Utilities()
    {
    }

    public static int dip2px(Context context, float f)
    {
        return (int)(context.getResources().getDisplayMetrics().density * f + 0.5F);
    }

    public static boolean isBlank(String s)
    {
        boolean flag1 = false;
        if (s == null) goto _L2; else goto _L1
_L1:
        int j = s.length();
        if (j != 0 && !s.equals("null")) goto _L3; else goto _L2
_L2:
        boolean flag = true;
_L5:
        return flag;
_L3:
        int i = 0;
label0:
        do
        {
label1:
            {
                if (i >= j)
                {
                    break label1;
                }
                flag = flag1;
                if (!Character.isWhitespace(s.charAt(i)))
                {
                    break label0;
                }
                i++;
            }
        } while (true);
        if (true) goto _L5; else goto _L4
_L4:
        return true;
    }

    public static boolean isEmpty(String s)
    {
        return s == null || s.length() == 0 || s.equals("null");
    }

    public static boolean isNotBlank(String s)
    {
        return !isBlank(s);
    }

    public static boolean isNotEmpty(String s)
    {
        return !isEmpty(s);
    }

    public static float pixels2Sp(Context context, float f)
    {
        return f / context.getResources().getDisplayMetrics().scaledDensity;
    }

    public static int px2dip(Context context, float f)
    {
        return (int)(f / context.getResources().getDisplayMetrics().density + 0.5F);
    }

    public static Map readXMLFileFromSdcard(String s, String s1)
    {
        Object obj = null;
        HashMap hashmap = new HashMap();
        if ("mounted".equals(Environment.getExternalStorageState()))
        {
            if (Environment.getExternalStorageDirectory().canWrite())
            {
                obj = new File(Environment.getExternalStorageDirectory().getPath());
                if (((File) (obj)).exists() && ((File) (obj)).canWrite())
                {
                    File file = new File((new StringBuilder()).append(((File) (obj)).getAbsolutePath()).append(s).toString());
                    Logger.w("test", (new StringBuilder()).append(((File) (obj)).getAbsolutePath()).append(s).toString());
                    file.mkdirs();
                    if (file.exists() && file.canWrite())
                    {
                        s = new File((new StringBuilder()).append(file.getAbsolutePath()).append("/").append(s1).toString());
                        s1 = DocumentBuilderFactory.newInstance();
                        try
                        {
                            s1 = s1.newDocumentBuilder();
                        }
                        // Misplaced declaration of an exception variable
                        catch (String s)
                        {
                            return null;
                        }
                        if (!s.exists())
                        {
                            hashmap.put("username", "");
                            hashmap.put("password", "");
                            hashmap.put("isRegisterFirst", "true");
                        } else
                        {
                            Object obj1;
                            try
                            {
                                s1 = s1.parse(s);
                            }
                            // Misplaced declaration of an exception variable
                            catch (String s)
                            {
                                Logger.log("readXMLFileFromSdcard", s.getMessage(), true);
                                return null;
                            }
                            // Misplaced declaration of an exception variable
                            catch (String s)
                            {
                                Logger.log("readXMLFileFromSdcard", s.getMessage(), true);
                                return null;
                            }
                            // Misplaced declaration of an exception variable
                            catch (String s)
                            {
                                Logger.log("readXMLFileFromSdcard", s.getMessage(), true);
                                return null;
                            }
                            obj1 = s1.getElementsByTagName("username").item(0);
                            s = s1.getElementsByTagName("password").item(0);
                            s1 = s1.getElementsByTagName("isRegisterFirst").item(0);
                            obj1 = ((Node) (obj1)).getFirstChild().getNodeValue();
                            s = s.getFirstChild().getNodeValue();
                            s1 = s1.getFirstChild().getNodeValue();
                            hashmap.put("username", obj1);
                            hashmap.put("password", s);
                            hashmap.put("isRegisterFirst", s1);
                        }
                    }
                }
            }
            obj = hashmap;
        }
        return ((Map) (obj));
    }
}
