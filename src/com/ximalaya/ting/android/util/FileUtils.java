// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.text.TextUtils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.Stack;
import org.apache.http.util.EncodingUtils;

// Referenced classes of package com.ximalaya.ting.android.util:
//            Logger

public class FileUtils
{

    private static final int DEF_BUFF_SIZE = 8192;
    private static final String ENCODE = "UTF-8";
    private static final String TAG = "FileUtils";

    public FileUtils()
    {
    }

    public static int copyAssetsToFile(AssetManager assetmanager, String s, String s1)
    {
        Object obj;
        Object obj1;
        obj1 = null;
        obj = null;
        assetmanager = assetmanager.open(s);
        s = new File(s1);
        if (!s.exists()) goto _L2; else goto _L1
_L1:
        long l = s.length();
        if (l <= 0L) goto _L2; else goto _L3
_L3:
        int i;
        if (false)
        {
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
        }
        if (assetmanager == null)
        {
            break MISSING_BLOCK_LABEL_60;
        }
        assetmanager.close();
_L9:
        return 0;
_L2:
        s = new FileOutputStream(s);
        s1 = new byte[8192];
_L7:
        i = assetmanager.read(s1);
        if (i <= 0) goto _L5; else goto _L4
_L4:
        if (i <= 0) goto _L7; else goto _L6
_L6:
        s.write(s1, 0, i);
          goto _L7
        obj;
        s1 = s;
        s = ((String) (obj));
_L13:
        s.printStackTrace();
        if (s1 != null)
        {
            try
            {
                s1.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
        }
        if (assetmanager == null) goto _L9; else goto _L8
_L8:
        assetmanager.close();
        return 0;
        assetmanager;
_L11:
        assetmanager.printStackTrace();
        return 0;
_L5:
        s.flush();
        if (s != null)
        {
            try
            {
                s.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
        }
        if (assetmanager == null) goto _L9; else goto _L10
_L10:
        assetmanager.close();
        return 0;
        assetmanager;
          goto _L11
        s;
        assetmanager = null;
        s1 = obj1;
_L12:
        if (s1 != null)
        {
            try
            {
                s1.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s1)
            {
                s1.printStackTrace();
            }
        }
        if (assetmanager != null)
        {
            try
            {
                assetmanager.close();
            }
            // Misplaced declaration of an exception variable
            catch (AssetManager assetmanager)
            {
                assetmanager.printStackTrace();
            }
        }
        throw s;
        assetmanager;
          goto _L11
        s;
        s1 = obj1;
          goto _L12
        obj;
        s1 = s;
        s = ((String) (obj));
          goto _L12
        s;
          goto _L12
        s;
        assetmanager = null;
        s1 = ((String) (obj));
          goto _L13
        s;
        s1 = ((String) (obj));
          goto _L13
    }

    public static int copyFile(File file, File file1)
    {
        Object obj;
        Object obj1;
        obj = null;
        obj1 = null;
        file = (new FileInputStream(file)).getChannel();
        Exception exception;
        Exception exception1;
        try
        {
            file1 = (new FileOutputStream(file1)).getChannel();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            file1 = file;
            file = null;
            continue; /* Loop/switch isn't completed */
        }
        finally
        {
            continue; /* Loop/switch isn't completed */
        }
        file1.transferFrom(file, 0L, file.size());
        int i;
        if (file != null)
        {
            try
            {
                file.close();
            }
            // Misplaced declaration of an exception variable
            catch (File file)
            {
                file.printStackTrace();
            }
        }
        if (file1 != null)
        {
            try
            {
                file1.close();
            }
            // Misplaced declaration of an exception variable
            catch (File file)
            {
                file.printStackTrace();
            }
        }
        i = 0;
        return i;
        obj;
        file = null;
        file1 = obj1;
_L4:
        ((IOException) (obj)).printStackTrace();
        i = -1;
        if (file1 != null)
        {
            try
            {
                file1.close();
            }
            // Misplaced declaration of an exception variable
            catch (File file1)
            {
                file1.printStackTrace();
            }
        }
        if (file != null)
        {
            try
            {
                file.close();
            }
            // Misplaced declaration of an exception variable
            catch (File file)
            {
                file.printStackTrace();
                return -1;
            }
            return -1;
        } else
        {
            break MISSING_BLOCK_LABEL_58;
        }
        file1;
        file = null;
_L2:
        if (file != null)
        {
            try
            {
                file.close();
            }
            // Misplaced declaration of an exception variable
            catch (File file)
            {
                file.printStackTrace();
            }
        }
        if (obj != null)
        {
            try
            {
                ((FileChannel) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (File file)
            {
                file.printStackTrace();
            }
        }
        throw file1;
        exception;
        obj = file1;
        file1 = exception;
        continue; /* Loop/switch isn't completed */
        exception1;
        obj = file1;
        File file2 = file;
        file1 = exception1;
        file = ((File) (obj));
        obj = file2;
        if (true) goto _L2; else goto _L1
_L1:
        break MISSING_BLOCK_LABEL_28;
        obj;
        File file3 = file;
        file = file1;
        file1 = file3;
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static int copyFile(String s, String s1)
    {
        return copyFile(s, s1);
    }

    public static boolean deleteDir(File file)
    {
        if (file.isDirectory())
        {
            File afile[] = file.listFiles();
            int j = afile.length;
            for (int i = 0; i < j; i++)
            {
                deleteDir(afile[i]);
            }

            file.delete();
        } else
        {
            file.delete();
        }
        return true;
    }

    public static boolean deleteDir(String s)
    {
        return deleteDir(new File(s));
    }

    public static long getFileSize(File file)
    {
        long l;
        if (file == null || !file.exists())
        {
            return 0L;
        }
        if (!file.canRead())
        {
            Logger.e("FileUtils", (new StringBuilder()).append("GetFileSize Failed due to permission deny ").append(file.getAbsolutePath()).toString());
            return 0L;
        }
        if (file.isFile())
        {
            return file.length();
        }
        Stack stack = new Stack();
        stack.push(file);
        l = 0L;
        do
        {
label0:
            {
                do
                {
                    if (stack.empty())
                    {
                        break label0;
                    }
                    file = (File)stack.pop();
                } while (file == null);
                if (file.isFile())
                {
                    l += file.length();
                } else
                {
                    file = file.listFiles();
                    if (file != null && file.length != 0)
                    {
                        int j = file.length;
                        int i = 0;
                        long l1 = l;
                        do
                        {
                            l = l1;
                            if (i >= j)
                            {
                                break;
                            }
                            File file1 = file[i];
                            if (file1 != null)
                            {
                                if (file1.isFile())
                                {
                                    l1 += file1.length();
                                } else
                                {
                                    stack.push(file1);
                                }
                            }
                            i++;
                        } while (true);
                    }
                }
            }
        } while (true);
        return l;
    }

    public static long getFileSize(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return 0L;
        } else
        {
            return getFileSize(new File(s));
        }
    }

    public static String inputStream2String(InputStream inputstream, String s)
    {
        try
        {
            byte abyte0[] = new byte[inputstream.available()];
            inputstream.read(abyte0);
            inputstream = EncodingUtils.getString(abyte0, s);
        }
        // Misplaced declaration of an exception variable
        catch (InputStream inputstream)
        {
            inputstream.printStackTrace();
            inputstream = "";
        }
        s = inputstream;
        if (inputstream == null)
        {
            s = "";
        }
        return s;
    }

    public static String readAssetFileData(Context context, String s)
    {
        if (context != null) goto _L2; else goto _L1
_L1:
        s = "";
_L4:
        return s;
_L2:
        Object obj;
        Object obj1;
        if ("" != null && !"".equals(""))
        {
            break; /* Loop/switch isn't completed */
        }
        obj1 = context.getResources().getAssets();
        obj = null;
        context = null;
        s = ((AssetManager) (obj1)).open(s);
        context = s;
        obj = s;
        obj1 = inputStream2String(s, "UTF-8");
        obj = obj1;
        context = ((Context) (obj));
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_67;
        }
        s.close();
        context = ((Context) (obj));
_L5:
        s = context;
        if (context == null)
        {
            return "";
        }
        if (true) goto _L4; else goto _L3
        s;
        obj = context;
        Logger.log((new StringBuilder()).append("readCategoryTagMenuData getAssets \u53D1\u751F\u5F02\u5E38:").append(s.toString()).toString());
        if (context != null)
        {
            try
            {
                context.close();
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                Logger.log((new StringBuilder()).append("readCategoryTagMenuData close getAssets \u53D1\u751F\u5F02\u5E38:").append(context.toString()).toString());
            }
        }
        context = "";
          goto _L5
        context;
        if (obj != null)
        {
            try
            {
                ((InputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.log((new StringBuilder()).append("readCategoryTagMenuData close getAssets \u53D1\u751F\u5F02\u5E38:").append(s.toString()).toString());
            }
        }
        throw context;
        context;
        Logger.log((new StringBuilder()).append("readCategoryTagMenuData close getAssets \u53D1\u751F\u5F02\u5E38:").append(context.toString()).toString());
        context = ((Context) (obj));
          goto _L5
_L3:
        context = "";
          goto _L5
    }

    public static byte[] readByteFromFile(String s)
    {
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        Logger.e("FileUtils", "readByteFromFile path is null");
_L6:
        return null;
_L2:
        Object obj1;
        int j;
        s = new File(s);
        if (!s.exists() || s.isDirectory() || s.length() == 0L)
        {
            Logger.e("FileUtils", "readByteFromFile file not exists or is a directory");
            return null;
        }
        j = (int)s.length();
        obj1 = new byte[j];
        Object obj = new FileInputStream(s);
        int i = 0;
_L4:
        s = ((String) (obj));
        int k = ((FileInputStream) (obj)).read(((byte []) (obj1)), i, Math.min(2048, j - i));
        if (k <= 0)
        {
            break; /* Loop/switch isn't completed */
        }
        i += k;
        if (true) goto _L4; else goto _L3
        obj1;
        obj = null;
_L9:
        s = ((String) (obj));
        ((Exception) (obj1)).printStackTrace();
        if (obj == null) goto _L6; else goto _L5
_L5:
        try
        {
            ((FileInputStream) (obj)).close();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return null;
        }
        return null;
        obj;
        s = null;
_L8:
        if (s != null)
        {
            try
            {
                s.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
        }
        throw obj;
_L3:
        if (obj != null)
        {
            try
            {
                ((FileInputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
        }
        return ((byte []) (obj1));
        obj;
        if (true) goto _L8; else goto _L7
_L7:
        obj1;
          goto _L9
    }

    public static String readStrFromFile(String s)
    {
        Object obj2;
        Object obj3;
        obj2 = null;
        obj3 = null;
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        Object obj1 = obj3;
_L6:
        return ((String) (obj1));
_L2:
        Object obj = new BufferedReader(new FileReader(s));
        s = ((String) (obj));
        obj1 = new StringBuffer();
_L4:
        s = ((String) (obj));
        String s1 = ((BufferedReader) (obj)).readLine();
        if (s1 == null)
        {
            break; /* Loop/switch isn't completed */
        }
        s = ((String) (obj));
        ((StringBuffer) (obj1)).append(s1);
        if (true) goto _L4; else goto _L3
        obj1;
_L11:
        s = ((String) (obj));
        ((Exception) (obj1)).printStackTrace();
        obj1 = obj3;
        if (obj == null) goto _L6; else goto _L5
_L5:
        ((BufferedReader) (obj)).close();
        return null;
        obj;
        s = obj2;
_L8:
        ((IOException) (obj)).printStackTrace();
        return s;
_L3:
        s = ((String) (obj));
        obj1 = ((StringBuffer) (obj1)).toString();
        s = ((String) (obj1));
        obj1 = s;
        if (obj == null) goto _L6; else goto _L7
_L7:
        ((BufferedReader) (obj)).close();
        return s;
        obj;
          goto _L8
        obj;
        s = null;
_L10:
        if (s != null)
        {
            try
            {
                s.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
        }
        throw obj;
        obj;
        if (true) goto _L10; else goto _L9
_L9:
        obj1;
        obj = null;
          goto _L11
    }

    public static int renameFile(File file, File file1)
    {
        Exception exception;
        Object obj1;
        byte byte0;
        obj1 = null;
        exception = null;
        byte0 = 0;
        if (file != null && file.canRead()) goto _L2; else goto _L1
_L1:
        byte0 = -1;
_L4:
        return byte0;
_L2:
        if (file1 == null)
        {
            return -2;
        }
        boolean flag;
        if (file1.exists())
        {
            break MISSING_BLOCK_LABEL_346;
        }
        flag = file1.createNewFile();
_L14:
        Object obj;
        int i;
        if (flag)
        {
            break MISSING_BLOCK_LABEL_86;
        }
        byte0 = -3;
        if (false)
        {
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (File file)
            {
                file.printStackTrace();
            }
        }
        if (true) goto _L4; else goto _L3
_L3:
        try
        {
            throw new NullPointerException();
        }
        // Misplaced declaration of an exception variable
        catch (File file)
        {
            file.printStackTrace();
        }
        return -3;
        obj = new FileInputStream(file);
        file = new FileOutputStream(file1);
        file1 = new byte[8192];
_L6:
        i = ((FileInputStream) (obj)).read(file1);
        if (i <= 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        file.write(file1, 0, i);
        file.flush();
        if (i > 0) goto _L6; else goto _L5
_L5:
        i = byte0;
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_155;
        }
        ((FileInputStream) (obj)).close();
        i = byte0;
_L11:
        byte0 = i;
        if (file == null) goto _L4; else goto _L7
_L7:
        file.flush();
        file.close();
        return i;
        file;
        file.printStackTrace();
_L9:
        return 1;
        obj;
        file = null;
        file1 = exception;
_L13:
        ((Exception) (obj)).printStackTrace();
        if (file == null)
        {
            break MISSING_BLOCK_LABEL_198;
        }
        file.close();
        i = -4;
_L10:
        byte0 = i;
        if (file1 == null) goto _L4; else goto _L8
_L8:
        file1.flush();
        file1.close();
        return i;
        file;
        file.printStackTrace();
          goto _L9
        file;
        obj = null;
        file1 = obj1;
_L12:
        if (obj != null)
        {
            try
            {
                ((FileInputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        if (file1 != null)
        {
            try
            {
                file1.flush();
                file1.close();
            }
            // Misplaced declaration of an exception variable
            catch (File file1)
            {
                file1.printStackTrace();
            }
        }
        throw file;
        file;
        file.printStackTrace();
        i = 1;
          goto _L10
        file1;
        file1.printStackTrace();
        i = 1;
          goto _L11
        file;
        file1 = obj1;
          goto _L12
        exception;
        file1 = file;
        file = exception;
          goto _L12
        exception;
        obj = file;
        file = exception;
          goto _L12
        file1;
        file = ((File) (obj));
        obj = file1;
        file1 = exception;
          goto _L13
        Exception exception1;
        exception1;
        file1 = file;
        file = ((File) (obj));
        obj = exception1;
          goto _L13
        flag = false;
          goto _L14
    }

    public static int renameFile(String s, String s1)
    {
        return renameFile(new File(s), new File(s1));
    }

    public static long sizeOfDirectory(File file)
    {
        Object obj;
        long l;
        l = 0L;
        obj = file.listFiles();
        if (obj != null) goto _L2; else goto _L1
_L1:
        long l1;
        obj = null;
        if (!file.exists())
        {
            obj = (new StringBuilder()).append(file).append(" does not exist").toString();
        } else
        if (!file.isDirectory())
        {
            obj = (new StringBuilder()).append(file).append(" is not a directory").toString();
        }
        l1 = l;
        if (obj == null);
_L4:
        return l1;
_L2:
        int i = 0;
        do
        {
            l1 = l;
            if (i >= obj.length)
            {
                continue;
            }
            file = obj[i];
            if (file.isDirectory())
            {
                l += sizeOfDirectory(file);
            } else
            {
                l += file.length();
            }
            i++;
        } while (true);
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static void writeStr2File(String s, String s1)
    {
        if (!TextUtils.isEmpty(s) && !TextUtils.isEmpty(s1)) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Exception exception = null;
        BufferedWriter bufferedwriter = new BufferedWriter(new FileWriter(s1));
        s1 = bufferedwriter;
        bufferedwriter.write(s);
        if (bufferedwriter == null) goto _L1; else goto _L3
_L3:
        try
        {
            bufferedwriter.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s) { }
_L5:
        s.printStackTrace();
        return;
        exception;
        s = null;
_L8:
        s1 = s;
        exception.printStackTrace();
        if (s == null) goto _L1; else goto _L4
_L4:
        try
        {
            s.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s) { }
          goto _L5
        s;
        s1 = exception;
_L7:
        if (s1 != null)
        {
            try
            {
                s1.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s1)
            {
                s1.printStackTrace();
            }
        }
        throw s;
        s;
        if (true) goto _L7; else goto _L6
_L6:
        exception;
        s = bufferedwriter;
          goto _L8
    }
}
