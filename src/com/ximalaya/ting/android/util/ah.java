// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.graphics.Bitmap;
import com.squareup.picasso.Transformation;

// Referenced classes of package com.ximalaya.ting.android.util:
//            ImageManager2, Blur

class ah
    implements Transformation
{

    final String a;
    final int b;
    final ImageManager2 c;

    ah(ImageManager2 imagemanager2, String s, int i)
    {
        c = imagemanager2;
        a = s;
        b = i;
        super();
    }

    public String key()
    {
        return a;
    }

    public Bitmap transform(Bitmap bitmap)
    {
        Object obj = ImageManager2.access$000();
        int i;
        if (b != 0)
        {
            i = b;
        } else
        {
            i = -50;
        }
        obj = Blur.fastBlur(((android.content.Context) (obj)), bitmap, 40, i);
        bitmap.recycle();
        return ((Bitmap) (obj));
    }
}
