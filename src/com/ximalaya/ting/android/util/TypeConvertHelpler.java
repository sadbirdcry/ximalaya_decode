// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class TypeConvertHelpler
{

    public TypeConvertHelpler()
    {
    }

    public static Object bytes2Object(byte abyte0[])
        throws IOException, ClassNotFoundException
    {
        return (new ObjectInputStream(new ByteArrayInputStream(abyte0))).readObject();
    }

    public static boolean getBooleanFromInt(Integer integer)
    {
        while (integer == null || integer.intValue() == 0) 
        {
            return false;
        }
        return true;
    }

    public static byte[] object2Bytes(Object obj)
        throws IOException
    {
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
        (new ObjectOutputStream(bytearrayoutputstream)).writeObject(obj);
        return bytearrayoutputstream.toByteArray();
    }
}
