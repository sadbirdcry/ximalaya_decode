// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.library.util.FreeFlowUtilBase;
import com.ximalaya.ting.android.model.free_flow.PhoneUserModel;
import java.text.SimpleDateFormat;
import java.util.Date;

// Referenced classes of package com.ximalaya.ting.android.util:
//            MyAsyncTask, FreeFlowUtil, Logger, Utilities

class it> extends MyAsyncTask
{

    final FreeFlowUtil a;
    private volatile int b;

    public transient String a(Integer ainteger[])
    {
        b = ainteger[0].intValue();
        if (b > 3)
        {
            return null;
        } else
        {
            ainteger = FreeFlowUtil.access$900(a);
            Logger.log((new StringBuilder()).append("FreeFlowUtil getPhoneUserInfo response = ").append(ainteger).toString());
            Logger.logToSd((new StringBuilder()).append("FreeFlowUtil getPhoneUserInfo response = ").append(ainteger).toString());
            return ainteger;
        }
    }

    public void a(String s)
    {
        if (!Utilities.isNotEmpty(s))
        {
            break MISSING_BLOCK_LABEL_297;
        }
        if (s.equals("timeout"))
        {
            (new <init>(a)).myexec(new Integer[] {
                Integer.valueOf(b + 1)
            });
            return;
        }
        String s1;
        try
        {
            s = (PhoneUserModel)JSON.parseObject(s, com/ximalaya/ting/android/model/free_flow/PhoneUserModel);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.log((new StringBuilder()).append("FreeFlowUtil GetPhoneUserInfoFromUnicomServer exception: ").append(s.getMessage()).toString());
            Logger.logToSd((new StringBuilder()).append("FreeFlowUtil GetPhoneUserInfoFromUnicomServer exception: ").append(s.getMessage()).toString());
            FreeFlowUtil.access$400(a);
            return;
        }
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_265;
        }
        if (!"000000".equals(s.getReturnCode()))
        {
            break MISSING_BLOCK_LABEL_265;
        }
        Logger.log((new StringBuilder()).append("FreeFlowUtil saved number = ").append(s.getMobile()).toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil saved number = ").append(s.getMobile()).toString());
        s1 = ((TelephonyManager)FreeFlowUtilBase.mContext.getSystemService("phone")).getSubscriberId();
        if (TextUtils.isEmpty(s.getImsi()) || s.getImsi() != null && s.getImsi().equals(s1))
        {
            a.savePhoneNumber(s.getMobile());
            a.saveIMSI(s1);
            FreeFlowUtil.access$1000(a, s.getMobile());
            return;
        }
        FreeFlowUtil.access$400(a);
        return;
        (new <init>(a)).myexec(new Integer[] {
            Integer.valueOf(b + 1)
        });
        return;
        s = new Date();
        s = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss aa")).format(s);
        Logger.log((new StringBuilder()).append("FreeFlowUtil ").append(s).append(" \u51FA\u73B0\u53D6\u53F7\u5931\u8D25\u73B0\u8C61").toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil ").append(s).append(" \u51FA\u73B0\u53D6\u53F7\u5931\u8D25\u73B0\u8C61").toString());
        FreeFlowUtil.access$400(a);
        return;
    }

    public Object doInBackground(Object aobj[])
    {
        return a((Integer[])aobj);
    }

    public void onPostExecute(Object obj)
    {
        a((String)obj);
    }

    wUtilBase(FreeFlowUtil freeflowutil)
    {
        a = freeflowutil;
        super();
    }
}
