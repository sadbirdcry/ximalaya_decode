// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.graphics.Bitmap;
import com.squareup.picasso.Transformation;

// Referenced classes of package com.ximalaya.ting.android.util:
//            BitmapUtils, ImageManager2

class af
    implements Transformation
{

    final int a;
    final String b;
    final ImageManager2 c;

    af(ImageManager2 imagemanager2, int i, String s)
    {
        c = imagemanager2;
        a = i;
        b = s;
        super();
    }

    public String key()
    {
        if (b == null)
        {
            return "";
        } else
        {
            return b;
        }
    }

    public Bitmap transform(Bitmap bitmap)
    {
        Bitmap bitmap1 = BitmapUtils.getRoundCornerBitmap(bitmap, a);
        bitmap.recycle();
        return bitmap1;
    }
}
