// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.Log;
import android.widget.EditText;
import com.ximalaya.ting.android.activity.play.HtmlImageGetter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Referenced classes of package com.ximalaya.ting.android.util:
//            ToolUtil, BitmapUtils

public class EmotionUtil
{

    private static byte sLock[] = new byte[0];
    private static EmotionUtil sUtil = null;
    private Context mAppContext;
    private int mEmotionIcons[];
    private int mEmotionNames[];
    private HtmlImageGetter mHtmlImageGetter;
    private Resources mResources;

    private EmotionUtil()
    {
        mEmotionNames = null;
        mEmotionIcons = null;
        init(null, 0, 0);
    }

    private CharSequence convertChangeLine(String s)
    {
        String s1 = s;
        if (s == null)
        {
            s1 = "";
        }
        for (s = Pattern.compile("\n").matcher(s1); s.find();)
        {
            s1 = s1.replace(s.group(), "<br>");
        }

        return s1;
    }

    public static EmotionUtil getInstance()
    {
        if (sUtil == null)
        {
            synchronized (sLock)
            {
                if (sUtil == null)
                {
                    sUtil = new EmotionUtil();
                }
            }
        }
        return sUtil;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void addEmotion(String s, int i, int j)
    {
    }

    public CharSequence convertEmotion(String s)
    {
        String s1 = s;
        if (s == null)
        {
            s1 = "";
        }
        s = Pattern.compile("\\[[^\\[\\]]*\\]").matcher(s1);
        do
        {
            if (!s.find())
            {
                break;
            }
            String s2 = s.group();
            if (isEmotionName(s2))
            {
                int i = getEmotionIconId(s2);
                s1 = s1.replace(s2, (new StringBuilder()).append("<img src = '").append(mResources.getResourceName(i)).append("'/>").toString());
            }
        } while (true);
        return Html.fromHtml(s1, mHtmlImageGetter, null);
    }

    public CharSequence convertEmotionText(CharSequence charsequence)
    {
        Object obj = charsequence.toString();
        charsequence = ((CharSequence) (obj));
        if (obj == null)
        {
            charsequence = "";
        }
        obj = Pattern.compile("\\[[^\\[\\]]*\\]").matcher(charsequence);
        do
        {
            if (!((Matcher) (obj)).find())
            {
                break;
            }
            String s = ((Matcher) (obj)).group();
            if (isEmotionName(s))
            {
                int i = getEmotionIconId(s);
                charsequence = charsequence.replace(s, (new StringBuilder()).append("<img src = '").append(mResources.getResourceName(i)).append("'/>").toString());
            }
        } while (true);
        return Html.fromHtml(charsequence, mHtmlImageGetter, null);
    }

    public CharSequence convertEmotionTextWithChangeLine(String s)
    {
        return convertEmotionText(convertChangeLine(s));
    }

    public boolean deleteEmotion(EditText edittext)
    {
        int k = edittext.getSelectionStart();
        if (k == 0)
        {
            return false;
        }
        String s = edittext.getEditableText().toString();
        String s1 = s.substring(0, k - 1);
        String s2 = s.substring(k - 1, k);
        int i;
        if (s.contains("[") && s.contains("]") && "]".equals(s2))
        {
            int j = s1.lastIndexOf("[");
            i = j;
            if (!isEmotionName(s.substring(j, k)))
            {
                i = k - 1;
            }
        } else
        {
            i = k - 1;
        }
        edittext.getEditableText().delete(i, k);
        return true;
    }

    public int getEmotionCount()
    {
        if (mEmotionIcons == null || mEmotionNames == null)
        {
            return 0;
        } else
        {
            return Math.min(mEmotionIcons.length, mEmotionNames.length);
        }
    }

    public Drawable getEmotionIconAt(int i)
    {
        Object obj = null;
        Drawable drawable = obj;
        if (i >= 0)
        {
            drawable = obj;
            if (i < mEmotionIcons.length)
            {
                drawable = mResources.getDrawable(mEmotionIcons[i]);
            }
        }
        return drawable;
    }

    public int getEmotionIconId(String s)
    {
        int i = getEmotionIndex(s);
        if (i != -1)
        {
            return mEmotionIcons[i];
        } else
        {
            return 0;
        }
    }

    public int getEmotionIconIdAt(int i)
    {
        if (i >= 0 && i < mEmotionIcons.length)
        {
            return mEmotionIcons[i];
        } else
        {
            return 0;
        }
    }

    public int getEmotionIconIdByName(String s)
    {
        int i = getEmotionIndex(s);
        if (i != -1)
        {
            return mEmotionIcons[i];
        } else
        {
            return 0;
        }
    }

    public int getEmotionIndex(String s)
    {
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        int j = -1;
_L4:
        return j;
_L2:
        int i = 0;
label0:
        do
        {
label1:
            {
                if (i >= mEmotionNames.length)
                {
                    break label1;
                }
                j = i;
                if (s.equals(mResources.getString(mEmotionNames[i])))
                {
                    break label0;
                }
                i++;
            }
        } while (true);
        if (true) goto _L4; else goto _L3
_L3:
        return -1;
    }

    public String getEmotionNameAt(int i)
    {
        if (i < 0 || i > mEmotionNames.length)
        {
            return null;
        } else
        {
            return mResources.getString(mEmotionNames[i]);
        }
    }

    public String getEmotionNameByResId(int i)
    {
        if (i != 0)
        {
            return mResources.getString(i);
        } else
        {
            return null;
        }
    }

    public void init(Context context, int i, int j)
    {
        Object obj;
        Context context1;
        Context context2;
        Object obj1;
        boolean flag;
        context2 = null;
        context1 = null;
        obj1 = null;
        obj = null;
        flag = false;
        if (mAppContext == null || mResources == null || mEmotionNames == null || mEmotionIcons == null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (context == null || i == 0 || j == 0)
        {
            mEmotionIcons = new int[0];
            mEmotionNames = new int[0];
            return;
        }
        mAppContext = context.getApplicationContext();
        mResources = mAppContext.getResources();
        mHtmlImageGetter = new HtmlImageGetter(context, 30, 30);
        context = mResources.obtainTypedArray(j);
        context2 = context;
        context = context1;
        context1 = context2;
        obj = obj1;
        obj1 = mResources.obtainTypedArray(i);
        context = ((Context) (obj1));
        context1 = context2;
        obj = obj1;
        j = Math.min(context2.length(), ((TypedArray) (obj1)).length());
        context = ((Context) (obj1));
        context1 = context2;
        obj = obj1;
        mEmotionIcons = new int[j];
        context = ((Context) (obj1));
        context1 = context2;
        obj = obj1;
        mEmotionNames = new int[j];
        i = ((flag) ? 1 : 0);
_L4:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        context = ((Context) (obj1));
        context1 = context2;
        obj = obj1;
        mEmotionIcons[i] = context2.getResourceId(i, 0);
        context = ((Context) (obj1));
        context1 = context2;
        obj = obj1;
        mEmotionNames[i] = ((TypedArray) (obj1)).getResourceId(i, 0);
        i++;
        if (true) goto _L4; else goto _L3
        obj1;
        context2 = null;
_L8:
        context = ((Context) (obj));
        context1 = context2;
        ((Exception) (obj1)).printStackTrace();
        if (context2 != null)
        {
            context2.recycle();
        }
        if (obj == null) goto _L1; else goto _L5
_L5:
        ((TypedArray) (obj)).recycle();
        return;
        obj;
        context1 = null;
        context = context2;
_L7:
        if (context1 != null)
        {
            context1.recycle();
        }
        if (context != null)
        {
            context.recycle();
        }
        throw obj;
_L3:
        if (context2 != null)
        {
            context2.recycle();
        }
        if (obj1 == null) goto _L1; else goto _L6
_L6:
        obj = obj1;
          goto _L5
        obj;
          goto _L7
        obj1;
          goto _L8
    }

    public void insertEmotion(EditText edittext, String s, Drawable drawable)
    {
        this;
        JVM INSTR monitorenter ;
        int i = edittext.getSelectionStart();
        drawable = BitmapUtils.getBitmap(drawable, ToolUtil.dp2px(mAppContext, 30F), ToolUtil.dp2px(mAppContext, 30F));
        drawable = new ImageSpan(mAppContext, drawable);
        SpannableString spannablestring = new SpannableString(s);
        spannablestring.setSpan(drawable, 0, s.length(), 33);
        edittext.requestFocus();
        edittext.getEditableText().insert(i, spannablestring);
        this;
        JVM INSTR monitorexit ;
        return;
        edittext;
        throw edittext;
    }

    public boolean isEmotionName(String s)
    {
        return getEmotionIndex(s) != -1;
    }

    public boolean isPositionInEmotion(String s, int i, int ai[])
    {
label0:
        {
label1:
            {
                if (i <= 0)
                {
                    return false;
                }
                if (!s.contains("[") || !s.contains("]"))
                {
                    break label0;
                }
                if (ai == null || ai.length < 2)
                {
                    Log.e("", "Range array size must be 2!");
                    return false;
                }
                int j = i - 1;
                if (s.charAt(j) != ']')
                {
                    break label1;
                }
                ai[1] = j;
                int i1;
                do
                {
                    i1 = j - 1;
                    if (i1 < 0)
                    {
                        break label1;
                    }
                    j = i1;
                } while (s.charAt(i1) != '[');
                ai[0] = i1;
                return isEmotionName(s.substring(ai[0], ai[1] + 1));
            }
            int k = i;
            int j1;
            do
            {
                j1 = k - 1;
                if (j1 < 0)
                {
                    break label0;
                }
                k = j1;
            } while (s.charAt(j1) != '[');
            ai[0] = j1;
            i--;
            do
            {
                int l = i + 1;
                if (l < s.length())
                {
                    if (s.charAt(l) == '[')
                    {
                        return false;
                    }
                    i = l;
                    if (s.charAt(l) == ']')
                    {
                        ai[1] = l;
                        return isEmotionName(s.substring(ai[0], ai[1] + 1));
                    }
                } else
                {
                    return false;
                }
            } while (true);
        }
        return false;
    }

}
