// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.util:
//            MD5, FileUtils, ToolUtil, Utilities

public class MyFileUtils
{

    private static String appParamMetersFilename = "app_parameters.txt";
    private static String downloadDataFileName = "download_data.txt";

    public MyFileUtils()
    {
    }

    public static void deleteFile(File file)
    {
        if (!file.exists()) goto _L2; else goto _L1
_L1:
        if (!file.isFile()) goto _L4; else goto _L3
_L3:
        file.delete();
_L2:
        return;
_L4:
        if (file.isDirectory())
        {
            file = file.listFiles();
            int i = 0;
            while (i < file.length) 
            {
                deleteFile(file[i]);
                i++;
            }
        }
        if (true) goto _L2; else goto _L5
_L5:
    }

    public static void deleteFileForImage()
    {
        File file = new File(a.af);
        if (file != null && file.isDirectory())
        {
            File afile[] = file.listFiles();
            int j = afile.length;
            for (int i = 0; i < j; i++)
            {
                afile[i].delete();
            }

        }
    }

    public static void deleteFileFromSdcard(String s, String s1)
    {
        if ("mounted".equals(Environment.getExternalStorageState()) && Environment.getExternalStorageDirectory().canWrite())
        {
            s = new File(s);
            if (s.exists())
            {
                s.delete();
            }
        }
    }

    public static String getAppParametersFromSD()
    {
        if (!Environment.getExternalStorageState().equals("mounted"))
        {
            return null;
        }
        String s;
        try
        {
            s = getStringFromFile(a.ac + "/" + appParamMetersFilename);
        }
        catch (Exception exception)
        {
            return null;
        }
        return s;
    }

    public static double getDirSize(File file)
    {
        double d;
        double d1;
        d = 0.0D;
        d1 = d;
        if (!file.exists()) goto _L2; else goto _L1
_L1:
        if (!file.isDirectory())
        {
            break MISSING_BLOCK_LABEL_75;
        }
        file = file.listFiles();
        d1 = d;
        if (file == null) goto _L2; else goto _L3
_L3:
        if (file.length != 0) goto _L5; else goto _L4
_L4:
        d1 = d;
_L2:
        return d1;
_L5:
        int j = file.length;
        int i = 0;
        do
        {
            d1 = d;
            if (i >= j)
            {
                break;
            }
            d1 = getDirSize(file[i]);
            i++;
            d = d1 + d;
        } while (true);
        if (true) goto _L2; else goto _L6
_L6:
        return (double)file.length() / 1024D / 1024D;
    }

    public static String getDownloadSoundFromSD()
    {
        if (!Environment.getExternalStorageState().equals("mounted"))
        {
            return null;
        }
        String s;
        try
        {
            s = getStringFromFile((new StringBuilder()).append(Environment.getExternalStorageDirectory().getPath()).append("/ting/download/").append(MD5.md5(downloadDataFileName)).toString());
        }
        catch (Exception exception)
        {
            return null;
        }
        return s;
    }

    public static final List getStoragePaths(Context context)
    {
        return new ArrayList();
    }

    public static String getStringFromFile(String s)
    {
        if (!Environment.getExternalStorageState().equals("mounted"))
        {
            return null;
        }
        try
        {
            s = FileUtils.readStrFromFile(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return null;
        }
        return s;
    }

    public static final boolean isFileExist(DownloadTask downloadtask)
    {
        boolean flag;
        ToolUtil.createAppDirectory();
        downloadtask = new File(a.aj, ToolUtil.md5(downloadtask.downLoadUrl));
        if (!downloadtask.isFile())
        {
            break MISSING_BLOCK_LABEL_40;
        }
        flag = downloadtask.exists();
        if (flag)
        {
            return true;
        }
        break MISSING_BLOCK_LABEL_40;
        downloadtask;
        return false;
    }

    public static final boolean isSDcardInvalid()
    {
        String s = Environment.getExternalStorageState();
        return !s.equals("mounted") && !s.equals("mounted_ro") && !s.equals("checking");
    }

    public static final boolean isSDcardPrepared()
    {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static final boolean isSourceInLocal(Context context, String s)
    {
        boolean flag1 = false;
        boolean flag = flag1;
        if (Utilities.isNotBlank(s))
        {
            flag = flag1;
            if (DownloadHandler.hasDownloaded(s, context))
            {
                flag = true;
            }
        }
        return flag;
    }

    public static final void writeAppParametersToSD(String s)
    {
        if (!Environment.getExternalStorageState().equals("mounted"))
        {
            return;
        }
        try
        {
            FileUtils.writeStr2File(s, a.ac + "/" + appParamMetersFilename);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
    }

    public static void writeCrashLogToSD(String s)
    {
        if (!Environment.getExternalStorageState().equals("mounted"))
        {
            return;
        }
        try
        {
            FileUtils.writeStr2File(s, (new StringBuilder()).append(Environment.getExternalStorageDirectory().getPath()).append("/ting/crash_log.txt").toString());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
    }

    public static void writeDownloadSoundToSD(String s)
    {
        while (TextUtils.isEmpty(s) || !Environment.getExternalStorageState().equals("mounted")) 
        {
            return;
        }
        try
        {
            FileUtils.writeStr2File(s, (new StringBuilder()).append(Environment.getExternalStorageDirectory().getPath()).append("/ting/download/").append(MD5.md5(downloadDataFileName)).toString());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
    }

}
