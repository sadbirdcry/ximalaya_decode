// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

// Referenced classes of package com.ximalaya.ting.android.util:
//            MyAsyncTask, FreeFlowUtil, Logger

class x extends MyAsyncTask
{

    final String a;
    final FreeFlowUtil b;

    x(FreeFlowUtil freeflowutil, String s)
    {
        b = freeflowutil;
        a = s;
        super();
    }

    public transient Integer a(String as[])
    {
        as = FreeFlowUtil.access$000(b, as[0]);
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil verifyUserOrderStatus response = ").append(as).toString());
        if (TextUtils.isEmpty(as) || as.equals("timeout"))
        {
            break MISSING_BLOCK_LABEL_185;
        }
        as = JSON.parseObject(as);
        Logger.log((new StringBuilder()).append("FreeFlowUtil orderstatus = ").append(as.toJSONString()).toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil orderstatus = ").append(as.toJSONString()).toString());
        if (as == null)
        {
            break MISSING_BLOCK_LABEL_185;
        }
        int i = as.getIntValue("x_status");
        if ("000000".equals(as.getString("x_returnCode")))
        {
            return Integer.valueOf(i);
        }
        break MISSING_BLOCK_LABEL_185;
        as;
        Logger.log((new StringBuilder()).append("FreeFlowUtil verifyUserOrderStatus exception: ").append(as.getMessage()).toString());
        Logger.logToSd((new StringBuilder()).append("FreeFlowUtil verifyUserOrderStatus exception: ").append(as.getMessage()).toString());
        return Integer.valueOf(-1);
    }

    public void a(Integer integer)
    {
        switch (integer.intValue())
        {
        default:
            return;

        case 1: // '\001'
        case 2: // '\002'
            FreeFlowUtil.access$100(b);
            FreeFlowUtil.access$200(b, integer.intValue());
            return;

        case -1: 
            if (b.getOrderStatus() == -1)
            {
                FreeFlowUtil.access$300(b, a);
            }
            FreeFlowUtil.access$400(b);
            return;

        case 0: // '\0'
            FreeFlowUtil.access$200(b, integer.intValue());
            FreeFlowUtil.access$400(b);
            return;
        }
    }

    public Object doInBackground(Object aobj[])
    {
        return a((String[])aobj);
    }

    public void onPostExecute(Object obj)
    {
        a((Integer)obj);
    }
}
