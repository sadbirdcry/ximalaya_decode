// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.app.Activity;
import com.ximalaya.ting.android.MyApplication;

// Referenced classes of package com.ximalaya.ting.android.util:
//            ap, ToolUtil

public final class NotifyDialogUtil
{

    public NotifyDialogUtil()
    {
    }

    public static final void showDialogOnSpaceShortage()
    {
        if (MyApplication.f != null)
        {
            MyApplication.f.runOnUiThread(new ap());
        } else
        if (MyApplication.b() != null)
        {
            ToolUtil.makePlayNotification(MyApplication.b(), "\u559C\u9A6C\u62C9\u96C5", "\u6E29\u99A8\u63D0\u793A", "\u78C1\u76D8\u7A7A\u95F4\u4E0D\u8DB3\uFF0C\u4E3A\u4E86\u4E0D\u5F71\u54CD\u6B63\u5E38\u4F7F\u7528\uFF0C\u8BF7\u53CA\u65F6\u6E05\u7406\u7F13\u5B58\u6587\u4EF6");
            return;
        }
    }
}
