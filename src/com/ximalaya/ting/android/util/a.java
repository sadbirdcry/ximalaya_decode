// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;

// Referenced classes of package com.ximalaya.ting.android.util:
//            BitmapUtils, Logger

final class a extends Thread
{

    final Uri a;
    final BitmapUtils.CompressCallback b;
    final boolean c;

    a(Uri uri, BitmapUtils.CompressCallback compresscallback, boolean flag)
    {
        a = uri;
        b = compresscallback;
        c = flag;
        super();
    }

    public void run()
    {
        Object obj;
        Object obj3;
        boolean flag;
        obj = null;
        flag = false;
        obj3 = new File(URI.create(a.toString()));
        if (((File) (obj3)).exists()) goto _L2; else goto _L1
_L1:
        b.onFinished(null, false);
_L8:
        return;
_L2:
        Object obj2;
        obj2 = new android.graphics.BitmapFactory.Options();
        obj2.inSampleSize = 1;
        obj2.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(((File) (obj3)).getAbsolutePath(), ((android.graphics.BitmapFactory.Options) (obj2)));
        int i = ((android.graphics.BitmapFactory.Options) (obj2)).outHeight * ((android.graphics.BitmapFactory.Options) (obj2)).outWidth * 4;
        if (i > 0x493e00)
        {
            obj2.inSampleSize = (int)Math.sqrt(i / 4 / 1000 / 1000);
        }
        obj2.inJustDecodeBounds = false;
        obj2 = BitmapFactory.decodeFile(((File) (obj3)).getAbsolutePath(), ((android.graphics.BitmapFactory.Options) (obj2)));
        if (!c) goto _L4; else goto _L3
_L3:
        BitmapUtils.writeBitmapToFile(((Bitmap) (obj2)), ((File) (obj3)).getAbsolutePath(), ((File) (obj3)).getAbsolutePath());
_L9:
        obj3 = b;
        if (!c) goto _L6; else goto _L5
_L5:
        obj = a;
_L10:
        ((BitmapUtils.CompressCallback) (obj3)).onFinished(((Uri) (obj)), flag);
        if (obj2 == null || ((Bitmap) (obj2)).isRecycled()) goto _L8; else goto _L7
_L7:
        ((Bitmap) (obj2)).recycle();
        return;
_L4:
        obj = BitmapUtils.access$000(((File) (obj3)).getAbsolutePath());
        BitmapUtils.writeBitmapToFile(((Bitmap) (obj2)), ((String) (obj)), ((String) (obj)));
        flag = true;
          goto _L9
_L6:
        obj = Uri.fromFile(new File(((String) (obj))));
          goto _L10
        Object obj1;
        obj1;
        Logger.e("ImageManager2.compressImage", "\u627E\u4E0D\u5230\u6307\u5B9A\u6587\u4EF6", ((Throwable) (obj1)));
        b.onFinished(null, false);
        if (obj2 == null || ((Bitmap) (obj2)).isRecycled())
        {
            break; /* Loop/switch isn't completed */
        }
          goto _L7
        obj1;
        Logger.e("ImageManager2.compressImage", "\u5199\u5165\u6587\u4EF6\u53D1\u751F\u9519\u8BEF", ((Throwable) (obj1)));
        b.onFinished(null, false);
        if (obj2 == null || ((Bitmap) (obj2)).isRecycled()) goto _L8; else goto _L7
        obj1;
        if (obj2 != null && !((Bitmap) (obj2)).isRecycled())
        {
            ((Bitmap) (obj2)).recycle();
        }
        throw obj1;
    }
}
