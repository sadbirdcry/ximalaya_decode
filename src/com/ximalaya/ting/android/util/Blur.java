// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import java.lang.reflect.Array;

public class Blur
{

    private static final String TAG = "Blur";

    public Blur()
    {
    }

    private static int adjustLightness(int i, int j)
    {
        int k = 255;
        int k1 = Color.alpha(i);
        int i1 = Color.red(i);
        int l = Color.green(i);
        i = Color.blue(i);
        int j1 = i1 + j;
        l += j;
        i1 = i + j;
        if (j1 > 255)
        {
            i = 255;
        } else
        {
            i = j1;
            if (j1 < 0)
            {
                i = 0;
            }
        }
        if (l > 255)
        {
            j = 255;
        } else
        {
            j = l;
            if (l < 0)
            {
                j = 0;
            }
        }
        if (i1 <= 255)
        {
            if (i1 < 0)
            {
                k = 0;
            } else
            {
                k = i1;
            }
        }
        return Color.argb(k1, i, j, k);
    }

    public static Bitmap fastBlur(Context context, Bitmap bitmap, int i)
    {
        return fastBlur(context, bitmap, i, 0);
    }

    public static Bitmap fastBlur(Context context, Bitmap bitmap, int i, int j)
    {
        if (bitmap == null)
        {
            return null;
        }
        Thread.currentThread().setPriority(10);
        if (bitmap.getConfig() != null)
        {
            break MISSING_BLOCK_LABEL_39;
        }
        context = android.graphics.Bitmap.Config.ARGB_8888;
_L1:
        context = bitmap.copy(context, true);
        if (i < 1)
        {
            return null;
        }
        break MISSING_BLOCK_LABEL_47;
        int ai[];
        int ai1[];
        int ai2[];
        int ai3[];
        int ai4[];
        int ai5[][];
        int ai6[];
        int k;
        int l;
        int j1;
        int l1;
        int i2;
        int j2;
        int k2;
        int l2;
        int i3;
        int j3;
        int k3;
        int l3;
        int i4;
        int j4;
        int k4;
        int l4;
        int i5;
        int j5;
        int k5;
        int l5;
        int j6;
        try
        {
            context = bitmap.getConfig();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            return null;
        }
          goto _L1
        k4 = context.getWidth();
        l4 = context.getHeight();
        bitmap = new int[k4 * l4];
        Log.e("pix", (new StringBuilder()).append(k4).append(" ").append(l4).append(" ").append(bitmap.length).toString());
        context.getPixels(bitmap, 0, k4, 0, 0, k4, l4);
        j4 = k4 - 1;
        i5 = l4 - 1;
        k = k4 * l4;
        j5 = i + i + 1;
        ai = new int[k];
        ai1 = new int[k];
        ai2 = new int[k];
        ai3 = new int[Math.max(k4, l4)];
        k = j5 + 1 >> 1;
        l = k * k;
        ai4 = new int[l * 256];
        k = 0;
_L3:
        if (k >= l * 256)
        {
            break; /* Loop/switch isn't completed */
        }
        ai4[k] = k / l;
        k++;
        if (true) goto _L3; else goto _L2
_L2:
        ai5 = (int[][])Array.newInstance(Integer.TYPE, new int[] {
            j5, 3
        });
        k5 = i + 1;
        i3 = 0;
        k = 0;
        j3 = 0;
          goto _L4
_L17:
        if (k3 > i) goto _L6; else goto _L5
_L5:
        l3 = bitmap[Math.min(j4, Math.max(k3, 0)) + k];
        ai6 = ai5[k3 + i];
        ai6[0] = (0xff0000 & l3) >> 16;
        ai6[1] = (0xff00 & l3) >> 8;
        ai6[2] = l3 & 0xff;
        l3 = k5 - Math.abs(k3);
        break MISSING_BLOCK_LABEL_352;
_L18:
        if (i2 >= k4) goto _L8; else goto _L7
_L7:
        ai[k] = ai4[l2];
        ai1[k] = ai4[k3];
        ai2[k] = ai4[l3];
        ai6 = ai5[((k2 - i) + j5) % j5];
        j6 = ai6[0];
        l5 = ai6[1];
        i4 = ai6[2];
        if (j3 != 0) goto _L10; else goto _L9
_L9:
        ai3[i2] = Math.min(i2 + i + 1, j4);
          goto _L10
_L21:
        if (j3 > i) goto _L12; else goto _L11
_L11:
        k3 = Math.max(0, i3) + k;
        ai6 = ai5[j3 + i];
        ai6[0] = ai[k3];
        ai6[1] = ai1[k3];
        ai6[2] = ai2[k3];
        l3 = k5 - Math.abs(j3);
        break MISSING_BLOCK_LABEL_580;
_L22:
        if (j2 >= l4)
        {
            break MISSING_BLOCK_LABEL_1530;
        }
        bitmap[l1] = 0xff000000 & bitmap[l1] | ai4[l2] << 16 | ai4[i3] << 8 | ai4[k3];
        if (j == 0)
        {
            break MISSING_BLOCK_LABEL_694;
        }
        bitmap[l1] = adjustLightness(bitmap[l1], j);
        ai6 = ai5[((j3 - i) + j5) % j5];
        j4 = ai6[0];
        i4 = ai6[1];
        l3 = ai6[2];
        if (k != 0) goto _L14; else goto _L13
_L13:
        ai3[j2] = Math.min(j2 + k5, i5) * k4;
          goto _L14
_L20:
        Log.e("pix", (new StringBuilder()).append(k4).append(" ").append(l4).append(" ").append(bitmap.length).toString());
        context.setPixels(bitmap, 0, k4, 0, 0, k4, l4);
        return context;
_L4:
        if (j3 >= l4) goto _L16; else goto _L15
_L15:
        int i1;
        int k1;
        l = 0;
        k3 = -i;
        l1 = 0;
        i1 = 0;
        k1 = 0;
        i2 = 0;
        j2 = 0;
        k2 = 0;
        l2 = 0;
        j1 = 0;
          goto _L17
        l2 += ai6[0] * l3;
        k2 += ai6[1] * l3;
        j2 += l3 * ai6[2];
        if (k3 > 0)
        {
            l1 += ai6[0];
            j1 += ai6[1];
            l += ai6[2];
        } else
        {
            i2 += ai6[0];
            k1 += ai6[1];
            i1 += ai6[2];
        }
        k3++;
          goto _L17
_L6:
        k3 = k2;
        l3 = j2;
        i4 = 0;
        k2 = i;
        j2 = i2;
        i2 = i4;
          goto _L18
_L10:
        int k6 = bitmap[ai3[i2] + i3];
        ai6[0] = (0xff0000 & k6) >> 16;
        ai6[1] = (0xff00 & k6) >> 8;
        ai6[2] = k6 & 0xff;
        l1 += ai6[0];
        j1 += ai6[1];
        l += ai6[2];
        l2 = (l2 - j2) + l1;
        k3 = (k3 - k1) + j1;
        l3 = (l3 - i1) + l;
        k2 = (k2 + 1) % j5;
        ai6 = ai5[k2 % j5];
        j2 = (j2 - j6) + ai6[0];
        k1 = (k1 - l5) + ai6[1];
        i1 = (i1 - i4) + ai6[2];
        l1 -= ai6[0];
        j1 -= ai6[1];
        l -= ai6[2];
        k++;
        i2++;
          goto _L18
_L8:
        i3 += k4;
        j3++;
          goto _L4
_L16:
        k = 0;
_L23:
        if (k >= k4) goto _L20; else goto _L19
_L19:
        l = 0;
        i3 = -i * k4;
        j3 = -i;
        l1 = 0;
        i1 = 0;
        k1 = 0;
        i2 = 0;
        j2 = 0;
        k2 = 0;
        l2 = 0;
        j1 = 0;
          goto _L21
        i4 = ai[k3];
        j4 = ai1[k3];
        l5 = ai2[k3];
        if (j3 > 0)
        {
            l1 += ai6[0];
            j1 += ai6[1];
            l += ai6[2];
        } else
        {
            i2 += ai6[0];
            k1 += ai6[1];
            i1 += ai6[2];
        }
        k3 = i3;
        if (j3 < i5)
        {
            k3 = i3 + k4;
        }
        j3++;
        j2 = l5 * l3 + j2;
        k2 = j4 * l3 + k2;
        l2 = i4 * l3 + l2;
        i3 = k3;
          goto _L21
_L12:
        i3 = k2;
        j4 = 0;
        k3 = j2;
        j2 = k;
        k2 = i1;
        l3 = k1;
        i4 = i2;
        j3 = i;
        i1 = j1;
        k1 = l1;
        j1 = k2;
        i2 = l3;
        k2 = i4;
        l1 = j2;
        j2 = j4;
          goto _L22
_L14:
        int i6 = ai3[j2] + k;
        ai6[0] = ai[i6];
        ai6[1] = ai1[i6];
        ai6[2] = ai2[i6];
        k1 += ai6[0];
        i1 += ai6[1];
        l += ai6[2];
        l2 = (l2 - k2) + k1;
        i3 = (i3 - i2) + i1;
        k3 = (k3 - j1) + l;
        j3 = (j3 + 1) % j5;
        ai6 = ai5[j3];
        k2 = (k2 - j4) + ai6[0];
        i2 = (i2 - i4) + ai6[1];
        j1 = (j1 - l3) + ai6[2];
        k1 -= ai6[0];
        i1 -= ai6[1];
        l -= ai6[2];
        l1 += k4;
        j2++;
          goto _L22
        k++;
          goto _L23
    }
}
