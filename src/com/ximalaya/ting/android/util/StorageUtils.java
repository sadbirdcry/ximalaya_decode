// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.text.TextUtils;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.a.c;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.d.r;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

// Referenced classes of package com.ximalaya.ting.android.util:
//            ToolUtil, MyFileUtils, Utilities, FileUtils, 
//            Session, Logger, MemorySpaceUtil

public class StorageUtils
{

    public static int count = 0;
    public static ArrayList externalDownloadPaths = new ArrayList();
    public static ArrayList externalRingtonePaths = new ArrayList();
    public static ArrayList externalSDcardPaths = new ArrayList();
    public static String internalDownloadPath = null;
    public static String internalRingtonesPath = null;
    public static String internalSDcardPath = null;
    public static String labels[];
    public static String pathForKITKAT = "/Android/data/com.ximalaya.ting.android/files";
    public static String paths[];
    private static ArrayList sVold = new ArrayList();

    public StorageUtils()
    {
    }

    public static final void checkAndMakeDownloadFolder()
    {
        ToolUtil.createAppDirectory();
        if (externalDownloadPaths != null && externalDownloadPaths.size() > 0)
        {
            for (Iterator iterator = externalDownloadPaths.iterator(); iterator.hasNext(); ToolUtil.createDirectory((String)iterator.next())) { }
        }
        if (externalRingtonePaths != null && externalRingtonePaths.size() > 0)
        {
            for (Iterator iterator1 = externalRingtonePaths.iterator(); iterator1.hasNext(); ToolUtil.createDirectory((String)iterator1.next())) { }
        }
    }

    public static final void cleanAllDownloadFiles()
    {
        if (isInternalSDcardAvaliable())
        {
            MyFileUtils.deleteFile(new File(a.aj));
        }
        if (isExternalSDcardAvaliable())
        {
            for (Iterator iterator = externalDownloadPaths.iterator(); iterator.hasNext(); MyFileUtils.deleteFile(new File((String)iterator.next()))) { }
        }
    }

    public static void determineStorageUtils(Context context)
    {
        com/ximalaya/ting/android/util/StorageUtils;
        JVM INSTR monitorenter ;
        if (labels == null) goto _L2; else goto _L1
_L1:
        int i = labels.length;
        if (i <= 0) goto _L2; else goto _L3
_L3:
        com/ximalaya/ting/android/util/StorageUtils;
        JVM INSTR monitorexit ;
        return;
_L2:
        readVoldFile(context);
        testAndCleanList();
        setProperties();
        checkAndMakeDownloadFolder();
        if (true) goto _L3; else goto _L4
_L4:
        context;
        throw context;
    }

    public static final long getCachesSize()
    {
        float f;
        float f4;
        String s;
        s = null;
        f = 0.0F;
        f4 = 0.0F;
        if (!Environment.getExternalStorageState().equals("mounted")) goto _L2; else goto _L1
_L1:
        float f2;
        File file1;
        File file = new File("/ting/images");
        if (file != null && file.isDirectory())
        {
            File afile[] = file.listFiles();
            int j = afile.length;
            int i = 0;
            float f1 = 0.0F;
            do
            {
                f = f1;
                if (i >= j)
                {
                    break;
                }
                f = afile[i].length();
                i++;
                f1 = f + f1;
            } while (true);
        } else
        {
            f = 0.0F;
        }
        file1 = new File(c.d);
        f2 = f;
        if (file1 == null) goto _L4; else goto _L3
_L3:
        f2 = f;
        if (!file1.exists()) goto _L4; else goto _L5
_L5:
        f2 = f;
        if (!file1.isDirectory()) goto _L4; else goto _L6
_L6:
        Object obj = LocalMediaService.getInstance();
        if (obj == null) goto _L8; else goto _L7
_L7:
        String s1 = ToolUtil.getIncomPlayingAudioFilename(((LocalMediaService) (obj)));
        if (!Utilities.isNotBlank(s1)) goto _L8; else goto _L9
_L9:
        obj = (new StringBuilder()).append(s1).append(".chunk").toString();
        s = (new StringBuilder()).append(s1).append(".index").toString();
_L13:
        long l = FileUtils.sizeOfDirectory(file1);
        f2 = l;
_L10:
        float f3;
        Exception exception;
        Exception exception1;
        long l1;
        if (Utilities.isNotBlank(((String) (obj))))
        {
            f3 = (new File(((String) (obj)))).length();
        } else
        {
            f3 = 0.0F;
        }
        if (Utilities.isNotBlank(s))
        {
            f4 = (new File(s)).length();
        }
        f2 = (f + f2) - (f4 + f3);
_L4:
        l1 = r.a().h();
        f2 += l1;
_L11:
        f = f2 / 1024F / 1024F;
_L2:
        Session.getSession().put("TOTAL_CACHE_SIZE", Long.valueOf((long)f));
        return (long)f;
        exception1;
        exception1.printStackTrace();
        f2 = 0.0F;
          goto _L10
        exception;
        Logger.e(exception);
          goto _L11
_L8:
        obj = null;
        if (true) goto _L13; else goto _L12
_L12:
    }

    public static final long getCachesSize2()
    {
        float f;
        float f1;
        float f2;
        String s;
        boolean flag;
        s = null;
        flag = false;
        f2 = 0.0F;
        f = 0.0F;
        f1 = 0.0F;
        if (!Environment.getExternalStorageState().equals("mounted")) goto _L2; else goto _L1
_L1:
        File file1;
        File file = new File("/ting/images");
        f = f2;
        if (file != null)
        {
            f = f2;
            if (file.isDirectory())
            {
                File afile[] = file.listFiles();
                int k = afile.length;
                int i = 0;
                do
                {
                    f = f1;
                    if (i >= k)
                    {
                        break;
                    }
                    f = afile[i].length();
                    i++;
                    f1 = f + f1;
                } while (true);
            }
        }
        file1 = new File(c.d);
        f1 = f;
        if (file1 == null) goto _L4; else goto _L3
_L3:
        f1 = f;
        if (!file1.isDirectory()) goto _L4; else goto _L5
_L5:
        Object obj = LocalMediaService.getInstance();
        if (obj == null) goto _L7; else goto _L6
_L6:
        Object obj1 = ToolUtil.getIncomPlayingAudioFilename(((LocalMediaService) (obj)));
        if (!Utilities.isNotBlank(((String) (obj1)))) goto _L7; else goto _L8
_L8:
        obj = (new StringBuilder()).append(((String) (obj1))).append(".chunk").toString();
        s = (new StringBuilder()).append(((String) (obj1))).append(".index").toString();
_L17:
        File afile1[];
        int j;
        int l;
        afile1 = file1.listFiles();
        l = afile1.length;
        j = ((flag) ? 1 : 0);
_L12:
        f1 = f;
        if (j >= l) goto _L4; else goto _L9
_L9:
        obj1 = afile1[j];
        if (obj == null || !((String) (obj)).equalsIgnoreCase(((File) (obj1)).getName())) goto _L11; else goto _L10
_L10:
        f1 = f;
_L15:
        j++;
        f = f1;
          goto _L12
_L11:
        if (s == null) goto _L14; else goto _L13
_L13:
        f1 = f;
        if (s.equalsIgnoreCase(((File) (obj1)).getName())) goto _L15; else goto _L14
_L14:
        f1 = f + (float)((File) (obj1)).length();
          goto _L15
_L4:
        f = f1 / 1024F / 1024F;
_L2:
        return (long)f;
_L7:
        obj = null;
        if (true) goto _L17; else goto _L16
_L16:
    }

    public static final String getCurrentDownloadLocation()
    {
        String s;
        if (MyApplication.b() == null)
        {
            s = null;
        } else
        {
            String s1 = SharedPreferencesUtil.getInstance(MyApplication.b()).getString("download_location");
            s = s1;
            if (!isDirAvaliable(s1))
            {
                return setDefaultDownloadLocation();
            }
        }
        return s;
    }

    public static final String getDownloadDir(String s)
    {
        if (!Utilities.isBlank(s))
        {
            if (s.contains("/ting") || s.contains("/ting/download"))
            {
                return s;
            }
            if (Utilities.isNotBlank(internalSDcardPath) && internalSDcardPath.equals(s))
            {
                return internalDownloadPath;
            }
            if (externalSDcardPaths != null && externalSDcardPaths.size() > 0 && externalSDcardPaths.contains(s))
            {
                if (s.endsWith("/"))
                {
                    return s + "ting";
                } else
                {
                    return s + "/" + "ting";
                }
            }
        }
        return null;
    }

    public static final ArrayList getExternalLocation()
    {
        if (isExternalSDcardAvaliable())
        {
            return externalDownloadPaths;
        } else
        {
            return null;
        }
    }

    public static final double getExternalSDcardFreeSize()
    {
        double d1;
        if (isExternalSDcardAvaliable())
        {
            Iterator iterator = externalSDcardPaths.iterator();
            double d = 0.0D;
            do
            {
                d1 = d;
                if (!iterator.hasNext())
                {
                    break;
                }
                d += MemorySpaceUtil.getAvailableMemorySize(new File((String)iterator.next()));
            } while (true);
        } else
        {
            d1 = 0.0D;
        }
        return d1;
    }

    public static final double getFreeStorageSize()
    {
        double d = 0.0D;
        double d1;
        double d2;
        if (isInternalSDcardAvaliable())
        {
            d1 = MemorySpaceUtil.getAvailableMemorySize(new File(internalSDcardPath));
        } else
        {
            d1 = 0.0D;
        }
        d2 = d;
        if (isExternalSDcardAvaliable())
        {
            Iterator iterator = externalSDcardPaths.iterator();
            do
            {
                d2 = d;
                if (!iterator.hasNext())
                {
                    break;
                }
                d += MemorySpaceUtil.getAvailableMemorySize(new File((String)iterator.next()));
            } while (true);
        }
        return d1 + d2;
    }

    public static final String getInternalLocation()
    {
        if (isInternalSDcardAvaliable())
        {
            return a.aj;
        } else
        {
            return null;
        }
    }

    public static final double getInternalSDcardFreeSize()
    {
        double d = 0.0D;
        if (isInternalSDcardAvaliable())
        {
            d = MemorySpaceUtil.getAvailableMemorySize(new File(internalSDcardPath));
        }
        return d;
    }

    public static String getRingtoneDir()
    {
label0:
        {
            if (isInternalSDcardAvaliable())
            {
                return a.ak;
            }
            if (!isExternalSDcardAvaliable())
            {
                break label0;
            }
            Iterator iterator = externalRingtonePaths.iterator();
            String s;
            do
            {
                if (!iterator.hasNext())
                {
                    break label0;
                }
                s = (String)iterator.next();
            } while (!isDirAvaliable(s));
            return s;
        }
        return null;
    }

    public static final long getTotalDownloadSize()
    {
        double d = 0.0D;
        double d1;
        double d2;
        long l;
        if (isInternalSDcardAvaliable())
        {
            d1 = MyFileUtils.getDirSize(new File(a.aj));
        } else
        {
            d1 = 0.0D;
        }
        d2 = d;
        if (isExternalSDcardAvaliable())
        {
            Iterator iterator = externalDownloadPaths.iterator();
            do
            {
                d2 = d;
                if (!iterator.hasNext())
                {
                    break;
                }
                d += MyFileUtils.getDirSize(new File((String)iterator.next()));
            } while (true);
        }
        l = (long)(d1 + d2);
        Session.getSession().put("TOTAL_DOWNLOAD_SIZE", Long.valueOf(l));
        return l;
    }

    public static final double getTotalStorageSize()
    {
        double d1;
        double d2;
        if (isInternalSDcardAvaliable())
        {
            double d = MemorySpaceUtil.getTotalMemorySize(new File(internalSDcardPath));
            Iterator iterator;
            if (d <= 0.0D)
            {
                d = 0.0D;
            }
            d1 = d;
        } else
        {
            d1 = 0.0D;
        }
        if (isExternalSDcardAvaliable())
        {
            iterator = externalSDcardPaths.iterator();
            d = 0.0D;
            do
            {
                d2 = d;
                if (!iterator.hasNext())
                {
                    break;
                }
                d2 = MemorySpaceUtil.getTotalMemorySize(new File((String)iterator.next()));
                if (d2 <= 0.0D)
                {
                    d2 = 0.0D;
                }
                d = d2 + d;
            } while (true);
        } else
        {
            d2 = 0.0D;
        }
        return d1 + d2;
    }

    public static String[] getVolumnPaths(Context context)
    {
        context = (StorageManager)context.getSystemService("storage");
        try
        {
            context = (String[])(String[])context.getClass().getMethod("getVolumePaths", new Class[0]).invoke(context, new Object[0]);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            return null;
        }
        return context;
    }

    public static final boolean isDirAvaliable(String s)
    {
        if (Utilities.isNotBlank(s))
        {
            s = new File(s);
            if (s.exists() && s.isDirectory() && s.canRead())
            {
                return true;
            }
        }
        return false;
    }

    public static final boolean isExternalSDcard(File file)
    {
        if (!isExternalSDcardAvaliable()) goto _L2; else goto _L1
_L1:
        Iterator iterator = externalDownloadPaths.iterator();
_L3:
        String s;
        if (!iterator.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        s = (String)iterator.next();
        boolean flag = s.equals(file.getCanonicalPath());
        if (flag)
        {
            return true;
        }
        continue; /* Loop/switch isn't completed */
        IOException ioexception;
        ioexception;
        ioexception.printStackTrace();
        if (true) goto _L3; else goto _L2
_L2:
        return false;
    }

    public static final boolean isExternalSDcardAvaliable()
    {
        return externalDownloadPaths.size() > 0;
    }

    public static final boolean isFileAvaliable(File file)
    {
        return file != null && file.isFile() && file.exists();
    }

    public static final boolean isFileAvaliable(String s)
    {
        if (Utilities.isNotBlank(s))
        {
            s = new File(s);
            if (s.isFile() && s.exists())
            {
                return true;
            }
        }
        return false;
    }

    public static final boolean isInternalSDcard(File file)
    {
label0:
        {
            boolean flag1 = false;
            boolean flag = flag1;
            boolean flag2;
            try
            {
                if (!isInternalSDcardAvaliable())
                {
                    break label0;
                }
                flag2 = file.getCanonicalPath().equals(Environment.getExternalStorageDirectory().getCanonicalPath());
            }
            // Misplaced declaration of an exception variable
            catch (File file)
            {
                file.printStackTrace();
                return false;
            }
            flag = flag1;
            if (flag2)
            {
                flag = true;
            }
        }
        return flag;
    }

    public static final boolean isInternalSDcardAvaliable()
    {
        if (Environment.getExternalStorageState().equals("mounted"))
        {
            if (Utilities.isBlank(internalSDcardPath))
            {
                internalSDcardPath = Environment.getExternalStorageDirectory().getPath();
                internalDownloadPath = a.aj;
                internalRingtonesPath = a.ak;
            }
            return true;
        } else
        {
            return false;
        }
    }

    private static final ArrayList readExternalLocations()
    {
        ArrayList arraylist = new ArrayList();
        int i = 0;
        while (i < paths.length) 
        {
            if (!Environment.getExternalStorageState().equals("mounted") || !paths[i].equals(Environment.getExternalStorageDirectory().getAbsolutePath()))
            {
                String s;
                if (paths[i].endsWith("/"))
                {
                    s = paths[i] + "ting";
                } else
                {
                    s = paths[i] + "/" + "ting";
                }
                arraylist.add(s);
            }
            i++;
        }
        return arraylist;
    }

    private static void readVoldFile(Context context)
    {
        com/ximalaya/ting/android/util/StorageUtils;
        JVM INSTR monitorenter ;
        int i = sVold.size();
        if (i <= 0) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/util/StorageUtils;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (android.os.Build.VERSION.SDK_INT < 19) goto _L4; else goto _L3
_L3:
        Object obj = context.getExternalFilesDirs(null);
_L20:
        if (obj == null) goto _L6; else goto _L5
_L5:
        if (obj.length <= 1) goto _L6; else goto _L7
_L7:
        if (!Environment.getExternalStorageState().equals("mounted")) goto _L9; else goto _L8
_L8:
        String s;
        s = Environment.getExternalStorageDirectory().getPath();
        if (!sVold.contains(s))
        {
            sVold.add(s);
        }
_L19:
        int j = obj.length;
        i = 0;
_L21:
        if (i >= j) goto _L11; else goto _L10
_L10:
        Object obj1 = obj[i];
        if (TextUtils.isEmpty(s))
        {
            break MISSING_BLOCK_LABEL_131;
        }
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_438;
        }
        if (((File) (obj1)).getPath() == null || ((File) (obj1)).getPath().contains(s))
        {
            break MISSING_BLOCK_LABEL_438;
        }
        sVold.add(((File) (obj1)).getPath());
        break MISSING_BLOCK_LABEL_438;
_L6:
        if (Environment.getExternalStorageState().equals("mounted"))
        {
            obj = Environment.getExternalStorageDirectory().getPath();
            if (!sVold.contains(obj))
            {
                sVold.add(obj);
            }
        }
        obj1 = new Scanner(new File("/system/etc/vold.fstab"));
_L14:
        if (!((Scanner) (obj1)).hasNext()) goto _L11; else goto _L12
_L12:
        obj = ((Scanner) (obj1)).nextLine().trim();
        if (!((String) (obj)).startsWith("dev_mount")) goto _L14; else goto _L13
_L13:
        s = ((String) (obj)).split(" ")[2];
        obj = s;
        if (s.contains(":"))
        {
            obj = s.substring(0, s.indexOf(":"));
        }
        if (!((String) (obj)).contains("usb") && !sVold.contains(((String) (obj)).trim()))
        {
            obj = (new StringBuilder()).append(((String) (obj))).append(pathForKITKAT).toString();
            sVold.add(obj);
        }
          goto _L14
        obj;
        ((Exception) (obj)).printStackTrace();
_L11:
        context = getVolumnPaths(context);
        if (context == null) goto _L1; else goto _L15
_L15:
        j = context.length;
        i = 0;
_L18:
        if (i >= j) goto _L1; else goto _L16
_L16:
        obj = context[i];
        s = (new StringBuilder()).append(((String) (obj))).append(pathForKITKAT).toString();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_410;
        }
        if (!((String) (obj)).contains("usb") && !sVold.contains(obj) && !sVold.contains(s))
        {
            sVold.add(s);
        }
        i++;
        if (true) goto _L18; else goto _L17
_L17:
          goto _L1
        context;
        throw context;
_L9:
        s = null;
          goto _L19
_L4:
        obj = null;
          goto _L20
        i++;
          goto _L21
    }

    public static final String setDefaultDownloadLocation()
    {
label0:
        {
            if (isInternalSDcardAvaliable())
            {
                setDownloadLocation(a.aj);
                return a.aj;
            }
            if (!isExternalSDcardAvaliable())
            {
                break label0;
            }
            Iterator iterator = externalDownloadPaths.iterator();
            String s;
            do
            {
                if (!iterator.hasNext())
                {
                    break label0;
                }
                s = (String)iterator.next();
            } while (!isDirAvaliable(s));
            setDownloadLocation(s);
            return s;
        }
        return null;
    }

    public static final void setDownloadLocation(String s)
    {
        if (!Utilities.isBlank(s))
        {
            File file = new File(s);
            file.mkdirs();
            if (file.isDirectory() && MyApplication.b() != null)
            {
                SharedPreferencesUtil.getInstance(MyApplication.b()).saveString("download_location", s);
                return;
            }
        }
    }

    private static void setProperties()
    {
        boolean flag = false;
        ArrayList arraylist = new ArrayList();
        if (sVold.size() > 0)
        {
            int i = 0;
            while (i < sVold.size()) 
            {
                String s = (new StringBuffer()).toString();
                if (isInternalSDcard(new File((String)sVold.get(i))))
                {
                    s = (new StringBuilder()).append(s).append("\u624B\u673A").toString();
                } else
                {
                    s = (new StringBuilder()).append(s).append("\u5185\u5B58\u5361").toString();
                }
                arraylist.add(s);
                i++;
            }
        }
        labels = new String[arraylist.size()];
        arraylist.toArray(labels);
        paths = new String[sVold.size()];
        sVold.toArray(paths);
        count = Math.min(labels.length, paths.length);
        externalSDcardPaths.clear();
        externalDownloadPaths.clear();
        externalRingtonePaths.clear();
        int j = 0;
        while (j < paths.length) 
        {
            if (!Environment.getExternalStorageState().equals("mounted") || !paths[j].equals(Environment.getExternalStorageDirectory().getAbsolutePath()))
            {
                String s1;
                String s2;
                if (paths[j].endsWith("/"))
                {
                    s2 = paths[j] + "ting";
                    s1 = paths[j] + "ting/ringtones";
                } else
                {
                    s2 = paths[j] + "/" + "ting";
                    s1 = paths[j] + "/" + "ting/ringtones";
                }
                externalSDcardPaths.add(paths[j]);
                externalDownloadPaths.add(s2);
                externalRingtonePaths.add(s1);
            }
            j++;
        }
        if (isInternalSDcardAvaliable())
        {
            internalSDcardPath = Environment.getExternalStorageDirectory().getPath();
            internalDownloadPath = a.aj;
            internalRingtonesPath = a.ak;
        }
        String as[] = paths;
        int l = as.length;
        for (int k = ((flag) ? 1 : 0); k < l; k++)
        {
            String s3 = as[k];
            Logger.log("print_path", (new StringBuilder()).append("====paths==").append(s3).toString());
        }

        Logger.log("print_path", (new StringBuilder()).append("====internalSDcardPath==").append(internalSDcardPath).toString());
        Logger.log("print_path", (new StringBuilder()).append("====internalDownloadPaths==").append(getInternalLocation()).toString());
        String s4;
        for (Iterator iterator = externalSDcardPaths.iterator(); iterator.hasNext(); Logger.log("print_path", (new StringBuilder()).append("====externalSDcardPaths==").append(s4).toString()))
        {
            s4 = (String)iterator.next();
        }

        String s5;
        for (Iterator iterator1 = externalDownloadPaths.iterator(); iterator1.hasNext(); Logger.log("print_path", (new StringBuilder()).append("====externalDownloadPaths==").append(s5).toString()))
        {
            s5 = (String)iterator1.next();
        }

        sVold.clear();
    }

    private static void testAndCleanList()
    {
        int j;
        for (int i = 0; i < sVold.size(); i = j + 1)
        {
            File file = new File((String)sVold.get(i));
            file.mkdirs();
            if (file.exists() && file.isDirectory())
            {
                j = i;
                if (file.canWrite())
                {
                    continue;
                }
            }
            sVold.remove(i);
            j = i - 1;
        }

    }

    public final void codingOutlines()
    {
    }

}
