// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Session
{

    public static final String RECORDINGMODEL = "RECORDINGMODEL";
    private static Session session;
    private Map _objectContainer;
    public volatile boolean isHadNewRecording;

    private Session()
    {
        isHadNewRecording = false;
        _objectContainer = new ConcurrentHashMap();
    }

    public static Session getSession()
    {
        if (session == null)
        {
            session = new Session();
        }
        return session;
    }

    public void cleanUpSession()
    {
        _objectContainer.clear();
    }

    public boolean containsKey(Object obj)
    {
        return _objectContainer.containsKey(obj);
    }

    public Object get(Object obj)
    {
        return _objectContainer.get(obj);
    }

    public void put(Object obj, Object obj1)
    {
        if (obj1 != null)
        {
            _objectContainer.put(obj, obj1);
        }
    }

    public Object remove(Object obj)
    {
        return _objectContainer.remove(obj);
    }
}
