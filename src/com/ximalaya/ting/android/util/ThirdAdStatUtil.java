// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.ad.AdCollectData;
import com.ximalaya.ting.android.modelnew.FocusImageModelNew;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Referenced classes of package com.ximalaya.ting.android.util:
//            Logger, aw, ax, at, 
//            ToolUtil, DataCollectUtil, ay, au, 
//            av

public class ThirdAdStatUtil
{
    public static interface Callback
    {

        public abstract void execute(String s);
    }


    private static final String FIRST_OPEN_TIME = "ad_first_open_time";
    private static ThirdAdStatUtil mInstance;
    private Map adFields;
    private boolean isInit;
    private Context mContext;
    private MyApplication mMyApplication;

    private ThirdAdStatUtil()
    {
        isInit = false;
        adFields = new HashMap();
    }

    private String changeJtParams(String s)
        throws UnsupportedEncodingException
    {
        int j = s.indexOf("&jt=");
        int i = j;
        if (j < 0)
        {
            i = s.indexOf("?jt=");
        }
        Object obj = s;
        if (i >= 0)
        {
            obj = new StringBuffer();
            ((StringBuffer) (obj)).append(s.substring(0, i + 4));
            ((StringBuffer) (obj)).append(URLEncoder.encode(changeParams(URLDecoder.decode(s.substring(i + 4), "utf-8")), "utf-8"));
            obj = ((StringBuffer) (obj)).toString();
        }
        return ((String) (obj));
    }

    private String changeParams(String s)
    {
        Logger.log((new StringBuilder()).append("ThirdAd changeParams src url = ").append(s).toString());
        if (TextUtils.isEmpty(s))
        {
            return null;
        }
        Iterator iterator = adFields.keySet().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            String s1 = (String)iterator.next();
            String s2 = (String)adFields.get(s1);
            if (s1 != null && s2 != null)
            {
                s = s.replace((new StringBuilder()).append("{").append(s1).append("}").toString(), s2);
            }
        } while (true);
        s = Pattern.compile("\\{\\w*\\}").matcher(s).replaceAll("");
        s = Pattern.compile("\\[\\w*\\]").matcher(s).replaceAll("").replace(" ", "");
        Logger.log((new StringBuilder()).append("ThirdAd changeParams dst url = ").append(s).toString());
        return s;
    }

    private String getFirstInTime()
    {
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mContext);
        String s = sharedpreferencesutil.getString("ad_first_open_time");
        Object obj = s;
        if (TextUtils.isEmpty(s))
        {
            obj = new Date();
            obj = (new SimpleDateFormat("yyyyMMddHHmmss")).format(((Date) (obj)));
            sharedpreferencesutil.saveString("ad_first_open_time", ((String) (obj)));
        }
        return ((String) (obj));
    }

    public static ThirdAdStatUtil getInstance()
    {
        if (mInstance != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/util/ThirdAdStatUtil;
        JVM INSTR monitorenter ;
        if (mInstance == null)
        {
            mInstance = new ThirdAdStatUtil();
        }
        com/ximalaya/ting/android/util/ThirdAdStatUtil;
        JVM INSTR monitorexit ;
_L2:
        return mInstance;
        Exception exception;
        exception;
        com/ximalaya/ting/android/util/ThirdAdStatUtil;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private String getRealLink(String s, MyApplication myapplication)
    {
        if (myapplication == null)
        {
            return s;
        }
        StringBuffer stringbuffer = new StringBuffer(s);
        if (s.contains("?"))
        {
            stringbuffer.append("&appid=0&device=android&android_id=");
        } else
        {
            stringbuffer.append("?appid=0&device=android&android_id=");
        }
        try
        {
            stringbuffer.append(android.provider.Settings.Secure.getString(myapplication.getContentResolver(), "android_id"));
        }
        // Misplaced declaration of an exception variable
        catch (String s) { }
        s = (TelephonyManager)myapplication.getSystemService("phone");
        if (s != null)
        {
            s = s.getDeviceId();
            stringbuffer.append("&native_device_id=");
            stringbuffer.append(s);
        }
        return stringbuffer.toString();
    }

    private void initShowTime()
    {
        long l = System.currentTimeMillis();
        Object obj = new Date(l);
        obj = (new SimpleDateFormat("yyyyMMddHHmmss")).format(((Date) (obj)));
        adFields.put("TS", (new StringBuilder()).append("").append(l).toString());
        adFields.put("CLICKTIME", obj);
    }

    public void execAfterDecorateUrl(String s, Callback callback)
    {
        execAfterDecorateUrl(false, s, callback);
    }

    public void execAfterDecorateUrl(boolean flag, String s, Callback callback)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        } else
        {
            (new aw(this, s, flag, callback)).start();
            return;
        }
    }

    public void execAfterDecorateUrlNoJT(String s, Callback callback)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        } else
        {
            (new ax(this, s, callback)).start();
            return;
        }
    }

    public void init(Context context)
    {
        if (isInit)
        {
            return;
        }
        mContext = context;
        if (context != null)
        {
            mMyApplication = (MyApplication)context.getApplicationContext();
        } else
        {
            mMyApplication = (MyApplication)MyApplication.b();
        }
        (new at(this, context)).start();
    }

    public void statFocusAd(List list, boolean flag)
    {
        if (list != null && list.size() > 0)
        {
            list = list.iterator();
            do
            {
                if (!list.hasNext())
                {
                    break;
                }
                FocusImageModelNew focusimagemodelnew = (FocusImageModelNew)list.next();
                if (focusimagemodelnew.type == 10 && !TextUtils.isEmpty(focusimagemodelnew.third_url))
                {
                    thirdAdStatRequest(focusimagemodelnew.third_url);
                }
                if (focusimagemodelnew.type == 10)
                {
                    AdCollectData adcollectdata = new AdCollectData();
                    adcollectdata.setAdSource("0");
                    adcollectdata.setAndroidId(ToolUtil.getAndroidId(mMyApplication));
                    adcollectdata.setLogType("tingShow");
                    if (flag)
                    {
                        adcollectdata.setPositionName("focus_map");
                    } else
                    {
                        adcollectdata.setPositionName("focus_map");
                    }
                    adcollectdata.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                    adcollectdata.setTrackId("-1");
                    DataCollectUtil.getInstance(mMyApplication).statOnlineAd(adcollectdata);
                }
            } while (true);
        }
    }

    public void statSoundAd(long l)
    {
        (new ay(this, l)).myexec(new Void[0]);
    }

    public void thirdAdStatRequest(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        } else
        {
            (new au(this, s)).start();
            return;
        }
    }

    public void thirdLoadingAdStatRequest(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        } else
        {
            (new av(this, s)).start();
            return;
        }
    }






/*
    static boolean access$402(ThirdAdStatUtil thirdadstatutil, boolean flag)
    {
        thirdadstatutil.isInit = flag;
        return flag;
    }

*/




}
