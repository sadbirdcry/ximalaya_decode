// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;


public class EventStatisticsIds
{

    public static final String CLICK_ALBUM_TAB_CLASSIC = "CLICK_ALBUM_TAB_CLASSIC";
    public static final String CLICK_ALBUM_TAB_NEW = "CLICK_ALBUM_TAB_NEW";
    public static final String CLICK_BOOK_TYPE_ALL = "CLICK_BOOK_TYPE_ALL";
    public static final String CLICK_BOOK_TYPE_FINISHED = "CLICK_BOOK_TYPE_FINISHED";
    public static final String CLICK_BOOK_TYPE_WRITTING = "CLICK_BOOK_TYPE_WRITTING";
    public static final String CLICK_FIND_SUBJECT_MORE = "CLICK_FIND_SUBJECT_MORE";
    public static final String CLICK_HOT_SOUND_TAB_LIKED = "CLICK_HOT_SOUND_TAB_LIKED";
    public static final String CLICK_HOT_SOUND_TAB_NEW = "CLICK_HOT_SOUND_TAB_NEW";
    public static final String CLICK_PERSON_STATION_LIST_TAB_NEW = "CLICK_PERSON_STATION_LIST_TAB_NEW";
    public static final String CLICK_SEARCH_ALBUM = "CLICK_SEARCH_ALBUM";
    public static final String CLICK_SEARCH_ALBUM_NEW = "CLICK_SEARCH_ALBUM_NEW";
    public static final String CLICK_SEARCH_SOUND = "CLICK_SEARCH_SOUND";
    public static final String CLICK_SEARCH_SOUND_NEW = "CLICK_SEARCH_SOUND_NEW";
    public static final String CLICK_SEARCH_SOUND_PLAY = "CLICK_SEARCH_SOUND_PLAY";
    public static final String CLICK_SEARCH_USER = "CLICK_SEARCH_USER";
    public static final String CLICK_SEARCH_USER_FUNS = "CLICK_SEARCH_USER_FUNS";
    public static final String CLICK_SEARCH_USER_SOUNDS = "CLICK_SEARCH_USER_SOUNDS";
    public static final String COLLECTION_ATTENTION = "Collection_Attention";
    public static final String COLLECTION_RECOMMEND = "Collection_Recommend";
    public static final String COLLECTION_RECOMMEND_FOLLOW = "Collection_Recommend_Follow";
    public static final String FEED_ATTENTION = "Feed_Attention";
    public static final String FEED_DOWNLOAD = "Feed_Download";
    public static final String FEED_RECOMMEND = "Feed_Recommend";
    public static final String FEED_RECOMMEND_FOLLOW = "Feed_Recommend_Follow";
    public static final String FEED_SET_BLOCKED = "Feed_Set_Blocked";
    public static final String FEED_SET_TOP = "Feed_Set_Top";
    public static final String FOUND_ZONE = "Found_Zone";
    public static final String INVITE_SHARE_INVITE_NOW = "INVITE_SHARE_INVITE_NOW";
    public static final String INVITE_SHARE_NEXT_TIME = "INVITE_SHARE_NEXT_TIME";
    public static final String LANRENRADIO_CLICK_SOUND_ITEM = "LanRenRadio_SoundList_Item";
    public static final String LANREN_RADIO_ANCHORS_NULL = "LanRenRadio_AnchorList_Null";
    public static final String LANREN_RADIO_ANCHOR_DETAIL = "LanRenRadio_AnchorDetails";
    public static final String LANREN_RADIO_CHANGE_ANCHOR_ACTIVE = "LanRenRadio_ChangeAnchor_Active";
    public static final String LANREN_RADIO_CHANGE_ANCHOR_PASSIVE = "LanRenRadio_ChangeAnchor_Passive";
    public static final String LANREN_RADIO_DELETE = "LanRenRadio_Delete";
    public static final String LANREN_RADIO_DOWNLOAD_SOUND = "LanRenRadio_SoundList_Download";
    public static final String LANREN_RADIO_ENTER = "Found_LanRenRadio";
    public static final String LANREN_RADIO_FOLLOW_UNFOLLOW = "LanRenRadio_Follow_Unfollow";
    public static final String LANREN_RADIO_PLAY_PAUSE = "LanRenRadio_Play_Pause";
    public static final String LANREN_RADIO_SHOW_SOUNDSLIST = "LanRenRadio_SoundList";
    public static final String LANREN_RADIO_USE_DAYS = "LanRenRadio_UseDays";
    public static final String LANREN_RADIO_USE_MORE_THAN_10MIN = "LanRenRadio_Play_MoreThan10min";
    public static final String NETWORKSWITCH_ADS = "NetworkSwitch_Ads";
    public static final String NETWORKSWITCH_ADS_AGREE = "NetworkSwitch_Ads_Agree";
    public static final String NETWORKSWITCH_ADS_CLICK = "NetworkSwitch_Ads_Click";
    public static final String NETWORKSWITCH_ADS_IGNORE = "NetworkSwitch_Ads_Ignore";
    public static final String NETWORKSWITCH_NOADS = "NetworkSwitch_NoAds";
    public static final String NETWORKSWITCH_NOADS_AGREE = "NetworkSwitch_NoAds_Agree";
    public static final String NETWORKSWITCH_NOADS_IGNORE = "NetworkSwitch_NoAds_Ignore";
    public static final String PATH = "PATH";
    public static final String PLAY_BANNER = "play_banner";
    public static final String PLAY_CATEGORY = "play_category";
    public static final String PLAY_DOWNLOAD = "play_download";
    public static final String PLAY_FEED = "play_feed";
    public static final String PLAY_HOT_PROGRAM = "play_hot_program";
    public static final String PLAY_PATH = "PLAY_WAY";
    public static final String PLAY_PERSONAL_RADIO = "play_personal_radio";
    public static final String PLAY_RECOMMENDED_ALBUM = "play_recommended_album";
    public static final String PLAY_SEARCH = "play_search";
    public static final String PLAY_SUBJECTS = "play_subjects";
    public static final String PLAY_SUBSCRIBE = "play_Subscribe";
    public static final String SET_RINGTONE_SUCCESS = "Set_Ringtone_Success";
    public static final String SHARE_CLICK_NOW_PLAYING_BUBBLE = "SHARE_CLICK_NOW_PLAYING_SHARE";
    public static final String SHARE_DOWNLOAD_BACK_POINT = "SHARE_DOWNLOAD_BACK_POINT";
    public static final String SHARE_DOWNLOAD_NO_BACK_POINT = "SHARE_DOWNLOAD_NO_BACK_POINT";
    public static final String SHARE_QQ_FRIEND_SUCCESS = "SHARE_QQ_FRIEND_SUCCESS";
    public static final String SHARE_QZONE_SUCCESS = "SHARE_QZONE_SUCCESS";
    public static final String SHARE_RENREN_SUCCESS = "SHARE_RENREN_SUCCESS";
    public static final String SHARE_T_QQ_SUCCESS = "SHARE_T_QQ_SUCCESS";
    public static final String SHARE_T_SINA_SUCCESS = "SHARE_T_SINA_SUCCESS";
    public static final String SHARE_WEIXIN_FRIEND_SUCCESS = "SHARE_WEIXIN_FRIEND_SUCCESS";
    public static final String SHARE_WEIXIN_GROUP_SUCCESS = "SHARE_WEIXIN_GROUP_SUCCESS";
    public static final String ZONE_HOMEPAGE = "zone_homepage";
    public static final String ZONE_MAIN = "zone_main";
    public static final String ZONE_MINE = "zone_mine";
    public static final String ZONE_POST_CREATE = "zone_post_create";
    public static final String ZONE_POST_MAIN = "zone_post_main";
    public static final String ZONE_POST_MESSAGE = "zone_post_message";
    public static final String ZONE_RECOMMEND = "zone_recommend";

    public EventStatisticsIds()
    {
    }
}
