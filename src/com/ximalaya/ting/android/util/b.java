// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.util:
//            ViewUtil, BitmapUtils, Logger

final class b extends Thread
{

    final List a;
    final boolean b;
    final BitmapUtils.CompressCallback2 c;

    b(List list, boolean flag, BitmapUtils.CompressCallback2 compresscallback2)
    {
        a = list;
        b = flag;
        c = compresscallback2;
        super();
    }

    public void run()
    {
        HashMap hashmap;
        Iterator iterator;
        boolean flag;
        hashmap = new HashMap();
        iterator = a.iterator();
        flag = false;
_L4:
        if (!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        Object obj3;
        String s;
        s = (String)iterator.next();
        obj3 = new File(s);
        if (!((File) (obj3)).exists()) goto _L4; else goto _L3
_L3:
        Object obj = new android.graphics.BitmapFactory.Options();
        obj.inSampleSize = 1;
        obj.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(((File) (obj3)).getAbsolutePath(), ((android.graphics.BitmapFactory.Options) (obj)));
        int i = ((android.graphics.BitmapFactory.Options) (obj)).outHeight * ((android.graphics.BitmapFactory.Options) (obj)).outWidth * 4;
        if (i > 0x493e00)
        {
            obj.inSampleSize = (int)Math.sqrt(i / 4 / 1000 / 1000);
        }
        obj.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFile(((File) (obj3)).getAbsolutePath(), ((android.graphics.BitmapFactory.Options) (obj)));
        i = ViewUtil.getBitmapDegree(((File) (obj3)).getAbsolutePath());
        Object obj1;
        Object obj2;
        if (i != 0)
        {
            obj = new Matrix();
            ((Matrix) (obj)).postRotate(i);
            try
            {
                obj = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), ((Matrix) (obj)), true);
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                obj = null;
            }
        } else
        {
            obj = null;
        }
        if (!b) goto _L6; else goto _L5
_L5:
        if (obj == null)
        {
            obj1 = bitmap;
        } else
        {
            obj1 = obj;
        }
        BitmapUtils.writeBitmapToFile(((Bitmap) (obj1)), ((File) (obj3)).getAbsolutePath(), ((File) (obj3)).getAbsolutePath());
        obj1 = null;
_L7:
        if (!b)
        {
            break MISSING_BLOCK_LABEL_340;
        }
        obj1 = Uri.fromFile(new File(s));
_L8:
        hashmap.put(s, obj1);
        if (bitmap != null && !bitmap.isRecycled())
        {
            bitmap.recycle();
        }
        if (obj != null && !((Bitmap) (obj)).isRecycled())
        {
            bitmap.recycle();
        }
          goto _L4
_L6:
        obj3 = BitmapUtils.access$000(((File) (obj3)).getAbsolutePath());
        if (obj == null)
        {
            obj1 = bitmap;
        } else
        {
            obj1 = obj;
        }
        BitmapUtils.writeBitmapToFile(((Bitmap) (obj1)), ((String) (obj3)), ((String) (obj3)));
        obj1 = obj3;
        flag = true;
          goto _L7
        obj1 = Uri.fromFile(new File(((String) (obj1))));
          goto _L8
        obj2;
_L10:
        Logger.e("ImageManager2.compressImage", "\u627E\u4E0D\u5230\u6307\u5B9A\u6587\u4EF6", ((Throwable) (obj2)));
        if (bitmap != null && !bitmap.isRecycled())
        {
            bitmap.recycle();
        }
        if (obj != null && !((Bitmap) (obj)).isRecycled())
        {
            bitmap.recycle();
        }
          goto _L4
        obj2;
_L9:
        Logger.e("ImageManager2.compressImage", "\u5199\u5165\u6587\u4EF6\u53D1\u751F\u9519\u8BEF", ((Throwable) (obj2)));
        if (bitmap != null && !bitmap.isRecycled())
        {
            bitmap.recycle();
        }
        if (obj != null && !((Bitmap) (obj)).isRecycled())
        {
            bitmap.recycle();
        }
          goto _L4
        obj2;
        if (bitmap != null && !bitmap.isRecycled())
        {
            bitmap.recycle();
        }
        if (obj != null && !((Bitmap) (obj)).isRecycled())
        {
            bitmap.recycle();
        }
        throw obj2;
_L2:
        c.onFinished(hashmap, flag);
        return;
        obj2;
          goto _L9
        obj2;
          goto _L10
    }
}
