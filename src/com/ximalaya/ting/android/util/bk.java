// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.graphics.Rect;
import android.view.TouchDelegate;
import android.view.View;

// Referenced classes of package com.ximalaya.ting.android.util:
//            ToolUtil

final class bk
    implements Runnable
{

    final Context a;
    final View b;
    final int c;
    final int d;
    final int e;
    final int f;

    bk(Context context, View view, int i, int j, int k, int l)
    {
        a = context;
        b = view;
        c = i;
        d = j;
        e = k;
        f = l;
        super();
    }

    public void run()
    {
        if (a != null)
        {
            Object obj = new Rect();
            View view = b;
            view.getHitRect(((Rect) (obj)));
            obj.top = ((Rect) (obj)).top - ToolUtil.dp2px(a, c);
            obj.bottom = ((Rect) (obj)).bottom + ToolUtil.dp2px(a, d);
            obj.right = ((Rect) (obj)).right + ToolUtil.dp2px(a, e);
            obj.left = ((Rect) (obj)).left - ToolUtil.dp2px(a, f);
            obj = new TouchDelegate(((Rect) (obj)), view);
            if (android/view/View.isInstance(view.getParent()))
            {
                ((View)view.getParent()).setTouchDelegate(((TouchDelegate) (obj)));
                return;
            }
        }
    }
}
