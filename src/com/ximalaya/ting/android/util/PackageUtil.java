// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import java.io.IOException;

// Referenced classes of package com.ximalaya.ting.android.util:
//            BuildProperties

public final class PackageUtil
{

    private static final String KEY_MIUI_INTERNAL_STORAGE = "ro.miui.internal.storage";
    private static final String KEY_MIUI_VERSION_CODE = "ro.miui.ui.version.code";
    private static final String KEY_MIUI_VERSION_NAME = "ro.miui.ui.version.name";
    public static final String PACKAGE_360_WEISHI = "com.qihoo360.mobilesafe";
    public static final String PACKAGE_BAIDU_WEISHI = "cn.opda.a.phonoalbumshoushou";
    public static final String PACKAGE_LBE_ANQUAN = "com.lbe.security";
    public static final String PACKAGE_LIEBAO_DASHI = "com.cleanmaster.mguard_cn";
    public static final String PACKAGE_TENCENT_GUANJIA = "com.tencent.qqpimsecure";

    private PackageUtil()
    {
    }

    public static boolean isAppInstalled(Context context, String s)
    {
        try
        {
            context = context.getPackageManager().getPackageInfo(s, 0);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        return context != null;
    }

    public static boolean isMIUI()
    {
label0:
        {
label1:
            {
                boolean flag = false;
                Object obj;
                try
                {
                    obj = BuildProperties.newInstance();
                    if (((BuildProperties) (obj)).getProperty("ro.miui.ui.version.code", null) != null || ((BuildProperties) (obj)).getProperty("ro.miui.ui.version.name", null) != null)
                    {
                        break label1;
                    }
                    obj = ((BuildProperties) (obj)).getProperty("ro.miui.internal.storage", null);
                }
                catch (IOException ioexception)
                {
                    return false;
                }
                if (obj == null)
                {
                    break label0;
                }
            }
            flag = true;
        }
        return flag;
    }

    public static boolean isMeizu()
    {
        return "Meizu".equals(Build.BRAND);
    }

    public static boolean isPostHB()
    {
        return android.os.Build.VERSION.SDK_INT >= 11;
    }

    public static boolean isPostICS()
    {
        return android.os.Build.VERSION.SDK_INT >= 14;
    }
}
