// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.os.Environment;
import android.os.StatFs;
import java.io.File;

public class MemorySpaceUtil
{

    static final int ERROR = -1;

    public MemorySpaceUtil()
    {
    }

    public static String formatSize(long l)
    {
        String s = null;
        long l1 = l;
        if (l >= 1024L)
        {
            s = "KiB";
            l /= 1024L;
            l1 = l;
            if (l >= 1024L)
            {
                s = "MiB";
                l1 = l / 1024L;
            }
        }
        StringBuilder stringbuilder = new StringBuilder(Long.toString(l1));
        for (int i = stringbuilder.length() - 3; i > 0; i -= 3)
        {
            stringbuilder.insert(i, ',');
        }

        if (s != null)
        {
            stringbuilder.append(s);
        }
        return stringbuilder.toString();
    }

    public static long getAvailableExternalMemorySize()
    {
        if (isExternalMemoryAvailable())
        {
            StatFs statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            long l = statfs.getBlockSize();
            return (long)statfs.getAvailableBlocks() * l;
        } else
        {
            return -1L;
        }
    }

    public static long getAvailableInternalMemorySize()
    {
        StatFs statfs = new StatFs(Environment.getDataDirectory().getPath());
        long l = statfs.getBlockSize();
        return (long)statfs.getAvailableBlocks() * l;
    }

    public static long getAvailableMemorySize(File file)
    {
        if (file != null && file.exists())
        {
            file = new StatFs(file.getPath());
            long l = file.getBlockSize();
            return (long)file.getAvailableBlocks() * l;
        } else
        {
            return -1L;
        }
    }

    public static long getTotalExternalMemorySize()
    {
        if (isExternalMemoryAvailable())
        {
            StatFs statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            long l = statfs.getBlockSize();
            return (long)statfs.getBlockCount() * l;
        } else
        {
            return -1L;
        }
    }

    public static long getTotalInternalMemorySize()
    {
        StatFs statfs = new StatFs(Environment.getDataDirectory().getPath());
        long l = statfs.getBlockSize();
        return (long)statfs.getBlockCount() * l;
    }

    public static long getTotalMemorySize(File file)
    {
        if (file != null && file.exists())
        {
            file = new StatFs(file.getPath());
            long l = file.getBlockSize();
            return (long)file.getBlockCount() * l;
        } else
        {
            return -1L;
        }
    }

    public static boolean isExternalMemoryAvailable()
    {
        return Environment.getExternalStorageState().equals("mounted");
    }
}
