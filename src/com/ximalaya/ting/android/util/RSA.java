// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import com.rt.CharacterDecoder;
import com.rt.CharacterEncoder;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class RSA
{

    private static final Charset UTF8 = Charset.forName("utf8");
    private static ThreadLocal base64DecoderThreadLocal = new ThreadLocal();
    private static ThreadLocal base64EncoderThreadLocal = new ThreadLocal();
    private static KeyFactory keyFactory;
    private int keyLen;
    private int keyLen2;
    private ThreadLocal publicCipherThreadLocal;
    private RSAPublicKey publicKey;

    public RSA()
    {
        publicCipherThreadLocal = new ThreadLocal();
    }

    public RSA(RSAPublicKey rsapublickey)
    {
        publicCipherThreadLocal = new ThreadLocal();
        setPublicKey(rsapublickey);
    }

    private byte[] doFinal(byte abyte0[], int i, int j, Cipher cipher)
    {
        try
        {
            abyte0 = cipher.doFinal(abyte0, i, j);
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            throw new RuntimeException(abyte0);
        }
        return abyte0;
    }

    public static String encryptByPublicKey(String s, RSAPublicKey rsapublickey)
    {
        return (new RSA(rsapublickey)).encryptByPublicKey(s);
    }

    private static CharacterDecoder getBASE64Decoder()
    {
        BASE64Decoder base64decoder1 = (BASE64Decoder)base64DecoderThreadLocal.get();
        BASE64Decoder base64decoder = base64decoder1;
        if (base64decoder1 == null)
        {
            base64decoder = new BASE64Decoder();
            base64DecoderThreadLocal.set(base64decoder);
        }
        return base64decoder;
    }

    private static CharacterEncoder getBASE64Encoder()
    {
        BASE64Encoder base64encoder1 = (BASE64Encoder)base64EncoderThreadLocal.get();
        BASE64Encoder base64encoder = base64encoder1;
        if (base64encoder1 == null)
        {
            base64encoder = new BASE64Encoder();
            base64EncoderThreadLocal.set(base64encoder);
        }
        return base64encoder;
    }

    private Cipher getPublicEncryptCipher()
    {
        Cipher cipher1 = (Cipher)publicCipherThreadLocal.get();
        Cipher cipher = cipher1;
        if (cipher1 == null)
        {
            try
            {
                cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                publicCipherThreadLocal.set(cipher);
                cipher.init(1, publicKey);
            }
            catch (Exception exception)
            {
                throw new RuntimeException(exception);
            }
        }
        return cipher;
    }

    public static RSAPublicKey getPublicKey(String s)
    {
        try
        {
            s = new X509EncodedKeySpec(getBASE64Decoder().decodeBuffer(s));
            s = (RSAPublicKey)keyFactory.generatePublic(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            throw new RuntimeException(s);
        }
        return s;
    }

    public String encryptByPublicKey(String s)
    {
        Cipher cipher = getPublicEncryptCipher();
        StringBuilder stringbuilder = new StringBuilder();
        s = s.getBytes(UTF8);
        int i;
        int j;
        boolean flag;
        if (s.length > keyLen)
        {
            i = keyLen;
        } else
        {
            i = s.length;
        }
        flag = false;
        j = i;
        for (i = ((flag) ? 1 : 0); i < s.length;)
        {
            int k = j;
            if (j > s.length - i)
            {
                k = s.length - i;
            }
            byte abyte0[] = doFinal(s, i, k, cipher);
            stringbuilder.append(getBASE64Encoder().encodeBuffer(abyte0));
            i += keyLen;
            j = k;
        }

        return stringbuilder.toString();
    }

    public void setPublicKey(RSAPublicKey rsapublickey)
    {
        publicKey = rsapublickey;
        keyLen2 = rsapublickey.getModulus().bitLength() / 8;
        keyLen = keyLen2 - 11;
    }

    static 
    {
        try
        {
            keyFactory = KeyFactory.getInstance("RSA");
        }
        catch (NoSuchAlgorithmException nosuchalgorithmexception) { }
    }
}
