// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.content.res.Resources;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.ximalaya.ting.android.MyApplication;
import java.net.URLEncoder;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.util:
//            ThirdAdStatUtil, ToolUtil, MD5, NetworkUtils

class at extends Thread
{

    final Context a;
    final ThirdAdStatUtil b;

    at(ThirdAdStatUtil thirdadstatutil, Context context)
    {
        b = thirdadstatutil;
        a = context;
        super();
    }

    public void run()
    {
        String as[] = a.getResources().getStringArray(0x7f0c002b);
        for (int i = 0; i < as.length; i++)
        {
            ThirdAdStatUtil.access$000(b).put(as[i], "");
        }

        ThirdAdStatUtil.access$000(b).put("OS", ThirdAdStatUtil.access$100(b).h());
        String s = ((TelephonyManager)a.getSystemService("phone")).getDeviceId();
        ThirdAdStatUtil.access$000(b).put("IMEI", s);
        s = ToolUtil.getLocalMacAddress(ThirdAdStatUtil.access$200(b));
        if (!TextUtils.isEmpty(s))
        {
            ThirdAdStatUtil.access$000(b).put("MAC2", s);
            String s1 = MD5.md5(s);
            ThirdAdStatUtil.access$000(b).put("MAC1", s1);
            s = MD5.md5(s.replaceAll(":", ""));
            ThirdAdStatUtil.access$000(b).put("MAC", s);
        }
        s = (new StringBuilder()).append("").append(android.provider.Settings.Secure.getString(ThirdAdStatUtil.access$200(b).getContentResolver(), "android_id")).toString();
        ThirdAdStatUtil.access$000(b).put("AndroidID", MD5.md5(s));
        ThirdAdStatUtil.access$000(b).put("AndroidID1", s);
        s = ToolUtil.getDeviceToken(ThirdAdStatUtil.access$200(b));
        ThirdAdStatUtil.access$000(b).put("OpenUDID", s);
        ThirdAdStatUtil.access$000(b).put("UDID", MD5.md5(s));
        ThirdAdStatUtil.access$000(b).put("IP", NetworkUtils.getPhoneIP(ThirdAdStatUtil.access$200(b)));
        ThirdAdStatUtil.access$000(b).put("UA", ThirdAdStatUtil.access$100(b).l());
        ThirdAdStatUtil.access$000(b).put("OSVS", ToolUtil.getSystemVersion());
        ThirdAdStatUtil.access$000(b).put("TERM", ToolUtil.getPhoneVersion());
        ThirdAdStatUtil.access$000(b).put("APPID", "0");
        s = a.getResources().getString(0x7f090000);
        ThirdAdStatUtil.access$000(b).put("APPNAME", URLEncoder.encode(s));
        ThirdAdStatUtil.access$000(b).put("BSSID", ToolUtil.getLocalMacAddress(ThirdAdStatUtil.access$200(b)));
        ThirdAdStatUtil.access$000(b).put("FIRSTOPENTIME", ThirdAdStatUtil.access$300(b));
        ThirdAdStatUtil.access$402(b, true);
    }
}
