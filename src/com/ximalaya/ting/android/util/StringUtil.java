// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.text.TextUtils;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringUtil
{

    public StringUtil()
    {
    }

    public static String fullWidthToHalfWidth(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return s;
        }
        s = s.toCharArray();
        int i = 0;
        while (i < s.length) 
        {
            if (s[i] == '\u3000')
            {
                s[i] = ' ';
            } else
            if (s[i] >= '\uFF01' && s[i] <= '\uFF5E')
            {
                s[i] = (char)(s[i] - 65248);
            } else
            {
                s[i] = s[i];
            }
            i++;
        }
        return new String(s);
    }

    public static String getFirstTag(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return "";
        }
        s = s.split(",");
        if (s.length > 0)
        {
            return s[0];
        } else
        {
            return "";
        }
    }

    public static String getFriendlyDataStr(long l)
    {
        Date date = new Date(l);
        return (new SimpleDateFormat("yyyy-MM-dd")).format(date);
    }

    public static String getFriendlyNumStr(int i)
    {
        return getFriendlyNumStr(i);
    }

    public static String getFriendlyNumStr(long l)
    {
        if (l < 10000L)
        {
            return String.valueOf(l);
        }
        if (l / 10000L < 10000L)
        {
            return getShortenNum(l / 1000L, "\u4E07");
        } else
        {
            return getShortenNum(l / 10000L / 1000L, "\u4EBF");
        }
    }

    public static String getFriendlyNumStr(String s)
    {
        return getFriendlyNumStr(Long.valueOf(s.trim()).longValue());
    }

    public static String getFriendlyTag(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return "";
        } else
        {
            return s.replace(",", " | ").replace("\uFF0C", " | ");
        }
    }

    private static String getShortenNum(long l, String s)
    {
        if (l % 10L == 0L)
        {
            return (new StringBuilder()).append(String.valueOf(l / 10L)).append(s).toString();
        } else
        {
            String s1 = String.valueOf(l);
            int i = s1.length();
            StringBuilder stringbuilder = new StringBuilder();
            stringbuilder.append(s1.substring(0, i - 1));
            stringbuilder.append(".").append(s1.substring(i - 1, i));
            stringbuilder.append(s);
            return stringbuilder.toString();
        }
    }

    public static String halfWidthToFullWidth(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return s;
        }
        s = s.toCharArray();
        int i = 0;
        while (i < s.length) 
        {
            if (s[i] == ' ')
            {
                s[i] = '\u3000';
            } else
            if (s[i] >= '!' && s[i] <= '~')
            {
                s[i] = (char)(s[i] + 65248);
            } else
            {
                s[i] = s[i];
            }
            i++;
        }
        return new String(s);
    }
}
