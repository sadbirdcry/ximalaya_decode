// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.RemoteViews;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Utils;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.view.RoundedImageView;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;

// Referenced classes of package com.ximalaya.ting.android.util:
//            FreeFlowUtil, aa, ah, ai, 
//            ac, ad, ae, af, 
//            ag, Utilities, ab, MD5

public class ImageManager2
{
    public static interface DisplayCallback
    {

        public abstract void onCompleteDisplay(String s, Bitmap bitmap);
    }

    public static class Options
    {

        public int errorResId;
        public boolean fadeAlways;
        public boolean noPlaceholder;
        public int placeholder;
        public int targetHeight;
        public int targetWidth;

        public Options()
        {
            noPlaceholder = false;
        }
    }


    private static final String DOWNLOAD_CACHE_DIR;
    private static final String TAG = "ImageManager2";
    private static ImageManager2 imageManager;
    private static OkHttpDownloader mOkHttpDownloader;
    private static MyApplication myapp;
    private static Picasso picasso;

    private ImageManager2(Context context)
    {
    }

    static int calculateMemoryCacheSize(Context context)
    {
        context = (ActivityManager)context.getSystemService("activity");
        int i;
        if (context != null)
        {
            if (context.getMemoryClass() / 7 > 5)
            {
                i = 5;
            } else
            {
                i = context.getMemoryClass() / 7;
            }
        } else
        {
            i = 4;
        }
        return i * 0x100000;
    }

    public static ImageManager2 from(Context context)
    {
        initContext(context);
        if (imageManager != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/util/ImageManager2;
        JVM INSTR monitorenter ;
        Object obj;
        OkHttpClient okhttpclient;
        imageManager = new ImageManager2(myapp);
        mOkHttpDownloader = null;
        okhttpclient = new OkHttpClient();
        obj = Utils.createDefaultCacheDir(myapp);
        okhttpclient.setCache(new Cache(((File) (obj)), Utils.calculateDiskCacheSize(((File) (obj)))));
_L5:
        if (!FreeFlowUtil.getInstance().isNeedFreeFlowProxy()) goto _L4; else goto _L3
_L3:
        if (FreeFlowUtil.getNetType() != 2)
        {
            break MISSING_BLOCK_LABEL_212;
        }
        obj = FreeFlowUtil.NET_PROXY_HOST;
_L6:
        okhttpclient.setProxy(new Proxy(java.net.Proxy.Type.HTTP, new InetSocketAddress(((String) (obj)), 8080)));
_L4:
        mOkHttpDownloader = new aa(okhttpclient);
        if (!(new File(DOWNLOAD_CACHE_DIR)).exists())
        {
            (new File(DOWNLOAD_CACHE_DIR)).mkdirs();
        }
        picasso = (new com.squareup.picasso.Picasso.Builder(myapp)).memoryCache(new LruCache(calculateMemoryCacheSize(context))).permanentDiskCache(DOWNLOAD_CACHE_DIR).downloader(mOkHttpDownloader).build();
        com/ximalaya/ting/android/util/ImageManager2;
        JVM INSTR monitorexit ;
_L2:
        return imageManager;
        obj;
        ((IOException) (obj)).printStackTrace();
          goto _L5
        context;
        com/ximalaya/ting/android/util/ImageManager2;
        JVM INSTR monitorexit ;
        throw context;
        obj = FreeFlowUtil.WAP_PROXY_HOST;
          goto _L6
    }

    private static void initContext(Context context)
    {
label0:
        {
            if (myapp == null)
            {
                if (context == null)
                {
                    break label0;
                }
                myapp = (MyApplication)context.getApplicationContext();
            }
            return;
        }
        myapp = (MyApplication)MyApplication.b();
    }

    public static boolean isBlurImageView(ImageView imageview)
    {
        if (imageview == null)
        {
            return false;
        }
        imageview = ((ImageView) (imageview.getTag(0x7f0a003b)));
        boolean flag;
        if (imageview != null && ((Boolean)imageview).booleanValue())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        return flag;
    }

    private byte[] loadByteArrayFromNetwork(String s)
    {
        byte abyte0[] = null;
        s = new HttpGet(s);
        byte abyte1[] = EntityUtils.toByteArray(myapp.j().execute(s).getEntity());
        abyte0 = abyte1;
        if (s != null)
        {
            s.abort();
            abyte0 = abyte1;
        }
_L2:
        return abyte0;
        s;
        s = null;
_L5:
        if (s == null) goto _L2; else goto _L1
_L1:
        s.abort();
        return null;
        s;
        Object obj;
        String s1;
        s1 = null;
        obj = s;
_L4:
        if (s1 != null)
        {
            s1.abort();
        }
        throw obj;
        obj;
        s1 = s;
        if (true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
          goto _L5
    }

    public void clear()
    {
        Picasso.with(myapp).clearMemoryCache();
    }

    public void deleteBitmapFromDownloadCache(String s)
    {
        picasso.removePermanentDiskCache(s);
    }

    public void displayImage(ImageView imageview, String s, int i)
    {
        displayImage(imageview, s, i, false);
    }

    public void displayImage(ImageView imageview, String s, int i, int j, int k)
    {
        displayImage(imageview, s, i, j, k, false);
    }

    public void displayImage(ImageView imageview, String s, int i, int j, int k, boolean flag)
    {
        displayImage(imageview, s, i, j, k, flag, null);
    }

    public void displayImage(ImageView imageview, String s, int i, int j, int k, boolean flag, DisplayCallback displaycallback)
    {
        RequestCreator requestcreator;
        android.widget.ImageView.ScaleType scaletype;
        if (TextUtils.isEmpty(s))
        {
            if (i != -1)
            {
                imageview.setImageDrawable(myapp.getResources().getDrawable(i));
            }
            return;
        }
        requestcreator = picasso.load(s);
        boolean flag1 = true;
        if (imageview.getTag(0x7f0a003a) != null)
        {
            flag1 = ((Boolean)imageview.getTag(0x7f0a003a)).booleanValue();
        }
        if (!flag1 || (imageview instanceof RoundedImageView))
        {
            requestcreator.noFade();
        }
        if (i != -1)
        {
            requestcreator.placeholder(i);
        }
        requestcreator.tag("ImageManager2");
        requestcreator.savePermanent(flag);
        if (isBlurImageView(imageview))
        {
            if (imageview.getTag(0x7f0a0024) == null)
            {
                i = -50;
            } else
            {
                i = ((Integer)imageview.getTag(0x7f0a0024)).intValue();
            }
            requestcreator.transform(new ah(this, (new StringBuilder()).append(s).append("/blur").toString(), i));
        }
        if (j == 0 || k == 0) goto _L2; else goto _L1
_L1:
        requestcreator.resize(j, k);
        scaletype = imageview.getScaleType();
        if (scaletype != android.widget.ImageView.ScaleType.CENTER_CROP) goto _L4; else goto _L3
_L3:
        requestcreator.centerCrop();
_L2:
        requestcreator.into(imageview, new ai(this, displaycallback, s));
        imageview.setTag(0x7f0a0031, s);
        return;
_L4:
        if (scaletype == android.widget.ImageView.ScaleType.CENTER_INSIDE)
        {
            requestcreator.centerInside();
        }
        if (true) goto _L2; else goto _L5
_L5:
    }

    public void displayImage(ImageView imageview, String s, int i, DisplayCallback displaycallback)
    {
        displayImage(imageview, s, i, false, false, displaycallback);
    }

    public void displayImage(ImageView imageview, String s, int i, boolean flag)
    {
        displayImage(imageview, s, i, flag, false, ((DisplayCallback) (null)));
    }

    public void displayImage(ImageView imageview, String s, int i, boolean flag, boolean flag1, DisplayCallback displaycallback)
    {
        if (TextUtils.isEmpty(s) || "".equals(s.trim()))
        {
            if (i != -1)
            {
                imageview.setImageDrawable(myapp.getResources().getDrawable(i));
            }
            return;
        }
        RequestCreator requestcreator = picasso.load(s);
        flag1 = true;
        if (imageview.getTag(0x7f0a003a) != null)
        {
            flag1 = ((Boolean)imageview.getTag(0x7f0a003a)).booleanValue();
        }
        if (!flag1 || (imageview instanceof RoundedImageView))
        {
            requestcreator.noFade();
        }
        if (i != -1)
        {
            requestcreator.placeholder(i);
        } else
        {
            requestcreator.noPlaceholder();
        }
        requestcreator.savePermanent(flag);
        if (isBlurImageView(imageview))
        {
            if (imageview.getTag(0x7f0a0024) == null)
            {
                i = -50;
            } else
            {
                i = ((Integer)imageview.getTag(0x7f0a0024)).intValue();
            }
            requestcreator.transform(new ac(this, (new StringBuilder()).append(s).append("/blur").toString(), i));
        }
        requestcreator.tag("ImageManager2");
        requestcreator.into(imageview, new ad(this, displaycallback, s));
        imageview.setTag(0x7f0a0031, s);
    }

    public void displayImage(ImageView imageview, String s, Drawable drawable, int i, int j)
    {
        if (TextUtils.isEmpty(s))
        {
            if (drawable != null)
            {
                imageview.setImageDrawable(drawable);
            }
            return;
        } else
        {
            picasso.load(s).placeholder(drawable).resize(i, j).into(imageview);
            imageview.setTag(0x7f0a0031, s);
            return;
        }
    }

    public void displayImage(ImageView imageview, String s, Options options, DisplayCallback displaycallback)
    {
        RequestCreator requestcreator = picasso.load(s).fadeAlways(options.fadeAlways);
        if (options.placeholder > 0)
        {
            requestcreator.placeholder(options.placeholder);
        }
        if (options.targetWidth > 0 || options.targetHeight > 0)
        {
            requestcreator.resize(options.targetWidth, options.targetHeight);
        }
        if (options.errorResId > 0)
        {
            requestcreator.error(options.errorResId);
        }
        if (options.noPlaceholder)
        {
            requestcreator.noPlaceholder();
        }
        requestcreator.tag("ImageManager2");
        requestcreator.fadeAlways(options.fadeAlways);
        requestcreator.into(imageview, new ae(this, displaycallback, s));
        imageview.setTag(0x7f0a0031, s);
    }

    public void displayImage(RemoteViews remoteviews, int i, String s, int j, int k, int l, int i1, 
            DisplayCallback displaycallback)
    {
        if (TextUtils.isEmpty(s))
        {
            if (j != 0)
            {
                remoteviews.setImageViewResource(i, j);
            }
            return;
        }
        RequestCreator requestcreator = picasso.load(s);
        if (k != 0 && k != 0)
        {
            requestcreator.resize(k, k);
        } else
        {
            requestcreator.resize(Utilities.dip2px(myapp, 120F), Utilities.dip2px(myapp, 120F));
        }
        requestcreator.tag("ImageManager2");
        if (i1 != 0)
        {
            requestcreator.transform(new af(this, i1, s));
        }
        s = new ag(this, displaycallback, s);
        requestcreator.into(remoteviews, i, new int[0], s);
    }

    public void displayImage(RemoteViews remoteviews, int i, String s, int j, DisplayCallback displaycallback)
    {
        displayImage(remoteviews, i, s, j, 0, 0, 0, displaycallback);
    }

    public void downLoadBitmap(String s)
    {
        downloadBitmap(s, null);
    }

    public void downloadBitmap(String s, DisplayCallback displaycallback)
    {
        downloadBitmap(s, null, displaycallback);
    }

    public void downloadBitmap(String s, Options options, DisplayCallback displaycallback)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        }
        RequestCreator requestcreator = picasso.load(s);
        if (options != null && (options.targetWidth > 0 || options.targetHeight > 0))
        {
            requestcreator.resize(options.targetWidth, options.targetHeight);
        }
        requestcreator.savePermanent(true).fetch(new ab(this, displaycallback, s));
    }

    public final Bitmap getBitmapFromUrl(String s)
    {
        s = loadByteArrayFromNetwork(s);
        if (s != null)
        {
            return BitmapFactory.decodeByteArray(s, 0, s.length);
        } else
        {
            return null;
        }
    }

    public Bitmap getFromMemCache(String s)
    {
        return picasso.quickMemoryCacheCheck(s);
    }

    public Bitmap loadBitmapFromDownLoadCache(String s)
    {
        while (TextUtils.isEmpty(s) || picasso == null) 
        {
            return null;
        }
        return picasso.getFromPermanentDiskCache(s);
    }

    public void pause()
    {
        picasso.pauseTag("ImageManager2");
    }

    public void put(String s, Bitmap bitmap)
    {
        if (s == null || bitmap == null)
        {
            return;
        } else
        {
            picasso.cache(s, bitmap);
            return;
        }
    }

    public void reSet()
    {
    }

    public void reSetForProxy(Context context)
    {
        initContext(context);
        OkHttpClient okhttpclient = new OkHttpClient();
        context = Utils.createDefaultCacheDir(myapp);
        try
        {
            okhttpclient.setCache(new Cache(context, Utils.calculateDiskCacheSize(context)));
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
        if (FreeFlowUtil.getInstance().isNeedFreeFlowProxy())
        {
            if (FreeFlowUtil.getNetType() == 2)
            {
                context = FreeFlowUtil.NET_PROXY_HOST;
            } else
            {
                context = FreeFlowUtil.WAP_PROXY_HOST;
            }
            okhttpclient.setProxy(new Proxy(java.net.Proxy.Type.HTTP, new InetSocketAddress(context, 8080)));
        }
        if (mOkHttpDownloader == null)
        {
            return;
        } else
        {
            mOkHttpDownloader.setHttpClient(okhttpclient);
            return;
        }
    }

    public void resume()
    {
        picasso.resumeTag("ImageManager2");
    }

    public String urlToFileName(String s)
    {
        int i = s.lastIndexOf('.');
        if (i == -1)
        {
            return null;
        } else
        {
            StringBuilder stringbuilder = new StringBuilder();
            stringbuilder.append(MD5.md5(s)).append(s.substring(i + 1));
            return stringbuilder.toString();
        }
    }

    static 
    {
        DOWNLOAD_CACHE_DIR = a.af;
    }

}
