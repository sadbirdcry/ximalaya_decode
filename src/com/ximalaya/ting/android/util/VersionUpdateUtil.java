// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.app.Activity;
import android.content.res.Resources;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.check_version.CheckVersionResult;

// Referenced classes of package com.ximalaya.ting.android.util:
//            bg, bh, bi, bj, 
//            Logger

public class VersionUpdateUtil
{

    public VersionUpdateUtil()
    {
    }

    private void askVersionUpdate(CheckVersionResult checkversionresult)
    {
        Activity activity = MyApplication.a();
        if (activity == null || activity.isFinishing())
        {
            return;
        } else
        {
            activity.runOnUiThread(new bg(this, checkversionresult, activity));
            return;
        }
    }

    private String getApkName(String s, Activity activity)
    {
        return (new StringBuilder()).append(activity.getResources().getString(0x7f09009e)).append(s).toString();
    }

    private void showNormalDialog(String s, Activity activity, CheckVersionResult checkversionresult)
    {
        (new DialogBuilder(activity)).setTitle("\u6E29\u99A8\u63D0\u793A").setMessage(checkversionresult.msg).setOkBtn("\u53BB\u4E0B\u8F7D", new bh(this, activity, s, checkversionresult)).setCancelBtn("\u53D6\u6D88").showConfirm();
    }

    private void showWeblinkDialog(String s, Activity activity, CheckVersionResult checkversionresult)
    {
        SpannableString spannablestring = new SpannableString((new StringBuilder()).append(checkversionresult.msg).append("\uFF08\u67E5\u770B\u65B0\u7248\u672C\u4ECB\u7ECD\uFF09").toString());
        bi bi1 = new bi(this, activity, checkversionresult);
        ForegroundColorSpan foregroundcolorspan = new ForegroundColorSpan(activity.getResources().getColor(0x7f070005));
        spannablestring.setSpan(bi1, spannablestring.length() - 8, spannablestring.length() - 1, 17);
        spannablestring.setSpan(foregroundcolorspan, spannablestring.length() - 8, spannablestring.length() - 1, 17);
        (new DialogBuilder(activity, false)).setTitle("\u6E29\u99A8\u63D0\u793A").setMessage(spannablestring).setTextClickable().setOkBtn("\u53BB\u4E0B\u8F7D", new bj(this, activity, s, checkversionresult)).setCancelBtn("\u53D6\u6D88").showConfirm();
    }

    public void upgrade(String s)
    {
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if ((s = (CheckVersionResult)JSON.parseObject(s, com/ximalaya/ting/android/model/check_version/CheckVersionResult)) == null) goto _L1; else goto _L3
_L3:
        boolean flag;
        if (((CheckVersionResult) (s)).ret == 300)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (!(flag & ((CheckVersionResult) (s)).hasNewVersion)) goto _L1; else goto _L4
_L4:
        askVersionUpdate(s);
        return;
        s;
        Logger.log((new StringBuilder()).append("VersionUpdate exception=").append(s.getMessage()).toString());
        return;
    }



}
