// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.view.View;
import java.lang.ref.WeakReference;

public class OneClickHelper
{

    private static OneClickHelper oneClickHelper;
    private long disTime;
    private long lastTime;
    private WeakReference lastView;

    private OneClickHelper()
    {
        disTime = 600L;
    }

    public static OneClickHelper getInstance()
    {
        if (oneClickHelper == null)
        {
            oneClickHelper = new OneClickHelper();
        }
        return oneClickHelper;
    }

    public boolean onClick(View view)
    {
        if (System.currentTimeMillis() - lastTime < disTime && lastView != null && lastView.get() == view)
        {
            return false;
        } else
        {
            lastView = new WeakReference(view);
            lastTime = System.currentTimeMillis();
            return true;
        }
    }
}
