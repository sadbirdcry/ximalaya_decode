// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.a;


public final class com.ximalaya.ting.android.a.a
{
    public static final class a extends Enum
    {

        public static final a a;
        public static final a b;
        public static final a c;
        public static final a d;
        public static final a e;
        public static final a f;
        public static final a g;
        public static final a h;
        private static final a k[];
        private String i;
        private int j;

        public static a valueOf(String s)
        {
            return (a)Enum.valueOf(com/ximalaya/ting/android/a/a$a, s);
        }

        public static a[] values()
        {
            return (a[])k.clone();
        }

        public String a()
        {
            return i;
        }

        public int b()
        {
            return j;
        }

        static 
        {
            a = new a("ENUM_SUCCESS", 0, "\u52A0\u5165\u4E0B\u8F7D\u5217\u8868\u6210\u529F", 1);
            b = new a("ENUM_SAVE_FAILED", 1, "\u6570\u636E\u5E93\u5B58\u50A8\u5931\u8D25", 2);
            c = new a("ENUM_DUMP_TASK", 2, "\u4E0D\u80FD\u91CD\u590D\u4E0B\u8F7D", 3);
            d = new a("ENUM_NONE_NETWORK", 3, "\u6CA1\u6709\u53EF\u7528\u7F51\u7EDC", 4);
            e = new a("ENUM_INVALID_URL", 4, "\u65E0\u6548\u7684\u94FE\u63A5", 5);
            f = new a("ENUM_NO_SDCARD", 5, "\u68C0\u6D4B\u4E0D\u5230SD\u5361", 6);
            g = new a("ENUM_EMPTY_LIST", 6, "\u7A7A\u5217\u8868", 7);
            h = new a("ENUM_DECIDE_SDCARD", 7, "\u8BF7\u9009\u62E9\u4E0B\u8F7D\u76EE\u5F55", 8);
            k = (new a[] {
                a, b, c, d, e, f, g, h
            });
        }

        private a(String s, int l, String s1, int i1)
        {
            super(s, l);
            i = s1;
            j = i1;
        }
    }

}
