// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import com.ximalaya.ting.android.activity.login.Reg_AgreementActivity;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;

// Referenced classes of package com.ximalaya.ting.android.login:
//            VerificatePhoneNumberLoader, PhoneNumberVerificationModel, RegisterStepTwoFragment

public class RegisterStepOneFragment extends BaseActivityLikeFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, android.view.View.OnClickListener
{

    private ImageButton mClearInputBtn;
    private Loader mLoader;
    private Button mNextBtn;
    private String mPhoneNumber;
    private EditText mPhoneNumberInput;
    private ProgressBar mProgressBar;

    public RegisterStepOneFragment()
    {
    }

    private boolean checkPhoneNumber()
    {
        String s = mPhoneNumberInput.getText().toString();
        if (TextUtils.isEmpty(s))
        {
            showToast("\u624B\u673A\u53F7\u7801\u4E0D\u80FD\u4E3A\u7A7A");
            return false;
        }
        if (!s.startsWith("1") || s.length() != 11)
        {
            showToast("\u624B\u673A\u53F7\u7801\u683C\u5F0F\u4E0D\u6B63\u786E");
            return false;
        } else
        {
            return true;
        }
    }

    public static RegisterStepOneFragment newInstance()
    {
        return new RegisterStepOneFragment();
    }

    private void setEventHandler()
    {
        mClearInputBtn.setOnClickListener(this);
        getView().findViewById(0x7f0a037d).setOnClickListener(this);
        mPhoneNumberInput.addTextChangedListener(new _cls1());
        mNextBtn.setOnClickListener(this);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        setTitleText("\u624B\u673A\u6CE8\u518C");
        bundle = mNextBtn;
        boolean flag;
        if (!TextUtils.isEmpty(mPhoneNumberInput.getText().toString()))
        {
            flag = true;
        } else
        {
            flag = false;
        }
        bundle.setEnabled(flag);
        setEventHandler();
        getActivity().getWindow().setSoftInputMode(5);
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR tableswitch 2131362681 2131362685: default 40
    //                   2131362681 41
    //                   2131362682 40
    //                   2131362683 40
    //                   2131362684 51
    //                   2131362685 95;
           goto _L1 _L2 _L1 _L1 _L3 _L4
_L1:
        return;
_L2:
        mPhoneNumberInput.setText("");
        return;
_L3:
        if (checkPhoneNumber())
        {
            if (mLoader == null)
            {
                mLoader = getLoaderManager().initLoader(0, null, this);
                return;
            } else
            {
                mLoader = getLoaderManager().restartLoader(0, null, this);
                return;
            }
        }
          goto _L1
_L4:
        startActivity(new Intent(getActivity(), com/ximalaya/ting/android/activity/login/Reg_AgreementActivity));
        return;
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        mProgressBar.setVisibility(0);
        mPhoneNumber = mPhoneNumberInput.getText().toString();
        return new VerificatePhoneNumberLoader(getActivity(), mPhoneNumber, mNextBtn);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f0300d3, viewgroup, false);
        mPhoneNumberInput = (EditText)layoutinflater.findViewById(0x7f0a037b);
        mClearInputBtn = (ImageButton)layoutinflater.findViewById(0x7f0a0379);
        mNextBtn = (Button)layoutinflater.findViewById(0x7f0a037c);
        mProgressBar = (ProgressBar)layoutinflater.findViewById(0x7f0a0093);
        return layoutinflater;
    }

    public void onLoadFinished(Loader loader, PhoneNumberVerificationModel phonenumberverificationmodel)
    {
        mProgressBar.setVisibility(4);
        if (phonenumberverificationmodel == null)
        {
            showToast("\u83B7\u53D6\u9A8C\u8BC1\u7801\u9519\u8BEF\uFF0C\u8BF7\u91CD\u8BD5");
            return;
        }
        if (phonenumberverificationmodel.ret == 0)
        {
            loader = RegisterStepTwoFragment.newInstance(mPhoneNumber);
            phonenumberverificationmodel = getFragmentManager().beginTransaction();
            phonenumberverificationmodel.replace(0x7f0a00bc, loader);
            phonenumberverificationmodel.commitAllowingStateLoss();
            return;
        } else
        {
            showToast(phonenumberverificationmodel.msg);
            return;
        }
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (PhoneNumberVerificationModel)obj);
    }

    public void onLoaderReset(Loader loader)
    {
    }



    private class _cls1
        implements TextWatcher
    {

        final RegisterStepOneFragment this$0;

        public void afterTextChanged(Editable editable)
        {
        }

        public void beforeTextChanged(CharSequence charsequence, int i, int j, int k)
        {
        }

        public void onTextChanged(CharSequence charsequence, int i, int j, int k)
        {
            if (!TextUtils.isEmpty(charsequence))
            {
                mClearInputBtn.setVisibility(0);
                mNextBtn.setEnabled(true);
                return;
            } else
            {
                mNextBtn.setEnabled(false);
                mClearInputBtn.setVisibility(4);
                return;
            }
        }

        _cls1()
        {
            this$0 = RegisterStepOneFragment.this;
            super();
        }
    }

}
