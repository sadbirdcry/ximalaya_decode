// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.login;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;

// Referenced classes of package com.ximalaya.ting.android.login:
//            VerifyVerificationCodeModel

public class VerifyVerificationCodeLoader extends AsyncTaskLoader
{

    private static final String PATH = "passport/register/checkcode";
    private VerifyVerificationCodeModel mData;
    private String mPhoneNumber;
    private String mVerificationCode;
    private View mView;

    public VerifyVerificationCodeLoader(Context context, String s, String s1, View view)
    {
        super(context);
        mPhoneNumber = s;
        mVerificationCode = s1;
        mView = view;
    }

    public void deliverResult(VerifyVerificationCodeModel verifyverificationcodemodel)
    {
        super.deliverResult(verifyverificationcodemodel);
        mData = verifyverificationcodemodel;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((VerifyVerificationCodeModel)obj);
    }

    public VerifyVerificationCodeModel loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("mPhone", mPhoneNumber);
        ((RequestParams) (obj)).put("checkCode", mVerificationCode);
        obj = f.a().b((new StringBuilder()).append(a.M).append("passport/register/checkcode").toString(), ((RequestParams) (obj)), mView, mView);
        if (!TextUtils.isEmpty(((CharSequence) (obj))))
        {
            mData = (VerifyVerificationCodeModel)JSONObject.parseObject(((String) (obj)), com/ximalaya/ting/android/login/VerifyVerificationCodeModel);
        }
        return mData;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
