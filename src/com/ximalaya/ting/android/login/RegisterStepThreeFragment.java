// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.login;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.model.BaseModel;

// Referenced classes of package com.ximalaya.ting.android.login:
//            SetPasswordLoader, RegisterStepFourFragment

public class RegisterStepThreeFragment extends BaseActivityLikeFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, android.view.View.OnClickListener
{

    private Loader mLoader;
    private Button mNextBtn;
    private EditText mPassword;
    private ProgressBar mProgressBar;
    private ImageButton mShowPassword;
    private String mUuid;

    public RegisterStepThreeFragment()
    {
    }

    private boolean isPasswordValide()
    {
        String s;
        s = mPassword.getText().toString();
        break MISSING_BLOCK_LABEL_11;
        while (true) 
        {
            do
            {
                return false;
            } while (TextUtils.isEmpty(s) || s.length() < 6 || s.length() > 16);
            int i = 0;
label0:
            do
            {
label1:
                {
                    if (i >= s.length())
                    {
                        break label1;
                    }
                    if (!Character.isLetterOrDigit(Character.valueOf(s.charAt(i)).charValue()))
                    {
                        break label0;
                    }
                    i++;
                }
            } while (true);
        }
        return true;
    }

    public static RegisterStepThreeFragment newInstance(String s)
    {
        Bundle bundle = new Bundle();
        bundle.putString("uuid", s);
        s = new RegisterStepThreeFragment();
        s.setArguments(bundle);
        return s;
    }

    private void setEventHandler()
    {
        mPassword.addTextChangedListener(new _cls1());
        mShowPassword.setOnClickListener(this);
        mNextBtn.setOnClickListener(this);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        setTitleText("\u8BBE\u7F6E\u5BC6\u7801");
        mPassword.requestFocus();
        mNextBtn.setEnabled(false);
        setEventHandler();
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR tableswitch 2131362684 2131362686: default 32
    //                   2131362684 99
    //                   2131362685 32
    //                   2131362686 33;
           goto _L1 _L2 _L1 _L3
_L1:
        return;
_L3:
        if (mPassword.getInputType() == 144)
        {
            mPassword.setInputType(129);
        } else
        {
            mPassword.setInputType(144);
        }
        view = mPassword.getText().toString();
        if (!TextUtils.isEmpty(view))
        {
            mPassword.setSelection(view.length());
            return;
        }
          goto _L1
_L2:
        if (!isPasswordValide())
        {
            showToast("\u5BC6\u7801\u683C\u5F0F\u9519\u8BEF");
            return;
        }
        view = mPassword.getText().toString();
        Bundle bundle = new Bundle();
        bundle.putString("passwd", view);
        if (mLoader == null)
        {
            mLoader = getLoaderManager().initLoader(0, bundle, this);
            return;
        } else
        {
            mLoader = getLoaderManager().restartLoader(0, bundle, this);
            return;
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mUuid = bundle.getString("uuid");
        }
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        mProgressBar.setVisibility(0);
        bundle = bundle.getString("passwd");
        return new SetPasswordLoader(getActivity(), getView().findViewById(0x7f0a037c), mUuid, bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f0300d4, viewgroup, false);
        mPassword = (EditText)layoutinflater.findViewById(0x7f0a0113);
        mShowPassword = (ImageButton)layoutinflater.findViewById(0x7f0a037e);
        mNextBtn = (Button)layoutinflater.findViewById(0x7f0a037c);
        mProgressBar = (ProgressBar)layoutinflater.findViewById(0x7f0a0093);
        return layoutinflater;
    }

    public void onLoadFinished(Loader loader, BaseModel basemodel)
    {
        mProgressBar.setVisibility(4);
        if (basemodel == null || basemodel.ret != 0)
        {
            if (basemodel == null)
            {
                loader = "\u8BBE\u7F6E\u5BC6\u7801\u51FA\u9519";
            } else
            {
                loader = basemodel.msg;
            }
            showToast(loader);
            return;
        } else
        {
            loader = RegisterStepFourFragment.newInstance(mUuid);
            basemodel = getFragmentManager().beginTransaction();
            basemodel.replace(0x7f0a00bc, loader);
            basemodel.commitAllowingStateLoss();
            return;
        }
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (BaseModel)obj);
    }

    public void onLoaderReset(Loader loader)
    {
    }



    private class _cls1
        implements TextWatcher
    {

        final RegisterStepThreeFragment this$0;

        public void afterTextChanged(Editable editable)
        {
        }

        public void beforeTextChanged(CharSequence charsequence, int i, int j, int k)
        {
        }

        public void onTextChanged(CharSequence charsequence, int i, int j, int k)
        {
            if (TextUtils.isEmpty(charsequence))
            {
                mShowPassword.setVisibility(4);
                mNextBtn.setEnabled(false);
                return;
            } else
            {
                mNextBtn.setEnabled(true);
                mShowPassword.setVisibility(0);
                return;
            }
        }

        _cls1()
        {
            this$0 = RegisterStepThreeFragment.this;
            super();
        }
    }

}
