// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.login;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.util.MD5;
import com.ximalaya.ting.android.util.RSA;

public class SetPasswordLoader extends AsyncTaskLoader
{

    private static final String PATH = "passport/register/password";
    private BaseModel mData;
    private String mPassword;
    private String mUuid;
    private View mView;

    public SetPasswordLoader(Context context, View view, String s, String s1)
    {
        super(context);
        mView = view;
        mUuid = s;
        mPassword = s1;
    }

    public void deliverResult(BaseModel basemodel)
    {
        super.deliverResult(basemodel);
        mData = basemodel;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((BaseModel)obj);
    }

    public BaseModel loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("uuid", mUuid);
        try
        {
            ((RequestParams) (obj)).put("password", (new RSA(RSA.getPublicKey("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCVhaR3Or7suUlwHUl2Ly36uVmboZ3+HhovogDjLgRE9CbaUokS2eqGaVFfbxAUxFThNDuXq/fBD+SdUgppmcZrIw4HMMP4AtE2qJJQH/KxPWmbXH7Lv+9CisNtPYOlvWJ/GHRqf9x3TBKjjeJ2CjuVxlPBDX63+Ecil2JR9klVawIDAQAB"))).encryptByPublicKey(MD5.md5(mPassword)));
        }
        catch (Exception exception1)
        {
            exception1.printStackTrace();
        }
        obj = f.a().b((new StringBuilder()).append(a.M).append("passport/register/password").toString(), ((RequestParams) (obj)), mView, mView);
        if (!TextUtils.isEmpty(((CharSequence) (obj))))
        {
            try
            {
                mData = (BaseModel)JSONObject.parseObject(((String) (obj)), com/ximalaya/ting/android/model/BaseModel);
            }
            catch (Exception exception) { }
        }
        return mData;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
