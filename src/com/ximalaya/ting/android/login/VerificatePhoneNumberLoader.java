// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.login;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;

// Referenced classes of package com.ximalaya.ting.android.login:
//            PhoneNumberVerificationModel

public class VerificatePhoneNumberLoader extends AsyncTaskLoader
{

    private static final String PATH = "passport/register/mPhone";
    private PhoneNumberVerificationModel mData;
    private String mPhoneNumber;
    private View mView;

    public VerificatePhoneNumberLoader(Context context, String s, View view)
    {
        super(context);
        mView = view;
        mPhoneNumber = s;
    }

    public void deliverResult(PhoneNumberVerificationModel phonenumberverificationmodel)
    {
        super.deliverResult(phonenumberverificationmodel);
        mData = phonenumberverificationmodel;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((PhoneNumberVerificationModel)obj);
    }

    public PhoneNumberVerificationModel loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("mPhone", mPhoneNumber);
        obj = f.a().b((new StringBuilder()).append(a.M).append("passport/register/mPhone").toString(), ((RequestParams) (obj)), mView, mView);
        if (!TextUtils.isEmpty(((CharSequence) (obj))))
        {
            try
            {
                mData = (PhoneNumberVerificationModel)JSONObject.parseObject(((String) (obj)), com/ximalaya/ting/android/login/PhoneNumberVerificationModel);
            }
            catch (Exception exception) { }
        }
        return mData;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
