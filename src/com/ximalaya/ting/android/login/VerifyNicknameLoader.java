// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.login;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;

// Referenced classes of package com.ximalaya.ting.android.login:
//            VerifyNicknameModel

public class VerifyNicknameLoader extends MyAsyncTaskLoader
{

    private static final String PATH = "passport/register/check-nickname";
    private VerifyNicknameModel mData;
    private String mNickname;
    private View mView;

    public VerifyNicknameLoader(Context context, String s, View view)
    {
        super(context);
        mNickname = s;
        mView = view;
    }

    public void deliverResult(VerifyNicknameModel verifynicknamemodel)
    {
        super.deliverResult(verifynicknamemodel);
        mData = verifynicknamemodel;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((VerifyNicknameModel)obj);
    }

    public VerifyNicknameModel loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("nickname", mNickname);
        obj = f.a().b((new StringBuilder()).append(a.M).append("passport/register/check-nickname").toString(), ((RequestParams) (obj)), mView, mView);
        if (!TextUtils.isEmpty(((CharSequence) (obj))))
        {
            try
            {
                mData = (VerifyNicknameModel)JSONObject.parseObject(((String) (obj)), com/ximalaya/ting/android/login/VerifyNicknameModel);
            }
            catch (Exception exception) { }
        }
        return mData;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
