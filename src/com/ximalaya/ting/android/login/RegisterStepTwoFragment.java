// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.login;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import java.util.Timer;

// Referenced classes of package com.ximalaya.ting.android.login:
//            VerifyVerificationCodeLoader, VerifyVerificationCodeModel, RegisterStepThreeFragment

public class RegisterStepTwoFragment extends BaseActivityLikeFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, android.view.View.OnClickListener
{

    private static final int GET_VERIFICATION_CODE = 0;
    private static final int VALIDATE_VERIFICATION_CODE = 1;
    private int mElapsedTime;
    private android.support.v4.app.LoaderManager.LoaderCallbacks mGetVerificationCodeCallback;
    private Loader mGetVerificationCodeLoader;
    private Button mNextBtn;
    private String mPhoneNumber;
    private TextView mPhoneNumberLabel;
    private ProgressBar mProgressBar;
    private TextView mResend;
    private int mResendTimes;
    private Timer mTimer;
    private TextView mTiming;
    private EditText mVerificationCode;
    private Loader mVerifyVerificationCodeLoader;

    public RegisterStepTwoFragment()
    {
        mResendTimes = 0;
        mGetVerificationCodeCallback = new _cls1();
    }

    public static RegisterStepTwoFragment newInstance(String s)
    {
        RegisterStepTwoFragment registersteptwofragment = new RegisterStepTwoFragment();
        Bundle bundle = new Bundle();
        bundle.putString("phone_number", s);
        registersteptwofragment.setArguments(bundle);
        return registersteptwofragment;
    }

    private void setEventHandler()
    {
        mNextBtn.setOnClickListener(this);
        mResend.setOnClickListener(this);
        mVerificationCode.addTextChangedListener(new _cls2());
    }

    private void startTimingResend()
    {
        mResendTimes = mResendTimes + 1;
        mTimer = new Timer();
        mElapsedTime = 0;
        mTimer.scheduleAtFixedRate(new _cls3(), 0L, 1000L);
    }

    private void stopTiming()
    {
        if (mTimer != null)
        {
            mTimer.cancel();
        }
    }

    private void timingResend()
    {
        mResend.post(new _cls4());
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        setTitleText("\u624B\u673A\u6CE8\u518C");
        mVerificationCode.requestFocus();
        mPhoneNumberLabel.setText((new StringBuilder()).append("+86 ").append(mPhoneNumber).toString());
        mNextBtn.setEnabled(false);
        setEventHandler();
        mResend.setEnabled(false);
        startTimingResend();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362684: 
            view = mVerificationCode.getText().toString();
            if (TextUtils.isEmpty(view))
            {
                showToast("\u9A8C\u8BC1\u7801\u4E0D\u80FD\u4E3A\u7A7A");
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("verification_code", view);
            if (mVerifyVerificationCodeLoader == null)
            {
                mVerifyVerificationCodeLoader = getLoaderManager().initLoader(1, bundle, this);
                return;
            } else
            {
                mVerifyVerificationCodeLoader = getLoaderManager().restartLoader(1, bundle, this);
                return;
            }

        case 2131362691: 
            break;
        }
        if (mGetVerificationCodeLoader == null)
        {
            mGetVerificationCodeLoader = getLoaderManager().initLoader(0, null, mGetVerificationCodeCallback);
            return;
        } else
        {
            mGetVerificationCodeLoader = getLoaderManager().restartLoader(0, null, mGetVerificationCodeCallback);
            return;
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        mPhoneNumber = getArguments().getString("phone_number");
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        mProgressBar.setVisibility(0);
        if (bundle != null)
        {
            bundle = bundle.getString("verification_code");
            return new VerifyVerificationCodeLoader(getActivity(), mPhoneNumber, bundle, getView().findViewById(0x7f0a037c));
        } else
        {
            return null;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f0300d5, viewgroup, false);
        mPhoneNumberLabel = (TextView)layoutinflater.findViewById(0x7f0a037b);
        mTiming = (TextView)layoutinflater.findViewById(0x7f0a0382);
        mResend = (TextView)layoutinflater.findViewById(0x7f0a0383);
        mVerificationCode = (EditText)layoutinflater.findViewById(0x7f0a0380);
        mNextBtn = (Button)layoutinflater.findViewById(0x7f0a037c);
        mProgressBar = (ProgressBar)layoutinflater.findViewById(0x7f0a0093);
        return layoutinflater;
    }

    public void onDestroy()
    {
        super.onDestroy();
        stopTiming();
    }

    public void onLoadFinished(Loader loader, VerifyVerificationCodeModel verifyverificationcodemodel)
    {
        mProgressBar.setVisibility(4);
        if (verifyverificationcodemodel == null || verifyverificationcodemodel.ret != 0)
        {
            showToast("\u9A8C\u8BC1\u7801\u9519\u8BEF");
            return;
        } else
        {
            loader = RegisterStepThreeFragment.newInstance(verifyverificationcodemodel.uuid);
            verifyverificationcodemodel = getFragmentManager().beginTransaction();
            verifyverificationcodemodel.replace(0x7f0a00bc, loader);
            verifyverificationcodemodel.commitAllowingStateLoss();
            return;
        }
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (VerifyVerificationCodeModel)obj);
    }

    public void onLoaderReset(Loader loader)
    {
    }







/*
    static int access$408(RegisterStepTwoFragment registersteptwofragment)
    {
        int i = registersteptwofragment.mElapsedTime;
        registersteptwofragment.mElapsedTime = i + 1;
        return i;
    }

*/





    private class _cls1
        implements android.support.v4.app.LoaderManager.LoaderCallbacks
    {

        final RegisterStepTwoFragment this$0;

        public Loader onCreateLoader(int i, Bundle bundle)
        {
            return new VerificatePhoneNumberLoader(getActivity(), mPhoneNumber, mResend);
        }

        public void onLoadFinished(Loader loader, PhoneNumberVerificationModel phonenumberverificationmodel)
        {
            startTimingResend();
        }

        public volatile void onLoadFinished(Loader loader, Object obj)
        {
            onLoadFinished(loader, (PhoneNumberVerificationModel)obj);
        }

        public void onLoaderReset(Loader loader)
        {
        }

        _cls1()
        {
            this$0 = RegisterStepTwoFragment.this;
            super();
        }
    }


    private class _cls2
        implements TextWatcher
    {

        final RegisterStepTwoFragment this$0;

        public void afterTextChanged(Editable editable)
        {
        }

        public void beforeTextChanged(CharSequence charsequence, int i, int j, int k)
        {
        }

        public void onTextChanged(CharSequence charsequence, int i, int j, int k)
        {
            if (TextUtils.isEmpty(charsequence))
            {
                mNextBtn.setEnabled(false);
                return;
            } else
            {
                mNextBtn.setEnabled(true);
                return;
            }
        }

        _cls2()
        {
            this$0 = RegisterStepTwoFragment.this;
            super();
        }
    }



    private class _cls4
        implements Runnable
    {

        final RegisterStepTwoFragment this$0;

        public void run()
        {
            if (mElapsedTime >= 60)
            {
                stopTiming();
                mTiming.setVisibility(8);
                mResend.setTextColor(getResources().getColor(0x7f070004));
                mResend.setText("\u91CD\u65B0\u53D1\u9001");
                mResend.setEnabled(true);
                return;
            }
            int i = 60 - mElapsedTime;
            String s;
            if (i < 10)
            {
                s = (new StringBuilder()).append("00:0").append(i).append("\u540E").toString();
            } else
            {
                s = (new StringBuilder()).append("00:").append(i).append("\u540E").toString();
            }
            mTiming.setVisibility(0);
            mTiming.setText(s);
            mTiming.setTextColor(Color.parseColor("#999999"));
            mResend.setText((new StringBuilder()).append("\u91CD\u65B0\u53D1\u9001\uFF08\u7B2C").append(mResendTimes).append("\u6B21\uFF09").toString());
            mResend.setTextColor(Color.parseColor("#999999"));
            mResend.setEnabled(false);
        }

        _cls4()
        {
            this$0 = RegisterStepTwoFragment.this;
            super();
        }
    }

}
