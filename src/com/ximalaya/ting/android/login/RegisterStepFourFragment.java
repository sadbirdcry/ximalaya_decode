// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.model.album.LocalCollectAlbumUploader;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.Utilities;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Referenced classes of package com.ximalaya.ting.android.login:
//            VerifyNicknameModel, SetNicknameLoader

public class RegisterStepFourFragment extends BaseActivityLikeFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, android.view.View.OnClickListener
{
    private static class NicknameSuggestionAdapter extends ArrayAdapter
    {

        private String mData[];
        private String mKeyword;

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            view = super.getView(i, view, viewgroup);
            if ((view instanceof TextView) && !TextUtils.isEmpty(mKeyword))
            {
                Object obj = Pattern.compile(mKeyword);
                String s = mData[i];
                viewgroup = new SpannableString(s);
                for (obj = ((Pattern) (obj)).matcher(s); ((Matcher) (obj)).find(); viewgroup.setSpan(new ForegroundColorSpan(Color.parseColor("#f86442")), ((Matcher) (obj)).start(), ((Matcher) (obj)).end(), 18)) { }
                ((TextView)view).setText(viewgroup);
            }
            return view;
        }

        public void setKeyword(String s)
        {
            mKeyword = s;
        }

        public NicknameSuggestionAdapter(Context context, int i, String as[])
        {
            super(context, i, as);
            mData = as;
        }
    }


    private static final int SET_NICKNAME = 1;
    private static final int VERIFY_NICKNAME = 2;
    private ImageButton mClearInput;
    private Button mDoneBtn;
    private EditText mNickname;
    private ProgressBar mProgressBar;
    private Loader mSetNicknameLoader;
    private String mUuid;
    private android.support.v4.app.LoaderManager.LoaderCallbacks mVerifyNicknameCallback;
    private Loader mVerifyNicknameLoader;

    public RegisterStepFourFragment()
    {
        mVerifyNicknameCallback = new _cls1();
    }

    private void completeLogin(LoginInfoModel logininfomodel)
    {
        UserInfoMannage.getInstance().setUser(logininfomodel);
        if (logininfomodel.isFirst)
        {
            (new LocalCollectAlbumUploader(getActivity().getApplicationContext())).myexec(new Void[0]);
        }
        ((MyApplication)getActivity().getApplication()).e = 1;
        logininfomodel = ScoreManage.getInstance(getActivity().getApplicationContext());
        if (logininfomodel != null)
        {
            logininfomodel.initBehaviorScore();
            logininfomodel.updateScore();
        }
        logininfomodel = new Intent(getActivity(), com/ximalaya/ting/android/activity/MainTabActivity2);
        getActivity().finish();
        getActivity().startActivity(logininfomodel);
    }

    private void initEventHandler()
    {
        mClearInput.setOnClickListener(this);
        mDoneBtn.setOnClickListener(this);
        mNickname.addTextChangedListener(new _cls3());
    }

    public static RegisterStepFourFragment newInstance(String s)
    {
        RegisterStepFourFragment registerstepfourfragment = new RegisterStepFourFragment();
        Bundle bundle = new Bundle();
        bundle.putString("uuid", s);
        registerstepfourfragment.setArguments(bundle);
        return registerstepfourfragment;
    }

    private void setNickname()
    {
        String s = mNickname.getText().toString();
        if (!TextUtils.isEmpty(s))
        {
            Bundle bundle = new Bundle();
            bundle.putString("nickname", s);
            if (mSetNicknameLoader == null)
            {
                mSetNicknameLoader = getLoaderManager().initLoader(1, bundle, this);
                return;
            } else
            {
                mSetNicknameLoader = getLoaderManager().restartLoader(1, bundle, this);
                return;
            }
        } else
        {
            showToast("\u6635\u79F0\u4E0D\u80FD\u4E3A\u7A7A");
            return;
        }
    }

    private void showSuggestNickname(final VerifyNicknameModel data)
    {
        if (data.recommand != null && data.recommand.length > 0)
        {
            final PopupWindow pw = new PopupWindow(getActivity());
            pw.setHeight(-2);
            pw.setWidth(getView().findViewById(0x7f0a0377).getWidth());
            pw.setBackgroundDrawable(new BitmapDrawable());
            pw.setOutsideTouchable(true);
            pw.setTouchable(true);
            final ListView listView = (ListView)View.inflate(getActivity(), 0x7f030152, null);
            listView.setDivider(new ColorDrawable(Color.parseColor("#dedede")));
            listView.setDividerHeight(1);
            pw.setContentView(listView);
            listView.addHeaderView(LayoutInflater.from(getActivity()).inflate(0x7f030101, listView, false), null, false);
            listView.addHeaderView(LayoutInflater.from(getActivity()).inflate(0x7f030102, listView, false), null, false);
            listView.setHeaderDividersEnabled(true);
            NicknameSuggestionAdapter nicknamesuggestionadapter = new NicknameSuggestionAdapter(getActivity(), 0x7f030135, data.recommand);
            nicknamesuggestionadapter.setKeyword(mNickname.getText().toString());
            listView.setAdapter(nicknamesuggestionadapter);
            listView.setOnItemClickListener(new _cls2());
            pw.showAsDropDown(getView().findViewById(0x7f0a0377), 0, Utilities.dip2px(getActivity(), 5F));
        }
    }

    private void verifyNickname()
    {
        String s = mNickname.getText().toString();
        if (TextUtils.isEmpty(s))
        {
            showToast("\u6635\u79F0\u4E0D\u80FD\u4E3A\u7A7A");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString("nickname", s);
        if (mVerifyNicknameLoader == null)
        {
            mVerifyNicknameLoader = getLoaderManager().initLoader(2, bundle, mVerifyNicknameCallback);
            return;
        } else
        {
            mVerifyNicknameLoader = getLoaderManager().restartLoader(2, bundle, mVerifyNicknameCallback);
            return;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        setTitleText("\u8BBE\u7F6E\u6635\u79F0");
        mNickname.requestFocus();
        mDoneBtn.setEnabled(false);
        initEventHandler();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362681: 
            mNickname.setText("");
            mClearInput.setVisibility(4);
            return;

        case 2131362682: 
            setNickname();
            break;
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mUuid = bundle.getString("uuid");
        }
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        mProgressBar.setVisibility(0);
        bundle = bundle.getString("nickname");
        return new SetNicknameLoader(getActivity(), mUuid, bundle, mDoneBtn);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f0300d2, viewgroup, false);
        mClearInput = (ImageButton)layoutinflater.findViewById(0x7f0a0379);
        mNickname = (EditText)layoutinflater.findViewById(0x7f0a0378);
        mDoneBtn = (Button)layoutinflater.findViewById(0x7f0a037a);
        mProgressBar = (ProgressBar)layoutinflater.findViewById(0x7f0a0093);
        return layoutinflater;
    }

    public void onLoadFinished(Loader loader, LoginInfoModel logininfomodel)
    {
        mProgressBar.setVisibility(4);
        if (logininfomodel != null)
        {
            if (logininfomodel.ret == 0)
            {
                completeLogin(logininfomodel);
                return;
            } else
            {
                showToast(logininfomodel.msg);
                return;
            }
        } else
        {
            showToast("\u8BBE\u7F6E\u6635\u79F0\u51FA\u9519");
            return;
        }
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (LoginInfoModel)obj);
    }

    public void onLoaderReset(Loader loader)
    {
    }






    private class _cls1
        implements android.support.v4.app.LoaderManager.LoaderCallbacks
    {

        final RegisterStepFourFragment this$0;

        public Loader onCreateLoader(int i, Bundle bundle)
        {
            bundle = bundle.getString("nickname");
            return new VerifyNicknameLoader(getActivity(), bundle, mDoneBtn);
        }

        public void onLoadFinished(Loader loader, VerifyNicknameModel verifynicknamemodel)
        {
            if (verifynicknamemodel != null)
            {
                showSuggestNickname(verifynicknamemodel);
            }
        }

        public volatile void onLoadFinished(Loader loader, Object obj)
        {
            onLoadFinished(loader, (VerifyNicknameModel)obj);
        }

        public void onLoaderReset(Loader loader)
        {
        }

        _cls1()
        {
            this$0 = RegisterStepFourFragment.this;
            super();
        }
    }


    private class _cls3
        implements TextWatcher
    {

        final RegisterStepFourFragment this$0;

        public void afterTextChanged(Editable editable)
        {
        }

        public void beforeTextChanged(CharSequence charsequence, int i, int j, int k)
        {
        }

        public void onTextChanged(CharSequence charsequence, int i, int j, int k)
        {
            if (TextUtils.isEmpty(charsequence))
            {
                mClearInput.setVisibility(4);
                mDoneBtn.setEnabled(false);
                return;
            } else
            {
                mDoneBtn.setEnabled(true);
                mClearInput.setVisibility(0);
                verifyNickname();
                return;
            }
        }

        _cls3()
        {
            this$0 = RegisterStepFourFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final RegisterStepFourFragment this$0;
        final VerifyNicknameModel val$data;
        final ListView val$listView;
        final PopupWindow val$pw;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            pw.dismiss();
            i -= listView.getHeaderViewsCount();
            if (i < 0 || i >= data.recommand.length)
            {
                return;
            } else
            {
                mNickname.setText(data.recommand[i]);
                return;
            }
        }

        _cls2()
        {
            this$0 = RegisterStepFourFragment.this;
            pw = popupwindow;
            listView = listview;
            data = verifynicknamemodel;
            super();
        }
    }

}
