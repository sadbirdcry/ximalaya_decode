// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.login;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Referenced classes of package com.ximalaya.ting.android.login:
//            RegisterStepFourFragment

private static class mData extends ArrayAdapter
{

    private String mData[];
    private String mKeyword;

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        view = super.getView(i, view, viewgroup);
        if ((view instanceof TextView) && !TextUtils.isEmpty(mKeyword))
        {
            Object obj = Pattern.compile(mKeyword);
            String s = mData[i];
            viewgroup = new SpannableString(s);
            for (obj = ((Pattern) (obj)).matcher(s); ((Matcher) (obj)).find(); viewgroup.setSpan(new ForegroundColorSpan(Color.parseColor("#f86442")), ((Matcher) (obj)).start(), ((Matcher) (obj)).end(), 18)) { }
            ((TextView)view).setText(viewgroup);
        }
        return view;
    }

    public void setKeyword(String s)
    {
        mKeyword = s;
    }

    public (Context context, int i, String as[])
    {
        super(context, i, as);
        mData = as;
    }
}
