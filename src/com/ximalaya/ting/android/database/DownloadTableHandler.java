// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.TypeConvertHelpler;
import com.ximalaya.ting.android.util.Utilities;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;

// Referenced classes of package com.ximalaya.ting.android.database:
//            DataBaseHelper

public class DownloadTableHandler
{

    private static final String ORDER_POSITION = "order_position";
    private final String ALBUM_COVER = "mAlbumImage";
    private final String ALBUM_ID = "albumId";
    private final String ALBUM_NAME = "albumName";
    private final String COVER_LARGE = "image_path_large";
    private final String COVER_SMALL = "image_path";
    private final String CREATE_AT = "create_at";
    private final String DOWNLOADED = "downloaded";
    private final String DOWNLOAD_ID = "_download_id";
    private final String DOWNLOAD_TYPE = "downloadType";
    private final String DOWNLOAD_URL = "download_url";
    private final String DURATION = "duration";
    private final String FILESIZE = "filesize";
    private final String FILE_LOCATION = "file_location";
    private final String IS_FAVORITED = "is_favorited";
    private final String LIKES = "likes";
    private final String NICKNAME = "nickname";
    private final String ORDER_NUM = "orderNum";
    private final String PLAYURL32 = "playurl32";
    private final String PLAYURL64 = "playurl64";
    private final String PLAY_TIMES = "play_times";
    private final String SEQUNCE_ID = "sequnceId";
    private final String STATUS = "status";
    private final String TABLE_NAME = "t_download";
    private final String TITLE = "title";
    private final String TRACK_ID = "track_id";
    private final String UID = "uid";
    private final String USER_COVER_PATH = "userCoverPath";
    private Context mContext;

    public DownloadTableHandler(Context context)
    {
        mContext = context;
    }

    private String getAllColumn()
    {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append("_download_id").append(", ");
        stringbuilder.append("title").append(", ");
        stringbuilder.append("nickname").append(", ");
        stringbuilder.append("play_times").append(", ");
        stringbuilder.append("likes").append(", ");
        stringbuilder.append("duration").append(", ");
        stringbuilder.append("create_at").append(", ");
        stringbuilder.append("playurl32").append(", ");
        stringbuilder.append("filesize").append(", ");
        stringbuilder.append("downloaded").append(", ");
        stringbuilder.append("status").append(", ");
        stringbuilder.append("track_id").append(", ");
        stringbuilder.append("image_path").append(", ");
        stringbuilder.append("image_path_large").append(", ");
        stringbuilder.append("playurl64").append(", ");
        stringbuilder.append("orderNum").append(", ");
        stringbuilder.append("albumId").append(", ");
        stringbuilder.append("albumName").append(", ");
        stringbuilder.append("uid").append(", ");
        stringbuilder.append("mAlbumImage").append(", ");
        stringbuilder.append("userCoverPath").append(", ");
        stringbuilder.append("download_url").append(", ");
        stringbuilder.append("file_location").append(", ");
        stringbuilder.append("downloadType").append(", ");
        stringbuilder.append("sequnceId").append(", ");
        stringbuilder.append("is_favorited").append(", ");
        stringbuilder.append("order_position");
        return stringbuilder.toString();
    }

    public final void QueryAlbumSoundOrders(HashMap hashmap, HashMap hashmap1)
    {
        Iterator iterator = hashmap.keySet().iterator();
_L4:
        if (!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        Object obj1 = (Long)iterator.next();
        if (((Long) (obj1)).longValue() <= 0L || !isAlbumOrdersEmpty((ArrayList)hashmap.get(obj1))) goto _L4; else goto _L3
_L3:
        Object obj;
        ArrayList arraylist;
        arraylist = (ArrayList)hashmap.get(obj1);
        obj = String.valueOf(((DownloadTask)arraylist.get(0)).trackId);
        for (int i = 1; i < arraylist.size(); i++)
        {
            obj = (new StringBuilder()).append(((String) (obj))).append(",").toString();
            obj = (new StringBuilder()).append(((String) (obj))).append(String.valueOf(((DownloadTask)arraylist.get(i)).trackId)).toString();
        }

        RequestParams requestparams = new RequestParams();
        requestparams.put("albumId", (new StringBuilder()).append(obj1).append("").toString());
        requestparams.put("albumUid", (new StringBuilder()).append(((DownloadTask)arraylist.get(0)).uid).append("").toString());
        requestparams.put("trackIds", (new StringBuilder()).append(((String) (obj))).append("").toString());
        obj = f.a().a(e.V, requestparams, null);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1)
        {
            break MISSING_BLOCK_LABEL_397;
        }
        obj = ((com.ximalaya.ting.android.b.n.a) (obj)).a;
_L9:
        if (obj == null) goto _L4; else goto _L5
_L5:
        obj = JSON.parseObject(((String) (obj)));
        if (!"0".equals(((JSONObject) (obj)).getString("ret"))) goto _L4; else goto _L6
_L6:
        obj = JSON.parseArray(((JSONObject) (obj)).getString("data"), java/lang/Long);
        int j = 0;
_L8:
        if (j >= ((List) (obj)).size()) goto _L4; else goto _L7
_L7:
        obj1 = (DownloadTask)hashmap1.get((Long)((List) (obj)).get(j));
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_379;
        }
        obj1.orderNum = j;
        updateSoundRecord(((DownloadTask) (obj1)));
        j++;
          goto _L8
        obj;
        ((Exception) (obj)).printStackTrace();
          goto _L4
_L2:
        return;
        obj = null;
          goto _L9
    }

    public final void checkUpDownloadFiles(Context context)
    {
        Context context1;
        Object obj;
        ArrayList arraylist;
        obj = null;
        context1 = null;
        arraylist = new ArrayList();
        context = DataBaseHelper.dbRawQuery(context, "SELECT track_id, filesize, downloaded, status, download_url FROM t_download", null);
        context1 = context;
        obj = context;
        context.moveToFirst();
        context1 = context;
        obj = context;
        int i = context.getCount();
_L2:
        context1 = context;
        if (context.getPosition() == context.getCount())
        {
            break; /* Loop/switch isn't completed */
        }
        context1 = context;
        obj = new DownloadTask();
        context1 = context;
        obj.trackId = context.getLong(context.getColumnIndex("track_id"));
        context1 = context;
        obj.downLoadUrl = context.getString(context.getColumnIndex("download_url"));
        context1 = context;
        obj.downloadStatus = context.getInt(context.getColumnIndex("status"));
        context1 = context;
        obj.filesize = context.getLong(context.getColumnIndex("filesize"));
        context1 = context;
        obj.downloaded = context.getLong(context.getColumnIndex("downloaded"));
        context1 = context;
        if (((DownloadTask) (obj)).downloadStatus == 4)
        {
            break MISSING_BLOCK_LABEL_207;
        }
        context1 = context;
        if (((DownloadTask) (obj)).filesize <= 0L)
        {
            break MISSING_BLOCK_LABEL_296;
        }
        context1 = context;
        if (((DownloadTask) (obj)).filesize != ((DownloadTask) (obj)).downloaded)
        {
            break MISSING_BLOCK_LABEL_296;
        }
        context1 = context;
        if (!Utilities.isNotBlank(((DownloadTask) (obj)).downLoadUrl))
        {
            break MISSING_BLOCK_LABEL_296;
        }
        context1 = context;
        if (!((DownloadTask) (obj)).downLoadUrl.contains("http"))
        {
            break MISSING_BLOCK_LABEL_296;
        }
        context1 = context;
        if ((new File((new StringBuilder()).append(a.aj).append("/").append(ToolUtil.md5(((DownloadTask) (obj)).downLoadUrl)).toString())).exists())
        {
            break MISSING_BLOCK_LABEL_296;
        }
        context1 = context;
        arraylist.add(Long.valueOf(((DownloadTask) (obj)).trackId));
        context1 = context;
        context.moveToNext();
        if (true) goto _L2; else goto _L1
        Throwable throwable;
        throwable;
_L6:
        context1 = context;
        Logger.log("checkUpDownloadFiles", throwable.toString(), true);
        int j = i;
        if (context == null) goto _L4; else goto _L3
_L3:
        context.close();
        j = i;
_L4:
        try
        {
            if (arraylist.size() > 0)
            {
                for (context = arraylist.iterator(); context.hasNext(); deleteRecord(((Long)context.next()).longValue())) { }
            }
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Logger.log("DownloadTableHandler", context.getMessage());
        }
        if (arraylist.size() == 0)
        {
            MyApplication.g = 0;
            return;
        }
        break MISSING_BLOCK_LABEL_422;
        context;
        if (context1 != null)
        {
            context1.close();
        }
        throw context;
        if (arraylist.size() < j)
        {
            MyApplication.g = 1;
            return;
        } else
        {
            MyApplication.g = 2;
            return;
        }
_L1:
        j = i;
        if (context == null) goto _L4; else goto _L3
        throwable;
        i = 0;
        context = ((Context) (obj));
        if (true) goto _L6; else goto _L5
_L5:
    }

    public final void checkUpOrderNumbers()
    {
        Object obj;
        Object obj1;
        Object obj2;
        HashMap hashmap;
        HashMap hashmap1;
        obj1 = null;
        obj = null;
        hashmap = new HashMap();
        hashmap1 = new HashMap();
        obj2 = (new StringBuilder()).append("SELECT ").append(getAllColumn()).append(" FROM t_download").toString();
        obj2 = DataBaseHelper.dbRawQuery(mContext, ((String) (obj2)), null);
        obj = obj2;
        obj1 = obj2;
        ((Cursor) (obj2)).moveToFirst();
_L2:
        obj = obj2;
        obj1 = obj2;
        if (((Cursor) (obj2)).getPosition() == ((Cursor) (obj2)).getCount())
        {
            break; /* Loop/switch isn't completed */
        }
        obj = obj2;
        obj1 = obj2;
        DownloadTask downloadtask = new DownloadTask();
        obj = obj2;
        obj1 = obj2;
        downloadtask.title = ((Cursor) (obj2)).getString(((Cursor) (obj2)).getColumnIndex("title"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.nickname = ((Cursor) (obj2)).getString(((Cursor) (obj2)).getColumnIndex("nickname"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.plays_counts = ((Cursor) (obj2)).getInt(((Cursor) (obj2)).getColumnIndex("play_times"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.favorites_counts = ((Cursor) (obj2)).getInt(((Cursor) (obj2)).getColumnIndex("likes"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.duration = ((Cursor) (obj2)).getLong(((Cursor) (obj2)).getColumnIndex("duration"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.create_at = ((Cursor) (obj2)).getLong(((Cursor) (obj2)).getColumnIndex("create_at"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.playUrl32 = ((Cursor) (obj2)).getString(((Cursor) (obj2)).getColumnIndex("playurl32"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.filesize = ((Cursor) (obj2)).getLong(((Cursor) (obj2)).getColumnIndex("filesize"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.downloaded = ((Cursor) (obj2)).getLong(((Cursor) (obj2)).getColumnIndex("downloaded"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.downloadStatus = ((Cursor) (obj2)).getInt(((Cursor) (obj2)).getColumnIndex("status"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.trackId = ((Cursor) (obj2)).getLong(((Cursor) (obj2)).getColumnIndex("track_id"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.playUrl64 = ((Cursor) (obj2)).getString(((Cursor) (obj2)).getColumnIndex("playurl64"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.orderNum = ((Cursor) (obj2)).getInt(((Cursor) (obj2)).getColumnIndex("orderNum"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.albumId = ((Cursor) (obj2)).getLong(((Cursor) (obj2)).getColumnIndex("albumId"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.albumName = ((Cursor) (obj2)).getString(((Cursor) (obj2)).getColumnIndex("albumName"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.uid = ((Cursor) (obj2)).getLong(((Cursor) (obj2)).getColumnIndex("uid"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.coverSmall = ((Cursor) (obj2)).getString(((Cursor) (obj2)).getColumnIndex("image_path"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.coverLarge = ((Cursor) (obj2)).getString(((Cursor) (obj2)).getColumnIndex("image_path_large"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.albumCoverPath = ((Cursor) (obj2)).getString(((Cursor) (obj2)).getColumnIndex("mAlbumImage"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.userCoverPath = ((Cursor) (obj2)).getString(((Cursor) (obj2)).getColumnIndex("userCoverPath"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.downLoadUrl = ((Cursor) (obj2)).getString(((Cursor) (obj2)).getColumnIndex("download_url"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.downloadLocation = ((Cursor) (obj2)).getString(((Cursor) (obj2)).getColumnIndex("file_location"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.downloadType = ((Cursor) (obj2)).getInt(((Cursor) (obj2)).getColumnIndex("downloadType"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.sequnceId = ((Cursor) (obj2)).getString(((Cursor) (obj2)).getColumnIndex("sequnceId"));
        obj = obj2;
        obj1 = obj2;
        downloadtask.is_favorited = TypeConvertHelpler.getBooleanFromInt(Integer.valueOf(((Cursor) (obj2)).getColumnIndex("is_favorited")));
        obj = obj2;
        obj1 = obj2;
        if (hashmap.containsKey(Long.valueOf(downloadtask.albumId)))
        {
            break MISSING_BLOCK_LABEL_735;
        }
        obj = obj2;
        obj1 = obj2;
        ArrayList arraylist = new ArrayList();
        obj = obj2;
        obj1 = obj2;
        hashmap.put(Long.valueOf(downloadtask.albumId), arraylist);
        obj = obj2;
        obj1 = obj2;
        ((ArrayList)hashmap.get(Long.valueOf(downloadtask.albumId))).add(downloadtask);
        obj = obj2;
        obj1 = obj2;
        hashmap1.put(Long.valueOf(downloadtask.trackId), downloadtask);
        obj = obj2;
        obj1 = obj2;
        ((Cursor) (obj2)).moveToNext();
        if (true) goto _L2; else goto _L1
        obj2;
        obj1 = obj;
        Logger.log("readDownloadList1", ((Throwable) (obj2)).toString(), true);
        if (obj == null) goto _L4; else goto _L3
_L3:
        ((Cursor) (obj)).close();
_L4:
        return;
_L1:
        obj = obj2;
        obj1 = obj2;
        QueryAlbumSoundOrders(hashmap, hashmap1);
        if (obj2 == null) goto _L4; else goto _L5
_L5:
        obj = obj2;
        if (true) goto _L3; else goto _L6
_L6:
        Exception exception;
        exception;
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
        throw exception;
    }

    public boolean contains(long l)
    {
        Object obj;
        Object obj1;
        Cursor cursor;
        boolean flag;
        boolean flag1;
        boolean flag2;
        obj = null;
        cursor = null;
        flag = false;
        flag2 = false;
        flag1 = false;
        obj1 = (new StringBuilder()).append("SELECT * FROM t_download WHERE track_id = '").append(l).append("'").toString();
        obj1 = DataBaseHelper.dbRawQuery(mContext, ((String) (obj1)), null);
        cursor = ((Cursor) (obj1));
        obj = obj1;
        int i = ((Cursor) (obj1)).getCount();
        flag = flag1;
        if (i > 0)
        {
            flag = true;
        }
        flag1 = flag;
        if (obj1 == null) goto _L2; else goto _L1
_L1:
        obj = obj1;
_L3:
        ((Cursor) (obj)).close();
        flag1 = flag;
_L2:
        return flag1;
        obj;
        if (cursor != null)
        {
            cursor.close();
        }
        throw obj;
        Throwable throwable;
        throwable;
        flag1 = flag2;
        if (obj == null) goto _L2; else goto _L3
    }

    public boolean deleteAllRows()
    {
        try
        {
            DataBaseHelper.dbDelete(mContext, "t_download", "1", null);
        }
        catch (Throwable throwable)
        {
            return false;
        }
        return true;
    }

    public boolean deleteRecord(long l)
    {
        try
        {
            DataBaseHelper.dbDelete(mContext, "t_download", "track_id = ?", new String[] {
                String.valueOf(l)
            });
        }
        catch (Throwable throwable)
        {
            return false;
        }
        return true;
    }

    public boolean deleteRecords(List list)
    {
        if (list == null || list.size() <= 0)
        {
            return false;
        }
        Iterator iterator = list.iterator();
        list = "";
        while (iterator.hasNext()) 
        {
            DownloadTask downloadtask = (DownloadTask)iterator.next();
            if (Utilities.isBlank(list))
            {
                list = (new StringBuilder()).append(list).append("(").toString();
            } else
            {
                list = (new StringBuilder()).append(list).append(", ").toString();
            }
            list = (new StringBuilder()).append(list).append(downloadtask.trackId).toString();
        }
        list = (new StringBuilder()).append(list).append(")").toString();
        list = (new StringBuilder()).append("track_id in ").append(list).toString();
        try
        {
            DataBaseHelper.dbDelete(mContext, "t_download", list, null);
        }
        // Misplaced declaration of an exception variable
        catch (List list)
        {
            return false;
        }
        return true;
    }

    public int getCount()
    {
        Object obj = DataBaseHelper.dbRawQuery(mContext, "SELECT *  FROM t_download", null);
        int j = ((Cursor) (obj)).getCount();
        int i = j;
        if (obj == null) goto _L2; else goto _L1
_L1:
        i = j;
_L4:
        ((Cursor) (obj)).close();
_L2:
        return i;
        obj;
        obj = null;
_L7:
        i = 0;
        j = 0;
        if (obj == null) goto _L2; else goto _L3
_L3:
        i = j;
          goto _L4
        obj;
        Object obj1;
        Object obj2;
        obj2 = null;
        obj1 = obj;
_L6:
        if (obj2 != null)
        {
            ((Cursor) (obj2)).close();
        }
        throw obj1;
        obj1;
        obj2 = obj;
        if (true) goto _L6; else goto _L5
_L5:
        Throwable throwable;
        throwable;
          goto _L7
    }

    public int hasDownloadCompleted(long l)
    {
        Object obj1;
        boolean flag;
        flag = true;
        obj1 = null;
        Object obj = DataBaseHelper.dbRawQuery(mContext, "SELECT downloaded, filesize FROM t_download WHERE track_id = ?", new String[] {
            String.valueOf(l)
        });
        obj1 = obj;
        if (((Cursor) (obj)).getCount() <= 0) goto _L2; else goto _L1
_L1:
        obj1 = obj;
        ((Cursor) (obj)).moveToFirst();
        obj1 = obj;
        l = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("downloaded"));
        obj1 = obj;
        long l1 = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("filesize"));
        byte byte0;
        byte0 = flag;
        if (l > 0L)
        {
            byte0 = flag;
            if (l == l1)
            {
                byte0 = 2;
            }
        }
_L4:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        return byte0;
_L2:
        byte0 = 0;
        if (true) goto _L4; else goto _L3
_L3:
        obj;
        obj = null;
_L6:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        return 0;
        Exception exception;
        exception;
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
        throw exception;
        Throwable throwable;
        throwable;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public final boolean isAlbumOrdersEmpty(ArrayList arraylist)
    {
        if (arraylist == null || arraylist.size() <= 1)
        {
            return false;
        }
        for (arraylist = arraylist.iterator(); arraylist.hasNext();)
        {
            if (((DownloadTask)arraylist.next()).orderNum > 0L)
            {
                return false;
            }
        }

        return true;
    }

    public boolean isDownloadCompleted(long l)
    {
        Object obj1;
        boolean flag;
        flag = true;
        obj1 = null;
        Object obj = DataBaseHelper.dbRawQuery(mContext, "SELECT * FROM t_download WHERE track_id = ? AND downloaded > 0 AND downloaded = filesize", new String[] {
            String.valueOf(l)
        });
        obj1 = obj;
        int i = ((Cursor) (obj)).getCount();
        if (i <= 0)
        {
            flag = false;
        }
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        return flag;
        obj;
        obj = null;
_L2:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        return false;
        Exception exception;
        exception;
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
        throw exception;
        Throwable throwable;
        throwable;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public final boolean isOrderNumberEmpty()
    {
        Object obj;
        Cursor cursor1;
        boolean flag;
        boolean flag1;
        boolean flag2;
        obj = null;
        cursor1 = null;
        flag = false;
        flag2 = false;
        flag1 = false;
        Cursor cursor = DataBaseHelper.dbRawQuery(mContext, "SELECT * FROM t_download WHERE orderNum > 0", null);
        cursor1 = cursor;
        obj = cursor;
        int i = cursor.getCount();
        if (i > 0)
        {
            flag = flag1;
        } else
        {
            flag = true;
        }
        flag1 = flag;
        if (cursor == null) goto _L2; else goto _L1
_L1:
        obj = cursor;
_L3:
        ((Cursor) (obj)).close();
        flag1 = flag;
_L2:
        return flag1;
        obj;
        if (cursor1 != null)
        {
            cursor1.close();
        }
        throw obj;
        Throwable throwable;
        throwable;
        flag1 = flag2;
        if (obj == null) goto _L2; else goto _L3
    }

    public final void onAppStart()
    {
        checkUpOrderNumbers();
    }

    public ArrayList readDownloadList(ExecutorService executorservice)
    {
        Object obj;
        Object obj1;
        ArrayList arraylist;
        obj = null;
        executorservice = null;
        arraylist = new ArrayList();
        obj1 = (new StringBuilder()).append("SELECT _download_id, ").append(getAllColumn()).append(" FROM t_download").toString();
        obj1 = DataBaseHelper.dbRawQuery(mContext, ((String) (obj1)), null);
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        ((Cursor) (obj1)).moveToFirst();
_L4:
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        if (((Cursor) (obj1)).getPosition() == ((Cursor) (obj1)).getCount()) goto _L2; else goto _L1
_L1:
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        DownloadTask downloadtask = new DownloadTask();
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.title = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("title"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.nickname = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("nickname"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.plays_counts = ((Cursor) (obj1)).getInt(((Cursor) (obj1)).getColumnIndex("play_times"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.favorites_counts = ((Cursor) (obj1)).getInt(((Cursor) (obj1)).getColumnIndex("likes"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.duration = ((Cursor) (obj1)).getLong(((Cursor) (obj1)).getColumnIndex("duration"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.create_at = ((Cursor) (obj1)).getLong(((Cursor) (obj1)).getColumnIndex("create_at"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.playUrl32 = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("playurl32"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.filesize = ((Cursor) (obj1)).getLong(((Cursor) (obj1)).getColumnIndex("filesize"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.downloaded = ((Cursor) (obj1)).getLong(((Cursor) (obj1)).getColumnIndex("downloaded"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.downloadStatus = ((Cursor) (obj1)).getInt(((Cursor) (obj1)).getColumnIndex("status"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.trackId = ((Cursor) (obj1)).getLong(((Cursor) (obj1)).getColumnIndex("track_id"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.playUrl64 = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("playurl64"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.orderNum = ((Cursor) (obj1)).getInt(((Cursor) (obj1)).getColumnIndex("orderNum"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.albumId = ((Cursor) (obj1)).getLong(((Cursor) (obj1)).getColumnIndex("albumId"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.albumName = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("albumName"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.uid = ((Cursor) (obj1)).getLong(((Cursor) (obj1)).getColumnIndex("uid"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.albumCoverPath = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("mAlbumImage"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.coverSmall = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("image_path"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.coverLarge = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("image_path_large"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.userCoverPath = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("userCoverPath"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.downLoadUrl = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("download_url"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.downloadLocation = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("file_location"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.downloadType = ((Cursor) (obj1)).getInt(((Cursor) (obj1)).getColumnIndex("downloadType"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.sequnceId = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("sequnceId"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.is_favorited = TypeConvertHelpler.getBooleanFromInt(Integer.valueOf(((Cursor) (obj1)).getInt(((Cursor) (obj1)).getColumnIndex("is_favorited"))));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.orderPositon = ((Cursor) (obj1)).getInt(((Cursor) (obj1)).getColumnIndex("order_position"));
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        if (!Utilities.isBlank(downloadtask.downloadLocation))
        {
            break MISSING_BLOCK_LABEL_729;
        }
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.downloadLocation = a.aj;
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        if (downloadtask.downloadStatus == -1)
        {
            break MISSING_BLOCK_LABEL_767;
        }
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        if (downloadtask.downloadStatus == 1)
        {
            break MISSING_BLOCK_LABEL_767;
        }
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        if (downloadtask.downloadStatus != 0)
        {
            break MISSING_BLOCK_LABEL_777;
        }
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        downloadtask.downloadStatus = 2;
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        arraylist.add(downloadtask);
        executorservice = ((ExecutorService) (obj1));
        obj = obj1;
        ((Cursor) (obj1)).moveToNext();
        if (true) goto _L4; else goto _L3
_L3:
        obj1;
        obj = executorservice;
        Logger.log("readDownloadList1", ((Throwable) (obj1)).toString(), true);
        if (executorservice == null) goto _L6; else goto _L5
_L5:
        executorservice.close();
_L6:
        return arraylist;
        executorservice;
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        throw executorservice;
_L2:
        if (obj1 == null) goto _L6; else goto _L7
_L7:
        executorservice = ((ExecutorService) (obj1));
        if (true) goto _L5; else goto _L8
_L8:
    }

    public ArrayList readDownloadList(ExecutorService executorservice, int i)
    {
        Cursor cursor;
        Cursor cursor1;
        Object obj;
        ArrayList arraylist;
        cursor1 = null;
        cursor = null;
        arraylist = new ArrayList();
        obj = (new StringBuilder()).append("SELECT ").append(getAllColumn()).append(" FROM t_download").toString();
        Logger.log("1122-2", (new StringBuilder()).append("[readDownloadList]==1=[").append(((String) (obj))).append("]").toString());
        obj = DataBaseHelper.dbRawQuery(mContext, ((String) (obj)), null);
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        ((Cursor) (obj)).moveToFirst();
_L4:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (((Cursor) (obj)).getPosition() == ((Cursor) (obj)).getCount()) goto _L2; else goto _L1
_L1:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        DownloadTask downloadtask = new DownloadTask();
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.title = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("title"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.nickname = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("nickname"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.plays_counts = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("play_times"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.favorites_counts = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("likes"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.duration = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("duration"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.create_at = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("create_at"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.playUrl32 = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("playurl32"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.filesize = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("filesize"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloaded = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("downloaded"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadStatus = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("status"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.trackId = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("track_id"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.playUrl64 = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("playurl64"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.orderNum = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("orderNum"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.albumId = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("albumId"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.albumName = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("albumName"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.uid = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("uid"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.coverSmall = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("image_path"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.coverLarge = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("image_path_large"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.albumCoverPath = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("mAlbumImage"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.userCoverPath = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("userCoverPath"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downLoadUrl = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("download_url"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadLocation = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("file_location"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.is_favorited = TypeConvertHelpler.getBooleanFromInt(Integer.valueOf(((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("is_favorited"))));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.orderPositon = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("order_position"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (!Utilities.isBlank(downloadtask.downloadLocation))
        {
            break MISSING_BLOCK_LABEL_858;
        }
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadLocation = a.aj;
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (downloadtask.downloadStatus == 2)
        {
            break MISSING_BLOCK_LABEL_921;
        }
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (downloadtask.downloadStatus == -1)
        {
            break MISSING_BLOCK_LABEL_921;
        }
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (downloadtask.downloadStatus == 1)
        {
            break MISSING_BLOCK_LABEL_921;
        }
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (downloadtask.downloadStatus != 0)
        {
            break MISSING_BLOCK_LABEL_949;
        }
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadStatus = 0;
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        executorservice.execute(downloadtask);
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        arraylist.add(downloadtask);
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        ((Cursor) (obj)).moveToNext();
        if (true) goto _L4; else goto _L3
_L3:
        executorservice;
        cursor1 = cursor;
        Logger.log("readDownloadList1", executorservice.toString(), true);
        if (cursor == null) goto _L6; else goto _L5
_L5:
        cursor.close();
_L6:
        Logger.log("1122-2", (new StringBuilder()).append("[readDownloadList]==1=[").append(arraylist.size()).append("]").toString());
        return arraylist;
        executorservice;
        if (cursor1 != null)
        {
            cursor1.close();
        }
        throw executorservice;
_L2:
        if (obj == null) goto _L6; else goto _L7
_L7:
        cursor = ((Cursor) (obj));
        if (true) goto _L5; else goto _L8
_L8:
    }

    public ArrayList readDownloadList(ExecutorService executorservice, int i, boolean flag)
    {
        Cursor cursor;
        Cursor cursor1;
        Object obj;
        ArrayList arraylist;
        cursor1 = null;
        cursor = null;
        i = 0;
        arraylist = new ArrayList();
        obj = (new StringBuilder()).append("SELECT ").append(getAllColumn()).append(" FROM t_download").toString();
        obj = DataBaseHelper.dbRawQuery(mContext, ((String) (obj)), null);
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        ((Cursor) (obj)).moveToFirst();
_L8:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (((Cursor) (obj)).getPosition() == ((Cursor) (obj)).getCount()) goto _L2; else goto _L1
_L1:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        DownloadTask downloadtask = new DownloadTask();
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.title = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("title"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.nickname = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("nickname"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.plays_counts = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("play_times"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.favorites_counts = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("likes"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.duration = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("duration"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.create_at = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("create_at"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.playUrl32 = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("playurl32"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.filesize = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("filesize"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloaded = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("downloaded"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadStatus = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("status"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.trackId = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("track_id"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.playUrl64 = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("playurl64"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.orderNum = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("orderNum"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.albumId = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("albumId"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.albumName = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("albumName"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.uid = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("uid"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.coverSmall = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("image_path"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.coverLarge = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("image_path_large"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.albumCoverPath = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("mAlbumImage"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.userCoverPath = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("userCoverPath"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downLoadUrl = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("download_url"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadLocation = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("file_location"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadType = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("downloadType"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.sequnceId = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("sequnceId"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.is_favorited = TypeConvertHelpler.getBooleanFromInt(Integer.valueOf(((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("is_favorited"))));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.orderPositon = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("order_position"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (!Utilities.isBlank(downloadtask.downloadLocation))
        {
            break MISSING_BLOCK_LABEL_915;
        }
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadLocation = a.aj;
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (downloadtask.downloadStatus == 2) goto _L4; else goto _L3
_L3:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (downloadtask.downloadStatus == -1) goto _L4; else goto _L5
_L5:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (downloadtask.downloadStatus == 1) goto _L4; else goto _L6
_L6:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (downloadtask.downloadStatus != 0) goto _L7; else goto _L4
_L4:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadStatus = 0;
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        executorservice.execute(downloadtask);
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        arraylist.add(0, downloadtask);
_L11:
        int j;
        j = i;
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_1091;
        }
        j = i;
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (downloadtask.orderNum != 0L)
        {
            break MISSING_BLOCK_LABEL_1091;
        }
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.orderNum = i;
        j = i + 1;
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        updateSoundRecord(downloadtask);
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        ((Cursor) (obj)).moveToNext();
        i = j;
          goto _L8
        executorservice;
        cursor1 = cursor;
        Logger.log("readDownloadList1", executorservice.toString(), true);
        if (cursor == null) goto _L10; else goto _L9
_L9:
        cursor.close();
_L10:
        return arraylist;
_L7:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        arraylist.add(downloadtask);
          goto _L11
        executorservice;
        if (cursor1 != null)
        {
            cursor1.close();
        }
        throw executorservice;
_L2:
        if (obj == null) goto _L10; else goto _L12
_L12:
        cursor = ((Cursor) (obj));
          goto _L9
    }

    public ArrayList readDownloadList(ExecutorService executorservice, boolean flag)
    {
        Cursor cursor;
        Cursor cursor1;
        Object obj;
        ArrayList arraylist;
        int i;
        cursor1 = null;
        cursor = null;
        i = 0;
        arraylist = new ArrayList();
        obj = (new StringBuilder()).append("SELECT ").append(getAllColumn()).append(" FROM t_download").toString();
        obj = DataBaseHelper.dbRawQuery(mContext, ((String) (obj)), null);
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        ((Cursor) (obj)).moveToFirst();
_L7:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (((Cursor) (obj)).getPosition() == ((Cursor) (obj)).getCount()) goto _L2; else goto _L1
_L1:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        DownloadTask downloadtask = new DownloadTask();
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.title = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("title"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.nickname = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("nickname"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.plays_counts = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("play_times"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.favorites_counts = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("likes"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.duration = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("duration"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.create_at = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("create_at"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.playUrl32 = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("playurl32"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.filesize = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("filesize"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloaded = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("downloaded"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadStatus = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("status"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.trackId = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("track_id"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.playUrl64 = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("playurl64"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.orderNum = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("orderNum"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.albumId = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("albumId"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.albumName = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("albumName"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.uid = ((Cursor) (obj)).getLong(((Cursor) (obj)).getColumnIndex("uid"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.albumCoverPath = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("mAlbumImage"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.coverSmall = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("image_path"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.coverLarge = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("image_path_large"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.userCoverPath = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("userCoverPath"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downLoadUrl = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("download_url"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadLocation = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("file_location"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadType = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("downloadType"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.sequnceId = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("sequnceId"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.is_favorited = TypeConvertHelpler.getBooleanFromInt(Integer.valueOf(((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("is_favorited"))));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.orderPositon = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("order_position"));
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (!Utilities.isBlank(downloadtask.downloadLocation))
        {
            break MISSING_BLOCK_LABEL_884;
        }
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadLocation = a.aj;
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (downloadtask.downloadStatus == -1) goto _L4; else goto _L3
_L3:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (downloadtask.downloadStatus == 1) goto _L4; else goto _L5
_L5:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (downloadtask.downloadStatus != 0) goto _L6; else goto _L4
_L4:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.downloadStatus = 0;
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        executorservice.execute(downloadtask);
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        arraylist.add(0, downloadtask);
_L10:
        int j;
        j = i;
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_1038;
        }
        j = i;
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        if (downloadtask.orderNum != 0L)
        {
            break MISSING_BLOCK_LABEL_1038;
        }
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        downloadtask.orderNum = i;
        j = i + 1;
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        updateSoundRecord(downloadtask);
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        ((Cursor) (obj)).moveToNext();
        i = j;
          goto _L7
        executorservice;
        cursor1 = cursor;
        Logger.log("readDownloadList1", executorservice.toString(), true);
        if (cursor == null) goto _L9; else goto _L8
_L8:
        cursor.close();
_L9:
        return arraylist;
_L6:
        cursor = ((Cursor) (obj));
        cursor1 = ((Cursor) (obj));
        arraylist.add(downloadtask);
          goto _L10
        executorservice;
        if (cursor1 != null)
        {
            cursor1.close();
        }
        throw executorservice;
_L2:
        if (obj == null) goto _L9; else goto _L11
_L11:
        cursor = ((Cursor) (obj));
          goto _L8
    }

    public final List save(List list)
    {
        if (list == null || list.size() <= 0)
        {
            return null;
        }
        ArrayList arraylist = new ArrayList();
        ContentValues contentvalues;
        for (list = list.iterator(); list.hasNext(); arraylist.add(contentvalues))
        {
            DownloadTask downloadtask = (DownloadTask)list.next();
            contentvalues = new ContentValues();
            contentvalues.put("title", downloadtask.title);
            contentvalues.put("nickname", downloadtask.nickname);
            contentvalues.put("play_times", Integer.valueOf(downloadtask.plays_counts));
            contentvalues.put("likes", Integer.valueOf(downloadtask.favorites_counts));
            contentvalues.put("duration", Double.valueOf(downloadtask.duration));
            contentvalues.put("create_at", Long.valueOf(downloadtask.create_at));
            contentvalues.put("playurl32", downloadtask.playUrl32);
            contentvalues.put("filesize", Long.valueOf(downloadtask.filesize));
            contentvalues.put("downloaded", Long.valueOf(downloadtask.downloaded));
            contentvalues.put("track_id", Long.valueOf(downloadtask.trackId));
            contentvalues.put("image_path", downloadtask.coverSmall);
            contentvalues.put("image_path_large", downloadtask.coverLarge);
            contentvalues.put("playurl64", downloadtask.playUrl64);
            contentvalues.put("orderNum", Long.valueOf(downloadtask.orderNum));
            contentvalues.put("albumId", Long.valueOf(downloadtask.albumId));
            contentvalues.put("albumName", downloadtask.albumName);
            contentvalues.put("uid", Long.valueOf(downloadtask.uid));
            contentvalues.put("mAlbumImage", downloadtask.albumCoverPath);
            contentvalues.put("status", Integer.valueOf(downloadtask.downloadStatus));
            contentvalues.put("userCoverPath", downloadtask.userCoverPath);
            if (Utilities.isBlank(downloadtask.downloadLocation))
            {
                downloadtask.downloadLocation = a.aj;
            }
            contentvalues.put("download_url", downloadtask.downLoadUrl);
            contentvalues.put("file_location", downloadtask.downloadLocation);
            contentvalues.put("downloadType", Integer.valueOf(downloadtask.downloadType));
            contentvalues.put("sequnceId", downloadtask.sequnceId);
            contentvalues.put("is_favorited", Boolean.valueOf(downloadtask.is_favorited));
            contentvalues.put("order_position", Integer.valueOf(downloadtask.orderPositon));
        }

        return DataBaseHelper.dbBatchInsert(mContext, "t_download", arraylist, "track_id");
    }

    public boolean save(DownloadTask downloadtask)
    {
        boolean flag = false;
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("title", downloadtask.title);
        contentvalues.put("nickname", downloadtask.nickname);
        contentvalues.put("play_times", Integer.valueOf(downloadtask.plays_counts));
        contentvalues.put("likes", Integer.valueOf(downloadtask.favorites_counts));
        contentvalues.put("duration", Double.valueOf(downloadtask.duration));
        contentvalues.put("create_at", Long.valueOf(downloadtask.create_at));
        contentvalues.put("playurl32", downloadtask.playUrl32);
        contentvalues.put("filesize", Long.valueOf(downloadtask.filesize));
        contentvalues.put("downloaded", Long.valueOf(downloadtask.downloaded));
        contentvalues.put("track_id", Long.valueOf(downloadtask.trackId));
        contentvalues.put("image_path", downloadtask.coverSmall);
        contentvalues.put("image_path_large", downloadtask.coverLarge);
        contentvalues.put("playurl64", downloadtask.playUrl64);
        contentvalues.put("orderNum", Long.valueOf(downloadtask.orderNum));
        contentvalues.put("albumId", Long.valueOf(downloadtask.albumId));
        contentvalues.put("albumName", downloadtask.albumName);
        contentvalues.put("uid", Long.valueOf(downloadtask.uid));
        contentvalues.put("mAlbumImage", downloadtask.albumCoverPath);
        contentvalues.put("status", Integer.valueOf(downloadtask.downloadStatus));
        contentvalues.put("userCoverPath", downloadtask.userCoverPath);
        contentvalues.put("download_url", downloadtask.downLoadUrl);
        contentvalues.put("file_location", downloadtask.downloadLocation);
        contentvalues.put("downloadType", Integer.valueOf(downloadtask.downloadType));
        contentvalues.put("sequnceId", downloadtask.sequnceId);
        contentvalues.put("is_favorited", Boolean.valueOf(downloadtask.is_favorited));
        contentvalues.put("order_position", Integer.valueOf(downloadtask.orderPositon));
        long l;
        try
        {
            l = DataBaseHelper.dbInsert(mContext, "t_download", contentvalues);
        }
        // Misplaced declaration of an exception variable
        catch (DownloadTask downloadtask)
        {
            return false;
        }
        if (l > 0L)
        {
            flag = true;
        }
        return flag;
    }

    public boolean updateFavoritedInDB(long l, boolean flag)
    {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("is_favorited", Boolean.valueOf(flag));
        try
        {
            DataBaseHelper.dbUpdate(mContext, "t_download", contentvalues, "track_id = ?", new String[] {
                String.valueOf(l)
            });
        }
        catch (Throwable throwable)
        {
            return false;
        }
        return true;
    }

    public void updateOrderPosition(List list)
    {
        ArrayList arraylist = new ArrayList(list.size());
        ArrayList arraylist1 = new ArrayList(list.size());
        DownloadTask downloadtask;
        for (list = list.iterator(); list.hasNext(); arraylist1.add(new String[] {
    String.valueOf(((SoundInfo) (downloadtask)).trackId)
}))
        {
            downloadtask = (DownloadTask)list.next();
            ContentValues contentvalues = new ContentValues(1);
            contentvalues.put("order_position", Integer.valueOf(((SoundInfo) (downloadtask)).orderPositon));
            arraylist.add(contentvalues);
        }

        DataBaseHelper.dbBatchUpdate(mContext, "t_download", arraylist, "track_id = ?", arraylist1);
    }

    public boolean updateSoundRecord(DownloadTask downloadtask)
    {
        long l = downloadtask.trackId;
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("title", downloadtask.title);
        contentvalues.put("nickname", downloadtask.nickname);
        contentvalues.put("play_times", Integer.valueOf(downloadtask.plays_counts));
        contentvalues.put("likes", Integer.valueOf(downloadtask.favorites_counts));
        contentvalues.put("duration", Double.valueOf(downloadtask.duration));
        contentvalues.put("create_at", Long.valueOf(downloadtask.create_at));
        contentvalues.put("playurl32", downloadtask.playUrl32);
        contentvalues.put("filesize", Long.valueOf(downloadtask.filesize));
        contentvalues.put("downloaded", Long.valueOf(downloadtask.downloaded));
        contentvalues.put("playurl64", downloadtask.playUrl64);
        contentvalues.put("orderNum", Long.valueOf(downloadtask.orderNum));
        contentvalues.put("albumId", Long.valueOf(downloadtask.albumId));
        contentvalues.put("albumName", downloadtask.albumName);
        contentvalues.put("uid", Long.valueOf(downloadtask.uid));
        contentvalues.put("mAlbumImage", downloadtask.albumCoverPath);
        contentvalues.put("status", Integer.valueOf(downloadtask.downloadStatus));
        contentvalues.put("download_url", downloadtask.downLoadUrl);
        contentvalues.put("file_location", downloadtask.downloadLocation);
        contentvalues.put("downloadType", Integer.valueOf(downloadtask.downloadType));
        contentvalues.put("sequnceId", downloadtask.sequnceId);
        contentvalues.put("is_favorited", Boolean.valueOf(downloadtask.is_favorited));
        contentvalues.put("order_position", Integer.valueOf(downloadtask.orderPositon));
        try
        {
            DataBaseHelper.dbUpdate(mContext, "t_download", contentvalues, "track_id = ?", new String[] {
                String.valueOf(l)
            });
        }
        // Misplaced declaration of an exception variable
        catch (DownloadTask downloadtask)
        {
            return false;
        }
        return true;
    }
}
