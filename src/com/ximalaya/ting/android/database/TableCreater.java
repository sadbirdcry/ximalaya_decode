// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.database;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.ximalaya.ting.android.util.Logger;

public class TableCreater
{

    private static final String TAG = "TableCreater";

    public TableCreater()
    {
    }

    public static void createBind(SQLiteDatabase sqlitedatabase)
    {
        try
        {
            sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_bind(_id integer primary key autoincrement,thirdpartyname varchar(200),thirdpartyId varchar(200),thirdpartynickname varchar(200),isexpired varchar(200),homepage varchar(200),thirdpartyuid varchar(200))");
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            Logger.log("TableCreater", sqlitedatabase.toString());
        }
        Logger.log("TableCreater", "Create Table.......t_bind");
    }

    public static void createCacheFeed(SQLiteDatabase sqlitedatabase)
    {
        try
        {
            sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_page_cache_feed(sid integer NOT NULL PRIMARY KEY AUTOINCREMENT, title varchar(200), uid varchar(200), image BLOB, nickname varchar(200), play_times integer, likes integer, duration integer, create_at integer, playurl32 varchar(200), track_id integer, time_line double, image_url text, total_size integer,source integer,comments integer, playurl64 varchar(200));");
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            Logger.log("TableCreater", sqlitedatabase.toString());
        }
        Logger.log("TableCreater", "Create Table.......t_page_cache_feed");
    }

    public static void createCacheHomePage(SQLiteDatabase sqlitedatabase)
    {
        try
        {
            sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_page_cache_homepage(sid integer NOT NULL PRIMARY KEY AUTOINCREMENT, uid integer, nickname varchar(200), largeLogo BLOB, smallLogo BLOB, followings integer, events integer, followingTags integer, tracks integer, messages integer, followers integer, noReadFollowers integer, leters integer, albums integer, favorites integer, isFollowed integer, personalSignature text, ptitle varchar(200), large_logo_url text, small_logo_url text);");
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            Logger.log("TableCreater", sqlitedatabase.toString());
        }
        Logger.log("TableCreater", "Create Table.......t_page_cache_homepage");
    }

    public static void createDownload(SQLiteDatabase sqlitedatabase)
    {
        try
        {
            sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_download(_download_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, title varchar(200), image BLOB, nickname varchar(200), play_times integer, likes integer, duration integer, create_at integer, playurl32 varchar(200), file_dir varchar(200), filename varchar(200), filesize integer, downloaded integer, status integer, track_id integer, image_path text, image_path_large text,playurl64 varchar(200),orderNum integer,albumId varchar(200),albumName varchar(200),uid varchar(200),mAlbumImage varchar(200),userCoverPath varchar(200),download_url varchar(200),file_location varchar(200),downloadType integer,sequnceId varchar(200),is_favorited integer,order_position integer);");
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            Logger.log("TableCreater", sqlitedatabase.toString());
        }
        Logger.log("TableCreater", "Create Table.......t_download");
    }

    public static void createPageCacheLiked(SQLiteDatabase sqlitedatabase)
    {
        try
        {
            sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_page_cache_liked(sid integer NOT NULL PRIMARY KEY AUTOINCREMENT, title varchar(200), uid varchar(200), image BLOB, nickname varchar(200), play_times integer, likes integer, duration integer, create_at integer, playurl32 varchar(200), track_id integer, image_url text,source integer,comments integer, playurl64 varchar(200));");
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            Logger.log("TableCreater", sqlitedatabase.toString());
        }
        Logger.log("TableCreater", "Create Table.......t_page_cache_liked");
    }

    public static void createPageCacheListened(SQLiteDatabase sqlitedatabase)
    {
        try
        {
            sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_page_cache_listened(sid integer NOT NULL PRIMARY KEY AUTOINCREMENT, title varchar(200), uid varchar(200), image BLOB, nickname varchar(200), play_times integer, likes integer, duration integer, create_at integer, playurl32 varchar(200), track_id integer, image_url text,source integer,comments integer, playurl64 varchar(200));");
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            Logger.log("TableCreater", sqlitedatabase.toString());
        }
        Logger.log("TableCreater", "Create Table.......t_page_cache_listened");
    }

    public static void createPageCacheMySounds(SQLiteDatabase sqlitedatabase)
    {
        try
        {
            sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_page_cache_mysounds(sid integer NOT NULL PRIMARY KEY AUTOINCREMENT, title varchar(200), uid varchar(200), image BLOB, nickname varchar(200), play_times integer, likes integer, duration integer, create_at integer, playurl32 varchar(200), track_id integer, image_url text,processstate varchar(200), source integer,comments integer, playurl64 varchar(200));");
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            Logger.log("TableCreater", sqlitedatabase.toString());
        }
        Logger.log("TableCreater", "Create Table.......t_page_cache_mysounds");
    }

    public static void createRecorder(SQLiteDatabase sqlitedatabase)
    {
        try
        {
            sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_recorder(_id integer NOT NULL PRIMARY KEY AUTOINCREMENT,uid long ,audio_path  varchar(200)   ,image_path  varchar(200)   ,title      varchar(200)   ,categoryId varchar(200)   ,source     varchar(200)   ,tags       varchar(200)   ,intro      varchar(200)   ,isPublic   varchar(200)   ,needUpload varchar(200)   ,file_uploaded varchar(200)   ,form_uploaded varchar(200)   ,duration \t varchar(200)   ,image      \t BLOB ,create_at \t varchar(200)   unique ,image_id      varchar(50)           ,form_id       long                  ,album_id      long                  ,album_title   varchar(100)          ,processState INTEGER                ,activity_id long                    ,audioId long)");
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            Logger.log("TableCreater", sqlitedatabase.toString());
        }
        Logger.log("TableCreater", "Create Table.......t_recorder");
    }

    public static void createSearch(SQLiteDatabase sqlitedatabase)
    {
        try
        {
            sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_search(_id integer primary key autoincrement,account varchar(200))");
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            Logger.log("TableCreater", sqlitedatabase.toString());
        }
        Logger.log("TableCreater", "Create Table.......t_search");
    }

    public static void createTables(SQLiteDatabase sqlitedatabase)
    {
        createCacheFeed(sqlitedatabase);
        createCacheHomePage(sqlitedatabase);
        createPageCacheLiked(sqlitedatabase);
        createPageCacheMySounds(sqlitedatabase);
        createPageCacheListened(sqlitedatabase);
        createDownload(sqlitedatabase);
        createRecorder(sqlitedatabase);
        createSearch(sqlitedatabase);
        createBind(sqlitedatabase);
        createUser(sqlitedatabase);
    }

    public static void createUser(SQLiteDatabase sqlitedatabase)
    {
        try
        {
            sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_user(_id integer primary key autoincrement,account varchar(200),password varchar(200),rememberme varchar(200),uid varchar(200),token varchar(200),nickname varchar(200),email varchar(200),isverified varchar(200),thirdpartyName varchar(200),largeLogo varchar(200),middleLogo varchar(200),smallLogo varchar(200),expiresIn varchar(200),expiresDate varchar(200))");
            Logger.log("TableCreater", "Create Table.......t_user");
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            Logger.log("TableCreater", sqlitedatabase.toString());
        }
        Logger.log("TableCreater", "Create Table.......t_user");
    }
}
