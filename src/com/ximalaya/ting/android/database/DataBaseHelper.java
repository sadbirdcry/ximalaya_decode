// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.Utilities;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.database:
//            TableCreater

public class DataBaseHelper extends SQLiteOpenHelper
{

    private static final int DATABASE_VERSION = 13;
    private static String DB_NAME = "ting.db";
    private static final Object INSTANCE_LOCK = new Object();
    private static volatile int count = 0;
    private static DataBaseHelper databaseHelper = null;

    private DataBaseHelper(Context context)
    {
        super(context, DB_NAME, null, 13);
    }

    public static void cleanOnAppStart(Context context)
    {
        synchronized (INSTANCE_LOCK)
        {
            (new DataBaseHelper(context)).close();
        }
        return;
        context;
        obj;
        JVM INSTR monitorexit ;
        throw context;
    }

    public static final List dbBatchInsert(Context context, String s, List list)
    {
        return dbBatchInsert(context, s, list, null);
    }

    public static final List dbBatchInsert(Context context, String s, List list, String s1)
    {
        ArrayList arraylist;
        DataBaseHelper databasehelper;
        if (Utilities.isBlank(s) || list == null || list.size() <= 0)
        {
            return null;
        }
        arraylist = new ArrayList();
        databasehelper = getInstance(context);
        context = databasehelper.getWritableDatabase();
        context.beginTransaction();
        list = list.iterator();
_L6:
        boolean flag = list.hasNext();
        if (!flag) goto _L2; else goto _L1
_L1:
        ContentValues contentvalues;
        String s2;
        contentvalues = (ContentValues)list.next();
        s2 = contentvalues.getAsString(s1);
        if (!Utilities.isNotBlank(s1) || !contentvalues.containsKey(s1)) goto _L4; else goto _L3
_L3:
        flag = databasehelper.existsRecordIntable(context, s, s1, s2);
        if (!flag) goto _L4; else goto _L5
_L5:
        flag = false;
_L7:
        arraylist.add(Boolean.valueOf(flag));
          goto _L6
        s;
        Logger.log("dbInsert", s.getMessage());
        context.endTransaction();
_L8:
        release();
        return arraylist;
_L4:
        long l = context.insert(s, null, contentvalues);
        Throwable throwable;
        if (l > 0L)
        {
            flag = true;
        } else
        {
            flag = false;
        }
          goto _L7
        throwable;
        arraylist.add(Boolean.valueOf(false));
          goto _L6
        s;
        context.endTransaction();
        release();
        throw s;
        s;
        arraylist.add(Boolean.valueOf(false));
        throw s;
_L2:
        context.setTransactionSuccessful();
        context.endTransaction();
          goto _L8
    }

    public static void dbBatchUpdate(Context context, String s, ArrayList arraylist, String s1, ArrayList arraylist1)
    {
        Object obj;
        obj = getInstance(context);
        context = null;
        obj = ((DataBaseHelper) (obj)).getWritableDatabase();
        context = ((Context) (obj));
        context.beginTransaction();
        int i = 0;
_L2:
        if (i >= arraylist.size() || i >= arraylist1.size())
        {
            break; /* Loop/switch isn't completed */
        }
        context.update(s, (ContentValues)arraylist.get(i), s1, (String[])arraylist1.get(i));
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        context.setTransactionSuccessful();
        context.endTransaction();
        if (context != null)
        {
            context.close();
        }
_L4:
        return;
        s;
_L7:
        s.printStackTrace();
        context.endTransaction();
        if (context == null) goto _L4; else goto _L3
_L3:
        context.close();
        return;
        s;
        context = null;
_L6:
        context.endTransaction();
        if (context != null)
        {
            context.close();
        }
        throw s;
        s;
        continue; /* Loop/switch isn't completed */
        s;
        if (true) goto _L6; else goto _L5
_L5:
        s;
          goto _L7
    }

    public static int dbDelete(Context context, String s, String s1, String as[])
        throws Throwable
    {
        context = getInstance(context);
        int i = context.getWritableDatabase().delete(s, s1, as);
        release();
        return i;
        context;
        Logger.log("dbDelete", context.getMessage());
        throw context;
        context;
        release();
        throw context;
    }

    public static void dbExcuteSQL(Context context, String s)
        throws Throwable
    {
        context = getInstance(context);
        context.getWritableDatabase().execSQL(s);
        release();
        return;
        context;
        Logger.log("dbExcuteSQL1", context.getMessage());
        throw context;
        context;
        release();
        throw context;
    }

    public static void dbExcuteSQL(Context context, String s, Object aobj[])
        throws Throwable
    {
        context = getInstance(context);
        context.getWritableDatabase().execSQL(s, aobj);
        release();
        return;
        context;
        Logger.log("dbExcuteSQL2", context.getMessage());
        throw context;
        context;
        release();
        throw context;
    }

    public static long dbInsert(Context context, String s, ContentValues contentvalues)
        throws Throwable
    {
        context = getInstance(context);
        long l = context.getWritableDatabase().insert(s, null, contentvalues);
        release();
        return l;
        context;
        Logger.log("dbInsert", context.getMessage());
        throw context;
        context;
        release();
        throw context;
    }

    public static Cursor dbRawQuery(Context context, String s, String as[])
        throws Throwable
    {
        context = getInstance(context);
        context = context.getReadableDatabase().rawQuery(s, as);
        context.getCount();
_L2:
        release();
        return context;
        context;
        Logger.log("dbRawQuery", context.getMessage());
        context = null;
        if (true) goto _L2; else goto _L1
_L1:
        context;
        release();
        throw context;
    }

    public static int dbUpdate(Context context, String s, ContentValues contentvalues, String s1, String as[])
        throws Throwable
    {
        context = getInstance(context);
        int i = context.getWritableDatabase().update(s, contentvalues, s1, as);
        release();
        return i;
        context;
        Logger.log("dbUpdate", context.getMessage());
        throw context;
        context;
        release();
        throw context;
    }

    private boolean existsColumnInTable(SQLiteDatabase sqlitedatabase, String s, String s1)
    {
        boolean flag = false;
        int i;
        try
        {
            i = sqlitedatabase.rawQuery((new StringBuilder()).append("SELECT * FROM ").append(s).append(" LIMIT 0").toString(), null).getColumnIndex(s1);
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            Logger.log("... - existsColumnInTable", (new StringBuilder()).append("When checking whether a column exists in the table, an error occurred: ").append(sqlitedatabase.getMessage()).toString());
            return false;
        }
        if (i != -1)
        {
            flag = true;
        }
        return flag;
    }

    private static DataBaseHelper getInstance(Context context)
    {
        synchronized (INSTANCE_LOCK)
        {
            count++;
            if (databaseHelper == null)
            {
                databaseHelper = new DataBaseHelper(context);
                count = 1;
            }
        }
        return databaseHelper;
        context;
        obj;
        JVM INSTR monitorexit ;
        throw context;
    }

    private static void release()
    {
        if (databaseHelper == null)
        {
            break MISSING_BLOCK_LABEL_50;
        }
        Object obj = INSTANCE_LOCK;
        obj;
        JVM INSTR monitorenter ;
        int i;
        if (databaseHelper == null)
        {
            break MISSING_BLOCK_LABEL_42;
        }
        i = count - 1;
        count = i;
        if (i != 0)
        {
            break MISSING_BLOCK_LABEL_42;
        }
        databaseHelper.close();
        databaseHelper = null;
        obj;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public static void releaseOnExit()
    {
        if (databaseHelper != null)
        {
            databaseHelper.close();
            databaseHelper = null;
        }
    }

    private void upgradeDb(SQLiteDatabase sqlitedatabase)
    {
        sqlitedatabase.execSQL("drop table IF EXISTS t_offline_caching;");
        if (!existsColumnInTable(sqlitedatabase, "t_recorder", "uid"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_recorder ADD COLUMN uid long");
            Logger.log("t_recorder \u4E0D\u5B58\u5728 uid");
        } else
        {
            Logger.log("t_recorder \u5B58\u5728 uid");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_recorder", "activity_id"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_recorder ADD COLUMN activity_id long");
            Logger.log("t_recorder \u4E0D\u5B58\u5728 activity_id");
        } else
        {
            Logger.log("t_recorder \u5B58\u5728 activity_id");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_recorder", "processState"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_recorder ADD COLUMN processState INTEGER");
            Logger.log("t_recorder \u4E0D\u5B58\u5728 processState");
        } else
        {
            Logger.log("t_recorder \u5B58\u5728 processState");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_recorder", "audioId"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_recorder ADD COLUMN audioId long");
            Logger.log("t_recorder \u4E0D\u5B58\u5728 audioId");
        } else
        {
            Logger.log("t_recorder \u5B58\u5728 audioId");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_recorder", "image_id"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_recorder ADD COLUMN image_id varchar(50)");
            Logger.log("t_recorder \u4E0D\u5B58\u5728 image_id");
        } else
        {
            Logger.log("t_recorder \u5B58\u5728 image_id");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_recorder", "album_id"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_recorder ADD COLUMN album_id long");
            Logger.log("t_recorder \u4E0D\u5B58\u5728 album_id");
        } else
        {
            Logger.log("t_recorder \u5B58\u5728 album_id");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_recorder", "album_title"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_recorder ADD COLUMN album_title varchar(100)");
            Logger.log("t_recorder \u4E0D\u5B58\u5728 album_title");
        } else
        {
            Logger.log("t_recorder \u5B58\u5728 album_title");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_recorder", "form_id"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_recorder ADD COLUMN form_id long");
            Logger.log("t_recorder \u4E0D\u5B58\u5728 form_id");
        } else
        {
            Logger.log("t_recorder \u5B58\u5728 form_id");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_download", "userCoverPath"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_download ADD COLUMN userCoverPath varchar(200)");
            Logger.log("t_download \u4E0D\u5B58\u5728 userCoverPath");
        } else
        {
            Logger.log("t_download \u5B58\u5728 userCoverPath");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_download", "image_path_large"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_download ADD COLUMN image_path_large text");
            Logger.log("t_download \u4E0D\u5B58\u5728 image_path_large");
        } else
        {
            Logger.log("t_download \u5B58\u5728 image_path_large");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_download", "download_url"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_download ADD COLUMN download_url varchar(200)");
            Logger.log("t_download \u4E0D\u5B58\u5728 download_url");
        } else
        {
            Logger.log("t_download \u5B58\u5728 download_url");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_download", "file_location"))
        {
            sqlitedatabase.execSQL((new StringBuilder()).append("ALTER TABLE t_download ADD COLUMN file_location varchar(200) DEFAULT '").append(a.aj).append("'").toString());
            Logger.log("t_download \u4E0D\u5B58\u5728 file_location");
        } else
        {
            Logger.log("t_download \u5B58\u5728 file_location");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_download", "downloadType"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_download ADD COLUMN downloadType integer");
            Logger.log("t_download \u4E0D\u5B58\u5728 downloadType");
        } else
        {
            Logger.log("t_download \u5B58\u5728 downloadType");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_download", "sequnceId"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_download ADD COLUMN sequnceId varchar(200)");
            Logger.log("t_download \u4E0D\u5B58\u5728 sequnceId");
        } else
        {
            Logger.log("t_download \u5B58\u5728 sequnceId");
        }
        if (!existsColumnInTable(sqlitedatabase, "t_download", "is_favorited"))
        {
            sqlitedatabase.execSQL("ALTER TABLE t_download ADD COLUMN is_favorited integer");
            Logger.log("t_download \u4E0D\u5B58\u5728 is_favorited");
            return;
        } else
        {
            Logger.log("t_download \u5B58\u5728 is_favorited");
            return;
        }
    }

    private void upgradeFrom1To2(SQLiteDatabase sqlitedatabase)
    {
        sqlitedatabase.execSQL("ALTER TABLE t_download RENAME TO __temp__t_download;");
        sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_download(_download_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, title varchar(200), image BLOB, nickname varchar(200), play_times integer, likes integer, duration integer, create_at integer, playurl32 varchar(200), file_dir varchar(200), filename varchar(200), filesize integer, downloaded integer, status integer, track_id integer, image_path text, playurl64 varchar(200),orderNum integer DEFAULT 0, albumId varchar(200) DEFAULT '0',albumName varchar(200) DEFAULT '',uid varchar(200) DEFAULT '0',mAlbumImage varchar(200) DEFAULT '');");
        sqlitedatabase.execSQL("INSERT INTO t_download(_download_id, title, image, nickname, play_times, likes, duration, create_at, playurl32, file_dir, filename, filesize, downloaded, status, track_id, image_path, playurl64) SELECT _download_id, title, image, nickname, play_times, likes, duration, create_at, playurl, file_dir, filename, filesize, downloaded, status, track_id, image_path, playurl  FROM __temp__t_download;");
        sqlitedatabase.execSQL("drop table __temp__t_download;");
        sqlitedatabase.execSQL("ALTER TABLE t_offline_caching RENAME TO __temp__t_offline_caching;");
        sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_offline_caching(_download_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, title varchar(200), image BLOB, nickname varchar(200), play_times integer, likes integer, duration integer, create_at integer, playurl32 varchar(200), coversmall varchar(200), file_dir varchar(200), filename varchar(200), filesize integer, downloaded integer, status integer, track_id integer, playurl64 varchar(200));");
        sqlitedatabase.execSQL("INSERT INTO t_offline_caching(_download_id, title, image, nickname, play_times, likes, duration, create_at, playurl32, coversmall, file_dir, filename, filesize, downloaded, status, track_id, playurl64) SELECT _download_id, title, image, nickname, play_times, likes, duration, create_at, playurl, coversmall, file_dir, filename, filesize, downloaded, status, track_id, playurl  FROM __temp__t_offline_caching;");
        sqlitedatabase.execSQL("drop table __temp__t_offline_caching;");
        sqlitedatabase.execSQL("ALTER TABLE t_page_cache_feed RENAME TO __temp__t_page_cache_feed;");
        sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_page_cache_feed(sid integer NOT NULL PRIMARY KEY AUTOINCREMENT, title varchar(200), uid varchar(200), image BLOB, nickname varchar(200), play_times integer, likes integer, duration integer, create_at integer, playurl32 varchar(200), track_id integer, time_line double, image_url text, total_size integer,source integer DEFAULT 1,comments integer DEFAULT 0, playurl64 varchar(200));");
        sqlitedatabase.execSQL("INSERT INTO t_page_cache_feed(sid, title, uid, image, nickname, play_times, likes, duration, create_at, playurl32, track_id, time_line, image_url, total_size, playurl64) SELECT sid, title, uid, image, nickname, play_times, likes, duration, create_at, playurl, track_id, time_line, image_url, total_size, playurl  FROM __temp__t_page_cache_feed;");
        sqlitedatabase.execSQL("drop table __temp__t_page_cache_feed;");
        sqlitedatabase.execSQL("ALTER TABLE t_page_cache_liked RENAME TO __temp__t_page_cache_liked;");
        sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_page_cache_liked(sid integer NOT NULL PRIMARY KEY AUTOINCREMENT, title varchar(200), uid varchar(200), image BLOB, nickname varchar(200), play_times integer, likes integer, duration integer, create_at integer, playurl32 varchar(200), track_id integer, image_url text,source integer DEFAULT 1,comments integer DEFAULT 0,playurl64 varchar(200));");
        sqlitedatabase.execSQL("INSERT INTO t_page_cache_liked(sid, title, uid, image, nickname, play_times, likes, duration, create_at, playurl32, track_id, image_url, playurl64) SELECT sid, title, uid, image, nickname, play_times, likes, duration, create_at, playurl, track_id, image_url, playurl  FROM __temp__t_page_cache_liked;");
        sqlitedatabase.execSQL("drop table __temp__t_page_cache_liked;");
        sqlitedatabase.execSQL("ALTER TABLE t_page_cache_listened RENAME TO __temp__t_page_cache_listened;");
        sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_page_cache_listened(sid integer NOT NULL PRIMARY KEY AUTOINCREMENT, title varchar(200), uid varchar(200), image BLOB, nickname varchar(200), play_times integer, likes integer, duration integer, create_at integer, playurl32 varchar(200), track_id integer, image_url text,source integer DEFAULT 1,comments integer DEFAULT 0,playurl64 varchar(200));");
        sqlitedatabase.execSQL("INSERT INTO t_page_cache_listened(sid, title, uid, image, nickname, play_times, likes, duration, create_at, playurl32, track_id, image_url, playurl64) SELECT sid, title, uid, image, nickname, play_times, likes, duration, create_at, playurl, track_id, image_url, playurl  FROM __temp__t_page_cache_listened;");
        sqlitedatabase.execSQL("drop table __temp__t_page_cache_listened;");
        sqlitedatabase.execSQL("ALTER TABLE t_page_cache_mysounds RENAME TO __temp__t_page_cache_mysounds;");
        sqlitedatabase.execSQL("CREATE TABLE IF NOT EXISTS t_page_cache_mysounds(sid integer NOT NULL PRIMARY KEY AUTOINCREMENT, title varchar(200), uid varchar(200), image BLOB, nickname varchar(200), play_times integer, likes integer, duration integer, create_at integer, playurl32 varchar(200), track_id integer, image_url text,processstate varchar(200), source integer DEFAULT 1,comments integer DEFAULT 0,playurl64 varchar(200));");
        sqlitedatabase.execSQL("INSERT INTO t_page_cache_mysounds(sid, title, uid, image, nickname, play_times, likes, duration, create_at, playurl32, track_id, image_url, processstate, source, playurl64) SELECT sid, title, uid, image, nickname, play_times, likes, duration, create_at, playurl, track_id, image_url, processstate, source, playurl  FROM __temp__t_page_cache_mysounds;");
        sqlitedatabase.execSQL("drop table __temp__t_page_cache_mysounds;");
    }

    public void close()
    {
        super.close();
    }

    public boolean existsRecordIntable(SQLiteDatabase sqlitedatabase, String s, String s1, String s2)
    {
        SQLiteDatabase sqlitedatabase1;
        SQLiteDatabase sqlitedatabase2;
        boolean flag;
        boolean flag1;
        sqlitedatabase2 = null;
        sqlitedatabase1 = null;
        flag = false;
        flag1 = false;
        sqlitedatabase = sqlitedatabase.rawQuery((new StringBuilder()).append("SELECT ").append(s1).append(" FROM ").append(s).append(" WHERE ").append(s1).append(" = '").append(s2).append("'").toString(), null);
        sqlitedatabase1 = sqlitedatabase;
        sqlitedatabase2 = sqlitedatabase;
        int i = sqlitedatabase.getCount();
        if (i <= 0) goto _L2; else goto _L1
_L1:
        flag = true;
        flag1 = true;
        if (sqlitedatabase == null) goto _L4; else goto _L3
_L3:
        flag = flag1;
_L8:
        sqlitedatabase.close();
_L4:
        return flag;
        sqlitedatabase;
        sqlitedatabase2 = sqlitedatabase1;
        Logger.log("... - existsColumnInTable", (new StringBuilder()).append("When checking whether a column exists in the table, an error occurred: ").append(sqlitedatabase.getMessage()).toString());
        if (sqlitedatabase1 == null) goto _L4; else goto _L5
_L5:
        flag = flag1;
        sqlitedatabase = sqlitedatabase1;
        continue; /* Loop/switch isn't completed */
        sqlitedatabase;
        if (sqlitedatabase2 != null)
        {
            sqlitedatabase2.close();
        }
        throw sqlitedatabase;
_L2:
        if (sqlitedatabase == null) goto _L4; else goto _L6
_L6:
        flag = flag1;
        if (true) goto _L8; else goto _L7
_L7:
    }

    public void onCreate(SQLiteDatabase sqlitedatabase)
    {
        TableCreater.createTables(sqlitedatabase);
    }

    public void onUpgrade(SQLiteDatabase sqlitedatabase, int i, int j)
    {
        Logger.log("db_upgrade", (new StringBuilder()).append("oldVersion: ").append(i).append(", newVersion: ").append(j).toString());
        Logger.log("test_order", "===DataBaseHelper: onUpgrade()==003-1");
        if (j > i)
        {
            if (1 == i)
            {
                upgradeFrom1To2(sqlitedatabase);
            }
            upgradeDb(sqlitedatabase);
            if (i <= 11 && !existsColumnInTable(sqlitedatabase, "t_download", "order_position"))
            {
                sqlitedatabase.execSQL("ALTER TABLE t_download ADD COLUMN order_position integer");
            }
        }
        Logger.log("test_order", "===DataBaseHelper: onUpgrade()==003-2");
    }

}
