// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.lib.mpg123;

import java.nio.ByteBuffer;

public class Mp3Decoder
{

    private ByteBuffer mBuff;
    private int mBuffSize;
    private String mPath;
    private long mToken;

    public Mp3Decoder()
    {
    }

    public static native int decode(String s, String s1);

    private int getBufferContent(ByteBuffer bytebuffer, int i, byte abyte0[], int j, int k)
    {
        j = 0;
        bytebuffer.rewind();
        int l = bytebuffer.remaining();
        if (!bytebuffer.hasArray())
        {
            i = j;
            do
            {
                if (i >= l)
                {
                    return l;
                }
                abyte0[i] = bytebuffer.get(i);
                i++;
            } while (true);
        } else
        {
            System.arraycopy(bytebuffer.array(), i, abyte0, 0, k);
            return l;
        }
    }

    public int getBufferSize()
    {
        return mBuffSize;
    }

    public int getChannels()
    {
        if (mToken == 0L)
        {
            throw new IllegalStateException("Init function has not been called!");
        } else
        {
            return getChannels(mToken);
        }
    }

    public native int getChannels(long l);

    public long getSampleRateInHz()
    {
        if (mToken == 0L)
        {
            throw new IllegalStateException("Init function has not been called!");
        } else
        {
            return getSampleRateInHz(mToken);
        }
    }

    public native long getSampleRateInHz(long l);

    public native String getVersion();

    public int initDecoder(String s, int i)
    {
        if (mToken != 0L)
        {
            releaseDecoder(mToken);
        }
        mToken = initDecoder(s);
        if (mToken != 0L)
        {
            if (i <= 0)
            {
                releaseDecoder(mToken);
                throw new IllegalArgumentException("Buffer size must larger than zero!");
            } else
            {
                mBuffSize = i;
                mBuff = ByteBuffer.allocateDirect(i);
                return 0;
            }
        } else
        {
            return -1;
        }
    }

    public native long initDecoder(String s);

    public native int readPCM(long l, ByteBuffer bytebuffer, int i);

    public int readPCM(byte abyte0[], int i)
    {
        if (mToken == 0L)
        {
            throw new IllegalStateException("Init function has not been called!");
        } else
        {
            i = Math.min(Math.min(mBuffSize, i), abyte0.length);
            mBuff.clear();
            i = readPCM(mToken, mBuff, i);
            getBufferContent(mBuff, 0, abyte0, 0, i);
            return i;
        }
    }

    public native int releaseDecoder(long l);

    public void releaseDecoder()
    {
        if (mToken != 0L)
        {
            releaseDecoder(mToken);
            mToken = 0L;
        }
        if (mBuff != null)
        {
            mBuff.clear();
            mBuff = null;
        }
        mBuffSize = 0;
    }

    public long seek(long l)
    {
        if (mToken == 0L)
        {
            throw new IllegalStateException("Init function has not been called!");
        } else
        {
            return seek(mToken, l);
        }
    }

    public native long seek(long l, long l1);

    static 
    {
        System.loadLibrary("mp3decoder");
    }
}
