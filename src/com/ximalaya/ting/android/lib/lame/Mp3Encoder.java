// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.lib.lame;

import java.nio.ByteBuffer;

public class Mp3Encoder
{

    public static final int QUALITY_HIGH = 2;
    public static final int QUALITY_LOW = 7;
    public static final int QUALITY_MIDDLE = 5;
    private static final String TAG = "Mp3Encoder";
    private int mBuffSize;
    private ByteBuffer mInBuffer;
    private ByteBuffer mOutBuffer;
    private long mToken;

    public Mp3Encoder()
    {
    }

    private int getBufferContent(ByteBuffer bytebuffer, int i, byte abyte0[], int j, int k)
    {
        j = 0;
        bytebuffer.rewind();
        int l = bytebuffer.remaining();
        if (!bytebuffer.hasArray())
        {
            i = j;
            do
            {
                if (i >= l)
                {
                    return l;
                }
                abyte0[i] = bytebuffer.get(i);
                i++;
            } while (true);
        } else
        {
            System.arraycopy(bytebuffer.array(), i, abyte0, 0, k);
            return l;
        }
    }

    public native int encode(long l, String s, String s1);

    public native int encode(long l, ByteBuffer bytebuffer, int i, ByteBuffer bytebuffer1, int j);

    public int encode(String s, String s1)
    {
        if (mToken != 0L)
        {
            return encode(mToken, s, s1);
        } else
        {
            return -1;
        }
    }

    public int encode(byte abyte0[], int i, byte abyte1[])
    {
        if (mToken == 0L)
        {
            throw new IllegalStateException("Init function has not been called!");
        }
        if (i > abyte0.length)
        {
            i = abyte0.length;
        }
        if (i > mBuffSize)
        {
            throw new IllegalArgumentException((new StringBuilder("Byte array size too big! input len ")).append(i).append(", max len ").append(mBuffSize).toString());
        }
        if (abyte1.length < i)
        {
            throw new IllegalArgumentException((new StringBuilder("Out buffer array size too small! out len ")).append(abyte1.length).append(", inlen ").append(i).toString());
        }
        mInBuffer.clear();
        mInBuffer.put(abyte0, 0, i);
        i = encode(mToken, mInBuffer, i, mOutBuffer, abyte1.length);
        if (i > 0)
        {
            getBufferContent(mOutBuffer, 0, abyte1, 0, i);
            mOutBuffer.clear();
        }
        return i;
    }

    public native int flush(long l, ByteBuffer bytebuffer, int i);

    public int flush(byte abyte0[])
    {
        if (abyte0.length < mBuffSize)
        {
            throw new IllegalArgumentException((new StringBuilder("Out buffer array size too small! ")).append(abyte0.length).append(", need ").append(mBuffSize).toString());
        }
        if (mToken != 0L)
        {
            int i = flush(mToken, mOutBuffer, mBuffSize);
            getBufferContent(mOutBuffer, 0, abyte0, 0, i);
            mOutBuffer.clear();
            return i;
        } else
        {
            return -1;
        }
    }

    public int initEncoder(int i, int j, int k, int l, int i1)
    {
        mToken = initEncoder(i, j, k, l);
        if (mToken != 0L)
        {
            if (i1 <= 0)
            {
                throw new IllegalArgumentException((new StringBuilder("Buffer size must be a positive integer!")).append(i1).toString());
            } else
            {
                mBuffSize = i1;
                mInBuffer = ByteBuffer.allocateDirect(i1);
                mOutBuffer = ByteBuffer.allocateDirect(i1);
                return 0;
            }
        } else
        {
            return -1;
        }
    }

    public native long initEncoder(int i, int j, int k, int l);

    public native int releaseEncoder(long l);

    public void releaseEncoder()
    {
        if (mToken != 0L)
        {
            releaseEncoder(mToken);
            mToken = 0L;
        }
        if (mInBuffer != null)
        {
            mInBuffer.clear();
            mInBuffer = null;
        }
        if (mOutBuffer != null)
        {
            mOutBuffer.clear();
            mOutBuffer = null;
        }
        mBuffSize = 0;
    }

    static 
    {
        System.loadLibrary("mp3encoder");
    }
}
