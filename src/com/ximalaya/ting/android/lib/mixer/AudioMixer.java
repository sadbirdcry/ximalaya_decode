// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.lib.mixer;

import java.nio.ByteBuffer;

public class AudioMixer
{

    private ByteBuffer mBg;
    private ByteBuffer mMix;
    private int mSize;
    private ByteBuffer mSrc;
    private long mToken;

    public AudioMixer()
    {
    }

    private int getBufferContent(ByteBuffer bytebuffer, int i, byte abyte0[], int j, int k)
    {
        j = 0;
        bytebuffer.rewind();
        int l = bytebuffer.remaining();
        if (!bytebuffer.hasArray())
        {
            i = j;
            do
            {
                if (i >= l)
                {
                    return l;
                }
                abyte0[i] = bytebuffer.get(i);
                i++;
            } while (true);
        } else
        {
            System.arraycopy(bytebuffer.array(), i, abyte0, 0, k);
            return l;
        }
    }

    public native long init(int i);

    public void init(int i, int j)
    {
        if (mToken != 0L)
        {
            release(mToken);
        }
        mSize = i;
        mSrc = ByteBuffer.allocateDirect(i);
        mBg = ByteBuffer.allocateDirect(i);
        mMix = ByteBuffer.allocateDirect(i);
        mToken = init(j);
    }

    public native int normalizeMix(long l, ByteBuffer bytebuffer, int i, float f, ByteBuffer bytebuffer1, int j, 
            float f1, ByteBuffer bytebuffer2, int k);

    public int normalizeMix(byte abyte0[], int i, float f, byte abyte1[], int j, float f1, byte abyte2[])
    {
        if (mSize <= 0 || mToken == 0L)
        {
            throw new IllegalStateException("Called function without init!");
        }
        if (i > mSize || j > mSize || abyte2.length < i)
        {
            throw new IllegalArgumentException("Illegal buffer size!");
        }
        mSrc.clear();
        mSrc.put(abyte0, 0, i);
        mBg.clear();
        mBg.put(abyte1, 0, j);
        mMix.clear();
        i = normalizeMix(mToken, mSrc, i, f, mBg, j, f1, mMix, mSize);
        if (i > 0)
        {
            getBufferContent(mMix, 0, abyte2, 0, i);
        }
        return i;
    }

    public native int release(long l);

    public void release()
    {
        if (mToken != 0L)
        {
            release(mToken);
        }
        mSize = 0;
        mSrc = null;
        mBg = null;
        mMix = null;
    }

    static 
    {
        System.loadLibrary("audiomixer");
    }
}
