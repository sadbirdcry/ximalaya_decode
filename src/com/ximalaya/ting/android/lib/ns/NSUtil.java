// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.lib.ns;

import android.util.Log;
import java.nio.ByteBuffer;

public class NSUtil
{

    private static final int DEF_BUFF_SIZE = 1024;
    private static final String TAG = "NSUtil";
    private int mBufferSize;
    private ByteBuffer mInBuffer;
    private boolean mInit;
    private byte mLock[];
    private ByteBuffer mOutBuffer;
    private long mToken;

    public NSUtil()
    {
        mInit = false;
        mLock = new byte[0];
        setBufferSize(1024);
    }

    private int getBufferContent(ByteBuffer bytebuffer, int i, byte abyte0[], int j, int k)
    {
        j = 0;
        bytebuffer.rewind();
        int l = bytebuffer.remaining();
        if (!bytebuffer.hasArray())
        {
            i = j;
            do
            {
                if (i >= l)
                {
                    return l;
                }
                abyte0[i] = bytebuffer.get(i);
                i++;
            } while (true);
        } else
        {
            System.arraycopy(bytebuffer.array(), i, abyte0, 0, k);
            return l;
        }
    }

    public static boolean isRrmV7Plus()
    {
        String s = System.getProperty("os.arch");
        if (s == null || !s.contains("arm")) goto _L2; else goto _L1
_L1:
        int i = 0;
_L5:
        if (i < s.length()) goto _L3; else goto _L2
_L2:
        return false;
_L3:
        if (s.charAt(i) >= '0' && s.charAt(i) <= '9' && s.charAt(i) >= '7')
        {
            return true;
        }
        i++;
        if (true) goto _L5; else goto _L4
_L4:
    }

    public int getBufferSize()
    {
        return mBufferSize;
    }

    public long getToken()
    {
        return mToken;
    }

    public native long init();

    public native int ns(long l, ByteBuffer bytebuffer, int i, ByteBuffer bytebuffer1, int j);

    public int ns(byte abyte0[], int i, byte abyte1[], int j)
    {
        byte abyte2[] = mLock;
        abyte2;
        JVM INSTR monitorenter ;
        if (mToken == 0L)
        {
            throw new IllegalStateException("Function Not Init! ");
        }
        break MISSING_BLOCK_LABEL_34;
        abyte0;
        abyte2;
        JVM INSTR monitorexit ;
        throw abyte0;
        if (mInit);
        mInBuffer.clear();
        mInBuffer.put(abyte0, 0, i);
        mOutBuffer.clear();
        i = ns(mToken, mInBuffer, i, mOutBuffer, j);
        getBufferContent(mOutBuffer, 0, abyte1, 0, i);
        abyte2;
        JVM INSTR monitorexit ;
        return i;
    }

    public int nsInit()
    {
        byte abyte0[] = mLock;
        abyte0;
        JVM INSTR monitorenter ;
        if (mToken != 0L)
        {
            release(mToken);
        }
        mToken = init();
        Exception exception;
        int i;
        if (mToken != 0L)
        {
            i = 0;
        } else
        {
            i = -1;
        }
        mInit = true;
        abyte0;
        JVM INSTR monitorexit ;
        return i;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public int nsRelease()
    {
        int i = -1;
        synchronized (mLock)
        {
            if (mToken != 0L)
            {
                i = release(mToken);
                mToken = 0L;
            }
            mInit = false;
        }
        return i;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public native int release(long l);

    public void setBufferSize(int i)
    {
        if (i <= 0 || i == mBufferSize)
        {
            Log.e("NSUtil", (new StringBuilder("Invalid Buffer Size! ")).append(i).toString());
            return;
        } else
        {
            mBufferSize = i;
            mInBuffer = ByteBuffer.allocateDirect(mBufferSize);
            mOutBuffer = ByteBuffer.allocateDirect(mBufferSize);
            return;
        }
    }

    static 
    {
        if (isRrmV7Plus())
        {
            Log.d("NSUtil", "isRrmV7Plus true, load armeabi-v7");
            System.loadLibrary("noisereduction_v7");
        } else
        {
            Log.d("NSUtil", "isRrmV7Plus false, load armeabi");
            System.loadLibrary("noisereduction");
        }
    }
}
