// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Messenger;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            SdlController

class this._cls0
    implements ServiceConnection
{

    final SdlController this$0;

    public void onServiceConnected(ComponentName componentname, IBinder ibinder)
    {
        class _cls1
            implements android.os.IBinder.DeathRecipient
        {

            final SdlController._cls1 this$1;

            public void binderDied()
            {
                SdlController.access$102(this$0, false);
                SdlController.access$200(this$0).onDisConnected();
                SdlController.access$300(this$0);
                SdlController.access$402(this$0, null);
            }

            _cls1()
            {
                this$1 = SdlController._cls1.this;
                super();
            }
        }

        try
        {
            ibinder.linkToDeath(new _cls1(), 0);
        }
        // Misplaced declaration of an exception variable
        catch (ComponentName componentname)
        {
            componentname.printStackTrace();
        }
        SdlController.access$402(SdlController.this, new Messenger(ibinder));
        SdlController.access$500(SdlController.this);
    }

    public void onServiceDisconnected(ComponentName componentname)
    {
        SdlController.access$102(SdlController.this, false);
        SdlController.access$402(SdlController.this, null);
        SdlController.access$600(SdlController.this);
        SdlController.access$300(SdlController.this);
        SdlController.access$200(SdlController.this).onDisConnected();
    }

    lControllerListener()
    {
        this$0 = SdlController.this;
        super();
    }
}
