// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.text.TextUtils;
import com.ford.syncV4.exception.SyncException;
import com.ford.syncV4.proxy.SyncProxyALM;
import com.ford.syncV4.proxy.rpc.Image;
import com.ford.syncV4.proxy.rpc.enums.FileType;
import com.ford.syncV4.proxy.rpc.enums.ImageType;
import com.ford.syncV4.proxy.rpc.enums.Language;
import com.ximalaya.ting.android.library.util.BitmapUtils;
import com.ximalaya.ting.android.util.FileUtils;
import com.ximalaya.ting.android.util.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            ProxyListenerALMEx, IdGenerator, IProxyListenerALMEx

public class SyncProxyEx extends SyncProxyALM
{
    private class PutFileThread extends Thread
    {

        final SyncProxyEx this$0;

        public void run()
        {
            super.run();
            Logger.e("SyncProxyEx", "===PutFileThread run");
_L2:
            if (!mRuning)
            {
                break; /* Loop/switch isn't completed */
            }
            UploadTask uploadtask;
            String s;
            int i;
            uploadtask = (UploadTask)mPutFileTasks.take();
            s = uploadtask.name;
            i = uploadtask.id;
            if (!isSyncFileAdded(s))
            {
                break MISSING_BLOCK_LABEL_127;
            }
            Logger.e("SyncProxyEx", (new StringBuilder()).append("===PutFileThread added ").append(i).append(", ").append(s).toString());
            if (i == 64202)
            {
                byte abyte1[];
                try
                {
                    setAppIconInternal(s);
                }
                catch (Exception exception) { }
                continue; /* Loop/switch isn't completed */
            }
            if (i != 64201)
            {
                continue; /* Loop/switch isn't completed */
            }
            showImgInternal(s);
            continue; /* Loop/switch isn't completed */
            abyte1 = uploadtask.getImageRawData();
            if (abyte1 == null)
            {
                break MISSING_BLOCK_LABEL_141;
            }
            if (abyte1.length > 0)
            {
                break MISSING_BLOCK_LABEL_151;
            }
            Logger.e("SyncProxyEx", "==UploadThread fileData is null");
            continue; /* Loop/switch isn't completed */
            mLastFileName = s;
            putfile(s, uploadtask.fileType, Boolean.valueOf(false), abyte1, Integer.valueOf(i));
            Logger.e("SyncProxyEx", (new StringBuilder()).append("===PutFileThread sending ").append(i).append(", ").append(s).append(", ").append(abyte1.length).toString());
            synchronized (mPutFileLock)
            {
                mPutFileLock.wait();
            }
            if (true) goto _L2; else goto _L1
            exception1;
            abyte0;
            JVM INSTR monitorexit ;
            throw exception1;
_L1:
            Logger.e("SyncProxyEx", "===PutFileThread stop");
            return;
        }

        private PutFileThread()
        {
            this$0 = SyncProxyEx.this;
            super();
        }

    }

    private class UploadTask
    {

        private static final int TYPE_BITMAP = 1;
        private static final int TYPE_DRAWABLE = 2;
        private static final int TYPE_FILE = 3;
        public Bitmap bitmap;
        public Drawable drawable;
        public FileType fileType;
        public int id;
        public String name;
        public String path;
        final SyncProxyEx this$0;
        private int type;

        byte[] getImageRawData()
        {
            byte abyte0[] = null;
            if (type == 2)
            {
                abyte0 = BitmapUtils.getBitmapByte(BitmapUtils.drawable2bitmap(drawable), getCompressFormat(fileType), mCompressQuality);
            } else
            {
                if (type == 1)
                {
                    return BitmapUtils.getBitmapByte(bitmap, getCompressFormat(fileType), mCompressQuality);
                }
                if (type == 3)
                {
                    return FileUtils.readByteFromFile(path);
                }
            }
            return abyte0;
        }

        public UploadTask(String s, Bitmap bitmap1, FileType filetype, int i)
        {
            this$0 = SyncProxyEx.this;
            super();
            type = 1;
            name = s;
            bitmap = bitmap1;
            fileType = filetype;
            id = i;
        }

        public UploadTask(String s, Drawable drawable1, FileType filetype, int i)
        {
            this$0 = SyncProxyEx.this;
            super();
            type = 2;
            name = s;
            drawable = drawable1;
            fileType = filetype;
            id = i;
        }

        public UploadTask(String s, String s1, FileType filetype, int i)
        {
            this$0 = SyncProxyEx.this;
            super();
            type = 3;
            name = s;
            path = s1;
            fileType = filetype;
            id = i;
        }
    }


    static final int COID_APPICON = 64202;
    static final int COID_SHOWIMG = 64201;
    static final int COID_VOICESELECT = 64200;
    private static final String TAG = "SyncProxyEx";
    private List mAddedFiles;
    private int mCompressQuality;
    private String mLastFileName;
    private byte mPutFileLock[];
    private ArrayBlockingQueue mPutFileTasks;
    private boolean mRuning;
    private List mSupportImgType;
    private PutFileThread mThread;

    public SyncProxyEx(IProxyListenerALMEx iproxylisteneralmex, String s, Boolean boolean1, Language language, Language language1, String s1)
        throws SyncException
    {
        super(ProxyListenerALMEx.getInstance(), s, boolean1, language, language1, s1);
        mAddedFiles = new ArrayList();
        mSupportImgType = Arrays.asList(new String[] {
            "jpg", "jpeg", "png", "bmp"
        });
        mCompressQuality = 100;
        mPutFileLock = new byte[0];
        mPutFileTasks = new ArrayBlockingQueue(24);
        mRuning = true;
        ProxyListenerALMEx.getInstance().setProxyAndForwordListener(this, iproxylisteneralmex);
        mThread = new PutFileThread();
        mThread.start();
    }

    private void clearAddedFile()
    {
        synchronized (mAddedFiles)
        {
            mAddedFiles.clear();
        }
        return;
        exception;
        list;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public static byte[] getByte(File file)
    {
        Object obj;
        Exception exception;
        Object obj3;
        Object obj4;
        exception = null;
        obj3 = null;
        obj = null;
        obj4 = null;
        if (file == null)
        {
            break MISSING_BLOCK_LABEL_238;
        }
        obj = new FileInputStream(file);
        Object obj1 = obj;
        long l = file.length();
        int i = (int)l;
        if (i <= 0x7fffffff) goto _L2; else goto _L1
_L1:
        obj1 = obj4;
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        ((InputStream) (obj)).close();
        obj1 = obj4;
_L8:
        return ((byte []) (obj1));
_L2:
        obj1 = obj;
        file = new byte[i];
        i = 0;
_L4:
        obj1 = obj;
        if (i >= file.length)
        {
            break; /* Loop/switch isn't completed */
        }
        obj1 = obj;
        int j = ((InputStream) (obj)).read(file, i, file.length - i);
        if (j < 0)
        {
            break; /* Loop/switch isn't completed */
        }
        i += j;
        if (true) goto _L4; else goto _L3
_L3:
        obj1 = obj;
        j = file.length;
        if (i >= j) goto _L6; else goto _L5
_L5:
        obj1 = obj4;
        if (obj == null) goto _L8; else goto _L7
_L7:
        ((InputStream) (obj)).close();
        return null;
        file;
        obj = exception;
_L10:
        file.printStackTrace();
        return ((byte []) (obj));
        exception;
        obj = null;
        file = obj3;
_L13:
        obj1 = obj;
        exception.printStackTrace();
        obj1 = file;
        if (obj == null) goto _L8; else goto _L9
_L9:
        ((InputStream) (obj)).close();
        return file;
        obj1;
        obj = file;
        file = ((File) (obj1));
          goto _L10
        file;
        obj1 = null;
_L12:
        if (obj1 != null)
        {
            try
            {
                ((InputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        throw file;
        file;
        obj = exception;
          goto _L10
_L6:
        obj1 = file;
        if (obj == null) goto _L8; else goto _L11
_L11:
        ((InputStream) (obj)).close();
        return file;
        IOException ioexception;
        ioexception;
        obj = file;
        file = ioexception;
          goto _L10
        file;
          goto _L12
        exception;
        file = obj3;
          goto _L13
        exception;
          goto _L13
        Object obj2 = null;
        file = ((File) (obj));
        obj = obj2;
          goto _L6
    }

    private android.graphics.Bitmap.CompressFormat getCompressFormat(FileType filetype)
    {
        static class _cls1
        {

            static final int $SwitchMap$com$ford$syncV4$proxy$rpc$enums$FileType[];

            static 
            {
                $SwitchMap$com$ford$syncV4$proxy$rpc$enums$FileType = new int[FileType.values().length];
                try
                {
                    $SwitchMap$com$ford$syncV4$proxy$rpc$enums$FileType[FileType.GRAPHIC_JPEG.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ford$syncV4$proxy$rpc$enums$FileType[FileType.GRAPHIC_PNG.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        switch (_cls1..SwitchMap.com.ford.syncV4.proxy.rpc.enums.FileType[filetype.ordinal()])
        {
        default:
            return android.graphics.Bitmap.CompressFormat.JPEG;

        case 1: // '\001'
            return android.graphics.Bitmap.CompressFormat.JPEG;

        case 2: // '\002'
            return android.graphics.Bitmap.CompressFormat.PNG;
        }
    }

    private FileType getFileType(String s)
    {
        if (s.equalsIgnoreCase("jpeg") || s.equalsIgnoreCase("jpg"))
        {
            return FileType.GRAPHIC_JPEG;
        }
        if (s.equalsIgnoreCase("bmp"))
        {
            return FileType.GRAPHIC_BMP;
        } else
        {
            return FileType.GRAPHIC_PNG;
        }
    }

    private String getSubfix(FileType filetype)
    {
        switch (_cls1..SwitchMap.com.ford.syncV4.proxy.rpc.enums.FileType[filetype.ordinal()])
        {
        default:
            return ".bmp";

        case 1: // '\001'
            return ".jpg";

        case 2: // '\002'
            return ".png";
        }
    }

    private void saveBitmap(String s, byte abyte0[], FileType filetype)
    {
        try
        {
            File file = new File((new StringBuilder()).append(Environment.getExternalStorageDirectory()).append(File.separator).append("ATest").append(File.separator).toString());
            if (file.isFile())
            {
                file.delete();
            }
            if (!file.exists())
            {
                file.mkdirs();
            }
            s = new FileOutputStream(new File(file, (new StringBuilder()).append(s).append(getSubfix(filetype)).toString()));
            s.write(abyte0);
            s.flush();
            s.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
    }

    private void setAppIconInternal(String s)
        throws SyncException
    {
        setappicon(s, Integer.valueOf(IdGenerator.next()));
    }

    public void addSyncFileName(String s)
    {
label0:
        {
            synchronized (mAddedFiles)
            {
                if (!TextUtils.isEmpty(s))
                {
                    break label0;
                }
            }
            return;
        }
        if (!mAddedFiles.contains(s))
        {
            mAddedFiles.add(s);
        }
        list;
        JVM INSTR monitorexit ;
        return;
        s;
        list;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void clearCache()
    {
        clearAddedFile();
    }

    public boolean isSyncFileAdded(String s)
    {
        List list = mAddedFiles;
        list;
        JVM INSTR monitorenter ;
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        boolean flag = false;
_L4:
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_45;
        }
        Logger.e("SyncProxyEx", (new StringBuilder()).append("isSyncFileAdded ").append(s).toString());
        list;
        JVM INSTR monitorexit ;
        return flag;
_L2:
        flag = mAddedFiles.contains(s);
        if (true) goto _L4; else goto _L3
_L3:
        s;
        list;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void notifyPutFile()
    {
        synchronized (mPutFileLock)
        {
            mPutFileLock.notify();
        }
        return;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    void onPutFileSuccess(int i, boolean flag)
    {
        if (!flag || TextUtils.isEmpty(mLastFileName)) goto _L2; else goto _L1
_L1:
        addSyncFileName(mLastFileName);
        if (i != 64201) goto _L4; else goto _L3
_L3:
        try
        {
            showImgInternal(mLastFileName);
        }
        catch (SyncException syncexception)
        {
            syncexception.printStackTrace();
        }
_L2:
        notifyPutFile();
        return;
_L4:
        if (i != 64202) goto _L2; else goto _L5
_L5:
        setAppIconInternal(mLastFileName);
          goto _L2
    }

    public void removeSyncFileName(String s)
    {
label0:
        {
            synchronized (mAddedFiles)
            {
                if (!TextUtils.isEmpty(s))
                {
                    break label0;
                }
            }
            return;
        }
        mAddedFiles.remove(s);
        list;
        JVM INSTR monitorexit ;
        return;
        s;
        list;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void setAppIcon(String s, Bitmap bitmap)
        throws SyncException
    {
        if (isSyncFileAdded(s))
        {
            setAppIconInternal(s);
            return;
        } else
        {
            syncPutFile(new UploadTask(s, bitmap, FileType.GRAPHIC_PNG, 64202));
            return;
        }
    }

    public void setAppIcon(String s, Drawable drawable)
        throws SyncException
    {
        if (isSyncFileAdded(s))
        {
            setAppIconInternal(s);
            return;
        } else
        {
            syncPutFile(new UploadTask(s, drawable, FileType.GRAPHIC_PNG, 64202));
            return;
        }
    }

    public void setAppIcon(String s, String s1)
        throws SyncException
    {
        if (isSyncFileAdded(s))
        {
            setAppIconInternal(s);
            return;
        } else
        {
            syncPutFile(new UploadTask(s, s1, FileType.GRAPHIC_PNG, 64202));
            return;
        }
    }

    public void showImg(String s)
        throws SyncException
    {
        File file = new File(s);
        String s1 = file.getName();
        String s2 = s1.substring(s1.lastIndexOf(".") + 1);
        if (!file.exists() || file.isDirectory() || !mSupportImgType.contains(s2))
        {
            throw new SyncException(new Throwable((new StringBuilder()).append("Illegal File ").append(s).toString()));
        } else
        {
            showImg(s1, BitmapFactory.decodeFile(s), getFileType(s2));
            return;
        }
    }

    public void showImg(String s, Bitmap bitmap, FileType filetype)
        throws SyncException
    {
        Logger.e("SyncProxyEx", (new StringBuilder()).append("showImg bitmap ").append(s).toString());
        if (isSyncFileAdded(s))
        {
            showImgInternal(s);
            return;
        } else
        {
            syncPutFile(new UploadTask(s, bitmap, filetype, 64201));
            return;
        }
    }

    public void showImg(String s, Drawable drawable, FileType filetype)
        throws SyncException
    {
        Logger.e("SyncProxyEx", (new StringBuilder()).append("showImg drawable ").append(s).toString());
        if (isSyncFileAdded(s))
        {
            showImgInternal(s);
            return;
        } else
        {
            syncPutFile(new UploadTask(s, drawable, filetype, 64201));
            return;
        }
    }

    public void showImg(String s, String s1)
        throws SyncException
    {
        Logger.e("SyncProxyEx", (new StringBuilder()).append("showImg path ").append(s).append(", ").append(s1).toString());
        if (isSyncFileAdded(s))
        {
            showImgInternal(s);
            return;
        } else
        {
            syncPutFile(new UploadTask(s, s1, getFileType(s.substring(s.lastIndexOf(".") + 1)), 64201));
            return;
        }
    }

    void showImgInternal(String s)
        throws SyncException
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        } else
        {
            Image image = new Image();
            image.setImageType(ImageType.DYNAMIC);
            image.setValue(s);
            int i = IdGenerator.next();
            Logger.e("SyncProxyEx", (new StringBuilder()).append("---show img ").append(i).append(", ").append(s).toString());
            show(null, null, null, null, image, null, null, null, Integer.valueOf(i));
            return;
        }
    }

    public void stopPutFileThread()
    {
        mRuning = false;
        mThread.interrupt();
        notifyPutFile();
    }

    public void syncPutFile(UploadTask uploadtask)
    {
        mPutFileTasks.add(uploadtask);
    }

    public String uploadImg(String s, int i)
        throws SyncException
    {
        s = new File(s);
        String s1 = s.getName();
        String s2 = s1.substring(s1.lastIndexOf(".") + 1);
        if (s2.equalsIgnoreCase("jpg"))
        {
            putfile(s1, FileType.GRAPHIC_JPEG, Boolean.valueOf(false), getByte(s), Integer.valueOf(i));
        } else
        {
            if (s2.equalsIgnoreCase("png"))
            {
                putfile(s1, FileType.GRAPHIC_PNG, Boolean.valueOf(false), getByte(s), Integer.valueOf(i));
                return s1;
            }
            if (s2.equalsIgnoreCase("bmp"))
            {
                putfile(s1, FileType.GRAPHIC_BMP, Boolean.valueOf(false), getByte(s), Integer.valueOf(i));
                return s1;
            }
        }
        return s1;
    }





/*
    static String access$402(SyncProxyEx syncproxyex, String s)
    {
        syncproxyex.mLastFileName = s;
        return s;
    }

*/



}
