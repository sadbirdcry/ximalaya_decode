// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import com.smartdevicelink.proxy.rpc.enums.AudioStreamingState;
import com.smartdevicelink.proxy.rpc.enums.HMILevel;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            SdlService

static class ingState
{

    static final int $SwitchMap$com$smartdevicelink$proxy$rpc$enums$AudioStreamingState[];
    static final int $SwitchMap$com$smartdevicelink$proxy$rpc$enums$HMILevel[];

    static 
    {
        $SwitchMap$com$smartdevicelink$proxy$rpc$enums$HMILevel = new int[HMILevel.values().length];
        try
        {
            $SwitchMap$com$smartdevicelink$proxy$rpc$enums$HMILevel[HMILevel.HMI_FULL.ordinal()] = 1;
        }
        catch (NoSuchFieldError nosuchfielderror5) { }
        try
        {
            $SwitchMap$com$smartdevicelink$proxy$rpc$enums$HMILevel[HMILevel.HMI_LIMITED.ordinal()] = 2;
        }
        catch (NoSuchFieldError nosuchfielderror4) { }
        try
        {
            $SwitchMap$com$smartdevicelink$proxy$rpc$enums$HMILevel[HMILevel.HMI_BACKGROUND.ordinal()] = 3;
        }
        catch (NoSuchFieldError nosuchfielderror3) { }
        try
        {
            $SwitchMap$com$smartdevicelink$proxy$rpc$enums$HMILevel[HMILevel.HMI_NONE.ordinal()] = 4;
        }
        catch (NoSuchFieldError nosuchfielderror2) { }
        $SwitchMap$com$smartdevicelink$proxy$rpc$enums$AudioStreamingState = new int[AudioStreamingState.values().length];
        try
        {
            $SwitchMap$com$smartdevicelink$proxy$rpc$enums$AudioStreamingState[AudioStreamingState.AUDIBLE.ordinal()] = 1;
        }
        catch (NoSuchFieldError nosuchfielderror1) { }
        try
        {
            $SwitchMap$com$smartdevicelink$proxy$rpc$enums$AudioStreamingState[AudioStreamingState.NOT_AUDIBLE.ordinal()] = 2;
        }
        catch (NoSuchFieldError nosuchfielderror)
        {
            return;
        }
    }
}
