// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.dl.PluginConstants;
import com.ximalaya.ting.android.dl.PluginManager;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            AppLinkService, SdlService

public class SdlConnectManager
{

    private static final String ACTION_USB_STATE = "android.hardware.usb.action.USB_STATE";
    private static final String APPLINK_NAME = "SYNC";
    private static final String TAG = "SdlConnectManager";
    private static final String USB_CONNECTED = "connected";
    private static SdlConnectManager sInstance;
    private static byte sLock[] = new byte[0];
    private Context mAppCtx;
    private BluetoothAdapter mBtAdapter;
    private IntentFilter mFilter;
    private Handler mHandler;
    private volatile boolean mIsAppLinkRunning;
    private volatile boolean mIsSdlRunning;
    private PluginManager mPluginManager;
    private BroadcastReceiver mReceiver;
    private UsbManager mUsbManager;

    private SdlConnectManager(Context context)
    {
        mIsAppLinkRunning = false;
        mIsSdlRunning = false;
        mAppCtx = context.getApplicationContext();
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        mHandler = new Handler(Looper.getMainLooper());
        if (a.h)
        {
            mUsbManager = (UsbManager)mAppCtx.getSystemService("usb");
        }
        mPluginManager = PluginManager.getInstance(mAppCtx);
    }

    private void checkBtStatus()
    {
        if (mBtAdapter != null && mBtAdapter.isEnabled() && mBtAdapter.getBondedDevices() != null)
        {
            startAppLink();
        }
    }

    private void checkUsbStatus()
    {
        if (mUsbManager != null)
        {
            onUsbConnect();
        }
    }

    private void createReceiver()
    {
        if (mReceiver != null)
        {
            return;
        } else
        {
            mReceiver = new _cls1();
            return;
        }
    }

    public static SdlConnectManager getInstance(Context context)
    {
        if (sInstance == null)
        {
            synchronized (sLock)
            {
                if (sInstance == null)
                {
                    sInstance = new SdlConnectManager(context);
                }
            }
        }
        return sInstance;
        context;
        abyte0;
        JVM INSTR monitorexit ;
        throw context;
    }

    private void initFilter()
    {
        if (mFilter == null)
        {
            mFilter = new IntentFilter();
            mFilter.addAction("android.bluetooth.device.action.ACL_CONNECTED");
            mFilter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
            mFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
            if (a.h)
            {
                mFilter.addAction("android.hardware.usb.action.USB_STATE");
                mFilter.addAction("com.smartdevicelink.USB_ACCESSORY_ATTACHED");
                return;
            }
        }
    }

    private boolean isFordAccessory(UsbAccessory usbaccessory)
    {
        boolean flag = "Ford".equals(usbaccessory.getManufacturer());
        boolean flag1 = "HMI".equals(usbaccessory.getModel());
        boolean flag2 = "1.0".equals(usbaccessory.getVersion());
        return flag && flag1 && flag2;
    }

    private boolean isHavalAccessory(UsbAccessory usbaccessory)
    {
        boolean flag = "HAVAL".equals(usbaccessory.getManufacturer());
        boolean flag1 = "HMI".equals(usbaccessory.getModel());
        boolean flag2 = "1.0".equals(usbaccessory.getVersion());
        return flag && flag1 && flag2;
    }

    private void registeReceiver()
    {
        mAppCtx.registerReceiver(mReceiver, mFilter);
    }

    private void showToast(String s)
    {
    }

    private void startAppLink()
    {
        StringBuilder stringbuilder = (new StringBuilder()).append("startAppLink ");
        boolean flag;
        if (LocalMediaService.getInstance() != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        Logger.e("SdlConnectManager", stringbuilder.append(flag).toString());
        mPluginManager.loadPluginAsync(PluginConstants.PLUGIN_APPLINK, new _cls2());
    }

    private void startApplinkInternal()
    {
        (new _cls3()).start();
    }

    private void startSDL()
    {
        mPluginManager.loadPluginAsync(PluginConstants.PLUGIN_SDL, new _cls4());
    }

    private void startSdlInternal()
    {
        (new _cls5()).start();
    }

    private void stopAppLinkServer()
    {
        if (!mPluginManager.isPluginLoaded(PluginConstants.PLUGIN_APPLINK))
        {
            return;
        }
        AppLinkService applinkservice = AppLinkService.getInstance();
        if (applinkservice != null)
        {
            try
            {
                applinkservice.disposeSyncProxy();
                return;
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        return;
    }

    private void stopSDLServer()
    {
        if (!mPluginManager.isPluginLoaded(PluginConstants.PLUGIN_SDL))
        {
            return;
        }
        SdlService sdlservice = SdlService.getInstance();
        if (sdlservice != null)
        {
            try
            {
                sdlservice.stopSDL();
                return;
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        return;
    }

    private void stopService()
    {
        try
        {
            if (mIsAppLinkRunning && mPluginManager.isPluginLoaded(PluginConstants.PLUGIN_APPLINK))
            {
                mAppCtx.stopService(new Intent(mAppCtx, com/ximalaya/ting/android/applink/AppLinkService));
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        try
        {
            if (mIsSdlRunning && mPluginManager.isPluginLoaded(PluginConstants.PLUGIN_SDL))
            {
                mAppCtx.stopService(new Intent(mAppCtx, com/ximalaya/ting/android/applink/SdlService));
            }
            return;
        }
        catch (Exception exception1)
        {
            exception1.printStackTrace();
        }
    }

    private void unregisteReceiver()
    {
        mAppCtx.unregisterReceiver(mReceiver);
    }

    public boolean isAppLinkRunning()
    {
        return mIsAppLinkRunning;
    }

    public boolean isSdlRunning()
    {
        return mIsSdlRunning;
    }

    public void onBtConnect(BluetoothDevice bluetoothdevice)
    {
        Logger.e("SdlConnectManager", "onBtConnect ");
        if (mBtAdapter == null || !mBtAdapter.isEnabled() || bluetoothdevice == null)
        {
            Logger.e("SdlConnectManager", "bt connect return");
        } else
        {
            bluetoothdevice = bluetoothdevice.getName();
            if (!TextUtils.isEmpty(bluetoothdevice))
            {
                Logger.e("SdlConnectManager", (new StringBuilder()).append("onBtConnect name : ").append(bluetoothdevice).toString());
                if ("SYNC".contains(bluetoothdevice.trim()))
                {
                    startAppLink();
                    return;
                }
            }
        }
    }

    public void onBtDisconnect(BluetoothDevice bluetoothdevice)
    {
        Logger.e("SdlConnectManager", "onBtDisconnect");
        if (mBtAdapter != null)
        {
            if (bluetoothdevice != null)
            {
                if (!TextUtils.isEmpty(bluetoothdevice = bluetoothdevice.getName()))
                {
                    Logger.e("SdlConnectManager", (new StringBuilder()).append("onBtDisconnect name : ").append(bluetoothdevice).toString());
                    if ("SYNC".contains(bluetoothdevice.trim()))
                    {
                        stopAppLinkServer();
                        return;
                    }
                }
            } else
            {
                stopAppLinkServer();
                return;
            }
        }
    }

    public void onUsbConnect()
    {
        Logger.e("SdlConnectManager", "onUsbConnect");
        showToast("onUsbConnect");
        if (a.h && mUsbManager != null) goto _L2; else goto _L1
_L1:
        UsbAccessory ausbaccessory[];
        return;
_L2:
        if ((ausbaccessory = mUsbManager.getAccessoryList()) != null)
        {
            int j = ausbaccessory.length;
            int i = 0;
            while (i < j) 
            {
                UsbAccessory usbaccessory = ausbaccessory[i];
                if (isHavalAccessory(usbaccessory))
                {
                    Logger.e("SdlConnectManager", "isHavalAccessory");
                    startSDL();
                    return;
                }
                if (isFordAccessory(usbaccessory))
                {
                    Logger.e("SdlConnectManager", "isFordAccessory");
                    startSDL();
                    return;
                }
                i++;
            }
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    public void onUsbDisconnect()
    {
        boolean flag1;
        flag1 = true;
        Logger.e("SdlConnectManager", "onUsbDisconnect");
        showToast("onUsbDisconnect");
        if (a.h) goto _L2; else goto _L1
_L1:
        UsbAccessory ausbaccessory[];
        return;
_L2:
        if ((ausbaccessory = mUsbManager.getAccessoryList()) == null) goto _L1; else goto _L3
_L3:
        int i;
        int j;
        j = ausbaccessory.length;
        i = 0;
_L8:
        UsbAccessory usbaccessory;
        if (i >= j)
        {
            break MISSING_BLOCK_LABEL_94;
        }
        usbaccessory = ausbaccessory[i];
        if (!isFordAccessory(usbaccessory)) goto _L5; else goto _L4
_L4:
        boolean flag = flag1;
_L7:
        if (!flag)
        {
            stopSDLServer();
            return;
        }
          goto _L1
_L5:
        flag = flag1;
        if (isHavalAccessory(usbaccessory)) goto _L7; else goto _L6
_L6:
        i++;
          goto _L8
        flag = false;
          goto _L7
    }

    public void release()
    {
        Logger.e("SdlConnectManager", "release");
        stopController();
        stopService();
        mHandler.removeCallbacksAndMessages(null);
        mReceiver = null;
        mFilter = null;
        mBtAdapter = null;
        mUsbManager = null;
        sInstance = null;
    }

    public void setAppLinkRunning(boolean flag)
    {
        mIsAppLinkRunning = flag;
    }

    public void setSdlRunning(boolean flag)
    {
        mIsSdlRunning = flag;
    }

    public void startController()
    {
        checkBtStatus();
        checkUsbStatus();
    }

    public void stopController()
    {
        if (mIsAppLinkRunning)
        {
            stopAppLinkServer();
        }
        if (mIsSdlRunning)
        {
            stopSDLServer();
        }
    }





    private class _cls1 extends BroadcastReceiver
    {

        final SdlConnectManager this$0;

        public void onReceive(Context context, Intent intent)
        {
            context = intent.getAction();
            if ("android.bluetooth.device.action.ACL_CONNECTED".equals(context))
            {
                context = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                onBtConnect(context);
            } else
            {
                if ("android.bluetooth.device.action.ACL_DISCONNECTED".equals(context))
                {
                    context = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                    onBtDisconnect(context);
                    return;
                }
                if ("android.bluetooth.adapter.action.STATE_CHANGED".equals(context))
                {
                    onBtDisconnect(null);
                    return;
                }
                if ("com.smartdevicelink.USB_ACCESSORY_ATTACHED".equals(context))
                {
                    Logger.e("SdlConnectManager", "USBTransport.ACTION_USB_ACCESSORY_ATTACHED");
                    onUsbConnect();
                    return;
                }
                if ("android.hardware.usb.action.USB_STATE".equals(context) && !intent.getBooleanExtra("connected", false))
                {
                    onUsbDisconnect();
                    return;
                }
            }
        }

        _cls1()
        {
            this$0 = SdlConnectManager.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final SdlConnectManager this$0;

        public void run()
        {
            startApplinkInternal();
        }

        _cls2()
        {
            this$0 = SdlConnectManager.this;
            super();
        }
    }


    private class _cls3 extends Thread
    {

        final SdlConnectManager this$0;

        public void run()
        {
            if (LocalMediaService.startLocalMediaService(mAppCtx))
            {
                Intent intent = new Intent(mAppCtx, com/ximalaya/ting/android/applink/AppLinkService);
                mAppCtx.startService(intent);
            }
        }

        _cls3()
        {
            this$0 = SdlConnectManager.this;
            super();
        }
    }


    private class _cls4
        implements Runnable
    {

        final SdlConnectManager this$0;

        public void run()
        {
            startSdlInternal();
        }

        _cls4()
        {
            this$0 = SdlConnectManager.this;
            super();
        }
    }


    private class _cls5 extends Thread
    {

        final SdlConnectManager this$0;

        public void run()
        {
            if (LocalMediaService.startLocalMediaService(mAppCtx))
            {
                Intent intent = new Intent(mAppCtx, com/ximalaya/ting/android/applink/SdlService);
                mAppCtx.startService(intent);
            }
        }

        _cls5()
        {
            this$0 = SdlConnectManager.this;
            super();
        }
    }

}
