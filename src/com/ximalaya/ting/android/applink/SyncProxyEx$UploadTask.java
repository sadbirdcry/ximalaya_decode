// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.ford.syncV4.proxy.rpc.enums.FileType;
import com.ximalaya.ting.android.library.util.BitmapUtils;
import com.ximalaya.ting.android.util.FileUtils;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            SyncProxyEx

private class id
{

    private static final int TYPE_BITMAP = 1;
    private static final int TYPE_DRAWABLE = 2;
    private static final int TYPE_FILE = 3;
    public Bitmap bitmap;
    public Drawable drawable;
    public FileType fileType;
    public int id;
    public String name;
    public String path;
    final SyncProxyEx this$0;
    private int type;

    byte[] getImageRawData()
    {
        byte abyte0[] = null;
        if (type == 2)
        {
            abyte0 = BitmapUtils.getBitmapByte(BitmapUtils.drawable2bitmap(drawable), SyncProxyEx.access$600(SyncProxyEx.this, fileType), SyncProxyEx.access$700(SyncProxyEx.this));
        } else
        {
            if (type == 1)
            {
                return BitmapUtils.getBitmapByte(bitmap, SyncProxyEx.access$600(SyncProxyEx.this, fileType), SyncProxyEx.access$700(SyncProxyEx.this));
            }
            if (type == 3)
            {
                return FileUtils.readByteFromFile(path);
            }
        }
        return abyte0;
    }

    public (String s, Bitmap bitmap1, FileType filetype, int i)
    {
        this$0 = SyncProxyEx.this;
        super();
        type = 1;
        name = s;
        bitmap = bitmap1;
        fileType = filetype;
        id = i;
    }

    public id(String s, Drawable drawable1, FileType filetype, int i)
    {
        this$0 = SyncProxyEx.this;
        super();
        type = 2;
        name = s;
        drawable = drawable1;
        fileType = filetype;
        id = i;
    }

    public id(String s, String s1, FileType filetype, int i)
    {
        this$0 = SyncProxyEx.this;
        super();
        type = 3;
        name = s;
        path = s1;
        fileType = filetype;
        id = i;
    }
}
