// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import java.util.concurrent.atomic.AtomicInteger;

public class IdGenerator
{

    private static AtomicInteger sAtomicInteger = new AtomicInteger(0);
    private static int sMax = 0x7fffffff;
    private static int sMin = 0;

    public IdGenerator()
    {
    }

    public static void clear()
    {
        sAtomicInteger.set(sMin);
    }

    public static int incrementAndGet()
    {
        return sAtomicInteger.incrementAndGet();
    }

    public static void init(int i, int j)
    {
        sMin = i;
        sMax = j;
    }

    public static int next()
    {
        int i = incrementAndGet();
        if (i > sMax)
        {
            clear();
        }
        return i;
    }

}
