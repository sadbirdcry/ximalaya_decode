// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import com.ford.syncV4.proxy.rpc.AddCommandResponse;
import com.ford.syncV4.proxy.rpc.AddSubMenuResponse;
import com.ford.syncV4.proxy.rpc.AlertResponse;
import com.ford.syncV4.proxy.rpc.ChangeRegistrationResponse;
import com.ford.syncV4.proxy.rpc.CreateInteractionChoiceSetResponse;
import com.ford.syncV4.proxy.rpc.DeleteCommandResponse;
import com.ford.syncV4.proxy.rpc.DeleteFileResponse;
import com.ford.syncV4.proxy.rpc.DeleteInteractionChoiceSetResponse;
import com.ford.syncV4.proxy.rpc.DeleteSubMenuResponse;
import com.ford.syncV4.proxy.rpc.DiagnosticMessageResponse;
import com.ford.syncV4.proxy.rpc.EndAudioPassThruResponse;
import com.ford.syncV4.proxy.rpc.GenericResponse;
import com.ford.syncV4.proxy.rpc.GetDTCsResponse;
import com.ford.syncV4.proxy.rpc.GetVehicleDataResponse;
import com.ford.syncV4.proxy.rpc.ListFilesResponse;
import com.ford.syncV4.proxy.rpc.OnAudioPassThru;
import com.ford.syncV4.proxy.rpc.OnButtonEvent;
import com.ford.syncV4.proxy.rpc.OnButtonPress;
import com.ford.syncV4.proxy.rpc.OnCommand;
import com.ford.syncV4.proxy.rpc.OnDriverDistraction;
import com.ford.syncV4.proxy.rpc.OnHMIStatus;
import com.ford.syncV4.proxy.rpc.OnHashChange;
import com.ford.syncV4.proxy.rpc.OnKeyboardInput;
import com.ford.syncV4.proxy.rpc.OnLanguageChange;
import com.ford.syncV4.proxy.rpc.OnLockScreenStatus;
import com.ford.syncV4.proxy.rpc.OnPermissionsChange;
import com.ford.syncV4.proxy.rpc.OnSystemRequest;
import com.ford.syncV4.proxy.rpc.OnTBTClientState;
import com.ford.syncV4.proxy.rpc.OnTouchEvent;
import com.ford.syncV4.proxy.rpc.OnVehicleData;
import com.ford.syncV4.proxy.rpc.PerformAudioPassThruResponse;
import com.ford.syncV4.proxy.rpc.PerformInteractionResponse;
import com.ford.syncV4.proxy.rpc.PutFileResponse;
import com.ford.syncV4.proxy.rpc.ReadDIDResponse;
import com.ford.syncV4.proxy.rpc.ResetGlobalPropertiesResponse;
import com.ford.syncV4.proxy.rpc.ScrollableMessageResponse;
import com.ford.syncV4.proxy.rpc.SetAppIconResponse;
import com.ford.syncV4.proxy.rpc.SetDisplayLayoutResponse;
import com.ford.syncV4.proxy.rpc.SetGlobalPropertiesResponse;
import com.ford.syncV4.proxy.rpc.SetMediaClockTimerResponse;
import com.ford.syncV4.proxy.rpc.ShowResponse;
import com.ford.syncV4.proxy.rpc.SliderResponse;
import com.ford.syncV4.proxy.rpc.SpeakResponse;
import com.ford.syncV4.proxy.rpc.SubscribeButtonResponse;
import com.ford.syncV4.proxy.rpc.SubscribeVehicleDataResponse;
import com.ford.syncV4.proxy.rpc.SystemRequestResponse;
import com.ford.syncV4.proxy.rpc.UnsubscribeButtonResponse;
import com.ford.syncV4.proxy.rpc.UnsubscribeVehicleDataResponse;
import com.ford.syncV4.proxy.rpc.enums.SyncDisconnectedReason;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            IProxyListenerALMEx, AppLinkService, SelectByVoiceResponse

private class <init>
    implements IProxyListenerALMEx
{

    final AppLinkService this$0;

    public void onAddCommandResponse(AddCommandResponse addcommandresponse)
    {
        AppLinkService.access$1900().onAddCommandResponse(addcommandresponse);
    }

    public void onAddSubMenuResponse(AddSubMenuResponse addsubmenuresponse)
    {
        AppLinkService.access$1900().onAddSubMenuResponse(addsubmenuresponse);
    }

    public void onAlertResponse(AlertResponse alertresponse)
    {
        AppLinkService.access$1900().onAlertResponse(alertresponse);
    }

    public void onChangeRegistrationResponse(ChangeRegistrationResponse changeregistrationresponse)
    {
        AppLinkService.access$1900().onChangeRegistrationResponse(changeregistrationresponse);
    }

    public void onCreateInteractionChoiceSetResponse(CreateInteractionChoiceSetResponse createinteractionchoicesetresponse)
    {
        AppLinkService.access$1900().onCreateInteractionChoiceSetResponse(createinteractionchoicesetresponse);
    }

    public void onDeleteCommandResponse(DeleteCommandResponse deletecommandresponse)
    {
        AppLinkService.access$1900().onDeleteCommandResponse(deletecommandresponse);
    }

    public void onDeleteFileResponse(DeleteFileResponse deletefileresponse)
    {
        AppLinkService.access$1900().onDeleteFileResponse(deletefileresponse);
    }

    public void onDeleteInteractionChoiceSetResponse(DeleteInteractionChoiceSetResponse deleteinteractionchoicesetresponse)
    {
        AppLinkService.access$1900().onDeleteInteractionChoiceSetResponse(deleteinteractionchoicesetresponse);
    }

    public void onDeleteSubMenuResponse(DeleteSubMenuResponse deletesubmenuresponse)
    {
        AppLinkService.access$1900().onDeleteSubMenuResponse(deletesubmenuresponse);
    }

    public void onDiagnosticMessageResponse(DiagnosticMessageResponse diagnosticmessageresponse)
    {
        AppLinkService.access$1900().onDiagnosticMessageResponse(diagnosticmessageresponse);
    }

    public void onEndAudioPassThruResponse(EndAudioPassThruResponse endaudiopassthruresponse)
    {
        AppLinkService.access$1900().onEndAudioPassThruResponse(endaudiopassthruresponse);
    }

    public void onError(String s, Exception exception)
    {
        AppLinkService.access$1900().onError(s, exception);
    }

    public void onGenericResponse(GenericResponse genericresponse)
    {
        AppLinkService.access$1900().onGenericResponse(genericresponse);
    }

    public void onGetDTCsResponse(GetDTCsResponse getdtcsresponse)
    {
        AppLinkService.access$1900().onGetDTCsResponse(getdtcsresponse);
    }

    public void onGetVehicleDataResponse(GetVehicleDataResponse getvehicledataresponse)
    {
        AppLinkService.access$1900().onGetVehicleDataResponse(getvehicledataresponse);
    }

    public void onListFilesResponse(ListFilesResponse listfilesresponse)
    {
        AppLinkService.access$1900().onListFilesResponse(listfilesresponse);
    }

    public void onOnAudioPassThru(OnAudioPassThru onaudiopassthru)
    {
        AppLinkService.access$1900().onOnAudioPassThru(onaudiopassthru);
    }

    public void onOnButtonEvent(OnButtonEvent onbuttonevent)
    {
        AppLinkService.access$1900().onOnButtonEvent(onbuttonevent);
    }

    public void onOnButtonPress(OnButtonPress onbuttonpress)
    {
        AppLinkService.access$1900().onOnButtonPress(onbuttonpress);
    }

    public void onOnCommand(OnCommand oncommand)
    {
        AppLinkService.access$1900().onOnCommand(oncommand);
    }

    public void onOnDriverDistraction(OnDriverDistraction ondriverdistraction)
    {
        AppLinkService.access$1900().onOnDriverDistraction(ondriverdistraction);
    }

    public void onOnHMIStatus(OnHMIStatus onhmistatus)
    {
        AppLinkService.access$1900().onOnHMIStatus(onhmistatus);
    }

    public void onOnHashChange(OnHashChange onhashchange)
    {
        AppLinkService.access$1900().onOnHashChange(onhashchange);
    }

    public void onOnKeyboardInput(OnKeyboardInput onkeyboardinput)
    {
        AppLinkService.access$1900().onOnKeyboardInput(onkeyboardinput);
    }

    public void onOnLanguageChange(OnLanguageChange onlanguagechange)
    {
        AppLinkService.access$1900().onOnLanguageChange(onlanguagechange);
    }

    public void onOnLockScreenNotification(OnLockScreenStatus onlockscreenstatus)
    {
        AppLinkService.access$1900().onOnLockScreenNotification(onlockscreenstatus);
    }

    public void onOnPermissionsChange(OnPermissionsChange onpermissionschange)
    {
        AppLinkService.access$1900().onOnPermissionsChange(onpermissionschange);
    }

    public void onOnSystemRequest(OnSystemRequest onsystemrequest)
    {
        AppLinkService.access$1900().onOnSystemRequest(onsystemrequest);
    }

    public void onOnTBTClientState(OnTBTClientState ontbtclientstate)
    {
        AppLinkService.access$1900().onOnTBTClientState(ontbtclientstate);
    }

    public void onOnTouchEvent(OnTouchEvent ontouchevent)
    {
        AppLinkService.access$1900().onOnTouchEvent(ontouchevent);
    }

    public void onOnVehicleData(OnVehicleData onvehicledata)
    {
        AppLinkService.access$1900().onOnVehicleData(onvehicledata);
    }

    public void onPerformAudioPassThruResponse(PerformAudioPassThruResponse performaudiopassthruresponse)
    {
        AppLinkService.access$1900().onPerformAudioPassThruResponse(performaudiopassthruresponse);
    }

    public void onPerformInteractionResponse(PerformInteractionResponse performinteractionresponse)
    {
        AppLinkService.access$1900().onPerformInteractionResponse(performinteractionresponse);
    }

    public void onProxyClosed(String s, Exception exception, SyncDisconnectedReason syncdisconnectedreason)
    {
        AppLinkService.access$1900().onProxyClosed(s, exception, syncdisconnectedreason);
    }

    public void onPutFileResponse(PutFileResponse putfileresponse)
    {
        AppLinkService.access$1900().onPutFileResponse(putfileresponse);
    }

    public void onReadDIDResponse(ReadDIDResponse readdidresponse)
    {
        AppLinkService.access$1900().onReadDIDResponse(readdidresponse);
    }

    public void onResetGlobalPropertiesResponse(ResetGlobalPropertiesResponse resetglobalpropertiesresponse)
    {
        AppLinkService.access$1900().onResetGlobalPropertiesResponse(resetglobalpropertiesresponse);
    }

    public void onScrollableMessageResponse(ScrollableMessageResponse scrollablemessageresponse)
    {
        AppLinkService.access$1900().onScrollableMessageResponse(scrollablemessageresponse);
    }

    public void onSelectByVoice(SelectByVoiceResponse selectbyvoiceresponse)
    {
        AppLinkService.access$1900().onSelectByVoice(selectbyvoiceresponse);
    }

    public void onSetAppIconResponse(SetAppIconResponse setappiconresponse)
    {
        AppLinkService.access$1900().onSetAppIconResponse(setappiconresponse);
    }

    public void onSetDisplayLayoutResponse(SetDisplayLayoutResponse setdisplaylayoutresponse)
    {
        AppLinkService.access$1900().onSetDisplayLayoutResponse(setdisplaylayoutresponse);
    }

    public void onSetGlobalPropertiesResponse(SetGlobalPropertiesResponse setglobalpropertiesresponse)
    {
        AppLinkService.access$1900().onSetGlobalPropertiesResponse(setglobalpropertiesresponse);
    }

    public void onSetMediaClockTimerResponse(SetMediaClockTimerResponse setmediaclocktimerresponse)
    {
        AppLinkService.access$1900().onSetMediaClockTimerResponse(setmediaclocktimerresponse);
    }

    public void onShowResponse(ShowResponse showresponse)
    {
        AppLinkService.access$1900().onShowResponse(showresponse);
    }

    public void onSliderResponse(SliderResponse sliderresponse)
    {
        AppLinkService.access$1900().onSliderResponse(sliderresponse);
    }

    public void onSpeakResponse(SpeakResponse speakresponse)
    {
        AppLinkService.access$1900().onSpeakResponse(speakresponse);
    }

    public void onSubscribeButtonResponse(SubscribeButtonResponse subscribebuttonresponse)
    {
        AppLinkService.access$1900().onSubscribeButtonResponse(subscribebuttonresponse);
    }

    public void onSubscribeVehicleDataResponse(SubscribeVehicleDataResponse subscribevehicledataresponse)
    {
        AppLinkService.access$1900().onSubscribeVehicleDataResponse(subscribevehicledataresponse);
    }

    public void onSystemRequestResponse(SystemRequestResponse systemrequestresponse)
    {
        AppLinkService.access$1900().onSystemRequestResponse(systemrequestresponse);
    }

    public void onUnsubscribeButtonResponse(UnsubscribeButtonResponse unsubscribebuttonresponse)
    {
        AppLinkService.access$1900().onUnsubscribeButtonResponse(unsubscribebuttonresponse);
    }

    public void onUnsubscribeVehicleDataResponse(UnsubscribeVehicleDataResponse unsubscribevehicledataresponse)
    {
        AppLinkService.access$1900().onUnsubscribeVehicleDataResponse(unsubscribevehicledataresponse);
    }

    private ()
    {
        this$0 = AppLinkService.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
