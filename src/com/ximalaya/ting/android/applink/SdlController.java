// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.SparseArray;
import com.mxnavi.sdl.SdlAppInfo;
import com.smartdevicelink.proxy.RPCMessage;
import com.smartdevicelink.proxy.RPCNotification;
import com.smartdevicelink.proxy.RPCRequest;
import com.smartdevicelink.proxy.RPCRequestFactory;
import com.smartdevicelink.proxy.RPCResponse;
import com.smartdevicelink.proxy.TTSChunkFactory;
import com.smartdevicelink.proxy.rpc.AddCommand;
import com.smartdevicelink.proxy.rpc.AddCommandResponse;
import com.smartdevicelink.proxy.rpc.AddSubMenu;
import com.smartdevicelink.proxy.rpc.AddSubMenuResponse;
import com.smartdevicelink.proxy.rpc.Alert;
import com.smartdevicelink.proxy.rpc.CreateInteractionChoiceSet;
import com.smartdevicelink.proxy.rpc.CreateInteractionChoiceSetResponse;
import com.smartdevicelink.proxy.rpc.DeleteFile;
import com.smartdevicelink.proxy.rpc.DeleteFileResponse;
import com.smartdevicelink.proxy.rpc.DeleteInteractionChoiceSet;
import com.smartdevicelink.proxy.rpc.DeleteInteractionChoiceSetResponse;
import com.smartdevicelink.proxy.rpc.Image;
import com.smartdevicelink.proxy.rpc.OnButtonEvent;
import com.smartdevicelink.proxy.rpc.OnButtonPress;
import com.smartdevicelink.proxy.rpc.OnCommand;
import com.smartdevicelink.proxy.rpc.OnHMIStatus;
import com.smartdevicelink.proxy.rpc.OnTouchEvent;
import com.smartdevicelink.proxy.rpc.PerformInteraction;
import com.smartdevicelink.proxy.rpc.PerformInteractionResponse;
import com.smartdevicelink.proxy.rpc.PutFile;
import com.smartdevicelink.proxy.rpc.PutFileResponse;
import com.smartdevicelink.proxy.rpc.SetMediaClockTimer;
import com.smartdevicelink.proxy.rpc.StartTime;
import com.smartdevicelink.proxy.rpc.TouchCoord;
import com.smartdevicelink.proxy.rpc.TouchEvent;
import com.smartdevicelink.proxy.rpc.enums.AppHMIType;
import com.smartdevicelink.proxy.rpc.enums.ButtonName;
import com.smartdevicelink.proxy.rpc.enums.FileType;
import com.smartdevicelink.proxy.rpc.enums.InteractionMode;
import com.smartdevicelink.proxy.rpc.enums.LayoutMode;
import com.smartdevicelink.proxy.rpc.enums.TextAlignment;
import com.smartdevicelink.proxy.rpc.enums.TouchType;
import com.smartdevicelink.proxy.rpc.enums.UpdateMode;
import com.ximalaya.ting.android.util.Logger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            PendingRpcRequestList, SdlImageResource

public class SdlController
{
    private final class IncommingHandler extends Handler
    {

        final SdlController this$0;

        public void handleMessage(Message message)
        {
            switch (message.what)
            {
            case 2: // '\002'
            default:
                return;

            case 0: // '\0'
                Logger.e("XmSdlController", "IncommingHandler::handleMessage<<< SDL_CONNECTED");
                isConnected = true;
                mListener.onConnected();
                return;

            case 1: // '\001'
                Logger.e("XmSdlController", "IncommingHandler::handleMessage<<< SDL_DISCONNECTED");
                if (isConnected)
                {
                    unbindService();
                }
                mListener.onDisConnected();
                isConnected = false;
                initialize();
                return;

            case 3: // '\003'
                break;
            }
            try
            {
                message.getData().setClassLoader(com/ximalaya/ting/android/applink/SdlController.getClassLoader());
                onMessageRessultReceived((RPCMessage)message.getData().getParcelable("data"));
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Message message)
            {
                message.printStackTrace();
            }
        }

        private IncommingHandler()
        {
            this$0 = SdlController.this;
            super();
        }

        IncommingHandler(_cls1 _pcls1)
        {
            this();
        }
    }

    public static interface SdlControllerListener
    {

        public abstract void onButtonEvent(OnButtonEvent onbuttonevent);

        public abstract void onButtonPress(OnButtonPress onbuttonpress);

        public abstract void onCommand(OnCommand oncommand);

        public abstract void onConnected();

        public abstract void onCreateInteractionChoiceSetResponse(CreateInteractionChoiceSet createinteractionchoiceset, CreateInteractionChoiceSetResponse createinteractionchoicesetresponse);

        public abstract void onDeleteFile(DeleteFile deletefile, DeleteFileResponse deletefileresponse);

        public abstract void onDeleteInteractionChoiceSetResponse(DeleteInteractionChoiceSet deleteinteractionchoiceset, DeleteInteractionChoiceSetResponse deleteinteractionchoicesetresponse);

        public abstract void onDisConnected();

        public abstract void onHMIStatus(OnHMIStatus onhmistatus);

        public abstract void onInteraction(PerformInteraction performinteraction, PerformInteractionResponse performinteractionresponse);

        public abstract void onPutFile(PutFile putfile, PutFileResponse putfileresponse);

        public abstract void onTouchEvent(TouchType touchtype, Point point);
    }


    public static final int NAVI_MAP_MODE_GUIDE = 1;
    public static final int NAVI_MAP_MODE_NO_GUIDE = 0;
    private static final String SDL_SERVICE_ACTION = "com.mxnavi.sdl.sdlservice";
    private static final String SDL_SERVICE_PACKAGE = "com.mxnavi.sdl";
    private static final String TAG = "XmSdlController";
    private PerformInteraction currentInteraction;
    private boolean isConnected;
    private boolean isShowing;
    private List mAddedImageList;
    private SdlAppInfo mAppInfo;
    private ServiceConnection mConnection;
    private Context mContext;
    private SdlControllerListener mListener;
    private Messenger mMessenger;
    private PendingRpcRequestList mPendingRequestList;
    private SparseArray mRequestList;
    private Messenger serviceMessenger;

    public SdlController(Context context, SdlControllerListener sdlcontrollerlistener)
    {
        serviceMessenger = null;
        mMessenger = new Messenger(new IncommingHandler(null));
        isConnected = false;
        mRequestList = null;
        mAddedImageList = null;
        mPendingRequestList = new PendingRpcRequestList();
        isShowing = false;
        mConnection = new _cls1();
        mContext = context;
        mListener = sdlcontrollerlistener;
    }

    private void bindService()
    {
        Intent intent = new Intent("com.mxnavi.sdl.sdlservice");
        if (android.os.Build.VERSION.SDK_INT >= 21)
        {
            intent.setPackage("com.mxnavi.sdl");
        }
        mContext.bindService(intent, mConnection, 1);
    }

    private void initialize()
    {
        if (mRequestList != null)
        {
            mRequestList.clear();
        } else
        {
            mRequestList = new SparseArray();
        }
        if (mAddedImageList != null)
        {
            mAddedImageList.clear();
            return;
        } else
        {
            mAddedImageList = new ArrayList();
            return;
        }
    }

    private void onMessageRessultReceived(RPCMessage rpcmessage)
    {
        if ("request".equals(rpcmessage.getMessageType()))
        {
            onRPCRequestReceived((RPCRequest)rpcmessage);
            return;
        }
        if ("response".equals(rpcmessage.getMessageType()))
        {
            onRPCResponseReceived((RPCResponse)rpcmessage);
            return;
        }
        if ("notification".equals(rpcmessage.getMessageType()))
        {
            onRPCNotificationReceived((RPCNotification)rpcmessage);
            return;
        } else
        {
            Logger.e("XmSdlController", (new StringBuilder()).append("unkown message received from sdl service").append(rpcmessage.getMessageType()).toString());
            return;
        }
    }

    private void onRPCNotificationReceived(RPCNotification rpcnotification)
    {
        Logger.e("XmSdlController", (new StringBuilder()).append("onRPCNotificationReceived<<<").append(rpcnotification.getFunctionName()).toString());
        if (rpcnotification.getFunctionName().equals("OnHMIStatus"))
        {
            mListener.onHMIStatus((OnHMIStatus)rpcnotification);
        } else
        {
            if (rpcnotification.getFunctionName().equals("OnButtonEvent"))
            {
                rpcnotification = (OnButtonEvent)rpcnotification;
                mListener.onButtonEvent(rpcnotification);
                return;
            }
            if (rpcnotification.getFunctionName().equals("OnButtonPress"))
            {
                rpcnotification = (OnButtonPress)rpcnotification;
                mListener.onButtonPress(rpcnotification);
                return;
            }
            if (rpcnotification.getFunctionName().equals("OnCommand"))
            {
                mListener.onCommand((OnCommand)rpcnotification);
                return;
            }
            if (rpcnotification.getFunctionName().equals("OnTouchEvent"))
            {
                rpcnotification = (OnTouchEvent)rpcnotification;
                mListener.onTouchEvent(rpcnotification.getType(), new Point(((TouchCoord)((TouchEvent)rpcnotification.getEvent().get(0)).getC().get(0)).getX().intValue(), ((TouchCoord)((TouchEvent)rpcnotification.getEvent().get(0)).getC().get(0)).getX().intValue()));
                return;
            }
        }
    }

    private void onRPCRequestReceived(RPCRequest rpcrequest)
    {
        Logger.e("XmSdlController", (new StringBuilder()).append("onRPCRequestReceived<<<<").append(rpcrequest.getFunctionName()).append("(").append(rpcrequest.getCorrelationID()).append(")").toString());
        mRequestList.put(rpcrequest.getCorrelationID().intValue(), rpcrequest);
    }

    private void onRPCResponseReceived(RPCResponse rpcresponse)
    {
        Object obj;
        Object obj1;
        obj1 = rpcresponse.getFunctionName();
        Logger.e("XmSdlController", (new StringBuilder()).append("---Response : ").append(((String) (obj1))).append(", ").append(rpcresponse.getSuccess()).append(", ").append(rpcresponse.getInfo()).append(",").append(rpcresponse.describeContents()).toString());
        obj = (RPCRequest)mRequestList.get(rpcresponse.getCorrelationID().intValue());
        if (obj != null) goto _L2; else goto _L1
_L1:
        Logger.e("XmSdlController", (new StringBuilder()).append("Received unknown response:").append(rpcresponse.getFunctionName()).append("(").append(rpcresponse.getCorrelationID()).append(")").toString());
_L4:
        return;
_L2:
        mRequestList.remove(rpcresponse.getCorrelationID().intValue());
        if (((String) (obj1)).equals("PutFile"))
        {
            obj = (PutFile)obj;
            Logger.e("XmSdlController", (new StringBuilder()).append("PUT_FILE_RESPONSE ").append(((PutFile) (obj)).getSdlFileName()).toString());
            obj1 = SdlImageResource.getImageResourceByName(((PutFile) (obj)).getSdlFileName());
            if (obj1 != null)
            {
                mAddedImageList.add(obj1);
                sendPendingRequest(((SdlImageResource) (obj1)));
                return;
            } else
            {
                mListener.onPutFile(((PutFile) (obj)), (PutFileResponse)rpcresponse);
                return;
            }
        }
        if (!((String) (obj1)).equals("DeleteFile"))
        {
            break; /* Loop/switch isn't completed */
        }
        obj = (DeleteFile)obj;
        rpcresponse = (DeleteFileResponse)rpcresponse;
        obj1 = SdlImageResource.getImageResourceByName(((DeleteFile) (obj)).getSdlFileName());
        if (obj1 != null)
        {
            if (rpcresponse.getSuccess().booleanValue())
            {
                mAddedImageList.remove(obj1);
                return;
            }
        } else
        {
            mListener.onDeleteFile(((DeleteFile) (obj)), rpcresponse);
            return;
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (!((String) (obj1)).equals("AddCommand"))
        {
            continue; /* Loop/switch isn't completed */
        }
        if (((AddCommandResponse)rpcresponse).getSuccess().booleanValue())
        {
            rpcresponse = (AddCommand)obj;
            return;
        }
        continue; /* Loop/switch isn't completed */
        if (((String) (obj1)).equals("DeleteCommand")) goto _L4; else goto _L5
_L5:
        if (!((String) (obj1)).equals("AddSubMenu"))
        {
            continue; /* Loop/switch isn't completed */
        }
        if (((AddSubMenuResponse)rpcresponse).getSuccess().booleanValue())
        {
            rpcresponse = (AddSubMenu)obj;
            return;
        }
        continue; /* Loop/switch isn't completed */
        if (((String) (obj1)).equals("DeleteSubMenu")) goto _L4; else goto _L6
_L6:
        if (!((String) (obj1)).equals("CreateInteractionChoiceSet"))
        {
            break; /* Loop/switch isn't completed */
        }
        obj = (CreateInteractionChoiceSet)obj;
        if (currentInteraction != null && ((CreateInteractionChoiceSet) (obj)).getInteractionChoiceSetID().intValue() == ((Integer)currentInteraction.getInteractionChoiceSetIDList().get(0)).intValue())
        {
            sendRPCRequestToService(currentInteraction);
            return;
        }
        if (mListener != null)
        {
            mListener.onCreateInteractionChoiceSetResponse(((CreateInteractionChoiceSet) (obj)), (CreateInteractionChoiceSetResponse)rpcresponse);
            return;
        }
        if (true) goto _L4; else goto _L7
_L7:
        if (!((String) (obj1)).equals("DeleteInteractionChoiceSet"))
        {
            break; /* Loop/switch isn't completed */
        }
        obj = (DeleteInteractionChoiceSet)obj;
        if (mListener != null)
        {
            mListener.onDeleteInteractionChoiceSetResponse(((DeleteInteractionChoiceSet) (obj)), (DeleteInteractionChoiceSetResponse)rpcresponse);
            return;
        }
        if (true) goto _L4; else goto _L8
_L8:
        if (!((String) (obj1)).equals("PerformInteraction"))
        {
            continue; /* Loop/switch isn't completed */
        }
        rpcresponse = (PerformInteractionResponse)rpcresponse;
        obj = (PerformInteraction)obj;
        if (mListener == null) goto _L4; else goto _L9
_L9:
        mListener.onInteraction(((PerformInteraction) (obj)), rpcresponse);
        return;
        if (!((String) (obj1)).equals("ShowConstantTBT")) goto _L4; else goto _L10
_L10:
        isShowing = false;
        return;
    }

    private void sendConnectMsg()
    {
        Message message = Message.obtain(null, 2);
        message.getData().putParcelable("data", mAppInfo);
        sendSdlMessage(message);
    }

    private void sendPendingRequest(SdlImageResource sdlimageresource)
    {
        Object obj = mPendingRequestList.get(sdlimageresource);
        if (obj == null)
        {
            return;
        }
        for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext(); sendRPCRequestToService((RPCRequest)((Iterator) (obj)).next())) { }
        mPendingRequestList.clear(sdlimageresource);
    }

    private void sendRPCRequestToService(RPCRequest rpcrequest)
    {
        if (!isConnected)
        {
            return;
        } else
        {
            Message message = Message.obtain(null, 6);
            message.getData().putParcelable("data", rpcrequest);
            sendSdlMessage(message);
            return;
        }
    }

    private void sendSdlMessage(Message message)
    {
        if (serviceMessenger != null)
        {
            message.replyTo = mMessenger;
            try
            {
                serviceMessenger.send(message);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Message message)
            {
                message.printStackTrace();
            }
            return;
        } else
        {
            isConnected = false;
            return;
        }
    }

    private void unbindService()
    {
        try
        {
            mContext.unbindService(mConnection);
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    public void SdlPutFile(String s, FileType filetype, byte abyte0[])
    {
        sendRPCRequestToService(RPCRequestFactory.buildPutFile(s, filetype, Boolean.valueOf(true), abyte0, null));
    }

    public void connect(String s, String s1, int i)
    {
        Logger.e("XmSdlController", "connect()");
        if (isConnected)
        {
            Logger.e("XmSdlController", "service is already connected!");
            return;
        }
        mAppInfo = new SdlAppInfo();
        mAppInfo.setName(s);
        mAppInfo.setAppHMIType(AppHMIType.MEDIA);
        mAppInfo.setIsMedia(true);
        initialize();
        if (serviceMessenger == null)
        {
            bindService();
            return;
        } else
        {
            sendConnectMsg();
            return;
        }
    }

    public void disconnect()
    {
        Logger.e("XmSdlController", (new StringBuilder()).append("disconnect() ").append(isConnected).toString());
        if (isConnected)
        {
            sendSdlMessage(Message.obtain(null, 3));
            unbindService();
            isConnected = false;
            return;
        } else
        {
            Logger.e("XmSdlController", "Service is not connected!");
            return;
        }
    }

    public boolean isConnected()
    {
        return isConnected;
    }

    public void sdlAddCommand(int i, String s, int j, int k, Image image)
    {
        s = RPCRequestFactory.buildAddCommand(Integer.valueOf(i), s, Integer.valueOf(j), Integer.valueOf(k), null, image, null);
        if (image != null)
        {
            image = SdlImageResource.getImageResourceByName(image.getValue());
            if (!mAddedImageList.contains(image.toString()))
            {
                mPendingRequestList.add(image, s);
                SdlPutFile(image.toString(), image.getFileType(), SdlImageResource.getImageRawDataByImageResource(mContext.getResources(), image));
                return;
            } else
            {
                sendRPCRequestToService(s);
                return;
            }
        } else
        {
            sendRPCRequestToService(s);
            return;
        }
    }

    public void sdlAddSubmenu(int i, String s, int j)
    {
        sendRPCRequestToService(RPCRequestFactory.buildAddSubMenu(Integer.valueOf(i), s, Integer.valueOf(j), null));
    }

    public void sdlAlert(String s, String s1, String s2, int i, Vector vector, boolean flag, boolean flag1)
    {
        Alert alert = new Alert();
        alert.setAlertText1(s);
        alert.setAlertText2(s1);
        alert.setAlertText3(s2);
        alert.setDuration(Integer.valueOf(i));
        alert.setSoftButtons(vector);
        alert.setPlayTone(Boolean.valueOf(flag));
        alert.setProgressIndicator(Boolean.valueOf(flag1));
        sendRPCRequestToService(alert);
    }

    public void sdlAlert(String s, String s1, String s2, boolean flag, Integer integer)
    {
        sendRPCRequestToService(RPCRequestFactory.buildAlert(s, s1, s2, Boolean.valueOf(flag), integer, null));
    }

    public void sdlCreateChoiceSet(Vector vector, Integer integer)
    {
        sendRPCRequestToService(RPCRequestFactory.buildCreateInteractionChoiceSet(vector, integer, null));
    }

    public void sdlCreateChoiceSetAndPerform(Vector vector, Integer integer, String s, int i, InteractionMode interactionmode, LayoutMode layoutmode)
    {
        sdlCreateChoiceSet(vector, integer);
        currentInteraction = new PerformInteraction();
        currentInteraction.setInitialText(s);
        vector = new ArrayList();
        vector.add(integer);
        currentInteraction.setInteractionChoiceSetIDList(vector);
        currentInteraction.setInteractionMode(interactionmode);
        currentInteraction.setInteractionLayout(layoutmode);
        currentInteraction.setTimeout(Integer.valueOf(i));
    }

    public void sdlDeleteChoiceSet(int i)
    {
        sendRPCRequestToService(RPCRequestFactory.buildDeleteInteractionChoiceSet(Integer.valueOf(i), null));
    }

    public void sdlDeleteCommand(int i)
    {
        sendRPCRequestToService(RPCRequestFactory.buildDeleteCommand(Integer.valueOf(i), null));
    }

    public void sdlDeleteFile(String s)
    {
        sendRPCRequestToService(RPCRequestFactory.buildDeleteFile(s, null));
    }

    public void sdlDeleteSubmenu(int i)
    {
        sendRPCRequestToService(RPCRequestFactory.buildDeleteSubMenu(Integer.valueOf(i), null));
    }

    public void sdlPerformInteraction(String s, String s1, Integer integer, String s2, String s3, InteractionMode interactionmode, Integer integer1)
    {
        sendRPCRequestToService(RPCRequestFactory.buildPerformInteraction(s, s1, integer, s2, s3, interactionmode, integer1, null));
    }

    public void sdlPerformInteraction(String s, List list, int i, InteractionMode interactionmode, LayoutMode layoutmode)
    {
        PerformInteraction performinteraction = new PerformInteraction();
        performinteraction.setInitialText(s);
        performinteraction.setInteractionChoiceSetIDList(list);
        performinteraction.setInteractionMode(interactionmode);
        performinteraction.setInteractionLayout(layoutmode);
        performinteraction.setTimeout(Integer.valueOf(i));
        sendRPCRequestToService(performinteraction);
    }

    public void sdlSetIcon(SdlImageResource sdlimageresource)
    {
        com.smartdevicelink.proxy.rpc.SetAppIcon setappicon = RPCRequestFactory.buildSetAppIcon(sdlimageresource.toString(), null);
        if (!mAddedImageList.contains(sdlimageresource))
        {
            mPendingRequestList.add(sdlimageresource, setappicon);
            SdlPutFile(sdlimageresource.toString(), sdlimageresource.getFileType(), SdlImageResource.getImageRawDataByImageResource(mContext.getResources(), sdlimageresource));
            return;
        } else
        {
            sendRPCRequestToService(setappicon);
            return;
        }
    }

    public void sdlSetIcon(String s)
    {
        SdlImageResource sdlimageresource = SdlImageResource.getImageResourceByName(s);
        s = RPCRequestFactory.buildSetAppIcon(s, null);
        if (!mAddedImageList.contains(sdlimageresource))
        {
            mPendingRequestList.add(sdlimageresource, s);
            return;
        } else
        {
            sendRPCRequestToService(s);
            return;
        }
    }

    public void sdlSetMediaClockTimer(StartTime starttime, StartTime starttime1, UpdateMode updatemode)
    {
        SetMediaClockTimer setmediaclocktimer = new SetMediaClockTimer();
        setmediaclocktimer.setStartTime(starttime);
        setmediaclocktimer.setEndTime(starttime1);
        setmediaclocktimer.setUpdateMode(updatemode);
        setmediaclocktimer.setCorrelationID(null);
        sendRPCRequestToService(setmediaclocktimer);
    }

    public void sdlSetMediaClockTimer(Integer integer, Integer integer1, Integer integer2, UpdateMode updatemode)
    {
        sendRPCRequestToService(RPCRequestFactory.buildSetMediaClockTimer(integer, integer1, integer2, updatemode, null));
    }

    public void sdlShow(String s, String s1, String s2, String s3, String s4, String s5, String s6, 
            Image image, Vector vector, Vector vector1, TextAlignment textalignment)
    {
        sendRPCRequestToService(RPCRequestFactory.buildShow(s, s1, s2, s3, s4, s5, s6, image, vector, vector1, textalignment, null));
    }

    public void sdlSpeak(String s)
    {
        sendRPCRequestToService(RPCRequestFactory.buildSpeak(TTSChunkFactory.createSimpleTTSChunks(s), null));
    }

    public void sdlSubscribeButton(ButtonName buttonname)
    {
        sendRPCRequestToService(RPCRequestFactory.buildSubscribeButton(buttonname, null));
    }

    public void setAppIcon(SdlImageResource sdlimageresource)
    {
        if (mAddedImageList.contains(sdlimageresource))
        {
            sdlSetIcon(sdlimageresource);
            return;
        } else
        {
            SdlPutFile(sdlimageresource.toString(), sdlimageresource.getFileType(), SdlImageResource.getImageRawDataByImageResource(mContext.getResources(), sdlimageresource));
            return;
        }
    }



/*
    static boolean access$102(SdlController sdlcontroller, boolean flag)
    {
        sdlcontroller.isConnected = flag;
        return flag;
    }

*/




/*
    static Messenger access$402(SdlController sdlcontroller, Messenger messenger)
    {
        sdlcontroller.serviceMessenger = messenger;
        return messenger;
    }

*/




    private class _cls1
        implements ServiceConnection
    {

        final SdlController this$0;

        public void onServiceConnected(ComponentName componentname, IBinder ibinder)
        {
            class _cls1
                implements android.os.IBinder.DeathRecipient
            {

                final _cls1 this$1;

                public void binderDied()
                {
                    isConnected = false;
                    mListener.onDisConnected();
                    initialize();
                    serviceMessenger = null;
                }

                _cls1()
                {
                    this$1 = _cls1.this;
                    super();
                }
            }

            try
            {
                ibinder.linkToDeath(new _cls1(), 0);
            }
            // Misplaced declaration of an exception variable
            catch (ComponentName componentname)
            {
                componentname.printStackTrace();
            }
            serviceMessenger = new Messenger(ibinder);
            sendConnectMsg();
        }

        public void onServiceDisconnected(ComponentName componentname)
        {
            isConnected = false;
            serviceMessenger = null;
            unbindService();
            initialize();
            mListener.onDisConnected();
        }

        _cls1()
        {
            this$0 = SdlController.this;
            super();
        }
    }

}
