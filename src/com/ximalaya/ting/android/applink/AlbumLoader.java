// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.sound.AlbumSoundModel;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            AlbumMenu

public class AlbumLoader
{
    private class LoaderTask
        implements Runnable
    {

        private TaskWrapper mWrapper;
        final AlbumLoader this$0;

        public void run()
        {
            Object obj;
            obj = new RequestParams();
            String s = (new StringBuilder()).append(mUrl).append(mWrapper.albumMenu.getAlbumId()).append("/").append(true).append("/").append(mWrapper.pageId).append("/").append(mPageSize).toString();
            obj = f.a().a(s, ((RequestParams) (obj)), false);
            if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1) goto _L2; else goto _L1
_L1:
            Object obj2;
            int i;
            try
            {
                obj2 = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
                i = ((JSONObject) (obj2)).getIntValue("ret");
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1)
            {
                ((Exception) (obj1)).printStackTrace();
                mWorkerHandler.post(this);
                return;
            }
            if (i == 76)
            {
                break MISSING_BLOCK_LABEL_422;
            }
            if (i != 0)
            {
                return;
            }
            obj = ((JSONObject) (obj2)).getString("album");
            if (TextUtils.isEmpty(((CharSequence) (obj))))
            {
                break MISSING_BLOCK_LABEL_417;
            }
            obj1 = (AlbumModel)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/album/AlbumModel);
_L3:
            obj2 = ((JSONObject) (obj2)).getJSONObject("tracks");
            i = ((JSONObject) (obj2)).getIntValue("maxPageId");
            if (((JSONObject) (obj2)).getIntValue("totalCount") <= 0 || mWrapper.pageId > i)
            {
                break MISSING_BLOCK_LABEL_422;
            }
            obj2 = JSON.parseArray(((JSONObject) (obj2)).getString("list"), com/ximalaya/ting/android/model/sound/AlbumSoundModel);
            if (obj2 == null)
            {
                break MISSING_BLOCK_LABEL_213;
            }
            if (((List) (obj2)).size() != 0)
            {
                break MISSING_BLOCK_LABEL_244;
            }
            mWorkerHandler.post(this);
            return;
            if (obj1 == null)
            {
                break MISSING_BLOCK_LABEL_298;
            }
            Iterator iterator = ((List) (obj2)).iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                AlbumSoundModel albumsoundmodel = (AlbumSoundModel)iterator.next();
                if (TextUtils.isEmpty(albumsoundmodel.albumImage))
                {
                    albumsoundmodel.albumImage = ((AlbumModel) (obj1)).coverLarge;
                }
            } while (true);
            synchronized (mTasks)
            {
                mTasks.remove(mWrapper);
            }
            obj1 = mUiHandler.obtainMessage(1);
            obj1.obj = ((Object) (new Object[] {
                mWrapper, ModelHelper.toSoundInfoforAlbum(((List) (obj2)))
            }));
            ((Message) (obj1)).sendToTarget();
            return;
            exception;
            obj1;
            JVM INSTR monitorexit ;
            throw exception;
_L2:
            Logger.e("AlbumLoader", (new StringBuilder()).append("retry load ").append(mWrapper.albumMenu.getMenuName()).toString());
            mWorkerHandler.post(this);
            return;
            obj1 = null;
              goto _L3
        }

        public LoaderTask(TaskWrapper taskwrapper)
        {
            this$0 = AlbumLoader.this;
            super();
            mWrapper = taskwrapper;
        }
    }

    private class TaskWrapper
    {

        public AlbumMenu albumMenu;
        public long id;
        public int pageId;
        final AlbumLoader this$0;

        private AlbumLoader getOuterType()
        {
            return AlbumLoader.this;
        }

        public boolean equals(Object obj)
        {
            if (this != obj)
            {
                if (obj == null)
                {
                    return false;
                }
                if (getClass() != obj.getClass())
                {
                    return false;
                }
                obj = (TaskWrapper)obj;
                if (!getOuterType().equals(((TaskWrapper) (obj)).getOuterType()))
                {
                    return false;
                }
                if (id != ((TaskWrapper) (obj)).id)
                {
                    return false;
                }
                if (pageId != ((TaskWrapper) (obj)).pageId)
                {
                    return false;
                }
            }
            return true;
        }

        public int hashCode()
        {
            return ((getOuterType().hashCode() + 31) * 31 + (int)(id ^ id >>> 32)) * 31 + pageId;
        }

        public TaskWrapper(AlbumMenu albummenu, int i)
        {
            this$0 = AlbumLoader.this;
            super();
            albumMenu = albummenu;
            id = albummenu.getAlbumId();
            pageId = i;
        }
    }


    private static final int MSG_UPDATE_LIST = 1;
    private static final String TAG = "AlbumLoader";
    private static volatile int mInstanceCount = 0;
    private static AlbumLoader sInstance;
    private static byte sLock[] = new byte[0];
    private int mPageSize;
    private List mTasks;
    private Handler mUiHandler;
    private String mUrl;
    private HandlerThread mWorker;
    private Handler mWorkerHandler;

    private AlbumLoader()
    {
        mPageSize = 15;
        mTasks = new ArrayList();
        mUrl = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString();
        mWorker = new HandlerThread("album-loader");
        mWorker.start();
        mWorkerHandler = new Handler(mWorker.getLooper());
        mUiHandler = new _cls1(Looper.getMainLooper());
    }

    public static AlbumLoader getInstance()
    {
        if (sInstance == null)
        {
            synchronized (sLock)
            {
                if (sInstance == null)
                {
                    sInstance = new AlbumLoader();
                }
            }
        }
        mInstanceCount++;
        return sInstance;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private void updateList(TaskWrapper taskwrapper, List list)
    {
        if (taskwrapper.pageId == 1)
        {
            taskwrapper.albumMenu.setSoundList(list);
        } else
        {
            taskwrapper.albumMenu.addSoundList(list);
        }
        taskwrapper.albumMenu.setPageId(taskwrapper.pageId);
    }

    public void preload(AlbumMenu albummenu, int i)
    {
        if (sInstance == null || albummenu == null || albummenu.getAlbumId() <= 0L)
        {
            return;
        } else
        {
            Logger.e("AlbumLoader", (new StringBuilder()).append("Preload ").append(albummenu.getAlbumId()).append("|").append(albummenu.getMenuName()).toString());
            albummenu = new LoaderTask(new TaskWrapper(albummenu, i));
            mWorkerHandler.post(albummenu);
            return;
        }
    }

    public void release()
    {
        mInstanceCount--;
        if (mInstanceCount <= 0)
        {
            mWorkerHandler.removeCallbacksAndMessages(null);
            mUiHandler.removeCallbacksAndMessages(null);
            mWorker.quit();
            sInstance = null;
        }
    }

    public void setPageSize(int i)
    {
        mPageSize = i;
    }








    private class _cls1 extends Handler
    {

        final AlbumLoader this$0;

        public void handleMessage(Message message)
        {
            switch (message.what)
            {
            default:
                return;

            case 1: // '\001'
                updateList((TaskWrapper)((Object[])(Object[])message.obj)[0], (List)((Object[])(Object[])message.obj)[1]);
                break;
            }
        }

        _cls1(Looper looper)
        {
            this$0 = AlbumLoader.this;
            super(looper);
        }
    }

}
