// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            BaseMenu

public class AlbumMenu extends BaseMenu
{

    private long mAlbumId;
    private int mCurrentIndex;
    private int mPageId;
    private int mPageSize;
    private List mSoundList;

    public AlbumMenu()
    {
        mCurrentIndex = 0;
    }

    public void addSoundList(List list)
    {
        if (list == null || list.size() == 0)
        {
            return;
        }
        if (mSoundList == null)
        {
            mSoundList = new ArrayList();
        }
        mSoundList.addAll(list);
    }

    public boolean equals(Object obj)
    {
        if (this != obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (getClass() != obj.getClass())
            {
                return false;
            }
            obj = (AlbumMenu)obj;
            if (mAlbumId != ((AlbumMenu) (obj)).mAlbumId)
            {
                return false;
            }
        }
        return true;
    }

    public long getAlbumId()
    {
        return mAlbumId;
    }

    public int getCurrentIndex()
    {
        return mCurrentIndex;
    }

    public int getPageId()
    {
        return mPageId;
    }

    public int getPageSize()
    {
        return mPageSize;
    }

    public List getSoundList()
    {
        return mSoundList;
    }

    public int hashCode()
    {
        return (int)(mAlbumId ^ mAlbumId >>> 32) + 31;
    }

    public void setAlbumId(long l)
    {
        mAlbumId = l;
    }

    public void setCurrentIndex(int i)
    {
        mCurrentIndex = i;
    }

    public void setPageId(int i)
    {
        mPageId = i;
    }

    public void setPageSize(int i)
    {
        mPageSize = i;
    }

    public void setSoundList(List list)
    {
        mSoundList = list;
    }
}
