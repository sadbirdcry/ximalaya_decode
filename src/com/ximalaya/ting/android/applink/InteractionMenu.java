// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import java.util.Vector;

public class InteractionMenu
{

    private Vector mChoiceSet;
    private int mChoiceSetId;
    private String mChoiceSetName;
    private int mLastChoiceSetId;
    private int mRequestId;

    public InteractionMenu()
    {
    }

    public Vector getChoiceSet()
    {
        return mChoiceSet;
    }

    public int getChoiceSetId()
    {
        return mChoiceSetId;
    }

    public String getChoiceSetName()
    {
        return mChoiceSetName;
    }

    public int getLastChoiceId()
    {
        return mLastChoiceSetId;
    }

    public int getRequestId()
    {
        return mRequestId;
    }

    public void resetLastChoiceId()
    {
        mLastChoiceSetId = 0;
    }

    public void setChoiceSet(Vector vector)
    {
        mChoiceSet = vector;
    }

    public void setChoiceSetId(int i)
    {
        mLastChoiceSetId = mChoiceSetId;
        mChoiceSetId = i;
    }

    public void setChoiceSetName(String s)
    {
        mChoiceSetName = s;
    }

    public void setRequestId(int i)
    {
        mRequestId = i;
    }
}
