// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import com.smartdevicelink.proxy.RPCRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            SdlImageResource

public class PendingRpcRequestList
{

    private HashMap mList;

    public PendingRpcRequestList()
    {
        mList = new HashMap();
    }

    public void add(SdlImageResource sdlimageresource, RPCRequest rpcrequest)
    {
        if (!mList.containsKey(sdlimageresource))
        {
            ArrayList arraylist = new ArrayList();
            mList.put(sdlimageresource, arraylist);
        }
        if (!((List)mList.get(sdlimageresource)).contains(rpcrequest))
        {
            ((List)mList.get(sdlimageresource)).add(rpcrequest);
        }
    }

    public void clear(SdlImageResource sdlimageresource)
    {
        if (mList.get(sdlimageresource) != null)
        {
            ((List)mList.get(sdlimageresource)).clear();
        }
    }

    public List get(SdlImageResource sdlimageresource)
    {
        return (List)mList.get(sdlimageresource);
    }
}
