// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            SdlConnectManager

class this._cls0 extends BroadcastReceiver
{

    final SdlConnectManager this$0;

    public void onReceive(Context context, Intent intent)
    {
        context = intent.getAction();
        if ("android.bluetooth.device.action.ACL_CONNECTED".equals(context))
        {
            context = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            onBtConnect(context);
        } else
        {
            if ("android.bluetooth.device.action.ACL_DISCONNECTED".equals(context))
            {
                context = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                onBtDisconnect(context);
                return;
            }
            if ("android.bluetooth.adapter.action.STATE_CHANGED".equals(context))
            {
                onBtDisconnect(null);
                return;
            }
            if ("com.smartdevicelink.USB_ACCESSORY_ATTACHED".equals(context))
            {
                Logger.e("SdlConnectManager", "USBTransport.ACTION_USB_ACCESSORY_ATTACHED");
                onUsbConnect();
                return;
            }
            if ("android.hardware.usb.action.USB_STATE".equals(context) && !intent.getBooleanExtra("connected", false))
            {
                onUsbDisconnect();
                return;
            }
        }
    }

    ()
    {
        this$0 = SdlConnectManager.this;
        super();
    }
}
