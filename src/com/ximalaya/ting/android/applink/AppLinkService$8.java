// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import com.ford.syncV4.proxy.rpc.enums.AudioStreamingState;
import com.ford.syncV4.proxy.rpc.enums.HMILevel;
import com.ford.syncV4.proxy.rpc.enums.SystemContext;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            AppLinkService

static class 
{

    static final int $SwitchMap$com$ford$syncV4$proxy$rpc$enums$AudioStreamingState[];
    static final int $SwitchMap$com$ford$syncV4$proxy$rpc$enums$HMILevel[];
    static final int $SwitchMap$com$ford$syncV4$proxy$rpc$enums$SystemContext[];

    static 
    {
        $SwitchMap$com$ford$syncV4$proxy$rpc$enums$HMILevel = new int[HMILevel.values().length];
        try
        {
            $SwitchMap$com$ford$syncV4$proxy$rpc$enums$HMILevel[HMILevel.HMI_FULL.ordinal()] = 1;
        }
        catch (NoSuchFieldError nosuchfielderror8) { }
        try
        {
            $SwitchMap$com$ford$syncV4$proxy$rpc$enums$HMILevel[HMILevel.HMI_LIMITED.ordinal()] = 2;
        }
        catch (NoSuchFieldError nosuchfielderror7) { }
        try
        {
            $SwitchMap$com$ford$syncV4$proxy$rpc$enums$HMILevel[HMILevel.HMI_BACKGROUND.ordinal()] = 3;
        }
        catch (NoSuchFieldError nosuchfielderror6) { }
        try
        {
            $SwitchMap$com$ford$syncV4$proxy$rpc$enums$HMILevel[HMILevel.HMI_NONE.ordinal()] = 4;
        }
        catch (NoSuchFieldError nosuchfielderror5) { }
        $SwitchMap$com$ford$syncV4$proxy$rpc$enums$AudioStreamingState = new int[AudioStreamingState.values().length];
        try
        {
            $SwitchMap$com$ford$syncV4$proxy$rpc$enums$AudioStreamingState[AudioStreamingState.AUDIBLE.ordinal()] = 1;
        }
        catch (NoSuchFieldError nosuchfielderror4) { }
        try
        {
            $SwitchMap$com$ford$syncV4$proxy$rpc$enums$AudioStreamingState[AudioStreamingState.NOT_AUDIBLE.ordinal()] = 2;
        }
        catch (NoSuchFieldError nosuchfielderror3) { }
        $SwitchMap$com$ford$syncV4$proxy$rpc$enums$SystemContext = new int[SystemContext.values().length];
        try
        {
            $SwitchMap$com$ford$syncV4$proxy$rpc$enums$SystemContext[SystemContext.SYSCTXT_MAIN.ordinal()] = 1;
        }
        catch (NoSuchFieldError nosuchfielderror2) { }
        try
        {
            $SwitchMap$com$ford$syncV4$proxy$rpc$enums$SystemContext[SystemContext.SYSCTXT_VRSESSION.ordinal()] = 2;
        }
        catch (NoSuchFieldError nosuchfielderror1) { }
        try
        {
            $SwitchMap$com$ford$syncV4$proxy$rpc$enums$SystemContext[SystemContext.SYSCTXT_MENU.ordinal()] = 3;
        }
        catch (NoSuchFieldError nosuchfielderror)
        {
            return;
        }
    }
}
