// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import com.ford.syncV4.proxy.rpc.AddCommandResponse;
import com.ford.syncV4.proxy.rpc.AddSubMenuResponse;
import com.ford.syncV4.proxy.rpc.AlertResponse;
import com.ford.syncV4.proxy.rpc.ChangeRegistrationResponse;
import com.ford.syncV4.proxy.rpc.CreateInteractionChoiceSetResponse;
import com.ford.syncV4.proxy.rpc.DeleteCommandResponse;
import com.ford.syncV4.proxy.rpc.DeleteFileResponse;
import com.ford.syncV4.proxy.rpc.DeleteInteractionChoiceSetResponse;
import com.ford.syncV4.proxy.rpc.DeleteSubMenuResponse;
import com.ford.syncV4.proxy.rpc.DiagnosticMessageResponse;
import com.ford.syncV4.proxy.rpc.EndAudioPassThruResponse;
import com.ford.syncV4.proxy.rpc.GenericResponse;
import com.ford.syncV4.proxy.rpc.GetDTCsResponse;
import com.ford.syncV4.proxy.rpc.GetVehicleDataResponse;
import com.ford.syncV4.proxy.rpc.ListFilesResponse;
import com.ford.syncV4.proxy.rpc.OnAudioPassThru;
import com.ford.syncV4.proxy.rpc.OnButtonEvent;
import com.ford.syncV4.proxy.rpc.OnButtonPress;
import com.ford.syncV4.proxy.rpc.OnCommand;
import com.ford.syncV4.proxy.rpc.OnDriverDistraction;
import com.ford.syncV4.proxy.rpc.OnHMIStatus;
import com.ford.syncV4.proxy.rpc.OnHashChange;
import com.ford.syncV4.proxy.rpc.OnKeyboardInput;
import com.ford.syncV4.proxy.rpc.OnLanguageChange;
import com.ford.syncV4.proxy.rpc.OnLockScreenStatus;
import com.ford.syncV4.proxy.rpc.OnPermissionsChange;
import com.ford.syncV4.proxy.rpc.OnSystemRequest;
import com.ford.syncV4.proxy.rpc.OnTBTClientState;
import com.ford.syncV4.proxy.rpc.OnTouchEvent;
import com.ford.syncV4.proxy.rpc.OnVehicleData;
import com.ford.syncV4.proxy.rpc.PerformAudioPassThruResponse;
import com.ford.syncV4.proxy.rpc.PerformInteractionResponse;
import com.ford.syncV4.proxy.rpc.PutFileResponse;
import com.ford.syncV4.proxy.rpc.ReadDIDResponse;
import com.ford.syncV4.proxy.rpc.ResetGlobalPropertiesResponse;
import com.ford.syncV4.proxy.rpc.ScrollableMessageResponse;
import com.ford.syncV4.proxy.rpc.SetAppIconResponse;
import com.ford.syncV4.proxy.rpc.SetDisplayLayoutResponse;
import com.ford.syncV4.proxy.rpc.SetGlobalPropertiesResponse;
import com.ford.syncV4.proxy.rpc.SetMediaClockTimerResponse;
import com.ford.syncV4.proxy.rpc.ShowResponse;
import com.ford.syncV4.proxy.rpc.SliderResponse;
import com.ford.syncV4.proxy.rpc.SpeakResponse;
import com.ford.syncV4.proxy.rpc.SubscribeButtonResponse;
import com.ford.syncV4.proxy.rpc.SubscribeVehicleDataResponse;
import com.ford.syncV4.proxy.rpc.SystemRequestResponse;
import com.ford.syncV4.proxy.rpc.UnsubscribeButtonResponse;
import com.ford.syncV4.proxy.rpc.UnsubscribeVehicleDataResponse;
import com.ford.syncV4.proxy.rpc.enums.SyncDisconnectedReason;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            IProxyListenerALMEx, SyncProxyEx, SelectByVoiceResponse

public class ProxyListenerALMEx
    implements IProxyListenerALMEx
{

    private static final String TAG = "ProxyListenerALMEx";
    private static ProxyListenerALMEx mInstance = null;
    private IProxyListenerALMEx mForwordListener;
    private SyncProxyEx mProxy;

    protected ProxyListenerALMEx()
    {
        mProxy = null;
        mForwordListener = null;
    }

    public static ProxyListenerALMEx getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new ProxyListenerALMEx();
        }
        return mInstance;
    }

    public void onAddCommandResponse(AddCommandResponse addcommandresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onAddCommandResponse(addcommandresponse);
        }
    }

    public void onAddSubMenuResponse(AddSubMenuResponse addsubmenuresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onAddSubMenuResponse(addsubmenuresponse);
        }
    }

    public void onAlertResponse(AlertResponse alertresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onAlertResponse(alertresponse);
        }
    }

    public void onChangeRegistrationResponse(ChangeRegistrationResponse changeregistrationresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onChangeRegistrationResponse(changeregistrationresponse);
        }
    }

    public void onCreateInteractionChoiceSetResponse(CreateInteractionChoiceSetResponse createinteractionchoicesetresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onCreateInteractionChoiceSetResponse(createinteractionchoicesetresponse);
        }
    }

    public void onDeleteCommandResponse(DeleteCommandResponse deletecommandresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onDeleteCommandResponse(deletecommandresponse);
        }
    }

    public void onDeleteFileResponse(DeleteFileResponse deletefileresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onDeleteFileResponse(deletefileresponse);
        }
    }

    public void onDeleteInteractionChoiceSetResponse(DeleteInteractionChoiceSetResponse deleteinteractionchoicesetresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onDeleteInteractionChoiceSetResponse(deleteinteractionchoicesetresponse);
        }
    }

    public void onDeleteSubMenuResponse(DeleteSubMenuResponse deletesubmenuresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onDeleteSubMenuResponse(deletesubmenuresponse);
        }
    }

    public void onDiagnosticMessageResponse(DiagnosticMessageResponse diagnosticmessageresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onDiagnosticMessageResponse(diagnosticmessageresponse);
        }
    }

    public void onEndAudioPassThruResponse(EndAudioPassThruResponse endaudiopassthruresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onEndAudioPassThruResponse(endaudiopassthruresponse);
        }
    }

    public void onError(String s, Exception exception)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onError(s, exception);
        }
    }

    public void onGenericResponse(GenericResponse genericresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onGenericResponse(genericresponse);
        }
    }

    public void onGetDTCsResponse(GetDTCsResponse getdtcsresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onGetDTCsResponse(getdtcsresponse);
        }
    }

    public void onGetVehicleDataResponse(GetVehicleDataResponse getvehicledataresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onGetVehicleDataResponse(getvehicledataresponse);
        }
    }

    public void onListFilesResponse(ListFilesResponse listfilesresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onListFilesResponse(listfilesresponse);
        }
    }

    public void onOnAudioPassThru(OnAudioPassThru onaudiopassthru)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnAudioPassThru(onaudiopassthru);
        }
    }

    public void onOnButtonEvent(OnButtonEvent onbuttonevent)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnButtonEvent(onbuttonevent);
        }
    }

    public void onOnButtonPress(OnButtonPress onbuttonpress)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnButtonPress(onbuttonpress);
        }
    }

    public void onOnCommand(OnCommand oncommand)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnCommand(oncommand);
        }
    }

    public void onOnDriverDistraction(OnDriverDistraction ondriverdistraction)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnDriverDistraction(ondriverdistraction);
        }
    }

    public void onOnHMIStatus(OnHMIStatus onhmistatus)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnHMIStatus(onhmistatus);
        }
    }

    public void onOnHashChange(OnHashChange onhashchange)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnHashChange(onhashchange);
        }
    }

    public void onOnKeyboardInput(OnKeyboardInput onkeyboardinput)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnKeyboardInput(onkeyboardinput);
        }
    }

    public void onOnLanguageChange(OnLanguageChange onlanguagechange)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnLanguageChange(onlanguagechange);
        }
    }

    public void onOnLockScreenNotification(OnLockScreenStatus onlockscreenstatus)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnLockScreenNotification(onlockscreenstatus);
        }
    }

    public void onOnPermissionsChange(OnPermissionsChange onpermissionschange)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnPermissionsChange(onpermissionschange);
        }
    }

    public void onOnSystemRequest(OnSystemRequest onsystemrequest)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnSystemRequest(onsystemrequest);
        }
    }

    public void onOnTBTClientState(OnTBTClientState ontbtclientstate)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnTBTClientState(ontbtclientstate);
        }
    }

    public void onOnTouchEvent(OnTouchEvent ontouchevent)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnTouchEvent(ontouchevent);
        }
    }

    public void onOnVehicleData(OnVehicleData onvehicledata)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onOnVehicleData(onvehicledata);
        }
    }

    public void onPerformAudioPassThruResponse(PerformAudioPassThruResponse performaudiopassthruresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onPerformAudioPassThruResponse(performaudiopassthruresponse);
        }
    }

    public void onPerformInteractionResponse(PerformInteractionResponse performinteractionresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onPerformInteractionResponse(performinteractionresponse);
        }
    }

    public void onProxyClosed(String s, Exception exception, SyncDisconnectedReason syncdisconnectedreason)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onProxyClosed(s, exception, syncdisconnectedreason);
        }
    }

    public void onPutFileResponse(PutFileResponse putfileresponse)
    {
        Logger.e("ProxyListenerALMEx", (new StringBuilder()).append("onPutFileResponse ").append(putfileresponse.getCorrelationID()).append(", ").append(putfileresponse.getSuccess()).append(", ").append(putfileresponse.getInfo()).toString());
        mProxy.onPutFileSuccess(putfileresponse.getCorrelationID().intValue(), putfileresponse.getSuccess().booleanValue());
        if (mForwordListener != null)
        {
            mForwordListener.onPutFileResponse(putfileresponse);
        }
    }

    public void onReadDIDResponse(ReadDIDResponse readdidresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onReadDIDResponse(readdidresponse);
        }
    }

    public void onResetGlobalPropertiesResponse(ResetGlobalPropertiesResponse resetglobalpropertiesresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onResetGlobalPropertiesResponse(resetglobalpropertiesresponse);
        }
    }

    public void onScrollableMessageResponse(ScrollableMessageResponse scrollablemessageresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onScrollableMessageResponse(scrollablemessageresponse);
        }
    }

    public void onSelectByVoice(SelectByVoiceResponse selectbyvoiceresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onSelectByVoice(selectbyvoiceresponse);
        }
    }

    public void onSetAppIconResponse(SetAppIconResponse setappiconresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onSetAppIconResponse(setappiconresponse);
        }
    }

    public void onSetDisplayLayoutResponse(SetDisplayLayoutResponse setdisplaylayoutresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onSetDisplayLayoutResponse(setdisplaylayoutresponse);
        }
    }

    public void onSetGlobalPropertiesResponse(SetGlobalPropertiesResponse setglobalpropertiesresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onSetGlobalPropertiesResponse(setglobalpropertiesresponse);
        }
    }

    public void onSetMediaClockTimerResponse(SetMediaClockTimerResponse setmediaclocktimerresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onSetMediaClockTimerResponse(setmediaclocktimerresponse);
        }
    }

    public void onShowResponse(ShowResponse showresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onShowResponse(showresponse);
        }
    }

    public void onSliderResponse(SliderResponse sliderresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onSliderResponse(sliderresponse);
        }
    }

    public void onSpeakResponse(SpeakResponse speakresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onSpeakResponse(speakresponse);
        }
    }

    public void onSubscribeButtonResponse(SubscribeButtonResponse subscribebuttonresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onSubscribeButtonResponse(subscribebuttonresponse);
        }
    }

    public void onSubscribeVehicleDataResponse(SubscribeVehicleDataResponse subscribevehicledataresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onSubscribeVehicleDataResponse(subscribevehicledataresponse);
        }
    }

    public void onSystemRequestResponse(SystemRequestResponse systemrequestresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onSystemRequestResponse(systemrequestresponse);
        }
    }

    public void onUnsubscribeButtonResponse(UnsubscribeButtonResponse unsubscribebuttonresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onUnsubscribeButtonResponse(unsubscribebuttonresponse);
        }
    }

    public void onUnsubscribeVehicleDataResponse(UnsubscribeVehicleDataResponse unsubscribevehicledataresponse)
    {
        if (mForwordListener != null)
        {
            mForwordListener.onUnsubscribeVehicleDataResponse(unsubscribevehicledataresponse);
        }
    }

    public void setProxyAndForwordListener(SyncProxyEx syncproxyex, IProxyListenerALMEx iproxylisteneralmex)
    {
        mProxy = syncproxyex;
        mForwordListener = iproxylisteneralmex;
    }

}
