// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.ximalaya.ting.android.fragment.BaseFragment;

public class LockScreenFragment extends BaseFragment
{

    private ImageView mLockScreenImage;
    private int mResId;

    public LockScreenFragment()
    {
    }

    public void onActivityCreated(Bundle bundle)
    {
        mLockScreenImage.setImageResource(mResId);
        mLockScreenImage.setOnClickListener(new _cls2());
        super.onActivityCreated(bundle);
    }

    public boolean onBackPressed()
    {
        return true;
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuinflater)
    {
        menu.add("menu");
        super.onCreateOptionsMenu(menu, menuinflater);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f03001a, viewgroup, false);
        mLockScreenImage = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a00bb);
        return fragmentBaseContainerView;
    }

    public void setLockScreenImage(int i)
    {
        mResId = i;
        FragmentActivity fragmentactivity = getActivity();
        if (fragmentactivity == null || fragmentactivity.isFinishing())
        {
            return;
        } else
        {
            getActivity().runOnUiThread(new _cls1());
            return;
        }
    }



    private class _cls2
        implements android.view.View.OnClickListener
    {

        final LockScreenFragment this$0;

        public void onClick(View view)
        {
        }

        _cls2()
        {
            this$0 = LockScreenFragment.this;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final LockScreenFragment this$0;

        public void run()
        {
            if (mLockScreenImage != null)
            {
                mLockScreenImage.setImageResource(mResId);
            }
        }

        _cls1()
        {
            this$0 = LockScreenFragment.this;
            super();
        }
    }

}
