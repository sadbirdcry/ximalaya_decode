// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.graphics.Point;
import com.smartdevicelink.proxy.rpc.CreateInteractionChoiceSet;
import com.smartdevicelink.proxy.rpc.CreateInteractionChoiceSetResponse;
import com.smartdevicelink.proxy.rpc.DeleteFile;
import com.smartdevicelink.proxy.rpc.DeleteFileResponse;
import com.smartdevicelink.proxy.rpc.DeleteInteractionChoiceSet;
import com.smartdevicelink.proxy.rpc.DeleteInteractionChoiceSetResponse;
import com.smartdevicelink.proxy.rpc.OnButtonEvent;
import com.smartdevicelink.proxy.rpc.OnButtonPress;
import com.smartdevicelink.proxy.rpc.OnCommand;
import com.smartdevicelink.proxy.rpc.OnHMIStatus;
import com.smartdevicelink.proxy.rpc.PerformInteraction;
import com.smartdevicelink.proxy.rpc.PerformInteractionResponse;
import com.smartdevicelink.proxy.rpc.PutFile;
import com.smartdevicelink.proxy.rpc.PutFileResponse;
import com.smartdevicelink.proxy.rpc.enums.TouchType;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            SdlController

public static interface 
{

    public abstract void onButtonEvent(OnButtonEvent onbuttonevent);

    public abstract void onButtonPress(OnButtonPress onbuttonpress);

    public abstract void onCommand(OnCommand oncommand);

    public abstract void onConnected();

    public abstract void onCreateInteractionChoiceSetResponse(CreateInteractionChoiceSet createinteractionchoiceset, CreateInteractionChoiceSetResponse createinteractionchoicesetresponse);

    public abstract void onDeleteFile(DeleteFile deletefile, DeleteFileResponse deletefileresponse);

    public abstract void onDeleteInteractionChoiceSetResponse(DeleteInteractionChoiceSet deleteinteractionchoiceset, DeleteInteractionChoiceSetResponse deleteinteractionchoicesetresponse);

    public abstract void onDisConnected();

    public abstract void onHMIStatus(OnHMIStatus onhmistatus);

    public abstract void onInteraction(PerformInteraction performinteraction, PerformInteractionResponse performinteractionresponse);

    public abstract void onPutFile(PutFile putfile, PutFileResponse putfileresponse);

    public abstract void onTouchEvent(TouchType touchtype, Point point);
}
