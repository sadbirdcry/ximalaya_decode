// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import com.ford.syncV4.proxy.rpc.enums.UpdateMode;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            AppLinkService

class this._cls0
    implements com.ximalaya.ting.android.service.play.nPlayerStatusUpdateListener
{

    final AppLinkService this$0;

    public void onBufferUpdated(int i)
    {
    }

    public void onLogoPlayFinished()
    {
    }

    public void onPlayCompleted()
    {
        AppLinkService.access$902(AppLinkService.this, false);
        SoundInfo soundinfo = AppLinkService.access$1000(AppLinkService.this).getCurSound();
        if (soundinfo != null)
        {
            AppLinkService.access$1300(AppLinkService.this, soundinfo.albumName, soundinfo.title, AppLinkService.access$1100(AppLinkService.this), 0, UpdateMode.CLEAR);
        }
    }

    public void onPlayPaused()
    {
        AppLinkService.access$902(AppLinkService.this, false);
        SoundInfo soundinfo = AppLinkService.access$1000(AppLinkService.this).getCurSound();
        if (soundinfo != null)
        {
            AppLinkService.access$1300(AppLinkService.this, soundinfo.albumName, soundinfo.title, AppLinkService.access$1100(AppLinkService.this), AppLinkService.access$1200(AppLinkService.this).getCurPosition(), UpdateMode.PAUSE);
        }
    }

    public void onPlayProgressUpdate(int i, int j)
    {
    }

    public void onPlayStarted()
    {
        Logger.e("AppLinkService", "onPlayStarted");
        AppLinkService.access$902(AppLinkService.this, false);
        SoundInfo soundinfo = AppLinkService.access$1000(AppLinkService.this).getCurSound();
        if (soundinfo != null)
        {
            AppLinkService.access$1300(AppLinkService.this, soundinfo.albumName, soundinfo.title, AppLinkService.access$1100(AppLinkService.this), AppLinkService.access$1200(AppLinkService.this).getCurPosition(), UpdateMode.COUNTUP);
        }
    }

    public void onPlayerBuffering(boolean flag)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("onPlayerBuffering ").append(flag).toString());
        AppLinkService.access$902(AppLinkService.this, flag);
    }

    public void onSoundPrepared(int i)
    {
        AppLinkService.access$902(AppLinkService.this, false);
    }

    public void onStartPlayLogo()
    {
    }

    rvice()
    {
        this$0 = AppLinkService.this;
        super();
    }
}
