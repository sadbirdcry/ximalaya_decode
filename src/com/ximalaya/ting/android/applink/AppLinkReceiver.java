// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.ximalaya.ting.android.dl.PluginConstants;
import com.ximalaya.ting.android.dl.PluginManager;
import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            SdlConnectManager

public class AppLinkReceiver extends BroadcastReceiver
{

    private static final String TAG = "AppLinkReceiver";

    public AppLinkReceiver()
    {
    }

    private boolean isAppLinkLibLoaded(Context context)
    {
        return PluginManager.getInstance(context).isPluginLoaded(PluginConstants.PLUGIN_APPLINK);
    }

    private boolean isSdlLibLoaded(Context context)
    {
        return PluginManager.getInstance(context).isPluginLoaded(PluginConstants.PLUGIN_SDL);
    }

    public void onReceive(Context context, Intent intent)
    {
        String s;
        SdlConnectManager sdlconnectmanager;
        s = intent.getAction();
        sdlconnectmanager = SdlConnectManager.getInstance(context);
        if (!"android.bluetooth.device.action.ACL_CONNECTED".equals(s)) goto _L2; else goto _L1
_L1:
        if (isAppLinkLibLoaded(context)) goto _L4; else goto _L3
_L3:
        Logger.e("AppLinkReceiver", "AppLink Dex Library Not Load!");
_L6:
        return;
_L4:
        sdlconnectmanager.onBtConnect((BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE"));
        return;
_L2:
        int i;
        if ("android.bluetooth.device.action.ACL_DISCONNECTED".equals(s))
        {
            if (!isAppLinkLibLoaded(context))
            {
                Logger.e("AppLinkReceiver", "AppLink Dex Library Not Load!");
                return;
            } else
            {
                sdlconnectmanager.onBtDisconnect((BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE"));
                return;
            }
        }
        if (!"android.bluetooth.adapter.action.STATE_CHANGED".equals(s))
        {
            continue; /* Loop/switch isn't completed */
        }
        if (!isAppLinkLibLoaded(context))
        {
            Logger.e("AppLinkReceiver", "AppLink Dex Library Not Load!");
            return;
        }
        i = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", -1);
        if (i == 11 || i != 13) goto _L6; else goto _L5
_L5:
        sdlconnectmanager.onBtDisconnect(null);
        return;
        if (!"com.smartdevicelink.USB_ACCESSORY_ATTACHED".equals(s)) goto _L6; else goto _L7
_L7:
        if (!isSdlLibLoaded(context))
        {
            Logger.e("AppLinkReceiver", "SDL Dex Library Not Load!");
            return;
        } else
        {
            Logger.e("AppLinkReceiver", "USBTransport.ACTION_USB_ACCESSORY_ATTACHED");
            sdlconnectmanager.onUsbConnect();
            return;
        }
    }
}
