// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.content.res.Resources;
import android.text.TextUtils;
import com.smartdevicelink.proxy.rpc.Choice;
import com.smartdevicelink.proxy.rpc.Image;
import com.smartdevicelink.proxy.rpc.enums.ImageType;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.HistoryManage;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            SdlService, AlbumMenu, AlbumLoader, InteractionMenu, 
//            SdlController

private class <init> extends MyAsyncTask
{

    final SdlService this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        avoid = SdlService.access$6000(SdlService.this).getSoundInfoList();
        Object obj = new ArrayList();
        if (avoid != null)
        {
            ((List) (obj)).addAll(avoid);
        }
        avoid = new ArrayList();
        if (obj != null && ((List) (obj)).size() > 0)
        {
            obj = ((List) (obj)).iterator();
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break;
                }
                SoundInfo soundinfo = (SoundInfo)((Iterator) (obj)).next();
                if (soundinfo.albumId > 0L && !TextUtils.isEmpty(soundinfo.albumName))
                {
                    AlbumMenu albummenu1 = new AlbumMenu();
                    albummenu1.setAlbumId(soundinfo.albumId);
                    if (!avoid.contains(albummenu1))
                    {
                        albummenu1.setMenuName(soundinfo.albumName);
                        albummenu1.setMenuId(SdlService.access$2808(SdlService.this));
                        avoid.add(albummenu1);
                        SdlService.access$3200(SdlService.this).preload(albummenu1, albummenu1.getPageId() + 1);
                    }
                }
            } while (true);
        }
        if (avoid.size() == 0)
        {
            AlbumMenu albummenu = new AlbumMenu();
            albummenu.setMenuName("\u65E0\u64AD\u653E\u5386\u53F2");
            albummenu.setAlbumId(-1L);
            albummenu.setMenuId(SdlService.access$2808(SdlService.this));
            avoid.add(albummenu);
            SdlService.access$5000(SdlService.this).add(albummenu);
        }
        return avoid;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(List list)
    {
        super.onPostExecute(list);
        SdlService.access$5902(SdlService.this, false);
        if (!checkSdlStatus())
        {
            return;
        }
        SdlService.access$6102(SdlService.this, System.currentTimeMillis());
        SdlService.access$6200(SdlService.this).clear();
        SdlService.access$6200(SdlService.this).addAll(list);
        list = new Vector();
        Choice choice;
        for (Iterator iterator = SdlService.access$6200(SdlService.this).iterator(); iterator.hasNext(); list.add(choice))
        {
            Object obj = (AlbumMenu)iterator.next();
            choice = new Choice();
            choice.setChoiceID(Integer.valueOf(((AlbumMenu) (obj)).getMenuId()));
            choice.setMenuName(SdlService.access$5100(SdlService.this, ((AlbumMenu) (obj)).getMenuName()));
            choice.setVrCommands(new Vector(Arrays.asList(new String[] {
                SdlService.access$5100(SdlService.this, ((AlbumMenu) (obj)).getMenuName())
            })));
            obj = new Image();
            ((Image) (obj)).setImageType(ImageType.DYNAMIC);
            ((Image) (obj)).setValue(SdlService.access$5200(SdlService.this));
            choice.setImage(((Image) (obj)));
        }

        if (SdlService.access$4000(SdlService.this).getChoiceSetId() > 0)
        {
            SdlService.access$1300(SdlService.this).sdlDeleteChoiceSet(SdlService.access$4000(SdlService.this).getChoiceSetId());
        }
        SdlService.access$4000(SdlService.this).setChoiceSet(list);
        SdlService.access$4000(SdlService.this).setChoiceSetId(SdlService.access$2808(SdlService.this));
        SdlService.access$4000(SdlService.this).setChoiceSetName(getResources().getString(0x7f090231));
        SdlService.access$3700(SdlService.this, SdlService.access$4000(SdlService.this));
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        SdlService.access$5902(SdlService.this, true);
    }

    private ()
    {
        this$0 = SdlService.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
