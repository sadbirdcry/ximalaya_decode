// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ford.syncV4.exception.SyncException;
import com.ford.syncV4.exception.SyncExceptionCause;
import com.ford.syncV4.proxy.SyncProxyALM;
import com.ford.syncV4.proxy.rpc.AddCommandResponse;
import com.ford.syncV4.proxy.rpc.AddSubMenuResponse;
import com.ford.syncV4.proxy.rpc.AlertResponse;
import com.ford.syncV4.proxy.rpc.ChangeRegistrationResponse;
import com.ford.syncV4.proxy.rpc.Choice;
import com.ford.syncV4.proxy.rpc.CreateInteractionChoiceSetResponse;
import com.ford.syncV4.proxy.rpc.DeleteCommandResponse;
import com.ford.syncV4.proxy.rpc.DeleteFileResponse;
import com.ford.syncV4.proxy.rpc.DeleteInteractionChoiceSetResponse;
import com.ford.syncV4.proxy.rpc.DeleteSubMenuResponse;
import com.ford.syncV4.proxy.rpc.DiagnosticMessageResponse;
import com.ford.syncV4.proxy.rpc.EndAudioPassThruResponse;
import com.ford.syncV4.proxy.rpc.GenericResponse;
import com.ford.syncV4.proxy.rpc.GetDTCsResponse;
import com.ford.syncV4.proxy.rpc.GetVehicleDataResponse;
import com.ford.syncV4.proxy.rpc.ListFilesResponse;
import com.ford.syncV4.proxy.rpc.OnAudioPassThru;
import com.ford.syncV4.proxy.rpc.OnButtonEvent;
import com.ford.syncV4.proxy.rpc.OnButtonPress;
import com.ford.syncV4.proxy.rpc.OnCommand;
import com.ford.syncV4.proxy.rpc.OnDriverDistraction;
import com.ford.syncV4.proxy.rpc.OnHMIStatus;
import com.ford.syncV4.proxy.rpc.OnHashChange;
import com.ford.syncV4.proxy.rpc.OnKeyboardInput;
import com.ford.syncV4.proxy.rpc.OnLanguageChange;
import com.ford.syncV4.proxy.rpc.OnLockScreenStatus;
import com.ford.syncV4.proxy.rpc.OnPermissionsChange;
import com.ford.syncV4.proxy.rpc.OnSystemRequest;
import com.ford.syncV4.proxy.rpc.OnTBTClientState;
import com.ford.syncV4.proxy.rpc.OnTouchEvent;
import com.ford.syncV4.proxy.rpc.OnVehicleData;
import com.ford.syncV4.proxy.rpc.PerformAudioPassThruResponse;
import com.ford.syncV4.proxy.rpc.PerformInteractionResponse;
import com.ford.syncV4.proxy.rpc.PutFileResponse;
import com.ford.syncV4.proxy.rpc.ReadDIDResponse;
import com.ford.syncV4.proxy.rpc.ResetGlobalPropertiesResponse;
import com.ford.syncV4.proxy.rpc.ScrollableMessageResponse;
import com.ford.syncV4.proxy.rpc.SetAppIconResponse;
import com.ford.syncV4.proxy.rpc.SetDisplayLayoutResponse;
import com.ford.syncV4.proxy.rpc.SetGlobalPropertiesResponse;
import com.ford.syncV4.proxy.rpc.SetMediaClockTimerResponse;
import com.ford.syncV4.proxy.rpc.ShowResponse;
import com.ford.syncV4.proxy.rpc.SliderResponse;
import com.ford.syncV4.proxy.rpc.SoftButton;
import com.ford.syncV4.proxy.rpc.SpeakResponse;
import com.ford.syncV4.proxy.rpc.SubscribeButtonResponse;
import com.ford.syncV4.proxy.rpc.SubscribeVehicleDataResponse;
import com.ford.syncV4.proxy.rpc.SystemRequestResponse;
import com.ford.syncV4.proxy.rpc.UnsubscribeButtonResponse;
import com.ford.syncV4.proxy.rpc.UnsubscribeVehicleDataResponse;
import com.ford.syncV4.proxy.rpc.enums.AudioStreamingState;
import com.ford.syncV4.proxy.rpc.enums.ButtonName;
import com.ford.syncV4.proxy.rpc.enums.FileType;
import com.ford.syncV4.proxy.rpc.enums.HMILevel;
import com.ford.syncV4.proxy.rpc.enums.InteractionMode;
import com.ford.syncV4.proxy.rpc.enums.Language;
import com.ford.syncV4.proxy.rpc.enums.LockScreenStatus;
import com.ford.syncV4.proxy.rpc.enums.SoftButtonType;
import com.ford.syncV4.proxy.rpc.enums.SyncDisconnectedReason;
import com.ford.syncV4.proxy.rpc.enums.SystemAction;
import com.ford.syncV4.proxy.rpc.enums.SystemContext;
import com.ford.syncV4.proxy.rpc.enums.UpdateMode;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoNew;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.HistoryManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.modelnew.AlbumModelNew;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.service.play.Playlist;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            InteractionMenu, BtnId, IdGenerator, SyncProxyEx, 
//            AlbumMenu, SdlConnectManager, AlbumLoader, SelectByVoiceResponse, 
//            IProxyListenerALMEx

public class AppLinkService extends Service
{
    private class LoadCollectedTask extends MyAsyncTask
    {

        private String mUrl;
        final AppLinkService this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            int i = 0;
            avoid = AlbumModelManage.getInstance();
            if (!UserInfoMannage.hasLogined() && avoid.isLocalCollectEnable(mAppCtx))
            {
                avoid = avoid.getLocalCollectAlbumList();
                mLoadCollectSuccess = true;
                StringBuilder stringbuilder = (new StringBuilder()).append("get local collected album ");
                if (avoid != null)
                {
                    i = avoid.size();
                }
                Logger.e("AppLinkService", stringbuilder.append(i).toString());
                return avoid;
            }
            avoid = new RequestParams();
            avoid.put("pageId", (new StringBuilder()).append(mCollectPageId).append("").toString());
            avoid.put("pageSize", (new StringBuilder()).append(mPageSize).append("").toString());
            avoid = f.a().a(mUrl, avoid, false);
            if (((com.ximalaya.ting.android.b.n.a) (avoid)).b == 1 && !TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (avoid)).a))
            {
                try
                {
                    avoid = JSON.parseArray(JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (avoid)).a).getString("list"), com/ximalaya/ting/android/model/album/AlbumModel);
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    avoid.printStackTrace();
                    return null;
                }
                return avoid;
            } else
            {
                alert("\u4EB2\uFF0C\u7F51\u7EDC\u4E0D\u7ED9\u529B", "\u8BF7\u7A0D\u5019\u518D\u8BD5");
                return null;
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            super.onPostExecute(list);
            mLoadingCollect = false;
            mLoadCollectSuccess = false;
            if (!checkProxyStatus())
            {
                return;
            }
            if (list != null && list.size() > 0)
            {
                mLastLoadCollectTime = System.currentTimeMillis();
                mLoadCollectSuccess = true;
                if (mCollectPageId == 1)
                {
                    mCollectedAlbums.clear();
                }
                for (list = list.iterator(); list.hasNext();)
                {
                    AlbumModel albummodel = (AlbumModel)list.next();
                    if (TextUtils.isEmpty(albummodel.title))
                    {
                        Logger.e("AppLinkService", (new StringBuilder()).append("Ignore album ").append(albummodel.albumId).toString());
                    } else
                    {
                        AlbumMenu albummenu = new AlbumMenu();
                        albummenu.setMenuName(albummodel.title);
                        albummenu.setAlbumId(albummodel.albumId);
                        albummenu.setMenuId(IdGenerator.next());
                        mCollectedAlbums.add(albummenu);
                        mAlbumLoader.preload(albummenu, albummenu.getPageId() + 1);
                    }
                }

                int i = 
// JavaClassFileOutputException: get_constant: invalid tag

        protected void onPreExecute()
        {
            onPreExecute();
            mLoadingCollect = true;
            Logger.e("AppLinkService", "LoadCollectedTask");
        }

        private LoadCollectedTask()
        {
            this$0 = AppLinkService.this;
            super();
            mUrl = "mobile/album/subscribe/list";
        }

        LoadCollectedTask(_cls1 _pcls1)
        {
            this();
        }
    }

    private class LoadHistoryTask extends MyAsyncTask
    {

        final AppLinkService this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            avoid = mHistoryManage.getSoundInfoList();
            Object obj = new ArrayList();
            if (avoid != null)
            {
                ((List) (obj)).addAll(avoid);
            }
            avoid = new ArrayList();
            if (obj != null && ((List) (obj)).size() > 0)
            {
                obj = ((List) (obj)).iterator();
                do
                {
                    if (!((Iterator) (obj)).hasNext())
                    {
                        break;
                    }
                    SoundInfo soundinfo = (SoundInfo)((Iterator) (obj)).next();
                    if (soundinfo.albumId > 0L && !TextUtils.isEmpty(soundinfo.albumName))
                    {
                        AlbumMenu albummenu1 = new AlbumMenu();
                        albummenu1.setAlbumId(soundinfo.albumId);
                        if (!avoid.contains(albummenu1))
                        {
                            Logger.e("AppLinkService", (new StringBuilder()).append("AlbumName ").append(soundinfo.albumName).toString());
                            albummenu1.setMenuName(soundinfo.albumName);
                            albummenu1.setMenuId(IdGenerator.next());
                            avoid.add(albummenu1);
                            mAlbumLoader.preload(albummenu1, albummenu1.getPageId() + 1);
                        }
                    }
                } while (true);
            }
            if (avoid.size() == 0)
            {
                AlbumMenu albummenu = new AlbumMenu();
                albummenu.setMenuName("\u65E0\u64AD\u653E\u5386\u53F2");
                albummenu.setAlbumId(-1L);
                albummenu.setMenuId(IdGenerator.next());
                avoid.add(albummenu);
            }
            return avoid;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            onPostExecute(list);
            mLoadingHistory = false;
            if (!checkProxyStatus())
            {
                return;
            }
            mLastLoadHistoryTime = System.currentTimeMillis();
            mHistoryAlbums.clear();
            mHistoryAlbums.addAll(list);
            list = new Vector();
            Choice choice;
            for (Iterator iterator = mHistoryAlbums.iterator(); iterator.hasNext(); list.add(choice))
            {
                AlbumMenu albummenu = (AlbumMenu)iterator.next();
                choice = new Choice();
                choice.setChoiceID(Integer.valueOf(albummenu.getMenuId()));
                choice.setMenuName(albummenu.getMenuName());
                choice.setVrCommands(new Vector(Arrays.asList(new String[] {
                    albummenu.getMenuName()
                })));
            }

            mHistoryAlbumChoiceSet.setChoiceSet(list);
            mHistoryAlbumChoiceSet.setChoiceSetId(IdGenerator.next());
            mHistoryAlbumChoiceSet.setChoiceSetName(getString(0x7f090226));
            createInteractionChoiceSet(mHistoryAlbumChoiceSet);
        }

        protected void onPreExecute()
        {
            onPreExecute();
            mLoadingHistory = true;
        }

        private LoadHistoryTask()
        {
            this$0 = AppLinkService.this;
            super();
        }

        LoadHistoryTask(_cls1 _pcls1)
        {
            this();
        }
    }

    private class LoadHotAlbumTask extends MyAsyncTask
    {

        private String mUrl;
        final AppLinkService this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            avoid = new RequestParams();
            avoid.put("page", (new StringBuilder()).append("").append(mHotAlbumPageId).toString());
            avoid.put("per_page", (new StringBuilder()).append("").append(mPageSize).toString());
            avoid.put("category_name", "all");
            avoid.put("condition", "hot");
            avoid.put("status", "0");
            avoid.put("tag_name", "");
            avoid = f.a().a(mUrl, avoid, true);
            if (((com.ximalaya.ting.android.b.n.a) (avoid)).b != 1) goto _L2; else goto _L1
_L1:
            avoid = JSON.parseArray(JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (avoid)).a).getString("list"), com/ximalaya/ting/android/modelnew/AlbumModelNew);
            return avoid;
            avoid;
            avoid.printStackTrace();
_L4:
            return null;
_L2:
            alert("\u4EB2\uFF0C\u7F51\u7EDC\u4E0D\u7ED9\u529B", "\u8BF7\u7A0D\u5019\u518D\u8BD5");
            if (true) goto _L4; else goto _L3
_L3:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            super.onPostExecute(list);
            mLoadingHotAlbum = false;
            mLoadHotAlbumSuccess = false;
            if (!checkProxyStatus())
            {
                return;
            }
            Vector vector = new Vector();
            if (list != null && list.size() != 0)
            {
                mLastLoadHotAlbumTime = System.currentTimeMillis();
                mLoadHotAlbumSuccess = true;
                if (mHotAlbumPageId == 1)
                {
                    mHotAlbums.clear();
                }
                Object obj;
                for (list = list.iterator(); list.hasNext(); vector.add(obj))
                {
                    obj = (AlbumModelNew)list.next();
                    AlbumMenu albummenu = new AlbumMenu();
                    albummenu.setMenuName(((AlbumModelNew) (obj)).title);
                    albummenu.setAlbumId(((AlbumModelNew) (obj)).id);
                    albummenu.setMenuId(IdGenerator.next());
                    mHotAlbums.add(albummenu);
                    mAlbumLoader.preload(albummenu, albummenu.getPageId() + 1);
                    obj = new Choice();
                    ((Choice) (obj)).setChoiceID(Integer.valueOf(albummenu.getMenuId()));
                    ((Choice) (obj)).setMenuName(albummenu.getMenuName());
                    ((Choice) (obj)).setVrCommands(new Vector(Arrays.asList(new String[] {
                        albummenu.getMenuName()
                    })));
                }

            } else
            {
                mLoadHotAlbumSuccess = false;
            }
            if (mHotAlbums.size() == 0)
            {
                list = new AlbumMenu();
                list.setMenuName("\u65E0\u70ED\u95E8\u4E13\u8F91");
                list.setAlbumId(-1L);
                list.setMenuId(IdGenerator.next());
                mHotAlbums.add(list);
                Choice choice = new Choice();
                choice.setChoiceID(Integer.valueOf(list.getMenuId()));
                choice.setMenuName(list.getMenuName());
                choice.setVrCommands(new Vector(Arrays.asList(new String[] {
                    list.getMenuName()
                })));
                vector.add(choice);
            }
            mHotAlbumChoiceSet.setChoiceSet(vector);
            mHotAlbumChoiceSet.setChoiceSetId(IdGenerator.next());
            mHotAlbumChoiceSet.setChoiceSetName(getResources().getString(0x7f090228));
            createInteractionChoiceSet(mHotAlbumChoiceSet);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            mLoadingHotAlbum = true;
        }

        private LoadHotAlbumTask()
        {
            this$0 = AppLinkService.this;
            super();
            mUrl = (new StringBuilder()).append(ApiUtil.getApiHost()).append("m/explore_album_list").toString();
        }

        LoadHotAlbumTask(_cls1 _pcls1)
        {
            this();
        }
    }

    private class LoadHotSoundTask extends MyAsyncTask
    {

        private String mUrl;
        final AppLinkService this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            avoid = new RequestParams();
            avoid.put("page", (new StringBuilder()).append("").append(mHotSoundPageId).toString());
            avoid.put("per_page", (new StringBuilder()).append("").append(mPageSize).toString());
            avoid.put("condition", "daily");
            avoid.put("category_name", "all");
            avoid.put("tag_name", "");
            avoid = f.a().a(mUrl, avoid, true);
            if (((com.ximalaya.ting.android.b.n.a) (avoid)).b != 1) goto _L2; else goto _L1
_L1:
            avoid = ModelHelper.toSoundInfo(JSON.parseArray(JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (avoid)).a).getString("list"), com/ximalaya/ting/android/model/sound/SoundInfoNew));
            return avoid;
            avoid;
            avoid.printStackTrace();
_L4:
            return null;
_L2:
            alert("\u4EB2\uFF0C\u7F51\u7EDC\u4E0D\u7ED9\u529B", "\u8BF7\u7A0D\u5019\u518D\u8BD5");
            if (true) goto _L4; else goto _L3
_L3:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            super.onPostExecute(list);
            mLoadingHotSound = false;
            mLoadHotSoundSuccess = false;
            if (!checkProxyStatus())
            {
                return;
            }
            if (list == null || list.size() == 0)
            {
                mLoadHotSoundSuccess = false;
                return;
            }
            mLoadHotSoundSuccess = true;
            mLastLoadHotSoundTime = System.currentTimeMillis();
            if (mHotSoundPageId == 1)
            {
                mHotSounds.clear();
            }
            mHotSounds.addAll(list);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            mLoadingHotSound = true;
        }

        private LoadHotSoundTask()
        {
            this$0 = AppLinkService.this;
            super();
            mUrl = (new StringBuilder()).append(ApiUtil.getApiHost()).append("m/explore_track_list").toString();
        }

        LoadHotSoundTask(_cls1 _pcls1)
        {
            this();
        }
    }

    private class LoadLocalAlbumTask extends MyAsyncTask
    {

        final AppLinkService this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            avoid = DownloadHandler.getInstance(mAppCtx).getSortedFinishedDownloadList();
            if (avoid == null)
            {
                return null;
            }
            Vector vector = mLocalAlbums;
            vector;
            JVM INSTR monitorenter ;
            Iterator iterator;
            mLocalAlbums.clear();
            iterator = avoid.iterator();
_L7:
            if (!iterator.hasNext()) goto _L2; else goto _L1
_L1:
            AlbumMenu albummenu;
            DownloadTask downloadtask;
            downloadtask = (DownloadTask)iterator.next();
            albummenu = getLocalAlbumByAlbumId(downloadtask.albumId);
            avoid = albummenu;
            if (albummenu != null) goto _L4; else goto _L3
_L3:
            avoid = new AlbumMenu();
            if (downloadtask.albumId <= 0L || TextUtils.isEmpty(downloadtask.albumName))
            {
                break MISSING_BLOCK_LABEL_186;
            }
            avoid.setMenuName(downloadtask.albumName);
_L5:
            avoid.setAlbumId(downloadtask.albumId);
            avoid.setSoundList(new Vector());
            avoid.setMenuId(IdGenerator.next());
            mLocalAlbums.add(avoid);
_L4:
            avoid.getSoundList().add(downloadtask);
            continue; /* Loop/switch isn't completed */
            avoid;
            vector;
            JVM INSTR monitorexit ;
            throw avoid;
            avoid.setMenuName("\u672A\u547D\u540D\u4E13\u8F91");
              goto _L5
_L2:
            if (mLocalAlbums.size() == 0)
            {
                avoid = new AlbumMenu();
                avoid.setAlbumId(-1L);
                avoid.setMenuName("\u672C\u5730\u65E0\u4E13\u8F91");
                avoid.setSoundList(new Vector(0));
                avoid.setMenuId(IdGenerator.next());
                mLocalAlbums.add(avoid);
            }
            vector;
            JVM INSTR monitorexit ;
            return null;
            if (true) goto _L7; else goto _L6
_L6:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            super.onPostExecute(void1);
            mAlbumLoading = false;
            if (!checkProxyStatus())
            {
                return;
            }
            void1 = new Vector();
            int j = mLocalAlbums.size();
            for (int i = 0; i < j; i++)
            {
                AlbumMenu albummenu = (AlbumMenu)mLocalAlbums.get(i);
                Choice choice = new Choice();
                choice.setChoiceID(Integer.valueOf(albummenu.getMenuId()));
                choice.setMenuName(albummenu.getMenuName());
                choice.setVrCommands(new Vector(Arrays.asList(new String[] {
                    albummenu.getMenuName()
                })));
                void1.add(choice);
            }

            mLocalAlbumChoiceSet.setChoiceSet(void1);
            mLocalAlbumChoiceSet.setChoiceSetId(IdGenerator.next());
            mLocalAlbumChoiceSet.setChoiceSetName(getResources().getString(0x7f090224));
            createInteractionChoiceSet(mLocalAlbumChoiceSet);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            mAlbumLoading = true;
        }

        private LoadLocalAlbumTask()
        {
            this$0 = AppLinkService.this;
            super();
        }

        LoadLocalAlbumTask(_cls1 _pcls1)
        {
            this();
        }
    }

    private class ProxyListener
        implements IProxyListenerALMEx
    {

        final AppLinkService this$0;

        public void onAddCommandResponse(AddCommandResponse addcommandresponse)
        {
            AppLinkService.instance.onAddCommandResponse(addcommandresponse);
        }

        public void onAddSubMenuResponse(AddSubMenuResponse addsubmenuresponse)
        {
            AppLinkService.instance.onAddSubMenuResponse(addsubmenuresponse);
        }

        public void onAlertResponse(AlertResponse alertresponse)
        {
            AppLinkService.instance.onAlertResponse(alertresponse);
        }

        public void onChangeRegistrationResponse(ChangeRegistrationResponse changeregistrationresponse)
        {
            AppLinkService.instance.onChangeRegistrationResponse(changeregistrationresponse);
        }

        public void onCreateInteractionChoiceSetResponse(CreateInteractionChoiceSetResponse createinteractionchoicesetresponse)
        {
            AppLinkService.instance.onCreateInteractionChoiceSetResponse(createinteractionchoicesetresponse);
        }

        public void onDeleteCommandResponse(DeleteCommandResponse deletecommandresponse)
        {
            AppLinkService.instance.onDeleteCommandResponse(deletecommandresponse);
        }

        public void onDeleteFileResponse(DeleteFileResponse deletefileresponse)
        {
            AppLinkService.instance.onDeleteFileResponse(deletefileresponse);
        }

        public void onDeleteInteractionChoiceSetResponse(DeleteInteractionChoiceSetResponse deleteinteractionchoicesetresponse)
        {
            AppLinkService.instance.onDeleteInteractionChoiceSetResponse(deleteinteractionchoicesetresponse);
        }

        public void onDeleteSubMenuResponse(DeleteSubMenuResponse deletesubmenuresponse)
        {
            AppLinkService.instance.onDeleteSubMenuResponse(deletesubmenuresponse);
        }

        public void onDiagnosticMessageResponse(DiagnosticMessageResponse diagnosticmessageresponse)
        {
            AppLinkService.instance.onDiagnosticMessageResponse(diagnosticmessageresponse);
        }

        public void onEndAudioPassThruResponse(EndAudioPassThruResponse endaudiopassthruresponse)
        {
            AppLinkService.instance.onEndAudioPassThruResponse(endaudiopassthruresponse);
        }

        public void onError(String s, Exception exception)
        {
            AppLinkService.instance.onError(s, exception);
        }

        public void onGenericResponse(GenericResponse genericresponse)
        {
            AppLinkService.instance.onGenericResponse(genericresponse);
        }

        public void onGetDTCsResponse(GetDTCsResponse getdtcsresponse)
        {
            AppLinkService.instance.onGetDTCsResponse(getdtcsresponse);
        }

        public void onGetVehicleDataResponse(GetVehicleDataResponse getvehicledataresponse)
        {
            AppLinkService.instance.onGetVehicleDataResponse(getvehicledataresponse);
        }

        public void onListFilesResponse(ListFilesResponse listfilesresponse)
        {
            AppLinkService.instance.onListFilesResponse(listfilesresponse);
        }

        public void onOnAudioPassThru(OnAudioPassThru onaudiopassthru)
        {
            AppLinkService.instance.onOnAudioPassThru(onaudiopassthru);
        }

        public void onOnButtonEvent(OnButtonEvent onbuttonevent)
        {
            AppLinkService.instance.onOnButtonEvent(onbuttonevent);
        }

        public void onOnButtonPress(OnButtonPress onbuttonpress)
        {
            AppLinkService.instance.onOnButtonPress(onbuttonpress);
        }

        public void onOnCommand(OnCommand oncommand)
        {
            AppLinkService.instance.onOnCommand(oncommand);
        }

        public void onOnDriverDistraction(OnDriverDistraction ondriverdistraction)
        {
            AppLinkService.instance.onOnDriverDistraction(ondriverdistraction);
        }

        public void onOnHMIStatus(OnHMIStatus onhmistatus)
        {
            AppLinkService.instance.onOnHMIStatus(onhmistatus);
        }

        public void onOnHashChange(OnHashChange onhashchange)
        {
            AppLinkService.instance.onOnHashChange(onhashchange);
        }

        public void onOnKeyboardInput(OnKeyboardInput onkeyboardinput)
        {
            AppLinkService.instance.onOnKeyboardInput(onkeyboardinput);
        }

        public void onOnLanguageChange(OnLanguageChange onlanguagechange)
        {
            AppLinkService.instance.onOnLanguageChange(onlanguagechange);
        }

        public void onOnLockScreenNotification(OnLockScreenStatus onlockscreenstatus)
        {
            AppLinkService.instance.onOnLockScreenNotification(onlockscreenstatus);
        }

        public void onOnPermissionsChange(OnPermissionsChange onpermissionschange)
        {
            AppLinkService.instance.onOnPermissionsChange(onpermissionschange);
        }

        public void onOnSystemRequest(OnSystemRequest onsystemrequest)
        {
            AppLinkService.instance.onOnSystemRequest(onsystemrequest);
        }

        public void onOnTBTClientState(OnTBTClientState ontbtclientstate)
        {
            AppLinkService.instance.onOnTBTClientState(ontbtclientstate);
        }

        public void onOnTouchEvent(OnTouchEvent ontouchevent)
        {
            AppLinkService.instance.onOnTouchEvent(ontouchevent);
        }

        public void onOnVehicleData(OnVehicleData onvehicledata)
        {
            AppLinkService.instance.onOnVehicleData(onvehicledata);
        }

        public void onPerformAudioPassThruResponse(PerformAudioPassThruResponse performaudiopassthruresponse)
        {
            AppLinkService.instance.onPerformAudioPassThruResponse(performaudiopassthruresponse);
        }

        public void onPerformInteractionResponse(PerformInteractionResponse performinteractionresponse)
        {
            AppLinkService.instance.onPerformInteractionResponse(performinteractionresponse);
        }

        public void onProxyClosed(String s, Exception exception, SyncDisconnectedReason syncdisconnectedreason)
        {
            AppLinkService.instance.onProxyClosed(s, exception, syncdisconnectedreason);
        }

        public void onPutFileResponse(PutFileResponse putfileresponse)
        {
            AppLinkService.instance.onPutFileResponse(putfileresponse);
        }

        public void onReadDIDResponse(ReadDIDResponse readdidresponse)
        {
            AppLinkService.instance.onReadDIDResponse(readdidresponse);
        }

        public void onResetGlobalPropertiesResponse(ResetGlobalPropertiesResponse resetglobalpropertiesresponse)
        {
            AppLinkService.instance.onResetGlobalPropertiesResponse(resetglobalpropertiesresponse);
        }

        public void onScrollableMessageResponse(ScrollableMessageResponse scrollablemessageresponse)
        {
            AppLinkService.instance.onScrollableMessageResponse(scrollablemessageresponse);
        }

        public void onSelectByVoice(SelectByVoiceResponse selectbyvoiceresponse)
        {
            AppLinkService.instance.onSelectByVoice(selectbyvoiceresponse);
        }

        public void onSetAppIconResponse(SetAppIconResponse setappiconresponse)
        {
            AppLinkService.instance.onSetAppIconResponse(setappiconresponse);
        }

        public void onSetDisplayLayoutResponse(SetDisplayLayoutResponse setdisplaylayoutresponse)
        {
            AppLinkService.instance.onSetDisplayLayoutResponse(setdisplaylayoutresponse);
        }

        public void onSetGlobalPropertiesResponse(SetGlobalPropertiesResponse setglobalpropertiesresponse)
        {
            AppLinkService.instance.onSetGlobalPropertiesResponse(setglobalpropertiesresponse);
        }

        public void onSetMediaClockTimerResponse(SetMediaClockTimerResponse setmediaclocktimerresponse)
        {
            AppLinkService.instance.onSetMediaClockTimerResponse(setmediaclocktimerresponse);
        }

        public void onShowResponse(ShowResponse showresponse)
        {
            AppLinkService.instance.onShowResponse(showresponse);
        }

        public void onSliderResponse(SliderResponse sliderresponse)
        {
            AppLinkService.instance.onSliderResponse(sliderresponse);
        }

        public void onSpeakResponse(SpeakResponse speakresponse)
        {
            AppLinkService.instance.onSpeakResponse(speakresponse);
        }

        public void onSubscribeButtonResponse(SubscribeButtonResponse subscribebuttonresponse)
        {
            AppLinkService.instance.onSubscribeButtonResponse(subscribebuttonresponse);
        }

        public void onSubscribeVehicleDataResponse(SubscribeVehicleDataResponse subscribevehicledataresponse)
        {
            AppLinkService.instance.onSubscribeVehicleDataResponse(subscribevehicledataresponse);
        }

        public void onSystemRequestResponse(SystemRequestResponse systemrequestresponse)
        {
            AppLinkService.instance.onSystemRequestResponse(systemrequestresponse);
        }

        public void onUnsubscribeButtonResponse(UnsubscribeButtonResponse unsubscribebuttonresponse)
        {
            AppLinkService.instance.onUnsubscribeButtonResponse(unsubscribebuttonresponse);
        }

        public void onUnsubscribeVehicleDataResponse(UnsubscribeVehicleDataResponse unsubscribevehicledataresponse)
        {
            AppLinkService.instance.onUnsubscribeVehicleDataResponse(unsubscribevehicledataresponse);
        }

        private ProxyListener()
        {
            this$0 = AppLinkService.this;
            super();
        }

        ProxyListener(_cls1 _pcls1)
        {
            this();
        }
    }


    private static final int CONNECTION_TIMEOUT = 60000;
    private static final long LOAD_EXPIRE_IN = 60000L;
    private static final long LOAD_HISTORY_EXPIRE_IN = 60000L;
    private static final int STOP_SERVICE_DELAY = 5000;
    private static final String TAG = "AppLinkService";
    private static AppLinkService instance = null;
    private AlbumLoader mAlbumLoader;
    private boolean mAlbumLoading;
    private Context mAppCtx;
    private String mAppIconName;
    private int mAppResId;
    private BluetoothAdapter mBtAdapter;
    private SoftButton mBtnCollect;
    private SoftButton mBtnHistory;
    private BtnId mBtnId;
    private SoftButton mBtnLocal;
    private volatile boolean mBuffering;
    private Runnable mCheckConnectionRunnable;
    private InteractionMenu mCollectAlbumChoiceSet;
    private int mCollectPageId;
    private Vector mCollectedAlbums;
    private Vector mCommonSoftbutton;
    private DownloadHandler mDownloadHander;
    private com.ximalaya.ting.android.transaction.download.DownloadHandler.DownloadSoundsListener mDownloadListener;
    private boolean mEnable3G;
    private InteractionMenu mHistoryAlbumChoiceSet;
    private Vector mHistoryAlbums;
    private HistoryManage mHistoryManage;
    private InteractionMenu mHotAlbumChoiceSet;
    private int mHotAlbumPageId;
    private Vector mHotAlbums;
    private int mHotSoundPageId;
    private List mHotSounds;
    private long mLastAlert;
    private String mLastCoverUrl;
    private long mLastLoadCollectTime;
    private long mLastLoadHistoryTime;
    private long mLastLoadHotAlbumTime;
    private long mLastLoadHotSoundTime;
    private LoadLocalAlbumTask mLoadAlbumTask;
    private boolean mLoadCollectSuccess;
    private LoadCollectedTask mLoadCollectedTask;
    private LoadHistoryTask mLoadHistoryTask;
    private boolean mLoadHotAlbumSuccess;
    private LoadHotAlbumTask mLoadHotAlbumTask;
    private boolean mLoadHotSoundSuccess;
    private LoadHotSoundTask mLoadHotSoundTask;
    private boolean mLoadingCollect;
    private boolean mLoadingHistory;
    private boolean mLoadingHotAlbum;
    private boolean mLoadingHotSound;
    private InteractionMenu mLocalAlbumChoiceSet;
    private Vector mLocalAlbums;
    private int mPageSize;
    private boolean mPaused;
    private PlayListControl mPlayList;
    private LocalMediaService mPlayer;
    private SyncProxyEx mProxy;
    private ProxyListener mProxyListener;
    private boolean mRegisted;
    private com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener mServiceListener;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mStatusListener;
    private Runnable mStopServiceRunnable;
    private Handler mUiHandler;

    public AppLinkService()
    {
        mProxy = null;
        mLastAlert = 0L;
        mCommonSoftbutton = new Vector();
        mLocalAlbums = new Vector();
        mCollectedAlbums = new Vector();
        mHistoryAlbums = new Vector();
        mHotAlbums = new Vector();
        mHotSounds = new ArrayList();
        mAlbumLoading = false;
        mLoadingCollect = false;
        mLoadingHistory = false;
        mLoadingHotSound = false;
        mLoadingHotAlbum = false;
        mLoadCollectSuccess = false;
        mLoadHotSoundSuccess = false;
        mLoadHotAlbumSuccess = false;
        mPaused = false;
        mCollectPageId = 1;
        mHotAlbumPageId = 1;
        mHotSoundPageId = 1;
        mPageSize = 20;
        mLastLoadCollectTime = 0L;
        mLastLoadHotSoundTime = 0L;
        mLastLoadHotAlbumTime = 0L;
        mLastLoadHistoryTime = 0L;
        mEnable3G = false;
        mLocalAlbumChoiceSet = new InteractionMenu();
        mCollectAlbumChoiceSet = new InteractionMenu();
        mHistoryAlbumChoiceSet = new InteractionMenu();
        mHotAlbumChoiceSet = new InteractionMenu();
        mBuffering = false;
        mBtnId = new BtnId();
        mRegisted = false;
        mAppIconName = "app_icon";
        mAppResId = 0x7f020598;
        mUiHandler = new _cls1();
        mDownloadListener = new _cls2();
        mStatusListener = new _cls3();
        mServiceListener = new _cls4();
        mCheckConnectionRunnable = new _cls5();
        mStopServiceRunnable = new _cls6();
    }

    private void addCommonCommand()
    {
        if (!checkProxyStatus())
        {
            return;
        }
        try
        {
            mBtnId.btn_hot_album = IdGenerator.next();
            mProxy.addCommand(Integer.valueOf(mBtnId.btn_hot_album), getResources().getString(0x7f090223), new Vector(Arrays.asList(new String[] {
                getResources().getString(0x7f090223)
            })), Integer.valueOf(IdGenerator.next()));
            mBtnId.btn_hot_sound = IdGenerator.next();
            mProxy.addCommand(Integer.valueOf(mBtnId.btn_hot_sound), getResources().getString(0x7f090222), new Vector(Arrays.asList(new String[] {
                getResources().getString(0x7f090222)
            })), Integer.valueOf(IdGenerator.next()));
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void alert(String s, String s1)
    {
        if (checkProxyStatus()) goto _L2; else goto _L1
_L1:
        long l;
        return;
_L2:
        if ((l = System.currentTimeMillis()) - mLastAlert < (long)5000) goto _L1; else goto _L3
_L3:
        mLastAlert = l;
        Logger.e("AppLinkService", (new StringBuilder()).append("Alert ").append(s1).toString());
        StringBuilder stringbuilder = (new StringBuilder()).append(s);
        String s2;
        if (TextUtils.isEmpty(s1))
        {
            s2 = "";
        } else
        {
            s2 = s1;
        }
        try
        {
            s2 = stringbuilder.append(s2).toString();
            mProxy.alert(s2, s, s1, Boolean.valueOf(true), Integer.valueOf(5000), Integer.valueOf(IdGenerator.next()));
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
        return;
    }

    private void clearLockScreen()
    {
        MainTabActivity2 maintabactivity2 = MainTabActivity2.mainTabActivity;
        if (maintabactivity2 != null)
        {
            maintabactivity2.clearLockScreen();
        }
    }

    private void clearProxyCache()
    {
        if (mProxy != null)
        {
            mProxy.clearCache();
        }
    }

    private void deleteSyncFile(String s)
    {
        if (!checkProxyStatus())
        {
            return;
        }
        try
        {
            mProxy.deletefile(s, Integer.valueOf(IdGenerator.next()));
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
    }

    private void doActionAudible()
    {
        if (mPaused && mPlayer != null && !mPlayer.isPlaying())
        {
            mPlayer.start();
            mPaused = false;
        }
    }

    private void doActionChooseAlbum(Message message)
    {
        int i = message.arg1;
        int j = message.arg2;
        Logger.e("AppLinkService", (new StringBuilder()).append("doActionChooseAlbum reqId:").append(j).append(", albumId:").append(i).toString());
        message = getAlbumMenu(i, j);
        if (message == null || message.getAlbumId() == -1L) goto _L2; else goto _L1
_L1:
        if (message.getSoundList() == null || message.getSoundList().size() <= 0) goto _L4; else goto _L3
_L3:
        Logger.e("AppLinkService", (new StringBuilder()).append("Choose Album ").append(message.getMenuName()).toString());
        PlayTools.gotoPlay(3, message.getSoundList(), 0, mAppCtx, false, null);
_L6:
        return;
_L4:
        alert("\u4EB2\uFF0C\u4E13\u8F91\u5217\u8868\u6B63\u5728\u52A0\u8F7D\u4E2D\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5...", null);
        return;
_L2:
        if (message == null)
        {
            Logger.e("AppLinkService", "doActionChooseAlbum find album null");
            return;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    private void doActionNotAudible()
    {
        if (mPlayer != null && mPlayer.isPlaying())
        {
            mPlayer.pause();
            mPaused = true;
        }
    }

    private void doActionPlayNext()
    {
        if (mPlayer == null || mPlayList.getPlaylist().getData() == null)
        {
            return;
        }
        if (hasNextSound())
        {
            mPlayer.playNext(true);
            return;
        } else
        {
            alert("\u4EB2\uFF0C\u6CA1\u6709\u4E0B\u4E00\u9996\u4E86", null);
            return;
        }
    }

    private void doActionPlayOrPause()
    {
        if (mPlayer == null)
        {
            return;
        }
        switch (mPlayer.getMediaPlayerState())
        {
        case 1: // '\001'
        default:
            return;

        case 0: // '\0'
        case 6: // '\006'
            mPlayer.doPlay();
            return;

        case 3: // '\003'
        case 5: // '\005'
        case 7: // '\007'
            mPlayer.start();
            return;

        case 4: // '\004'
            mPlayer.pause();
            return;

        case 2: // '\002'
        case 8: // '\b'
            mPlayer.restart();
            return;
        }
    }

    private void doActionPlayPre()
    {
        if (mPlayer == null || mPlayList.getPlaylist().getData() == null)
        {
            return;
        }
        if (hasPreSound())
        {
            mPlayer.playPrev(true);
            return;
        } else
        {
            alert("\u4EB2\uFF0C\u6CA1\u6709\u4E0A\u4E00\u9996\u4E86", null);
            return;
        }
    }

    private AlbumMenu getAlbumMenu(int i, int j)
    {
        if (j == mLocalAlbumChoiceSet.getRequestId())
        {
            return getLocalAlbumMenu(i);
        }
        if (j == mCollectAlbumChoiceSet.getRequestId())
        {
            return getCollectAlbumMenu(i);
        }
        if (j == mHistoryAlbumChoiceSet.getRequestId())
        {
            return getHistoryAlbumMenu(i);
        }
        if (j == mHotAlbumChoiceSet.getRequestId())
        {
            return getHotAlbumMenu(i);
        } else
        {
            return null;
        }
    }

    private AlbumMenu getAlbumMenuByMenuId(int i)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("Choose Album menuId ").append(i).toString());
        for (Iterator iterator = mCollectedAlbums.iterator(); iterator.hasNext();)
        {
            AlbumMenu albummenu = (AlbumMenu)iterator.next();
            if (albummenu.getMenuId() == i)
            {
                return albummenu;
            }
        }

        for (Iterator iterator1 = mHotAlbums.iterator(); iterator1.hasNext();)
        {
            AlbumMenu albummenu1 = (AlbumMenu)iterator1.next();
            if (albummenu1.getMenuId() == i)
            {
                return albummenu1;
            }
        }

        for (Iterator iterator2 = mHistoryAlbums.iterator(); iterator2.hasNext();)
        {
            AlbumMenu albummenu2 = (AlbumMenu)iterator2.next();
            if (albummenu2.getMenuId() == i)
            {
                return albummenu2;
            }
        }

        for (Iterator iterator3 = mLocalAlbums.iterator(); iterator3.hasNext();)
        {
            AlbumMenu albummenu3 = (AlbumMenu)iterator3.next();
            if (albummenu3.getMenuId() == i)
            {
                return albummenu3;
            }
        }

        return null;
    }

    private AlbumMenu getCollectAlbumMenu(int i)
    {
        for (Iterator iterator = mCollectedAlbums.iterator(); iterator.hasNext();)
        {
            AlbumMenu albummenu = (AlbumMenu)iterator.next();
            if (albummenu.getMenuId() == i)
            {
                return albummenu;
            }
        }

        return null;
    }

    private String getCoverUrl(SoundInfo soundinfo)
    {
        if (!TextUtils.isEmpty(soundinfo.albumCoverPath))
        {
            return soundinfo.albumCoverPath;
        }
        if (!TextUtils.isEmpty(soundinfo.coverLarge))
        {
            return soundinfo.coverLarge;
        }
        if (!TextUtils.isEmpty(soundinfo.coverSmall))
        {
            return soundinfo.coverSmall;
        } else
        {
            return null;
        }
    }

    private AlbumMenu getHistoryAlbumMenu(int i)
    {
        for (Iterator iterator = mHistoryAlbums.iterator(); iterator.hasNext();)
        {
            AlbumMenu albummenu = (AlbumMenu)iterator.next();
            if (albummenu.getMenuId() == i)
            {
                return albummenu;
            }
        }

        return null;
    }

    private AlbumMenu getHotAlbumMenu(int i)
    {
        for (Iterator iterator = mHotAlbums.iterator(); iterator.hasNext();)
        {
            AlbumMenu albummenu = (AlbumMenu)iterator.next();
            if (albummenu.getMenuId() == i)
            {
                return albummenu;
            }
        }

        return null;
    }

    public static AppLinkService getInstance()
    {
        return instance;
    }

    private AlbumMenu getLocalAlbumByAlbumId(long l)
    {
        for (Iterator iterator = mLocalAlbums.iterator(); iterator.hasNext();)
        {
            AlbumMenu albummenu = (AlbumMenu)iterator.next();
            if (albummenu.getAlbumId() == l)
            {
                return albummenu;
            }
        }

        return null;
    }

    private AlbumMenu getLocalAlbumMenu(int i)
    {
        for (Iterator iterator = mLocalAlbums.iterator(); iterator.hasNext();)
        {
            AlbumMenu albummenu = (AlbumMenu)iterator.next();
            if (albummenu.getMenuId() == i)
            {
                return albummenu;
            }
        }

        return null;
    }

    private String getMediaTrackString()
    {
        if (mPlayer == null || mPlayList == null)
        {
            return "";
        }
        if (mBuffering)
        {
            return "\u7F13\u51B2\u4E2D";
        }
        if (mPlayer.isPlaying())
        {
            return (new StringBuilder()).append(mPlayList.curIndex + 1).append("/").append(mPlayList.getSize()).toString();
        } else
        {
            return "\u6682\u505C\u4E2D";
        }
    }

    private String getSubfixName(String s)
    {
        return s.substring(s.lastIndexOf("."), s.length());
    }

    private boolean hasNextSound()
    {
        int i = mPlayList.curIndex;
        return mPlayList.getSize() > 1 && i + 1 < mPlayList.getSize();
    }

    private boolean hasPreSound()
    {
        int i = mPlayList.curIndex;
        return mPlayList.getSize() > 1 && i - 1 >= 0;
    }

    private void registeListener()
    {
        boolean flag = true;
        if (!mRegisted)
        {
            mRegisted = true;
            StringBuilder stringbuilder = (new StringBuilder()).append("registePlayerListener ");
            if (mPlayer == null)
            {
                flag = false;
            }
            Logger.e("AppLinkService", stringbuilder.append(flag).toString());
            if (mPlayer != null)
            {
                mPlayer.setOnPlayerStatusUpdateListener(mStatusListener);
                mPlayer.setOnPlayServiceUpdateListener(mServiceListener);
            }
            if (mDownloadHander != null)
            {
                mDownloadHander.addDownloadListeners(mDownloadListener);
                return;
            }
        }
    }

    private void releaseCollectMenu()
    {
        if (checkProxyStatus())
        {
            Logger.e("AppLinkService", "releaseCollectMenu");
            if (mCollectAlbumChoiceSet.getLastChoiceId() > 0)
            {
                try
                {
                    mProxy.deleteInteractionChoiceSet(Integer.valueOf(mCollectAlbumChoiceSet.getLastChoiceId()), Integer.valueOf(IdGenerator.next()));
                    return;
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
                return;
            }
        }
    }

    private void releaseHistoryMenu()
    {
        if (checkProxyStatus())
        {
            Logger.e("AppLinkService", "releaseHistoryMenu");
            if (mHistoryAlbumChoiceSet.getLastChoiceId() > 0)
            {
                try
                {
                    mProxy.deleteInteractionChoiceSet(Integer.valueOf(mHistoryAlbumChoiceSet.getLastChoiceId()), Integer.valueOf(IdGenerator.next()));
                    return;
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
                return;
            }
        }
    }

    private void releaseHotAlbumMenu()
    {
        if (checkProxyStatus())
        {
            Logger.e("AppLinkService", "releaseHotAlbumMenu");
            if (mHotAlbumChoiceSet.getLastChoiceId() > 0)
            {
                try
                {
                    mProxy.deleteInteractionChoiceSet(Integer.valueOf(mHotAlbumChoiceSet.getLastChoiceId()), Integer.valueOf(IdGenerator.next()));
                    return;
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
                return;
            }
        }
    }

    private void releaseLocalMenu()
    {
        if (checkProxyStatus())
        {
            Logger.e("AppLinkService", "releaseLocalMenu");
            if (mLocalAlbumChoiceSet.getLastChoiceId() > 0)
            {
                try
                {
                    mProxy.deleteInteractionChoiceSet(Integer.valueOf(mLocalAlbumChoiceSet.getLastChoiceId()), Integer.valueOf(IdGenerator.next()));
                    return;
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
                return;
            }
        }
    }

    private void releaseMenu()
    {
        if (!checkProxyStatus())
        {
            return;
        } else
        {
            releaseLocalMenu();
            releaseCollectMenu();
            releaseHistoryMenu();
            releaseHotAlbumMenu();
            return;
        }
    }

    private void resetResource()
    {
        Logger.e("AppLinkService", "xxxx resetResource");
        mLastCoverUrl = null;
        mCommonSoftbutton.clear();
        IdGenerator.clear();
    }

    private void saveBitmap(String s, Bitmap bitmap)
    {
        bitmap = new File((new StringBuilder()).append(Environment.getExternalStorageDirectory()).append(File.separator).append("ATest1").append(File.separator).toString());
        if (bitmap.isFile())
        {
            bitmap.delete();
        }
        if (!bitmap.exists())
        {
            bitmap.mkdirs();
        }
        s = new File(bitmap, (new StringBuilder()).append(ToolUtil.md516(s)).append(getSubfixName(s)).toString());
        try
        {
            new FileOutputStream(s);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
    }

    private void setAppIcon()
    {
        if (!checkProxyStatus())
        {
            return;
        }
        try
        {
            mProxy.setAppIcon(mAppIconName, getResources().getDrawable(mAppResId));
            return;
        }
        catch (SyncException syncexception)
        {
            syncexception.printStackTrace();
        }
        Logger.e("AppLinkService", "setAppIcon fail");
    }

    private void showImg(String s, Bitmap bitmap, FileType filetype)
    {
        if (!checkProxyStatus())
        {
            return;
        }
        try
        {
            mProxy.showImg(s, bitmap, filetype);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
    }

    private void showImg(String s, Drawable drawable, FileType filetype)
    {
        if (!checkProxyStatus())
        {
            return;
        }
        try
        {
            mProxy.showImg(s, drawable, filetype);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
    }

    private void showImg(String s, String s1)
    {
        if (!checkProxyStatus())
        {
            return;
        }
        try
        {
            mProxy.showImg(s, s1);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
    }

    private void showLockScreen()
    {
        MainTabActivity2 maintabactivity2 = MainTabActivity2.mainTabActivity;
        if (maintabactivity2 != null)
        {
            maintabactivity2.showLockScreen();
        }
    }

    private void subscribeVoiceCommand()
    {
        if (!checkProxyStatus())
        {
            return;
        }
        try
        {
            mProxy.addCommand(Integer.valueOf(mBtnId.btn_local), new Vector(Arrays.asList(new String[] {
                getResources().getString(0x7f09021f)
            })), Integer.valueOf(IdGenerator.next()));
            mProxy.addCommand(Integer.valueOf(mBtnId.btn_collect), new Vector(Arrays.asList(new String[] {
                getResources().getString(0x7f090220)
            })), Integer.valueOf(IdGenerator.next()));
            mProxy.addCommand(Integer.valueOf(mBtnId.btn_history), new Vector(Arrays.asList(new String[] {
                getResources().getString(0x7f090221)
            })), Integer.valueOf(IdGenerator.next()));
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void unregisteListener()
    {
        mRegisted = false;
        if (mPlayer != null)
        {
            mPlayer.removeOnPlayerUpdateListener(mStatusListener);
            mPlayer.removeOnPlayServiceUpdateListener(mServiceListener);
        }
        if (mDownloadHander != null)
        {
            mDownloadHander.removeDownloadListeners(mDownloadListener);
        }
    }

    private void updateCollectAlbum()
    {
        while (mLoadingCollect || mLoadCollectedTask != null && mLoadCollectedTask.getStatus() != android.os.AsyncTask.Status.FINISHED) 
        {
            return;
        }
        mLoadCollectedTask = new LoadCollectedTask(null);
        mLoadCollectedTask.myexec(new Void[0]);
    }

    private void updateCommonSoftButton()
    {
        Logger.e("AppLinkService", "xxxx updateCommonSoftButton");
        mCommonSoftbutton.clear();
        mBtnId.btn_local = IdGenerator.next();
        mBtnLocal = new SoftButton();
        mBtnLocal.setSoftButtonID(Integer.valueOf(mBtnId.btn_local));
        mBtnLocal.setText(getResources().getString(0x7f09021f));
        mBtnLocal.setType(SoftButtonType.SBT_TEXT);
        mBtnLocal.setIsHighlighted(Boolean.valueOf(false));
        mBtnLocal.setSystemAction(SystemAction.DEFAULT_ACTION);
        mCommonSoftbutton.add(mBtnLocal);
        mBtnId.btn_collect = IdGenerator.next();
        mBtnCollect = new SoftButton();
        mBtnCollect.setSoftButtonID(Integer.valueOf(mBtnId.btn_collect));
        mBtnCollect.setText(getResources().getString(0x7f090220));
        mBtnCollect.setType(SoftButtonType.SBT_TEXT);
        mBtnCollect.setIsHighlighted(Boolean.valueOf(false));
        mBtnCollect.setSystemAction(SystemAction.DEFAULT_ACTION);
        mCommonSoftbutton.add(mBtnCollect);
        mBtnId.btn_history = IdGenerator.next();
        mBtnHistory = new SoftButton();
        mBtnHistory.setSoftButtonID(Integer.valueOf(mBtnId.btn_history));
        mBtnHistory.setText(getResources().getString(0x7f090221));
        mBtnHistory.setType(SoftButtonType.SBT_TEXT);
        mBtnHistory.setIsHighlighted(Boolean.valueOf(false));
        mBtnHistory.setSystemAction(SystemAction.DEFAULT_ACTION);
        mCommonSoftbutton.add(mBtnHistory);
    }

    private void updateHistoryAlbum()
    {
        while (mLoadingHistory || mLoadHistoryTask != null && mLoadHistoryTask.getStatus() != android.os.AsyncTask.Status.FINISHED) 
        {
            return;
        }
        mLoadHistoryTask = new LoadHistoryTask(null);
        mLoadHistoryTask.myexec(new Void[0]);
    }

    private void updateHotAlbum()
    {
        while (mLoadingHotAlbum || mLoadHotAlbumTask != null && mLoadHotAlbumTask.getStatus() != android.os.AsyncTask.Status.FINISHED) 
        {
            return;
        }
        mLoadHotAlbumTask = new LoadHotAlbumTask(null);
        mLoadHotAlbumTask.myexec(new Void[0]);
    }

    private void updateHotSound()
    {
        while (mLoadingHotSound || mLoadHotSoundTask != null && mLoadHotSoundTask.getStatus() != android.os.AsyncTask.Status.FINISHED) 
        {
            return;
        }
        mLoadHotSoundTask = new LoadHotSoundTask(null);
        mLoadHotSoundTask.myexec(new Void[0]);
    }

    private void updateLocalAlbum()
    {
        Logger.e("AppLinkService", "updateLocalAlbum");
        if (mAlbumLoading)
        {
            Logger.e("AppLinkService", "Local Album Already Loading!");
        } else
        if (mLoadAlbumTask == null || mLoadAlbumTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
        {
            mLoadAlbumTask = new LoadLocalAlbumTask(null);
            mLoadAlbumTask.myexec(new Void[0]);
            return;
        }
    }

    private void updateMainScreen(String s, String s1, String s2, int i, UpdateMode updatemode)
    {
        if (!checkProxyStatus())
        {
            return;
        }
        String s3 = s;
        if (TextUtils.isEmpty(s))
        {
            s3 = "\u559C\u9A6C\u62C9\u96C5\u597D\u58F0\u97F3";
        }
        try
        {
            int j = i / 0x36ee80;
            int k = (i % 0x36ee80) / 60000;
            i = (i % 60000) / 1000;
            mProxy.setMediaClockTimer(Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(i), updatemode, Integer.valueOf(IdGenerator.next()));
            mProxy.show(s3, s1, null, null, null, null, s2, null, mCommonSoftbutton, null, null, Integer.valueOf(IdGenerator.next()));
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
    }

    private void updateSoundCover(SoundInfo soundinfo)
    {
        soundinfo = getCoverUrl(soundinfo);
        Logger.e("AppLinkService", (new StringBuilder()).append("updateSoundCover ").append(mLastCoverUrl).append("|").append(soundinfo).toString());
        if (TextUtils.isEmpty(soundinfo))
        {
            showImg(mAppIconName, getResources().getDrawable(mAppResId), FileType.GRAPHIC_PNG);
        } else
        if (TextUtils.isEmpty(mLastCoverUrl) || !soundinfo.equals(mLastCoverUrl))
        {
            showImg(mAppIconName, getResources().getDrawable(mAppResId), FileType.GRAPHIC_PNG);
            mLastCoverUrl = soundinfo;
            soundinfo = new com.ximalaya.ting.android.util.ImageManager2.Options();
            soundinfo.targetWidth = 185;
            soundinfo.targetHeight = ((com.ximalaya.ting.android.util.ImageManager2.Options) (soundinfo)).targetWidth;
            Logger.e("AppLinkService", (new StringBuilder()).append("onSoundChanged mLastCoverUrl:").append(mLastCoverUrl).toString());
            ImageManager2.from(mAppCtx).downloadBitmap(mLastCoverUrl, soundinfo, new _cls7());
            return;
        }
    }

    public boolean checkProxyStatus()
    {
        if (mProxy != null && mProxy.getIsConnected().booleanValue())
        {
            return true;
        } else
        {
            Logger.e("AppLinkService", "checkProxyStatus closed");
            return false;
        }
    }

    public void createInteractionChoiceSet(InteractionMenu interactionmenu)
    {
        if (!checkProxyStatus())
        {
            return;
        }
        try
        {
            interactionmenu.setRequestId(IdGenerator.next());
            mProxy.createInteractionChoiceSet(interactionmenu.getChoiceSet(), Integer.valueOf(interactionmenu.getChoiceSetId()), Integer.valueOf(interactionmenu.getRequestId()));
            Logger.e("AppLinkService", (new StringBuilder()).append("createInteractionChoiceSet,id:").append(interactionmenu.getChoiceSetId()).append(",reqId:").append(interactionmenu.getRequestId()).append(",name:").append(interactionmenu.getChoiceSetName()).append(",chSize:").append(interactionmenu.getChoiceSet().size()).toString());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (InteractionMenu interactionmenu)
        {
            interactionmenu.printStackTrace();
        }
    }

    public void disposeSyncProxy()
    {
        Logger.e("AppLinkService", "disposeSyncProxy");
        if (mProxy != null)
        {
            try
            {
                mProxy.stopPutFileThread();
                mProxy.dispose();
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
            mProxy = null;
            SdlConnectManager.getInstance(mAppCtx).setAppLinkRunning(false);
            clearLockScreen();
            IdGenerator.clear();
        }
    }

    public SyncProxyALM getProxy()
    {
        return mProxy;
    }

    public void onAddCommandResponse(AddCommandResponse addcommandresponse)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("onAddCommandResponse ").append(addcommandresponse.getSuccess()).append(",").append(addcommandresponse.getInfo()).toString());
    }

    public void onAddSubMenuResponse(AddSubMenuResponse addsubmenuresponse)
    {
        Logger.e("AppLinkService", "onAddSubMenuResponse");
    }

    public void onAlertResponse(AlertResponse alertresponse)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("onAlertResponse ").append(alertresponse.getInfo()).toString());
    }

    public IBinder onBind(Intent intent)
    {
        return null;
    }

    public void onChangeRegistrationResponse(ChangeRegistrationResponse changeregistrationresponse)
    {
        Logger.e("AppLinkService", "onChangeRegistrationResponse");
    }

    public void onCreate()
    {
        super.onCreate();
        instance = this;
        mAppCtx = getApplicationContext();
        mAlbumLoader = AlbumLoader.getInstance();
        IdGenerator.init(0, 6000);
        Logger.e("AppLinkService", "---onCreate");
    }

    public void onCreateInteractionChoiceSetResponse(CreateInteractionChoiceSetResponse createinteractionchoicesetresponse)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("onCreateInteractionChoiceSetResponse ").append(createinteractionchoicesetresponse.getInfo()).append(",").append(createinteractionchoicesetresponse.getCorrelationID()).toString());
    }

    public void onDeleteCommandResponse(DeleteCommandResponse deletecommandresponse)
    {
        Logger.e("AppLinkService", "onDeleteCommandResponse");
    }

    public void onDeleteFileResponse(DeleteFileResponse deletefileresponse)
    {
        Logger.e("AppLinkService", "onDeleteFileResponse");
    }

    public void onDeleteInteractionChoiceSetResponse(DeleteInteractionChoiceSetResponse deleteinteractionchoicesetresponse)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("onDeleteInteractionChoiceSetResponse ").append(deleteinteractionchoicesetresponse.getInfo()).toString());
    }

    public void onDeleteSubMenuResponse(DeleteSubMenuResponse deletesubmenuresponse)
    {
        Logger.e("AppLinkService", "onDeleteSubMenuResponse");
    }

    public void onDestroy()
    {
        Logger.e("AppLinkService", "---onDestroy");
        mAlbumLoader.release();
        releaseMenu();
        disposeSyncProxy();
        clearLockScreen();
        unregisteListener();
        instance = null;
        super.onDestroy();
    }

    public void onDiagnosticMessageResponse(DiagnosticMessageResponse diagnosticmessageresponse)
    {
        Logger.e("AppLinkService", "onDiagnosticMessageResponse");
    }

    public void onEndAudioPassThruResponse(EndAudioPassThruResponse endaudiopassthruresponse)
    {
        Logger.e("AppLinkService", "onEndAudioPassThruResponse");
    }

    public void onError(String s, Exception exception)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("onError ").append(s).toString());
    }

    public void onGenericResponse(GenericResponse genericresponse)
    {
        Logger.e("AppLinkService", "onGenericResponse");
    }

    public void onGetDTCsResponse(GetDTCsResponse getdtcsresponse)
    {
        Logger.e("AppLinkService", "onGetDTCsResponse");
    }

    public void onGetVehicleDataResponse(GetVehicleDataResponse getvehicledataresponse)
    {
        Logger.e("AppLinkService", "onGetVehicleDataResponse");
    }

    public void onListFilesResponse(ListFilesResponse listfilesresponse)
    {
        Logger.e("AppLinkService", "onListFilesResponse");
    }

    public void onOnAudioPassThru(OnAudioPassThru onaudiopassthru)
    {
        Logger.e("AppLinkService", "onOnAudioPassThru");
    }

    public void onOnButtonEvent(OnButtonEvent onbuttonevent)
    {
        Logger.e("AppLinkService", "onOnButtonEvent");
    }

    public void onOnButtonPress(OnButtonPress onbuttonpress)
    {
        ButtonName buttonname = onbuttonpress.getButtonName();
        Logger.e("AppLinkService", (new StringBuilder()).append("onOnButtonPress ").append(buttonname).toString());
        if (buttonname.equals(ButtonName.OK))
        {
            mUiHandler.obtainMessage(1).sendToTarget();
        } else
        {
            if (buttonname.equals(ButtonName.SEEKLEFT))
            {
                mUiHandler.obtainMessage(2).sendToTarget();
                return;
            }
            if (buttonname.equals(ButtonName.SEEKRIGHT))
            {
                mUiHandler.obtainMessage(3).sendToTarget();
                return;
            }
            if (buttonname.equals(ButtonName.CUSTOM_BUTTON))
            {
                int i = onbuttonpress.getCustomButtonName().intValue();
                if (i == mBtnLocal.getSoftButtonID().intValue())
                {
                    performInteraction(mLocalAlbumChoiceSet, InteractionMode.MANUAL_ONLY);
                    return;
                }
                if (i == mBtnHistory.getSoftButtonID().intValue())
                {
                    if (System.currentTimeMillis() - mLastLoadHistoryTime > 60000L)
                    {
                        updateHistoryAlbum();
                    }
                    performInteraction(mHistoryAlbumChoiceSet, InteractionMode.MANUAL_ONLY);
                    return;
                }
                if (i == mBtnCollect.getSoftButtonID().intValue())
                {
                    if (!mLoadCollectSuccess)
                    {
                        updateCollectAlbum();
                    }
                    performInteraction(mCollectAlbumChoiceSet, InteractionMode.MANUAL_ONLY);
                    return;
                }
            }
        }
    }

    public void onOnCommand(OnCommand oncommand)
    {
        int i = oncommand.getCmdID().intValue();
        Logger.e("AppLinkService", (new StringBuilder()).append("onOnCommand ").append(i).toString());
        if (i == mBtnId.btn_hot_album)
        {
            if (!mLoadHotAlbumSuccess || System.currentTimeMillis() - mLastLoadHotAlbumTime > 60000L)
            {
                updateHotAlbum();
            }
            performInteraction(mHotAlbumChoiceSet, InteractionMode.MANUAL_ONLY);
        } else
        {
            if (i == mBtnId.btn_hot_sound)
            {
                if (!mLoadHotSoundSuccess || System.currentTimeMillis() - mLastLoadHotSoundTime > 60000L)
                {
                    updateHotSound();
                }
                mUiHandler.obtainMessage(8).sendToTarget();
                return;
            }
            if (i == mBtnId.btn_local)
            {
                performInteraction(mLocalAlbumChoiceSet, InteractionMode.MANUAL_ONLY);
                return;
            }
            if (i == mBtnId.btn_history)
            {
                if (System.currentTimeMillis() - mLastLoadHistoryTime > 60000L)
                {
                    updateHistoryAlbum();
                }
                performInteraction(mHistoryAlbumChoiceSet, InteractionMode.MANUAL_ONLY);
                return;
            }
            if (i == mBtnId.btn_collect)
            {
                if (!mLoadCollectSuccess)
                {
                    updateCollectAlbum();
                }
                performInteraction(mCollectAlbumChoiceSet, InteractionMode.MANUAL_ONLY);
                return;
            }
        }
    }

    public void onOnDriverDistraction(OnDriverDistraction ondriverdistraction)
    {
        Logger.e("AppLinkService", "onOnDriverDistraction");
    }

    public void onOnHMIStatus(OnHMIStatus onhmistatus)
    {
        static class _cls8
        {

            static final int $SwitchMap$com$ford$syncV4$proxy$rpc$enums$AudioStreamingState[];
            static final int $SwitchMap$com$ford$syncV4$proxy$rpc$enums$HMILevel[];
            static final int $SwitchMap$com$ford$syncV4$proxy$rpc$enums$SystemContext[];

            static 
            {
                $SwitchMap$com$ford$syncV4$proxy$rpc$enums$HMILevel = new int[HMILevel.values().length];
                try
                {
                    $SwitchMap$com$ford$syncV4$proxy$rpc$enums$HMILevel[HMILevel.HMI_FULL.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror8) { }
                try
                {
                    $SwitchMap$com$ford$syncV4$proxy$rpc$enums$HMILevel[HMILevel.HMI_LIMITED.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror7) { }
                try
                {
                    $SwitchMap$com$ford$syncV4$proxy$rpc$enums$HMILevel[HMILevel.HMI_BACKGROUND.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror6) { }
                try
                {
                    $SwitchMap$com$ford$syncV4$proxy$rpc$enums$HMILevel[HMILevel.HMI_NONE.ordinal()] = 4;
                }
                catch (NoSuchFieldError nosuchfielderror5) { }
                $SwitchMap$com$ford$syncV4$proxy$rpc$enums$AudioStreamingState = new int[AudioStreamingState.values().length];
                try
                {
                    $SwitchMap$com$ford$syncV4$proxy$rpc$enums$AudioStreamingState[AudioStreamingState.AUDIBLE.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror4) { }
                try
                {
                    $SwitchMap$com$ford$syncV4$proxy$rpc$enums$AudioStreamingState[AudioStreamingState.NOT_AUDIBLE.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror3) { }
                $SwitchMap$com$ford$syncV4$proxy$rpc$enums$SystemContext = new int[SystemContext.values().length];
                try
                {
                    $SwitchMap$com$ford$syncV4$proxy$rpc$enums$SystemContext[SystemContext.SYSCTXT_MAIN.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    $SwitchMap$com$ford$syncV4$proxy$rpc$enums$SystemContext[SystemContext.SYSCTXT_VRSESSION.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ford$syncV4$proxy$rpc$enums$SystemContext[SystemContext.SYSCTXT_MENU.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        _cls8..SwitchMap.com.ford.syncV4.proxy.rpc.enums.SystemContext[onhmistatus.getSystemContext().ordinal()];
        JVM INSTR tableswitch 1 3: default 36
    //                   1 37
    //                   2 37
    //                   3 37;
           goto _L1 _L2 _L2 _L2
_L1:
        return;
_L2:
        _cls8..SwitchMap.com.ford.syncV4.proxy.rpc.enums.AudioStreamingState[onhmistatus.getAudioStreamingState().ordinal()];
        JVM INSTR tableswitch 1 2: default 72
    //                   1 73
    //                   2 484;
           goto _L3 _L4 _L5
_L3:
        return;
_L4:
        mUiHandler.obtainMessage(6).sendToTarget();
_L6:
        switch (_cls8..SwitchMap.com.ford.syncV4.proxy.rpc.enums.HMILevel[onhmistatus.getHmiLevel().ordinal()])
        {
        default:
            return;

        case 1: // '\001'
            Logger.e("AppLinkService", "HMI_FULL");
            if (onhmistatus.getFirstRun().booleanValue())
            {
                Logger.e("AppLinkService", "xxxx HMI_FULL getFirstRun");
                Object obj = getResources().getString(0x7f09021c);
                onhmistatus = SharedPreferencesUtil.getInstance(mAppCtx);
                mEnable3G = onhmistatus.getBoolean("is_download_enabled_in_3g", false);
                onhmistatus.saveBoolean("is_download_enabled_in_3g", true);
                int i = NetworkUtils.getNetType(mAppCtx);
                onhmistatus = ((OnHMIStatus) (obj));
                if (!mEnable3G)
                {
                    onhmistatus = ((OnHMIStatus) (obj));
                    if (i == 0)
                    {
                        onhmistatus = (new StringBuilder()).append(((String) (obj))).append(getResources().getString(0x7f09021e)).toString();
                    }
                }
                showSpeak(onhmistatus);
                if (mPlayer == null)
                {
                    mPlayer = LocalMediaService.getInstance();
                    mPlayList = PlayListControl.getPlayListManager();
                    mDownloadHander = DownloadHandler.getInstance(mAppCtx);
                }
                mHistoryManage = HistoryManage.getInstance(mAppCtx);
                updateCommonSoftButton();
                addCommonCommand();
                subButtons();
                obj = getResources().getString(0x7f09021b);
                String s = getResources().getString(0x7f09021d);
                i = mPlayer.getCurPosition();
                String s1;
                String s2;
                if (mPlayer.isPlaying())
                {
                    onhmistatus = UpdateMode.COUNTUP;
                } else
                {
                    onhmistatus = UpdateMode.PAUSE;
                }
                updateMainScreen(((String) (obj)), s, "", i, onhmistatus);
                subscribeVoiceCommand();
                mCollectPageId = 1;
                mHotSoundPageId = 1;
                mHotAlbumPageId = 1;
                updateHistoryAlbum();
                updateHotAlbum();
                updateHotSound();
                updateLocalAlbum();
                updateCollectAlbum();
            }
            registeListener();
            ToolUtil.startMainAct(mAppCtx);
            obj = mPlayList.getCurSound();
            if (obj != null)
            {
                s = ((SoundInfo) (obj)).albumName;
                s1 = ((SoundInfo) (obj)).title;
                s2 = getMediaTrackString();
                i = mPlayer.getCurPosition();
                if (mPlayer.isPlaying())
                {
                    onhmistatus = UpdateMode.COUNTUP;
                } else
                {
                    onhmistatus = UpdateMode.PAUSE;
                }
                updateMainScreen(s, s1, s2, i, onhmistatus);
                updateSoundCover(((SoundInfo) (obj)));
                return;
            } else
            {
                showImg(mAppIconName, getResources().getDrawable(mAppResId), FileType.GRAPHIC_PNG);
                return;
            }

        case 2: // '\002'
            Logger.e("AppLinkService", "HMI_LIMITED");
            return;

        case 3: // '\003'
            Logger.e("AppLinkService", "HMI_BACKGROUND");
            setAppIcon();
            return;

        case 4: // '\004'
            Logger.e("AppLinkService", "xxxx HMI_NONE");
            clearProxyCache();
            clearLockScreen();
            unregisteListener();
            return;
        }
_L5:
        mUiHandler.obtainMessage(7).sendToTarget();
          goto _L6
    }

    public void onOnHashChange(OnHashChange onhashchange)
    {
        Logger.e("AppLinkService", "onOnHashChange");
    }

    public void onOnKeyboardInput(OnKeyboardInput onkeyboardinput)
    {
        Logger.e("AppLinkService", "onOnKeyboardInput");
    }

    public void onOnLanguageChange(OnLanguageChange onlanguagechange)
    {
        Logger.e("AppLinkService", "onOnLanguageChange");
    }

    public void onOnLockScreenNotification(OnLockScreenStatus onlockscreenstatus)
    {
        Logger.e("AppLinkService", "onOnLockScreenNotification");
        onlockscreenstatus = onlockscreenstatus.getShowLockScreen();
        if (onlockscreenstatus == LockScreenStatus.REQUIRED || onlockscreenstatus == LockScreenStatus.OPTIONAL)
        {
            showLockScreen();
            return;
        } else
        {
            clearLockScreen();
            return;
        }
    }

    public void onOnPermissionsChange(OnPermissionsChange onpermissionschange)
    {
        Logger.e("AppLinkService", "onOnPermissionsChange");
    }

    public void onOnSystemRequest(OnSystemRequest onsystemrequest)
    {
        Logger.e("AppLinkService", "onOnSystemRequest");
    }

    public void onOnTBTClientState(OnTBTClientState ontbtclientstate)
    {
        Logger.e("AppLinkService", "onOnTBTClientState");
    }

    public void onOnTouchEvent(OnTouchEvent ontouchevent)
    {
        Logger.e("AppLinkService", "onOnTouchEvent");
    }

    public void onOnVehicleData(OnVehicleData onvehicledata)
    {
        Logger.e("AppLinkService", "onOnVehicleData");
    }

    public void onPerformAudioPassThruResponse(PerformAudioPassThruResponse performaudiopassthruresponse)
    {
        Logger.e("AppLinkService", "onPerformAudioPassThruResponse");
    }

    public void onPerformInteractionResponse(PerformInteractionResponse performinteractionresponse)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("onPerformInteractionResponse ").append(performinteractionresponse.getInfo()).toString());
        if (performinteractionresponse.getSuccess().booleanValue())
        {
            Message message = mUiHandler.obtainMessage(5);
            message.arg1 = performinteractionresponse.getChoiceID().intValue();
            message.arg2 = performinteractionresponse.getCorrelationID().intValue();
            message.sendToTarget();
        }
        releaseMenu();
    }

    public void onProxyClosed(String s, Exception exception, SyncDisconnectedReason syncdisconnectedreason)
    {
        Logger.e("AppLinkService", "xxxx onProxyClosed");
        clearLockScreen();
        resetResource();
        SdlConnectManager.getInstance(mAppCtx).setAppLinkRunning(false);
        if (((SyncException)exception).getSyncExceptionCause() != SyncExceptionCause.SYNC_PROXY_CYCLED && ((SyncException)exception).getSyncExceptionCause() != SyncExceptionCause.BLUETOOTH_DISABLED)
        {
            Logger.e("AppLinkService", "reset proxy in onproxy closed");
            reset();
            SdlConnectManager.getInstance(mAppCtx).setAppLinkRunning(true);
        }
    }

    public void onPutFileResponse(PutFileResponse putfileresponse)
    {
    }

    public void onReadDIDResponse(ReadDIDResponse readdidresponse)
    {
        Logger.e("AppLinkService", "onReadDIDResponse");
    }

    public void onResetGlobalPropertiesResponse(ResetGlobalPropertiesResponse resetglobalpropertiesresponse)
    {
        Logger.e("AppLinkService", "onResetGlobalPropertiesResponse");
    }

    public void onScrollableMessageResponse(ScrollableMessageResponse scrollablemessageresponse)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("onScrollableMessageResponse ").append(scrollablemessageresponse.getInfo()).toString());
    }

    public void onSelectByVoice(SelectByVoiceResponse selectbyvoiceresponse)
    {
    }

    public void onSetAppIconResponse(SetAppIconResponse setappiconresponse)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("onSetAppIconResponse ").append(setappiconresponse.getSuccess()).append(", ").append(setappiconresponse.getInfo()).toString());
    }

    public void onSetDisplayLayoutResponse(SetDisplayLayoutResponse setdisplaylayoutresponse)
    {
        Logger.e("AppLinkService", "onSetDisplayLayoutResponse");
    }

    public void onSetGlobalPropertiesResponse(SetGlobalPropertiesResponse setglobalpropertiesresponse)
    {
        Logger.e("AppLinkService", "onSetGlobalPropertiesResponse");
    }

    public void onSetMediaClockTimerResponse(SetMediaClockTimerResponse setmediaclocktimerresponse)
    {
        Logger.e("AppLinkService", "onSetMediaClockTimerResponse");
    }

    public void onShowResponse(ShowResponse showresponse)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("---onShowResponse ").append(showresponse.getCorrelationID()).append(", ").append(showresponse.getSuccess()).append(",").append(showresponse.getInfo()).toString());
    }

    public void onSliderResponse(SliderResponse sliderresponse)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("onSliderResponse ").append(sliderresponse.getInfo()).toString());
    }

    public void onSpeakResponse(SpeakResponse speakresponse)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("onSpeakResponse ").append(speakresponse.getInfo()).toString());
    }

    public int onStartCommand(Intent intent, int i, int j)
    {
        Logger.e("AppLinkService", "---onStartCommand");
        mUiHandler.removeCallbacks(mStopServiceRunnable);
        if (intent != null)
        {
            mBtAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBtAdapter != null && mBtAdapter.isEnabled())
            {
                startProxy();
            }
        }
        mUiHandler.removeCallbacks(mCheckConnectionRunnable);
        mUiHandler.postDelayed(mCheckConnectionRunnable, 60000L);
        return 1;
    }

    public void onSubscribeButtonResponse(SubscribeButtonResponse subscribebuttonresponse)
    {
        Logger.e("AppLinkService", (new StringBuilder()).append("onSubscribeButtonResponse ").append(subscribebuttonresponse.getResultCode()).append("|").append(subscribebuttonresponse.getInfo()).append("|").append(subscribebuttonresponse.getCorrelationID()).toString());
    }

    public void onSubscribeVehicleDataResponse(SubscribeVehicleDataResponse subscribevehicledataresponse)
    {
        Logger.e("AppLinkService", "onSubscribeVehicleDataResponse");
    }

    public void onSystemRequestResponse(SystemRequestResponse systemrequestresponse)
    {
        Logger.e("AppLinkService", "onSystemRequestResponse");
    }

    public void onUnsubscribeButtonResponse(UnsubscribeButtonResponse unsubscribebuttonresponse)
    {
        Logger.e("AppLinkService", "onUnsubscribeButtonResponse");
    }

    public void onUnsubscribeVehicleDataResponse(UnsubscribeVehicleDataResponse unsubscribevehicledataresponse)
    {
        Logger.e("AppLinkService", "onUnsubscribeVehicleDataResponse");
    }

    public void performInteraction(int i, String s, String s1, InteractionMode interactionmode, Integer integer)
    {
        if (!checkProxyStatus())
        {
            return;
        }
        try
        {
            mProxy.performInteraction(s, s1, Integer.valueOf(i), null, null, interactionmode, Integer.valueOf(0x186a0), integer);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
    }

    public void performInteraction(InteractionMenu interactionmenu, InteractionMode interactionmode)
    {
        interactionmenu.setRequestId(IdGenerator.next());
        performInteraction(interactionmenu.getChoiceSetId(), interactionmenu.getChoiceSetName(), interactionmenu.getChoiceSetName(), interactionmode, Integer.valueOf(interactionmenu.getRequestId()));
    }

    public void reset()
    {
        Logger.e("AppLinkService", "xxxx reset");
        if (mProxy == null)
        {
            break MISSING_BLOCK_LABEL_44;
        }
        clearProxyCache();
        mProxy.resetProxy();
_L2:
        return;
        Exception exception;
        exception;
        exception.printStackTrace();
        if (mProxy != null) goto _L2; else goto _L1
_L1:
        stopSelf();
        return;
        startProxy();
        return;
    }

    public void showSpeak(String s)
    {
        if (!checkProxyStatus())
        {
            return;
        }
        try
        {
            mProxy.speak(s, Integer.valueOf(IdGenerator.next()));
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
    }

    public void startProxy()
    {
        boolean flag1 = true;
        if (mProxy != null) goto _L2; else goto _L1
_L1:
        Object obj;
        obj = Locale.getDefault().getDisplayLanguage();
        Logger.e("AppLinkService", (new StringBuilder()).append("Language Is ").append(((String) (obj))).toString());
        if (!((String) (obj)).contains("\u4E2D\u6587")) goto _L4; else goto _L3
_L3:
        obj = Language.ZH_CN;
_L5:
        mProxyListener = new ProxyListener(null);
        mProxy = new SyncProxyEx(mProxyListener, "\u559C\u9A6C\u62C9\u96C5", Boolean.valueOf(true), ((Language) (obj)), ((Language) (obj)), "1502221248");
_L6:
        obj = SdlConnectManager.getInstance(mAppCtx);
        Exception exception;
        boolean flag;
        if (mProxy != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        ((SdlConnectManager) (obj)).setAppLinkRunning(flag);
        obj = (new StringBuilder()).append("startProxy Success! ");
        if (mProxy != null)
        {
            flag = flag1;
        } else
        {
            flag = false;
        }
        Logger.e("AppLinkService", ((StringBuilder) (obj)).append(flag).toString());
_L2:
        return;
_L4:
        obj = Language.EN_US;
          goto _L5
        exception;
        exception.printStackTrace();
        if (mProxy == null)
        {
            Logger.e("AppLinkService", "startProxy Failed!");
            stopSelf();
        }
          goto _L6
    }

    public void stopService()
    {
        mUiHandler.removeCallbacks(mStopServiceRunnable);
        mUiHandler.postDelayed(mStopServiceRunnable, 5000L);
    }

    public void subButtons()
    {
        if (!checkProxyStatus())
        {
            return;
        }
        try
        {
            mProxy.subscribeButton(ButtonName.OK, Integer.valueOf(IdGenerator.next()));
            mProxy.subscribeButton(ButtonName.SEEKLEFT, Integer.valueOf(IdGenerator.next()));
            mProxy.subscribeButton(ButtonName.SEEKRIGHT, Integer.valueOf(IdGenerator.next()));
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }


















/*
    static boolean access$2802(AppLinkService applinkservice, boolean flag)
    {
        applinkservice.mAlbumLoading = flag;
        return flag;
    }

*/






/*
    static boolean access$3202(AppLinkService applinkservice, boolean flag)
    {
        applinkservice.mLoadingCollect = flag;
        return flag;
    }

*/


/*
    static boolean access$3302(AppLinkService applinkservice, boolean flag)
    {
        applinkservice.mLoadCollectSuccess = flag;
        return flag;
    }

*/



/*
    static int access$3408(AppLinkService applinkservice)
    {
        int i = applinkservice.mCollectPageId;
        applinkservice.mCollectPageId = i + 1;
        return i;
    }

*/




/*
    static long access$3702(AppLinkService applinkservice, long l)
    {
        applinkservice.mLastLoadCollectTime = l;
        return l;
    }

*/






/*
    static boolean access$4102(AppLinkService applinkservice, boolean flag)
    {
        applinkservice.mLoadingHistory = flag;
        return flag;
    }

*/



/*
    static long access$4302(AppLinkService applinkservice, long l)
    {
        applinkservice.mLastLoadHistoryTime = l;
        return l;
    }

*/




/*
    static boolean access$4602(AppLinkService applinkservice, boolean flag)
    {
        applinkservice.mLoadingHotAlbum = flag;
        return flag;
    }

*/



/*
    static boolean access$4802(AppLinkService applinkservice, boolean flag)
    {
        applinkservice.mLoadHotAlbumSuccess = flag;
        return flag;
    }

*/


/*
    static long access$4902(AppLinkService applinkservice, long l)
    {
        applinkservice.mLastLoadHotAlbumTime = l;
        return l;
    }

*/





/*
    static boolean access$5202(AppLinkService applinkservice, boolean flag)
    {
        applinkservice.mLoadingHotSound = flag;
        return flag;
    }

*/



/*
    static boolean access$5402(AppLinkService applinkservice, boolean flag)
    {
        applinkservice.mLoadHotSoundSuccess = flag;
        return flag;
    }

*/


/*
    static long access$5502(AppLinkService applinkservice, long l)
    {
        applinkservice.mLastLoadHotSoundTime = l;
        return l;
    }

*/





/*
    static boolean access$902(AppLinkService applinkservice, boolean flag)
    {
        applinkservice.mBuffering = flag;
        return flag;
    }

*/

    private class _cls1 extends Handler
    {

        final AppLinkService this$0;

        public void handleMessage(Message message)
        {
            switch (message.what)
            {
            case 4: // '\004'
            default:
                return;

            case 1: // '\001'
                doActionPlayOrPause();
                return;

            case 2: // '\002'
                doActionPlayPre();
                return;

            case 3: // '\003'
                doActionPlayNext();
                return;

            case 5: // '\005'
                doActionChooseAlbum(message);
                return;

            case 6: // '\006'
                doActionAudible();
                return;

            case 7: // '\007'
                doActionNotAudible();
                return;

            case 8: // '\b'
                break;
            }
            if (mHotSounds == null || mHotSounds.size() == 0)
            {
                message = (new StringBuilder()).append(getResources().getString(0x7f090222)).append("\u6B63\u5728\u52A0\u8F7D\u4E2D\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\uFF01").toString();
                showSpeak(message);
                return;
            } else
            {
                PlayTools.gotoPlay(0, mHotSounds, 0, mAppCtx, false, null);
                return;
            }
        }

        _cls1()
        {
            this$0 = AppLinkService.this;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.transaction.download.DownloadHandler.DownloadSoundsListener
    {

        final AppLinkService this$0;

        public void onTaskComplete()
        {
        }

        public void onTaskDelete()
        {
        }

        public void updateActionInfo()
        {
        }

        public void updateDownloadInfo(int i)
        {
            Logger.e("AppLinkService", (new StringBuilder()).append("onDownloadChange ").append(i).toString());
            updateLocalAlbum();
        }

        _cls2()
        {
            this$0 = AppLinkService.this;
            super();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener
    {

        final AppLinkService this$0;

        public void onBufferUpdated(int i)
        {
        }

        public void onLogoPlayFinished()
        {
        }

        public void onPlayCompleted()
        {
            mBuffering = false;
            SoundInfo soundinfo = mPlayList.getCurSound();
            if (soundinfo != null)
            {
                updateMainScreen(soundinfo.albumName, soundinfo.title, getMediaTrackString(), 0, UpdateMode.CLEAR);
            }
        }

        public void onPlayPaused()
        {
            mBuffering = false;
            SoundInfo soundinfo = mPlayList.getCurSound();
            if (soundinfo != null)
            {
                updateMainScreen(soundinfo.albumName, soundinfo.title, getMediaTrackString(), mPlayer.getCurPosition(), UpdateMode.PAUSE);
            }
        }

        public void onPlayProgressUpdate(int i, int j)
        {
        }

        public void onPlayStarted()
        {
            Logger.e("AppLinkService", "onPlayStarted");
            mBuffering = false;
            SoundInfo soundinfo = mPlayList.getCurSound();
            if (soundinfo != null)
            {
                updateMainScreen(soundinfo.albumName, soundinfo.title, getMediaTrackString(), mPlayer.getCurPosition(), UpdateMode.COUNTUP);
            }
        }

        public void onPlayerBuffering(boolean flag)
        {
            Logger.e("AppLinkService", (new StringBuilder()).append("onPlayerBuffering ").append(flag).toString());
            mBuffering = flag;
        }

        public void onSoundPrepared(int i)
        {
            mBuffering = false;
        }

        public void onStartPlayLogo()
        {
        }

        _cls3()
        {
            this$0 = AppLinkService.this;
            super();
        }
    }


    private class _cls4
        implements com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
    {

        final AppLinkService this$0;

        public void onPlayCanceled()
        {
        }

        public void onSoundChanged(int i)
        {
            Logger.e("AppLinkService", (new StringBuilder()).append("onSoundChanged ").append(i).toString());
            mBuffering = true;
            SoundInfo soundinfo = mPlayList.getCurSound();
            if (soundinfo != null)
            {
                updateSoundCover(soundinfo);
                updateMainScreen(soundinfo.albumName, soundinfo.title, getMediaTrackString(), 0, UpdateMode.CLEAR);
            }
        }

        public void onSoundInfoChanged(int i, SoundInfo soundinfo)
        {
        }

        _cls4()
        {
            this$0 = AppLinkService.this;
            super();
        }
    }


    private class _cls5
        implements Runnable
    {

        final AppLinkService this$0;

        public void run()
        {
            Boolean boolean2 = Boolean.valueOf(true);
            Boolean boolean1 = boolean2;
            if (mProxy != null)
            {
                boolean1 = boolean2;
                if (mProxy.getIsConnected().booleanValue())
                {
                    boolean1 = Boolean.valueOf(false);
                }
            }
            if (boolean1.booleanValue())
            {
                mUiHandler.removeCallbacks(mCheckConnectionRunnable);
                mUiHandler.removeCallbacks(mStopServiceRunnable);
                stopSelf();
            }
        }

        _cls5()
        {
            this$0 = AppLinkService.this;
            super();
        }
    }


    private class _cls6
        implements Runnable
    {

        final AppLinkService this$0;

        public void run()
        {
            if (mProxy == null || !mProxy.getIsConnected().booleanValue())
            {
                mUiHandler.removeCallbacks(mCheckConnectionRunnable);
                mUiHandler.removeCallbacks(mStopServiceRunnable);
                stopSelf();
            }
        }

        _cls6()
        {
            this$0 = AppLinkService.this;
            super();
        }
    }


    private class _cls7
        implements com.ximalaya.ting.android.util.ImageManager2.DisplayCallback
    {

        final AppLinkService this$0;

        public void onCompleteDisplay(String s, Bitmap bitmap)
        {
            if (checkProxyStatus() && bitmap != null)
            {
                Logger.e("AppLinkService", (new StringBuilder()).append("onCompleteDisplay [").append(bitmap.getWidth()).append(",").append(bitmap.getHeight()).append("]").toString());
                if (s.equals(mLastCoverUrl))
                {
                    showImg(ToolUtil.md516(mLastCoverUrl), bitmap, FileType.GRAPHIC_JPEG);
                    return;
                }
            }
        }

        _cls7()
        {
            this$0 = AppLinkService.this;
            super();
        }
    }

}
