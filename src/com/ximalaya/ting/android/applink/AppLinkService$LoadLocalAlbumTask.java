// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.content.res.Resources;
import android.text.TextUtils;
import com.ford.syncV4.proxy.rpc.Choice;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            AppLinkService, AlbumMenu, IdGenerator, InteractionMenu

private class <init> extends MyAsyncTask
{

    final AppLinkService this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient Void doInBackground(Void avoid[])
    {
        avoid = DownloadHandler.getInstance(AppLinkService.access$700(AppLinkService.this)).getSortedFinishedDownloadList();
        if (avoid == null)
        {
            return null;
        }
        Vector vector = AppLinkService.access$2900(AppLinkService.this);
        vector;
        JVM INSTR monitorenter ;
        Iterator iterator;
        AppLinkService.access$2900(AppLinkService.this).clear();
        iterator = avoid.iterator();
_L7:
        if (!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        AlbumMenu albummenu;
        DownloadTask downloadtask;
        downloadtask = (DownloadTask)iterator.next();
        albummenu = AppLinkService.access$3000(AppLinkService.this, downloadtask.albumId);
        avoid = albummenu;
        if (albummenu != null) goto _L4; else goto _L3
_L3:
        avoid = new AlbumMenu();
        if (downloadtask.albumId <= 0L || TextUtils.isEmpty(downloadtask.albumName))
        {
            break MISSING_BLOCK_LABEL_186;
        }
        avoid.setMenuName(downloadtask.albumName);
_L5:
        avoid.setAlbumId(downloadtask.albumId);
        avoid.setSoundList(new Vector());
        avoid.setMenuId(IdGenerator.next());
        AppLinkService.access$2900(AppLinkService.this).add(avoid);
_L4:
        avoid.getSoundList().add(downloadtask);
        continue; /* Loop/switch isn't completed */
        avoid;
        vector;
        JVM INSTR monitorexit ;
        throw avoid;
        avoid.setMenuName("\u672A\u547D\u540D\u4E13\u8F91");
          goto _L5
_L2:
        if (AppLinkService.access$2900(AppLinkService.this).size() == 0)
        {
            avoid = new AlbumMenu();
            avoid.setAlbumId(-1L);
            avoid.setMenuName("\u672C\u5730\u65E0\u4E13\u8F91");
            avoid.setSoundList(new Vector(0));
            avoid.setMenuId(IdGenerator.next());
            AppLinkService.access$2900(AppLinkService.this).add(avoid);
        }
        vector;
        JVM INSTR monitorexit ;
        return null;
        if (true) goto _L7; else goto _L6
_L6:
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Void)obj);
    }

    protected void onPostExecute(Void void1)
    {
        super.onPostExecute(void1);
        AppLinkService.access$2802(AppLinkService.this, false);
        if (!checkProxyStatus())
        {
            return;
        }
        void1 = new Vector();
        int j = AppLinkService.access$2900(AppLinkService.this).size();
        for (int i = 0; i < j; i++)
        {
            AlbumMenu albummenu = (AlbumMenu)AppLinkService.access$2900(AppLinkService.this).get(i);
            Choice choice = new Choice();
            choice.setChoiceID(Integer.valueOf(albummenu.getMenuId()));
            choice.setMenuName(albummenu.getMenuName());
            choice.setVrCommands(new Vector(Arrays.asList(new String[] {
                albummenu.getMenuName()
            })));
            void1.add(choice);
        }

        AppLinkService.access$3100(AppLinkService.this).setChoiceSet(void1);
        AppLinkService.access$3100(AppLinkService.this).setChoiceSetId(IdGenerator.next());
        AppLinkService.access$3100(AppLinkService.this).setChoiceSetName(getResources().getString(0x7f090224));
        createInteractionChoiceSet(AppLinkService.access$3100(AppLinkService.this));
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        AppLinkService.access$2802(AppLinkService.this, true);
    }

    private I()
    {
        this$0 = AppLinkService.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
