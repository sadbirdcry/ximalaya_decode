// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.text.TextUtils;
import com.ford.syncV4.proxy.rpc.Choice;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.HistoryManage;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            AppLinkService, AlbumMenu, IdGenerator, AlbumLoader, 
//            InteractionMenu

private class <init> extends MyAsyncTask
{

    final AppLinkService this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        avoid = AppLinkService.access$4200(AppLinkService.this).getSoundInfoList();
        Object obj = new ArrayList();
        if (avoid != null)
        {
            ((List) (obj)).addAll(avoid);
        }
        avoid = new ArrayList();
        if (obj != null && ((List) (obj)).size() > 0)
        {
            obj = ((List) (obj)).iterator();
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break;
                }
                SoundInfo soundinfo = (SoundInfo)((Iterator) (obj)).next();
                if (soundinfo.albumId > 0L && !TextUtils.isEmpty(soundinfo.albumName))
                {
                    AlbumMenu albummenu1 = new AlbumMenu();
                    albummenu1.setAlbumId(soundinfo.albumId);
                    if (!avoid.contains(albummenu1))
                    {
                        Logger.e("AppLinkService", (new StringBuilder()).append("AlbumName ").append(soundinfo.albumName).toString());
                        albummenu1.setMenuName(soundinfo.albumName);
                        albummenu1.setMenuId(IdGenerator.next());
                        avoid.add(albummenu1);
                        AppLinkService.access$3900(AppLinkService.this).preload(albummenu1, albummenu1.getPageId() + 1);
                    }
                }
            } while (true);
        }
        if (avoid.size() == 0)
        {
            AlbumMenu albummenu = new AlbumMenu();
            albummenu.setMenuName("\u65E0\u64AD\u653E\u5386\u53F2");
            albummenu.setAlbumId(-1L);
            albummenu.setMenuId(IdGenerator.next());
            avoid.add(albummenu);
        }
        return avoid;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(List list)
    {
        super.onPostExecute(list);
        AppLinkService.access$4102(AppLinkService.this, false);
        if (!checkProxyStatus())
        {
            return;
        }
        AppLinkService.access$4302(AppLinkService.this, System.currentTimeMillis());
        AppLinkService.access$4400(AppLinkService.this).clear();
        AppLinkService.access$4400(AppLinkService.this).addAll(list);
        list = new Vector();
        Choice choice;
        for (Iterator iterator = AppLinkService.access$4400(AppLinkService.this).iterator(); iterator.hasNext(); list.add(choice))
        {
            AlbumMenu albummenu = (AlbumMenu)iterator.next();
            choice = new Choice();
            choice.setChoiceID(Integer.valueOf(albummenu.getMenuId()));
            choice.setMenuName(albummenu.getMenuName());
            choice.setVrCommands(new Vector(Arrays.asList(new String[] {
                albummenu.getMenuName()
            })));
        }

        AppLinkService.access$4500(AppLinkService.this).setChoiceSet(list);
        AppLinkService.access$4500(AppLinkService.this).setChoiceSetId(IdGenerator.next());
        AppLinkService.access$4500(AppLinkService.this).setChoiceSetName(getString(0x7f090226));
        createInteractionChoiceSet(AppLinkService.access$4500(AppLinkService.this));
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        AppLinkService.access$4102(AppLinkService.this, true);
    }

    private ()
    {
        this$0 = AppLinkService.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
