// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;


// Referenced classes of package com.ximalaya.ting.android.applink:
//            AlbumLoader, AlbumMenu

private class pageId
{

    public AlbumMenu albumMenu;
    public long id;
    public int pageId;
    final AlbumLoader this$0;

    private AlbumLoader getOuterType()
    {
        return AlbumLoader.this;
    }

    public boolean equals(Object obj)
    {
        if (this != obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (getClass() != obj.getClass())
            {
                return false;
            }
            obj = (this._cls0)obj;
            if (!getOuterType().equals(((getOuterType) (obj)).getOuterType()))
            {
                return false;
            }
            if (id != ((id) (obj)).id)
            {
                return false;
            }
            if (pageId != ((pageId) (obj)).pageId)
            {
                return false;
            }
        }
        return true;
    }

    public int hashCode()
    {
        return ((getOuterType().hashCode() + 31) * 31 + (int)(id ^ id >>> 32)) * 31 + pageId;
    }

    public (AlbumMenu albummenu, int i)
    {
        this$0 = AlbumLoader.this;
        super();
        albumMenu = albummenu;
        id = albummenu.getAlbumId();
        pageId = i;
    }
}
