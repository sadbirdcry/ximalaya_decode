// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.content.res.Resources;
import com.smartdevicelink.proxy.rpc.Choice;
import com.smartdevicelink.proxy.rpc.Image;
import com.smartdevicelink.proxy.rpc.enums.ImageType;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            SdlService, AlbumMenu, InteractionMenu

private class <init> extends MyAsyncTask
{

    final SdlService this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient Void doInBackground(Void avoid[])
    {
        avoid = DownloadHandler.getInstance(SdlService.access$1400(SdlService.this)).getSortedFinishedDownloadList();
        Vector vector = SdlService.access$4800(SdlService.this);
        vector;
        JVM INSTR monitorenter ;
        SdlService.access$4800(SdlService.this).clear();
        if (avoid == null) goto _L2; else goto _L1
_L1:
        Iterator iterator = avoid.iterator();
_L8:
        if (!iterator.hasNext()) goto _L2; else goto _L3
_L3:
        AlbumMenu albummenu;
        DownloadTask downloadtask;
        downloadtask = (DownloadTask)iterator.next();
        albummenu = SdlService.access$4900(SdlService.this, downloadtask.albumId);
        avoid = albummenu;
        if (albummenu != null) goto _L5; else goto _L4
_L4:
        avoid = new AlbumMenu();
        if (downloadtask.albumId <= 0L)
        {
            break MISSING_BLOCK_LABEL_177;
        }
        avoid.setMenuName(downloadtask.albumName);
_L6:
        avoid.setAlbumId(downloadtask.albumId);
        avoid.setSoundList(new Vector());
        avoid.setMenuId(SdlService.access$2808(SdlService.this));
        SdlService.access$4800(SdlService.this).add(avoid);
_L5:
        avoid.getSoundList().add(downloadtask);
        continue; /* Loop/switch isn't completed */
        avoid;
        vector;
        JVM INSTR monitorexit ;
        throw avoid;
        avoid.setMenuName("\u672A\u547D\u540D\u4E13\u8F91");
          goto _L6
_L2:
        if (SdlService.access$4800(SdlService.this).size() == 0)
        {
            avoid = new AlbumMenu();
            avoid.setAlbumId(-1L);
            avoid.setMenuName("\u672C\u5730\u65E0\u4E13\u8F91");
            avoid.setSoundList(new Vector(0));
            avoid.setMenuId(SdlService.access$2808(SdlService.this));
            SdlService.access$4800(SdlService.this).add(avoid);
            SdlService.access$5000(SdlService.this).add(avoid);
        }
        vector;
        JVM INSTR monitorexit ;
        return null;
        if (true) goto _L8; else goto _L7
_L7:
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Void)obj);
    }

    protected void onPostExecute(Void void1)
    {
        super.onPostExecute(void1);
        SdlService.access$4702(SdlService.this, false);
        if (!checkSdlStatus())
        {
            return;
        }
        void1 = new Vector();
        int j = Math.min(100, SdlService.access$4800(SdlService.this).size());
        for (int i = 0; i < j; i++)
        {
            Object obj = (AlbumMenu)SdlService.access$4800(SdlService.this).get(i);
            Choice choice = new Choice();
            choice.setChoiceID(Integer.valueOf(((AlbumMenu) (obj)).getMenuId()));
            choice.setMenuName(SdlService.access$5100(SdlService.this, ((AlbumMenu) (obj)).getMenuName()));
            choice.setVrCommands(new Vector(Arrays.asList(new String[] {
                SdlService.access$5100(SdlService.this, ((AlbumMenu) (obj)).getMenuName())
            })));
            obj = new Image();
            ((Image) (obj)).setImageType(ImageType.DYNAMIC);
            ((Image) (obj)).setValue(SdlService.access$5200(SdlService.this));
            choice.setImage(((Image) (obj)));
            void1.add(choice);
        }

        SdlService.access$3600(SdlService.this).setChoiceSet(void1);
        SdlService.access$3600(SdlService.this).setChoiceSetId(SdlService.access$2808(SdlService.this));
        SdlService.access$3600(SdlService.this).setChoiceSetName(getResources().getString(0x7f09022f));
        SdlService.access$3700(SdlService.this, SdlService.access$3600(SdlService.this));
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        SdlService.access$4702(SdlService.this, true);
    }

    private ()
    {
        this$0 = SdlService.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
