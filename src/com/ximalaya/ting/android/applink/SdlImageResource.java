// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.mxnavi.sdl.utils.AndroidUtils;
import com.smartdevicelink.proxy.rpc.Image;
import com.smartdevicelink.proxy.rpc.enums.FileType;
import com.smartdevicelink.proxy.rpc.enums.ImageType;

public final class SdlImageResource extends Enum
{

    private static final SdlImageResource $VALUES[];
    public static final SdlImageResource APP_ICON;
    private FileType fileType;
    private String friendlyName;
    private int imageID;

    private SdlImageResource(String s, int i, String s1, FileType filetype, int j)
    {
        super(s, i);
        friendlyName = s1;
        fileType = filetype;
        imageID = j;
    }

    public static Bitmap getBitmap(Resources resources, SdlImageResource sdlimageresource)
    {
        return sdlimageresource.getBitmap(resources);
    }

    public static Bitmap getBitmapByImageID(Resources resources, int i)
    {
        return getBitmap(resources, getImageResourceByImageID(i));
    }

    public static android.graphics.Bitmap.CompressFormat getCompressFormatByFileType(FileType filetype)
    {
        static class _cls1
        {

            static final int $SwitchMap$com$smartdevicelink$proxy$rpc$enums$FileType[];

            static 
            {
                $SwitchMap$com$smartdevicelink$proxy$rpc$enums$FileType = new int[FileType.values().length];
                try
                {
                    $SwitchMap$com$smartdevicelink$proxy$rpc$enums$FileType[FileType.GRAPHIC_JPEG.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$smartdevicelink$proxy$rpc$enums$FileType[FileType.GRAPHIC_PNG.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        switch (_cls1..SwitchMap.com.smartdevicelink.proxy.rpc.enums.FileType[filetype.ordinal()])
        {
        default:
            return android.graphics.Bitmap.CompressFormat.JPEG;

        case 1: // '\001'
            return android.graphics.Bitmap.CompressFormat.JPEG;

        case 2: // '\002'
            return android.graphics.Bitmap.CompressFormat.PNG;
        }
    }

    public static android.graphics.Bitmap.CompressFormat getCompressFormatByImageID(int i)
    {
        return getCompressFormatByFileType(getImageResourceByImageID(i).getFileType());
    }

    public static byte[] getImageRawData(Bitmap bitmap, FileType filetype)
    {
        return AndroidUtils.bitmapToRawBytes(bitmap, getCompressFormatByFileType(filetype));
    }

    public static byte[] getImageRawDataByImageID(Resources resources, int i)
    {
        return AndroidUtils.bitmapToRawBytes(getBitmapByImageID(resources, i), getCompressFormatByImageID(i));
    }

    public static byte[] getImageRawDataByImageResource(Resources resources, SdlImageResource sdlimageresource)
    {
        return AndroidUtils.bitmapToRawBytes(sdlimageresource.getBitmap(resources), getCompressFormatByImageID(sdlimageresource.getImageID()));
    }

    public static SdlImageResource getImageResourceByImageID(int i)
    {
        SdlImageResource asdlimageresource[] = values();
        int k = asdlimageresource.length;
        for (int j = 0; j < k; j++)
        {
            SdlImageResource sdlimageresource = asdlimageresource[j];
            if (i == sdlimageresource.getImageID())
            {
                return sdlimageresource;
            }
        }

        return null;
    }

    public static SdlImageResource getImageResourceByName(String s)
    {
        SdlImageResource asdlimageresource[] = values();
        int j = asdlimageresource.length;
        for (int i = 0; i < j; i++)
        {
            SdlImageResource sdlimageresource = asdlimageresource[i];
            if (sdlimageresource.toString().equals(s))
            {
                return sdlimageresource;
            }
        }

        return null;
    }

    public static SdlImageResource valueOf(String s)
    {
        return (SdlImageResource)Enum.valueOf(com/ximalaya/ting/android/applink/SdlImageResource, s);
    }

    public static SdlImageResource[] values()
    {
        return (SdlImageResource[])$VALUES.clone();
    }

    public Bitmap getBitmap(Resources resources)
    {
        return BitmapFactory.decodeResource(resources, getImageID());
    }

    public FileType getFileType()
    {
        return fileType;
    }

    public Image getImage()
    {
        Image image = new Image();
        image.setImageType(ImageType.DYNAMIC);
        image.setValue(friendlyName);
        return image;
    }

    public int getImageID()
    {
        return imageID;
    }

    public String toString()
    {
        return friendlyName;
    }

    static 
    {
        APP_ICON = new SdlImageResource("APP_ICON", 0, "AppIcon", FileType.GRAPHIC_PNG, 0x7f020598);
        $VALUES = (new SdlImageResource[] {
            APP_ICON
        });
    }
}
