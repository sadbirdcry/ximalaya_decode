// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.smartdevicelink.proxy.rpc.Choice;
import com.smartdevicelink.proxy.rpc.Image;
import com.smartdevicelink.proxy.rpc.OnButtonEvent;
import com.smartdevicelink.proxy.rpc.OnCommand;
import com.smartdevicelink.proxy.rpc.OnHMIStatus;
import com.smartdevicelink.proxy.rpc.SoftButton;
import com.smartdevicelink.proxy.rpc.StartTime;
import com.smartdevicelink.proxy.rpc.enums.AudioStreamingState;
import com.smartdevicelink.proxy.rpc.enums.ButtonName;
import com.smartdevicelink.proxy.rpc.enums.FileType;
import com.smartdevicelink.proxy.rpc.enums.HMILevel;
import com.smartdevicelink.proxy.rpc.enums.ImageType;
import com.smartdevicelink.proxy.rpc.enums.InteractionMode;
import com.smartdevicelink.proxy.rpc.enums.SoftButtonType;
import com.smartdevicelink.proxy.rpc.enums.SystemAction;
import com.smartdevicelink.proxy.rpc.enums.UpdateMode;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoNew;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.HistoryManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.modelnew.AlbumModelNew;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.service.play.Playlist;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.BitmapUtils;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            InteractionMenu, BtnId, SdlController, AlbumMenu, 
//            SoundMenu, SdlImageResource, BaseMenu, AlbumLoader

public class SdlService extends Service
{
    private class LoadCollectedTask extends MyAsyncTask
    {

        private String mUrl;
        final SdlService this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            Object obj = null;
            avoid = AlbumModelManage.getInstance();
            if (!UserInfoMannage.hasLogined() && avoid.isLocalCollectEnable(mAppCtx))
            {
                avoid = avoid.getLocalCollectAlbumList();
            } else
            {
                avoid = new RequestParams();
                avoid.put("pageId", (new StringBuilder()).append(mCollectPageId).append("").toString());
                avoid.put("pageSize", (new StringBuilder()).append(mPageSize).append("").toString());
                com.ximalaya.ting.android.b.n.a a = f.a().a(mUrl, avoid, false);
                avoid = obj;
                if (a.b == 1)
                {
                    avoid = obj;
                    if (!TextUtils.isEmpty(a.a))
                    {
                        try
                        {
                            avoid = JSON.parseArray(JSON.parseObject(a.a).getString("list"), com/ximalaya/ting/android/model/album/AlbumModel);
                        }
                        // Misplaced declaration of an exception variable
                        catch (Void avoid[])
                        {
                            avoid.printStackTrace();
                            return null;
                        }
                        return avoid;
                    }
                }
            }
            return avoid;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            super.onPostExecute(list);
            mLoadCollectSuccess = false;
            if (!checkSdlStatus())
            {
                return;
            }
            if (list != null && list.size() > 0)
            {
                mLoadCollectSuccess = true;
                mLastLoadCollectTime = System.currentTimeMillis();
                if (mCollectPageId == 1)
                {
                    mCollectedAlbums.clear();
                }
                list = list.iterator();
                do
                {
                    if (!list.hasNext())
                    {
                        break;
                    }
                    AlbumModel albummodel = (AlbumModel)list.next();
                    if (!TextUtils.isEmpty(albummodel.title))
                    {
                        AlbumMenu albummenu = new AlbumMenu();
                        albummenu.setMenuName(albummodel.title);
                        albummenu.setAlbumId(albummodel.albumId);
                        albummenu.setMenuId(int i = 
// JavaClassFileOutputException: get_constant: invalid tag

        protected void onPreExecute()
        {
            onPreExecute();
            mLoadingCollect = true;
        }

        private LoadCollectedTask()
        {
            this$0 = SdlService.this;
            super();
            mUrl = "mobile/album/subscribe/list";
        }

        LoadCollectedTask(_cls1 _pcls1)
        {
            this();
        }
    }

    private class LoadHistoryTask extends MyAsyncTask
    {

        final SdlService this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            avoid = mHistoryManage.getSoundInfoList();
            Object obj = new ArrayList();
            if (avoid != null)
            {
                ((List) (obj)).addAll(avoid);
            }
            avoid = new ArrayList();
            if (obj != null && ((List) (obj)).size() > 0)
            {
                obj = ((List) (obj)).iterator();
                do
                {
                    if (!((Iterator) (obj)).hasNext())
                    {
                        break;
                    }
                    SoundInfo soundinfo = (SoundInfo)((Iterator) (obj)).next();
                    if (soundinfo.albumId > 0L && !TextUtils.isEmpty(soundinfo.albumName))
                    {
                        AlbumMenu albummenu1 = new AlbumMenu();
                        albummenu1.setAlbumId(soundinfo.albumId);
                        if (!avoid.contains(albummenu1))
                        {
                            albummenu1.setMenuName(soundinfo.albumName);
                            albummenu1.setMenuId(int i = ((access._cls6102) (this)).access$6102);
                            avoid.add(albummenu1);
                            mAlbumLoader.preload(albummenu1, albummenu1.getPageId() + 1);
                        }
                    }
                } while (true);
            }
            if (avoid.size() == 0)
            {
                AlbumMenu albummenu = new AlbumMenu();
                albummenu.setMenuName("\u65E0\u64AD\u653E\u5386\u53F2");
                albummenu.setAlbumId(-1L);
                albummenu.setMenuId(int i = ((access._cls6102) (this)).access$6102);
                avoid.add(albummenu);
                mInvalidMenu.add(albummenu);
            }
            return avoid;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            super.onPostExecute(list);
            mLoadingHistory = false;
            if (!checkSdlStatus())
            {
                return;
            }
            mLastLoadHistoryTime = System.currentTimeMillis();
            mHistoryAlbums.clear();
            mHistoryAlbums.addAll(list);
            list = new Vector();
            Choice choice;
            for (Iterator iterator = mHistoryAlbums.iterator(); iterator.hasNext(); list.add(choice))
            {
                Object obj = (AlbumMenu)iterator.next();
                choice = new Choice();
                choice.setChoiceID(Integer.valueOf(((AlbumMenu) (obj)).getMenuId()));
                choice.setMenuName(subString(((AlbumMenu) (obj)).getMenuName()));
                choice.setVrCommands(new Vector(Arrays.asList(new String[] {
                    subString(((AlbumMenu) (obj)).getMenuName())
                })));
                obj = new Image();
                ((Image) (obj)).setImageType(ImageType.DYNAMIC);
                ((Image) (obj)).setValue(mAlbumIconName);
                choice.setImage(((Image) (obj)));
            }

            if (mHistoryAlbumChoiceSet.getChoiceSetId() > 0)
            {
                mSdlController.sdlDeleteChoiceSet(mHistoryAlbumChoiceSet.getChoiceSetId());
            }
            mHistoryAlbumChoiceSet.setChoiceSet(list);
            mHistoryAlbumChoiceSet.setChoiceSetId(int i = ((access._cls6102) (this)).access$6102);
            mHistoryAlbumChoiceSet.setChoiceSetName(getResources().getString(0x7f090231));
            createInteractionChoiceSet(mHistoryAlbumChoiceSet);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            mLoadingHistory = true;
        }

        private LoadHistoryTask()
        {
            this$0 = SdlService.this;
            super();
        }

        LoadHistoryTask(_cls1 _pcls1)
        {
            this();
        }
    }

    private class LoadHotAlbumTask extends MyAsyncTask
    {

        private String mUrl;
        final SdlService this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            avoid = new RequestParams();
            avoid.put("page", (new StringBuilder()).append("").append(mHotAlbumPageId).toString());
            avoid.put("per_page", (new StringBuilder()).append("").append(mPageSize).toString());
            avoid.put("category_name", "all");
            avoid.put("condition", "hot");
            avoid.put("status", "0");
            avoid.put("tag_name", "");
            avoid = f.a().a(mUrl, avoid, true);
            if (((com.ximalaya.ting.android.b.n.a) (avoid)).b != 1)
            {
                break MISSING_BLOCK_LABEL_148;
            }
            avoid = JSON.parseArray(JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (avoid)).a).getString("list"), com/ximalaya/ting/android/modelnew/AlbumModelNew);
            return avoid;
            avoid;
            avoid.printStackTrace();
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            super.onPostExecute(list);
            mLoadingHotAlbum = false;
            mLoadHotAlbumSuccess = false;
            if (!checkSdlStatus())
            {
                return;
            }
            Vector vector = new Vector();
            if (list != null && list.size() != 0)
            {
                mLastLoadHotAlbumTime = System.currentTimeMillis();
                mLoadHotAlbumSuccess = true;
                if (mHotAlbumPageId == 1)
                {
                    mHotAlbums.clear();
                }
                Object obj1;
                for (list = list.iterator(); list.hasNext(); vector.add(obj1))
                {
                    obj1 = (AlbumModelNew)list.next();
                    Object obj = new AlbumMenu();
                    ((AlbumMenu) (obj)).setMenuName(((AlbumModelNew) (obj1)).title);
                    ((AlbumMenu) (obj)).setAlbumId(((AlbumModelNew) (obj1)).id);
                    ((AlbumMenu) (obj)).setMenuId(int i = 
// JavaClassFileOutputException: get_constant: invalid tag

        protected void onPreExecute()
        {
            onPreExecute();
            mLoadingHotAlbum = true;
        }

        private LoadHotAlbumTask()
        {
            this$0 = SdlService.this;
            super();
            mUrl = (new StringBuilder()).append(ApiUtil.getApiHost()).append("m/explore_album_list").toString();
        }

        LoadHotAlbumTask(_cls1 _pcls1)
        {
            this();
        }
    }

    private class LoadHotSoundTask extends MyAsyncTask
    {

        private String mUrl;
        final SdlService this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            avoid = null;
            Object obj = null;
            Object obj1 = new RequestParams();
            ((RequestParams) (obj1)).put("page", (new StringBuilder()).append("").append(mHotSoundPageId).toString());
            ((RequestParams) (obj1)).put("per_page", (new StringBuilder()).append("").append(mPageSize).toString());
            ((RequestParams) (obj1)).put("condition", "daily");
            ((RequestParams) (obj1)).put("category_name", "all");
            ((RequestParams) (obj1)).put("tag_name", "");
            obj1 = f.a().a(mUrl, ((RequestParams) (obj1)), true);
            if (((com.ximalaya.ting.android.b.n.a) (obj1)).b == 1)
            {
                try
                {
                    avoid = JSON.parseArray(JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj1)).a).getString("list"), com/ximalaya/ting/android/model/sound/SoundInfoNew);
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    avoid.printStackTrace();
                    avoid = obj;
                }
                avoid = ModelHelper.toSoundInfo(avoid);
            }
            return avoid;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            onPostExecute(list);
            mLoadingHotSound = false;
            mLoadHotSoundSuccess = false;
            if (!checkSdlStatus())
            {
                return;
            }
            if (list == null || list.size() == 0)
            {
                mLoadHotSoundSuccess = false;
                return;
            }
            mLoadHotSoundSuccess = true;
            mLastLoadHotSoundTime = System.currentTimeMillis();
            if (mHotSoundPageId == 1)
            {
                mHotSounds.clear();
            }
            mHotSounds.addAll(list);
            int i = ((com.ximalaya.ting.android.applink) (this)).SoundMenu;
            mHotSoundMenu.clear();
            list = new Vector();
            if (mHotSounds.size() > 0)
            {
                Choice choice1;
                for (Iterator iterator = mHotSounds.iterator(); iterator.hasNext(); list.add(choice1))
                {
                    SoundInfo soundinfo = (SoundInfo)iterator.next();
                    SoundMenu soundmenu1 = new SoundMenu();
                    soundmenu1.setMenuId(int i = ((size) (this)).size);
                    soundmenu1.setSoundInfo(soundinfo);
                    mHotSoundMenu.add(soundmenu1);
                    choice1 = new Choice();
                    choice1.setChoiceID(Integer.valueOf(soundmenu1.getMenuId()));
                    choice1.setMenuName(subString(soundinfo.title));
                    choice1.setVrCommands(new Vector(Arrays.asList(new String[] {
                        subString(soundinfo.title)
                    })));
                }

            } else
            {
                SoundMenu soundmenu = new SoundMenu();
                soundmenu.setMenuId(int i = ((size) (this)).size);
                soundmenu.setSoundInfo(null);
                soundmenu.setMenuName("\u65E0\u70ED\u95E8\u58F0\u97F3");
                mHotSoundMenu.add(soundmenu);
                mInvalidMenu.add(soundmenu);
                Choice choice = new Choice();
                choice.setChoiceID(Integer.valueOf(soundmenu.getMenuId()));
                choice.setMenuName(soundmenu.getMenuName());
                choice.setVrCommands(new Vector(Arrays.asList(new String[] {
                    subString(soundmenu.getMenuName())
                })));
                list.add(choice);
            }
            mHotSoundChoiceSet.setChoiceSet(list);
            mHotSoundChoiceSet.setChoiceSetId(int i = ((size) (this)).size);
            mHotSoundChoiceSet.setChoiceSetName(getResources().getString(0x7f090232));
            createInteractionChoiceSet(mHotSoundChoiceSet);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            mLoadingHotSound = true;
        }

        private LoadHotSoundTask()
        {
            this$0 = SdlService.this;
            super();
            mUrl = (new StringBuilder()).append(ApiUtil.getApiHost()).append("m/explore_track_list").toString();
        }

        LoadHotSoundTask(_cls1 _pcls1)
        {
            this();
        }
    }

    private class LoadLocalAlbumTask extends MyAsyncTask
    {

        final SdlService this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            avoid = DownloadHandler.getInstance(mAppCtx).getSortedFinishedDownloadList();
            Vector vector = mLocalAlbums;
            vector;
            JVM INSTR monitorenter ;
            mLocalAlbums.clear();
            if (avoid == null) goto _L2; else goto _L1
_L1:
            Iterator iterator = avoid.iterator();
_L8:
            if (!iterator.hasNext()) goto _L2; else goto _L3
_L3:
            AlbumMenu albummenu;
            DownloadTask downloadtask;
            downloadtask = (DownloadTask)iterator.next();
            albummenu = getLocalAlbumMenu(downloadtask.albumId);
            avoid = albummenu;
            if (albummenu != null) goto _L5; else goto _L4
_L4:
            avoid = new AlbumMenu();
            if (downloadtask.albumId <= 0L)
            {
                break MISSING_BLOCK_LABEL_177;
            }
            avoid.setMenuName(downloadtask.albumName);
_L6:
            avoid.setAlbumId(downloadtask.albumId);
            avoid.setSoundList(new Vector());
            avoid.setMenuId(int i = 
// JavaClassFileOutputException: get_constant: invalid tag

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            onPostExecute(void1);
            mAlbumLoading = false;
            if (!checkSdlStatus())
            {
                return;
            }
            void1 = new Vector();
            int j = Math.min(100, mLocalAlbums.size());
            for (int i = 0; i < j; i++)
            {
                Object obj = (AlbumMenu)mLocalAlbums.get(i);
                Choice choice = new Choice();
                choice.setChoiceID(Integer.valueOf(((AlbumMenu) (obj)).getMenuId()));
                choice.setMenuName(subString(((AlbumMenu) (obj)).getMenuName()));
                choice.setVrCommands(new Vector(Arrays.asList(new String[] {
                    subString(((AlbumMenu) (obj)).getMenuName())
                })));
                obj = new Image();
                ((Image) (obj)).setImageType(ImageType.DYNAMIC);
                ((Image) (obj)).setValue(mAlbumIconName);
                choice.setImage(((Image) (obj)));
                void1.add(choice);
            }

            mLocalAlbumChoiceSet.setChoiceSet(void1);
            mLocalAlbumChoiceSet.setChoiceSetId(int i = 
// JavaClassFileOutputException: get_constant: invalid tag

        protected void onPreExecute()
        {
            onPreExecute();
            mAlbumLoading = true;
        }

        private LoadLocalAlbumTask()
        {
            this$0 = SdlService.this;
            super();
        }

        LoadLocalAlbumTask(_cls1 _pcls1)
        {
            this();
        }
    }


    private static final int CONNECTION_TIMEOUT = 60000;
    private static final long LOAD_EXPIRE_IN = 60000L;
    private static final long LOAD_HISTORY_EXPIRE_IN = 2000L;
    private static final long LOAD_RECOMMEND_EXPIRE_IN = 0x927c0L;
    private static final int MAX_CHOICE_SET_SIZE = 100;
    private static final int STOP_SERVICE_DELAY = 5000;
    private static final String TAG = "XmSdlService";
    private static SdlService instance = null;
    private String mAlbumIconName;
    private AlbumLoader mAlbumLoader;
    private boolean mAlbumLoading;
    private Context mAppCtx;
    private int mAutoIncCorrId;
    private SoftButton mBtnCollect;
    private SoftButton mBtnHistory;
    private BtnId mBtnId;
    private SoftButton mBtnLocal;
    private SoftButton mBtnPlayList;
    private volatile boolean mBuffering;
    private Runnable mCheckConnectionRunnable;
    private InteractionMenu mCollectAlbumChoiceSet;
    private int mCollectPageId;
    private Vector mCollectedAlbums;
    private Vector mCommonSoftbutton;
    private Image mCoverImage;
    private int mCurrIndex;
    private SoundInfo mCurrSound;
    private DownloadHandler mDownloadHander;
    private com.ximalaya.ting.android.transaction.download.DownloadHandler.DownloadSoundsListener mDownloadListener;
    private boolean mEnable3G;
    private boolean mFirst;
    private InteractionMenu mHistoryAlbumChoiceSet;
    private Vector mHistoryAlbums;
    private HistoryManage mHistoryManage;
    private InteractionMenu mHotAlbumChoiceSet;
    private int mHotAlbumPageId;
    private Vector mHotAlbums;
    private InteractionMenu mHotSoundChoiceSet;
    private Vector mHotSoundMenu;
    private int mHotSoundPageId;
    private List mHotSounds;
    private Vector mInvalidMenu;
    private long mLastAlert;
    private String mLastCoverUrl;
    private int mLastIndex;
    private int mLastListSize;
    private long mLastLoadCollectTime;
    private long mLastLoadHistoryTime;
    private long mLastLoadHotAlbumTime;
    private long mLastLoadHotSoundTime;
    private SoundInfo mLastSound;
    private LoadLocalAlbumTask mLoadAlbumTask;
    private boolean mLoadCollectSuccess;
    private LoadCollectedTask mLoadCollectedTask;
    private LoadHistoryTask mLoadHistoryTask;
    private boolean mLoadHotAlbumSuccess;
    private LoadHotAlbumTask mLoadHotAlbumTask;
    private boolean mLoadHotSoundSuccess;
    private LoadHotSoundTask mLoadHotSoundTask;
    private boolean mLoadingCollect;
    private boolean mLoadingHistory;
    private boolean mLoadingHotAlbum;
    private boolean mLoadingHotSound;
    private InteractionMenu mLocalAlbumChoiceSet;
    private Vector mLocalAlbums;
    private final String mNetErrMsg = "\u7F51\u7EDC\u4E0D\u7ED9\u529B,\u8BF7\u7A0D\u540E\u518D\u8BD5";
    private int mPageSize;
    private boolean mPaused;
    private InteractionMenu mPlayListChoiceSet;
    private int mPlayListEnd;
    private int mPlayListLastEnd;
    private int mPlayListLastStart;
    private Vector mPlayListMenu;
    private int mPlayListStart;
    private LocalMediaService mPlayer;
    private SdlController mSdlController;
    private com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener mServiceListener;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mStatusListener;
    private Runnable mStopServiceRunnable;
    private Handler mUiHandler;

    public SdlService()
    {
        mAutoIncCorrId = 1;
        mFirst = true;
        mLastAlert = 0L;
        mCommonSoftbutton = new Vector();
        mLocalAlbums = new Vector();
        mCollectedAlbums = new Vector();
        mHistoryAlbums = new Vector();
        mHotAlbums = new Vector();
        mPlayListMenu = new Vector();
        mHotSoundMenu = new Vector();
        mInvalidMenu = new Vector();
        mHotSounds = new ArrayList();
        mAlbumLoading = false;
        mLoadingCollect = false;
        mLoadingHistory = false;
        mLoadingHotSound = false;
        mLoadingHotAlbum = false;
        mLoadCollectSuccess = false;
        mLoadHotSoundSuccess = false;
        mLoadHotAlbumSuccess = false;
        mCollectPageId = 1;
        mHotAlbumPageId = 1;
        mHotSoundPageId = 1;
        mPageSize = 15;
        mLastLoadCollectTime = 0L;
        mLastLoadHotSoundTime = 0L;
        mLastLoadHotAlbumTime = 0L;
        mLastLoadHistoryTime = 0L;
        mPaused = false;
        mAlbumIconName = "sdl_album_icon";
        mLocalAlbumChoiceSet = new InteractionMenu();
        mCollectAlbumChoiceSet = new InteractionMenu();
        mHistoryAlbumChoiceSet = new InteractionMenu();
        mHotAlbumChoiceSet = new InteractionMenu();
        mPlayListChoiceSet = new InteractionMenu();
        mHotSoundChoiceSet = new InteractionMenu();
        mBtnId = new BtnId();
        mBuffering = false;
        mCurrIndex = -1;
        mLastIndex = -1;
        mEnable3G = false;
        mUiHandler = new Handler();
        mDownloadListener = new _cls1();
        mStatusListener = new _cls2();
        mServiceListener = new _cls3();
        mCheckConnectionRunnable = new _cls5();
        mStopServiceRunnable = new _cls6();
    }

    private void addCommonCommand()
    {
        if (!checkSdlStatus())
        {
            return;
        } else
        {
            BtnId btnid = mBtnId;
            int i = mAutoIncCorrId;
            mAutoIncCorrId = i + 1;
            btnid.btn_hot_album = i;
            mSdlController.sdlAddCommand(mBtnId.btn_hot_album, getResources().getString(0x7f09022d), 0, 0, null);
            btnid = mBtnId;
            i = mAutoIncCorrId;
            mAutoIncCorrId = i + 1;
            btnid.btn_hot_sound = i;
            mSdlController.sdlAddCommand(mBtnId.btn_hot_sound, getResources().getString(0x7f09022c), 0, 0, null);
            return;
        }
    }

    private void alert(String s, String s1)
    {
        long l;
        if (checkSdlStatus())
        {
            if ((l = System.currentTimeMillis()) - mLastAlert >= (long)2500)
            {
                mLastAlert = l;
                mSdlController.sdlAlert(null, s, s1, true, Integer.valueOf(5000));
                return;
            }
        }
    }

    private void createInteractionChoiceSet(InteractionMenu interactionmenu)
    {
        while (interactionmenu == null || !checkSdlStatus()) 
        {
            return;
        }
        Logger.e("XmSdlService", (new StringBuilder()).append("createInteractionChoiceSet,").append(interactionmenu.getChoiceSetName()).append(",").append(interactionmenu.getChoiceSetId()).toString());
        mSdlController.sdlCreateChoiceSet(interactionmenu.getChoiceSet(), Integer.valueOf(interactionmenu.getChoiceSetId()));
    }

    private void doActionAudible()
    {
        if (mPaused && mPlayer != null && !mPlayer.isPlaying())
        {
            mPlayer.start();
            mPaused = false;
        }
    }

    private void doActionChooseAlbum(AlbumMenu albummenu)
    {
        if (albummenu == null)
        {
            alert("\u7F51\u7EDC\u4E0D\u7ED9\u529B,\u8BF7\u7A0D\u540E\u518D\u8BD5", null);
        } else
        {
            if (albummenu.getAlbumId() != -1L)
            {
                if (albummenu.getSoundList() != null && albummenu.getSoundList().size() > 0)
                {
                    Logger.e("XmSdlService", "---play sound list");
                    PlayTools.gotoPlay(3, albummenu.getSoundList(), 0, mAppCtx, false, null);
                    return;
                } else
                {
                    Logger.e("XmSdlService", "onInteraction AlbumMenu List null");
                    alert("\u7F51\u7EDC\u4E0D\u7ED9\u529B,\u8BF7\u7A0D\u540E\u518D\u8BD5", null);
                    return;
                }
            }
            if (albummenu.getAlbumId() == -1L)
            {
                Logger.e("XmSdlService", "-label click");
                return;
            }
        }
    }

    private void doActionChooseSound(SoundMenu soundmenu)
    {
        soundmenu = soundmenu.getSoundInfo();
        if (soundmenu != null)
        {
            List list = PlayListControl.getPlayListManager().getPlaylist().getData();
            Logger.e("XmSdlService", (new StringBuilder()).append("MSG_CHOOSE_SOUND ").append(list.size()).append(",").append(soundmenu).toString());
            if (list != null && list.size() != 0)
            {
                int i = list.indexOf(soundmenu);
                if (i >= 0 && i <= list.size())
                {
                    PlayTools.gotoPlay(0, new ArrayList(list), i, mAppCtx, false, null);
                    return;
                }
            }
        }
    }

    private void doActionDeleteFile(boolean flag, String s)
    {
        while (!checkSdlStatus() || !flag) 
        {
            return;
        }
        mCoverImage.setValue(SdlImageResource.APP_ICON.toString());
    }

    private void doActionNotAudible()
    {
        if (mPlayer != null && mPlayer.isPlaying())
        {
            mPlayer.pause();
            mPaused = true;
        }
    }

    private void doActionPlayHotSound(SoundMenu soundmenu)
    {
        soundmenu = soundmenu.getSoundInfo();
        if (soundmenu == null)
        {
            return;
        } else
        {
            PlayTools.gotoPlay(0, mHotSounds, mHotSounds.indexOf(soundmenu), mAppCtx, false, null);
            return;
        }
    }

    private void doActionPlayNext()
    {
        if (mPlayer == null || PlayListControl.getPlayListManager().getPlaylist().getData() == null)
        {
            return;
        }
        if (hasNextSound())
        {
            mPlayer.playNext(true);
            return;
        } else
        {
            alert("\u4EB2\uFF0C\u6CA1\u6709\u4E0B\u4E00\u9996\u4E86", null);
            return;
        }
    }

    private void doActionPlayOrPause()
    {
        if (mPlayer == null)
        {
            return;
        }
        switch (mPlayer.getMediaPlayerState())
        {
        case 1: // '\001'
        default:
            return;

        case 0: // '\0'
        case 6: // '\006'
            mPlayer.doPlay();
            return;

        case 3: // '\003'
        case 5: // '\005'
        case 7: // '\007'
            mPlayer.start();
            return;

        case 4: // '\004'
            mPlayer.pause();
            return;

        case 2: // '\002'
        case 8: // '\b'
            mPlayer.restart();
            return;
        }
    }

    private void doActionPlayPre()
    {
        if (mPlayer == null || PlayListControl.getPlayListManager().getPlaylist().getData() == null)
        {
            return;
        }
        if (hasPreSound())
        {
            mPlayer.playPrev(true);
            return;
        } else
        {
            alert("\u4EB2\uFF0C\u6CA1\u6709\u4E0A\u4E00\u9996\u4E86", null);
            return;
        }
    }

    private void doActionPutFile(boolean flag, String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        }
        if (s.equals(mAlbumIconName))
        {
            if (flag)
            {
                updateLocalAlbum();
                updateCollectAlbum();
                updateHistoryAlbum();
                updateHotAlbum();
                updateHotSound();
                updatePlayListSet();
                return;
            } else
            {
                putAlbumIcon();
                return;
            }
        }
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (flag && soundinfo != null && s.equals(ToolUtil.md5(mLastCoverUrl)))
        {
            mCoverImage.setValue(s);
            updateMainScreen(soundinfo.albumName, soundinfo.title, mCoverImage);
            return;
        } else
        {
            mCoverImage.setValue(SdlImageResource.APP_ICON.toString());
            return;
        }
    }

    private BaseMenu getAlbumMenuByMenuId(Vector vector, int i)
    {
        if (vector == null || vector.size() == 0)
        {
            return null;
        }
        for (vector = vector.iterator(); vector.hasNext();)
        {
            BaseMenu basemenu = (BaseMenu)vector.next();
            if (basemenu.getMenuId() == i)
            {
                return basemenu;
            }
        }

        return null;
    }

    private String getCoverUrl(SoundInfo soundinfo)
    {
        if (!TextUtils.isEmpty(soundinfo.albumCoverPath))
        {
            return soundinfo.albumCoverPath;
        }
        if (!TextUtils.isEmpty(soundinfo.coverLarge))
        {
            return soundinfo.coverLarge;
        }
        if (!TextUtils.isEmpty(soundinfo.coverSmall))
        {
            return soundinfo.coverSmall;
        } else
        {
            return null;
        }
    }

    public static SdlService getInstance()
    {
        return instance;
    }

    private BaseMenu getInvalidMenu(int i)
    {
        return getAlbumMenuByMenuId(mInvalidMenu, i);
    }

    private AlbumMenu getLocalAlbumMenu(long l)
    {
        for (Iterator iterator = mLocalAlbums.iterator(); iterator.hasNext();)
        {
            AlbumMenu albummenu = (AlbumMenu)iterator.next();
            if (albummenu.getAlbumId() == l)
            {
                return albummenu;
            }
        }

        return null;
    }

    private BaseMenu getMenuByMenuId(int i, int j)
    {
        Vector vector = null;
        if (i != mLocalAlbumChoiceSet.getChoiceSetId()) goto _L2; else goto _L1
_L1:
        vector = mLocalAlbums;
_L4:
        return getAlbumMenuByMenuId(vector, j);
_L2:
        if (i == mHistoryAlbumChoiceSet.getChoiceSetId())
        {
            vector = mHistoryAlbums;
        } else
        if (i == mCollectAlbumChoiceSet.getChoiceSetId())
        {
            vector = mCollectedAlbums;
        } else
        if (i == mPlayListChoiceSet.getChoiceSetId())
        {
            vector = mPlayListMenu;
        } else
        if (i == mHotAlbumChoiceSet.getChoiceSetId())
        {
            vector = mHotAlbums;
        } else
        if (i == mHotSoundChoiceSet.getChoiceSetId())
        {
            vector = mHotSoundMenu;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private UpdateMode getUpdateMode()
    {
        if (mBuffering)
        {
            return UpdateMode.PAUSE;
        }
        if (mPlayer.isPlaying())
        {
            return UpdateMode.COUNTUP;
        } else
        {
            return UpdateMode.PAUSE;
        }
    }

    private boolean hasNextSound()
    {
        int i = PlayListControl.getPlayListManager().curIndex;
        return PlayListControl.getPlayListManager().getSize() > 1 && i + 1 < PlayListControl.getPlayListManager().getSize();
    }

    private boolean hasPreSound()
    {
        int i = PlayListControl.getPlayListManager().curIndex;
        return PlayListControl.getPlayListManager().getSize() > 1 && i - 1 >= 0;
    }

    private boolean isInCurrentBound(int i)
    {
        return mPlayListLastEnd > 0 && i <= mPlayListLastEnd && i >= mPlayListLastStart;
    }

    private boolean isSamePlayList(List list)
    {
        return mLastSound != null && list.contains(mLastSound) && mLastListSize == list.size() && list.indexOf(mLastSound) == mLastIndex;
    }

    private void onButtonPressPrivate(OnButtonEvent onbuttonevent)
    {
        ButtonName buttonname;
        long l;
        l = System.currentTimeMillis();
        buttonname = onbuttonevent.getButtonName();
        if (buttonname != null) goto _L2; else goto _L1
_L1:
        Logger.e("XmSdlService", "onButtonPress Received Bad Message!");
_L4:
        return;
_L2:
        Logger.e("XmSdlService", (new StringBuilder()).append("------------------onOnButtonPress ").append(buttonname).toString());
        if (buttonname.equals(ButtonName.OK))
        {
            doActionPlayOrPause();
            return;
        }
        if (buttonname.equals(ButtonName.SEEKLEFT))
        {
            doActionPlayPre();
            return;
        }
        if (buttonname.equals(ButtonName.SEEKRIGHT))
        {
            doActionPlayNext();
            return;
        }
        if (!buttonname.equals(ButtonName.CUSTOM_BUTTON)) goto _L4; else goto _L3
_L3:
        int i;
        i = onbuttonevent.getCustomButtonID().intValue();
        Logger.e("XmSdlService", (new StringBuilder()).append("===========Customer id:").append(i).toString());
        if (i == mBtnId.btn_local)
        {
            if (mAlbumLoading)
            {
                alert("\u6B63\u5728\u52A0\u8F7D\u672C\u5730\u5217\u8868...", null);
                return;
            } else
            {
                performInteraction(mLocalAlbumChoiceSet.getChoiceSetId(), mLocalAlbumChoiceSet.getChoiceSetName(), mLocalAlbumChoiceSet.getChoiceSetName(), InteractionMode.MANUAL_ONLY);
                Logger.e("XmSdlService", (new StringBuilder()).append("----process button local event cost:").append(System.currentTimeMillis() - l).append("ms").toString());
                return;
            }
        }
        if (i == mBtnId.btn_history)
        {
            if (mLoadingHistory)
            {
                alert("\u6B63\u5728\u52A0\u8F7D\u64AD\u653E\u5386\u53F2...", null);
                return;
            } else
            {
                performInteraction(mHistoryAlbumChoiceSet.getChoiceSetId(), mHistoryAlbumChoiceSet.getChoiceSetName(), mHistoryAlbumChoiceSet.getChoiceSetName(), InteractionMode.MANUAL_ONLY);
                Logger.e("XmSdlService", (new StringBuilder()).append("----process button history event cost:").append(System.currentTimeMillis() - l).append("ms").toString());
                Logger.e("XmSdlService", (new StringBuilder()).append("show history ").append(mHistoryAlbumChoiceSet.getChoiceSetId()).toString());
                return;
            }
        }
        if (i != mBtnId.btn_collect)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (mLoadingCollect)
        {
            alert("\u6B63\u5728\u52A0\u8F7D\u6211\u7684\u6536\u85CF...", null);
            return;
        }
        Logger.e("XmSdlService", (new StringBuilder()).append("show collect ").append(mCollectAlbumChoiceSet.getChoiceSetId()).toString());
        performInteraction(mCollectAlbumChoiceSet.getChoiceSetId(), mCollectAlbumChoiceSet.getChoiceSetName(), mCollectAlbumChoiceSet.getChoiceSetName(), InteractionMode.MANUAL_ONLY);
        Logger.e("XmSdlService", (new StringBuilder()).append("----process button collect event cost:").append(System.currentTimeMillis() - l).append("ms").toString());
        if (mLoadCollectSuccess && System.currentTimeMillis() - mLastLoadCollectTime <= 60000L) goto _L4; else goto _L5
_L5:
        updateCollectAlbum();
        return;
        if (i != mBtnId.btn_play_list) goto _L4; else goto _L6
_L6:
        onbuttonevent = PlayListControl.getPlayListManager().getPlaylist();
        if (onbuttonevent == null || onbuttonevent.getData() == null || onbuttonevent.getData().size() == 0)
        {
            alert("\u4EB2\uFF0C\u64AD\u653E\u5668\u65E0\u64AD\u653E\u5217\u8868\u54E6", null);
            return;
        }
        if (mPlayListChoiceSet.getChoiceSetId() > 0)
        {
            performInteraction(mPlayListChoiceSet.getChoiceSetId(), mPlayListChoiceSet.getChoiceSetName(), mPlayListChoiceSet.getChoiceSetName(), InteractionMode.MANUAL_ONLY);
            Logger.e("XmSdlService", (new StringBuilder()).append("----process button playlist event cost:").append(System.currentTimeMillis() - l).append("ms").toString());
            Logger.e("XmSdlService", (new StringBuilder()).append("show play list ").append(mPlayListChoiceSet.getChoiceSetId()).toString());
            return;
        } else
        {
            alert("\u6B63\u5728\u52A0\u8F7D\u64AD\u653E\u5217\u8868\uFF0C\u8BF7\u7A0D\u5019\uFF01", null);
            updatePlayListSet();
            return;
        }
    }

    private void putAlbumIcon()
    {
        FileType filetype = FileType.GRAPHIC_PNG;
        mSdlController.SdlPutFile(mAlbumIconName, filetype, BitmapUtils.getBitmapBytes(getResources(), 0x7f0204fa));
    }

    private void registeListener()
    {
        StringBuilder stringbuilder = (new StringBuilder()).append("registePlayerListener ");
        boolean flag;
        if (mPlayer != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        Logger.e("XmSdlService", stringbuilder.append(flag).toString());
        if (mPlayer != null)
        {
            mPlayer.setOnPlayerStatusUpdateListener(mStatusListener);
            mPlayer.setOnPlayServiceUpdateListener(mServiceListener);
        }
        if (mDownloadHander != null)
        {
            mDownloadHander.addDownloadListeners(mDownloadListener);
        }
    }

    private void releaseCollectMenu()
    {
        while (!checkSdlStatus() || mCollectAlbumChoiceSet.getLastChoiceId() <= 0) 
        {
            return;
        }
        Logger.e("XmSdlService", (new StringBuilder()).append("releaseCollectMenu ").append(mCollectAlbumChoiceSet.getLastChoiceId()).toString());
        mSdlController.sdlDeleteChoiceSet(mCollectAlbumChoiceSet.getLastChoiceId());
    }

    private void releaseHistoryMenu()
    {
        while (!checkSdlStatus() || mHistoryAlbumChoiceSet.getLastChoiceId() <= 0) 
        {
            return;
        }
        Logger.e("XmSdlService", (new StringBuilder()).append("releaseHistoryMenu ").append(mHistoryAlbumChoiceSet.getLastChoiceId()).toString());
        mSdlController.sdlDeleteChoiceSet(mHistoryAlbumChoiceSet.getLastChoiceId());
    }

    private void releaseHotAlbumMenu()
    {
        while (!checkSdlStatus() || mHotAlbumChoiceSet.getLastChoiceId() <= 0) 
        {
            return;
        }
        Logger.e("XmSdlService", (new StringBuilder()).append("releaseHotAlbumMenu ").append(mHotAlbumChoiceSet.getLastChoiceId()).toString());
        mSdlController.sdlDeleteChoiceSet(mHotAlbumChoiceSet.getLastChoiceId());
    }

    private void releaseHotSoundMenu()
    {
        while (!checkSdlStatus() || mHotSoundChoiceSet.getLastChoiceId() <= 0) 
        {
            return;
        }
        Logger.e("XmSdlService", (new StringBuilder()).append("releaseHotSoundMenu ").append(mHotSoundChoiceSet.getLastChoiceId()).toString());
        mSdlController.sdlDeleteChoiceSet(mHotSoundChoiceSet.getLastChoiceId());
    }

    private void releaseLocalMenu()
    {
        while (!checkSdlStatus() || mLocalAlbumChoiceSet.getLastChoiceId() <= 0) 
        {
            return;
        }
        Logger.e("XmSdlService", (new StringBuilder()).append("releaseLocalMenu ").append(mLocalAlbumChoiceSet.getLastChoiceId()).toString());
        mSdlController.sdlDeleteChoiceSet(mLocalAlbumChoiceSet.getLastChoiceId());
    }

    private void releaseMenu()
    {
        Logger.e("XmSdlService", "releaseMenu");
        releaseLocalMenu();
        releaseCollectMenu();
        releaseHistoryMenu();
        releaseHotAlbumMenu();
        releasePlayListMenu();
        releaseHotSoundMenu();
    }

    private void releasePlayListMenu()
    {
        while (!checkSdlStatus() || mPlayListChoiceSet.getLastChoiceId() <= 0) 
        {
            return;
        }
        Logger.e("XmSdlService", (new StringBuilder()).append("releasePlayListMenu ").append(mPlayListChoiceSet.getLastChoiceId()).toString());
        mSdlController.sdlDeleteChoiceSet(mPlayListChoiceSet.getLastChoiceId());
    }

    private String subString(String s)
    {
        String s1;
        if (TextUtils.isEmpty(s))
        {
            s1 = null;
        } else
        {
            s1 = s;
            if (s.length() > 20)
            {
                return s.substring(0, 20);
            }
        }
        return s1;
    }

    private void unregisteListener()
    {
        if (mPlayer != null)
        {
            mPlayer.removeOnPlayerUpdateListener(mStatusListener);
            mPlayer.removeOnPlayServiceUpdateListener(mServiceListener);
        }
        if (mDownloadHander != null)
        {
            mDownloadHander.removeDownloadListeners(mDownloadListener);
        }
    }

    private void updateCollectAlbum()
    {
        Logger.e("XmSdlService", "updateCollectAlbum");
        while (mLoadingCollect || mLoadCollectedTask != null && mLoadCollectedTask.getStatus() != android.os.AsyncTask.Status.FINISHED) 
        {
            return;
        }
        mLoadCollectedTask = new LoadCollectedTask(null);
        mLoadCollectedTask.myexec(new Void[0]);
    }

    private void updateCommonSoftButton()
    {
        BtnId btnid = mBtnId;
        int i = mAutoIncCorrId;
        mAutoIncCorrId = i + 1;
        btnid.btn_local = i;
        mBtnLocal = new SoftButton();
        mBtnLocal.setSoftButtonID(Integer.valueOf(mBtnId.btn_local));
        mBtnLocal.setText(getResources().getString(0x7f090229));
        mBtnLocal.setType(SoftButtonType.SBT_TEXT);
        mBtnLocal.setIsHighlighted(Boolean.valueOf(false));
        mBtnLocal.setSystemAction(SystemAction.DEFAULT_ACTION);
        mCommonSoftbutton.add(mBtnLocal);
        btnid = mBtnId;
        i = mAutoIncCorrId;
        mAutoIncCorrId = i + 1;
        btnid.btn_collect = i;
        mBtnCollect = new SoftButton();
        mBtnCollect.setSoftButtonID(Integer.valueOf(mBtnId.btn_collect));
        mBtnCollect.setText(getResources().getString(0x7f09022a));
        mBtnCollect.setType(SoftButtonType.SBT_TEXT);
        mBtnCollect.setIsHighlighted(Boolean.valueOf(false));
        mBtnCollect.setSystemAction(SystemAction.DEFAULT_ACTION);
        mCommonSoftbutton.add(mBtnCollect);
        btnid = mBtnId;
        i = mAutoIncCorrId;
        mAutoIncCorrId = i + 1;
        btnid.btn_history = i;
        mBtnHistory = new SoftButton();
        mBtnHistory.setSoftButtonID(Integer.valueOf(mBtnId.btn_history));
        mBtnHistory.setText(getResources().getString(0x7f09022b));
        mBtnHistory.setType(SoftButtonType.SBT_TEXT);
        mBtnHistory.setIsHighlighted(Boolean.valueOf(false));
        mBtnHistory.setSystemAction(SystemAction.DEFAULT_ACTION);
        mCommonSoftbutton.add(mBtnHistory);
        btnid = mBtnId;
        i = mAutoIncCorrId;
        mAutoIncCorrId = i + 1;
        btnid.btn_play_list = i;
        mBtnPlayList = new SoftButton();
        mBtnPlayList.setSoftButtonID(Integer.valueOf(mBtnId.btn_play_list));
        mBtnPlayList.setText(getResources().getString(0x7f09022e));
        mBtnPlayList.setType(SoftButtonType.SBT_TEXT);
        mBtnPlayList.setIsHighlighted(Boolean.valueOf(false));
        mBtnPlayList.setSystemAction(SystemAction.DEFAULT_ACTION);
        mCommonSoftbutton.add(mBtnPlayList);
    }

    private void updateHistory()
    {
        if (System.currentTimeMillis() - mLastLoadHistoryTime > 2000L)
        {
            Toast.makeText(mAppCtx, "update history record", 0).show();
            updateHistoryAlbum();
        }
    }

    private void updateHistoryAlbum()
    {
        Logger.e("XmSdlService", "updateHistoryAlbum");
        while (mLoadingHistory || mLoadHistoryTask != null && mLoadHistoryTask.getStatus() != android.os.AsyncTask.Status.FINISHED) 
        {
            return;
        }
        mLoadHistoryTask = new LoadHistoryTask(null);
        mLoadHistoryTask.myexec(new Void[0]);
    }

    private void updateHotAlbum()
    {
        Logger.e("XmSdlService", "updateHotAlbum");
        while (mLoadingHotAlbum || mLoadHotAlbumTask != null && mLoadHotAlbumTask.getStatus() != android.os.AsyncTask.Status.FINISHED) 
        {
            return;
        }
        mLoadHotAlbumTask = new LoadHotAlbumTask(null);
        mLoadHotAlbumTask.myexec(new Void[0]);
    }

    private void updateHotSound()
    {
        Logger.e("XmSdlService", "updateHotSound");
        while (mLoadingHotSound || mLoadHotSoundTask != null && mLoadHotSoundTask.getStatus() != android.os.AsyncTask.Status.FINISHED) 
        {
            return;
        }
        mLoadHotSoundTask = new LoadHotSoundTask(null);
        mLoadHotSoundTask.myexec(new Void[0]);
    }

    private void updateLocalAlbum()
    {
        Logger.e("XmSdlService", "updateLocalAlbum");
        if (mAlbumLoading)
        {
            Logger.e("XmSdlService", "Local Album Already Loading!");
        } else
        if (mLoadAlbumTask == null || mLoadAlbumTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
        {
            mLoadAlbumTask = new LoadLocalAlbumTask(null);
            mLoadAlbumTask.myexec(new Void[0]);
            return;
        }
    }

    private void updateMainScreen(String s, String s1, Image image)
    {
        if (!checkSdlStatus())
        {
            return;
        }
        if (TextUtils.isEmpty(s))
        {
            s = "\u559C\u9A6C\u62C9\u96C5";
        }
        if (mBuffering)
        {
            s1 = "\u7F13\u51B2\u4E2D...";
        }
        mSdlController.sdlShow(s, s1, null, null, null, null, null, image, mCommonSoftbutton, null, null);
    }

    private void updateMediaTimer(int i, int j, UpdateMode updatemode)
    {
        if (mBuffering)
        {
            j = 0;
        }
        int k = i / 0x36ee80;
        int l = (i % 0x36ee80) / 60000;
        i = (i % 60000) / 1000;
        StartTime starttime = new StartTime();
        starttime.setHours(Integer.valueOf(k));
        starttime.setMinutes(Integer.valueOf(l));
        starttime.setSeconds(Integer.valueOf(i));
        i = j / 0x36ee80;
        k = (j % 0x36ee80) / 60000;
        j = (j % 60000) / 1000;
        StartTime starttime1 = new StartTime();
        starttime1.setHours(Integer.valueOf(i));
        starttime1.setMinutes(Integer.valueOf(k));
        starttime1.setSeconds(Integer.valueOf(j));
        mSdlController.sdlSetMediaClockTimer(starttime, starttime1, updatemode);
    }

    private void updatePlayListSet()
    {
        int l = PlayListControl.getPlayListManager().curIndex;
        Object obj1 = PlayListControl.getPlayListManager().getPlaylist().getData();
        if (obj1 == null || ((List) (obj1)).size() == 0)
        {
            Logger.e("XmSdlService", "no play list");
            return;
        }
        if (isSamePlayList(((List) (obj1))) && isInCurrentBound(l))
        {
            Logger.e("XmSdlService", "no need update play list");
            return;
        }
        Logger.e("XmSdlService", (new StringBuilder()).append("updatePlayListSet [").append(l).append(",(").append(mPlayListLastStart).append(",").append(mPlayListLastEnd).append(")").toString());
        mLastListSize = ((List) (obj1)).size();
        Object obj = new Vector();
        mPlayListMenu.clear();
        if (obj1 != null && ((List) (obj1)).size() > 0)
        {
            Logger.e("XmSdlService", (new StringBuilder()).append("update plau list \u64AD\u653E\u5185\u5BB9 ").append(((List) (obj1)).size()).toString());
            obj1 = new ArrayList(((java.util.Collection) (obj1)));
            mPlayListLastStart = mPlayListStart;
            mPlayListLastEnd = mPlayListEnd;
            int i;
            if (l - 10 < 0)
            {
                i = 0;
            } else
            {
                i = l - 10;
            }
            mPlayListStart = i;
            mPlayListEnd = Math.min(l + 10, ((List) (obj1)).size());
            i = mPlayListStart;
            while (i < mPlayListEnd) 
            {
                SoundInfo soundinfo = (SoundInfo)((List) (obj1)).get(i);
                if (!TextUtils.isEmpty(soundinfo.title))
                {
                    Choice choice1 = new Choice();
                    int i1 = mAutoIncCorrId;
                    mAutoIncCorrId = i1 + 1;
                    choice1.setChoiceID(Integer.valueOf(i1));
                    choice1.setMenuName(subString(soundinfo.title));
                    choice1.setVrCommands(new Vector(Arrays.asList(new String[] {
                        subString(soundinfo.title)
                    })));
                    ((Vector) (obj)).add(choice1);
                    SoundMenu soundmenu1 = new SoundMenu();
                    soundmenu1.setMenuId(choice1.getChoiceID().intValue());
                    soundmenu1.setSoundInfo(soundinfo);
                    mPlayListMenu.add(soundmenu1);
                }
                i++;
            }
        }
        if (((Vector) (obj)).size() == 0)
        {
            Logger.e("XmSdlService", "update plau list \u65E0\u64AD\u653E\u5185\u5BB9");
            Choice choice = new Choice();
            choice.setMenuName("\u65E0\u64AD\u653E\u5185\u5BB9");
            int j = mAutoIncCorrId;
            mAutoIncCorrId = j + 1;
            choice.setChoiceID(Integer.valueOf(j));
            ((Vector) (obj)).add(choice);
            SoundMenu soundmenu = new SoundMenu();
            soundmenu.setMenuId(choice.getChoiceID().intValue());
            soundmenu.setSoundInfo(null);
            mPlayListMenu.add(soundmenu);
        }
        mPlayListChoiceSet.setChoiceSet(((Vector) (obj)));
        obj = mPlayListChoiceSet;
        int k = mAutoIncCorrId;
        mAutoIncCorrId = k + 1;
        ((InteractionMenu) (obj)).setChoiceSetId(k);
        mPlayListChoiceSet.setChoiceSetName("\u64AD\u653E\u5217\u8868");
        createInteractionChoiceSet(mPlayListChoiceSet);
    }

    private void updateTextAndCover(SoundInfo soundinfo)
    {
        if (soundinfo == null)
        {
            return;
        }
        String s = getCoverUrl(soundinfo);
        Logger.e("XmSdlService", (new StringBuilder()).append("updateTextAndCover coverUrl:").append(s).toString());
        if (TextUtils.isEmpty(s) || s.equals(mLastCoverUrl))
        {
            updateMainScreen(soundinfo.albumName, soundinfo.title, mCoverImage);
            updateMediaTimer(mPlayer.getCurPosition(), mPlayer.getDuration(), getUpdateMode());
            return;
        }
        if (!TextUtils.isEmpty(mLastCoverUrl))
        {
            mSdlController.sdlDeleteFile(mLastCoverUrl);
        }
        mCoverImage.setValue(SdlImageResource.APP_ICON.toString());
        updateMainScreen(soundinfo.albumName, soundinfo.title, mCoverImage);
        updateMediaTimer(mPlayer.getCurPosition(), mPlayer.getDuration(), getUpdateMode());
        mLastCoverUrl = s;
        soundinfo = new Options();
        soundinfo.targetWidth = 176;
        soundinfo.targetHeight = ((com.ximalaya.ting.android.util.ImageManager2.Options) (soundinfo)).targetWidth;
        Logger.e("XmSdlService", (new StringBuilder()).append("updateTextAndCover mLastCoverUrl:").append(mLastCoverUrl).toString());
        ImageManager2.from(mAppCtx).downloadBitmap(mLastCoverUrl, soundinfo, new _cls4());
    }

    public boolean checkSdlStatus()
    {
        if (mSdlController != null && mSdlController.isConnected())
        {
            return true;
        } else
        {
            Logger.e("XmSdlService", "checkSdlStatus closed");
            return false;
        }
    }

    public IBinder onBind(Intent intent)
    {
        return null;
    }

    public void onCreate()
    {
        onCreate();
        instance = this;
        mAppCtx = getApplicationContext();
        Logger.e("XmSdlService", "---onCreate");
    }

    public void onDestroy()
    {
        Logger.e("XmSdlService", "---onDestroy");
        releaseMenu();
        stopSDL();
        unregisteListener();
        instance = null;
        onDestroy();
    }

    public void onOnCommand(OnCommand oncommand)
    {
        int i = oncommand.getCmdID().intValue();
        Logger.e("XmSdlService", (new StringBuilder()).append("onOnCommand ").append(i).toString());
        if (i == mBtnId.btn_hot_album)
        {
            performInteraction(mHotAlbumChoiceSet.getChoiceSetId(), mHotAlbumChoiceSet.getChoiceSetName(), mHotAlbumChoiceSet.getChoiceSetName(), InteractionMode.MANUAL_ONLY);
            if (!mLoadHotAlbumSuccess || System.currentTimeMillis() - mLastLoadHotAlbumTime > 0x927c0L)
            {
                updateHotAlbum();
            }
        } else
        {
            if (i == mBtnId.btn_hot_sound)
            {
                if (!mLoadHotSoundSuccess || System.currentTimeMillis() - mLastLoadHotSoundTime > 0x927c0L)
                {
                    updateHotSound();
                }
                performInteraction(mHotSoundChoiceSet.getChoiceSetId(), mHotSoundChoiceSet.getChoiceSetName(), mHotSoundChoiceSet.getChoiceSetName(), InteractionMode.MANUAL_ONLY);
                return;
            }
            if (i == mBtnId.btn_play_list)
            {
                return;
            }
        }
    }

    public void onOnHMIStatus(OnHMIStatus onhmistatus)
    {
        static class _cls8
        {

            static final int $SwitchMap$com$smartdevicelink$proxy$rpc$enums$AudioStreamingState[];
            static final int $SwitchMap$com$smartdevicelink$proxy$rpc$enums$HMILevel[];

            static 
            {
                $SwitchMap$com$smartdevicelink$proxy$rpc$enums$HMILevel = new int[HMILevel.values().length];
                try
                {
                    $SwitchMap$com$smartdevicelink$proxy$rpc$enums$HMILevel[HMILevel.HMI_FULL.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror5) { }
                try
                {
                    $SwitchMap$com$smartdevicelink$proxy$rpc$enums$HMILevel[HMILevel.HMI_LIMITED.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror4) { }
                try
                {
                    $SwitchMap$com$smartdevicelink$proxy$rpc$enums$HMILevel[HMILevel.HMI_BACKGROUND.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror3) { }
                try
                {
                    $SwitchMap$com$smartdevicelink$proxy$rpc$enums$HMILevel[HMILevel.HMI_NONE.ordinal()] = 4;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                $SwitchMap$com$smartdevicelink$proxy$rpc$enums$AudioStreamingState = new int[AudioStreamingState.values().length];
                try
                {
                    $SwitchMap$com$smartdevicelink$proxy$rpc$enums$AudioStreamingState[AudioStreamingState.AUDIBLE.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$smartdevicelink$proxy$rpc$enums$AudioStreamingState[AudioStreamingState.NOT_AUDIBLE.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        _cls8..SwitchMap.com.smartdevicelink.proxy.rpc.enums.AudioStreamingState[onhmistatus.getAudioStreamingState().ordinal()];
        JVM INSTR tableswitch 1 2: default 32
    //                   1 73
    //                   2 80;
           goto _L1 _L2 _L3
_L1:
        break; /* Loop/switch isn't completed */
_L3:
        break MISSING_BLOCK_LABEL_80;
_L4:
        switch (_cls8..SwitchMap.com.smartdevicelink.proxy.rpc.enums.HMILevel[onhmistatus.getHmiLevel().ordinal()])
        {
        default:
            return;

        case 1: // '\001'
            Logger.e("XmSdlService", (new StringBuilder()).append("HMI_FULL, first:").append(mFirst).toString());
            ToolUtil.startMainApp(mAppCtx);
            if (mFirst)
            {
                putAlbumIcon();
                mAutoIncCorrId = 1;
                onhmistatus = SharedPreferencesUtil.getInstance(mAppCtx);
                mEnable3G = onhmistatus.getBoolean("is_download_enabled_in_3g", false);
                onhmistatus.saveBoolean("is_download_enabled_in_3g", true);
                int i = NetworkUtils.getNetType(mAppCtx);
                if (!mEnable3G && i == 0)
                {
                    alert("\u6B22\u8FCE\u4F7F\u7528\u559C\u9A6C\u62C9\u96C5", "\u4F7F\u7528\u559C\u9A6C\u62C9\u96C5\u5C06\u4F1A\u6D88\u8017\u60A8\u7684\u6570\u636E\u6D41\u91CF");
                }
                showSpeak(getResources().getString(0x7f09021c));
                if (mPlayer == null)
                {
                    mPlayer = LocalMediaService.getInstance();
                    mDownloadHander = DownloadHandler.getInstance(mAppCtx);
                    mHistoryManage = HistoryManage.getInstance(mAppCtx);
                    registeListener();
                }
                mPlayer.pause();
                mCoverImage = new Image();
                mCoverImage.setImageType(ImageType.DYNAMIC);
                mCoverImage.setValue(SdlImageResource.APP_ICON.toString());
                updateCommonSoftButton();
                addCommonCommand();
                subButtons();
                updateMainScreen(getResources().getString(0x7f09021b), getResources().getString(0x7f09021d), mCoverImage);
                updateMediaTimer(mPlayer.getCurPosition(), mPlayer.getDuration(), getUpdateMode());
                mCollectPageId = 1;
                mHotSoundPageId = 1;
                mHotAlbumPageId = 1;
                mFirst = false;
            }
            onhmistatus = PlayListControl.getPlayListManager().getCurSound();
            if (onhmistatus != null)
            {
                updateTextAndCover(onhmistatus);
                return;
            } else
            {
                updateMainScreen(getResources().getString(0x7f09021c), getResources().getString(0x7f09021d), mCoverImage);
                updateMediaTimer(mPlayer.getCurPosition(), mPlayer.getDuration(), getUpdateMode());
                return;
            }

        case 2: // '\002'
            Logger.e("XmSdlService", "HMI_LIMITED");
            return;

        case 3: // '\003'
            Logger.e("XmSdlService", "HMI_BACKGROUND");
            return;

        case 4: // '\004'
            Logger.e("XmSdlService", "HMI_NONE");
            mFirst = true;
            mCommonSoftbutton.clear();
            mLocalAlbums.clear();
            mCollectedAlbums.clear();
            mHistoryAlbums.clear();
            mHotAlbums.clear();
            mHotSounds.clear();
            mInvalidMenu.clear();
            return;
        }
_L2:
        doActionAudible();
          goto _L4
        doActionNotAudible();
          goto _L4
    }

    public int onStartCommand(Intent intent, int i, int j)
    {
        mUiHandler.removeCallbacks(mStopServiceRunnable);
        Logger.e("XmSdlService", "---onStartCommand");
        if (intent != null)
        {
            startSDL();
        }
        mUiHandler.removeCallbacks(mCheckConnectionRunnable);
        mUiHandler.postDelayed(mCheckConnectionRunnable, 60000L);
        return 1;
    }

    public void performInteraction(int i, String s, String s1, InteractionMode interactionmode)
    {
        if (!checkSdlStatus())
        {
            return;
        } else
        {
            Logger.e("XmSdlService", (new StringBuilder()).append("performInteraction,").append(s1).append(",").append(i).toString());
            mSdlController.sdlPerformInteraction(s, s1, Integer.valueOf(i), null, null, InteractionMode.BOTH, Integer.valueOf(0x186a0));
            return;
        }
    }

    public void savelog(String s)
    {
        Object obj;
        Object obj1;
        Object obj2;
        obj1 = null;
        obj2 = null;
        obj = obj1;
        File file = new File((new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/ASDL.txt").toString());
        obj = obj1;
        if (file.exists())
        {
            break MISSING_BLOCK_LABEL_56;
        }
        obj = obj1;
        file.createNewFile();
        obj = obj1;
        obj1 = new FileOutputStream(file, true);
        ((FileOutputStream) (obj1)).write("\n".getBytes());
        ((FileOutputStream) (obj1)).write((new Date()).toString().getBytes());
        ((FileOutputStream) (obj1)).write("\n".getBytes());
        ((FileOutputStream) (obj1)).write(s.getBytes());
        ((FileOutputStream) (obj1)).flush();
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_126;
        }
        ((FileOutputStream) (obj1)).close();
_L2:
        return;
        obj1;
        s = obj2;
_L5:
        obj = s;
        ((Exception) (obj1)).printStackTrace();
        if (s == null) goto _L2; else goto _L1
_L1:
        try
        {
            s.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s) { }
_L3:
        s.printStackTrace();
        return;
        s;
_L4:
        if (obj != null)
        {
            try
            {
                ((FileOutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        throw s;
        s;
          goto _L3
        s;
        obj = obj1;
          goto _L4
        Exception exception;
        exception;
        s = ((String) (obj1));
        obj1 = exception;
          goto _L5
    }

    public void showSpeak(String s)
    {
        if (checkSdlStatus());
    }

    public void startSDL()
    {
        Logger.e("XmSdlService", "---startSDL");
        if (mSdlController == null)
        {
            mSdlController = new SdlController(mAppCtx, new _cls7());
        }
        if (!mSdlController.isConnected())
        {
            mSdlController.connect(getResources().getString(0x7f090000), null, 0);
        }
    }

    public void stopSDL()
    {
        if (mSdlController == null)
        {
            return;
        } else
        {
            mSdlController.disconnect();
            mSdlController = null;
            return;
        }
    }

    public void stopService()
    {
        mUiHandler.removeCallbacks(mStopServiceRunnable);
        mUiHandler.postDelayed(mStopServiceRunnable, 5000L);
    }

    public void subButtons()
    {
        mSdlController.sdlSubscribeButton(ButtonName.OK);
        mSdlController.sdlSubscribeButton(ButtonName.SEEKLEFT);
        mSdlController.sdlSubscribeButton(ButtonName.SEEKRIGHT);
    }





/*
    static SoundInfo access$1002(SdlService sdlservice, SoundInfo soundinfo)
    {
        sdlservice.mCurrSound = soundinfo;
        return soundinfo;
    }

*/


/*
    static boolean access$102(SdlService sdlservice, boolean flag)
    {
        sdlservice.mBuffering = flag;
        return flag;
    }

*/




/*
    static String access$1202(SdlService sdlservice, String s)
    {
        sdlservice.mLastCoverUrl = s;
        return s;
    }

*/


















/*
    static int access$2802(SdlService sdlservice, int i)
    {
        sdlservice.mAutoIncCorrId = i;
        return i;
    }

*/


/*
    static int access$2808(SdlService sdlservice)
    {
        int i = sdlservice.mAutoIncCorrId;
        sdlservice.mAutoIncCorrId = i + 1;
        return i;
    }

*/


/*
    static long access$2902(SdlService sdlservice, long l)
    {
        sdlservice.mLastAlert = l;
        return l;
    }

*/



/*
    static boolean access$3002(SdlService sdlservice, boolean flag)
    {
        sdlservice.mFirst = flag;
        return flag;
    }

*/




/*
    static AlbumLoader access$3202(SdlService sdlservice, AlbumLoader albumloader)
    {
        sdlservice.mAlbumLoader = albumloader;
        return albumloader;
    }

*/











/*
    static LocalMediaService access$402(SdlService sdlservice, LocalMediaService localmediaservice)
    {
        sdlservice.mPlayer = localmediaservice;
        return localmediaservice;
    }

*/



/*
    static boolean access$4702(SdlService sdlservice, boolean flag)
    {
        sdlservice.mAlbumLoading = flag;
        return flag;
    }

*/








/*
    static boolean access$5302(SdlService sdlservice, boolean flag)
    {
        sdlservice.mLoadingCollect = flag;
        return flag;
    }

*/




/*
    static boolean access$5602(SdlService sdlservice, boolean flag)
    {
        sdlservice.mLoadCollectSuccess = flag;
        return flag;
    }

*/


/*
    static long access$5702(SdlService sdlservice, long l)
    {
        sdlservice.mLastLoadCollectTime = l;
        return l;
    }

*/



/*
    static boolean access$5902(SdlService sdlservice, boolean flag)
    {
        sdlservice.mLoadingHistory = flag;
        return flag;
    }

*/




/*
    static long access$6102(SdlService sdlservice, long l)
    {
        sdlservice.mLastLoadHistoryTime = l;
        return l;
    }

*/



/*
    static boolean access$6302(SdlService sdlservice, boolean flag)
    {
        sdlservice.mLoadingHotAlbum = flag;
        return flag;
    }

*/



/*
    static boolean access$6502(SdlService sdlservice, boolean flag)
    {
        sdlservice.mLoadHotAlbumSuccess = flag;
        return flag;
    }

*/


/*
    static long access$6602(SdlService sdlservice, long l)
    {
        sdlservice.mLastLoadHotAlbumTime = l;
        return l;
    }

*/



/*
    static boolean access$6802(SdlService sdlservice, boolean flag)
    {
        sdlservice.mLoadingHotSound = flag;
        return flag;
    }

*/



/*
    static int access$6908(SdlService sdlservice)
    {
        int i = sdlservice.mHotSoundPageId;
        sdlservice.mHotSoundPageId = i + 1;
        return i;
    }

*/


/*
    static boolean access$7002(SdlService sdlservice, boolean flag)
    {
        sdlservice.mLoadHotSoundSuccess = flag;
        return flag;
    }

*/


/*
    static int access$702(SdlService sdlservice, int i)
    {
        sdlservice.mLastIndex = i;
        return i;
    }

*/


/*
    static long access$7102(SdlService sdlservice, long l)
    {
        sdlservice.mLastLoadHotSoundTime = l;
        return l;
    }

*/





/*
    static int access$802(SdlService sdlservice, int i)
    {
        sdlservice.mCurrIndex = i;
        return i;
    }

*/


/*
    static SoundInfo access$902(SdlService sdlservice, SoundInfo soundinfo)
    {
        sdlservice.mLastSound = soundinfo;
        return soundinfo;
    }

*/

    private class _cls1
        implements com.ximalaya.ting.android.transaction.download.DownloadHandler.DownloadSoundsListener
    {

        final SdlService this$0;

        public void onTaskComplete()
        {
        }

        public void onTaskDelete()
        {
        }

        public void updateActionInfo()
        {
        }

        public void updateDownloadInfo(int i)
        {
            updateLocalAlbum();
        }

        _cls1()
        {
            this$0 = SdlService.this;
            Object();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener
    {

        final SdlService this$0;

        public void onBufferUpdated(int i)
        {
        }

        public void onLogoPlayFinished()
        {
        }

        public void onPlayCompleted()
        {
            mBuffering = false;
            updateMediaTimer(mPlayer.getCurPosition(), mPlayer.getDuration(), UpdateMode.PAUSE);
        }

        public void onPlayPaused()
        {
            mBuffering = false;
            updateMediaTimer(mPlayer.getCurPosition(), mPlayer.getDuration(), UpdateMode.PAUSE);
        }

        public void onPlayProgressUpdate(int i, int j)
        {
        }

        public void onPlayStarted()
        {
            mBuffering = false;
            SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
            if (soundinfo != null)
            {
                updateMainScreen(soundinfo.albumName, soundinfo.title, mCoverImage);
            }
            updateMediaTimer(mPlayer.getCurPosition(), mPlayer.getDuration(), getUpdateMode());
        }

        public void onPlayerBuffering(boolean flag)
        {
            mBuffering = flag;
            SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
            if (soundinfo != null)
            {
                updateMainScreen(soundinfo.albumName, soundinfo.title, mCoverImage);
            } else
            {
                Logger.e("XmSdlService", "onPlayerBuffering return sound == null");
            }
            updateMediaTimer(mPlayer.getCurPosition(), mPlayer.getDuration(), getUpdateMode());
        }

        public void onSoundPrepared(int i)
        {
        }

        public void onStartPlayLogo()
        {
        }

        _cls2()
        {
            this$0 = SdlService.this;
            Object();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
    {

        final SdlService this$0;

        public void onPlayCanceled()
        {
            updateMediaTimer(mPlayer.getCurPosition(), mPlayer.getDuration(), getUpdateMode());
        }

        public void onSoundChanged(int i)
        {
            Logger.e("XmSdlService", (new StringBuilder()).append("onSoundChanged index:").append(i).toString());
            if (!checkSdlStatus())
            {
                return;
            }
            mLastIndex = mCurrIndex;
            mLastSound = mCurrSound;
            Object obj = PlayListControl.getPlayListManager().get(i);
            mCurrSound = ((SoundInfo) (obj));
            mCurrIndex = i;
            if (obj != null)
            {
                String s = getCoverUrl(((SoundInfo) (obj)));
                if (TextUtils.isEmpty(s) || s.equals(mLastCoverUrl))
                {
                    updateMainScreen(((SoundInfo) (obj)).albumName, ((SoundInfo) (obj)).title, mCoverImage);
                    updateMediaTimer(mPlayer.getCurPosition(), mPlayer.getDuration(), getUpdateMode());
                } else
                {
                    Logger.e("XmSdlService", (new StringBuilder()).append("onSoundChange coverUrl:").append(s).toString());
                    if (!TextUtils.isEmpty(mLastCoverUrl))
                    {
                        mSdlController.sdlDeleteFile(ToolUtil.md5(mLastCoverUrl));
                    }
                    mCoverImage.setValue(SdlImageResource.APP_ICON.toString());
                    updateMainScreen(((SoundInfo) (obj)).albumName, ((SoundInfo) (obj)).title, mCoverImage);
                    updateMediaTimer(mPlayer.getCurPosition(), mPlayer.getDuration(), getUpdateMode());
                    mLastCoverUrl = s;
                    obj = new com.ximalaya.ting.android.util.ImageManager2.Options();
                    obj.targetWidth = 176;
                    obj.targetHeight = ((com.ximalaya.ting.android.util.ImageManager2.Options) (obj)).targetWidth;
                    Logger.e("XmSdlService", (new StringBuilder()).append("onSoundChanged mLastCoverUrl:").append(mLastCoverUrl).toString());
                    class _cls1
                        implements com.ximalaya.ting.android.util.ImageManager2.DisplayCallback
                    {

                        final _cls3 this$1;

                        public void onCompleteDisplay(String s1, Bitmap bitmap)
                        {
                            if (checkSdlStatus())
                            {
                                Logger.e("XmSdlService", (new StringBuilder()).append("onCompleteDisplay [").append(bitmap.getWidth()).append(",").append(bitmap.getHeight()).append("]").toString());
                                if (s1.equals(mLastCoverUrl))
                                {
                                    s1 = FileType.GRAPHIC_PNG;
                                    mSdlController.SdlPutFile(ToolUtil.md5(mLastCoverUrl), s1, SdlImageResource.getImageRawData(bitmap, s1));
                                    return;
                                }
                            }
                        }

                _cls1()
                {
                    this$1 = _cls3.this;
                    super();
                }
                    }

                    ImageManager2.from(mAppCtx).downloadBitmap(mLastCoverUrl, ((com.ximalaya.ting.android.util.ImageManager2.Options) (obj)), new _cls1());
                }
            } else
            {
                Logger.e("XmSdlService", "onSoundChange sound == null");
            }
            updatePlayListSet();
            updateHistory();
        }

        public void onSoundInfoChanged(int i, SoundInfo soundinfo)
        {
        }

        _cls3()
        {
            this$0 = SdlService.this;
            Object();
        }
    }


    private class _cls5
        implements Runnable
    {

        final SdlService this$0;

        public void run()
        {
            Boolean boolean2 = Boolean.valueOf(true);
            Boolean boolean1 = boolean2;
            if (mSdlController != null)
            {
                boolean1 = boolean2;
                if (mSdlController.isConnected())
                {
                    boolean1 = Boolean.valueOf(false);
                }
            }
            if (boolean1.booleanValue())
            {
                mUiHandler.removeCallbacks(mCheckConnectionRunnable);
                mUiHandler.removeCallbacks(mStopServiceRunnable);
                stopSelf();
            }
        }

        _cls5()
        {
            this$0 = SdlService.this;
            Object();
        }
    }


    private class _cls6
        implements Runnable
    {

        final SdlService this$0;

        public void run()
        {
            if (mSdlController == null || !mSdlController.isConnected())
            {
                mUiHandler.removeCallbacks(mCheckConnectionRunnable);
                mUiHandler.removeCallbacks(mStopServiceRunnable);
                stopSelf();
            }
        }

        _cls6()
        {
            this$0 = SdlService.this;
            Object();
        }
    }


    private class _cls4
        implements com.ximalaya.ting.android.util.ImageManager2.DisplayCallback
    {

        final SdlService this$0;

        public void onCompleteDisplay(String s, Bitmap bitmap)
        {
            if (checkSdlStatus())
            {
                Logger.e("XmSdlService", (new StringBuilder()).append("onCompleteDisplay [").append(bitmap.getWidth()).append(",").append(bitmap.getHeight()).append("]").toString());
                if (s.equals(mLastCoverUrl))
                {
                    s = FileType.GRAPHIC_PNG;
                    mSdlController.SdlPutFile(ToolUtil.md5(mLastCoverUrl), s, SdlImageResource.getImageRawData(bitmap, s));
                    return;
                }
            }
        }

        _cls4()
        {
            this$0 = SdlService.this;
            Object();
        }
    }


    private class _cls7
        implements SdlController.SdlControllerListener
    {

        final SdlService this$0;

        public void onButtonEvent(OnButtonEvent onbuttonevent)
        {
            Logger.e("XmSdlService", (new StringBuilder()).append("onButtonEvent ").append(onbuttonevent.getButtonName()).append(",").append(onbuttonevent.getButtonEventMode()).toString());
            if (onbuttonevent.getButtonEventMode() == ButtonEventMode.BUTTONDOWN)
            {
                onButtonPressPrivate(onbuttonevent);
            }
        }

        public void onButtonPress(OnButtonPress onbuttonpress)
        {
        }

        public void onCommand(OnCommand oncommand)
        {
            onOnCommand(oncommand);
        }

        public void onConnected()
        {
            Logger.e("XmSdlService", "onConnected");
            mSdlController.sdlSetIcon(SdlImageResource.APP_ICON);
            mAlbumLoader = AlbumLoader.getInstance();
            SdlConnectManager.getInstance(mAppCtx).setSdlRunning(true);
        }

        public void onCreateInteractionChoiceSetResponse(CreateInteractionChoiceSet createinteractionchoiceset, CreateInteractionChoiceSetResponse createinteractionchoicesetresponse)
        {
            Logger.e("XmSdlService", (new StringBuilder()).append("onCreateInteractionChoiceSetResponse ").append(createinteractionchoiceset.getInteractionChoiceSetID()).append(",").append(createinteractionchoicesetresponse.getSuccess()).append(",").append(createinteractionchoicesetresponse.getInfo()).toString());
            if (!createinteractionchoicesetresponse.getSuccess().booleanValue())
            {
                int i = createinteractionchoiceset.getInteractionChoiceSetID().intValue();
                if (i == mLocalAlbumChoiceSet.getChoiceSetId())
                {
                    Logger.e("XmSdlService", "-----retry local");
                    createInteractionChoiceSet(mLocalAlbumChoiceSet);
                    return;
                }
                if (i == mCollectAlbumChoiceSet.getChoiceSetId())
                {
                    Logger.e("XmSdlService", "-----retry collect");
                    createInteractionChoiceSet(mCollectAlbumChoiceSet);
                    return;
                }
                if (i == mHotAlbumChoiceSet.getChoiceSetId())
                {
                    Logger.e("XmSdlService", "-----retry history");
                    createInteractionChoiceSet(mHistoryAlbumChoiceSet);
                    return;
                }
                if (i == mPlayListChoiceSet.getChoiceSetId())
                {
                    Logger.e("XmSdlService", "-----retry playList");
                    createInteractionChoiceSet(mPlayListChoiceSet);
                    return;
                }
                if (i == mHotAlbumChoiceSet.getChoiceSetId())
                {
                    Logger.e("XmSdlService", "-----retry hotAlbum");
                    createInteractionChoiceSet(mHotAlbumChoiceSet);
                    return;
                }
                if (i == mHotSoundChoiceSet.getChoiceSetId())
                {
                    Logger.e("XmSdlService", "-----retry hotSound");
                    createInteractionChoiceSet(mHotSoundChoiceSet);
                    return;
                }
            }
        }

        public void onDeleteFile(DeleteFile deletefile, DeleteFileResponse deletefileresponse)
        {
            Logger.e("XmSdlService", (new StringBuilder()).append("onDeleteFile ").append(deletefile.getSdlFileName()).append(",").append(deletefileresponse.getSuccess()).append(",").append(deletefileresponse.getInfo()).toString());
            doActionDeleteFile(deletefileresponse.getSuccess().booleanValue(), deletefile.getSdlFileName());
        }

        public void onDeleteInteractionChoiceSetResponse(DeleteInteractionChoiceSet deleteinteractionchoiceset, DeleteInteractionChoiceSetResponse deleteinteractionchoicesetresponse)
        {
            Logger.e("XmSdlService", (new StringBuilder()).append("onDeleteInteractionChoiceSetResponse ").append(deleteinteractionchoiceset.getInteractionChoiceSetID()).append(", ").append(deleteinteractionchoicesetresponse.getSuccess()).append(", ").append(deleteinteractionchoicesetresponse.getInfo()).toString());
            int i = deleteinteractionchoiceset.getInteractionChoiceSetID().intValue();
            if (i == mLocalAlbumChoiceSet.getLastChoiceId())
            {
                Logger.e("XmSdlService", "reset local choiceSet");
                mLocalAlbumChoiceSet.resetLastChoiceId();
            } else
            {
                if (i == mHistoryAlbumChoiceSet.getLastChoiceId())
                {
                    Logger.e("XmSdlService", "reset history choiceSet");
                    mHistoryAlbumChoiceSet.resetLastChoiceId();
                    return;
                }
                if (i == mCollectAlbumChoiceSet.getLastChoiceId())
                {
                    Logger.e("XmSdlService", "reset collect choiceSet");
                    mCollectAlbumChoiceSet.resetLastChoiceId();
                    return;
                }
                if (i == mHotAlbumChoiceSet.getLastChoiceId())
                {
                    Logger.e("XmSdlService", "reset hotalbum choiceSet");
                    mHotAlbumChoiceSet.resetLastChoiceId();
                    return;
                }
                if (i == mPlayListChoiceSet.getLastChoiceId())
                {
                    Logger.e("XmSdlService", "reset playlist choiceSet");
                    mPlayListChoiceSet.resetLastChoiceId();
                    return;
                }
                if (i == mHotSoundChoiceSet.getLastChoiceId())
                {
                    Logger.e("XmSdlService", "reset hotsound choiceSet");
                    mHotSoundChoiceSet.resetLastChoiceId();
                    return;
                }
            }
        }

        public void onDisConnected()
        {
            Logger.e("XmSdlService", "onDisConnected");
            mAutoIncCorrId = 1;
            mLastAlert = 0L;
            mLastCoverUrl = null;
            mFirst = false;
            unregisteListener();
            mPlayer = null;
            if (mAlbumLoader != null)
            {
                mAlbumLoader.release();
            }
            SdlConnectManager.getInstance(mAppCtx).setSdlRunning(false);
        }

        public void onHMIStatus(OnHMIStatus onhmistatus)
        {
            Logger.e("XmSdlService", "onHMIStatus");
            onOnHMIStatus(onhmistatus);
        }

        public void onInteraction(PerformInteraction performinteraction, PerformInteractionResponse performinteractionresponse)
        {
            Logger.e("XmSdlService", (new StringBuilder()).append("onInteraction ").append(performinteractionresponse.getSuccess()).append(", ").append(performinteractionresponse.getInfo()).toString());
            if (!performinteractionresponse.getSuccess().booleanValue() || performinteraction.getInteractionChoiceSetIDList().size() <= 0) goto _L2; else goto _L1
_L1:
            int i;
            i = ((Integer)performinteraction.getInteractionChoiceSetIDList().get(0)).intValue();
            int j = performinteractionresponse.getChoiceID().intValue();
            Logger.e("XmSdlService", (new StringBuilder()).append("onInteraction choiceSetid:").append(i).append(", menuId:").append(j).toString());
            if (getInvalidMenu(j) != null)
            {
                return;
            }
            performinteraction = getMenuByMenuId(i, j);
            if (performinteraction == null)
            {
                Logger.e("XmSdlService", "onInteraction Find BaseMenu null");
                alert("\u7F51\u7EDC\u4E0D\u7ED9\u529B,\u8BF7\u7A0D\u540E\u518D\u8BD5", null);
                return;
            }
            if (!(performinteraction instanceof AlbumMenu)) goto _L4; else goto _L3
_L3:
            doActionChooseAlbum((AlbumMenu)performinteraction);
_L2:
            releaseMenu();
            return;
_L4:
            if (performinteraction instanceof SoundMenu)
            {
                if (i == mHotSoundChoiceSet.getChoiceSetId())
                {
                    doActionPlayHotSound((SoundMenu)performinteraction);
                } else
                {
                    doActionChooseSound((SoundMenu)performinteraction);
                }
            }
            if (true) goto _L2; else goto _L5
_L5:
        }

        public void onPutFile(PutFile putfile, PutFileResponse putfileresponse)
        {
            Logger.e("XmSdlService", (new StringBuilder()).append("onPutFile ").append(putfileresponse.getInfo()).toString());
            doActionPutFile(putfileresponse.getSuccess().booleanValue(), putfile.getSdlFileName());
        }

        public void onTouchEvent(TouchType touchtype, Point point)
        {
            Logger.e("XmSdlService", "onTouchEvent");
        }

        _cls7()
        {
            this$0 = SdlService.this;
            Object();
        }
    }

}
