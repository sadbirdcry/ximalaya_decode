// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import com.smartdevicelink.proxy.rpc.enums.UpdateMode;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.applink:
//            SdlService

class this._cls0
    implements com.ximalaya.ting.android.service.play.er.OnPlayerStatusUpdateListener
{

    final SdlService this$0;

    public void onBufferUpdated(int i)
    {
    }

    public void onLogoPlayFinished()
    {
    }

    public void onPlayCompleted()
    {
        SdlService.access$102(SdlService.this, false);
        SdlService.access$600(SdlService.this, SdlService.access$400(SdlService.this).getCurPosition(), SdlService.access$400(SdlService.this).getDuration(), UpdateMode.PAUSE);
    }

    public void onPlayPaused()
    {
        SdlService.access$102(SdlService.this, false);
        SdlService.access$600(SdlService.this, SdlService.access$400(SdlService.this).getCurPosition(), SdlService.access$400(SdlService.this).getDuration(), UpdateMode.PAUSE);
    }

    public void onPlayProgressUpdate(int i, int j)
    {
    }

    public void onPlayStarted()
    {
        SdlService.access$102(SdlService.this, false);
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo != null)
        {
            SdlService.access$300(SdlService.this, soundinfo.albumName, soundinfo.title, SdlService.access$200(SdlService.this));
        }
        SdlService.access$600(SdlService.this, SdlService.access$400(SdlService.this).getCurPosition(), SdlService.access$400(SdlService.this).getDuration(), SdlService.access$500(SdlService.this));
    }

    public void onPlayerBuffering(boolean flag)
    {
        SdlService.access$102(SdlService.this, flag);
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo != null)
        {
            SdlService.access$300(SdlService.this, soundinfo.albumName, soundinfo.title, SdlService.access$200(SdlService.this));
        } else
        {
            Logger.e("XmSdlService", "onPlayerBuffering return sound == null");
        }
        SdlService.access$600(SdlService.this, SdlService.access$400(SdlService.this).getCurPosition(), SdlService.access$400(SdlService.this).getDuration(), SdlService.access$500(SdlService.this));
    }

    public void onSoundPrepared(int i)
    {
    }

    public void onStartPlayLogo()
    {
    }

    ()
    {
        this$0 = SdlService.this;
        super();
    }
}
