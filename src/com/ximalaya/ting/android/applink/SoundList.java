// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.applink;

import java.util.ArrayList;
import java.util.List;

public class SoundList
{

    private int mCurrentIndex;
    private int mPageId;
    private int mPageSize;
    private List mSoundList;

    public SoundList()
    {
        mCurrentIndex = 0;
    }

    public void addSoundList(List list)
    {
        if (list == null || list.size() == 0)
        {
            return;
        }
        if (mSoundList == null)
        {
            mSoundList = new ArrayList();
        }
        mSoundList.addAll(list);
    }

    public int getCurrentIndex()
    {
        return mCurrentIndex;
    }

    public int getPageId()
    {
        return mPageId;
    }

    public int getPageSize()
    {
        return mPageSize;
    }

    public List getSoundList()
    {
        return mSoundList;
    }

    public void setCurrentIndex(int i)
    {
        mCurrentIndex = i;
    }

    public void setPageId(int i)
    {
        mPageId = i;
    }

    public void setPageSize(int i)
    {
        mPageSize = i;
    }

    public void setSoundList(List list)
    {
        mSoundList = list;
    }
}
