// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.communication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.communication:
//            AlbumDetailDTO, aa, DownLoadTools

class z extends MyAsyncTask
{

    final AlbumModel a;
    final View b;
    final boolean c;
    final ProgressDialog d;
    final DownLoadTools.OnFinishCommitTask e;
    final DownLoadTools f;

    z(DownLoadTools downloadtools, AlbumModel albummodel, View view, boolean flag, ProgressDialog progressdialog, DownLoadTools.OnFinishCommitTask onfinishcommittask)
    {
        f = downloadtools;
        a = albummodel;
        b = view;
        c = flag;
        d = progressdialog;
        e = onfinishcommittask;
        super();
    }

    protected transient String a(Void avoid[])
    {
        String s;
        RequestParams requestparams;
        avoid = "";
        s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/").append("api1").append("/download/album/").toString();
        s = (new StringBuilder()).append(s).append(a.albumId).toString();
        s = (new StringBuilder()).append(s).append("/track").toString();
        requestparams = new RequestParams();
        requestparams.put("albumId", (new StringBuilder()).append(a.albumId).append("").toString());
        if (MyApplication.b() != null) goto _L2; else goto _L1
_L1:
        avoid = null;
_L4:
        return avoid;
_L2:
        s = com.ximalaya.ting.android.b.f.a().a(s, requestparams, b, b);
        Logger.log((new StringBuilder()).append("result:").append(s).toString());
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_306;
        }
        if (!"0".equals(JSON.parseObject(s).get("ret").toString())) goto _L4; else goto _L3
_L3:
        avoid = (AlbumDetailDTO)JSON.parseObject(s, com/ximalaya/ting/android/communication/AlbumDetailDTO);
        if (c)
        {
            MyApplication.a().runOnUiThread(new aa(this));
        }
        avoid = ModelHelper.toDownloadList(avoid);
        if (avoid == null)
        {
            break MISSING_BLOCK_LABEL_309;
        }
        if (avoid.size() <= 0)
        {
            break MISSING_BLOCK_LABEL_309;
        }
        avoid = DownloadHandler.getInstance(MyApplication.b()).insertAll(avoid, c);
        if (avoid != null)
        {
            try
            {
                return avoid.a();
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(avoid.getMessage()).append(Logger.getLineInfo()).toString());
            }
        } else
        {
            return "\u52A0\u5165\u4E0B\u8F7D\u5217\u8868\u5931\u8D25";
        }
        return "\u89E3\u6790json\u5F02\u5E38";
        return "\u7F51\u7EDC\u8BF7\u6C42\u8D85\u65F6\uFF0C\u8BF7\u91CD\u8BD5";
        return "\u4E13\u8F91\u5217\u8868\u4E3A\u7A7A\uFF0C\u8BF7\u91CD\u8BD5";
    }

    protected void a(String s)
    {
        if (d != null && d.isShowing() && c)
        {
            d.setMessage(s);
            d.dismiss();
        }
        if (e != null)
        {
            e.onFinishCommit();
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((String)obj);
    }
}
