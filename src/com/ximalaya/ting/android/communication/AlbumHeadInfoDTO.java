// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.communication;


public final class AlbumHeadInfoDTO
{

    public long albumId;
    public String avatarPath;
    public int categoryId;
    public String categoryName;
    public String coverLarge;
    public String coverOrigin;
    public String coverSmall;
    public String coverWebLarge;
    public long createdAt;
    public boolean hasNew;
    public String intro;
    public String introRich;
    public boolean isFavorite;
    public boolean isVerified;
    public long lastUptrackAt;
    public String nickname;
    public int playTimes;
    public int shares;
    public int status;
    public String tags;
    public String title;
    public int tracks;
    public long uid;
    public long updatedAt;

    public AlbumHeadInfoDTO()
    {
    }
}
