// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.communication;

import android.app.Activity;
import android.app.ProgressDialog;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.sound.AlbumSoundModel;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.communication:
//            ac, DownLoadTools

class ab extends MyAsyncTask
{

    final AlbumModel a;
    final boolean b;
    final ProgressDialog c;
    final DownLoadTools d;

    ab(DownLoadTools downloadtools, AlbumModel albummodel, boolean flag, ProgressDialog progressdialog)
    {
        d = downloadtools;
        a = albummodel;
        b = flag;
        c = progressdialog;
        super();
    }

    protected transient String a(Void avoid[])
    {
        Object obj;
        RequestParams requestparams;
        avoid = "";
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/album/track").toString();
        requestparams = new RequestParams();
        requestparams.put("albumId", (new StringBuilder()).append(a.albumId).append("").toString());
        requestparams.put("albumUid", (new StringBuilder()).append(a.uid).append("").toString());
        requestparams.put("pageId", "1");
        requestparams.put("pageSize", (new StringBuilder()).append(a.tracks).append("").toString());
        if (MyApplication.b() != null) goto _L2; else goto _L1
_L1:
        avoid = null;
_L4:
        return avoid;
_L2:
        obj = f.a().a(((String) (obj)), requestparams, null, null);
        Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_332;
        }
        obj = JSON.parseObject(((String) (obj)));
        if (!"0".equals(((JSONObject) (obj)).get("ret").toString())) goto _L4; else goto _L3
_L3:
        avoid = JSON.parseArray(((JSONObject) (obj)).getJSONObject("tracks").getString("list"), com/ximalaya/ting/android/model/sound/AlbumSoundModel);
        if (b)
        {
            MyApplication.a().runOnUiThread(new ac(this));
        }
        obj = ModelHelper.toDownloadListForAlbum(avoid);
        if (avoid == null)
        {
            break MISSING_BLOCK_LABEL_335;
        }
        if (avoid.size() <= 0)
        {
            break MISSING_BLOCK_LABEL_335;
        }
        DownLoadTools.access$200(d, ((List) (obj)));
        avoid = DownloadHandler.getInstance(MyApplication.b()).insertAll(((List) (obj)), b);
        if (avoid != null)
        {
            try
            {
                return avoid.a();
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(avoid.getMessage()).append(Logger.getLineInfo()).toString());
            }
        } else
        {
            return "\u52A0\u5165\u4E0B\u8F7D\u5217\u8868\u5931\u8D25";
        }
        return "\u89E3\u6790json\u5F02\u5E38";
        return "\u7F51\u7EDC\u8BF7\u6C42\u8D85\u65F6\uFF0C\u8BF7\u91CD\u8BD5";
        return "\u4E13\u8F91\u5217\u8868\u4E3A\u7A7A\uFF0C\u8BF7\u91CD\u8BD5";
    }

    protected void a(String s)
    {
        if (c != null && c.isShowing() && b)
        {
            c.setMessage(s);
            c.dismiss();
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((String)obj);
    }
}
