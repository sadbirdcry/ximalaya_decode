// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.communication;


public final class TrackInfoBelongAlbumDTO
{

    public long albumId;
    public String albumImage;
    public String albumTitle;
    public int comments;
    public String coverLarge;
    public String coverSmall;
    public long createdAt;
    public String downloadUrl;
    public long duration;
    public boolean isLike;
    public boolean isPublic;
    public boolean isRelay;
    public int likes;
    public String nickname;
    public int opType;
    public String playPathAacv164;
    public String playPathAacv224;
    public String playUrl32;
    public String playUrl64;
    public int playtimes;
    public int processState;
    public int shares;
    public String smallLogo;
    public int status;
    public String title;
    public long trackId;
    public long uid;
    public int userSource;

    public TrackInfoBelongAlbumDTO()
    {
    }
}
