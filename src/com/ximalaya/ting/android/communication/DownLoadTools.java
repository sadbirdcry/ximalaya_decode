// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.communication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.database.DownloadTableHandler;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.MyFileUtils;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.StorageUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.communication:
//            a, l, z, ab, 
//            t, DownloadHeaderInfo, u, p, 
//            y, x, AlbumHeadInfoDTO, ad, 
//            ae, v, w, af, 
//            b, c, n, o, 
//            e, d, g, j, 
//            i

public class DownLoadTools
{
    public static interface OnFinishCommitTask
    {

        public abstract void onFinishCommit();
    }

    public static interface OnFinishSaveTaskCallback
    {

        public abstract void onFinishSaveTask();
    }

    private class a extends MyAsyncTask
    {

        Context a;
        DownloadTask b;
        boolean c;
        final DownLoadTools d;

        protected transient com.ximalaya.ting.android.a.a.a a(Object aobj[])
        {
            a = (Context)aobj[1];
            b = (DownloadTask)aobj[0];
            if (MyApplication.b() != null)
            {
                return DownloadHandler.getInstance(MyApplication.b()).insert(b, c);
            } else
            {
                return com.ximalaya.ting.android.a.a.a.b;
            }
        }

        protected void a(com.ximalaya.ting.android.a.a.a a1)
        {
            if (a1 != com.ximalaya.ting.android.a.a.a.a)
            {
                ToolUtil.showToast(a1.a());
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((com.ximalaya.ting.android.a.a.a)obj);
        }

        public a(boolean flag)
        {
            d = DownLoadTools.this;
            super();
            c = true;
            c = flag;
        }
    }

    private class b extends MyAsyncTask
    {

        Context a;
        List b;
        boolean c;
        OnFinishSaveTaskCallback d;
        final DownLoadTools e;

        protected transient com.ximalaya.ting.android.a.a.a a(Object aobj[])
        {
            a = (Context)aobj[1];
            b = (List)aobj[0];
            if (MyApplication.b() != null)
            {
                return DownloadHandler.getInstance(MyApplication.b()).insertAll(b, c);
            } else
            {
                return com.ximalaya.ting.android.a.a.a.b;
            }
        }

        protected void a(com.ximalaya.ting.android.a.a.a a1)
        {
            if (com.ximalaya.ting.android.a.a.a.a.a().equals(a1.a()) && d != null)
            {
                d.onFinishSaveTask();
            }
            e.setLoadingMsg(a1.a(), true);
            if (!TextUtils.isEmpty(a1.a()))
            {
                ToolUtil.showToast(a1.a());
                return;
            } else
            {
                ToolUtil.showToast("\u52A0\u5165\u4E0B\u8F7D\u5217\u8868\u5931\u8D25\uFF0C\u8BF7\u91CD\u8BD5");
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((com.ximalaya.ting.android.a.a.a)obj);
        }

        public b(boolean flag, OnFinishSaveTaskCallback onfinishsavetaskcallback)
        {
            e = DownLoadTools.this;
            super();
            c = true;
            c = flag;
            d = onfinishsavetaskcallback;
        }
    }

    private class c extends MyAsyncTask
    {

        DownloadHeaderInfo a;
        DownloadTask b;
        MyApplication c;
        boolean d;
        Long e;
        Long f;
        Long g;
        final DownLoadTools h;

        protected transient Integer a(Object aobj[])
        {
            c = (MyApplication)aobj[0];
            e = (Long)aobj[1];
            f = (Long)aobj[2];
            g = (Long)aobj[3];
            b = (DownloadTask)aobj[4];
            a = DownLoadTools.queryDownloadInfo(f.longValue(), g.longValue(), c, (View)aobj[5]);
            if (a == null || a.ret != 0)
            {
                return Integer.valueOf(2);
            } else
            {
                return Integer.valueOf(3);
            }
        }

        protected void a(Integer integer)
        {
            super.onPostExecute(integer);
            if (integer.intValue() == 3 && a != null && b != null)
            {
                if (android.os.Build.VERSION.SDK_INT == 10 && !TextUtils.isEmpty(b.playUrl64))
                {
                    b.downLoadUrl = b.playUrl64;
                } else
                {
                    b.downLoadUrl = a.downloadAacUrl;
                }
                b.orderNum = a.orderNum;
                b.downloadType = a.downloadType;
                b.sequnceId = a.sequnceId;
                b.uid = a.uid;
                b.nickname = a.nickname;
                b.userCoverPath = a.smallLogo;
                b.albumId = a.albumId;
                b.albumName = a.albumTitle;
                b.albumCoverPath = a.albumCoverSmall;
                b.duration = a.duration;
                b.albumHeader = new AlbumHeadInfoDTO();
                b.albumHeader.albumId = a.albumId;
                b.albumHeader.title = a.albumTitle;
                b.albumHeader.coverSmall = a.albumCoverSmall;
                b.albumHeader.uid = a.uid;
                b.albumHeader.nickname = a.nickname;
                b.albumHeader.avatarPath = a.smallLogo;
                (h. new a(d)).myexec(new Object[] {
                    b, c.getApplicationContext()
                });
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((Integer)obj);
        }

        public c(boolean flag)
        {
            h = DownLoadTools.this;
            super();
            a = null;
            b = null;
            d = true;
            e = null;
            f = null;
            g = null;
            d = flag;
        }
    }

    private class d extends MyAsyncTask
    {

        List a;
        MyApplication b;
        boolean c;
        OnFinishSaveTaskCallback d;
        final ProgressDialog e = ToolUtil.createProgressDialog(MyApplication.a(), 0, true, true);
        final DownLoadTools f;

        protected transient Integer a(Object aobj[])
        {
            int k = 0;
            b = (MyApplication)aobj[0];
            a = (List)aobj[1];
            if (a != null)
            {
                for (int i1 = a.size(); k != i1; k++)
                {
                    DownLoadTools.updateDownloadInfo((DownloadTask)a.get(k), b);
                }

            }
            return Integer.valueOf(3);
        }

        protected void a(Integer integer)
        {
            super.onPostExecute(integer);
            if (c)
            {
                f.setLoadingMsg("\u52A0\u5165\u4E0B\u8F7D\u961F\u5217\u4E2D...", false);
            }
            if (integer.intValue() == 3 && a != null && !a.isEmpty())
            {
                (f. new b(c, d)).myexec(new Object[] {
                    a, b.getApplicationContext()
                });
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((Integer)obj);
        }

        protected void onPreExecute()
        {
            if (c)
            {
                f.showLoadingDlg("\u6B63\u5728\u83B7\u53D6\u5217\u8868...");
            }
        }

        public d(boolean flag, OnFinishSaveTaskCallback onfinishsavetaskcallback)
        {
            f = DownLoadTools.this;
            super();
            a = null;
            c = true;
            c = flag;
            d = onfinishsavetaskcallback;
        }
    }


    private static volatile int count = 0;
    private static DownLoadTools dlt;
    ProgressDialog mProgressDialog;

    public DownLoadTools()
    {
    }

    public static boolean callDownloadLocationSelectDialog()
    {
        if (MyApplication.c())
        {
            SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(MyApplication.b());
            boolean flag = sharedpreferencesutil.getBoolean("is_download_locations_selector_has_called", false);
            if (!StorageUtils.isExternalSDcardAvaliable() || flag)
            {
                return false;
            } else
            {
                MyApplication.f.runOnUiThread(new com.ximalaya.ting.android.communication.a());
                sharedpreferencesutil.saveBoolean("is_download_locations_selector_has_called", true);
                return true;
            }
        } else
        {
            return false;
        }
    }

    private void continueDownload()
    {
        (new l(this)).myexec(new Void[0]);
    }

    private void downloadAlbum(AlbumModel albummodel, boolean flag, OnFinishCommitTask onfinishcommittask, View view)
    {
        if (!MyApplication.c())
        {
            return;
        }
        ProgressDialog progressdialog = ToolUtil.createProgressDialog(MyApplication.a(), 0, true, true);
        if (flag)
        {
            progressdialog.setMessage("\u6B63\u5728\u83B7\u53D6\u4E13\u8F91\u5217\u8868...");
            progressdialog.show();
        }
        (new z(this, albummodel, view, flag, progressdialog, onfinishcommittask)).myexec(new Void[0]);
    }

    private void downloadAlbum2(AlbumModel albummodel, boolean flag)
    {
        if (!MyApplication.c())
        {
            return;
        }
        ProgressDialog progressdialog = ToolUtil.createProgressDialog(MyApplication.a(), 0, true, true);
        if (flag)
        {
            progressdialog.setMessage("\u6B63\u5728\u83B7\u53D6\u4E13\u8F91\u5217\u8868...");
            progressdialog.show();
        }
        (new ab(this, albummodel, flag, progressdialog)).myexec(new Void[0]);
    }

    private void downloadAlbumSounds(List list, long l1, boolean flag, OnFinishSaveTaskCallback onfinishsavetaskcallback, View view)
    {
        if (!MyApplication.c())
        {
            return;
        }
        if (flag)
        {
            showLoadingDlg("\u6B63\u5728\u83B7\u53D6\u5217\u8868...");
        }
        (new t(this, l1, view, flag, list, onfinishsavetaskcallback)).myexec(new Void[0]);
    }

    private void downloadTemp(DownloadTask downloadtask, boolean flag, Context context, View view)
    {
        if (downloadtask == null || context == null)
        {
            return;
        }
        long l1 = downloadtask.uid;
        long l2 = downloadtask.albumId;
        long l3 = downloadtask.trackId;
        if (0L != l1 && 0L != l3)
        {
            (new c(flag)).myexec(new Object[] {
                context, Long.valueOf(l2), Long.valueOf(l1), Long.valueOf(l3), downloadtask, view
            });
            return;
        } else
        {
            Toast.makeText(context, "\u65E0\u6CD5\u83B7\u53D6\u4E0B\u8F7D\u5730\u5740\uFF0C\u8BF7\u7A0D\u540E\u91CD\u8BD5", 0).show();
            return;
        }
    }

    private void downloadTemp(List list, boolean flag, Context context, OnFinishSaveTaskCallback onfinishsavetaskcallback)
    {
        if (list == null || list.isEmpty() || context == null)
        {
            return;
        } else
        {
            (new d(flag, onfinishsavetaskcallback)).myexec(new Object[] {
                context, list
            });
            return;
        }
    }

    private List getFilterTasks(List list, List list1)
    {
        ArrayList arraylist = new ArrayList();
        int i1 = list1.size();
label0:
        for (int k = 0; k != i1; k++)
        {
            Iterator iterator = list.iterator();
            DownloadTask downloadtask;
            do
            {
                if (!iterator.hasNext())
                {
                    continue label0;
                }
                downloadtask = (DownloadTask)iterator.next();
            } while (((DownloadTask)list1.get(k)).trackId != downloadtask.trackId);
            arraylist.add(downloadtask);
        }

        return arraylist;
    }

    public static DownLoadTools getInstance()
    {
        count++;
        if (dlt == null)
        {
            dlt = new DownLoadTools();
        }
        return dlt;
    }

    public static int hasDownloadCompleted(Context context, SoundInfo soundinfo)
    {
        return (new DownloadTableHandler(context)).hasDownloadCompleted(soundinfo.trackId);
    }

    public static DownloadHeaderInfo queryDownloadInfo(long l1, long l2, Context context, View view)
    {
        context = new DownloadHeaderInfo();
        RequestParams requestparams = new RequestParams();
        requestparams.put("uid", String.valueOf(l1));
        requestparams.put("trackId", String.valueOf(l2));
        String s = ApiUtil.getApiHost() + "mobile/download/" + l1 + "/track/" + l2;
        view = f.a().a(s, requestparams, view, null);
        try
        {
            view = (DownloadHeaderInfo)JSON.parseObject(view, com/ximalaya/ting/android/communication/DownloadHeaderInfo);
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(view.getMessage()).append(Logger.getLineInfo()).toString());
            return context;
        }
        return view;
    }

    private void setLoadingMsg(String s, boolean flag)
    {
        if (MyApplication.a() != null)
        {
            MyApplication.a().runOnUiThread(new u(this, s, flag));
        }
    }

    private void setOrderNumber(List list)
    {
        list = list.iterator();
        for (int k = 0; list.hasNext(); k++)
        {
            ((DownloadTask)list.next()).orderNum = k;
        }

    }

    public static final void showDialogOnDownloadFileMiss(SoundInfo soundinfo)
    {
        if (!MyFileUtils.isSDcardPrepared())
        {
            if (MyFileUtils.isSDcardInvalid())
            {
                if (MyApplication.c())
                {
                    Toast.makeText(MyApplication.f, (new StringBuilder()).append("\u68C0\u6D4B\u4E0D\u5230\u50A8\u5B58\u5361\uFF0C\u58F0\u97F3\u6587\u4EF6[").append(soundinfo.title).append("]\u65E0\u6CD5\u8BFB\u53D6").toString(), 1).show();
                    return;
                } else
                {
                    soundinfo = (new StringBuilder()).append("\u68C0\u6D4B\u4E0D\u5230\u50A8\u5B58\u5361\uFF0C\u58F0\u97F3\u6587\u4EF6[").append(soundinfo.title).append("]\u65E0\u6CD5\u8BFB\u53D6").toString();
                    ToolUtil.makePlayNotification(MyApplication.b(), "\u559C\u9A6C\u62C9\u96C5", "\u6E29\u99A8\u63D0\u793A", soundinfo);
                    return;
                }
            }
            if (MyApplication.c())
            {
                Toast.makeText(MyApplication.f, "\u624B\u673A\u6B63\u5728\u8BFB\u53D6\u50A8\u5B58\u5361\u4E2D\u58F0\u97F3\u6587\u4EF6\uFF0C\u8BF7\u7A0D\u7B49...", 1).show();
                return;
            } else
            {
                ToolUtil.makePlayNotification(MyApplication.b(), "\u559C\u9A6C\u62C9\u96C5", "\u6E29\u99A8\u63D0\u793A", "\u624B\u673A\u6B63\u5728\u8BFB\u53D6\u50A8\u5B58\u5361\u4E2D\u58F0\u97F3\u6587\u4EF6\uFF0C\u8BF7\u7A0D\u7B49...");
                return;
            }
        }
        if (MyApplication.f != null)
        {
            MyApplication.f.runOnUiThread(new p(soundinfo));
            return;
        } else
        {
            soundinfo = (new StringBuilder()).append("\u60A8\u4E0B\u8F7D\u7684[").append(soundinfo.title).append("]\u7684\u672C\u5730\u6587\u4EF6\u5DF2\u88AB\u5220\u9664\uFF0C\u65E0\u6CD5\u64AD\u653E").toString();
            ToolUtil.makePlayNotification(MyApplication.b(), "\u559C\u9A6C\u62C9\u96C5", "\u6E29\u99A8\u63D0\u793A", soundinfo);
            return;
        }
    }

    public static boolean showDownloadLocationSelectDialog()
    {
        if (MyApplication.b() != null)
        {
            SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(MyApplication.b());
            boolean flag = sharedpreferencesutil.getBoolean("is_download_locations_selector_has_called", false);
            if (StorageUtils.isExternalSDcardAvaliable() && !flag)
            {
                (new DialogBuilder(MyApplication.f)).setMessage("\u68C0\u6D4B\u5230\u60A8\u7684\u624B\u673A\u6B63\u5728\u4F7F\u7528\u5916\u7F6E\u5B58\u50A8\u5361, \u662F\u5426\u5C06\u58F0\u97F3\u4E0B\u8F7D\u81F3\u5916\u7F6E\u5B58\u50A8\u5361\uFF1F").setOkBtn(new y()).setCancelBtn(new x()).showConfirm();
                sharedpreferencesutil.saveBoolean("is_download_locations_selector_has_called", true);
                return true;
            }
        }
        return false;
    }

    private void showLoadingDlg(String s)
    {
        Activity activity = MyApplication.a();
        if (activity == null || activity.isFinishing())
        {
            return;
        }
        if (mProgressDialog == null || !mProgressDialog.isShowing())
        {
            mProgressDialog = ToolUtil.createProgressDialog(MyApplication.a(), 0, true, true);
        }
        mProgressDialog.setMessage(s);
        mProgressDialog.show();
    }

    public static void updateDownloadInfo(DownloadTask downloadtask, Context context)
    {
        context = queryDownloadInfo(downloadtask.uid, downloadtask.trackId, context, null);
        if (context != null && ((DownloadHeaderInfo) (context)).ret == 0)
        {
            if (android.os.Build.VERSION.SDK_INT == 10 && !TextUtils.isEmpty(downloadtask.playUrl64))
            {
                downloadtask.downLoadUrl = downloadtask.playUrl64;
            } else
            {
                downloadtask.downLoadUrl = ((DownloadHeaderInfo) (context)).downloadAacUrl;
            }
            downloadtask.orderNum = ((DownloadHeaderInfo) (context)).orderNum;
            downloadtask.downloadType = ((DownloadHeaderInfo) (context)).downloadType;
            downloadtask.sequnceId = ((DownloadHeaderInfo) (context)).sequnceId;
            downloadtask.uid = ((DownloadHeaderInfo) (context)).uid;
            downloadtask.nickname = ((DownloadHeaderInfo) (context)).nickname;
            downloadtask.userCoverPath = ((DownloadHeaderInfo) (context)).smallLogo;
            downloadtask.albumId = ((DownloadHeaderInfo) (context)).albumId;
            downloadtask.albumName = ((DownloadHeaderInfo) (context)).albumTitle;
            downloadtask.albumCoverPath = ((DownloadHeaderInfo) (context)).albumCoverSmall;
            downloadtask.duration = ((DownloadHeaderInfo) (context)).duration;
            downloadtask.albumHeader = new AlbumHeadInfoDTO();
            downloadtask.albumHeader.albumId = ((DownloadHeaderInfo) (context)).albumId;
            downloadtask.albumHeader.title = ((DownloadHeaderInfo) (context)).albumTitle;
            downloadtask.albumHeader.coverSmall = ((DownloadHeaderInfo) (context)).albumCoverSmall;
            downloadtask.albumHeader.uid = ((DownloadHeaderInfo) (context)).uid;
            downloadtask.albumHeader.nickname = ((DownloadHeaderInfo) (context)).nickname;
            downloadtask.albumHeader.avatarPath = ((DownloadHeaderInfo) (context)).smallLogo;
        }
    }

    public void downloadStatistics(DownloadTask downloadtask, Context context)
    {
        Object obj = null;
        if (downloadtask != null && context != null && !Utilities.isBlank(downloadtask.sequnceId) && downloadtask.trackId > 0L && downloadtask.downloadType >= 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        new BaseModel();
        context = new RequestParams();
        context.put("sequenceId", downloadtask.sequnceId);
        context.put("trackId", String.valueOf(downloadtask.trackId));
        context.put("downloadType", String.valueOf(downloadtask.downloadType));
        Object obj1 = ApiUtil.getApiHost() + "mobile/download/record";
        obj1 = f.a().a(((String) (obj1)), context, null);
        context = obj;
        if (((com.ximalaya.ting.android.b.n.a) (obj1)).b == 1)
        {
            context = ((com.ximalaya.ting.android.b.n.a) (obj1)).a;
        }
        if (((BaseModel)JSON.parseObject(context, com/ximalaya/ting/android/model/BaseModel)).ret != 0) goto _L1; else goto _L3
_L3:
        Logger.log("downloadTJ", (new StringBuilder()).append("==\u4E0B\u8F7D\u7EDF\u8BA1\u56DE\u4F20==").append(downloadtask.title).toString());
        return;
        downloadtask;
        Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(downloadtask.getMessage()).append(Logger.getLineInfo()).toString());
        return;
    }

    public com.ximalaya.ting.android.a.a.a goDownload(DownloadTask downloadtask, Context context, View view)
    {
        this;
        JVM INSTR monitorenter ;
        if (StorageUtils.isInternalSDcardAvaliable() || StorageUtils.isExternalSDcardAvaliable()) goto _L2; else goto _L1
_L1:
        CustomToast.showToast(context, com.ximalaya.ting.android.a.a.a.f.a(), 200);
        downloadtask = com.ximalaya.ting.android.a.a.a.f;
_L3:
        this;
        JVM INSTR monitorexit ;
        return downloadtask;
_L2:
        DownloadHandler downloadhandler;
        int k;
        callDownloadLocationSelectDialog();
        downloadhandler = DownloadHandler.getInstance(context);
        k = NetworkUtils.getNetType(context);
        if (-1 != k)
        {
            break MISSING_BLOCK_LABEL_74;
        }
        CustomToast.showToast(context, 0x7f09017c, 200);
        downloadtask = com.ximalaya.ting.android.a.a.a.d;
          goto _L3
label0:
        {
            if (!downloadhandler.isInDownloadList(downloadtask.trackId))
            {
                break label0;
            }
            CustomToast.showToast(context, 0x7f09017e, 200);
            downloadtask = com.ximalaya.ting.android.a.a.a.c;
        }
          goto _L3
        boolean flag = SharedPreferencesUtil.getInstance(context).getBoolean("is_download_enabled_in_3g", false);
        if (1 != k && !flag)
        {
            break MISSING_BLOCK_LABEL_152;
        }
        CustomToast.showToast(context, 0x7f09017f, 200);
        downloadTemp(downloadtask, true, context, view);
_L4:
        downloadtask = com.ximalaya.ting.android.a.a.a.a;
          goto _L3
        NetworkUtils.showChangeNetWorkSetConfirm(new ad(this, downloadtask, context, view), new ae(this, downloadtask, context, view));
          goto _L4
        downloadtask;
        throw downloadtask;
        downloadtask;
        this;
        JVM INSTR monitorexit ;
        throw downloadtask;
    }

    public void goDownload(List list, Context context)
    {
        this;
        JVM INSTR monitorenter ;
        goDownload(list, context, ((OnFinishSaveTaskCallback) (null)));
        this;
        JVM INSTR monitorexit ;
        return;
        list;
        throw list;
    }

    public void goDownload(List list, Context context, OnFinishSaveTaskCallback onfinishsavetaskcallback)
    {
        this;
        JVM INSTR monitorenter ;
        if (StorageUtils.isInternalSDcardAvaliable() || StorageUtils.isExternalSDcardAvaliable()) goto _L2; else goto _L1
_L1:
        CustomToast.showToast(context, com.ximalaya.ting.android.a.a.a.f.a(), 200);
_L7:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        DownloadHandler downloadhandler;
        int i1;
        callDownloadLocationSelectDialog();
        downloadhandler = DownloadHandler.getInstance(context);
        i1 = NetworkUtils.getNetType(context);
        boolean flag;
        int k;
        int j1;
        boolean flag1;
        if (-1 == i1)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        j1 = list.size();
        k = 0;
_L8:
        if (k == j1)
        {
            break MISSING_BLOCK_LABEL_214;
        }
        if (!downloadhandler.isInDownloadList(((DownloadTask)list.get(k)).trackId)) goto _L4; else goto _L3
_L3:
        k = 1;
_L9:
        if (!flag) goto _L6; else goto _L5
_L5:
        CustomToast.showToast(context, 0x7f09017c, 200);
          goto _L7
        list;
        throw list;
        list;
        this;
        JVM INSTR monitorexit ;
        throw list;
_L4:
        k++;
          goto _L8
_L6:
        if (k == 0)
        {
            break MISSING_BLOCK_LABEL_151;
        }
        CustomToast.showToast(context, 0x7f09017e, 200);
          goto _L7
        flag1 = SharedPreferencesUtil.getInstance(context).getBoolean("is_download_enabled_in_3g", false);
        if (1 != i1 && !flag1)
        {
            break MISSING_BLOCK_LABEL_186;
        }
        downloadTemp(list, true, context, onfinishsavetaskcallback);
          goto _L7
        NetworkUtils.showChangeNetWorkSetConfirm(new v(this, list, context, onfinishsavetaskcallback), new w(this, list, context, onfinishsavetaskcallback));
          goto _L7
        k = 0;
          goto _L9
    }

    public void goDownloadAlbum(AlbumModel albummodel)
    {
        this;
        JVM INSTR monitorenter ;
        goDownloadAlbum(albummodel, null, null);
        this;
        JVM INSTR monitorexit ;
        return;
        albummodel;
        throw albummodel;
    }

    public void goDownloadAlbum(AlbumModel albummodel, OnFinishCommitTask onfinishcommittask, View view)
    {
        this;
        JVM INSTR monitorenter ;
        if (albummodel == null) goto _L2; else goto _L1
_L1:
        boolean flag = MyApplication.c();
        if (flag) goto _L3; else goto _L2
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
_L3:
        if (MyApplication.b() == null) goto _L2; else goto _L4
_L4:
        int k = NetworkUtils.getNetType(MyApplication.b());
        if (-1 != k)
        {
            break MISSING_BLOCK_LABEL_62;
        }
        ToolUtil.showToast(MyApplication.f.getResources().getString(0x7f09017c));
          goto _L2
        albummodel;
        throw albummodel;
label0:
        {
            if (StorageUtils.isInternalSDcardAvaliable() || StorageUtils.isExternalSDcardAvaliable())
            {
                break label0;
            }
            ToolUtil.showToast(com.ximalaya.ting.android.a.a.a.f.a());
        }
          goto _L2
        flag = SharedPreferencesUtil.getInstance(MyApplication.b()).getBoolean("is_download_enabled_in_3g", false);
        if (1 != k && !flag)
        {
            break MISSING_BLOCK_LABEL_174;
        }
        (new DialogBuilder(MyApplication.a())).setMessage((new StringBuilder()).append("\u8981\u4E0B\u8F7D\u5168\u90E8").append(albummodel.tracks).append("\u96C6\u4E48\uFF1F").toString()).setOkBtn(new af(this, albummodel, onfinishcommittask, view)).showConfirm();
          goto _L2
        NetworkUtils.showChangeNetWorkSetConfirm(new com.ximalaya.ting.android.communication.b(this, albummodel, onfinishcommittask, view), new com.ximalaya.ting.android.communication.c(this, albummodel, onfinishcommittask, view));
          goto _L2
    }

    public void goDownloadAlbumSounds(List list, long l1)
    {
        this;
        JVM INSTR monitorenter ;
        goDownloadAlbumSounds(list, l1, null, null);
        this;
        JVM INSTR monitorexit ;
        return;
        list;
        throw list;
    }

    public void goDownloadAlbumSounds(List list, long l1, OnFinishSaveTaskCallback onfinishsavetaskcallback, View view)
    {
        this;
        JVM INSTR monitorenter ;
        boolean flag = MyApplication.c();
        if (flag) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (MyApplication.b() == null) goto _L1; else goto _L3
_L3:
        int k = NetworkUtils.getNetType(MyApplication.b());
        if (-1 != k)
        {
            break MISSING_BLOCK_LABEL_58;
        }
        ToolUtil.showToast(MyApplication.f.getResources().getString(0x7f09017c));
          goto _L1
        list;
        throw list;
label0:
        {
            if (StorageUtils.isInternalSDcardAvaliable() || StorageUtils.isExternalSDcardAvaliable())
            {
                break label0;
            }
            ToolUtil.showToast(com.ximalaya.ting.android.a.a.a.f.a());
        }
          goto _L1
        flag = SharedPreferencesUtil.getInstance(MyApplication.b()).getBoolean("is_download_enabled_in_3g", false);
        if (1 != k && !flag)
        {
            break MISSING_BLOCK_LABEL_122;
        }
        downloadAlbumSounds(list, l1, true, onfinishsavetaskcallback, view);
          goto _L1
        NetworkUtils.showChangeNetWorkSetConfirm(new n(this, list, l1, onfinishsavetaskcallback, view), new o(this, list, l1, onfinishsavetaskcallback, view));
          goto _L1
    }

    public void release()
    {
        int k = count - 1;
        count = k;
        if (k == 0)
        {
            dlt = null;
        }
    }

    public void resume(DownloadTask downloadtask)
    {
        this;
        JVM INSTR monitorenter ;
        Object obj = MyApplication.a();
        if (obj != null) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        int k;
        obj = MyApplication.a();
        k = NetworkUtils.getNetType(((Context) (obj)));
        if (-1 != k)
        {
            break MISSING_BLOCK_LABEL_50;
        }
        ToolUtil.showToast(MyApplication.f.getResources().getString(0x7f09017c));
          goto _L1
        downloadtask;
        throw downloadtask;
        if (1 != k)
        {
            break MISSING_BLOCK_LABEL_72;
        }
        obj = DownloadHandler.getInstance(((Context) (obj)));
        if (obj == null) goto _L1; else goto _L3
_L3:
        ((DownloadHandler) (obj)).resume(downloadtask);
          goto _L1
        if (!SharedPreferencesUtil.getInstance(MyApplication.b()).getBoolean("is_download_enabled_in_3g", false))
        {
            break MISSING_BLOCK_LABEL_105;
        }
        obj = DownloadHandler.getInstance(((Context) (obj)));
        if (obj == null) goto _L1; else goto _L4
_L4:
        ((DownloadHandler) (obj)).resume(downloadtask);
          goto _L1
        (new DialogBuilder(((Context) (obj)))).setMessage(0x7f090150).setTitle("\u6D41\u91CF\u63D0\u9192").setOkBtn("\u603B\u662F\u5141\u8BB8", new e(this, ((Activity) (obj)), downloadtask)).setNeutralBtn("\u5141\u8BB8\u672C\u6B21", new com.ximalaya.ting.android.communication.d(this, ((Activity) (obj)), downloadtask)).showMultiButton();
          goto _L1
    }

    public void resumeAll()
    {
        this;
        JVM INSTR monitorenter ;
        Object obj = MyApplication.a();
        if (obj != null) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        int k;
        obj = MyApplication.a();
        k = NetworkUtils.getNetType(((Context) (obj)));
        if (-1 != k)
        {
            break MISSING_BLOCK_LABEL_50;
        }
        ToolUtil.showToast(MyApplication.f.getResources().getString(0x7f09017c));
          goto _L1
        obj;
        throw obj;
        if (1 != k)
        {
            break MISSING_BLOCK_LABEL_75;
        }
        (new g(this, ((Activity) (obj)))).myexec(new Void[0]);
          goto _L1
label0:
        {
            if (!SharedPreferencesUtil.getInstance(MyApplication.b()).getBoolean("is_download_enabled_in_3g", false))
            {
                break label0;
            }
            continueDownload();
        }
          goto _L1
        (new DialogBuilder(((Context) (obj)))).setMessage(0x7f090150).setTitle("\u6D41\u91CF\u63D0\u9192").setOkBtn("\u603B\u662F\u5141\u8BB8", new j(this)).setNeutralBtn("\u5141\u8BB8\u672C\u6B21", new i(this)).showMultiButton();
          goto _L1
    }










}
