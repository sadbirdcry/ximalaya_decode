// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.communication;

import android.text.TextUtils;
import android.view.View;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.communication:
//            DownLoadTools, DownloadHeaderInfo, AlbumHeadInfoDTO

private class d extends MyAsyncTask
{

    DownloadHeaderInfo a;
    DownloadTask b;
    MyApplication c;
    boolean d;
    Long e;
    Long f;
    Long g;
    final DownLoadTools h;

    protected transient Integer a(Object aobj[])
    {
        c = (MyApplication)aobj[0];
        e = (Long)aobj[1];
        f = (Long)aobj[2];
        g = (Long)aobj[3];
        b = (DownloadTask)aobj[4];
        a = DownLoadTools.queryDownloadInfo(f.longValue(), g.longValue(), c, (View)aobj[5]);
        if (a == null || a.ret != 0)
        {
            return Integer.valueOf(2);
        } else
        {
            return Integer.valueOf(3);
        }
    }

    protected void a(Integer integer)
    {
        super.onPostExecute(integer);
        if (integer.intValue() == 3 && a != null && b != null)
        {
            if (android.os.K_INT == 10 && !TextUtils.isEmpty(b.playUrl64))
            {
                b.downLoadUrl = b.playUrl64;
            } else
            {
                b.downLoadUrl = a.downloadAacUrl;
            }
            b.orderNum = a.orderNum;
            b.downloadType = a.downloadType;
            b.sequnceId = a.sequnceId;
            b.uid = a.uid;
            b.nickname = a.nickname;
            b.userCoverPath = a.smallLogo;
            b.albumId = a.albumId;
            b.albumName = a.albumTitle;
            b.albumCoverPath = a.albumCoverSmall;
            b.duration = a.duration;
            b.albumHeader = new AlbumHeadInfoDTO();
            b.albumHeader.albumId = a.albumId;
            b.albumHeader.title = a.albumTitle;
            b.albumHeader.coverSmall = a.albumCoverSmall;
            b.albumHeader.uid = a.uid;
            b.albumHeader.nickname = a.nickname;
            b.albumHeader.avatarPath = a.smallLogo;
            (new <init>(h, d)).myexec(new Object[] {
                b, c.getApplicationContext()
            });
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a(aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((Integer)obj);
    }

    public fo(DownLoadTools downloadtools, boolean flag)
    {
        h = downloadtools;
        super();
        a = null;
        b = null;
        d = true;
        e = null;
        f = null;
        g = null;
        d = flag;
    }
}
