// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.communication;

import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.communication:
//            AlbumDetailDTO, DownLoadTools

class t extends MyAsyncTask
{

    final long a;
    final View b;
    final boolean c;
    final List d;
    final DownLoadTools.OnFinishSaveTaskCallback e;
    final DownLoadTools f;

    t(DownLoadTools downloadtools, long l, View view, boolean flag, List list, DownLoadTools.OnFinishSaveTaskCallback onfinishsavetaskcallback)
    {
        f = downloadtools;
        a = l;
        b = view;
        c = flag;
        d = list;
        e = onfinishsavetaskcallback;
        super();
    }

    protected transient String a(Void avoid[])
    {
        Object obj;
        avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/").append("api1").append("/download/album/").toString();
        avoid = (new StringBuilder()).append(avoid).append(a).toString();
        avoid = (new StringBuilder()).append(avoid).append("/track").toString();
        obj = new RequestParams();
        ((RequestParams) (obj)).put("albumId", (new StringBuilder()).append(a).append("").toString());
        if (MyApplication.b() == null)
        {
            return null;
        }
        obj = com.ximalaya.ting.android.b.f.a().a(avoid, ((RequestParams) (obj)), b, b);
        Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_332;
        }
        avoid = JSON.parseObject(((String) (obj)));
        String s = avoid.get("ret").toString();
        avoid = avoid.getString("msg").toString();
        if (!"0".equals(s))
        {
            break MISSING_BLOCK_LABEL_294;
        }
        obj = (AlbumDetailDTO)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/communication/AlbumDetailDTO);
        if (c && MyApplication.a() != null)
        {
            DownLoadTools.access$000(f, "\u52A0\u5165\u4E0B\u8F7D\u961F\u5217\u4E2D...", false);
        }
        obj = ModelHelper.toDownloadList(((AlbumDetailDTO) (obj)));
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_335;
        }
        if (((List) (obj)).size() <= 0)
        {
            break MISSING_BLOCK_LABEL_335;
        }
        try
        {
            obj = DownloadHandler.getInstance(MyApplication.b()).insertAll(DownLoadTools.access$700(f, ((List) (obj)), d), c);
        }
        // Misplaced declaration of an exception variable
        catch (Void avoid[])
        {
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(avoid.getMessage()).append(Logger.getLineInfo()).toString());
            return "\u89E3\u6790json\u5F02\u5E38";
        }
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_284;
        }
        return ((com.ximalaya.ting.android.a.a.a) (obj)).a();
        if (TextUtils.isEmpty(avoid))
        {
            return "\u7F51\u7EDC\u9519\u8BEF\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
        } else
        {
            return avoid;
        }
        return "\u7F51\u7EDC\u9519\u8BEF\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
        return "\u7F51\u7EDC\u8BF7\u6C42\u8D85\u65F6\uFF0C\u8BF7\u91CD\u8BD5";
        return "\u4E13\u8F91\u5217\u8868\u4E3A\u7A7A\uFF0C\u8BF7\u91CD\u8BD5";
    }

    protected void a(String s)
    {
label0:
        {
            if (c)
            {
                if (!com.ximalaya.ting.android.a.a.a.a.a().equals(s))
                {
                    break label0;
                }
                if (e != null)
                {
                    e.onFinishSaveTask();
                }
                DownLoadTools.access$000(f, s, true);
                ToolUtil.showToast(s);
            }
            return;
        }
        String s1 = s;
        if (TextUtils.isEmpty(s))
        {
            s1 = "\u52A0\u5165\u4E0B\u8F7D\u5217\u8868\u5931\u8D25\uFF0C\u8BF7\u91CD\u8BD5";
        }
        DownLoadTools.access$000(f, s1, true);
        ToolUtil.showToast(s1);
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((String)obj);
    }
}
