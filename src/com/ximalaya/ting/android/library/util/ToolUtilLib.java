// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import java.io.File;

public class ToolUtilLib
{

    private static String channel = null;
    private static int screenWidth = 0;

    public ToolUtilLib()
    {
    }

    public static boolean checkSdcard()
    {
        return "mounted".equals(Environment.getExternalStorageState()) && Environment.getExternalStorageDirectory().canWrite();
    }

    public static int dp2px(Context context, float f)
    {
        return dp2px(context.getResources(), f);
    }

    public static int dp2px(Resources resources, float f)
    {
        return (int)(resources.getDisplayMetrics().density * f + 0.5F);
    }

    public static String getChannel(Context context)
    {
        if (channel == null)
        {
            try
            {
                channel = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("UMENG_CHANNEL");
            }
            // Misplaced declaration of an exception variable
            catch (Context context) { }
        }
        return channel;
    }

    public static int getScreenWidth(Context context)
    {
        if (screenWidth == 0)
        {
            if (context == null)
            {
                return 0;
            }
            screenWidth = ((WindowManager)context.getSystemService("window")).getDefaultDisplay().getWidth();
        }
        return screenWidth;
    }

    public static void setVisibility(View view, int i)
    {
        while (view == null || i != 8 && i != 0 && i != 4) 
        {
            return;
        }
        if (view instanceof ViewGroup)
        {
            ViewGroup viewgroup = (ViewGroup)view;
            int k = viewgroup.getChildCount();
            if (k != 0)
            {
                for (int j = 0; j < k; j++)
                {
                    View view1 = viewgroup.getChildAt(j);
                    if (view1 != null)
                    {
                        view1.setVisibility(i);
                    }
                }

            }
        }
        view.setVisibility(i);
    }

}
