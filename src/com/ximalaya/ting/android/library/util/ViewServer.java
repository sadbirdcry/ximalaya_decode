// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewDebug;
import android.view.Window;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ViewServer
    implements Runnable
{
    private static class a extends ViewServer
    {

        public void addWindow(Activity activity)
        {
        }

        public void addWindow(View view, String s)
        {
        }

        public boolean isRunning()
        {
            return false;
        }

        public void removeWindow(Activity activity)
        {
        }

        public void removeWindow(View view)
        {
        }

        public void run()
        {
        }

        public void setFocusedWindow(Activity activity)
        {
        }

        public void setFocusedWindow(View view)
        {
        }

        public boolean start()
            throws IOException
        {
            return false;
        }

        public boolean stop()
        {
            return false;
        }

        private a()
        {
        }

    }

    private static class b extends OutputStream
    {

        private final OutputStream a;

        public void close()
            throws IOException
        {
        }

        public boolean equals(Object obj)
        {
            return a.equals(obj);
        }

        public void flush()
            throws IOException
        {
            a.flush();
        }

        public int hashCode()
        {
            return a.hashCode();
        }

        public String toString()
        {
            return a.toString();
        }

        public void write(int i)
            throws IOException
        {
            a.write(i);
        }

        public void write(byte abyte0[])
            throws IOException
        {
            a.write(abyte0);
        }

        public void write(byte abyte0[], int i, int j)
            throws IOException
        {
            a.write(abyte0, i, j);
        }

        b(OutputStream outputstream)
        {
            a = outputstream;
        }
    }

    private class c
        implements d, Runnable
    {

        final ViewServer a;
        private Socket b;
        private boolean c;
        private boolean d;
        private final Object e[] = new Object[0];

        private View a(int i)
        {
            if (i != -1)
            {
                break MISSING_BLOCK_LABEL_57;
            }
            a.mWindowsLock.readLock().lock();
            Object obj = a.mFocusedWindow;
            a.mWindowsLock.readLock().unlock();
            return ((View) (obj));
            Exception exception;
            exception;
            a.mWindowsLock.readLock().unlock();
            throw exception;
            a.mWindowsLock.readLock().lock();
            exception = a.mWindows.entrySet().iterator();
            java.util.Map.Entry entry;
            do
            {
                if (!exception.hasNext())
                {
                    break MISSING_BLOCK_LABEL_143;
                }
                entry = (java.util.Map.Entry)exception.next();
            } while (System.identityHashCode(entry.getKey()) != i);
            exception = (View)entry.getKey();
            a.mWindowsLock.readLock().unlock();
            return exception;
            a.mWindowsLock.readLock().unlock();
            return null;
            exception;
            a.mWindowsLock.readLock().unlock();
            throw exception;
        }

        private boolean a(Socket socket)
        {
            Object obj = null;
            a.mWindowsLock.readLock().lock();
            socket = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()), 8192);
            for (obj = a.mWindows.entrySet().iterator(); ((Iterator) (obj)).hasNext(); socket.write(10))
            {
                java.util.Map.Entry entry = (java.util.Map.Entry)((Iterator) (obj)).next();
                socket.write(Integer.toHexString(System.identityHashCode(entry.getKey())));
                socket.write(32);
                socket.append((CharSequence)entry.getValue());
            }

              goto _L1
            obj;
_L4:
            a.mWindowsLock.readLock().unlock();
            if (socket != null)
            {
                try
                {
                    socket.close();
                }
                // Misplaced declaration of an exception variable
                catch (Socket socket)
                {
                    return false;
                }
                return false;
            } else
            {
                return false;
            }
_L1:
            socket.write("DONE.\n");
            socket.flush();
            a.mWindowsLock.readLock().unlock();
            if (socket != null)
            {
                try
                {
                    socket.close();
                }
                // Misplaced declaration of an exception variable
                catch (Socket socket)
                {
                    return false;
                }
                return true;
            } else
            {
                return true;
            }
            obj;
            socket = null;
_L3:
            a.mWindowsLock.readLock().unlock();
            if (socket != null)
            {
                try
                {
                    socket.close();
                }
                // Misplaced declaration of an exception variable
                catch (Socket socket) { }
            }
            throw obj;
            obj;
            if (true) goto _L3; else goto _L2
_L2:
            socket;
            socket = ((Socket) (obj));
              goto _L4
        }

        private boolean a(Socket socket, String s, String s1)
        {
            Object obj;
            Object obj1;
            Object obj2;
            Object obj3;
            boolean flag1;
            flag1 = true;
            obj3 = null;
            obj2 = null;
            obj1 = s1;
            obj = obj3;
            int j = s1.indexOf(' ');
            int i;
            i = j;
            if (j != -1)
            {
                break MISSING_BLOCK_LABEL_47;
            }
            obj1 = s1;
            obj = obj3;
            i = s1.length();
            obj1 = s1;
            obj = obj3;
            j = (int)Long.parseLong(s1.substring(0, i), 16);
            obj1 = s1;
            obj = obj3;
            if (i >= s1.length()) goto _L2; else goto _L1
_L1:
            obj1 = s1;
            obj = obj3;
            s1 = s1.substring(i + 1);
_L4:
            obj1 = s1;
            obj = obj3;
            View view = a(j);
            boolean flag;
            Method method;
            if (view == null)
            {
                if (false)
                {
                    try
                    {
                        throw new NullPointerException();
                    }
                    // Misplaced declaration of an exception variable
                    catch (Socket socket)
                    {
                        return false;
                    }
                } else
                {
                    return false;
                }
            }
            break; /* Loop/switch isn't completed */
_L2:
            s1 = "";
            if (true) goto _L4; else goto _L3
_L3:
            obj1 = s1;
            obj = obj3;
            method = android/view/ViewDebug.getDeclaredMethod("dispatchCommand", new Class[] {
                android/view/View, java/lang/String, java/lang/String, java/io/OutputStream
            });
            obj1 = s1;
            obj = obj3;
            method.setAccessible(true);
            obj1 = s1;
            obj = obj3;
            method.invoke(null, new Object[] {
                view, s, s1, new b(socket.getOutputStream())
            });
            obj1 = s1;
            obj = obj3;
            if (socket.isOutputShutdown()) goto _L6; else goto _L5
_L5:
            obj1 = s1;
            obj = obj3;
            socket = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            socket.write("DONE\n");
            socket.flush();
_L13:
            flag = flag1;
            if (socket != null)
            {
                try
                {
                    socket.close();
                }
                // Misplaced declaration of an exception variable
                catch (Socket socket)
                {
                    flag = false;
                    continue; /* Loop/switch isn't completed */
                }
                flag = flag1;
            }
_L11:
            return flag;
            obj;
            s1 = ((String) (obj1));
            socket = obj2;
            obj1 = obj;
_L10:
            obj = socket;
            Log.w("ViewServer", (new StringBuilder()).append("Could not send command ").append(s).append(" with parameters ").append(s1).toString(), ((Throwable) (obj1)));
            if (socket != null)
            {
                try
                {
                    socket.close();
                }
                // Misplaced declaration of an exception variable
                catch (Socket socket)
                {
                    flag = false;
                    continue; /* Loop/switch isn't completed */
                }
                flag = false;
                continue; /* Loop/switch isn't completed */
            }
            break MISSING_BLOCK_LABEL_421;
            socket;
            s = ((String) (obj));
_L8:
            if (s != null)
            {
                try
                {
                    s.close();
                }
                // Misplaced declaration of an exception variable
                catch (String s) { }
            }
            throw socket;
            s1;
            s = socket;
            socket = s1;
            if (true) goto _L8; else goto _L7
_L7:
            obj1;
            if (true) goto _L10; else goto _L9
_L9:
            flag = false;
            if (true) goto _L11; else goto _L6
_L6:
            socket = null;
            if (true) goto _L13; else goto _L12
_L12:
        }

        private boolean b(Socket socket)
        {
            Object obj = null;
            socket = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()), 8192);
            a.mFocusLock.readLock().lock();
            obj = a.mFocusedWindow;
            a.mFocusLock.readLock().unlock();
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_132;
            }
            a.mWindowsLock.readLock().lock();
            String s = (String)a.mWindows.get(a.mFocusedWindow);
            a.mWindowsLock.readLock().unlock();
            socket.write(Integer.toHexString(System.identityHashCode(obj)));
            socket.write(32);
            socket.append(s);
            socket.write(10);
            socket.flush();
            if (socket != null)
            {
                Exception exception;
                try
                {
                    socket.close();
                }
                // Misplaced declaration of an exception variable
                catch (Socket socket)
                {
                    return false;
                }
                return true;
            } else
            {
                return true;
            }
            obj;
            try
            {
                a.mFocusLock.readLock().unlock();
                throw obj;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
            finally
            {
                obj = socket;
            }
_L3:
            if (socket != null)
            {
                try
                {
                    socket.close();
                }
                // Misplaced declaration of an exception variable
                catch (Socket socket)
                {
                    return false;
                }
                return false;
            } else
            {
                return false;
            }
            obj;
            a.mWindowsLock.readLock().unlock();
            throw obj;
            socket = exception;
_L2:
            if (obj != null)
            {
                try
                {
                    ((BufferedWriter) (obj)).close();
                }
                // Misplaced declaration of an exception variable
                catch (Object obj) { }
            }
            throw socket;
            socket;
            obj = null;
            if (true) goto _L2; else goto _L1
_L1:
            socket;
            socket = ((Socket) (obj));
              goto _L3
        }

        private boolean c()
        {
            a.addWindowListener(this);
            Object obj2 = new BufferedWriter(new OutputStreamWriter(b.getOutputStream()));
_L7:
            Object obj = obj2;
            if (Thread.interrupted()) goto _L2; else goto _L1
_L1:
            obj = obj2;
            Object obj3 = ((Object) (e));
            obj = obj2;
            obj3;
            JVM INSTR monitorenter ;
            while (!c && !d) 
            {
                ((Object) (e)).wait();
            }
              goto _L3
            Exception exception;
            exception;
            obj3;
            JVM INSTR monitorexit ;
            boolean flag;
            boolean flag1;
            obj = obj2;
            try
            {
                throw exception;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj3) { }
            finally
            {
                obj2 = obj;
            }
_L11:
            obj = obj2;
            Log.w("ViewServer", "Connection error: ", ((Throwable) (obj3)));
            Object obj1;
            if (obj2 != null)
            {
                try
                {
                    ((BufferedWriter) (obj2)).close();
                }
                // Misplaced declaration of an exception variable
                catch (Object obj1) { }
            }
            a.removeWindowListener(this);
            return true;
_L3:
            if (!c)
            {
                break MISSING_BLOCK_LABEL_258;
            }
            c = false;
            flag = true;
_L13:
            if (!d) goto _L5; else goto _L4
_L4:
            d = false;
            flag1 = true;
_L12:
            obj3;
            JVM INSTR monitorexit ;
            if (!flag)
            {
                continue; /* Loop/switch isn't completed */
            }
            obj = obj2;
            ((BufferedWriter) (obj2)).write("LIST UPDATE\n");
            obj = obj2;
            ((BufferedWriter) (obj2)).flush();
            if (!flag1) goto _L7; else goto _L6
_L6:
            obj = obj2;
            ((BufferedWriter) (obj2)).write("FOCUS UPDATE\n");
            obj = obj2;
            ((BufferedWriter) (obj2)).flush();
            if (true) goto _L7; else goto _L8
_L8:
            obj1 = obj3;
_L10:
            if (obj2 != null)
            {
                try
                {
                    ((BufferedWriter) (obj2)).close();
                }
                // Misplaced declaration of an exception variable
                catch (Object obj2) { }
            }
            a.removeWindowListener(this);
            throw obj1;
_L2:
            if (obj2 != null)
            {
                try
                {
                    ((BufferedWriter) (obj2)).close();
                }
                // Misplaced declaration of an exception variable
                catch (Object obj1) { }
            }
            a.removeWindowListener(this);
            return true;
            obj1;
            obj2 = null;
            if (true) goto _L10; else goto _L9
_L9:
            obj3;
            obj2 = null;
              goto _L11
_L5:
            flag1 = false;
              goto _L12
            flag = false;
              goto _L13
        }

        public void a()
        {
            synchronized (e)
            {
                c = true;
                ((Object) (e)).notifyAll();
            }
            return;
            exception;
            aobj;
            JVM INSTR monitorexit ;
            throw exception;
        }

        public void b()
        {
            synchronized (e)
            {
                d = true;
                ((Object) (e)).notifyAll();
            }
            return;
            exception;
            aobj;
            JVM INSTR monitorexit ;
            throw exception;
        }

        public void run()
        {
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(b.getInputStream()), 1024);
            Object obj = bufferedreader;
            String s1 = bufferedreader.readLine();
            obj = bufferedreader;
            int i = s1.indexOf(' ');
            if (i != -1) goto _L2; else goto _L1
_L1:
            String s = "";
_L5:
            obj = bufferedreader;
            if (!"PROTOCOL".equalsIgnoreCase(s1)) goto _L4; else goto _L3
_L3:
            obj = bufferedreader;
            boolean flag = ViewServer.writeValue(b, "4");
_L6:
            if (flag)
            {
                break MISSING_BLOCK_LABEL_114;
            }
            obj = bufferedreader;
            Log.w("ViewServer", (new StringBuilder()).append("An error occurred with the command: ").append(s1).toString());
            String s2;
            if (bufferedreader != null)
            {
                try
                {
                    bufferedreader.close();
                }
                // Misplaced declaration of an exception variable
                catch (Object obj)
                {
                    ((IOException) (obj)).printStackTrace();
                }
            }
            if (b == null)
            {
                break MISSING_BLOCK_LABEL_136;
            }
            b.close();
_L8:
            return;
_L2:
            obj = bufferedreader;
            s = s1.substring(0, i);
            obj = bufferedreader;
            s2 = s1.substring(i + 1);
            s1 = s;
            s = s2;
              goto _L5
_L4:
            obj = bufferedreader;
            if (!"SERVER".equalsIgnoreCase(s1))
            {
                break MISSING_BLOCK_LABEL_200;
            }
            obj = bufferedreader;
            flag = ViewServer.writeValue(b, "4");
              goto _L6
            obj = bufferedreader;
            if (!"LIST".equalsIgnoreCase(s1))
            {
                break MISSING_BLOCK_LABEL_228;
            }
            obj = bufferedreader;
            flag = a(b);
              goto _L6
            obj = bufferedreader;
            if (!"GET_FOCUS".equalsIgnoreCase(s1))
            {
                break MISSING_BLOCK_LABEL_256;
            }
            obj = bufferedreader;
            flag = b(b);
              goto _L6
            obj = bufferedreader;
            if (!"AUTOLIST".equalsIgnoreCase(s1))
            {
                break MISSING_BLOCK_LABEL_280;
            }
            obj = bufferedreader;
            flag = c();
              goto _L6
            obj = bufferedreader;
            flag = a(b, s1, s);
              goto _L6
            obj;
            ((IOException) (obj)).printStackTrace();
            return;
            IOException ioexception2;
            ioexception2;
            bufferedreader = null;
_L11:
            obj = bufferedreader;
            Log.w("ViewServer", "Connection error: ", ioexception2);
            if (bufferedreader != null)
            {
                try
                {
                    bufferedreader.close();
                }
                // Misplaced declaration of an exception variable
                catch (Object obj)
                {
                    ((IOException) (obj)).printStackTrace();
                }
            }
            if (b == null) goto _L8; else goto _L7
_L7:
            try
            {
                b.close();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
            return;
            Exception exception;
            exception;
            obj = null;
_L10:
            if (obj != null)
            {
                try
                {
                    ((BufferedReader) (obj)).close();
                }
                catch (IOException ioexception)
                {
                    ioexception.printStackTrace();
                }
            }
            if (b != null)
            {
                try
                {
                    b.close();
                }
                catch (IOException ioexception1)
                {
                    ioexception1.printStackTrace();
                }
            }
            throw exception;
            exception;
            if (true) goto _L10; else goto _L9
_L9:
            ioexception2;
              goto _L11
        }

        public c(Socket socket)
        {
            a = ViewServer.this;
            super();
            b = socket;
            c = false;
            d = false;
        }
    }

    private static interface d
    {

        public abstract void a();

        public abstract void b();
    }


    private static final String BUILD_TYPE_USER = "user";
    private static final String COMMAND_PROTOCOL_VERSION = "PROTOCOL";
    private static final String COMMAND_SERVER_VERSION = "SERVER";
    private static final String COMMAND_WINDOW_MANAGER_AUTOLIST = "AUTOLIST";
    private static final String COMMAND_WINDOW_MANAGER_GET_FOCUS = "GET_FOCUS";
    private static final String COMMAND_WINDOW_MANAGER_LIST = "LIST";
    private static final String LOG_TAG = "ViewServer";
    private static final String VALUE_PROTOCOL_VERSION = "4";
    private static final String VALUE_SERVER_VERSION = "4";
    private static final int VIEW_SERVER_DEFAULT_PORT = 4939;
    private static final int VIEW_SERVER_MAX_CONNECTIONS = 10;
    private static ViewServer sServer;
    private final ReentrantReadWriteLock mFocusLock;
    private View mFocusedWindow;
    private final List mListeners;
    private final int mPort;
    private ServerSocket mServer;
    private Thread mThread;
    private ExecutorService mThreadPool;
    private final HashMap mWindows;
    private final ReentrantReadWriteLock mWindowsLock;

    private ViewServer()
    {
        mListeners = new CopyOnWriteArrayList();
        mWindows = new HashMap();
        mWindowsLock = new ReentrantReadWriteLock();
        mFocusLock = new ReentrantReadWriteLock();
        mPort = -1;
    }

    private ViewServer(int i)
    {
        mListeners = new CopyOnWriteArrayList();
        mWindows = new HashMap();
        mWindowsLock = new ReentrantReadWriteLock();
        mFocusLock = new ReentrantReadWriteLock();
        mPort = i;
    }


    private void addWindowListener(d d1)
    {
        if (!mListeners.contains(d1))
        {
            mListeners.add(d1);
        }
    }

    private void fireFocusChangedEvent()
    {
        for (Iterator iterator = mListeners.iterator(); iterator.hasNext(); ((d)iterator.next()).b()) { }
    }

    private void fireWindowsChangedEvent()
    {
        for (Iterator iterator = mListeners.iterator(); iterator.hasNext(); ((d)iterator.next()).a()) { }
    }

    public static ViewServer get(Context context)
    {
        context = context.getApplicationInfo();
        if ("user".equals(Build.TYPE) && (((ApplicationInfo) (context)).flags & 2) != 0)
        {
            if (sServer == null)
            {
                sServer = new ViewServer(4939);
            }
            if (!sServer.isRunning())
            {
                try
                {
                    sServer.start();
                }
                // Misplaced declaration of an exception variable
                catch (Context context)
                {
                    Log.d("ViewServer", "Error:", context);
                }
            }
        } else
        {
            sServer = new a();
        }
        return sServer;
    }

    private void removeWindowListener(d d1)
    {
        mListeners.remove(d1);
    }

    private static boolean writeValue(Socket socket, String s)
    {
        Object obj = null;
        socket = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()), 8192);
        socket.write(s);
        socket.write("\n");
        socket.flush();
        if (socket != null)
        {
            try
            {
                socket.close();
            }
            // Misplaced declaration of an exception variable
            catch (Socket socket)
            {
                return false;
            }
        }
        return true;
        socket;
        socket = obj;
_L3:
        if (socket != null)
        {
            try
            {
                socket.close();
            }
            // Misplaced declaration of an exception variable
            catch (Socket socket)
            {
                return false;
            }
            return false;
        } else
        {
            return false;
        }
        s;
        socket = null;
_L2:
        if (socket != null)
        {
            try
            {
                socket.close();
            }
            // Misplaced declaration of an exception variable
            catch (Socket socket) { }
        }
        throw s;
        s;
        if (true) goto _L2; else goto _L1
_L1:
        s;
          goto _L3
    }

    public void addWindow(Activity activity)
    {
        String s = activity.getTitle().toString();
        if (TextUtils.isEmpty(s))
        {
            s = (new StringBuilder()).append(activity.getClass().getCanonicalName()).append("/0x").append(System.identityHashCode(activity)).toString();
        } else
        {
            s = (new StringBuilder()).append(s).append("(").append(activity.getClass().getCanonicalName()).append(")").toString();
        }
        addWindow(activity.getWindow().getDecorView(), s);
    }

    public void addWindow(View view, String s)
    {
        mWindowsLock.writeLock().lock();
        mWindows.put(view.getRootView(), s);
        mWindowsLock.writeLock().unlock();
        fireWindowsChangedEvent();
        return;
        view;
        mWindowsLock.writeLock().unlock();
        throw view;
    }

    public boolean isRunning()
    {
        return mThread != null && mThread.isAlive();
    }

    public void removeWindow(Activity activity)
    {
        removeWindow(activity.getWindow().getDecorView());
    }

    public void removeWindow(View view)
    {
        mWindowsLock.writeLock().lock();
        mWindows.remove(view.getRootView());
        mWindowsLock.writeLock().unlock();
        fireWindowsChangedEvent();
        return;
        view;
        mWindowsLock.writeLock().unlock();
        throw view;
    }

    public void run()
    {
        Object obj;
        try
        {
            mServer = new ServerSocket(mPort, 10, InetAddress.getLocalHost());
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            Log.w("ViewServer", "Starting ServerSocket error: ", ((Throwable) (obj)));
        }
_L2:
        if (mServer == null || Thread.currentThread() != mThread)
        {
            break; /* Loop/switch isn't completed */
        }
        obj = mServer.accept();
        if (mThreadPool != null)
        {
            mThreadPool.submit(new c(((Socket) (obj))));
            continue; /* Loop/switch isn't completed */
        }
        ((Socket) (obj)).close();
        continue; /* Loop/switch isn't completed */
        IOException ioexception;
        ioexception;
        try
        {
            ioexception.printStackTrace();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            Log.w("ViewServer", "Connection error: ", ((Throwable) (obj)));
        }
        if (true) goto _L2; else goto _L1
_L1:
    }

    public void setFocusedWindow(Activity activity)
    {
        setFocusedWindow(activity.getWindow().getDecorView());
    }

    public void setFocusedWindow(View view)
    {
        mFocusLock.writeLock().lock();
        if (view != null)
        {
            break MISSING_BLOCK_LABEL_36;
        }
        view = null;
_L1:
        mFocusedWindow = view;
        mFocusLock.writeLock().unlock();
        fireFocusChangedEvent();
        return;
        view = view.getRootView();
          goto _L1
        view;
        mFocusLock.writeLock().unlock();
        throw view;
    }

    public boolean start()
        throws IOException
    {
        if (mThread != null)
        {
            return false;
        } else
        {
            mThread = new Thread(this, (new StringBuilder()).append("Local View Server [port=").append(mPort).append("]").toString());
            mThreadPool = Executors.newFixedThreadPool(10);
            mThread.start();
            return true;
        }
    }

    public boolean stop()
    {
        if (mThread == null)
        {
            break MISSING_BLOCK_LABEL_78;
        }
        mThread.interrupt();
        if (mThreadPool != null)
        {
            try
            {
                mThreadPool.shutdownNow();
            }
            catch (SecurityException securityexception)
            {
                Log.w("ViewServer", "Could not stop all view server threads");
            }
        }
        mThreadPool = null;
        mThread = null;
        mServer.close();
        mServer = null;
        return true;
        Object obj;
        obj;
        Log.w("ViewServer", "Could not close the view server");
        mWindowsLock.writeLock().lock();
        mWindows.clear();
        mWindowsLock.writeLock().unlock();
        mFocusLock.writeLock().lock();
        mFocusedWindow = null;
        mFocusLock.writeLock().unlock();
        return false;
        obj;
        mWindowsLock.writeLock().unlock();
        throw obj;
        obj;
        mFocusLock.writeLock().unlock();
        throw obj;
    }







}
