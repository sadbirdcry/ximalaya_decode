// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.util;

import android.os.Environment;
import android.util.Log;
import java.io.File;

public class Logger
{

    public static final String JSON_ERROR = "\u89E3\u6790json\u5F02\u5E38";
    private static final int LOG_LEVEL = 0;

    public Logger()
    {
    }

    public static void d(String s, String s1)
    {
        if (s1 != null && isLoggable(s, 3))
        {
            Log.d(s, s1);
        }
    }

    public static void d(String s, String s1, Throwable throwable)
    {
        if (s1 != null && isLoggable(s, 3))
        {
            Log.d(s, s1, throwable);
        }
    }

    public static void e(Exception exception)
    {
        e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38").append(exception.getMessage()).append(getLineInfo()).toString());
    }

    public static void e(String s, String s1)
    {
        if (s1 != null && isLoggable(s, 6))
        {
            Log.e(s, s1);
        }
    }

    public static void e(String s, String s1, Throwable throwable)
    {
        if (s1 != null && isLoggable(s, 6))
        {
            Log.e(s, s1, throwable);
        }
    }

    public static String getLineInfo()
    {
        return "";
    }

    public static File getLogFilePath()
    {
        File file;
        if (!Environment.getExternalStorageState().equals("mounted"))
        {
            file = null;
        } else
        {
            File file1 = new File("/sdcard/ting/errorLog/infor.log");
            if (!file1.getParentFile().getParentFile().exists())
            {
                file1.getParentFile().getParentFile().mkdir();
            }
            file = file1;
            if (!file1.getParentFile().exists())
            {
                file1.getParentFile().mkdir();
                return file1;
            }
        }
        return file;
    }

    public static void i(String s, String s1)
    {
        if (s1 != null && isLoggable(s, 4))
        {
            Log.i(s, s1);
        }
    }

    public static void i(String s, String s1, Throwable throwable)
    {
        if (s1 != null && isLoggable(s, 4))
        {
            Log.i(s, s1, throwable);
        }
    }

    public static boolean isLoggable(String s, int j)
    {
        return false;
    }

    public static void log(Object obj)
    {
    }

    public static void log(String s, Object obj)
    {
    }

    public static void log(String s, String s1, boolean flag)
    {
        if (s1 != null && isLoggable(s, 3))
        {
            Log.d(s, s1);
        }
    }

    public static void logToSd(String s)
    {
    }

    public static void throwRuntimeException(Object obj)
    {
    }

    public static void v(String s, String s1)
    {
        if (s1 != null && isLoggable(s, 2))
        {
            Log.v(s, s1);
        }
    }

    public static void v(String s, String s1, Throwable throwable)
    {
        if (s1 != null && isLoggable(s, 2))
        {
            Log.v(s, s1, throwable);
        }
    }

    public static void w(String s, String s1)
    {
        if (s1 != null && isLoggable(s, 5))
        {
            Log.w(s, s1);
        }
    }

    public static void w(String s, String s1, Throwable throwable)
    {
        if (s1 != null && isLoggable(s, 5))
        {
            Log.w(s, s1, throwable);
        }
    }

    public static void w(String s, Throwable throwable)
    {
        if (throwable != null && isLoggable(s, 5))
        {
            Log.w(s, throwable);
        }
    }
}
