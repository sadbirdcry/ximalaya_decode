// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.util;

import android.content.Context;
import android.content.SharedPreferences;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.library.BaseApplication;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class SharedPreferencesUtil
{

    private static SharedPreferencesUtil instance;
    private SharedPreferences settings;

    private SharedPreferencesUtil(Context context)
    {
        settings = context.getSharedPreferences("ting_data", 4);
    }

    public static void apply(android.content.SharedPreferences.Editor editor)
    {
        if (android.os.Build.VERSION.SDK_INT >= 9)
        {
            editor.apply();
            return;
        } else
        {
            editor.commit();
            return;
        }
    }

    public static SharedPreferencesUtil getInstance(Context context)
    {
        if (instance == null)
        {
            if (context == null)
            {
                context = BaseApplication.getApplication();
            } else
            {
                context = context.getApplicationContext();
            }
            instance = new SharedPreferencesUtil(context);
        }
        return instance;
    }

    public boolean contains(String s)
    {
        return settings.contains(s);
    }

    public ArrayList getArrayList(String s)
    {
        ArrayList arraylist = new ArrayList();
        s = settings.getString(s, "{}");
        try
        {
            s = new JSONArray(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return arraylist;
        }
        if (s != null)
        {
            for (int i = 0; i < s.length(); i++)
            {
                arraylist.add(s.optString(i));
            }

        }
        return arraylist;
    }

    public boolean getBoolean(String s)
    {
        return settings.getBoolean(s, false);
    }

    public boolean getBoolean(String s, boolean flag)
    {
        return settings.getBoolean(s, flag);
    }

    public Double getDouble(String s)
    {
        Object obj = null;
        String s1 = settings.getString(s, null);
        s = obj;
        if (s1 != null)
        {
            double d;
            try
            {
                d = Double.parseDouble(s1);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return null;
            }
            s = Double.valueOf(d);
        }
        return s;
    }

    public HashMap getHashMapByKey(String s)
    {
        HashMap hashmap = new HashMap();
        s = settings.getString(s, "{}");
        try
        {
            s = new JSONObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return hashmap;
        }
        if (s != null)
        {
            String s1;
            for (Iterator iterator = s.keys(); iterator.hasNext(); hashmap.put(s1, s.optString(s1)))
            {
                s1 = (String)iterator.next();
            }

        }
        return hashmap;
    }

    public int getInt(String s, int i)
    {
        return settings.getInt(s, i);
    }

    public long getLong(String s)
    {
        return settings.getLong(s, -1L);
    }

    public Boolean getOptBoolean(String s)
    {
        s = settings.getString(s, null);
        boolean flag;
        try
        {
            flag = Boolean.parseBoolean(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return null;
        }
        return Boolean.valueOf(flag);
    }

    public Double getOptDouble(String s)
    {
        s = settings.getString(s, null);
        double d;
        try
        {
            d = Double.parseDouble(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return null;
        }
        return Double.valueOf(d);
    }

    public String getString(String s)
    {
        return settings.getString(s, "");
    }

    public void removeByKey(String s)
    {
        apply(settings.edit().remove(s));
    }

    public void saveArrayList(String s, ArrayList arraylist)
    {
        apply(settings.edit().putString(s, JSON.toJSONString(arraylist)));
    }

    public void saveBoolean(String s, boolean flag)
    {
        apply(settings.edit().putBoolean(s, flag));
    }

    public void saveHashMap(String s, HashMap hashmap)
    {
        hashmap = new JSONObject(hashmap);
        apply(settings.edit().putString(s, hashmap.toString()));
    }

    public void saveInt(String s, int i)
    {
        apply(settings.edit().putInt(s, i));
    }

    public void saveLong(String s, long l)
    {
        apply(settings.edit().putLong(s, l));
    }

    public void saveString(String s, String s1)
    {
        apply(settings.edit().putString(s, s1));
    }
}
