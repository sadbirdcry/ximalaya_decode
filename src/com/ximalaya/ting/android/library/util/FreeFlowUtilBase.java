// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import sun.misc.BASE64Encoder;

// Referenced classes of package com.ximalaya.ting.android.library.util:
//            SharedPreferencesUtil

public class FreeFlowUtilBase
{

    public static String APPKEY = "3000004628";
    public static String KEY = "B8EC2F22856E58AC";
    public static String NET_PROXY_HOST = "58.254.132.94";
    public static final int NO_ORDER_STATUS = 0;
    public static final int ORDERED_STATUS = 1;
    private static final String ORDER_STATUS = "order_status";
    public static final int REORDER_STATUS = 2;
    public static final int TYPE_NET = 2;
    public static final int TYPE_OTHER = 0;
    public static final int TYPE_WAP = 1;
    public static final int UNKNOWN_STATUS = -1;
    public static String WAP_PROXY_HOST = "10.123.254.46";
    public static volatile boolean isNeedFreeFlowProxy = false;
    public static Context mContext;

    public FreeFlowUtilBase()
    {
    }

    public static HttpURLConnection getHttpURLConnection(URL url)
        throws IOException
    {
        int i = getNetType();
        if (isNeedFreeFlowProxy || isOrdered() && i != 0)
        {
            String s;
            if (i == 2)
            {
                s = NET_PROXY_HOST;
            } else
            {
                s = WAP_PROXY_HOST;
            }
            url = (HttpURLConnection)url.openConnection(new Proxy(java.net.Proxy.Type.HTTP, new InetSocketAddress(s, 8080)));
            s = (new StringBuilder()).append(APPKEY).append(":").append(KEY).toString();
            s = (new BASE64Encoder()).encode(s.getBytes());
            url.setRequestProperty("Authorization", (new StringBuilder()).append("Basic ").append(s).toString());
        } else
        {
            url = (HttpURLConnection)url.openConnection();
        }
        url.setConnectTimeout(30000);
        url.setReadTimeout(30000);
        return url;
    }

    public static int getNetType()
    {
        NetworkInfo networkinfo = ((ConnectivityManager)mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkinfo != null && networkinfo.isAvailable())
        {
            if (networkinfo.getType() == 0)
            {
                String s = null;
                if (networkinfo.getExtraInfo() != null)
                {
                    s = networkinfo.getExtraInfo().toLowerCase();
                }
                return s == null || !s.contains("net") ? 1 : 2;
            } else
            {
                return 0;
            }
        } else
        {
            return 0;
        }
    }

    public static boolean isOrdered()
    {
        int i = SharedPreferencesUtil.getInstance(mContext).getInt("order_status", -1);
        return i == 1 || i == 2;
    }

}
