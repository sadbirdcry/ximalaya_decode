// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.util;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import java.io.ByteArrayOutputStream;

public class BitmapUtils
{

    public BitmapUtils()
    {
    }

    public static Drawable bitmap2drawable(Resources resources, Bitmap bitmap)
    {
        if (bitmap == null)
        {
            return null;
        } else
        {
            return new BitmapDrawable(resources, bitmap);
        }
    }

    public static Bitmap clone(Bitmap bitmap)
    {
        if (bitmap == null)
        {
            return null;
        } else
        {
            return bitmap.copy(bitmap.getConfig(), true);
        }
    }

    public static Drawable clone(Resources resources, Drawable drawable)
    {
        if (drawable == null)
        {
            return null;
        } else
        {
            return drawable.getConstantState().newDrawable(resources);
        }
    }

    public static Bitmap drawable2bitmap(Drawable drawable)
    {
        if (drawable == null)
        {
            return null;
        }
        int i = drawable.getIntrinsicWidth();
        int j = drawable.getIntrinsicHeight();
        Object obj;
        Canvas canvas;
        if (drawable.getOpacity() != -1)
        {
            obj = android.graphics.Bitmap.Config.ARGB_8888;
        } else
        {
            obj = android.graphics.Bitmap.Config.RGB_565;
        }
        obj = Bitmap.createBitmap(i, j, ((android.graphics.Bitmap.Config) (obj)));
        canvas = new Canvas(((Bitmap) (obj)));
        drawable.setBounds(0, 0, i, j);
        drawable.draw(canvas);
        return ((Bitmap) (obj));
    }

    public static Bitmap extractBitmap(Bitmap bitmap, int i, int j)
    {
        int i1;
        int j1;
        while (bitmap == null || i == 0 && j == 0) 
        {
            return bitmap;
        }
        i1 = bitmap.getWidth();
        j1 = bitmap.getHeight();
        if (i != 0) goto _L2; else goto _L1
_L1:
        int k;
        int l;
        k = (int)((float)j * ((float)i1 / (float)j1));
        l = j;
_L4:
        float f = (float)k / (float)i1;
        float f1 = (float)l / (float)j1;
        Matrix matrix = new Matrix();
        matrix.postScale(f, f1);
        return Bitmap.createBitmap(bitmap, 0, 0, i1, j1, matrix, true);
_L2:
        k = i;
        l = j;
        if (j == 0)
        {
            l = (int)((float)i * ((float)j1 / (float)i1));
            k = i;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static Bitmap getBitmap(Resources resources, int i, int j, int k)
    {
        if (resources == null || i == 0)
        {
            return null;
        } else
        {
            return getBitmap(resources.getDrawable(i), j, k);
        }
    }

    public static Bitmap getBitmap(Drawable drawable, int i, int j)
    {
        int k;
        int l;
        int i1;
label0:
        {
            if (drawable == null)
            {
                return null;
            }
            drawable = ((BitmapDrawable)drawable).getBitmap();
            l = drawable.getWidth();
            i1 = drawable.getHeight();
            if (i >= 0)
            {
                k = i;
                i = j;
                if (j >= 0)
                {
                    break label0;
                }
            }
            i = i1;
            j = l;
            k = j;
        }
        float f = (float)i / (float)i1;
        float f1 = (float)k / (float)l;
        Matrix matrix = new Matrix();
        matrix.postScale(f, f1);
        return Bitmap.createBitmap(drawable, 0, 0, l, i1, matrix, true);
    }

    public static byte[] getBitmapByte(Bitmap bitmap, android.graphics.Bitmap.CompressFormat compressformat, int i)
    {
        if (bitmap == null)
        {
            return null;
        } else
        {
            ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
            boolean flag = bitmap.compress(compressformat, i, bytearrayoutputstream);
            bitmap = bytearrayoutputstream.toByteArray();
            Log.e("", (new StringBuilder()).append("getBitmapByte ").append(flag).append(", ").append(bitmap.length).toString());
            return bitmap;
        }
    }

    public static Bitmap getBitmapFromByte(byte abyte0[])
    {
        if (abyte0 != null)
        {
            return BitmapFactory.decodeByteArray(abyte0, 0, abyte0.length);
        } else
        {
            return null;
        }
    }

    public static Bitmap getRoundCornerBitmap(Bitmap bitmap, float f)
    {
        if (bitmap == null)
        {
            return null;
        } else
        {
            int i = bitmap.getWidth();
            int j = bitmap.getHeight();
            Bitmap bitmap1 = Bitmap.createBitmap(i, j, android.graphics.Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap1);
            Paint paint = new Paint();
            Rect rect = new Rect(0, 0, i, j);
            RectF rectf = new RectF(rect);
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            canvas.drawRoundRect(rectf, f, f, paint);
            paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            return bitmap1;
        }
    }
}
