// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.constants;

import android.os.Environment;

public class LibraryConstants
{

    public static final String ADS_DIR = (new StringBuilder()).append(APP_BASE_DIR).append("/ads").toString();
    public static final String APP_BASE_DIR = (new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/ting").toString();
    public static final int DOWN_ERROR = 4;
    public static final int DOWN_OK = 3;
    public static String OPEN_APP_ACTION = "iting://open";
    public static String OPEN_SUB_APP_ACTION = "subting://open";
    public static final int REQUEST_TIME_OUT = 5;
    public static final int TIME_OUT = 30000;
    public static final boolean isDebug = false;
    public static final boolean isRelease = true;

    public LibraryConstants()
    {
    }

}
