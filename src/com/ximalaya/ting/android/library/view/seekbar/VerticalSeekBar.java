// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.seekbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ViewParent;
import android.widget.SeekBar;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

// Referenced classes of package com.ximalaya.ting.android.library.view.seekbar:
//            VerticalSeekBarWrapper

public class VerticalSeekBar extends SeekBar
{

    public static final int ROTATION_ANGLE_CW_270 = 270;
    public static final int ROTATION_ANGLE_CW_90 = 90;
    private boolean mIsDragging;
    private Method mMethodSetProgress;
    private int mRotationAngle;
    private Drawable mThumb_;

    public VerticalSeekBar(Context context)
    {
        super(context);
        mRotationAngle = 90;
        initialize(context, null, 0, 0);
    }

    public VerticalSeekBar(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        mRotationAngle = 90;
        initialize(context, attributeset, 0, 0);
    }

    public VerticalSeekBar(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mRotationAngle = 90;
        initialize(context, attributeset, i, 0);
    }

    private void attemptClaimDrag(boolean flag)
    {
        ViewParent viewparent = getParent();
        if (viewparent != null)
        {
            viewparent.requestDisallowInterceptTouchEvent(flag);
        }
    }

    private Drawable getThumbCompat()
    {
        return mThumb_;
    }

    private VerticalSeekBarWrapper getWrapper()
    {
        ViewParent viewparent = getParent();
        if (viewparent instanceof VerticalSeekBarWrapper)
        {
            return (VerticalSeekBarWrapper)viewparent;
        } else
        {
            return null;
        }
    }

    private void initialize(Context context, AttributeSet attributeset, int i, int j)
    {
        ViewCompat.setLayoutDirection(this, 0);
        if (attributeset != null)
        {
            context = context.obtainStyledAttributes(attributeset, com.ximalaya.ting.android.library.R.styleable.VerticalSeekBar, i, j);
            i = context.getInteger(0, 0);
            if (isValidRotationAngle(i))
            {
                mRotationAngle = i;
            }
            context.recycle();
        }
    }

    private static boolean isValidRotationAngle(int i)
    {
        return i == 90 || i == 270;
    }

    private void onStartTrackingTouch()
    {
        mIsDragging = true;
    }

    private void onStopTrackingTouch()
    {
        mIsDragging = false;
    }

    private boolean onTouchEventTraditionalRotation(MotionEvent motionevent)
    {
        Drawable drawable;
        if (!isEnabled())
        {
            return false;
        }
        drawable = getThumbCompat();
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 48
    //                   0 50
    //                   1 99
    //                   2 84
    //                   3 148;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        return true;
_L2:
        setPressed(true);
        if (drawable != null)
        {
            invalidate(drawable.getBounds());
        }
        onStartTrackingTouch();
        trackTouchEvent(motionevent);
        attemptClaimDrag(true);
        continue; /* Loop/switch isn't completed */
_L4:
        if (mIsDragging)
        {
            trackTouchEvent(motionevent);
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (mIsDragging)
        {
            trackTouchEvent(motionevent);
            onStopTrackingTouch();
            setPressed(false);
        } else
        {
            onStartTrackingTouch();
            trackTouchEvent(motionevent);
            onStopTrackingTouch();
            attemptClaimDrag(false);
        }
        invalidate();
        continue; /* Loop/switch isn't completed */
_L5:
        if (mIsDragging)
        {
            onStopTrackingTouch();
            setPressed(false);
        }
        invalidate();
        if (true) goto _L1; else goto _L6
_L6:
    }

    private boolean onTouchEventUseViewRotation(MotionEvent motionevent)
    {
        motionevent.getAction();
        JVM INSTR tableswitch 0 1: default 28
    //                   0 34
    //                   1 42;
           goto _L1 _L2 _L3
_L1:
        return super.onTouchEvent(motionevent);
_L2:
        attemptClaimDrag(true);
        continue; /* Loop/switch isn't completed */
_L3:
        attemptClaimDrag(false);
        if (true) goto _L1; else goto _L4
_L4:
    }

    private void refreshThumb()
    {
        onSizeChanged(super.getWidth(), super.getHeight(), 0, 0);
    }

    private void setProgress(int i, boolean flag)
    {
        this;
        JVM INSTR monitorenter ;
        Method method = mMethodSetProgress;
        if (method != null)
        {
            break MISSING_BLOCK_LABEL_47;
        }
        Exception exception;
        try
        {
            method = getClass().getMethod("setProgress", new Class[] {
                Integer.TYPE, Boolean.TYPE
            });
            method.setAccessible(true);
            mMethodSetProgress = method;
        }
        catch (NoSuchMethodException nosuchmethodexception) { }
        method = mMethodSetProgress;
        if (method == null)
        {
            break MISSING_BLOCK_LABEL_90;
        }
        try
        {
            mMethodSetProgress.invoke(this, new Object[] {
                Integer.valueOf(i), Boolean.valueOf(flag)
            });
        }
        catch (IllegalArgumentException illegalargumentexception) { }
        catch (IllegalAccessException illegalaccessexception) { }
        catch (InvocationTargetException invocationtargetexception) { }
        refreshThumb();
        this;
        JVM INSTR monitorexit ;
        return;
        super.setProgress(i);
        break MISSING_BLOCK_LABEL_83;
        exception;
        throw exception;
    }

    private void trackTouchEvent(MotionEvent motionevent)
    {
        float f2;
        int i;
        int j;
        int k;
        int l;
        f2 = 0.0F;
        i = super.getPaddingTop();
        k = super.getPaddingBottom();
        j = getHeight();
        k = j - i - k;
        l = (int)motionevent.getY();
        mRotationAngle;
        JVM INSTR lookupswitch 2: default 68
    //                   90: 101
    //                   270: 111;
           goto _L1 _L2 _L3
_L3:
        break MISSING_BLOCK_LABEL_111;
_L1:
        float f = 0.0F;
_L4:
        float f1 = f2;
        if (f >= 0.0F)
        {
            if (k == 0)
            {
                f1 = f2;
            } else
            if (f > (float)k)
            {
                f1 = 1.0F;
            } else
            {
                f1 = f / (float)k;
            }
        }
        setProgress((int)(f1 * (float)getMax()), true);
        return;
_L2:
        f = l - i;
          goto _L4
        f = j - i - l;
          goto _L4
    }

    public int getRotationAngle()
    {
        return mRotationAngle;
    }

    protected void onDraw(Canvas canvas)
    {
        this;
        JVM INSTR monitorenter ;
        if (useViewRotation()) goto _L2; else goto _L1
_L1:
        mRotationAngle;
        JVM INSTR lookupswitch 2: default 93
    //                   90: 48
    //                   270: 73;
           goto _L3 _L4 _L5
_L3:
        break; /* Loop/switch isn't completed */
_L5:
        break MISSING_BLOCK_LABEL_73;
_L2:
        super.onDraw(canvas);
        this;
        JVM INSTR monitorexit ;
        return;
_L4:
        canvas.rotate(90F);
        canvas.translate(0.0F, -super.getWidth());
          goto _L2
        canvas;
        throw canvas;
        canvas.rotate(-90F);
        canvas.translate(-super.getHeight(), 0.0F);
          goto _L2
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        int j;
        boolean flag;
        flag = false;
        j = -1;
        if (!isEnabled()) goto _L2; else goto _L1
_L1:
        i;
        JVM INSTR tableswitch 19 22: default 44
    //                   19 104
    //                   20 87
    //                   21 122
    //                   22 122;
           goto _L3 _L4 _L5 _L6 _L6
_L3:
        j = 0;
_L7:
        if (flag)
        {
            i = getKeyProgressIncrement();
            j = j * i + getProgress();
            if (j >= 0 && j <= getMax())
            {
                setProgress(j - i, true);
            }
            return true;
        }
        break; /* Loop/switch isn't completed */
_L5:
        if (mRotationAngle == 90)
        {
            j = 1;
        }
        flag = true;
        continue; /* Loop/switch isn't completed */
_L4:
        if (mRotationAngle == 270)
        {
            j = 1;
        }
        flag = true;
        continue; /* Loop/switch isn't completed */
_L6:
        j = 0;
        flag = true;
        if (true) goto _L7; else goto _L2
_L2:
        return super.onKeyDown(i, keyevent);
    }

    protected void onMeasure(int i, int j)
    {
        this;
        JVM INSTR monitorenter ;
        if (!useViewRotation()) goto _L2; else goto _L1
_L1:
        super.onMeasure(i, j);
_L3:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        android.view.ViewGroup.LayoutParams layoutparams;
        super.onMeasure(j, i);
        layoutparams = getLayoutParams();
        if (!isInEditMode() || layoutparams == null)
        {
            break MISSING_BLOCK_LABEL_70;
        }
        if (layoutparams.height < 0)
        {
            break MISSING_BLOCK_LABEL_70;
        }
        setMeasuredDimension(super.getMeasuredHeight(), getLayoutParams().height);
          goto _L3
        Exception exception;
        exception;
        throw exception;
        setMeasuredDimension(super.getMeasuredHeight(), super.getMeasuredWidth());
          goto _L3
    }

    protected void onSizeChanged(int i, int j, int k, int l)
    {
        if (useViewRotation())
        {
            super.onSizeChanged(i, j, k, l);
            return;
        } else
        {
            super.onSizeChanged(j, i, l, k);
            return;
        }
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        if (useViewRotation())
        {
            return onTouchEventUseViewRotation(motionevent);
        } else
        {
            return onTouchEventTraditionalRotation(motionevent);
        }
    }

    public void setProgress(int i)
    {
        this;
        JVM INSTR monitorenter ;
        super.setProgress(i);
        if (!useViewRotation())
        {
            refreshThumb();
        }
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void setRotationAngle(int i)
    {
        if (!isValidRotationAngle(i))
        {
            throw new IllegalArgumentException((new StringBuilder()).append("Invalid angle specified :").append(i).toString());
        }
        if (mRotationAngle != i)
        {
            mRotationAngle = i;
            if (useViewRotation())
            {
                VerticalSeekBarWrapper verticalseekbarwrapper = getWrapper();
                if (verticalseekbarwrapper != null)
                {
                    verticalseekbarwrapper.applyViewRotation();
                    return;
                }
            } else
            {
                requestLayout();
                return;
            }
        }
    }

    public void setThumb(Drawable drawable)
    {
        mThumb_ = drawable;
        super.setThumb(drawable);
    }

    boolean useViewRotation()
    {
        boolean flag;
        boolean flag1;
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        flag1 = isInEditMode();
        return flag && !flag1;
    }
}
