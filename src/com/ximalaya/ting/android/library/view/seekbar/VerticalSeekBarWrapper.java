// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.seekbar;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.widget.FrameLayout;

// Referenced classes of package com.ximalaya.ting.android.library.view.seekbar:
//            VerticalSeekBar

public class VerticalSeekBarWrapper extends FrameLayout
{

    public VerticalSeekBarWrapper(Context context)
    {
        this(context, null, 0);
    }

    public VerticalSeekBarWrapper(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0);
    }

    public VerticalSeekBarWrapper(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
    }

    private void applyViewRotation(int i, int j)
    {
        VerticalSeekBar verticalseekbar = getChildSeekBar();
        if (verticalseekbar != null)
        {
            int k = verticalseekbar.getRotationAngle();
            android.view.ViewGroup.LayoutParams layoutparams = verticalseekbar.getLayoutParams();
            layoutparams.width = j;
            layoutparams.height = -2;
            verticalseekbar.setLayoutParams(layoutparams);
            if (k == 90)
            {
                k = ViewCompat.getPaddingEnd(verticalseekbar);
                ViewCompat.setRotation(verticalseekbar, 90F);
                ViewCompat.setTranslationX(verticalseekbar, -(j - i) / 2);
                ViewCompat.setTranslationY(verticalseekbar, j / 2 - k);
            } else
            if (k == 270)
            {
                int l = ViewCompat.getPaddingStart(verticalseekbar);
                ViewCompat.setRotation(verticalseekbar, -90F);
                ViewCompat.setTranslationX(verticalseekbar, -(j - i) / 2);
                ViewCompat.setTranslationY(verticalseekbar, j / 2 - l);
                return;
            }
        }
    }

    private VerticalSeekBar getChildSeekBar()
    {
        android.view.View view;
        if (getChildCount() > 0)
        {
            view = getChildAt(0);
        } else
        {
            view = null;
        }
        if (view instanceof VerticalSeekBar)
        {
            return (VerticalSeekBar)view;
        } else
        {
            return null;
        }
    }

    private void onSizeChangedTraditionalRotation(int i, int j, int k, int l)
    {
        VerticalSeekBar verticalseekbar = getChildSeekBar();
        if (verticalseekbar != null)
        {
            android.widget.FrameLayout.LayoutParams layoutparams = (android.widget.FrameLayout.LayoutParams)verticalseekbar.getLayoutParams();
            layoutparams.width = -2;
            layoutparams.height = j;
            verticalseekbar.setLayoutParams(layoutparams);
            verticalseekbar.measure(0, 0);
            layoutparams.gravity = 51;
            layoutparams.leftMargin = (i - verticalseekbar.getMeasuredWidth()) / 2;
            verticalseekbar.setLayoutParams(layoutparams);
        }
        super.onSizeChanged(i, j, k, l);
    }

    private void onSizeChangedUseViewRotation(int i, int j, int k, int l)
    {
        applyViewRotation(i, j);
        super.onSizeChanged(i, j, k, l);
    }

    private boolean useViewRotation()
    {
        VerticalSeekBar verticalseekbar = getChildSeekBar();
        if (verticalseekbar != null)
        {
            return verticalseekbar.useViewRotation();
        } else
        {
            return false;
        }
    }

    void applyViewRotation()
    {
        applyViewRotation(getWidth(), getHeight());
    }

    protected void onMeasure(int i, int j)
    {
        VerticalSeekBar verticalseekbar = getChildSeekBar();
        int k = android.view.View.MeasureSpec.getMode(i);
        if (verticalseekbar != null && k != 0x40000000)
        {
            int l;
            int i1;
            if (useViewRotation())
            {
                verticalseekbar.measure(j, i);
                i1 = verticalseekbar.getMeasuredHeight();
                l = verticalseekbar.getMeasuredWidth();
            } else
            {
                verticalseekbar.measure(i, j);
                i1 = verticalseekbar.getMeasuredWidth();
                l = verticalseekbar.getMeasuredHeight();
            }
            setMeasuredDimension(ViewCompat.resolveSizeAndState(i1, i, 0), ViewCompat.resolveSizeAndState(l, j, 0));
            return;
        } else
        {
            super.onMeasure(i, j);
            return;
        }
    }

    protected void onSizeChanged(int i, int j, int k, int l)
    {
        if (useViewRotation())
        {
            onSizeChangedUseViewRotation(i, j, k, l);
            return;
        } else
        {
            onSizeChangedTraditionalRotation(i, j, k, l);
            return;
        }
    }
}
