// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.record;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.view.SurfaceHolder;
import com.ximalaya.ting.android.library.util.ToolUtilLib;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.library.view.record:
//            WaveformView, WaveformController, PaintBuffer

public class RenderThread extends Thread
{

    private static final int REFRESH_TIME = 100;
    private List mAmps;
    private int mBarPadding;
    private int mBarWidth;
    private List mCachedScaledAmp;
    private Paint mClearPaint;
    private WaveformController mController;
    private Paint mCutterPaint;
    private volatile int mCutterPos;
    private volatile int mEndPos;
    private int mHeight;
    private int mLastAmpSize;
    private boolean mPause;
    private volatile int mStartPos;
    private boolean mStop;
    private SurfaceHolder mSurfaceHolder;
    private WaveformView mView;
    private int mWidth;

    public RenderThread(WaveformController waveformcontroller, WaveformView waveformview)
    {
        mStop = false;
        mPause = false;
        mCachedScaledAmp = new ArrayList();
        mController = waveformcontroller;
        mView = waveformview;
        mCutterPaint = new Paint();
        mClearPaint = new Paint();
        mWidth = mView.getWidth();
        mHeight = mView.getHeight();
        mCutterPos = mWidth;
    }

    private void clearCanvas(Canvas canvas)
    {
        Paint paint = mClearPaint;
        paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.CLEAR));
        canvas.drawPaint(paint);
        paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.SRC));
    }

    private List computeScaledAmp(boolean flag)
    {
        if (mLastAmpSize != mAmps.size() || flag)
        {
            mLastAmpSize = mAmps.size();
            mCachedScaledAmp.clear();
            int k3 = mAmps.size();
            int k = mWidth - mController.getScaledCutterWidth() / 2;
            int i = mView.getBarWidth();
            int k1 = mView.getBarPadding() + i;
            i = k / k1;
            if (k % k1 != 0)
            {
                i++;
            }
            if (k3 > i)
            {
                int l3 = k3 / i;
                for (int l = 0; l < i - 1; l++)
                {
                    int i3 = l * l3;
                    int j4 = (l + 1) * l3;
                    int l1 = i3;
                    int k2 = 0;
                    for (; l1 < j4; l1++)
                    {
                        k2 += ((Integer)mAmps.get(l1)).intValue();
                    }

                    l1 = k2 / (j4 - i3);
                    mCachedScaledAmp.add(Integer.valueOf(l1));
                }

                int i2 = (i - 1) * l3;
                int i1 = 0;
                for (i = i2; i < k3; i++)
                {
                    i1 += ((Integer)mAmps.get(i)).intValue();
                }

                i = i1 / (k3 - i2);
                mCachedScaledAmp.add(Integer.valueOf(i));
            } else
            {
                int j3 = i - k3;
                k3 = mAmps.size() - 1;
                int i4 = j3 / k3;
                for (int j = 0; j < k3; j++)
                {
                    int l2 = j * i4;
                    int j1 = (j + 1) * i4;
                    if (j == k3 - 1)
                    {
                        j1 += j3 % k3;
                    }
                    mCachedScaledAmp.add(mAmps.get(j));
                    for (int j2 = l2; j2 < j1; j2++)
                    {
                        int k4 = ((Integer)mAmps.get(j)).intValue();
                        int l4 = ((((Integer)mAmps.get(j + 1)).intValue() - ((Integer)mAmps.get(j)).intValue()) * (j2 - l2)) / (j1 - l2);
                        mCachedScaledAmp.add(Integer.valueOf(l4 + k4));
                    }

                }

                mCachedScaledAmp.add(mAmps.get(k3));
            }
        }
        return mCachedScaledAmp;
    }

    private void drawBase(Canvas canvas)
    {
        canvas.drawColor(mView.getBackgroundColor());
    }

    private void drawEditor(Canvas canvas)
    {
        Paint paint;
        if (mController.isInDrag())
        {
            mCutterPaint.setAlpha(155);
        } else
        {
            mCutterPaint.setAlpha(255);
        }
        if (mCutterPos > mEndPos)
        {
            mCutterPos = mEndPos;
        }
        paint = PaintBuffer.obtain();
        paint.setColor(Color.parseColor("#eceef1"));
        paint.setAlpha(200);
        canvas.drawRect(mCutterPos, 0.0F, mWidth, mHeight - mController.getBaseLine(), paint);
        if (mController.isInDrag())
        {
            paint.setAlpha(180);
            canvas.drawBitmap(mController.getScaledCutter(), (mCutterPos - mController.getScaledCutterWidth()) + ToolUtilLib.dp2px(mView.getContext(), 3F), 0.0F, paint);
        } else
        {
            paint.setAlpha(255);
            canvas.drawBitmap(mController.getScaledCutter(), (mCutterPos - mController.getScaledCutterWidth()) + ToolUtilLib.dp2px(mView.getContext(), 3F), 0.0F, paint);
        }
        PaintBuffer.recycle(paint);
    }

    private void drawPlayer(Canvas canvas)
    {
        List list = computeScaledAmp(true);
        if (list == null || list.size() == 0)
        {
            return;
        }
        mStartPos = 0;
        mEndPos = 0;
        int j = list.size();
        int k = (int)((float)j * mView.getPlayerProgress());
        int i = 0;
        while (i < j) 
        {
            int l = mStartPos;
            l = (mBarWidth + mBarPadding) * i + l;
            int i1 = mHeight;
            int j1 = mController.getBaseLine();
            int k1 = ((Integer)list.get(i)).intValue();
            int l1 = mBarWidth;
            int i2 = mHeight;
            int j2 = mController.getBaseLine();
            Paint paint = PaintBuffer.obtain();
            if (i < k)
            {
                paint.setColor(mController.getPlayedColor());
            } else
            if (i == k)
            {
                if (mController.getScaledPlayPointer() != null)
                {
                    canvas.drawBitmap(mController.getScaledPlayPointer(), l, mHeight - mController.getBaseLine(), null);
                }
            } else
            {
                paint.setColor(mController.getBackgroundSoundColor());
            }
            canvas.drawRect(l, i1 - j1 - k1, l + l1, i2 - j2, paint);
            PaintBuffer.recycle(paint);
            i++;
        }
        mEndPos = mStartPos + (j - 1) * (mBarWidth + mBarPadding) + mBarWidth;
    }

    private void drawWaveform(Canvas canvas)
    {
        List list;
        boolean flag;
        if (mView.getMode() == 2 || mView.getMode() == 3 || mView.getMode() == 4)
        {
            list = computeScaledAmp(false);
            flag = false;
        } else
        {
            list = mAmps;
            flag = true;
        }
        int j1;
        if (list != null)
        {
            if ((j1 = list.size()) != 0)
            {
                int i = (j1 - 1) * (mBarWidth + mBarPadding) + mBarWidth;
                mStartPos = 0;
                mEndPos = 0;
                int j;
                if (i > mWidth)
                {
                    i = (mWidth + mBarPadding) / (mBarWidth + mBarPadding);
                    mStartPos = 0;
                    i = j1 - i;
                } else
                {
                    if (flag)
                    {
                        mStartPos = mWidth - i;
                    }
                    i = 0;
                }
                j = 0;
                while (j < j1 - i) 
                {
                    int k = mStartPos;
                    int k1 = (mBarWidth + mBarPadding) * j + k;
                    int l1 = mHeight;
                    int i2 = mController.getBaseLine();
                    int j2 = ((Integer)list.get(j + i)).intValue();
                    int k2 = mBarWidth;
                    int l2 = mHeight;
                    int i3 = mController.getBaseLine();
                    Paint paint = PaintBuffer.obtain();
                    if (flag)
                    {
                        int i1 = mView.getMarkedColor(j + i);
                        int l = i1;
                        if (i1 == 0)
                        {
                            l = mController.getRecordingColor();
                        }
                        paint.setColor(l);
                    } else
                    {
                        paint.setColor(mView.getWaveformColor());
                    }
                    canvas.drawRect(k1, l1 - i2 - j2, k1 + k2, l2 - i3, paint);
                    PaintBuffer.recycle(paint);
                    j++;
                }
                mEndPos = mStartPos + (j1 - 1) * (mBarWidth + mBarPadding) + mBarWidth;
                return;
            }
        }
    }

    int getCutterPos()
    {
        return mCutterPos;
    }

    int getEndPos()
    {
        int i;
        synchronized (mAmps)
        {
            i = mEndPos;
        }
        return i;
        exception;
        list;
        JVM INSTR monitorexit ;
        throw exception;
    }

    int getStartPos()
    {
        return mStartPos;
    }

    public void pauseRender()
    {
        mPause = true;
    }

    public void resumeRender()
    {
        mPause = false;
        this;
        JVM INSTR monitorenter ;
        notify();
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void run()
    {
        super.run();
        mStop = false;
        mPause = false;
_L15:
        if (mStop) goto _L2; else goto _L1
_L1:
        if (!mPause) goto _L4; else goto _L3
_L3:
        this;
        JVM INSTR monitorenter ;
        wait();
_L5:
        this;
        JVM INSTR monitorexit ;
_L4:
        if (!mStop)
        {
            break MISSING_BLOCK_LABEL_57;
        }
_L2:
        return;
        Object obj;
        obj;
        ((InterruptedException) (obj)).printStackTrace();
          goto _L5
        obj;
        this;
        JVM INSTR monitorexit ;
        throw obj;
        Object obj1;
        Object obj2;
        Object obj3;
        List list;
        list = null;
        obj3 = null;
        obj2 = obj3;
        obj1 = list;
        if (mSurfaceHolder == null)
        {
            break MISSING_BLOCK_LABEL_95;
        }
        obj2 = obj3;
        obj1 = list;
        boolean flag = mSurfaceHolder.isCreating();
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_120;
        }
        if (false)
        {
            try
            {
                mSurfaceHolder.unlockCanvasAndPost(null);
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1)
            {
                ((Exception) (obj1)).printStackTrace();
            }
        }
        continue; /* Loop/switch isn't completed */
        obj2 = obj3;
        obj1 = list;
        obj3 = mSurfaceHolder.lockCanvas();
        if (obj3 == null) goto _L7; else goto _L6
_L6:
        obj2 = obj3;
        obj1 = obj3;
        mBarWidth = mView.getBarWidth();
        obj2 = obj3;
        obj1 = obj3;
        mBarPadding = mView.getBarPadding();
        obj2 = obj3;
        obj1 = obj3;
        clearCanvas(((Canvas) (obj3)));
        obj2 = obj3;
        obj1 = obj3;
        drawBase(((Canvas) (obj3)));
        obj2 = obj3;
        obj1 = obj3;
        mAmps = mView.getAmps();
        obj2 = obj3;
        obj1 = obj3;
        if (mAmps == null)
        {
            break MISSING_BLOCK_LABEL_233;
        }
        obj2 = obj3;
        obj1 = obj3;
        int i = mAmps.size();
        if (i != 0)
        {
            break MISSING_BLOCK_LABEL_258;
        }
        if (obj3 != null)
        {
            try
            {
                mSurfaceHolder.unlockCanvasAndPost(((Canvas) (obj3)));
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1)
            {
                ((Exception) (obj1)).printStackTrace();
            }
        }
        continue; /* Loop/switch isn't completed */
        obj2 = obj3;
        obj1 = obj3;
        list = mAmps;
        obj2 = obj3;
        obj1 = obj3;
        list;
        JVM INSTR monitorenter ;
        mView.getMode();
        JVM INSTR tableswitch 2 3: default 441
    //                   2 343
    //                   3 400;
           goto _L8 _L9 _L10
_L8:
        drawWaveform(((Canvas) (obj3)));
_L11:
        list;
        JVM INSTR monitorexit ;
_L7:
        Exception exception2;
        if (obj3 != null)
        {
            try
            {
                mSurfaceHolder.unlockCanvasAndPost(((Canvas) (obj3)));
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1)
            {
                ((Exception) (obj1)).printStackTrace();
            }
        }
_L12:
        try
        {
            Thread.sleep(100L);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            ((InterruptedException) (obj1)).printStackTrace();
        }
        continue; /* Loop/switch isn't completed */
_L9:
        drawPlayer(((Canvas) (obj3)));
        drawEditor(((Canvas) (obj3)));
          goto _L11
        exception2;
        list;
        JVM INSTR monitorexit ;
        Exception exception1;
        obj2 = obj3;
        obj1 = obj3;
        try
        {
            throw exception2;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj3)
        {
            obj1 = obj2;
        }
        finally
        {
            if (obj1 == null) goto _L0; else goto _L0
        }
        ((Exception) (obj3)).printStackTrace();
        if (obj2 != null)
        {
            try
            {
                mSurfaceHolder.unlockCanvasAndPost(((Canvas) (obj2)));
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1)
            {
                ((Exception) (obj1)).printStackTrace();
            }
        }
          goto _L12
_L10:
        drawPlayer(((Canvas) (obj3)));
        if (true) goto _L11; else goto _L13
_L13:
        try
        {
            mSurfaceHolder.unlockCanvasAndPost(((Canvas) (obj1)));
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        throw exception1;
        if (true) goto _L15; else goto _L14
_L14:
    }

    void setCutterPos(float f)
    {
        mCutterPos = (int)f;
    }

    public void stopRender()
    {
        mStop = true;
        this;
        JVM INSTR monitorenter ;
        notify();
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void surfaceChanged(SurfaceHolder surfaceholder, int i, int j, int k)
    {
        mSurfaceHolder = surfaceholder;
        mWidth = j;
        mHeight = k;
    }
}
