// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.record;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import com.ximalaya.ting.android.library.util.BitmapUtils;
import java.math.BigDecimal;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.library.view.record:
//            WaveformView, RenderThread

public class WaveformController
    implements android.view.SurfaceHolder.Callback, android.view.View.OnTouchListener
{

    private List mAmps;
    private int mBarPadding;
    private int mBarWidth;
    private int mBaseLine;
    private int mBgSoundColor;
    private WaveformView.ClickListener mClickListener;
    private int mCurrCutterPos;
    private WaveformView.OnCutListener mCutListener;
    private Bitmap mCutter;
    private int mCutterWidth;
    private int mDragOffset;
    private volatile boolean mInDrag;
    private int mPlayPointHeight;
    private int mPlayedColor;
    private Bitmap mPointer;
    private float mPreX;
    private int mRecordingColor;
    private RenderThread mRender;
    private Resources mResources;
    private WaveformView mView;
    private int mViewHeight;

    public WaveformController(WaveformView waveformview)
    {
        mDragOffset = 10;
        mInDrag = false;
        mView = waveformview;
        mBarPadding = mView.getBarPadding();
        mBarWidth = mView.getBarWidth();
        mResources = mView.getResources();
        mDragOffset = dp2px(5F);
    }

    private int dp2px(float f)
    {
        return (int)(mResources.getDisplayMetrics().density * f + 0.5F);
    }

    private int getMinCutterPos()
    {
        return mCutterWidth;
    }

    private int getTruePos(int i)
    {
        int j = 3;
        if (i > mRender.getEndPos())
        {
            return mRender.getEndPos();
        }
        i = (new BigDecimal((float)i / (float)(mView.getBarWidth() + mView.getBarPadding()))).setScale(0, 4).intValue();
        if (i <= 3)
        {
            i = j;
        }
        j = mView.getBarWidth();
        return (i - 1) * mView.getBarPadding() + j * i;
    }

    private boolean handleEditEvent(MotionEvent motionevent)
    {
        float f;
        float f5;
        float f6;
        float f7;
        float f8;
        int i;
        f5 = 0.0F;
        f7 = 0.0F;
        f6 = 0.0F;
        f = 0.0F;
        i = 0;
        mCurrCutterPos = mRender.getCutterPos();
        f8 = motionevent.getX();
        motionevent.getAction() & 0xff;
        JVM INSTR tableswitch 0 3: default 68
    //                   0 70
    //                   1 347
    //                   2 181
    //                   3 347;
           goto _L1 _L2 _L3 _L4 _L3
_L1:
        return false;
_L2:
        if (f8 <= (float)(mCurrCutterPos + mDragOffset) && f8 >= (float)(mCurrCutterPos - mCutterWidth - mDragOffset))
        {
            mPreX = f8;
            mInDrag = true;
            if (mCutListener != null)
            {
                if (mRender != null)
                {
                    i = mRender.getEndPos();
                }
                if (i != 0)
                {
                    f = f8 / (float)mRender.getEndPos();
                }
                mCutListener.onStartDrag(f);
            }
        } else
        {
            mInDrag = false;
        }
        return true;
_L4:
        if (mInDrag)
        {
            float f1 = f8 - mPreX;
            if (Math.abs(f1) > (float)((mView.getBarWidth() + mView.getBarPadding()) / 2))
            {
                mCurrCutterPos = (int)(f1 + (float)mCurrCutterPos);
                if (mCurrCutterPos > mRender.getEndPos())
                {
                    mCurrCutterPos = mRender.getEndPos();
                }
                if (mCurrCutterPos < 0)
                {
                    mCurrCutterPos = 0;
                }
                mRender.setCutterPos(mCurrCutterPos);
                mPreX = mCurrCutterPos;
                if (mCutListener != null)
                {
                    float f2 = f5;
                    if (mRender != null)
                    {
                        f2 = f5;
                        if (mRender.getEndPos() != 0)
                        {
                            f2 = (float)mCurrCutterPos / (float)mRender.getEndPos();
                        }
                    }
                    mCutListener.onDragging(f2);
                }
            }
        }
        return false;
_L3:
        if (!mInDrag)
        {
            if (mClickListener != null)
            {
                float f3;
                int j;
                if (mRender == null)
                {
                    j = 0;
                } else
                {
                    j = mRender.getEndPos();
                }
                f3 = f7;
                if (j != 0)
                {
                    f3 = f8 / (float)mRender.getEndPos();
                }
                mClickListener.onClick(f3);
            }
            return false;
        }
        mInDrag = false;
        mPreX = 0.0F;
        if (mCutListener != null)
        {
            float f4 = f6;
            if (mRender != null)
            {
                f4 = f6;
                if (mRender.getEndPos() != 0)
                {
                    f4 = f8 / (float)mRender.getEndPos();
                }
            }
            mCutListener.onStopDrag(f4);
        }
        if (true) goto _L1; else goto _L5
_L5:
    }

    public void clearMarkColor()
    {
        mView.clearMarkColor();
    }

    public void cutFinish()
    {
        if (mRender != null)
        {
            mRender.setCutterPos(mRender.getEndPos());
        }
    }

    public int getBackgroundSoundColor()
    {
        return mBgSoundColor;
    }

    int getBaseLine()
    {
        return mBaseLine;
    }

    public int getPlayedColor()
    {
        return mPlayedColor;
    }

    public int getRecordingColor()
    {
        return mRecordingColor;
    }

    Bitmap getScaledCutter()
    {
        if (mCutter == null || mViewHeight != 0 && mCutter.getHeight() != mViewHeight)
        {
            mCutter = BitmapUtils.extractBitmap(mView.getCutterBitmap(), 0, mViewHeight - mView.getPlayPointerHeight());
            mCutterWidth = mCutter.getWidth();
        }
        return mCutter;
    }

    int getScaledCutterWidth()
    {
        return mCutterWidth;
    }

    Bitmap getScaledPlayPointer()
    {
        if (mPointer == null || mBaseLine == 0 || mBaseLine != 0 && mPointer.getHeight() != mBaseLine)
        {
            mPointer = BitmapUtils.extractBitmap(mView.getPointerBitmap(), 0, mView.getPlayPointerHeight());
            mBaseLine = mView.getPlayPointerHeight();
        }
        return mPointer;
    }

    boolean isInDrag()
    {
        return mInDrag;
    }

    public void onPause()
    {
        if (mRender != null)
        {
            mRender.pauseRender();
        }
    }

    public void onResume()
    {
        if (mRender != null)
        {
            mRender.resumeRender();
        }
    }

    public boolean onTouch(View view, MotionEvent motionevent)
    {
        switch (mView.getMode())
        {
        default:
            return view.onTouchEvent(motionevent);

        case 2: // '\002'
            return handleEditEvent(motionevent);
        }
    }

    public void setAmps(List list)
    {
        mAmps = list;
    }

    public void setBackgroundSoundClolor(int i)
    {
        mBgSoundColor = i;
    }

    void setOnClickListener(WaveformView.ClickListener clicklistener)
    {
        mClickListener = clicklistener;
    }

    void setOnCutListener(WaveformView.OnCutListener oncutlistener)
    {
        mCutListener = oncutlistener;
    }

    public void setPlayedColor(int i)
    {
        mPlayedColor = i;
    }

    public void setRecordingColor(int i)
    {
        mRecordingColor = i;
    }

    public void startEdit()
    {
        mView.getMode();
        mView.setWaveformColor(mBgSoundColor);
        mView.setAmps(mAmps);
        mView.setMode(2);
        if (mRender != null)
        {
            mRender.setCutterPos(mRender.getEndPos());
        }
        mView.setCutterVisible(true);
        mView.setPlayPointerVisible(true);
    }

    public void startPlay()
    {
        mView.setWaveformColor(mPlayedColor);
        mView.setAmps(mAmps);
        mView.setMode(3);
        mView.setCutterVisible(true);
        mView.setPlayPointerVisible(true);
    }

    public void startRecord()
    {
        mView.setBarPadding(mBarPadding);
        mView.setBarWidth(mBarWidth);
        mView.setAmps(mAmps);
        mView.setMode(1);
        mView.setCutterVisible(false);
        mView.setPlayPointerVisible(false);
    }

    public void stopRecord()
    {
        mView.setWaveformColor(mRecordingColor);
        mView.setMode(4);
    }

    public void surfaceChanged(SurfaceHolder surfaceholder, int i, int j, int k)
    {
        Log.d("", "surfaceChanged");
        mViewHeight = k;
        if (mRender != null)
        {
            mRender.surfaceChanged(surfaceholder, i, j, k);
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceholder)
    {
        Log.d("", "surfaceCreated");
        if (mRender == null || !mRender.isAlive())
        {
            mRender = new RenderThread(this, mView);
            mRender.start();
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceholder)
    {
        Log.d("", "surfaceDestroyed");
        if (mRender != null)
        {
            mRender.stopRender();
        }
        mRender = null;
    }
}
