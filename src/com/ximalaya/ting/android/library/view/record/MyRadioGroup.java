// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.record;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MyRadioGroup extends RadioGroup
    implements android.view.View.OnClickListener, android.view.ViewGroup.OnHierarchyChangeListener, android.widget.RadioGroup.OnCheckedChangeListener
{
    public static interface OnChildStatusChangeListener
    {

        public abstract void onCheck(RadioGroup radiogroup, View view);

        public abstract void onCheckChange(RadioGroup radiogroup, int i);
    }


    private OnChildStatusChangeListener mListener;

    public MyRadioGroup(Context context)
    {
        super(context);
        init();
    }

    public MyRadioGroup(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        init();
    }

    private void init()
    {
        setOnHierarchyChangeListener(this);
        setOnCheckedChangeListener(this);
    }

    public void onCheckedChanged(RadioGroup radiogroup, int i)
    {
        if (mListener != null)
        {
            mListener.onCheckChange(radiogroup, i);
        }
    }

    public void onChildViewAdded(View view, View view1)
    {
        if ((view instanceof MyRadioGroup) && (view1 instanceof RadioButton))
        {
            view1.setOnClickListener(this);
        }
    }

    public void onChildViewRemoved(View view, View view1)
    {
        if ((view instanceof MyRadioGroup) && (view1 instanceof RadioButton))
        {
            view1.setOnClickListener(null);
        }
    }

    public void onClick(View view)
    {
        if (mListener != null)
        {
            mListener.onCheck(this, view);
        }
    }

    public void setOnChildStatusChangeListener(OnChildStatusChangeListener onchildstatuschangelistener)
    {
        mListener = onchildstatuschangelistener;
    }
}
