// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.record;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.ximalaya.ting.android.library.util.BitmapUtils;
import com.ximalaya.ting.android.library.util.ToolUtilLib;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.library.view.record:
//            WaveformController

public class WaveformView extends SurfaceView
{
    public static interface ClickListener
    {

        public abstract void onClick(float f);
    }

    public static interface OnCutListener
    {

        public abstract void onDragging(float f);

        public abstract void onStartDrag(float f);

        public abstract void onStopDrag(float f);
    }


    public static final int MODE_EDIT = 2;
    public static final int MODE_IDLE = 0;
    public static final int MODE_PLAY = 3;
    public static final int MODE_RECORD = 1;
    public static final int MODE_STOP = 4;
    private List mAmps;
    private int mBackgroundColor;
    private int mBackgroundSoundColor;
    private int mBarPadding;
    private int mBarWidth;
    private List mColors;
    private WaveformController mController;
    private Bitmap mCutter;
    private Drawable mCutterDrawable;
    private volatile boolean mCutterVisible;
    private List mIndexs;
    private int mMark;
    private volatile int mMode;
    private volatile float mPlayPercent;
    private int mPlayPointerHeight;
    private volatile boolean mPlayPointerVisible;
    private int mPlayedColor;
    private Bitmap mPointer;
    private Drawable mPointerDrawable;
    private int mRecordingColor;
    private int mWaveformColor;

    public WaveformView(Context context)
    {
        this(context, null);
    }

    public WaveformView(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0);
    }

    public WaveformView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mMark = -1;
        mMode = 0;
        mIndexs = new ArrayList();
        mColors = new ArrayList();
        mBackgroundColor = -1;
        mRecordingColor = Color.parseColor("#ff6000");
        mBackgroundSoundColor = 0xff888888;
        mPlayedColor = 0xff0000ff;
        mCutterVisible = false;
        mPlayPointerVisible = false;
        parseAttr(context, attributeset, i);
        init();
    }

    private void init()
    {
        mController = new WaveformController(this);
        mController.setRecordingColor(mRecordingColor);
        mController.setBackgroundSoundClolor(mBackgroundSoundColor);
        mController.setPlayedColor(mPlayedColor);
        setKeepScreenOn(true);
        setFocusableInTouchMode(true);
        getHolder().addCallback(mController);
        setOnTouchListener(mController);
    }

    private void parseAttr(Context context, AttributeSet attributeset, int i)
    {
        if (attributeset != null)
        {
            context = context.obtainStyledAttributes(attributeset, com.ximalaya.ting.android.library.R.styleable.WaveformView);
            mBarWidth = context.getDimensionPixelSize(0, 3);
            mBarPadding = context.getDimensionPixelSize(1, 1);
            mRecordingColor = context.getColor(2, mRecordingColor);
            mBackgroundSoundColor = context.getColor(3, mBackgroundSoundColor);
            mPlayedColor = context.getColor(4, mPlayedColor);
            mBackgroundColor = context.getColor(5, mBackgroundColor);
            mCutterDrawable = context.getDrawable(6);
            mPlayPointerHeight = ToolUtilLib.dp2px(getContext(), 3F);
            context.recycle();
        }
    }

    void clearMarkColor()
    {
        mIndexs.clear();
        mColors.clear();
    }

    public List getAmps()
    {
        return mAmps;
    }

    public int getBackgroundColor()
    {
        return mBackgroundColor;
    }

    public int getBarPadding()
    {
        return mBarPadding;
    }

    public int getBarWidth()
    {
        return mBarWidth;
    }

    public WaveformController getController()
    {
        return mController;
    }

    public Bitmap getCutterBitmap()
    {
        if (mCutter == null)
        {
            mCutter = BitmapUtils.drawable2bitmap(mCutterDrawable);
        }
        return mCutter;
    }

    int getMarkedColor(int i)
    {
        for (int j = mIndexs.size() - 1; j >= 0; j--)
        {
            if (i >= ((Integer)mIndexs.get(j)).intValue())
            {
                return ((Integer)mColors.get(j)).intValue();
            }
        }

        return 0;
    }

    int getMarkedIndex()
    {
        return mMark;
    }

    public int getMode()
    {
        return mMode;
    }

    int getPlayPointerHeight()
    {
        return mPlayPointerHeight;
    }

    public boolean getPlayPointerVisible()
    {
        return mPlayPointerVisible;
    }

    float getPlayerProgress()
    {
        return mPlayPercent;
    }

    public Bitmap getPointerBitmap()
    {
        if (mPointer == null)
        {
            mPointer = BitmapUtils.drawable2bitmap(mPointerDrawable);
        }
        return mPointer;
    }

    int getWaveformColor()
    {
        return mWaveformColor;
    }

    public boolean isCutterVisible()
    {
        return mCutterVisible;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
    }

    public void onPause()
    {
        if (mController != null)
        {
            mController.onPause();
        }
    }

    public void onResume()
    {
        if (mController != null)
        {
            mController.onResume();
        }
    }

    public void setAmps(List list)
    {
        mAmps = list;
    }

    public void setBackgroundColor(int i)
    {
        super.setBackgroundColor(i);
        mBackgroundColor = i;
    }

    public void setBarPadding(int i)
    {
        mBarPadding = i;
    }

    public void setBarWidth(int i)
    {
        mBarWidth = i;
    }

    public void setClickListener(ClickListener clicklistener)
    {
        if (mController != null)
        {
            mController.setOnClickListener(clicklistener);
        }
    }

    public void setCutterDrawable(Drawable drawable)
    {
        if (drawable != null)
        {
            mCutter = null;
            mCutterDrawable = drawable;
            mCutter = getCutterBitmap();
            return;
        } else
        {
            mCutter = null;
            mCutterDrawable = null;
            return;
        }
    }

    public void setCutterResId(int i)
    {
        if (i > 0)
        {
            mCutter = null;
            mCutterDrawable = getResources().getDrawable(i);
            mCutter = getCutterBitmap();
            return;
        } else
        {
            mCutter = null;
            mCutterDrawable = null;
            return;
        }
    }

    public void setCutterVisible(boolean flag)
    {
        mCutterVisible = flag;
    }

    void setMarkedColor(int i)
    {
        int j;
        if (mAmps != null)
        {
            synchronized (mAmps)
            {
                j = mAmps.size();
            }
        } else
        {
            j = -1;
        }
        if (j > -1)
        {
            setMarkedWaveformColor(j, i);
        }
        return;
        exception;
        list;
        JVM INSTR monitorexit ;
        throw exception;
    }

    void setMarkedIndex(int i)
    {
        mMark = i;
    }

    void setMarkedWaveformColor(int i, int j)
    {
        synchronized (mIndexs)
        {
            if (mIndexs.size() > 0 && i < ((Integer)mIndexs.get(mIndexs.size() - 1)).intValue())
            {
                mIndexs.clear();
                mColors.clear();
            }
            mIndexs.add(Integer.valueOf(i));
            mColors.add(Integer.valueOf(j));
        }
        return;
        exception;
        list;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void setMode(int i)
    {
        mMode = i;
    }

    public void setOnCutListener(OnCutListener oncutlistener)
    {
        if (mController != null)
        {
            mController.setOnCutListener(oncutlistener);
        }
    }

    public void setPlayPointerVisible(boolean flag)
    {
        mPlayPointerVisible = flag;
    }

    public void setPointerDrawable(int i)
    {
        if (i > 0)
        {
            mPointerDrawable = getResources().getDrawable(i);
            mPointer = getPointerBitmap();
            return;
        } else
        {
            mPointerDrawable = null;
            mPointer = null;
            return;
        }
    }

    public void setWaveBarPadding(int i)
    {
        mBarPadding = i;
    }

    public void setWaveBarWidth(int i)
    {
        mBarWidth = i;
    }

    void setWaveformColor(int i)
    {
        mWaveformColor = i;
    }

    public void updatePlayerProgress(float f)
    {
        mPlayPercent = f;
    }
}
