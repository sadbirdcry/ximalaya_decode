// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.record;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Scroller;

public class TopDrawerView extends LinearLayout
{
    public static interface PullListener
    {

        public abstract void onPullDown(float f);

        public abstract void onPullEnd(boolean flag);

        public abstract void onPullUp(float f);

        public abstract void onPulling(boolean flag, float f);
    }


    private boolean mBeginDrag;
    private Context mContext;
    private boolean mDrawerDown;
    private int mDrawerHeight;
    private boolean mEnablePull;
    private boolean mFirst;
    private float mLastX;
    private float mLastY;
    private FrameLayout mMainContainer;
    private android.widget.LinearLayout.LayoutParams mMainContainerLp;
    private float mMinSlop;
    private int mScrollDuration;
    private float mScrollOffset;
    private Scroller mScroller;
    private FrameLayout mTopContainer;
    private android.widget.LinearLayout.LayoutParams mTopContainerLp;

    public TopDrawerView(Context context)
    {
        this(context, null);
    }

    public TopDrawerView(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0);
    }

    public TopDrawerView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset);
        mFirst = true;
        mScrollDuration = 300;
        mDrawerDown = false;
        mEnablePull = false;
        mBeginDrag = false;
        parseAttrs(context, attributeset, i);
        init();
    }

    private void init()
    {
        setOrientation(1);
        mMinSlop = ViewConfiguration.get(mContext).getScaledTouchSlop();
        mScroller = new Scroller(mContext);
        mTopContainer = new FrameLayout(mContext);
        mTopContainerLp = new android.widget.LinearLayout.LayoutParams(-1, -2);
        mMainContainer = new FrameLayout(mContext);
        mMainContainerLp = new android.widget.LinearLayout.LayoutParams(-1, -1);
        addViewInLayout(mTopContainer, 0, mTopContainerLp);
        addViewInLayout(mMainContainer, 1, mMainContainerLp);
    }

    private void parseAttrs(Context context, AttributeSet attributeset, int i)
    {
        mContext = context;
    }

    public void addDrawerView(View view)
    {
        mTopContainer.addView(view);
    }

    public void addDrawerView(View view, android.view.ViewGroup.LayoutParams layoutparams)
    {
        mTopContainer.addView(view, layoutparams);
    }

    public void addView(View view)
    {
        mMainContainer.addView(view);
    }

    public void addView(View view, int i)
    {
        mMainContainer.addView(view, i);
    }

    public void addView(View view, int i, int j)
    {
        mMainContainer.addView(view, i, j);
    }

    public void addView(View view, int i, android.view.ViewGroup.LayoutParams layoutparams)
    {
        mMainContainer.addView(view, i, layoutparams);
    }

    public void addView(View view, android.view.ViewGroup.LayoutParams layoutparams)
    {
        mMainContainer.addView(view, layoutparams);
    }

    public void computeScroll()
    {
        if (mScroller.computeScrollOffset())
        {
            scrollTo(mScroller.getCurrX(), mScroller.getCurrY());
            postInvalidate();
        }
        super.computeScroll();
    }

    public boolean isDrawerDown()
    {
        return mDrawerDown;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent)
    {
        if (!mEnablePull) goto _L2; else goto _L1
_L1:
        motionevent.getAction() & 0xff;
        JVM INSTR tableswitch 0 3: default 44
    //                   0 49
    //                   1 44
    //                   2 73
    //                   3 44;
           goto _L3 _L4 _L3 _L5 _L3
_L3:
        return mBeginDrag;
_L4:
        mLastX = motionevent.getX();
        mLastY = motionevent.getY();
        mBeginDrag = false;
        continue; /* Loop/switch isn't completed */
_L5:
        float f = motionevent.getX();
        float f1 = mLastX;
        float f2 = motionevent.getY() - mLastY;
        if (Math.abs(f2) > Math.abs(f - f1) && (mDrawerDown && f2 < 0.0F && f2 > (float)(-mDrawerHeight) || !mDrawerDown && f2 > 0.0F && f2 < (float)mDrawerHeight))
        {
            mBeginDrag = true;
        }
        if (true) goto _L3; else goto _L2
_L2:
        return super.onInterceptTouchEvent(motionevent);
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l)
    {
        super.onLayout(flag, i, j, k, l);
        if (mFirst)
        {
            mScroller.startScroll(mScroller.getFinalX(), mScroller.getFinalY(), 0, mDrawerHeight, 0);
            invalidate();
            mFirst = false;
        }
    }

    protected void onMeasure(int i, int j)
    {
        int k = android.view.View.MeasureSpec.getSize(i);
        int l = android.view.View.MeasureSpec.getSize(j);
        i = android.view.View.MeasureSpec.getMode(i);
        j = android.view.View.MeasureSpec.getMode(j);
        int i1 = k - getPaddingLeft() - getPaddingRight();
        int j1 = l - getPaddingTop() - getPaddingBottom();
        if (mDrawerHeight == 0 || mDrawerHeight > j1)
        {
            mDrawerHeight = j1;
        }
        mTopContainer.measure(android.view.View.MeasureSpec.makeMeasureSpec(i1, i), android.view.View.MeasureSpec.makeMeasureSpec(mDrawerHeight, 0x80000000));
        mDrawerHeight = mTopContainer.getMeasuredHeight();
        mMainContainer.measure(android.view.View.MeasureSpec.makeMeasureSpec(i1, i), android.view.View.MeasureSpec.makeMeasureSpec(j1, j));
        setMeasuredDimension(k, l);
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        boolean flag;
        boolean flag1;
        boolean flag2;
        flag = true;
        flag2 = true;
        flag1 = true;
        if (!mEnablePull) goto _L2; else goto _L1
_L1:
        float f;
        float f1;
        mFirst = false;
        if (!mScroller.isFinished())
        {
            mScroller.abortAnimation();
        }
        f = motionevent.getX();
        f1 = motionevent.getY();
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 84
    //                   0 94
    //                   1 319
    //                   2 111
    //                   3 319;
           goto _L2 _L3 _L4 _L5 _L4
_L2:
        flag = super.onTouchEvent(motionevent);
_L7:
        return flag;
_L3:
        mLastX = f;
        mLastY = f1;
        mScrollOffset = 0.0F;
        return true;
_L5:
        float f2 = mLastX;
        float f3 = f1 - mLastY;
        flag1 = flag2;
        if (Math.abs(f - f2) < Math.abs(f3))
        {
            if (mDrawerDown)
            {
                if (f3 > 0.0F || mScrollOffset + f3 < (float)(-mDrawerHeight))
                {
                    flag = false;
                }
            } else
            if (f3 < 0.0F || mScrollOffset + f3 > (float)mDrawerHeight)
            {
                flag = false;
            }
            flag1 = flag;
            if (flag)
            {
                mScrollOffset = mScrollOffset + f3;
                mScroller.startScroll(mScroller.getFinalX(), mScroller.getFinalY(), 0, (int)(-f3), mScrollDuration);
                invalidate();
                flag1 = flag;
            }
        }
        mLastX = f;
        mLastY = f1;
        Log.e("", (new StringBuilder()).append("scrollY ").append(getScrollY()).append(", ").append(mDrawerHeight).toString());
        return flag1;
_L4:
        int i = (int)Math.abs(mScrollOffset);
        if (mScrollOffset > 0.0F)
        {
            if (i >= mDrawerHeight / 3)
            {
                i = -(mDrawerHeight - i);
                mDrawerDown = true;
            }
        } else
        if (i < mDrawerHeight / 3)
        {
            i = -i;
        } else
        {
            i = mDrawerHeight - i;
            mDrawerDown = false;
        }
        mScroller.startScroll(mScroller.getFinalX(), mScroller.getFinalY(), 0, i, mScrollDuration);
        invalidate();
        mScrollOffset = 0.0F;
        flag = flag1;
        if (i == 0)
        {
            return false;
        }
        if (true) goto _L7; else goto _L6
_L6:
    }

    public void removeDrawerView(View view)
    {
        mTopContainer.removeView(view);
    }

    public void setDrawerDown(boolean flag)
    {
        if (mDrawerDown == flag)
        {
            return;
        }
        if (!mScroller.isFinished())
        {
            mScroller.abortAnimation();
        }
        Scroller scroller = mScroller;
        int j = mScroller.getFinalX();
        int k = mScroller.getCurrY();
        int i;
        if (flag)
        {
            i = -mDrawerHeight;
        } else
        {
            i = mDrawerHeight;
        }
        scroller.startScroll(j, k, 0, i, mScrollDuration);
        postInvalidate();
        mDrawerDown = flag;
    }

    public void setEnablePull(boolean flag)
    {
        mEnablePull = flag;
    }

    public void setScrollDuration(int i)
    {
        mScrollDuration = i;
    }
}
