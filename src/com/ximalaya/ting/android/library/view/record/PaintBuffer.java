// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.record;

import android.graphics.Paint;
import java.util.ArrayList;
import java.util.List;

public class PaintBuffer
{

    private static final int MAX_CACHE_SIZE = 5;
    private static List sCache = new ArrayList();

    public PaintBuffer()
    {
    }

    public static void clearCache()
    {
        synchronized (sCache)
        {
            sCache.clear();
        }
        return;
        exception;
        list;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public static Paint obtain()
    {
        Paint paint = null;
        synchronized (sCache)
        {
            if (sCache.size() > 0)
            {
                paint = (Paint)sCache.remove(0);
            }
        }
        obj = paint;
        if (paint == null)
        {
            obj = new Paint();
        }
        resetPaint(((Paint) (obj)));
        return ((Paint) (obj));
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public static void recycle(Paint paint)
    {
        synchronized (sCache)
        {
            if (sCache.size() < 5)
            {
                sCache.add(paint);
            }
        }
        return;
        paint;
        list;
        JVM INSTR monitorexit ;
        throw paint;
    }

    private static void resetPaint(Paint paint)
    {
        paint.setAlpha(255);
        paint.setAntiAlias(true);
        paint.clearShadowLayer();
    }

}
