// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view;

import android.content.Context;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.WrapperListAdapter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;

public class GridViewWithHeaderAndFooter extends GridView
{
    private static class a
    {

        public View a;
        public ViewGroup b;
        public Object c;
        public boolean d;

        private a()
        {
        }

    }

    private class b extends FrameLayout
    {

        final GridViewWithHeaderAndFooter a;

        protected void onLayout(boolean flag, int i, int j, int k, int l)
        {
            int i1 = a.getPaddingLeft() + getPaddingLeft();
            if (i1 != i)
            {
                offsetLeftAndRight(i1 - i);
            }
            super.onLayout(flag, i, j, k, l);
        }

        protected void onMeasure(int i, int j)
        {
            super.onMeasure(android.view.View.MeasureSpec.makeMeasureSpec(a.getMeasuredWidth() - a.getPaddingLeft() - a.getPaddingRight(), android.view.View.MeasureSpec.getMode(i)), j);
        }

        public b(Context context)
        {
            a = GridViewWithHeaderAndFooter.this;
            super(context);
        }
    }

    private static class c
        implements Filterable, WrapperListAdapter
    {

        static final ArrayList a = new ArrayList();
        ArrayList b;
        ArrayList c;
        boolean d;
        private final DataSetObservable e = new DataSetObservable();
        private final ListAdapter f;
        private int g;
        private int h;
        private final boolean i;
        private boolean j;
        private boolean k;

        private boolean a(ArrayList arraylist)
        {
label0:
            {
                if (arraylist == null)
                {
                    break label0;
                }
                arraylist = arraylist.iterator();
                do
                {
                    if (!arraylist.hasNext())
                    {
                        break label0;
                    }
                } while (((a)arraylist.next()).d);
                return false;
            }
            return true;
        }

        private int d()
        {
            return (int)(Math.ceil((1.0F * (float)f.getCount()) / (float)g) * (double)g);
        }

        public int a()
        {
            return b.size();
        }

        public void a(int l)
        {
            while (l < 1 || g == l) 
            {
                return;
            }
            g = l;
            c();
        }

        public boolean a(View view)
        {
            boolean flag1 = false;
            for (int l = 0; l < b.size(); l++)
            {
                if (((a)b.get(l)).a == view)
                {
                    b.remove(l);
                    boolean flag = flag1;
                    if (a(b))
                    {
                        flag = flag1;
                        if (a(c))
                        {
                            flag = true;
                        }
                    }
                    d = flag;
                    e.notifyChanged();
                    return true;
                }
            }

            return false;
        }

        public boolean areAllItemsEnabled()
        {
            return f == null || d && f.areAllItemsEnabled();
        }

        public int b()
        {
            return c.size();
        }

        public void b(int l)
        {
            h = l;
        }

        public boolean b(View view)
        {
            boolean flag1 = false;
            for (int l = 0; l < c.size(); l++)
            {
                if (((a)c.get(l)).a == view)
                {
                    c.remove(l);
                    boolean flag = flag1;
                    if (a(b))
                    {
                        flag = flag1;
                        if (a(c))
                        {
                            flag = true;
                        }
                    }
                    d = flag;
                    e.notifyChanged();
                    return true;
                }
            }

            return false;
        }

        public void c()
        {
            e.notifyChanged();
        }

        public int getCount()
        {
            if (f != null)
            {
                return (b() + a()) * g + d();
            } else
            {
                return (b() + a()) * g;
            }
        }

        public Filter getFilter()
        {
            if (i)
            {
                return ((Filterable)f).getFilter();
            } else
            {
                return null;
            }
        }

        public Object getItem(int l)
        {
            int i1 = a() * g;
            if (l < i1)
            {
                if (l % g == 0)
                {
                    return ((a)b.get(l / g)).c;
                } else
                {
                    return null;
                }
            }
            int k1 = l - i1;
            l = 0;
            if (f != null)
            {
                int j1 = d();
                l = j1;
                if (k1 < j1)
                {
                    if (k1 < f.getCount())
                    {
                        return f.getItem(k1);
                    } else
                    {
                        return null;
                    }
                }
            }
            l = k1 - l;
            if (l % g == 0)
            {
                return ((a)c.get(l)).c;
            } else
            {
                return null;
            }
        }

        public long getItemId(int l)
        {
            int i1 = a() * g;
            if (f != null && l >= i1)
            {
                l -= i1;
                if (l < f.getCount())
                {
                    return f.getItemId(l);
                }
            }
            return -1L;
        }

        public int getItemViewType(int l)
        {
            int i1;
            int j1;
            int k1;
            int l1 = a() * g;
            int i2;
            if (f == null)
            {
                k1 = 0;
            } else
            {
                k1 = f.getViewTypeCount() - 1;
            }
            j1 = -2;
            i1 = j1;
            if (j)
            {
                i1 = j1;
                if (l < l1)
                {
                    i1 = j1;
                    if (l == 0)
                    {
                        i1 = j1;
                        if (k)
                        {
                            i1 = b.size() + k1 + c.size() + 1 + 1;
                        }
                    }
                    if (l % g != 0)
                    {
                        i1 = l / g + 1 + k1;
                    }
                }
            }
            i2 = l - l1;
            if (f == null) goto _L2; else goto _L1
_L1:
            j1 = d();
            if (i2 < 0 || i2 >= j1) goto _L4; else goto _L3
_L3:
            if (i2 >= f.getCount()) goto _L6; else goto _L5
_L5:
            i1 = f.getItemViewType(i2);
_L4:
            l1 = i1;
            if (j)
            {
                j1 = i2 - j1;
                l1 = i1;
                if (j1 >= 0)
                {
                    l1 = i1;
                    if (j1 < getCount())
                    {
                        l1 = i1;
                        if (j1 % g != 0)
                        {
                            l1 = k1 + b.size() + 1 + (j1 / g + 1);
                        }
                    }
                }
            }
            if (GridViewWithHeaderAndFooter.DEBUG)
            {
                Log.d("GridViewHeaderAndFooter", String.format("getItemViewType: pos: %s, result: %s", new Object[] {
                    Integer.valueOf(l), Integer.valueOf(l1), Boolean.valueOf(j), Boolean.valueOf(k)
                }));
            }
            return l1;
_L6:
            if (j)
            {
                i1 = b.size() + k1 + 1;
            }
            continue; /* Loop/switch isn't completed */
_L2:
            j1 = 0;
            if (true) goto _L4; else goto _L7
_L7:
        }

        public View getView(int l, View view, ViewGroup viewgroup)
        {
            int i1 = 0;
            int j1;
            if (GridViewWithHeaderAndFooter.DEBUG)
            {
                boolean flag;
                if (view == null)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                Log.d("GridViewHeaderAndFooter", String.format("getView: %s, reused: %s", new Object[] {
                    Integer.valueOf(l), Boolean.valueOf(flag)
                }));
            }
            j1 = a() * g;
            if (l < j1)
            {
                ViewGroup viewgroup1 = ((a)b.get(l / g)).b;
                if (l % g == 0)
                {
                    return viewgroup1;
                }
                View view1 = view;
                if (view == null)
                {
                    view1 = new View(viewgroup.getContext());
                }
                view1.setVisibility(4);
                view1.setMinimumHeight(viewgroup1.getHeight());
                return view1;
            }
            int l1 = l - j1;
            if (f != null)
            {
                int k1 = d();
                i1 = k1;
                if (l1 < k1)
                {
                    if (l1 < f.getCount())
                    {
                        return f.getView(l1, view, viewgroup);
                    }
                    View view2 = view;
                    if (view == null)
                    {
                        view2 = new View(viewgroup.getContext());
                    }
                    view2.setVisibility(4);
                    view2.setMinimumHeight(h);
                    return view2;
                }
            }
            i1 = l1 - i1;
            if (i1 < getCount())
            {
                ViewGroup viewgroup2 = ((a)c.get(i1 / g)).b;
                if (l % g == 0)
                {
                    return viewgroup2;
                }
                View view3 = view;
                if (view == null)
                {
                    view3 = new View(viewgroup.getContext());
                }
                view3.setVisibility(4);
                view3.setMinimumHeight(viewgroup2.getHeight());
                return view3;
            } else
            {
                throw new ArrayIndexOutOfBoundsException(l);
            }
        }

        public int getViewTypeCount()
        {
            int l;
            int i1;
            if (f == null)
            {
                l = 1;
            } else
            {
                l = f.getViewTypeCount();
            }
            i1 = l;
            if (j)
            {
                int j1 = b.size() + 1 + c.size();
                i1 = j1;
                if (k)
                {
                    i1 = j1 + 1;
                }
                i1 = l + i1;
            }
            if (GridViewWithHeaderAndFooter.DEBUG)
            {
                Log.d("GridViewHeaderAndFooter", String.format("getViewTypeCount: %s", new Object[] {
                    Integer.valueOf(i1)
                }));
            }
            return i1;
        }

        public ListAdapter getWrappedAdapter()
        {
            return f;
        }

        public boolean hasStableIds()
        {
            return f != null && f.hasStableIds();
        }

        public boolean isEmpty()
        {
            return f == null || f.isEmpty();
        }

        public boolean isEnabled(int l)
        {
            int i1;
            boolean flag;
            flag = true;
            i1 = a() * g;
            if (l >= i1) goto _L2; else goto _L1
_L1:
            if (l % g == 0 && ((a)b.get(l / g)).d)
            {
                flag = true;
            } else
            {
                flag = false;
            }
_L4:
            return flag;
_L2:
            int k1 = l - i1;
            if (f != null)
            {
                int j1 = d();
                l = j1;
                if (k1 < j1)
                {
                    if (k1 >= f.getCount() || !f.isEnabled(k1))
                    {
                        return false;
                    }
                    continue; /* Loop/switch isn't completed */
                }
            } else
            {
                l = 0;
            }
            l = k1 - l;
            if (l % g != 0 || !((a)c.get(l / g)).d)
            {
                return false;
            }
            if (true) goto _L4; else goto _L3
_L3:
        }

        public void registerDataSetObserver(DataSetObserver datasetobserver)
        {
            e.registerObserver(datasetobserver);
            if (f != null)
            {
                f.registerDataSetObserver(datasetobserver);
            }
        }

        public void unregisterDataSetObserver(DataSetObserver datasetobserver)
        {
            e.unregisterObserver(datasetobserver);
            if (f != null)
            {
                f.unregisterDataSetObserver(datasetobserver);
            }
        }


        public c(ArrayList arraylist, ArrayList arraylist1, ListAdapter listadapter)
        {
            boolean flag = true;
            super();
            g = 1;
            h = -1;
            j = true;
            k = false;
            f = listadapter;
            i = listadapter instanceof Filterable;
            if (arraylist == null)
            {
                b = a;
            } else
            {
                b = arraylist;
            }
            if (arraylist1 == null)
            {
                c = a;
            } else
            {
                c = arraylist1;
            }
            if (!a(b) || !a(c))
            {
                flag = false;
            }
            d = flag;
        }
    }

    private class d
        implements android.widget.AdapterView.OnItemClickListener, android.widget.AdapterView.OnItemLongClickListener
    {

        final GridViewWithHeaderAndFooter a;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (a.mOnItemClickListener != null)
            {
                i -= a.getHeaderViewCount() * a.getNumColumnsCompatible();
                if (i >= 0)
                {
                    a.mOnItemClickListener.onItemClick(adapterview, view, i, l);
                }
            }
        }

        public boolean onItemLongClick(AdapterView adapterview, View view, int i, long l)
        {
            if (a.mOnItemLongClickListener != null)
            {
                i -= a.getHeaderViewCount() * a.getNumColumnsCompatible();
                if (i >= 0)
                {
                    a.mOnItemLongClickListener.onItemLongClick(adapterview, view, i, l);
                }
            }
            return true;
        }

        private d()
        {
            a = GridViewWithHeaderAndFooter.this;
            super();
        }

    }


    public static boolean DEBUG = false;
    private static final String LOG_TAG = "GridViewHeaderAndFooter";
    private ArrayList mFooterViewInfos;
    private ArrayList mHeaderViewInfos;
    private d mItemClickHandler;
    private int mNumColumns;
    private android.widget.AdapterView.OnItemClickListener mOnItemClickListener;
    private android.widget.AdapterView.OnItemLongClickListener mOnItemLongClickListener;
    private ListAdapter mOriginalAdapter;
    private int mRowHeight;
    private View mViewForMeasureRowHeight;

    public GridViewWithHeaderAndFooter(Context context)
    {
        super(context);
        mNumColumns = -1;
        mViewForMeasureRowHeight = null;
        mRowHeight = -1;
        mHeaderViewInfos = new ArrayList();
        mFooterViewInfos = new ArrayList();
        initHeaderGridView();
    }

    public GridViewWithHeaderAndFooter(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        mNumColumns = -1;
        mViewForMeasureRowHeight = null;
        mRowHeight = -1;
        mHeaderViewInfos = new ArrayList();
        mFooterViewInfos = new ArrayList();
        initHeaderGridView();
    }

    public GridViewWithHeaderAndFooter(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mNumColumns = -1;
        mViewForMeasureRowHeight = null;
        mRowHeight = -1;
        mHeaderViewInfos = new ArrayList();
        mFooterViewInfos = new ArrayList();
        initHeaderGridView();
    }

    private int getColumnWidthCompatible()
    {
        if (android.os.Build.VERSION.SDK_INT >= 16)
        {
            return super.getColumnWidth();
        }
        int i;
        try
        {
            Field field = android/widget/GridView.getDeclaredField("mColumnWidth");
            field.setAccessible(true);
            i = field.getInt(this);
        }
        catch (NoSuchFieldException nosuchfieldexception)
        {
            throw new RuntimeException(nosuchfieldexception);
        }
        catch (IllegalAccessException illegalaccessexception)
        {
            throw new RuntimeException(illegalaccessexception);
        }
        return i;
    }

    private d getItemClickHandler()
    {
        if (mItemClickHandler == null)
        {
            mItemClickHandler = new d();
        }
        return mItemClickHandler;
    }

    private int getNumColumnsCompatible()
    {
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            return super.getNumColumns();
        }
        int i;
        try
        {
            Field field = android/widget/GridView.getDeclaredField("mNumColumns");
            field.setAccessible(true);
            i = field.getInt(this);
        }
        catch (Exception exception)
        {
            if (mNumColumns != -1)
            {
                return mNumColumns;
            } else
            {
                throw new RuntimeException("Can not determine the mNumColumns for this API platform, please call setNumColumns to set it.");
            }
        }
        return i;
    }

    private void initHeaderGridView()
    {
    }

    private void removeFixedViewInfo(View view, ArrayList arraylist)
    {
        int j = arraylist.size();
        int i = 0;
        do
        {
label0:
            {
                if (i < j)
                {
                    if (((a)arraylist.get(i)).a != view)
                    {
                        break label0;
                    }
                    arraylist.remove(i);
                }
                return;
            }
            i++;
        } while (true);
    }

    public void addFooterView(View view)
    {
        addFooterView(view, null, true);
    }

    public void addFooterView(View view, Object obj, boolean flag)
    {
        ListAdapter listadapter = getAdapter();
        if (listadapter != null && !(listadapter instanceof c))
        {
            throw new IllegalStateException("Cannot add header view to grid -- setAdapter has already been called.");
        }
        android.view.ViewGroup.LayoutParams layoutparams = view.getLayoutParams();
        a a1 = new a();
        b b1 = new b(getContext());
        if (layoutparams != null)
        {
            view.setLayoutParams(new android.widget.FrameLayout.LayoutParams(layoutparams.width, layoutparams.height));
            b1.setLayoutParams(new android.widget.AbsListView.LayoutParams(layoutparams.width, layoutparams.height));
        }
        b1.addView(view);
        a1.a = view;
        a1.b = b1;
        a1.c = obj;
        a1.d = flag;
        mFooterViewInfos.add(a1);
        if (listadapter != null)
        {
            ((c)listadapter).c();
        }
    }

    public void addHeaderView(View view)
    {
        addHeaderView(view, null, true);
    }

    public void addHeaderView(View view, Object obj, boolean flag)
    {
        ListAdapter listadapter = getAdapter();
        if (listadapter != null && !(listadapter instanceof c))
        {
            throw new IllegalStateException("Cannot add header view to grid -- setAdapter has already been called.");
        }
        android.view.ViewGroup.LayoutParams layoutparams = view.getLayoutParams();
        a a1 = new a();
        b b1 = new b(getContext());
        if (layoutparams != null)
        {
            view.setLayoutParams(new android.widget.FrameLayout.LayoutParams(layoutparams.width, layoutparams.height));
            b1.setLayoutParams(new android.widget.AbsListView.LayoutParams(layoutparams.width, layoutparams.height));
        }
        b1.addView(view);
        a1.a = view;
        a1.b = b1;
        a1.c = obj;
        a1.d = flag;
        mHeaderViewInfos.add(a1);
        if (listadapter != null)
        {
            ((c)listadapter).c();
        }
    }

    public int getFooterViewCount()
    {
        return mFooterViewInfos.size();
    }

    public int getHeaderHeight(int i)
    {
        if (i >= 0)
        {
            return ((a)mHeaderViewInfos.get(i)).a.getMeasuredHeight();
        } else
        {
            return 0;
        }
    }

    public int getHeaderViewCount()
    {
        return mHeaderViewInfos.size();
    }

    public int getHorizontalSpacing()
    {
        int i;
        try
        {
            if (android.os.Build.VERSION.SDK_INT < 16)
            {
                Field field = android/widget/GridView.getDeclaredField("mHorizontalSpacing");
                field.setAccessible(true);
                return field.getInt(this);
            }
            i = super.getHorizontalSpacing();
        }
        catch (Exception exception)
        {
            return 0;
        }
        return i;
    }

    public ListAdapter getOriginalAdapter()
    {
        return mOriginalAdapter;
    }

    public int getRowHeight()
    {
        if (mRowHeight > 0)
        {
            return mRowHeight;
        }
        Object obj = getAdapter();
        int j = getNumColumnsCompatible();
        if (obj == null || ((ListAdapter) (obj)).getCount() <= (mHeaderViewInfos.size() + mFooterViewInfos.size()) * j)
        {
            return -1;
        }
        int i = getColumnWidthCompatible();
        View view = getAdapter().getView(j * mHeaderViewInfos.size(), mViewForMeasureRowHeight, this);
        android.widget.AbsListView.LayoutParams layoutparams = (android.widget.AbsListView.LayoutParams)view.getLayoutParams();
        obj = layoutparams;
        if (layoutparams == null)
        {
            obj = new android.widget.AbsListView.LayoutParams(-1, -2, 0);
            view.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
        }
        j = getChildMeasureSpec(android.view.View.MeasureSpec.makeMeasureSpec(0, 0), 0, ((android.widget.AbsListView.LayoutParams) (obj)).height);
        view.measure(getChildMeasureSpec(android.view.View.MeasureSpec.makeMeasureSpec(i, 0x40000000), 0, ((android.widget.AbsListView.LayoutParams) (obj)).width), j);
        mViewForMeasureRowHeight = view;
        mRowHeight = view.getMeasuredHeight();
        return mRowHeight;
    }

    public int getVerticalSpacing()
    {
        int i;
        try
        {
            if (android.os.Build.VERSION.SDK_INT < 16)
            {
                Field field = android/widget/GridView.getDeclaredField("mVerticalSpacing");
                field.setAccessible(true);
                return field.getInt(this);
            }
            i = super.getVerticalSpacing();
        }
        catch (Exception exception)
        {
            return 0;
        }
        return i;
    }

    public void invalidateRowHeight()
    {
        mRowHeight = -1;
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        mViewForMeasureRowHeight = null;
    }

    protected void onMeasure(int i, int j)
    {
        super.onMeasure(i, j);
        ListAdapter listadapter = getAdapter();
        if (listadapter != null && (listadapter instanceof c))
        {
            ((c)listadapter).a(getNumColumnsCompatible());
            ((c)listadapter).b(getRowHeight());
        }
    }

    public boolean removeFooterView(View view)
    {
        if (mFooterViewInfos.size() > 0)
        {
            ListAdapter listadapter = getAdapter();
            boolean flag;
            if (listadapter != null && ((c)listadapter).b(view))
            {
                flag = true;
            } else
            {
                flag = false;
            }
            removeFixedViewInfo(view, mFooterViewInfos);
            return flag;
        } else
        {
            return false;
        }
    }

    public boolean removeHeaderView(View view)
    {
        if (mHeaderViewInfos.size() > 0)
        {
            ListAdapter listadapter = getAdapter();
            boolean flag;
            if (listadapter != null && ((c)listadapter).a(view))
            {
                flag = true;
            } else
            {
                flag = false;
            }
            removeFixedViewInfo(view, mHeaderViewInfos);
            return flag;
        } else
        {
            return false;
        }
    }

    public volatile void setAdapter(Adapter adapter)
    {
        setAdapter((ListAdapter)adapter);
    }

    public void setAdapter(ListAdapter listadapter)
    {
        mOriginalAdapter = listadapter;
        if (mHeaderViewInfos.size() > 0 || mFooterViewInfos.size() > 0)
        {
            listadapter = new c(mHeaderViewInfos, mFooterViewInfos, listadapter);
            int i = getNumColumnsCompatible();
            if (i > 1)
            {
                listadapter.a(i);
            }
            listadapter.b(getRowHeight());
            super.setAdapter(listadapter);
            return;
        } else
        {
            super.setAdapter(listadapter);
            return;
        }
    }

    public void setClipChildren(boolean flag)
    {
    }

    public void setClipChildrenSupper(boolean flag)
    {
        super.setClipChildren(false);
    }

    public void setNumColumns(int i)
    {
        super.setNumColumns(i);
        mNumColumns = i;
        ListAdapter listadapter = getAdapter();
        if (listadapter != null && (listadapter instanceof c))
        {
            ((c)listadapter).a(i);
        }
    }

    public void setOnItemClickListener(android.widget.AdapterView.OnItemClickListener onitemclicklistener)
    {
        mOnItemClickListener = onitemclicklistener;
        super.setOnItemClickListener(getItemClickHandler());
    }

    public void setOnItemLongClickListener(android.widget.AdapterView.OnItemLongClickListener onitemlongclicklistener)
    {
        mOnItemLongClickListener = onitemlongclicklistener;
        super.setOnItemLongClickListener(getItemClickHandler());
    }

    public void tryToScrollToBottomSmoothly()
    {
        int i = getAdapter().getCount() - 1;
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            smoothScrollToPositionFromTop(i, 0);
            return;
        } else
        {
            setSelection(i);
            return;
        }
    }

    public void tryToScrollToBottomSmoothly(int i)
    {
        int j = getAdapter().getCount() - 1;
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            smoothScrollToPositionFromTop(j, 0, i);
            return;
        } else
        {
            setSelection(j);
            return;
        }
    }

    static 
    {
        DEBUG = false;
    }



}
