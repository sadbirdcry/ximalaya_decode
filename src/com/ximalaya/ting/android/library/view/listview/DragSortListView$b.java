// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.listview;

import android.os.SystemClock;
import android.view.View;

// Referenced classes of package com.ximalaya.ting.android.library.view.listview:
//            DragSortListView

private class j
    implements Runnable
{

    final DragSortListView a;
    private boolean b;
    private long c;
    private long d;
    private int e;
    private float f;
    private long g;
    private int h;
    private float i;
    private boolean j;

    public void a(int k)
    {
        if (!j)
        {
            b = false;
            j = true;
            g = SystemClock.uptimeMillis();
            c = g;
            h = k;
            a.post(this);
        }
    }

    public void a(boolean flag)
    {
        if (flag)
        {
            a.removeCallbacks(this);
            j = false;
            return;
        } else
        {
            b = true;
            return;
        }
    }

    public boolean a()
    {
        return j;
    }

    public int b()
    {
        if (j)
        {
            return h;
        } else
        {
            return -1;
        }
    }

    public void run()
    {
        if (b)
        {
            j = false;
            return;
        }
        int l = a.getFirstVisiblePosition();
        int k = a.getLastVisiblePosition();
        int j1 = a.getCount();
        int i1 = a.getPaddingTop();
        int k1 = a.getHeight() - i1 - a.getPaddingBottom();
        int l1 = Math.min(DragSortListView.access$600(a), DragSortListView.access$2000(a) + DragSortListView.access$400(a));
        int i2 = Math.max(DragSortListView.access$600(a), DragSortListView.access$2000(a) - DragSortListView.access$400(a));
        View view1;
        if (h == 0)
        {
            View view = a.getChildAt(0);
            if (view == null)
            {
                j = false;
                return;
            }
            if (l == 0 && view.getTop() == i1)
            {
                j = false;
                return;
            }
            i = DragSortListView.access$2300(a).getSpeed((DragSortListView.access$2100(a) - (float)i2) / DragSortListView.access$2200(a), c);
        } else
        {
            View view2 = a.getChildAt(k - l);
            if (view2 == null)
            {
                j = false;
                return;
            }
            if (k == j1 - 1 && view2.getBottom() <= k1 + i1)
            {
                j = false;
                return;
            }
            i = -DragSortListView.access$2300(a).getSpeed(((float)l1 - DragSortListView.access$2400(a)) / DragSortListView.access$2500(a), c);
        }
        d = SystemClock.uptimeMillis();
        f = d - c;
        e = Math.round(i * f);
        if (e >= 0)
        {
            e = Math.min(k1, e);
            k = l;
        } else
        {
            e = Math.max(-k1, e);
        }
        view1 = a.getChildAt(k - l);
        j1 = view1.getTop() + e;
        l = j1;
        if (k == 0)
        {
            l = j1;
            if (j1 > i1)
            {
                l = i1;
            }
        }
        DragSortListView.access$2602(a, true);
        a.setSelectionFromTop(k, l - i1);
        a.layoutChildren();
        a.invalidate();
        DragSortListView.access$2602(a, false);
        DragSortListView.access$2700(a, k, view1, false);
        c = d;
        a.post(this);
    }

    public agScrollProfile(DragSortListView dragsortlistview)
    {
        a = dragsortlistview;
        super();
        j = false;
    }
}
