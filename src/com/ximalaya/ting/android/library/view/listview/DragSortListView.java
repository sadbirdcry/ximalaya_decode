// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.listview;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.Checkable;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

// Referenced classes of package com.ximalaya.ting.android.library.view.listview:
//            b, DragSortController, c, DragSortItemView, 
//            d, DragSortItemViewCheckable

public class DragSortListView extends ListView
{
    public static interface DragListener
    {

        public abstract void drag(int i, int j);
    }

    public static interface DragScrollProfile
    {

        public abstract float getSpeed(float f1, long l);
    }

    public static interface DragSortListener
        extends DragListener, DropListener, RemoveListener
    {
    }

    public static interface DropListener
    {

        public abstract void drop(int i, int j);
    }

    public static interface FloatViewManager
    {

        public abstract View onCreateFloatView(int i);

        public abstract void onDestroyFloatView(View view);

        public abstract void onDragFloatView(View view, Point point, Point point1);
    }

    public static interface RemoveListener
    {

        public abstract void remove(int i);
    }

    private class a extends BaseAdapter
    {

        final DragSortListView a;
        private ListAdapter b;

        public ListAdapter a()
        {
            return b;
        }

        public boolean areAllItemsEnabled()
        {
            return b.areAllItemsEnabled();
        }

        public int getCount()
        {
            return b.getCount();
        }

        public Object getItem(int i)
        {
            return b.getItem(i);
        }

        public long getItemId(int i)
        {
            return b.getItemId(i);
        }

        public int getItemViewType(int i)
        {
            return b.getItemViewType(i);
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            if (view != null)
            {
                viewgroup = (DragSortItemView)view;
                View view1 = viewgroup.getChildAt(0);
                View view2 = b.getView(i, view1, a);
                view = viewgroup;
                if (view2 != view1)
                {
                    if (view1 != null)
                    {
                        viewgroup.removeViewAt(0);
                    }
                    viewgroup.addView(view2);
                    view = viewgroup;
                }
            } else
            {
                viewgroup = b.getView(i, null, a);
                if (viewgroup instanceof Checkable)
                {
                    view = new DragSortItemViewCheckable(a.getContext());
                } else
                {
                    view = new DragSortItemView(a.getContext());
                }
                view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, -2));
                view.addView(viewgroup);
            }
            a.adjustItem(a.getHeaderViewsCount() + i, view, true);
            return view;
        }

        public int getViewTypeCount()
        {
            return b.getViewTypeCount();
        }

        public boolean hasStableIds()
        {
            return b.hasStableIds();
        }

        public boolean isEmpty()
        {
            return b.isEmpty();
        }

        public boolean isEnabled(int i)
        {
            return b.isEnabled(i);
        }

        public a(ListAdapter listadapter)
        {
            a = DragSortListView.this;
            super();
            b = listadapter;
            b.registerDataSetObserver(new com.ximalaya.ting.android.library.view.listview.d(this, DragSortListView.this));
        }
    }

    private class b
        implements Runnable
    {

        final DragSortListView a;
        private boolean b;
        private long c;
        private long d;
        private int e;
        private float f;
        private long g;
        private int h;
        private float i;
        private boolean j;

        public void a(int k)
        {
            if (!j)
            {
                b = false;
                j = true;
                g = SystemClock.uptimeMillis();
                c = g;
                h = k;
                a.post(this);
            }
        }

        public void a(boolean flag)
        {
            if (flag)
            {
                a.removeCallbacks(this);
                j = false;
                return;
            } else
            {
                b = true;
                return;
            }
        }

        public boolean a()
        {
            return j;
        }

        public int b()
        {
            if (j)
            {
                return h;
            } else
            {
                return -1;
            }
        }

        public void run()
        {
            if (b)
            {
                j = false;
                return;
            }
            int l = a.getFirstVisiblePosition();
            int k = a.getLastVisiblePosition();
            int j1 = a.getCount();
            int i1 = a.getPaddingTop();
            int k1 = a.getHeight() - i1 - a.getPaddingBottom();
            int l1 = Math.min(a.mY, a.mFloatViewMid + a.mFloatViewHeightHalf);
            int i2 = Math.max(a.mY, a.mFloatViewMid - a.mFloatViewHeightHalf);
            View view1;
            if (h == 0)
            {
                View view = a.getChildAt(0);
                if (view == null)
                {
                    j = false;
                    return;
                }
                if (l == 0 && view.getTop() == i1)
                {
                    j = false;
                    return;
                }
                i = a.mScrollProfile.getSpeed((a.mUpScrollStartYF - (float)i2) / a.mDragUpScrollHeight, c);
            } else
            {
                View view2 = a.getChildAt(k - l);
                if (view2 == null)
                {
                    j = false;
                    return;
                }
                if (k == j1 - 1 && view2.getBottom() <= k1 + i1)
                {
                    j = false;
                    return;
                }
                i = -a.mScrollProfile.getSpeed(((float)l1 - a.mDownScrollStartYF) / a.mDragDownScrollHeight, c);
            }
            d = SystemClock.uptimeMillis();
            f = d - c;
            e = Math.round(i * f);
            if (e >= 0)
            {
                e = Math.min(k1, e);
                k = l;
            } else
            {
                e = Math.max(-k1, e);
            }
            view1 = a.getChildAt(k - l);
            j1 = view1.getTop() + e;
            l = j1;
            if (k == 0)
            {
                l = j1;
                if (j1 > i1)
                {
                    l = i1;
                }
            }
            a.mBlockLayoutRequests = true;
            a.setSelectionFromTop(k, l - i1);
            a.layoutChildren();
            a.invalidate();
            a.mBlockLayoutRequests = false;
            a.doDragFloatView(k, view1, false);
            c = d;
            a.post(this);
        }

        public b()
        {
            a = DragSortListView.this;
            super();
            j = false;
        }
    }

    private class c
    {

        StringBuilder a;
        File b;
        final DragSortListView c;
        private int d;
        private int e;
        private boolean f;

        public void a()
        {
            a.append("<DSLVStates>\n");
            e = 0;
            f = true;
        }

        public void b()
        {
            if (f)
            {
                a.append("<DSLVState>\n");
                int i1 = c.getChildCount();
                int j1 = c.getFirstVisiblePosition();
                a.append("    <Positions>");
                for (int i = 0; i < i1; i++)
                {
                    a.append(j1 + i).append(",");
                }

                a.append("</Positions>\n");
                a.append("    <Tops>");
                for (int j = 0; j < i1; j++)
                {
                    a.append(c.getChildAt(j).getTop()).append(",");
                }

                a.append("</Tops>\n");
                a.append("    <Bottoms>");
                for (int k = 0; k < i1; k++)
                {
                    a.append(c.getChildAt(k).getBottom()).append(",");
                }

                a.append("</Bottoms>\n");
                a.append("    <FirstExpPos>").append(c.mFirstExpPos).append("</FirstExpPos>\n");
                a.append("    <FirstExpBlankHeight>").append(c.getItemHeight(c.mFirstExpPos) - c.getChildHeight(c.mFirstExpPos)).append("</FirstExpBlankHeight>\n");
                a.append("    <SecondExpPos>").append(c.mSecondExpPos).append("</SecondExpPos>\n");
                a.append("    <SecondExpBlankHeight>").append(c.getItemHeight(c.mSecondExpPos) - c.getChildHeight(c.mSecondExpPos)).append("</SecondExpBlankHeight>\n");
                a.append("    <SrcPos>").append(c.mSrcPos).append("</SrcPos>\n");
                a.append("    <SrcHeight>").append(c.mFloatViewHeight + c.getDividerHeight()).append("</SrcHeight>\n");
                a.append("    <ViewHeight>").append(c.getHeight()).append("</ViewHeight>\n");
                a.append("    <LastY>").append(c.mLastY).append("</LastY>\n");
                a.append("    <FloatY>").append(c.mFloatViewMid).append("</FloatY>\n");
                a.append("    <ShuffleEdges>");
                for (int l = 0; l < i1; l++)
                {
                    a.append(c.getShuffleEdge(j1 + l, c.getChildAt(l).getTop())).append(",");
                }

                a.append("</ShuffleEdges>\n");
                a.append("</DSLVState>\n");
                d = d + 1;
                if (d > 1000)
                {
                    c();
                    d = 0;
                    return;
                }
            }
        }

        public void c()
        {
            boolean flag;
            flag = false;
            if (!f)
            {
                return;
            }
            FileWriter filewriter;
            IOException ioexception;
            if (e != 0)
            {
                flag = true;
            }
            filewriter = new FileWriter(b, flag);
            filewriter.write(a.toString());
            a.delete(0, a.length());
            filewriter.flush();
            filewriter.close();
            e = e + 1;
            return;
            ioexception;
        }

        public void d()
        {
            if (f)
            {
                a.append("</DSLVStates>\n");
                c();
                f = false;
            }
        }

        public c()
        {
            c = DragSortListView.this;
            super();
            a = new StringBuilder();
            d = 0;
            e = 0;
            f = false;
            b = new File(Environment.getExternalStorageDirectory(), "dslv_state.txt");
            if (b.exists())
            {
                break MISSING_BLOCK_LABEL_77;
            }
            b.createNewFile();
            Log.d("mobeta", "file created");
            return;
            dragsortlistview;
            Log.w("mobeta", "Could not create dslv_state.txt");
            Log.d("mobeta", getMessage());
            return;
        }
    }

    private class d extends h
    {

        final DragSortListView a;
        private int d;
        private int e;
        private float f;
        private float g;

        private int e()
        {
            int i = a.getFirstVisiblePosition();
            int j = (a.mItemHeightCollapsed + a.getDividerHeight()) / 2;
            View view = a.getChildAt(d - i);
            if (view != null)
            {
                if (d == e)
                {
                    return view.getTop();
                }
                if (d < e)
                {
                    return view.getTop() - j;
                } else
                {
                    return (view.getBottom() + j) - a.mFloatViewHeight;
                }
            } else
            {
                d();
                return -1;
            }
        }

        public void a()
        {
            d = a.mFloatPos;
            e = a.mSrcPos;
            a.mDragState = 2;
            f = a.mFloatLoc.y - e();
            g = a.mFloatLoc.x - a.getPaddingLeft();
        }

        public void a(float f1, float f2)
        {
            int i = e();
            int j = a.getPaddingLeft();
            f1 = a.mFloatLoc.y - i;
            float f3 = a.mFloatLoc.x - j;
            f2 = 1.0F - f2;
            if (f2 < Math.abs(f1 / f) || f2 < Math.abs(f3 / g))
            {
                a.mFloatLoc.y = i + (int)(f * f2);
                a.mFloatLoc.x = a.getPaddingLeft() + (int)(g * f2);
                a.doDragFloatView(true);
            }
        }

        public void b()
        {
            a.dropFloatView();
        }

        public d(float f1, int i)
        {
            a = DragSortListView.this;
            super(f1, i);
        }
    }

    private class e
    {

        final DragSortListView a;
        private SparseIntArray b;
        private ArrayList c;
        private int d;

        public int a(int i)
        {
            return b.get(i, -1);
        }

        public void a()
        {
            b.clear();
            c.clear();
        }

        public void a(int i, int j)
        {
            int k = b.get(i, -1);
            if (k != j)
            {
                if (k == -1)
                {
                    if (b.size() == d)
                    {
                        b.delete(((Integer)c.remove(0)).intValue());
                    }
                } else
                {
                    c.remove(Integer.valueOf(i));
                }
                b.put(i, j);
                c.add(Integer.valueOf(i));
            }
        }

        public e(int i)
        {
            a = DragSortListView.this;
            super();
            b = new SparseIntArray(i);
            c = new ArrayList(i);
            d = i;
        }
    }

    private class f extends h
    {

        final DragSortListView a;
        private float d;
        private float e;

        public void a()
        {
            d = a.mDragDeltaY;
            e = a.mFloatViewHeightHalf;
        }

        public void a(float f1, float f2)
        {
            if (a.mDragState != 4)
            {
                d();
                return;
            } else
            {
                a.mDragDeltaY = (int)(e * f2 + (1.0F - f2) * d);
                a.mFloatLoc.y = a.mY - a.mDragDeltaY;
                a.doDragFloatView(true);
                return;
            }
        }
    }

    private class h
        implements Runnable
    {

        private float a;
        protected long b;
        final DragSortListView c;
        private float d;
        private float e;
        private float f;
        private float g;
        private float h;
        private boolean i;

        public float a(float f1)
        {
            if (f1 < d)
            {
                return e * f1 * f1;
            }
            if (f1 < 1.0F - d)
            {
                return f + g * f1;
            } else
            {
                return 1.0F - h * (f1 - 1.0F) * (f1 - 1.0F);
            }
        }

        public void a()
        {
        }

        public void a(float f1, float f2)
        {
        }

        public void b()
        {
        }

        public void c()
        {
            b = SystemClock.uptimeMillis();
            i = false;
            a();
            c.post(this);
        }

        public void d()
        {
            i = true;
        }

        public void run()
        {
            if (i)
            {
                return;
            }
            float f1 = (float)(SystemClock.uptimeMillis() - b) / a;
            if (f1 >= 1.0F)
            {
                a(1.0F, 1.0F);
                b();
                return;
            } else
            {
                a(f1, a(f1));
                c.post(this);
                return;
            }
        }

        public h(float f1, int j)
        {
            c = DragSortListView.this;
            super();
            d = f1;
            a = j;
            f1 = 1.0F / (d * 2.0F * (1.0F - d));
            h = f1;
            e = f1;
            f = d / ((d - 1.0F) * 2.0F);
            g = 1.0F / (1.0F - d);
        }
    }


    private static final int DRAGGING = 4;
    public static final int DRAG_NEG_X = 2;
    public static final int DRAG_NEG_Y = 8;
    public static final int DRAG_POS_X = 1;
    public static final int DRAG_POS_Y = 4;
    private static final int DROPPING = 2;
    private static final int IDLE = 0;
    private static final int NO_CANCEL = 0;
    private static final int ON_INTERCEPT_TOUCH_EVENT = 2;
    private static final int ON_TOUCH_EVENT = 1;
    private static final int REMOVING = 1;
    private static final int STOPPED = 3;
    private static final int sCacheSize = 3;
    private a mAdapterWrapper;
    private boolean mAnimate;
    private boolean mBlockLayoutRequests;
    private MotionEvent mCancelEvent;
    private int mCancelMethod;
    private e mChildHeightCache;
    private float mCurrFloatAlpha;
    private int mDownScrollStartY;
    private float mDownScrollStartYF;
    private int mDragDeltaX;
    private int mDragDeltaY;
    private float mDragDownScrollHeight;
    private float mDragDownScrollStartFrac;
    private boolean mDragEnabled;
    private int mDragFlags;
    private DragListener mDragListener;
    private b mDragScroller;
    private c mDragSortTracker;
    private int mDragStartY;
    private int mDragState;
    private float mDragUpScrollHeight;
    private float mDragUpScrollStartFrac;
    private d mDropAnimator;
    private DropListener mDropListener;
    private int mFirstExpPos;
    private float mFloatAlpha;
    private Point mFloatLoc;
    private int mFloatPos;
    private View mFloatView;
    private int mFloatViewHeight;
    private int mFloatViewHeightHalf;
    private boolean mFloatViewInvalidated;
    private FloatViewManager mFloatViewManager;
    private int mFloatViewMid;
    private boolean mFloatViewOnMeasured;
    private boolean mIgnoreTouchEvent;
    private boolean mInTouchEvent;
    private int mItemHeightCollapsed;
    private boolean mLastCallWasIntercept;
    private int mLastX;
    private int mLastY;
    private f mLiftAnimator;
    private boolean mListViewIntercepted;
    private float mMaxScrollSpeed;
    private DataSetObserver mObserver;
    private int mOffsetX;
    private int mOffsetY;
    private g mRemoveAnimator;
    private RemoveListener mRemoveListener;
    private float mRemoveVelocityX;
    private View mSampleViewTypes[];
    private DragScrollProfile mScrollProfile;
    private int mSecondExpPos;
    private float mSlideFrac;
    private float mSlideRegionFrac;
    private int mSrcPos;
    private Point mTouchLoc;
    private boolean mTrackDragSort;
    private int mUpScrollStartY;
    private float mUpScrollStartYF;
    private boolean mUseRemoveVelocity;
    private int mWidthMeasureSpec;
    private int mX;
    private int mY;

    public DragSortListView(Context context, AttributeSet attributeset)
    {
        ListView(context, attributeset);
        mFloatLoc = new Point();
        mTouchLoc = new Point();
        mFloatViewOnMeasured = false;
        mFloatAlpha = 1.0F;
        mCurrFloatAlpha = 1.0F;
        mAnimate = false;
        mDragEnabled = true;
        mDragState = 0;
        mItemHeightCollapsed = 1;
        mWidthMeasureSpec = 0;
        mSampleViewTypes = new View[1];
        mDragUpScrollStartFrac = 0.3333333F;
        mDragDownScrollStartFrac = 0.3333333F;
        mMaxScrollSpeed = 0.5F;
        mScrollProfile = new b(this);
        mDragFlags = 0;
        mLastCallWasIntercept = false;
        mInTouchEvent = false;
        mFloatViewManager = null;
        mCancelMethod = 0;
        mSlideRegionFrac = 0.25F;
        mSlideFrac = 0.0F;
        mTrackDragSort = false;
        mBlockLayoutRequests = false;
        mIgnoreTouchEvent = false;
        mChildHeightCache = new e(3);
        mRemoveVelocityX = 0.0F;
        mListViewIntercepted = false;
        mFloatViewInvalidated = false;
        int j = 150;
        int i;
        if (attributeset != null)
        {
            context = getContext().obtainStyledAttributes(attributeset, com.ximalaya.ting.android.library.R.styleable.DragSortListView, 0, 0);
            mItemHeightCollapsed = Math.max(1, context.getDimensionPixelSize(0, 1));
            mTrackDragSort = context.getBoolean(5, false);
            if (mTrackDragSort)
            {
                mDragSortTracker = new c();
            }
            mFloatAlpha = context.getFloat(6, mFloatAlpha);
            mCurrFloatAlpha = mFloatAlpha;
            mDragEnabled = context.getBoolean(10, mDragEnabled);
            mSlideRegionFrac = Math.max(0.0F, Math.min(1.0F, 1.0F - context.getFloat(7, 0.75F)));
            boolean flag;
            if (mSlideRegionFrac > 0.0F)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            mAnimate = flag;
            setDragScrollStart(context.getFloat(1, mDragUpScrollStartFrac));
            mMaxScrollSpeed = context.getFloat(2, mMaxScrollSpeed);
            j = context.getInt(8, 150);
            i = context.getInt(9, 150);
            if (context.getBoolean(17, true))
            {
                flag = context.getBoolean(12, false);
                int k = context.getInt(4, 1);
                boolean flag1 = context.getBoolean(11, true);
                int l = context.getInt(13, 0);
                int i1 = context.getResourceId(14, 0);
                int j1 = context.getResourceId(15, 0);
                int k1 = context.getResourceId(16, 0);
                int l1 = context.getColor(3, 0xff000000);
                attributeset = new DragSortController(this, i1, l, k, k1, j1);
                attributeset.setRemoveEnabled(flag);
                attributeset.setSortEnabled(flag1);
                attributeset.setBackgroundColor(l1);
                mFloatViewManager = attributeset;
                setOnTouchListener(attributeset);
            }
            context.recycle();
        } else
        {
            i = 150;
        }
        mDragScroller = new b();
        if (j > 0)
        {
            mRemoveAnimator = new g(0.5F, j);
        }
        if (i > 0)
        {
            mDropAnimator = new d(0.5F, i);
        }
        mCancelEvent = MotionEvent.obtain(0L, 0L, 3, 0.0F, 0.0F, 0.0F, 0.0F, 0, 0.0F, 0.0F, 0, 0);
        mObserver = new c(this);
    }

    private void adjustAllItems()
    {
        int j = getFirstVisiblePosition();
        int k = getLastVisiblePosition();
        int i = Math.max(0, getHeaderViewsCount() - j);
        for (k = Math.min(k - j, getCount() - 1 - getFooterViewsCount() - j); i <= k; i++)
        {
            View view = getChildAt(i);
            if (view != null)
            {
                adjustItem(j + i, view, false);
            }
        }

    }

    private void adjustItem(int i)
    {
        View view = getChildAt(i - getFirstVisiblePosition());
        if (view != null)
        {
            adjustItem(i, view, false);
        }
    }

    private void adjustItem(int i, View view, boolean flag)
    {
        android.view.ViewGroup.LayoutParams layoutparams = view.getLayoutParams();
        int j;
        boolean flag1;
        int k;
        if (i != mSrcPos && i != mFirstExpPos && i != mSecondExpPos)
        {
            j = -2;
        } else
        {
            j = calcItemHeight(i, view, flag);
        }
        if (j != layoutparams.height)
        {
            layoutparams.height = j;
            view.setLayoutParams(layoutparams);
        }
        if (i != mFirstExpPos && i != mSecondExpPos) goto _L2; else goto _L1
_L1:
        if (i >= mSrcPos) goto _L4; else goto _L3
_L3:
        ((DragSortItemView)view).setGravity(80);
_L2:
        k = view.getVisibility();
        flag1 = false;
        j = ((flag1) ? 1 : 0);
        if (i == mSrcPos)
        {
            j = ((flag1) ? 1 : 0);
            if (mFloatView != null)
            {
                j = 4;
            }
        }
        if (j != k)
        {
            view.setVisibility(j);
        }
        return;
_L4:
        if (i > mSrcPos)
        {
            ((DragSortItemView)view).setGravity(48);
        }
        if (true) goto _L2; else goto _L5
_L5:
    }

    private void adjustOnReorder()
    {
        int i = 0;
        int j = getFirstVisiblePosition();
        if (mSrcPos < j)
        {
            View view = getChildAt(0);
            if (view != null)
            {
                i = view.getTop();
            }
            setSelectionFromTop(j - 1, i - getPaddingTop());
        }
    }

    private int adjustScroll(int i, View view, int j, int k)
    {
        int l = getChildHeight(i);
        int k1 = view.getHeight();
        int l1 = calcItemHeight(i, l);
        int i1;
        int j1;
        int i2;
        if (i != mSrcPos)
        {
            i1 = k1 - l;
            l = l1 - l;
        } else
        {
            l = l1;
            i1 = k1;
        }
        i2 = mFloatViewHeight;
        j1 = i2;
        if (mSrcPos != mFirstExpPos)
        {
            j1 = i2;
            if (mSrcPos != mSecondExpPos)
            {
                j1 = i2 - mItemHeightCollapsed;
            }
        }
        if (i <= j)
        {
            if (i > mFirstExpPos)
            {
                return (j1 - l) + 0;
            }
        } else
        {
            if (i == k)
            {
                if (i <= mFirstExpPos)
                {
                    return (i1 - j1) + 0;
                }
                if (i == mSecondExpPos)
                {
                    return (k1 - l1) + 0;
                } else
                {
                    return 0 + i1;
                }
            }
            if (i <= mFirstExpPos)
            {
                return 0 - j1;
            }
            if (i == mSecondExpPos)
            {
                return 0 - l;
            }
        }
        return 0;
    }

    private static int buildRunList(SparseBooleanArray sparsebooleanarray, int i, int j, int ai[], int ai1[])
    {
        int l = findFirstSetIndex(sparsebooleanarray, i, j);
        if (l == -1)
        {
            return 0;
        }
        int j1 = sparsebooleanarray.keyAt(l);
        int k = j1 + 1;
        l++;
        int i1 = 0;
        do
        {
            if (l >= sparsebooleanarray.size())
            {
                break;
            }
            int k1 = sparsebooleanarray.keyAt(l);
            if (k1 >= j)
            {
                break;
            }
            if (sparsebooleanarray.valueAt(l))
            {
                if (k1 == k)
                {
                    k++;
                } else
                {
                    ai[i1] = j1;
                    ai1[i1] = k;
                    i1++;
                    k = k1 + 1;
                    j1 = k1;
                }
            }
            l++;
        } while (true);
        l = k;
        if (k == j)
        {
            l = i;
        }
        ai[i1] = j1;
        ai1[i1] = l;
        k = i1 + 1;
        j = k;
        if (k > 1)
        {
            j = k;
            if (ai[0] == i)
            {
                j = k;
                if (ai1[k - 1] == i)
                {
                    ai[0] = ai[k - 1];
                    j = k - 1;
                }
            }
        }
        return j;
    }

    private int calcItemHeight(int i, int j)
    {
        int k;
        int l;
        int i1;
        getDividerHeight();
        if (mAnimate && mFirstExpPos != mSecondExpPos)
        {
            k = 1;
        } else
        {
            k = 0;
        }
        l = mFloatViewHeight - mItemHeightCollapsed;
        i1 = (int)(mSlideFrac * (float)l);
        if (i != mSrcPos) goto _L2; else goto _L1
_L1:
        if (mSrcPos != mFirstExpPos) goto _L4; else goto _L3
_L3:
        if (k == 0) goto _L6; else goto _L5
_L5:
        k = i1 + mItemHeightCollapsed;
_L8:
        return k;
_L6:
        return mFloatViewHeight;
_L4:
        if (mSrcPos == mSecondExpPos)
        {
            return mFloatViewHeight - i1;
        } else
        {
            return mItemHeightCollapsed;
        }
_L2:
        if (i == mFirstExpPos)
        {
            if (k != 0)
            {
                return j + i1;
            } else
            {
                return j + l;
            }
        }
        k = j;
        if (i == mSecondExpPos)
        {
            return (j + l) - i1;
        }
        if (true) goto _L8; else goto _L7
_L7:
    }

    private int calcItemHeight(int i, View view, boolean flag)
    {
        return calcItemHeight(i, getChildHeight(i, view, flag));
    }

    private void clearPositions()
    {
        mSrcPos = -1;
        mFirstExpPos = -1;
        mSecondExpPos = -1;
        mFloatPos = -1;
    }

    private void continueDrag(int i, int j)
    {
        mFloatLoc.x = i - mDragDeltaX;
        mFloatLoc.y = j - mDragDeltaY;
        doDragFloatView(true);
        i = Math.min(j, mFloatViewMid + mFloatViewHeightHalf);
        j = Math.max(j, mFloatViewMid - mFloatViewHeightHalf);
        int k = mDragScroller.b();
        if (i > mLastY && i > mDownScrollStartY && k != 1)
        {
            if (k != -1)
            {
                mDragScroller.a(true);
            }
            mDragScroller.a(1);
        } else
        {
            if (j < mLastY && j < mUpScrollStartY && k != 0)
            {
                if (k != -1)
                {
                    mDragScroller.a(true);
                }
                mDragScroller.a(0);
                return;
            }
            if (j >= mUpScrollStartY && i <= mDownScrollStartY && mDragScroller.a())
            {
                mDragScroller.a(true);
                return;
            }
        }
    }

    private void destroyFloatView()
    {
        if (mFloatView != null)
        {
            mFloatView.setVisibility(8);
            if (mFloatViewManager != null)
            {
                mFloatViewManager.onDestroyFloatView(mFloatView);
            }
            mFloatView = null;
            invalidate();
        }
    }

    private void doActionUpOrCancel()
    {
        mCancelMethod = 0;
        mInTouchEvent = false;
        if (mDragState == 3)
        {
            mDragState = 0;
        }
        mCurrFloatAlpha = mFloatAlpha;
        mListViewIntercepted = false;
        mChildHeightCache.a();
    }

    private void doDragFloatView(int i, View view, boolean flag)
    {
        mBlockLayoutRequests = true;
        updateFloatView();
        int j = mFirstExpPos;
        int k = mSecondExpPos;
        boolean flag1 = updatePositions();
        if (flag1)
        {
            adjustAllItems();
            setSelectionFromTop(i, (adjustScroll(i, view, j, k) + view.getTop()) - getPaddingTop());
            layoutChildren();
        }
        if (flag1 || flag)
        {
            invalidate();
        }
        mBlockLayoutRequests = false;
    }

    private void doDragFloatView(boolean flag)
    {
        int i = getFirstVisiblePosition();
        int j = getChildCount() / 2;
        View view = getChildAt(getChildCount() / 2);
        if (view == null)
        {
            return;
        } else
        {
            doDragFloatView(i + j, view, flag);
            return;
        }
    }

    private void doRemoveItem()
    {
        doRemoveItem(mSrcPos - getHeaderViewsCount());
    }

    private void doRemoveItem(int i)
    {
        mDragState = 1;
        if (mRemoveListener != null)
        {
            mRemoveListener.remove(i);
        }
        destroyFloatView();
        adjustOnReorder();
        clearPositions();
        if (mInTouchEvent)
        {
            mDragState = 3;
            return;
        } else
        {
            mDragState = 0;
            return;
        }
    }

    private void drawDivider(int i, Canvas canvas)
    {
        Drawable drawable = getDivider();
        int i1 = getDividerHeight();
        if (drawable != null && i1 != 0)
        {
            ViewGroup viewgroup = (ViewGroup)getChildAt(i - getFirstVisiblePosition());
            if (viewgroup != null)
            {
                int k = getPaddingLeft();
                int l = getWidth() - getPaddingRight();
                int j = viewgroup.getChildAt(0).getHeight();
                if (i > mSrcPos)
                {
                    j += viewgroup.getTop();
                    i = j + i1;
                } else
                {
                    i = viewgroup.getBottom() - j;
                    j = i - i1;
                }
                canvas.save();
                canvas.clipRect(k, j, l, i);
                drawable.setBounds(k, j, l, i);
                drawable.draw(canvas);
                canvas.restore();
            }
        }
    }

    private void dropFloatView()
    {
        mDragState = 2;
        if (mDropListener != null && mFloatPos >= 0 && mFloatPos < getCount())
        {
            int i = getHeaderViewsCount();
            mDropListener.drop(mSrcPos - i, mFloatPos - i);
        }
        destroyFloatView();
        adjustOnReorder();
        clearPositions();
        adjustAllItems();
        if (mInTouchEvent)
        {
            mDragState = 3;
            return;
        } else
        {
            mDragState = 0;
            return;
        }
    }

    private static int findFirstSetIndex(SparseBooleanArray sparsebooleanarray, int i, int j)
    {
        int k;
label0:
        {
            k = sparsebooleanarray.size();
            for (i = insertionIndexForKey(sparsebooleanarray, i); i < k && sparsebooleanarray.keyAt(i) < j && !sparsebooleanarray.valueAt(i); i++) { }
            if (i != k)
            {
                k = i;
                if (sparsebooleanarray.keyAt(i) < j)
                {
                    break label0;
                }
            }
            k = -1;
        }
        return k;
    }

    private int getChildHeight(int i)
    {
        int j = 0;
        if (i != mSrcPos)
        {
            View view = getChildAt(i - getFirstVisiblePosition());
            if (view != null)
            {
                return getChildHeight(i, view, false);
            }
            int l = mChildHeightCache.a(i);
            j = l;
            if (l == -1)
            {
                Object obj = getAdapter();
                int k = ((ListAdapter) (obj)).getItemViewType(i);
                int i1 = ((ListAdapter) (obj)).getViewTypeCount();
                if (i1 != mSampleViewTypes.length)
                {
                    mSampleViewTypes = new View[i1];
                }
                if (k >= 0)
                {
                    if (mSampleViewTypes[k] == null)
                    {
                        obj = ((ListAdapter) (obj)).getView(i, null, this);
                        mSampleViewTypes[k] = ((View) (obj));
                    } else
                    {
                        obj = ((ListAdapter) (obj)).getView(i, mSampleViewTypes[k], this);
                    }
                } else
                {
                    obj = ((ListAdapter) (obj)).getView(i, null, this);
                }
                k = getChildHeight(i, ((View) (obj)), true);
                mChildHeightCache.a(i, k);
                return k;
            }
        }
        return j;
    }

    private int getChildHeight(int i, View view, boolean flag)
    {
        boolean flag1 = false;
        if (i == mSrcPos)
        {
            i = ((flag1) ? 1 : 0);
        } else
        {
            View view1 = view;
            if (i >= getHeaderViewsCount())
            {
                if (i >= getCount() - getFooterViewsCount())
                {
                    view1 = view;
                } else
                {
                    view1 = ((ViewGroup)view).getChildAt(0);
                }
            }
            view = view1.getLayoutParams();
            if (view != null && ((android.view.ViewGroup.LayoutParams) (view)).height > 0)
            {
                return ((android.view.ViewGroup.LayoutParams) (view)).height;
            }
            i = view1.getHeight();
            if (i == 0 || flag)
            {
                measureItem(view1);
                return view1.getMeasuredHeight();
            }
        }
        return i;
    }

    private int getItemHeight(int i)
    {
        View view = getChildAt(i - getFirstVisiblePosition());
        if (view != null)
        {
            return view.getHeight();
        } else
        {
            return calcItemHeight(i, getChildHeight(i));
        }
    }

    private int getShuffleEdge(int i, int j)
    {
        int k = getHeaderViewsCount();
        int l = getFooterViewsCount();
        if (i <= k || i >= getCount() - l)
        {
            return j;
        }
        l = getDividerHeight();
        int j1 = mFloatViewHeight - mItemHeightCollapsed;
        int i1 = getChildHeight(i);
        int k1 = getItemHeight(i);
        if (mSecondExpPos <= mSrcPos)
        {
            if (i == mSecondExpPos && mFirstExpPos != mSecondExpPos)
            {
                if (i == mSrcPos)
                {
                    k = (j + k1) - mFloatViewHeight;
                } else
                {
                    k = ((k1 - i1) + j) - j1;
                }
            } else
            {
                k = j;
                if (i > mSecondExpPos)
                {
                    k = j;
                    if (i <= mSrcPos)
                    {
                        k = j - j1;
                    }
                }
            }
        } else
        if (i > mSrcPos && i <= mFirstExpPos)
        {
            k = j + j1;
        } else
        {
            k = j;
            if (i == mSecondExpPos)
            {
                k = j;
                if (mFirstExpPos != mSecondExpPos)
                {
                    k = j + (k1 - i1);
                }
            }
        }
        if (i <= mSrcPos)
        {
            return (mFloatViewHeight - l - getChildHeight(i - 1)) / 2 + k;
        } else
        {
            return (i1 - l - mFloatViewHeight) / 2 + k;
        }
    }

    private static int insertionIndexForKey(SparseBooleanArray sparsebooleanarray, int i)
    {
        int j = 0;
        for (int k = sparsebooleanarray.size(); k - j > 0;)
        {
            int l = j + k >> 1;
            if (sparsebooleanarray.keyAt(l) < i)
            {
                j = l + 1;
            } else
            {
                k = l;
            }
        }

        return j;
    }

    private void invalidateFloatView()
    {
        mFloatViewInvalidated = true;
    }

    private void measureFloatView()
    {
        if (mFloatView != null)
        {
            measureItem(mFloatView);
            mFloatViewHeight = mFloatView.getMeasuredHeight();
            mFloatViewHeightHalf = mFloatViewHeight / 2;
        }
    }

    private void measureItem(View view)
    {
        android.view.ViewGroup.LayoutParams layoutparams = view.getLayoutParams();
        Object obj = layoutparams;
        if (layoutparams == null)
        {
            obj = new LayoutParams(-1, -2);
            view.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
        }
        int j = ViewGroup.getChildMeasureSpec(mWidthMeasureSpec, getListPaddingLeft() + getListPaddingRight(), ((android.view.ViewGroup.LayoutParams) (obj)).width);
        int i;
        if (((android.view.ViewGroup.LayoutParams) (obj)).height > 0)
        {
            i = android.view.View.MeasureSpec.makeMeasureSpec(((android.view.ViewGroup.LayoutParams) (obj)).height, 0x40000000);
        } else
        {
            i = android.view.View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        view.measure(j, i);
    }

    private void printPosData()
    {
        Log.d("mobeta", (new StringBuilder()).append("mSrcPos=").append(mSrcPos).append(" mFirstExpPos=").append(mFirstExpPos).append(" mSecondExpPos=").append(mSecondExpPos).toString());
    }

    private static int rotate(int i, int j, int k, int l)
    {
        int i1 = l - k;
        j = i + j;
        if (j < k)
        {
            i = j + i1;
        } else
        {
            i = j;
            if (j >= l)
            {
                return j - i1;
            }
        }
        return i;
    }

    private void saveTouchCoords(MotionEvent motionevent)
    {
        int i = motionevent.getAction() & 0xff;
        if (i != 0)
        {
            mLastX = mX;
            mLastY = mY;
        }
        mX = (int)motionevent.getX();
        mY = (int)motionevent.getY();
        if (i == 0)
        {
            mLastX = mX;
            mLastY = mY;
        }
        mOffsetX = (int)motionevent.getRawX() - mX;
        mOffsetY = (int)motionevent.getRawY() - mY;
    }

    private void updateFloatView()
    {
        if (mFloatViewManager != null)
        {
            mTouchLoc.set(mX, mY);
            mFloatViewManager.onDragFloatView(mFloatView, mFloatLoc, mTouchLoc);
        }
        int i = mFloatLoc.x;
        int l = mFloatLoc.y;
        int j = getPaddingLeft();
        int k;
        int i1;
        int j1;
        if ((mDragFlags & 1) == 0 && i > j)
        {
            mFloatLoc.x = j;
        } else
        if ((mDragFlags & 2) == 0 && i < j)
        {
            mFloatLoc.x = j;
        }
        j = getHeaderViewsCount();
        k = getFooterViewsCount();
        i1 = getFirstVisiblePosition();
        j1 = getLastVisiblePosition();
        i = getPaddingTop();
        if (i1 < j)
        {
            i = getChildAt(j - i1 - 1).getBottom();
        }
        j = i;
        if ((mDragFlags & 8) == 0)
        {
            j = i;
            if (i1 <= mSrcPos)
            {
                j = Math.max(getChildAt(mSrcPos - i1).getTop(), i);
            }
        }
        i = getHeight() - getPaddingBottom();
        if (j1 >= getCount() - k - 1)
        {
            i = getChildAt(getCount() - k - 1 - i1).getBottom();
        }
        k = i;
        if ((mDragFlags & 4) == 0)
        {
            k = i;
            if (j1 >= mSrcPos)
            {
                k = Math.min(getChildAt(mSrcPos - i1).getBottom(), i);
            }
        }
        if (l < j)
        {
            mFloatLoc.y = j;
        } else
        if (mFloatViewHeight + l > k)
        {
            mFloatLoc.y = k - mFloatViewHeight;
        }
        mFloatViewMid = mFloatLoc.y + mFloatViewHeightHalf;
    }

    private boolean updatePositions()
    {
        int i;
        int j;
        int k1;
        int l1;
        int j2;
        i = getFirstVisiblePosition();
        j = mFirstExpPos;
        View view1 = getChildAt(j - i);
        View view = view1;
        if (view1 == null)
        {
            j = i + getChildCount() / 2;
            view = getChildAt(j - i);
        }
        k1 = view.getTop();
        l1 = view.getHeight();
        i = getShuffleEdge(j, k1);
        j2 = getDividerHeight();
        if (mFloatViewMid >= i) goto _L2; else goto _L1
_L1:
        int i1;
        int j1;
        int k = i;
        i1 = i;
        i = k;
        j1 = j;
_L8:
        int l;
        j = j1;
        l = i1;
        if (j1 < 0) goto _L4; else goto _L3
_L3:
        j1--;
        i = getItemHeight(j1);
        if (j1 != 0) goto _L6; else goto _L5
_L5:
        i = k1 - j2 - i;
        l = i1;
        j = j1;
_L4:
        j1 = getHeaderViewsCount();
        int i2 = getFooterViewsCount();
        k1 = mFirstExpPos;
        l1 = mSecondExpPos;
        float f1 = mSlideFrac;
        boolean flag;
        if (mAnimate)
        {
            j2 = Math.abs(i - l);
            float f2;
            int k2;
            if (mFloatViewMid >= i)
            {
                i1 = l;
                l = i;
                i = i1;
            }
            f2 = mSlideRegionFrac;
            i1 = (int)((float)j2 * (f2 * 0.5F));
            f2 = i1;
            l += i1;
            if (mFloatViewMid < l)
            {
                mFirstExpPos = j - 1;
                mSecondExpPos = j;
                mSlideFrac = ((float)(l - mFloatViewMid) * 0.5F) / f2;
            } else
            if (mFloatViewMid < i - i1)
            {
                mFirstExpPos = j;
                mSecondExpPos = j;
            } else
            {
                mFirstExpPos = j;
                mSecondExpPos = j + 1;
                mSlideFrac = (1.0F + (float)(i - mFloatViewMid) / f2) * 0.5F;
            }
        } else
        {
            mFirstExpPos = j;
            mSecondExpPos = j;
        }
        if (mFirstExpPos < j1)
        {
            mFirstExpPos = j1;
            mSecondExpPos = j1;
            j = j1;
        } else
        if (mSecondExpPos >= getCount() - i2)
        {
            j = getCount() - i2 - 1;
            mFirstExpPos = j;
            mSecondExpPos = j;
        }
        if (mFirstExpPos != k1 || mSecondExpPos != l1 || mSlideFrac != f1)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (j != mFloatPos)
        {
            if (mDragListener != null)
            {
                mDragListener.drag(mFloatPos - j1, j - j1);
            }
            mFloatPos = j;
            return true;
        } else
        {
            return flag;
        }
_L6:
        l1 = k1 - (i + j2);
        k1 = getShuffleEdge(j1, l1);
        j = j1;
        l = i1;
        i = k1;
        if (mFloatViewMid >= k1) goto _L4; else goto _L7
_L7:
        i1 = k1;
        i = k1;
        k1 = l1;
          goto _L8
_L2:
        k2 = getCount();
        l = i;
        j1 = i;
        i = l;
        i1 = j;
_L10:
        j = i1;
        l = j1;
        if (i1 < k2)
        {
label0:
            {
                if (i1 != k2 - 1)
                {
                    break label0;
                }
                i = k1 + j2 + l1;
                j = i1;
                l = j1;
            }
        }
          goto _L4
        i2 = k1 + (j2 + l1);
        l1 = getItemHeight(i1 + 1);
        k1 = getShuffleEdge(i1 + 1, i2);
        j = i1;
        l = j1;
        i = k1;
        if (mFloatViewMid < k1) goto _L4; else goto _L9
_L9:
        i1++;
        j1 = k1;
        i = k1;
        k1 = i2;
          goto _L10
    }

    private void updateScrollStarts()
    {
        int i = getPaddingTop();
        int j = getHeight() - i - getPaddingBottom();
        float f1 = j;
        mUpScrollStartYF = (float)i + mDragUpScrollStartFrac * f1;
        float f2 = i;
        mDownScrollStartYF = f1 * (1.0F - mDragDownScrollStartFrac) + f2;
        mUpScrollStartY = (int)mUpScrollStartYF;
        mDownScrollStartY = (int)mDownScrollStartYF;
        mDragUpScrollHeight = mUpScrollStartYF - (float)i;
        mDragDownScrollHeight = (float)(i + j) - mDownScrollStartYF;
    }

    public void cancelDrag()
    {
label0:
        {
            if (mDragState == 4)
            {
                mDragScroller.a(true);
                destroyFloatView();
                clearPositions();
                adjustAllItems();
                if (!mInTouchEvent)
                {
                    break label0;
                }
                mDragState = 3;
            }
            return;
        }
        mDragState = 0;
    }

    protected void dispatchDraw(Canvas canvas)
    {
        dispatchDraw(canvas);
        if (mDragState != 0)
        {
            if (mFirstExpPos != mSrcPos)
            {
                drawDivider(mFirstExpPos, canvas);
            }
            if (mSecondExpPos != mFirstExpPos && mSecondExpPos != mSrcPos)
            {
                drawDivider(mSecondExpPos, canvas);
            }
        }
        if (mFloatView != null)
        {
            int k = mFloatView.getWidth();
            int l = mFloatView.getHeight();
            int j = mFloatLoc.x;
            int i1 = getWidth();
            int i = j;
            if (j < 0)
            {
                i = -j;
            }
            float f1;
            if (i < i1)
            {
                f1 = (float)(i1 - i) / (float)i1;
                f1 *= f1;
            } else
            {
                f1 = 0.0F;
            }
            i = (int)(f1 * (255F * mCurrFloatAlpha));
            canvas.save();
            canvas.translate(mFloatLoc.x, mFloatLoc.y);
            canvas.clipRect(0, 0, k, l);
            canvas.saveLayerAlpha(0.0F, 0.0F, k, l, i, 31);
            mFloatView.draw(canvas);
            canvas.restore();
            canvas.restore();
        }
    }

    public float getFloatAlpha()
    {
        return mCurrFloatAlpha;
    }

    public ListAdapter getInputAdapter()
    {
        if (mAdapterWrapper == null)
        {
            return null;
        } else
        {
            return mAdapterWrapper.a();
        }
    }

    public boolean isDragEnabled()
    {
        return mDragEnabled;
    }

    protected void layoutChildren()
    {
        layoutChildren();
        if (mFloatView != null)
        {
            if (mFloatView.isLayoutRequested() && !mFloatViewOnMeasured)
            {
                measureFloatView();
            }
            mFloatView.layout(0, 0, mFloatView.getMeasuredWidth(), mFloatView.getMeasuredHeight());
            mFloatViewOnMeasured = false;
        }
    }

    public boolean listViewIntercepted()
    {
        return mListViewIntercepted;
    }

    public void moveCheckState(int i, int j)
    {
        SparseBooleanArray sparsebooleanarray = getCheckedItemPositions();
        int ai[];
        int ai1[];
        int k;
        int l;
        int i1;
        if (j < i)
        {
            l = i;
            k = j;
        } else
        {
            l = j;
            k = i;
        }
        i1 = l + 1;
        ai = new int[sparsebooleanarray.size()];
        ai1 = new int[sparsebooleanarray.size()];
        l = buildRunList(sparsebooleanarray, k, i1, ai, ai1);
        if (l != 1 || ai[0] != ai1[0]) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        i = 0;
        while (i != l) 
        {
            setItemChecked(rotate(ai[i], -1, k, i1), true);
            setItemChecked(rotate(ai1[i], -1, k, i1), false);
            i++;
        }
        if (true) goto _L1; else goto _L3
_L3:
        i = 0;
        while (i != l) 
        {
            setItemChecked(ai[i], false);
            setItemChecked(ai1[i], true);
            i++;
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    public void moveItem(int i, int j)
    {
        if (mDropListener != null)
        {
            int k = getInputAdapter().getCount();
            if (i >= 0 && i < k && j >= 0 && j < k)
            {
                mDropListener.drop(i, j);
            }
        }
    }

    protected boolean onDragTouchEvent(MotionEvent motionevent)
    {
        motionevent.getAction();
        motionevent.getAction() & 0xff;
        JVM INSTR tableswitch 1 3: default 40
    //                   1 61
    //                   2 82
    //                   3 42;
           goto _L1 _L2 _L3 _L4
_L1:
        return true;
_L4:
        if (mDragState == 4)
        {
            cancelDrag();
        }
        doActionUpOrCancel();
        continue; /* Loop/switch isn't completed */
_L2:
        if (mDragState == 4)
        {
            stopDrag(false);
        }
        doActionUpOrCancel();
        continue; /* Loop/switch isn't completed */
_L3:
        continueDrag((int)motionevent.getX(), (int)motionevent.getY());
        if (true) goto _L1; else goto _L5
_L5:
    }

    protected void onDraw(Canvas canvas)
    {
        onDraw(canvas);
        if (mTrackDragSort)
        {
            mDragSortTracker.b();
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent)
    {
        int i;
        if (!mDragEnabled)
        {
            return onInterceptTouchEvent(motionevent);
        }
        saveTouchCoords(motionevent);
        mLastCallWasIntercept = true;
        i = motionevent.getAction() & 0xff;
        if (i == 0)
        {
            if (mDragState != 0)
            {
                mIgnoreTouchEvent = true;
                return true;
            }
            mInTouchEvent = true;
        }
        if (mFloatView == null) goto _L2; else goto _L1
_L1:
        boolean flag = true;
_L4:
        if (i == 1 || i == 3)
        {
            mInTouchEvent = false;
        }
        return flag;
_L2:
        if (onInterceptTouchEvent(motionevent))
        {
            mListViewIntercepted = true;
            flag = true;
        } else
        {
            flag = false;
        }
        switch (i)
        {
        case 2: // '\002'
        default:
            if (flag)
            {
                mCancelMethod = 1;
            } else
            {
                mCancelMethod = 2;
            }
            break;

        case 1: // '\001'
        case 3: // '\003'
            doActionUpOrCancel();
            break;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    protected void onMeasure(int i, int j)
    {
        onMeasure(i, j);
        if (mFloatView != null)
        {
            if (mFloatView.isLayoutRequested())
            {
                measureFloatView();
            }
            mFloatViewOnMeasured = true;
        }
        mWidthMeasureSpec = i;
    }

    protected void onSizeChanged(int i, int j, int k, int l)
    {
        onSizeChanged(i, j, k, l);
        updateScrollStarts();
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        boolean flag;
        boolean flag2;
        flag2 = false;
        flag = false;
        if (!mIgnoreTouchEvent) goto _L2; else goto _L1
_L1:
        mIgnoreTouchEvent = false;
        flag2 = flag;
_L4:
        return flag2;
_L2:
        if (!mDragEnabled)
        {
            return onTouchEvent(motionevent);
        }
        boolean flag1 = mLastCallWasIntercept;
        mLastCallWasIntercept = false;
        if (!flag1)
        {
            saveTouchCoords(motionevent);
        }
        if (mDragState == 4)
        {
            onDragTouchEvent(motionevent);
            return true;
        }
        flag1 = flag2;
        if (mDragState == 0)
        {
            flag1 = flag2;
            if (onTouchEvent(motionevent))
            {
                flag1 = true;
            }
        }
        switch (motionevent.getAction() & 0xff)
        {
        case 2: // '\002'
        default:
            flag2 = flag1;
            if (flag1)
            {
                mCancelMethod = 1;
                return flag1;
            }
            break;

        case 1: // '\001'
        case 3: // '\003'
            doActionUpOrCancel();
            return flag1;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void removeCheckState(int i)
    {
        SparseBooleanArray sparsebooleanarray = getCheckedItemPositions();
        if (sparsebooleanarray.size() != 0)
        {
            int ai[] = new int[sparsebooleanarray.size()];
            int ai1[] = new int[sparsebooleanarray.size()];
            int k = sparsebooleanarray.keyAt(sparsebooleanarray.size() - 1) + 1;
            int l = buildRunList(sparsebooleanarray, i, k, ai, ai1);
            int j = 0;
            while (j != l) 
            {
                if (ai[j] != i && (ai1[j] >= ai[j] || ai1[j] <= i))
                {
                    setItemChecked(rotate(ai[j], -1, i, k), true);
                }
                setItemChecked(rotate(ai1[j], -1, i, k), false);
                j++;
            }
        }
    }

    public void removeItem(int i)
    {
        mUseRemoveVelocity = false;
        removeItem(i, 0.0F);
    }

    public void removeItem(int i, float f1)
    {
        if (mDragState != 0 && mDragState != 4) goto _L2; else goto _L1
_L1:
        if (mDragState == 0)
        {
            mSrcPos = getHeaderViewsCount() + i;
            mFirstExpPos = mSrcPos;
            mSecondExpPos = mSrcPos;
            mFloatPos = mSrcPos;
            View view = getChildAt(mSrcPos - getFirstVisiblePosition());
            if (view != null)
            {
                view.setVisibility(4);
            }
        }
        mDragState = 1;
        mRemoveVelocityX = f1;
        if (!mInTouchEvent) goto _L4; else goto _L3
_L3:
        mCancelMethod;
        JVM INSTR tableswitch 1 2: default 124
    //                   1 139
    //                   2 151;
           goto _L4 _L5 _L6
_L4:
        if (mRemoveAnimator == null)
        {
            break; /* Loop/switch isn't completed */
        }
        mRemoveAnimator.c();
_L2:
        return;
_L5:
        onTouchEvent(mCancelEvent);
        continue; /* Loop/switch isn't completed */
_L6:
        onInterceptTouchEvent(mCancelEvent);
        if (true) goto _L4; else goto _L7
_L7:
        doRemoveItem(i);
        return;
    }

    public void requestLayout()
    {
        if (!mBlockLayoutRequests)
        {
            requestLayout();
        }
    }

    public volatile void setAdapter(Adapter adapter)
    {
        setAdapter((ListAdapter)adapter);
    }

    public void setAdapter(ListAdapter listadapter)
    {
        if (listadapter != null)
        {
            mAdapterWrapper = new a(listadapter);
            listadapter.registerDataSetObserver(mObserver);
            if (listadapter instanceof DropListener)
            {
                setDropListener((DropListener)listadapter);
            }
            if (listadapter instanceof DragListener)
            {
                setDragListener((DragListener)listadapter);
            }
            if (listadapter instanceof RemoveListener)
            {
                setRemoveListener((RemoveListener)listadapter);
            }
        } else
        {
            mAdapterWrapper = null;
        }
        setAdapter(mAdapterWrapper);
    }

    public void setDragEnabled(boolean flag)
    {
        mDragEnabled = flag;
    }

    public void setDragListener(DragListener draglistener)
    {
        mDragListener = draglistener;
    }

    public void setDragScrollProfile(DragScrollProfile dragscrollprofile)
    {
        if (dragscrollprofile != null)
        {
            mScrollProfile = dragscrollprofile;
        }
    }

    public void setDragScrollStart(float f1)
    {
        setDragScrollStarts(f1, f1);
    }

    public void setDragScrollStarts(float f1, float f2)
    {
        if (f2 > 0.5F)
        {
            mDragDownScrollStartFrac = 0.5F;
        } else
        {
            mDragDownScrollStartFrac = f2;
        }
        if (f1 > 0.5F)
        {
            mDragUpScrollStartFrac = 0.5F;
        } else
        {
            mDragUpScrollStartFrac = f1;
        }
        if (getHeight() != 0)
        {
            updateScrollStarts();
        }
    }

    public void setDragSortListener(DragSortListener dragsortlistener)
    {
        setDropListener(dragsortlistener);
        setDragListener(dragsortlistener);
        setRemoveListener(dragsortlistener);
    }

    public void setDropListener(DropListener droplistener)
    {
        mDropListener = droplistener;
    }

    public void setFloatAlpha(float f1)
    {
        mCurrFloatAlpha = f1;
    }

    public void setFloatViewManager(FloatViewManager floatviewmanager)
    {
        mFloatViewManager = floatviewmanager;
    }

    public void setMaxScrollSpeed(float f1)
    {
        mMaxScrollSpeed = f1;
    }

    public void setRemoveListener(RemoveListener removelistener)
    {
        mRemoveListener = removelistener;
    }

    public boolean startDrag(int i, int j, int k, int l)
    {
        View view;
        if (mInTouchEvent && mFloatViewManager != null)
        {
            if ((view = mFloatViewManager.onCreateFloatView(i)) != null)
            {
                return startDrag(i, view, j, k, l);
            }
        }
        return false;
    }

    public boolean startDrag(int i, View view, int j, int k, int l)
    {
        boolean flag = true;
        if (mDragState == 0 && mInTouchEvent && mFloatView == null && view != null && mDragEnabled) goto _L2; else goto _L1
_L1:
        flag = false;
_L4:
        return flag;
_L2:
        if (getParent() != null)
        {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
        i = getHeaderViewsCount() + i;
        mFirstExpPos = i;
        mSecondExpPos = i;
        mSrcPos = i;
        mFloatPos = i;
        mDragState = 4;
        mDragFlags = 0;
        mDragFlags = mDragFlags | j;
        mFloatView = view;
        measureFloatView();
        mDragDeltaX = k;
        mDragDeltaY = l;
        mDragStartY = mY;
        mFloatLoc.x = mX - mDragDeltaX;
        mFloatLoc.y = mY - mDragDeltaY;
        view = getChildAt(mSrcPos - getFirstVisiblePosition());
        if (view != null)
        {
            view.setVisibility(4);
        }
        if (mTrackDragSort)
        {
            mDragSortTracker.a();
        }
        switch (mCancelMethod)
        {
        default:
            break;

        case 1: // '\001'
            break; /* Loop/switch isn't completed */

        case 2: // '\002'
            break;
        }
        break MISSING_BLOCK_LABEL_260;
_L5:
        requestLayout();
        if (mLiftAnimator != null)
        {
            mLiftAnimator.c();
            return true;
        }
        if (true) goto _L4; else goto _L3
_L3:
        onTouchEvent(mCancelEvent);
          goto _L5
        onInterceptTouchEvent(mCancelEvent);
          goto _L5
    }

    public boolean stopDrag(boolean flag)
    {
        mUseRemoveVelocity = false;
        return stopDrag(flag, 0.0F);
    }

    public boolean stopDrag(boolean flag, float f1)
    {
        if (mFloatView != null)
        {
            mDragScroller.a(true);
            if (flag)
            {
                removeItem(mSrcPos - getHeaderViewsCount(), f1);
            } else
            if (mDropAnimator != null)
            {
                mDropAnimator.c();
            } else
            {
                dropFloatView();
            }
            if (mTrackDragSort)
            {
                mDragSortTracker.d();
            }
            return true;
        } else
        {
            return false;
        }
    }

    public boolean stopDragWithVelocity(boolean flag, float f1)
    {
        mUseRemoveVelocity = true;
        return stopDrag(flag, f1);
    }





/*
    static int access$102(DragSortListView dragsortlistview, int i)
    {
        dragsortlistview.mDragState = i;
        return i;
    }

*/








/*
    static float access$1602(DragSortListView dragsortlistview, float f)
    {
        dragsortlistview.mRemoveVelocityX = f;
        return f;
    }

*/


/*
    static float access$1616(DragSortListView dragsortlistview, float f)
    {
        f = dragsortlistview.mRemoveVelocityX + f;
        dragsortlistview.mRemoveVelocityX = f;
        return f;
    }

*/












/*
    static boolean access$2602(DragSortListView dragsortlistview, boolean flag)
    {
        dragsortlistview.mBlockLayoutRequests = flag;
        return flag;
    }

*/







/*
    static int access$302(DragSortListView dragsortlistview, int i)
    {
        dragsortlistview.mDragDeltaY = i;
        return i;
    }

*/







}
