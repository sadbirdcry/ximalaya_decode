// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.listview;

import android.graphics.Point;
import android.os.SystemClock;
import android.view.View;

// Referenced classes of package com.ximalaya.ting.android.library.view.listview:
//            DragSortListView

private class h extends h
{

    final DragSortListView a;
    private float d;
    private float e;
    private float f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;

    public void a()
    {
        float f1;
        int l;
        l = -1;
        g = -1;
        h = -1;
        i = DragSortListView.access$1300(a);
        j = DragSortListView.access$1400(a);
        k = DragSortListView.access$900(a);
        DragSortListView.access$102(a, 1);
        d = DragSortListView.access$500(a).x;
        if (!DragSortListView.access$1500(a))
        {
            break MISSING_BLOCK_LABEL_205;
        }
        f1 = (float)a.getWidth() * 2.0F;
        if (DragSortListView.access$1600(a) != 0.0F) goto _L2; else goto _L1
_L1:
        DragSortListView dragsortlistview = a;
        if (d >= 0.0F)
        {
            l = 1;
        }
        DragSortListView.access$1602(dragsortlistview, (float)l * f1);
_L4:
        return;
_L2:
        f1 *= 2.0F;
        if (DragSortListView.access$1600(a) < 0.0F && DragSortListView.access$1600(a) > -f1)
        {
            DragSortListView.access$1602(a, -f1);
            return;
        }
        if (DragSortListView.access$1600(a) <= 0.0F || DragSortListView.access$1600(a) >= f1) goto _L4; else goto _L3
_L3:
        DragSortListView.access$1602(a, f1);
        return;
        DragSortListView.access$1700(a);
        return;
    }

    public void a(float f1, float f2)
    {
        View view;
        int k1;
        f1 = 1.0F - f2;
        k1 = a.getFirstVisiblePosition();
        view = a.getChildAt(i - k1);
        if (!DragSortListView.access$1500(a)) goto _L2; else goto _L1
_L1:
        f2 = (float)(SystemClock.uptimeMillis() - b) / 1000F;
        if (f2 != 0.0F) goto _L4; else goto _L3
_L3:
        return;
_L4:
        float f3 = DragSortListView.access$1600(a);
        int l1 = a.getWidth();
        DragSortListView dragsortlistview = a;
        int l;
        if (DragSortListView.access$1600(a) > 0.0F)
        {
            l = 1;
        } else
        {
            l = -1;
        }
        DragSortListView.access$1616(dragsortlistview, (float)l * f2 * (float)l1);
        d = d + f3 * f2;
        DragSortListView.access$500(a).x = (int)d;
        if (d < (float)l1 && d > (float)(-l1))
        {
            b = SystemClock.uptimeMillis();
            DragSortListView.access$700(a, true);
            return;
        }
_L2:
        if (view != null)
        {
            if (g == -1)
            {
                g = DragSortListView.access$1800(a, i, view, false);
                e = view.getHeight() - g;
            }
            int i1 = Math.max((int)(e * f1), 1);
            android.view.ams ams = view.getLayoutParams();
            ams.height = i1 + g;
            view.setLayoutParams(ams);
        }
        if (j != i)
        {
            View view1 = a.getChildAt(j - k1);
            if (view1 != null)
            {
                if (h == -1)
                {
                    h = DragSortListView.access$1800(a, j, view1, false);
                    f = view1.getHeight() - h;
                }
                int j1 = Math.max((int)(f * f1), 1);
                android.view.ams ams1 = view1.getLayoutParams();
                ams1.height = j1 + h;
                view1.setLayoutParams(ams1);
                return;
            }
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

    public void b()
    {
        DragSortListView.access$1900(a);
    }

    public (DragSortListView dragsortlistview, float f1, int l)
    {
        a = dragsortlistview;
        super(dragsortlistview, f1, l);
        g = -1;
        h = -1;
    }
}
