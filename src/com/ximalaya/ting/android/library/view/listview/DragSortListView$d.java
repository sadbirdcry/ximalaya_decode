// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.listview;

import android.graphics.Point;
import android.view.View;

// Referenced classes of package com.ximalaya.ting.android.library.view.listview:
//            DragSortListView

private class <init> extends <init>
{

    final DragSortListView a;
    private int d;
    private int e;
    private float f;
    private float g;

    private int e()
    {
        int i = a.getFirstVisiblePosition();
        int j = (DragSortListView.access$1000(a) + a.getDividerHeight()) / 2;
        View view = a.getChildAt(d - i);
        if (view != null)
        {
            if (d == e)
            {
                return view.getTop();
            }
            if (d < e)
            {
                return view.getTop() - j;
            } else
            {
                return (view.getBottom() + j) - DragSortListView.access$1100(a);
            }
        } else
        {
            d();
            return -1;
        }
    }

    public void a()
    {
        d = DragSortListView.access$800(a);
        e = DragSortListView.access$900(a);
        DragSortListView.access$102(a, 2);
        f = DragSortListView.access$500(a).y - e();
        g = DragSortListView.access$500(a).x - a.getPaddingLeft();
    }

    public void a(float f1, float f2)
    {
        int i = e();
        int j = a.getPaddingLeft();
        f1 = DragSortListView.access$500(a).y - i;
        float f3 = DragSortListView.access$500(a).x - j;
        f2 = 1.0F - f2;
        if (f2 < Math.abs(f1 / f) || f2 < Math.abs(f3 / g))
        {
            DragSortListView.access$500(a).y = i + (int)(f * f2);
            DragSortListView.access$500(a).x = a.getPaddingLeft() + (int)(g * f2);
            DragSortListView.access$700(a, true);
        }
    }

    public void b()
    {
        DragSortListView.access$1200(a);
    }

    public (DragSortListView dragsortlistview, float f1, int i)
    {
        a = dragsortlistview;
        super(dragsortlistview, f1, i);
    }
}
