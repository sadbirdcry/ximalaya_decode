// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.listview;

import android.content.Context;
import android.widget.Checkable;

// Referenced classes of package com.ximalaya.ting.android.library.view.listview:
//            DragSortItemView

public class DragSortItemViewCheckable extends DragSortItemView
    implements Checkable
{

    public DragSortItemViewCheckable(Context context)
    {
        super(context);
    }

    public boolean isChecked()
    {
        android.view.View view = getChildAt(0);
        if (view instanceof Checkable)
        {
            return ((Checkable)view).isChecked();
        } else
        {
            return false;
        }
    }

    public void setChecked(boolean flag)
    {
        android.view.View view = getChildAt(0);
        if (view instanceof Checkable)
        {
            ((Checkable)view).setChecked(flag);
        }
    }

    public void toggle()
    {
        android.view.View view = getChildAt(0);
        if (view instanceof Checkable)
        {
            ((Checkable)view).toggle();
        }
    }
}
