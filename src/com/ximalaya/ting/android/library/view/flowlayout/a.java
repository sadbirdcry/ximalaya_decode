// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.flowlayout;

import android.view.View;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.library.view.flowlayout:
//            LayoutConfiguration

class a
{

    private final List a = new ArrayList();
    private final LayoutConfiguration b;
    private final int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;

    public a(int j, LayoutConfiguration layoutconfiguration)
    {
        h = 0;
        i = 0;
        c = j;
        b = layoutconfiguration;
    }

    public int a()
    {
        return h;
    }

    public void a(int j)
    {
        int k = g;
        int l = e;
        g = j;
        e = j - (k - l);
    }

    public void a(int j, View view)
    {
        FlowLayout.LayoutParams layoutparams = (FlowLayout.LayoutParams)view.getLayoutParams();
        a.add(j, view);
        d = f + layoutparams.getLength();
        f = d + layoutparams.getSpacingLength();
        g = Math.max(g, layoutparams.getThickness() + layoutparams.getSpacingThickness());
        e = Math.max(e, layoutparams.getThickness());
    }

    public void a(View view)
    {
        a(a.size(), view);
    }

    public int b()
    {
        return g;
    }

    public void b(int j)
    {
        int k = f;
        int l = d;
        d = j;
        f = (k - l) + j;
    }

    public boolean b(View view)
    {
        int j;
        if (b.a() == 0)
        {
            j = view.getMeasuredWidth();
        } else
        {
            j = view.getMeasuredHeight();
        }
        return j + f <= c;
    }

    public int c()
    {
        return d;
    }

    public void c(int j)
    {
        h = h + j;
    }

    public int d()
    {
        return i;
    }

    public void d(int j)
    {
        i = i + j;
    }

    public List e()
    {
        return a;
    }
}
