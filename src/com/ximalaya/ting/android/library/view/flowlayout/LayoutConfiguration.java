// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.flowlayout;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

class LayoutConfiguration
{

    private int a;
    private boolean b;
    private float c;
    private int d;
    private int e;

    public LayoutConfiguration(Context context, AttributeSet attributeset)
    {
        a = 0;
        b = false;
        c = 0.0F;
        d = 51;
        e = 0;
        context = context.obtainStyledAttributes(attributeset, com.ximalaya.ting.android.library.R.styleable.FlowLayout);
        a(context.getInteger(1, 0));
        a(context.getBoolean(3, false));
        a(context.getFloat(4, 0.0F));
        b(context.getInteger(0, 0));
        c(context.getInteger(2, 0));
        context.recycle();
        return;
        attributeset;
        context.recycle();
        throw attributeset;
    }

    public int a()
    {
        return a;
    }

    public void a(float f)
    {
        c = Math.max(0.0F, f);
    }

    public void a(int i)
    {
        if (i == 1)
        {
            a = i;
            return;
        } else
        {
            a = 0;
            return;
        }
    }

    public void a(boolean flag)
    {
        b = flag;
    }

    public void b(int i)
    {
        if ((i & 7) == 0)
        {
            i |= 3;
        }
        int j = i;
        if ((i & 0x70) == 0)
        {
            j = i | 0x30;
        }
        d = j;
    }

    public boolean b()
    {
        return b;
    }

    public float c()
    {
        return c;
    }

    public void c(int i)
    {
        if (i == 1)
        {
            e = i;
            return;
        } else
        {
            e = 0;
            return;
        }
    }

    public int d()
    {
        return d;
    }

    public int e()
    {
        return e;
    }
}
