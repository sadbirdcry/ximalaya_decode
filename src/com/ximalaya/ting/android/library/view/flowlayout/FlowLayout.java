// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.flowlayout;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.library.view.flowlayout:
//            LayoutConfiguration, a

public class FlowLayout extends ViewGroup
{
    public static class LayoutParams extends android.view.ViewGroup.MarginLayoutParams
    {

        public int gravity;
        private int inlineStartLength;
        private int inlineStartThickness;
        private int length;
        public boolean newLine;
        private int spacingLength;
        private int spacingThickness;
        private int thickness;
        public float weight;
        private int x;
        private int y;

        private void readStyleParameters(Context context, AttributeSet attributeset)
        {
            context = context.obtainStyledAttributes(attributeset, com.ximalaya.ting.android.library.R.styleable.FlowLayout_LayoutParams);
            newLine = context.getBoolean(1, false);
            gravity = context.getInt(0, 0);
            weight = context.getFloat(2, -1F);
            context.recycle();
            return;
            attributeset;
            context.recycle();
            throw attributeset;
        }

        void clearCalculatedFields(int i)
        {
            if (i == 0)
            {
                spacingLength = leftMargin + rightMargin;
                spacingThickness = topMargin + bottomMargin;
                return;
            } else
            {
                spacingLength = topMargin + bottomMargin;
                spacingThickness = leftMargin + rightMargin;
                return;
            }
        }

        int getInlineStartLength()
        {
            return inlineStartLength;
        }

        int getInlineStartThickness()
        {
            return inlineStartThickness;
        }

        int getLength()
        {
            return length;
        }

        int getSpacingLength()
        {
            return spacingLength;
        }

        int getSpacingThickness()
        {
            return spacingThickness;
        }

        int getThickness()
        {
            return thickness;
        }

        public boolean gravitySpecified()
        {
            return gravity != 0;
        }

        void setInlineStartLength(int i)
        {
            inlineStartLength = i;
        }

        void setInlineStartThickness(int i)
        {
            inlineStartThickness = i;
        }

        void setLength(int i)
        {
            length = i;
        }

        void setPosition(int i, int j)
        {
            x = i;
            y = j;
        }

        void setThickness(int i)
        {
            thickness = i;
        }

        public boolean weightSpecified()
        {
            return weight >= 0.0F;
        }



        public LayoutParams(int i, int j)
        {
            super(i, j);
            newLine = false;
            gravity = 0;
            weight = -1F;
        }

        public LayoutParams(Context context, AttributeSet attributeset)
        {
            super(context, attributeset);
            newLine = false;
            gravity = 0;
            weight = -1F;
            readStyleParameters(context, attributeset);
        }

        public LayoutParams(android.view.ViewGroup.LayoutParams layoutparams)
        {
            super(layoutparams);
            newLine = false;
            gravity = 0;
            weight = -1F;
        }
    }


    public static final int HORIZONTAL = 0;
    public static final int LAYOUT_DIRECTION_LTR = 0;
    public static final int LAYOUT_DIRECTION_RTL = 1;
    public static final int VERTICAL = 1;
    private final LayoutConfiguration config;
    List lines;
    private int mLines;
    private int mValideViewNum;

    public FlowLayout(Context context)
    {
        super(context);
        lines = new ArrayList();
        mLines = 0x7fffffff;
        config = new LayoutConfiguration(context, null);
    }

    public FlowLayout(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        lines = new ArrayList();
        mLines = 0x7fffffff;
        config = new LayoutConfiguration(context, attributeset);
    }

    public FlowLayout(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        lines = new ArrayList();
        mLines = 0x7fffffff;
        config = new LayoutConfiguration(context, attributeset);
    }

    private void applyGravityToLine(a a1)
    {
        int i = a1.e().size();
        if (i > 0)
        {
            Object obj = a1.e().iterator();
            float f;
            for (f = 0.0F; ((Iterator) (obj)).hasNext(); f = getWeight((LayoutParams)((View)((Iterator) (obj)).next()).getLayoutParams()) + f) { }
            obj = (LayoutParams)((View)a1.e().get(i - 1)).getLayoutParams();
            int j = a1.c();
            int k = ((LayoutParams) (obj)).getLength();
            int l = ((LayoutParams) (obj)).getInlineStartLength();
            obj = a1.e().iterator();
            i = 0;
            while (((Iterator) (obj)).hasNext()) 
            {
                LayoutParams layoutparams = (LayoutParams)((View)((Iterator) (obj)).next()).getLayoutParams();
                float f1 = getWeight(layoutparams);
                int i1 = getGravity(layoutparams);
                int j1 = Math.round((f1 * (float)(j - (l + k))) / f);
                int k1 = layoutparams.getLength() + layoutparams.getSpacingLength();
                int l1 = layoutparams.getThickness();
                int i2 = layoutparams.getSpacingThickness();
                Rect rect = new Rect();
                rect.top = 0;
                rect.left = i;
                rect.right = k1 + j1 + i;
                rect.bottom = a1.b();
                Rect rect1 = new Rect();
                Gravity.apply(i1, k1, l1 + i2, rect, rect1);
                i += j1;
                layoutparams.setInlineStartLength(rect1.left + layoutparams.getInlineStartLength());
                layoutparams.setInlineStartThickness(rect1.top);
                layoutparams.setLength(rect1.width() - layoutparams.getSpacingLength());
                layoutparams.setThickness(rect1.height() - layoutparams.getSpacingThickness());
            }
        }
    }

    private void applyGravityToLines(List list, int i, int j)
    {
        int l = list.size();
        if (l > 0)
        {
            a a1 = (a)list.get(l - 1);
            int i1 = a1.b();
            int j1 = a1.a();
            list = list.iterator();
            int k = 0;
            while (list.hasNext()) 
            {
                a a2 = (a)list.next();
                int k1 = getGravity();
                int l1 = Math.round((1 * (j - (j1 + i1))) / l);
                int i2 = a2.c();
                int j2 = a2.b();
                Rect rect = new Rect();
                rect.top = k;
                rect.left = 0;
                rect.right = i;
                rect.bottom = j2 + l1 + k;
                Rect rect1 = new Rect();
                Gravity.apply(k1, i2, j2, rect, rect1);
                k += l1;
                a2.d(rect1.left);
                a2.c(rect1.top);
                a2.b(rect1.width());
                a2.a(rect1.height());
            }
        }
    }

    private void applyPositionsToViews(a a1)
    {
        for (Iterator iterator = a1.e().iterator(); iterator.hasNext();)
        {
            View view = (View)iterator.next();
            LayoutParams layoutparams = (LayoutParams)view.getLayoutParams();
            if (config.a() == 0)
            {
                layoutparams.setPosition(getPaddingLeft() + a1.d() + layoutparams.getInlineStartLength(), getPaddingTop() + a1.a() + layoutparams.getInlineStartThickness());
                view.measure(android.view.View.MeasureSpec.makeMeasureSpec(layoutparams.getLength(), 0x40000000), android.view.View.MeasureSpec.makeMeasureSpec(layoutparams.getThickness(), 0x40000000));
            } else
            {
                layoutparams.setPosition(getPaddingLeft() + a1.a() + layoutparams.getInlineStartThickness(), getPaddingTop() + a1.d() + layoutparams.getInlineStartLength());
                view.measure(android.view.View.MeasureSpec.makeMeasureSpec(layoutparams.getThickness(), 0x40000000), android.view.View.MeasureSpec.makeMeasureSpec(layoutparams.getLength(), 0x40000000));
            }
        }

    }

    private void calculateLinesAndChildPosition(List list)
    {
        list = list.iterator();
        int j = 0;
        do
        {
label0:
            {
                if (!list.hasNext())
                {
                    break label0;
                }
                Object obj = (a)list.next();
                ((a) (obj)).c(j);
                int k = j + ((a) (obj)).b();
                obj = ((a) (obj)).e().iterator();
                int i = 0;
                do
                {
                    j = k;
                    if (!((Iterator) (obj)).hasNext())
                    {
                        break;
                    }
                    LayoutParams layoutparams = (LayoutParams)((View)((Iterator) (obj)).next()).getLayoutParams();
                    layoutparams.setInlineStartLength(i);
                    j = layoutparams.getLength();
                    i = layoutparams.getSpacingLength() + j + i;
                } while (true);
            }
        } while (true);
    }

    private Paint createPaint(int i)
    {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(i);
        paint.setStrokeWidth(2.0F);
        return paint;
    }

    private void drawDebugInfo(Canvas canvas, View view)
    {
        if (config.b())
        {
            Paint paint = createPaint(-256);
            Paint paint1 = createPaint(0xffff0000);
            LayoutParams layoutparams = (LayoutParams)view.getLayoutParams();
            if (layoutparams.rightMargin > 0)
            {
                float f = view.getRight();
                float f6 = view.getTop();
                f6 = (float)view.getHeight() / 2.0F + f6;
                canvas.drawLine(f, f6, f + (float)layoutparams.rightMargin, f6, paint);
                canvas.drawLine(((float)layoutparams.rightMargin + f) - 4F, f6 - 4F, f + (float)layoutparams.rightMargin, f6, paint);
                canvas.drawLine(((float)layoutparams.rightMargin + f) - 4F, f6 + 4F, f + (float)layoutparams.rightMargin, f6, paint);
            }
            if (layoutparams.leftMargin > 0)
            {
                float f1 = view.getLeft();
                float f7 = view.getTop();
                f7 = (float)view.getHeight() / 2.0F + f7;
                canvas.drawLine(f1, f7, f1 - (float)layoutparams.leftMargin, f7, paint);
                canvas.drawLine((f1 - (float)layoutparams.leftMargin) + 4F, f7 - 4F, f1 - (float)layoutparams.leftMargin, f7, paint);
                canvas.drawLine((f1 - (float)layoutparams.leftMargin) + 4F, f7 + 4F, f1 - (float)layoutparams.leftMargin, f7, paint);
            }
            if (layoutparams.bottomMargin > 0)
            {
                float f2 = view.getLeft();
                f2 = (float)view.getWidth() / 2.0F + f2;
                float f8 = view.getBottom();
                canvas.drawLine(f2, f8, f2, f8 + (float)layoutparams.bottomMargin, paint);
                canvas.drawLine(f2 - 4F, ((float)layoutparams.bottomMargin + f8) - 4F, f2, f8 + (float)layoutparams.bottomMargin, paint);
                canvas.drawLine(f2 + 4F, ((float)layoutparams.bottomMargin + f8) - 4F, f2, f8 + (float)layoutparams.bottomMargin, paint);
            }
            if (layoutparams.topMargin > 0)
            {
                float f3 = view.getLeft();
                f3 = (float)view.getWidth() / 2.0F + f3;
                float f9 = view.getTop();
                canvas.drawLine(f3, f9, f3, f9 - (float)layoutparams.topMargin, paint);
                canvas.drawLine(f3 - 4F, (f9 - (float)layoutparams.topMargin) + 4F, f3, f9 - (float)layoutparams.topMargin, paint);
                canvas.drawLine(f3 + 4F, (f9 - (float)layoutparams.topMargin) + 4F, f3, f9 - (float)layoutparams.topMargin, paint);
            }
            if (layoutparams.newLine)
            {
                if (config.a() == 0)
                {
                    float f4 = view.getLeft();
                    float f10 = (float)view.getTop() + (float)view.getHeight() / 2.0F;
                    canvas.drawLine(f4, f10 - 6F, f4, f10 + 6F, paint1);
                    return;
                } else
                {
                    float f5 = (float)view.getLeft() + (float)view.getWidth() / 2.0F;
                    float f11 = view.getTop();
                    canvas.drawLine(f5 - 6F, f11, 6F + f5, f11, paint1);
                    return;
                }
            }
        }
    }

    private int findSize(int i, int j, int k)
    {
        switch (i)
        {
        case 0: // '\0'
        default:
            return k;

        case -2147483648: 
            return Math.min(k, j);

        case 1073741824: 
            return j;
        }
    }

    private int getGravity(LayoutParams layoutparams)
    {
        if (layoutparams.gravitySpecified())
        {
            return layoutparams.gravity;
        } else
        {
            return config.d();
        }
    }

    private float getWeight(LayoutParams layoutparams)
    {
        if (layoutparams.weightSpecified())
        {
            return layoutparams.weight;
        } else
        {
            return config.c();
        }
    }

    protected boolean checkLayoutParams(android.view.ViewGroup.LayoutParams layoutparams)
    {
        return layoutparams instanceof LayoutParams;
    }

    protected boolean drawChild(Canvas canvas, View view, long l)
    {
        boolean flag = super.drawChild(canvas, view, l);
        drawDebugInfo(canvas, view);
        return flag;
    }

    protected volatile android.view.ViewGroup.LayoutParams generateDefaultLayoutParams()
    {
        return generateDefaultLayoutParams();
    }

    protected LayoutParams generateDefaultLayoutParams()
    {
        return new LayoutParams(-2, -2);
    }

    public volatile android.view.ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeset)
    {
        return generateLayoutParams(attributeset);
    }

    protected volatile android.view.ViewGroup.LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams layoutparams)
    {
        return generateLayoutParams(layoutparams);
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeset)
    {
        return new LayoutParams(getContext(), attributeset);
    }

    protected LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams layoutparams)
    {
        return new LayoutParams(layoutparams);
    }

    public int getGravity()
    {
        return config.d();
    }

    public int getLayoutDirection()
    {
        if (config == null)
        {
            return 0;
        } else
        {
            return config.e();
        }
    }

    public int getOrientation()
    {
        return config.a();
    }

    public float getWeightDefault()
    {
        return config.c();
    }

    public boolean isDebugDraw()
    {
        return config.b();
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l)
    {
        j = getChildCount();
        Log.d("chenkai", (new StringBuilder()).append("validate view size: ").append(mValideViewNum).toString());
        for (i = 0; i < j && i < mValideViewNum; i++)
        {
            View view = getChildAt(i);
            LayoutParams layoutparams = (LayoutParams)view.getLayoutParams();
            k = layoutparams.x;
            l = layoutparams.leftMargin;
            int i1 = layoutparams.y;
            int j1 = layoutparams.topMargin;
            int k1 = layoutparams.x;
            int l1 = layoutparams.leftMargin;
            int i2 = view.getMeasuredWidth();
            int j2 = layoutparams.y;
            view.layout(k + l, i1 + j1, k1 + l1 + i2, layoutparams.topMargin + j2 + view.getMeasuredHeight());
        }

    }

    protected void onMeasure(int i, int j)
    {
        Object obj;
        Object obj1;
        int k;
        int l;
        int i1;
        int j2;
        l = android.view.View.MeasureSpec.getSize(i) - getPaddingRight() - getPaddingLeft();
        i1 = android.view.View.MeasureSpec.getSize(j) - getPaddingTop() - getPaddingBottom();
        int k1 = android.view.View.MeasureSpec.getMode(i);
        j2 = android.view.View.MeasureSpec.getMode(j);
        int k2;
        if (config.a() == 0)
        {
            k = l;
        } else
        {
            k = i1;
        }
        if (config.a() == 0)
        {
            l = i1;
        }
        if (config.a() == 0)
        {
            i1 = k1;
        } else
        {
            i1 = j2;
        }
        if (config.a() != 0);
        lines.clear();
        mValideViewNum = 0;
        obj = new a(k, config);
        lines.add(obj);
        k2 = getChildCount();
        k1 = 0;
        if (k1 >= k2) goto _L2; else goto _L1
_L1:
        obj1 = getChildAt(k1);
        if (((View) (obj1)).getVisibility() != 8) goto _L4; else goto _L3
_L3:
        k1++;
        break MISSING_BLOCK_LABEL_144;
_L4:
        int l1;
        LayoutParams layoutparams = (LayoutParams)((View) (obj1)).getLayoutParams();
        ((View) (obj1)).measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight(), layoutparams.width), getChildMeasureSpec(j, getPaddingTop() + getPaddingBottom(), layoutparams.height));
        layoutparams.clearCalculatedFields(config.a());
        boolean flag;
        if (config.a() == 0)
        {
            layoutparams.setLength(((View) (obj1)).getMeasuredWidth());
            layoutparams.setThickness(((View) (obj1)).getMeasuredHeight());
        } else
        {
            layoutparams.setLength(((View) (obj1)).getMeasuredHeight());
            layoutparams.setThickness(((View) (obj1)).getMeasuredWidth());
        }
        if (layoutparams.newLine || i1 != 0 && !((a) (obj)).b(((View) (obj1))))
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_465;
        }
        if (lines.size() < mLines) goto _L5; else goto _L2
_L2:
        calculateLinesAndChildPosition(lines);
        obj1 = lines.iterator();
        for (l1 = 0; ((Iterator) (obj1)).hasNext(); l1 = Math.max(l1, ((a)((Iterator) (obj1)).next()).c())) { }
        break MISSING_BLOCK_LABEL_519;
_L5:
        obj = new a(k, config);
        if (config.a() == 1 && config.e() == 1)
        {
            lines.add(0, obj);
        } else
        {
            lines.add(obj);
        }
        if (config.a() == 0 && config.e() == 1)
        {
            ((a) (obj)).a(0, ((View) (obj1)));
        } else
        {
            ((a) (obj)).a(((View) (obj1)));
        }
        if (true) goto _L3; else goto _L6
_L6:
        int i2 = ((a) (obj)).a();
        i2 = ((a) (obj)).b() + i2;
        k = findSize(i1, k, l1);
        l = findSize(j2, l, i2);
        applyGravityToLines(lines, k, l);
        a a1;
        for (obj = lines.iterator(); ((Iterator) (obj)).hasNext(); applyPositionsToViews(a1))
        {
            a1 = (a)((Iterator) (obj)).next();
            applyGravityToLine(a1);
        }

        l = getPaddingLeft() + getPaddingRight();
        k = getPaddingBottom();
        k = getPaddingTop() + k;
        if (config.a() == 0)
        {
            l += l1;
            k += i2;
        } else
        {
            l += i2;
            k += l1;
        }
        for (obj = lines.iterator(); ((Iterator) (obj)).hasNext();)
        {
            a a2 = (a)((Iterator) (obj)).next();
            int j1 = mValideViewNum;
            mValideViewNum = a2.e().size() + j1;
        }

        Log.d("chenkai", (new StringBuilder()).append("measure, validate size: ").append(mValideViewNum).toString());
        setMeasuredDimension(resolveSize(l, i), resolveSize(k, j));
        return;
    }

    public void setDebugDraw(boolean flag)
    {
        config.a(flag);
        invalidate();
    }

    public void setGravity(int i)
    {
        config.b(i);
        requestLayout();
    }

    public void setLayoutDirection(int i)
    {
        config.c(i);
        requestLayout();
    }

    public void setLine(int i)
    {
        mLines = i;
    }

    public void setOrientation(int i)
    {
        config.a(i);
        requestLayout();
    }

    public void setWeightDefault(float f)
    {
        config.a(f);
        requestLayout();
    }
}
