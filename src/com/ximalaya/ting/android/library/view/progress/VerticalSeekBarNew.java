// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.progress;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.widget.SeekBar;

public class VerticalSeekBarNew extends SeekBar
{

    private boolean isInScrollingContainer;
    private boolean mIsDragging;
    private int mScaledTouchSlop;
    private float mTouchDownY;
    float mTouchProgressOffset;

    public VerticalSeekBarNew(Context context)
    {
        super(context);
        isInScrollingContainer = false;
    }

    public VerticalSeekBarNew(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        isInScrollingContainer = false;
    }

    public VerticalSeekBarNew(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        isInScrollingContainer = false;
        mScaledTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    private void attemptClaimDrag()
    {
        ViewParent viewparent = getParent();
        if (viewparent != null)
        {
            viewparent.requestDisallowInterceptTouchEvent(true);
        }
    }

    private void trackTouchEvent(MotionEvent motionevent)
    {
        float f1 = 0.0F;
        int i = getHeight();
        int j = getPaddingTop();
        int k = getPaddingBottom();
        int l = i - j - k;
        int i1 = (int)motionevent.getY();
        float f;
        if (i1 > i - k)
        {
            f = 0.0F;
        } else
        if (i1 < j)
        {
            f = 1.0F;
        } else
        {
            f = (float)((l - i1) + j) / (float)l;
            f1 = mTouchProgressOffset;
        }
        setProgress((int)(f1 + f * (float)getMax()));
    }

    public boolean isInScrollingContainer()
    {
        return isInScrollingContainer;
    }

    protected void onDraw(Canvas canvas)
    {
        this;
        JVM INSTR monitorenter ;
        canvas.rotate(-90F);
        canvas.translate(-getHeight(), 0.0F);
        super.onDraw(canvas);
        this;
        JVM INSTR monitorexit ;
        return;
        canvas;
        throw canvas;
    }

    protected void onMeasure(int i, int j)
    {
        this;
        JVM INSTR monitorenter ;
        super.onMeasure(j, i);
        setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    protected void onSizeChanged(int i, int j, int k, int l)
    {
        super.onSizeChanged(j, i, l, k);
    }

    void onStartTrackingTouch()
    {
        mIsDragging = true;
    }

    void onStopTrackingTouch()
    {
        mIsDragging = false;
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        if (!isEnabled())
        {
            return false;
        }
        motionevent.getAction();
        JVM INSTR tableswitch 0 2: default 40
    //                   0 42
    //                   1 174
    //                   2 99;
           goto _L1 _L2 _L3 _L4
_L1:
        return true;
_L2:
        if (isInScrollingContainer())
        {
            mTouchDownY = motionevent.getY();
        } else
        {
            setPressed(true);
            invalidate();
            onStartTrackingTouch();
            trackTouchEvent(motionevent);
            attemptClaimDrag();
            onSizeChanged(getWidth(), getHeight(), 0, 0);
        }
        continue; /* Loop/switch isn't completed */
_L4:
        if (!mIsDragging) goto _L6; else goto _L5
_L5:
        trackTouchEvent(motionevent);
_L7:
        onSizeChanged(getWidth(), getHeight(), 0, 0);
        continue; /* Loop/switch isn't completed */
_L6:
        if (Math.abs(motionevent.getY() - mTouchDownY) > (float)mScaledTouchSlop)
        {
            setPressed(true);
            invalidate();
            onStartTrackingTouch();
            trackTouchEvent(motionevent);
            attemptClaimDrag();
        }
        if (true) goto _L7; else goto _L3
_L3:
        if (mIsDragging)
        {
            trackTouchEvent(motionevent);
            onStopTrackingTouch();
            setPressed(false);
        } else
        {
            onStartTrackingTouch();
            trackTouchEvent(motionevent);
            onStopTrackingTouch();
        }
        onSizeChanged(getWidth(), getHeight(), 0, 0);
        invalidate();
        if (true) goto _L1; else goto _L8
_L8:
    }

    public void setInScrollingContainer(boolean flag)
    {
        isInScrollingContainer = flag;
    }

    public void setProgress(int i)
    {
        this;
        JVM INSTR monitorenter ;
        super.setProgress(i);
        onSizeChanged(getWidth(), getHeight(), 0, 0);
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }
}
