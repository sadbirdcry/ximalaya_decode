// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.progress;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ViewParent;
import android.widget.AbsSeekBar;

public class VerticalSeekBar extends AbsSeekBar
{
    public static interface OnSeekBarChangeListener
    {

        public abstract void onProgressChanged(VerticalSeekBar verticalseekbar, int i, boolean flag);

        public abstract void onStartTrackingTouch(VerticalSeekBar verticalseekbar);

        public abstract void onStopTrackingTouch(VerticalSeekBar verticalseekbar);
    }


    private OnSeekBarChangeListener mOnSeekBarChangeListener;
    private Drawable mThumb;
    private boolean mWait;

    public VerticalSeekBar(Context context)
    {
        this(context, null);
    }

    public VerticalSeekBar(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0x101007b);
    }

    public VerticalSeekBar(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mWait = true;
        setPressed(true);
    }

    private void claimDrag()
    {
        if (getParent() != null)
        {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
    }

    private void setThumbPosition(int i, Drawable drawable, float f, int j)
    {
        int l = getPaddingLeft();
        int j1 = getPaddingRight();
        int k = drawable.getIntrinsicWidth();
        int i1 = drawable.getIntrinsicHeight();
        l = (int)((float)((i - l - j1 - k) + getThumbOffset() * 2) * f);
        if (j == 0x80000000)
        {
            Rect rect = drawable.getBounds();
            j = rect.top;
            i = rect.bottom;
        } else
        if (j == 0x7fffffff)
        {
            j = (getWidth() - getPaddingTop() - getPaddingBottom() - k) / 2 + getPaddingTop();
            i = j + k;
        } else
        {
            i = j + i1;
        }
        drawable.setBounds(l, j, k + l, i);
    }

    private void trackEvent(MotionEvent motionevent)
    {
        int i = getHeight();
        int j = getPaddingBottom();
        int k = getPaddingTop();
        int l = (int)motionevent.getY();
        float f;
        if (l > i - getPaddingBottom())
        {
            f = 0.0F;
        } else
        if (l < getPaddingTop())
        {
            f = 1.0F;
        } else
        {
            f = (float)(i - getPaddingBottom() - l) / (float)(i - j - k);
        }
        setProgress((int)((float)getMax() * f));
        setThumbPosition(getHeight(), mThumb, f, 0x7fffffff);
    }

    public boolean dispatchKeyEvent(KeyEvent keyevent)
    {
        if (keyevent.getAction() != 0) goto _L2; else goto _L1
_L1:
        keyevent.getKeyCode();
        JVM INSTR tableswitch 19 22: default 40
    //                   19 59
    //                   20 73
    //                   21 87
    //                   22 101;
           goto _L3 _L4 _L5 _L6 _L7
_L3:
        keyevent = new KeyEvent(0, keyevent.getKeyCode());
_L8:
        return keyevent.dispatch(this);
_L4:
        keyevent = new KeyEvent(0, 22);
        continue; /* Loop/switch isn't completed */
_L5:
        keyevent = new KeyEvent(0, 21);
        continue; /* Loop/switch isn't completed */
_L6:
        keyevent = new KeyEvent(0, 20);
        continue; /* Loop/switch isn't completed */
_L7:
        keyevent = new KeyEvent(0, 19);
        if (true) goto _L8; else goto _L2
_L2:
        return false;
    }

    protected void onDraw(Canvas canvas)
    {
        canvas.rotate(-90F);
        canvas.translate(-getHeight(), 0.0F);
        if (mWait && getWidth() != 0)
        {
            setThumbPosition(getHeight(), mThumb, (float)getProgress() / 100F, 0x7fffffff);
            mWait = false;
        }
        super.onDraw(canvas);
    }

    void onRefresh(float f, boolean flag)
    {
        Drawable drawable = mThumb;
        if (drawable != null)
        {
            setThumbPosition(getHeight(), drawable, f, 0x80000000);
            invalidate();
        }
        if (mOnSeekBarChangeListener != null)
        {
            mOnSeekBarChangeListener.onProgressChanged(this, getProgress(), flag);
        }
    }

    protected void onSizeChanged(int i, int j, int k, int l)
    {
        super.onSizeChanged(j, i, k, l);
    }

    void onStartTracking()
    {
        if (mOnSeekBarChangeListener != null)
        {
            mOnSeekBarChangeListener.onStartTrackingTouch(this);
        }
    }

    void onStopTracking()
    {
        if (mOnSeekBarChangeListener != null)
        {
            mOnSeekBarChangeListener.onStopTrackingTouch(this);
        }
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        if (!isEnabled())
        {
            return false;
        }
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 44
    //                   0 46
    //                   1 75
    //                   2 63
    //                   3 92;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        return true;
_L2:
        setPressed(true);
        onStartTracking();
        trackEvent(motionevent);
        continue; /* Loop/switch isn't completed */
_L4:
        trackEvent(motionevent);
        claimDrag();
        continue; /* Loop/switch isn't completed */
_L3:
        trackEvent(motionevent);
        onStopTracking();
        setPressed(false);
        continue; /* Loop/switch isn't completed */
_L5:
        onStopTracking();
        setPressed(false);
        if (true) goto _L1; else goto _L6
_L6:
    }

    public void setOnSeekBarChangeListener(OnSeekBarChangeListener onseekbarchangelistener)
    {
        mOnSeekBarChangeListener = onseekbarchangelistener;
    }

    public void setProgress(int i)
    {
        this;
        JVM INSTR monitorenter ;
        float f = 0.0F;
        if (getMax() != 0)
        {
            f = (float)i / (float)getMax();
        }
        onRefresh(f, false);
        super.setProgress(i);
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void setSecondaryProgress(int i)
    {
        this;
        JVM INSTR monitorenter ;
        super.setSecondaryProgress(i);
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void setThumb(Drawable drawable)
    {
        mThumb = drawable;
        super.setThumb(drawable);
    }
}
