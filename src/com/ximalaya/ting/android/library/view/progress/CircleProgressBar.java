// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.progress;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

public class CircleProgressBar extends View
{

    public static final int FILL = 1;
    public static final int STROKE = 0;
    private int mMax;
    private Paint mPaint;
    private int mProgress;
    private int mRoundColor;
    private int mRoundProgressColor;
    private float mRoundWidth;
    private int mStyle;
    private int mTextColor;
    private boolean mTextIsDisplayable;
    private float mTextSize;

    public CircleProgressBar(Context context)
    {
        this(context, null);
    }

    public CircleProgressBar(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0);
    }

    public CircleProgressBar(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mPaint = new Paint();
        context = context.obtainStyledAttributes(attributeset, com.ximalaya.ting.android.library.R.styleable.CircleProgressBar);
        mRoundColor = context.getColor(0, 0xffff0000);
        mRoundProgressColor = context.getColor(1, 0xff00ff00);
        mTextColor = context.getColor(3, 0xff00ff00);
        mTextSize = context.getDimension(4, 15F);
        mRoundWidth = context.getDimension(2, 5F);
        mMax = context.getInteger(5, 100);
        mTextIsDisplayable = context.getBoolean(6, true);
        mStyle = context.getInt(7, 0);
        context.recycle();
    }

    public int getCricleColor()
    {
        return mRoundColor;
    }

    public int getCricleProgressColor()
    {
        return mRoundProgressColor;
    }

    public int getMax()
    {
        this;
        JVM INSTR monitorenter ;
        int i = mMax;
        this;
        JVM INSTR monitorexit ;
        return i;
        Exception exception;
        exception;
        throw exception;
    }

    public int getProgress()
    {
        this;
        JVM INSTR monitorenter ;
        int i = mProgress;
        this;
        JVM INSTR monitorexit ;
        return i;
        Exception exception;
        exception;
        throw exception;
    }

    public float getRoundWidth()
    {
        return mRoundWidth;
    }

    public int getTextColor()
    {
        return mTextColor;
    }

    public float getTextSize()
    {
        return mTextSize;
    }

    protected void onDraw(Canvas canvas)
    {
        RectF rectf;
        super.onDraw(canvas);
        int i = getWidth() - getPaddingLeft() - getPaddingRight();
        int k = getHeight() - getPaddingTop() - getPaddingBottom();
        float f2 = (float)Math.min(i, k) / 2.0F;
        float f = getPaddingLeft();
        f = (float)i / 2.0F + f;
        float f1 = (float)getPaddingTop() + (float)k / 2.0F;
        f2 -= mRoundWidth;
        mPaint.setColor(mRoundColor);
        mPaint.setStyle(android.graphics.Paint.Style.STROKE);
        mPaint.setStrokeWidth(mRoundWidth);
        mPaint.setAntiAlias(true);
        canvas.drawCircle(f, f1, f2, mPaint);
        if (mTextIsDisplayable && mMax != 0 && mStyle == 0)
        {
            mPaint.setStrokeWidth(0.0F);
            mPaint.setColor(mTextColor);
            mPaint.setTextSize(mTextSize);
            mPaint.setTypeface(Typeface.DEFAULT_BOLD);
            int j = (int)(((float)mProgress / (float)mMax) * 100F);
            float f3 = mPaint.measureText((new StringBuilder()).append(j).append("%").toString());
            canvas.drawText((new StringBuilder()).append(j).append("%").toString(), f - f3 / 2.0F, mTextSize / 2.0F + f1, mPaint);
        }
        mPaint.setStrokeWidth(mRoundWidth);
        mPaint.setColor(mRoundProgressColor);
        rectf = new RectF(f - f2, f1 - f2, f + f2, f2 + f1);
        mStyle;
        JVM INSTR tableswitch 0 1: default 360
    //                   0 361
    //                   1 399;
           goto _L1 _L2 _L3
_L1:
        return;
_L2:
        mPaint.setStyle(android.graphics.Paint.Style.STROKE);
        canvas.drawArc(rectf, -90F, (mProgress * 360) / mMax, false, mPaint);
        return;
_L3:
        mPaint.setStyle(android.graphics.Paint.Style.FILL_AND_STROKE);
        if (mProgress != 0)
        {
            canvas.drawArc(rectf, -90F, (mProgress * 360) / mMax, true, mPaint);
            return;
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    public void setCricleColor(int i)
    {
        mRoundColor = i;
    }

    public void setCricleProgressColor(int i)
    {
        mRoundProgressColor = i;
    }

    public void setMax(int i)
    {
        this;
        JVM INSTR monitorenter ;
        if (i >= 0)
        {
            break MISSING_BLOCK_LABEL_21;
        }
        throw new IllegalArgumentException("max not less than 0");
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        mMax = i;
        this;
        JVM INSTR monitorexit ;
    }

    public void setProgress(int i)
    {
        this;
        JVM INSTR monitorenter ;
        if (i >= 0)
        {
            break MISSING_BLOCK_LABEL_21;
        }
        throw new IllegalArgumentException("progress not less than 0");
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        int j = i;
        if (i > mMax)
        {
            j = mMax;
        }
        if (j <= mMax)
        {
            mProgress = j;
            postInvalidate();
        }
        this;
        JVM INSTR monitorexit ;
    }

    public void setRoundWidth(float f)
    {
        mRoundWidth = f;
    }

    public void setTextColor(int i)
    {
        mTextColor = i;
    }

    public void setTextSize(float f)
    {
        mTextSize = f;
    }
}
