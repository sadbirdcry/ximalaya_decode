// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.progress;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;

public class ForbidableSeekBar extends SeekBar
{

    private boolean isCanSeek;

    public ForbidableSeekBar(Context context)
    {
        super(context);
    }

    public ForbidableSeekBar(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
    }

    public ForbidableSeekBar(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
    }

    public boolean isCanSeek()
    {
        return isCanSeek;
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        return super.onTouchEvent(motionevent);
    }

    public void setCanSeek(boolean flag)
    {
        isCanSeek = flag;
    }
}
