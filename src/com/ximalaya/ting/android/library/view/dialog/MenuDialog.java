// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.ximalaya.ting.android.library.util.ToolUtilLib;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.library.view.dialog:
//            f

public class MenuDialog extends Dialog
{
    public static interface ExtraCallback
    {

        public abstract void execute(String s, ViewHolder viewholder);
    }

    public class ViewHolder
    {

        public ImageView icon;
        final MenuDialog this$0;
        public TextView title;

        public ViewHolder()
        {
            this$0 = MenuDialog.this;
            super();
        }
    }

    abstract class a extends BaseAdapter
    {

        final MenuDialog b;

        public abstract void a(String s, ViewHolder viewholder);

        public int getCount()
        {
            return b.mSelections.size();
        }

        public Object getItem(int i)
        {
            return b.mSelections.get(i);
        }

        public long getItemId(int i)
        {
            return 0L;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            if (view != null)
            {
                break MISSING_BLOCK_LABEL_187;
            }
            view = LayoutInflater.from(b.mContext).inflate(com.ximalaya.ting.android.library.R.layout.menu_dialog_item, null);
            viewgroup = b. new ViewHolder();
            view.setTag(viewgroup);
            viewgroup.title = (TextView)view.findViewById(com.ximalaya.ting.android.library.R.id.group_item);
            viewgroup.icon = (ImageView)view.findViewById(com.ximalaya.ting.android.library.R.id.new_feature);
            b.mDrawableType;
            JVM INSTR tableswitch 0 1: default 96
        //                       0 161
        //                       1 174;
               goto _L1 _L2 _L3
_L1:
            ((ViewHolder) (viewgroup)).title.setText((CharSequence)b.mSelections.get(i));
            if (i == b.mPosition2showNewIcon)
            {
                ((ViewHolder) (viewgroup)).icon.setVisibility(0);
            } else
            {
                ((ViewHolder) (viewgroup)).icon.setVisibility(8);
            }
            a((String)b.mSelections.get(i), viewgroup);
            return view;
_L2:
            ((ViewHolder) (viewgroup)).icon.setImageResource(com.ximalaya.ting.android.library.R.drawable.new_img);
              goto _L1
_L3:
            ((ViewHolder) (viewgroup)).icon.setImageResource(com.ximalaya.ting.android.library.R.drawable.wifi_device_selected);
              goto _L1
            viewgroup = (ViewHolder)view.getTag();
              goto _L1
        }

        a()
        {
            b = MenuDialog.this;
            super();
        }
    }


    public static final int TYPE_NEW = 0;
    public static final int TYPE_WIFI = 1;
    private boolean isPad;
    private a mAdapter;
    private ExtraCallback mCallback;
    private Activity mContext;
    private int mDrawableType;
    private ListView mListView;
    private android.widget.AdapterView.OnItemClickListener mListener;
    private int mPosition2showNewIcon;
    private List mSelections;
    private String mTitle;

    public MenuDialog(Activity activity, int i)
    {
        this(activity, getTitles(activity, i), false, null, ((ExtraCallback) (null)));
    }

    public MenuDialog(Activity activity, int i, android.widget.AdapterView.OnItemClickListener onitemclicklistener)
    {
        this(activity, getTitles(activity, i), false, onitemclicklistener, ((ExtraCallback) (null)));
    }

    public MenuDialog(Activity activity, int i, android.widget.AdapterView.OnItemClickListener onitemclicklistener, ExtraCallback extracallback)
    {
        this(activity, getTitles(activity, i), false, onitemclicklistener, extracallback);
    }

    public MenuDialog(Activity activity, int i, boolean flag, android.widget.AdapterView.OnItemClickListener onitemclicklistener)
    {
        this(activity, getTitles(activity, i), flag, onitemclicklistener, ((ExtraCallback) (null)));
    }

    public MenuDialog(Activity activity, int i, boolean flag, android.widget.AdapterView.OnItemClickListener onitemclicklistener, ExtraCallback extracallback)
    {
        this(activity, getTitles(activity, i), flag, onitemclicklistener, extracallback);
    }

    public MenuDialog(Activity activity, List list)
    {
        this(activity, list, false, null, ((ExtraCallback) (null)));
    }

    public MenuDialog(Activity activity, List list, android.widget.AdapterView.OnItemClickListener onitemclicklistener)
    {
        this(activity, list, false, onitemclicklistener, ((ExtraCallback) (null)));
    }

    public MenuDialog(Activity activity, List list, boolean flag)
    {
        this(activity, list, flag, null, ((ExtraCallback) (null)));
    }

    public MenuDialog(Activity activity, List list, boolean flag, android.widget.AdapterView.OnItemClickListener onitemclicklistener)
    {
        this(activity, list, flag, onitemclicklistener, ((ExtraCallback) (null)));
    }

    public MenuDialog(Activity activity, List list, boolean flag, android.widget.AdapterView.OnItemClickListener onitemclicklistener, int i)
    {
        this(activity, list, flag, onitemclicklistener, ((ExtraCallback) (null)));
        setDrawable(i);
    }

    public MenuDialog(Activity activity, List list, boolean flag, android.widget.AdapterView.OnItemClickListener onitemclicklistener, ExtraCallback extracallback)
    {
        super(activity, com.ximalaya.ting.android.library.R.style.menuDialog);
        mTitle = "\u8BF7\u9009\u62E9\u9700\u8981\u7684\u64CD\u4F5C";
        mDrawableType = 0;
        isPad = false;
        mPosition2showNewIcon = -1;
        mContext = activity;
        mSelections = list;
        mListener = onitemclicklistener;
        mCallback = extracallback;
    }

    private static List getTitles(Context context, int i)
    {
        ArrayList arraylist = new ArrayList();
        context = context.getApplicationContext().getResources().getTextArray(i);
        int j = context.length;
        for (i = 0; i < j; i++)
        {
            arraylist.add(context[i].toString());
        }

        return arraylist;
    }

    public int getListNewIconPosition()
    {
        return mPosition2showNewIcon;
    }

    public List getSelections()
    {
        return mSelections;
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(com.ximalaya.ting.android.library.R.layout.menu_dialog);
        if (isPad)
        {
            bundle = (LinearLayout)findViewById(com.ximalaya.ting.android.library.R.id.context_ll);
            android.view.ViewGroup.LayoutParams layoutparams = bundle.getLayoutParams();
            layoutparams.width = ToolUtilLib.dp2px(getContext(), 350F);
            bundle.setLayoutParams(layoutparams);
        }
        mListView = (ListView)findViewById(com.ximalaya.ting.android.library.R.id.listview);
        ((TextView)findViewById(com.ximalaya.ting.android.library.R.id.title_tv)).setText(mTitle);
        mAdapter = new f(this);
        mListView.setAdapter(mAdapter);
    }

    public void setDrawable(int i)
    {
        mDrawableType = i;
    }

    public void setHeaderTitle(String s)
    {
        mTitle = s;
    }

    public void setOnItemClickListener(android.widget.AdapterView.OnItemClickListener onitemclicklistener)
    {
        mListener = onitemclicklistener;
    }

    public void setSelection(int i, String s)
    {
        if (mSelections != null && i >= 0 && i <= mSelections.size())
        {
            mSelections.set(i, s);
            if (mAdapter != null)
            {
                mAdapter.notifyDataSetChanged();
                return;
            }
        }
    }

    public void setSelections(List list)
    {
        mSelections = list;
        if (mAdapter != null)
        {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void show()
    {
        super.show();
        mListView.setOnItemClickListener(mListener);
    }

    public void showListNewIcon(int i)
    {
        mPosition2showNewIcon = i;
        mAdapter.notifyDataSetChanged();
    }





}
