// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

// Referenced classes of package com.ximalaya.ting.android.library.view.dialog:
//            d, e, b, c, 
//            a

public class DialogBuilder
{
    public static interface DialogCallback
    {

        public abstract void onExecute();
    }

    public static interface UrlCallback
    {

        public abstract void goToUrl(String s);
    }

    private static class a extends ClickableSpan
    {

        private String a;
        private UrlCallback b;

        public void onClick(View view)
        {
            if (b != null)
            {
                b.goToUrl(a);
            }
        }

        a(String s, UrlCallback urlcallback)
        {
            a = s;
            b = urlcallback;
        }
    }


    private String cancelBtnText;
    private DialogCallback cancelCallback;
    private boolean cancelable;
    private AlertDialog dialog;
    private String extraBtnText;
    private DialogCallback extraCallback;
    private View layout;
    private CharSequence message;
    private String neutralBtnText;
    private DialogCallback neutralCallback;
    private String okBtnText;
    private DialogCallback okCallback;
    private android.content.DialogInterface.OnKeyListener onKeyListener;
    private boolean outsideTouchCancel;
    private boolean outsideTouchExecCallback;
    private String title;
    private UrlCallback urlCallback;
    private boolean withExtraButton;

    private DialogBuilder()
    {
        title = "\u6E29\u99A8\u63D0\u793A";
        message = "\u662F\u5426\u786E\u8BA4\uFF1F";
        okBtnText = "\u786E\u5B9A";
        neutralBtnText = "\u5FFD\u7565";
        extraBtnText = "";
        cancelBtnText = "\u53D6\u6D88";
        cancelable = true;
        outsideTouchCancel = true;
        outsideTouchExecCallback = true;
        withExtraButton = false;
    }

    public DialogBuilder(Context context)
    {
        title = "\u6E29\u99A8\u63D0\u793A";
        message = "\u662F\u5426\u786E\u8BA4\uFF1F";
        okBtnText = "\u786E\u5B9A";
        neutralBtnText = "\u5FFD\u7565";
        extraBtnText = "";
        cancelBtnText = "\u53D6\u6D88";
        cancelable = true;
        outsideTouchCancel = true;
        outsideTouchExecCallback = true;
        withExtraButton = false;
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            dialog = (new android.app.AlertDialog.Builder(context, com.ximalaya.ting.android.library.R.style.menuDialog)).create();
        } else
        {
            dialog = (new android.app.AlertDialog.Builder(context)).create();
        }
        layout = LayoutInflater.from(context).inflate(com.ximalaya.ting.android.library.R.layout.alert_dialog, null);
    }

    public DialogBuilder(Context context, boolean flag)
    {
        title = "\u6E29\u99A8\u63D0\u793A";
        message = "\u662F\u5426\u786E\u8BA4\uFF1F";
        okBtnText = "\u786E\u5B9A";
        neutralBtnText = "\u5FFD\u7565";
        extraBtnText = "";
        cancelBtnText = "\u53D6\u6D88";
        cancelable = true;
        outsideTouchCancel = true;
        outsideTouchExecCallback = true;
        withExtraButton = false;
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            dialog = (new android.app.AlertDialog.Builder(context, com.ximalaya.ting.android.library.R.style.menuDialog)).create();
        } else
        {
            dialog = (new android.app.AlertDialog.Builder(context)).create();
        }
        if (flag)
        {
            layout = LayoutInflater.from(context).inflate(com.ximalaya.ting.android.library.R.layout.alert_dialog_with_button, null);
        } else
        {
            layout = LayoutInflater.from(context).inflate(com.ximalaya.ting.android.library.R.layout.alert_dialog, null);
        }
        withExtraButton = flag;
    }

    private void applyCancel(boolean flag)
    {
        if (flag)
        {
            Button button = (Button)layout.findViewById(com.ximalaya.ting.android.library.R.id.cancel_btn);
            button.setText(cancelBtnText);
            button.setOnClickListener(new d(this));
            button.setVisibility(0);
        } else
        {
            layout.findViewById(com.ximalaya.ting.android.library.R.id.cancel_btn).setVisibility(8);
        }
        if (outsideTouchCancel && outsideTouchExecCallback)
        {
            dialog.setOnCancelListener(new e(this));
        }
    }

    private void applyExtra()
    {
        if (withExtraButton)
        {
            ((RelativeLayout)layout.findViewById(com.ximalaya.ting.android.library.R.id.extra_layout)).setOnClickListener(new b(this));
            ((TextView)layout.findViewById(com.ximalaya.ting.android.library.R.id.extra_btn_tv)).setText(extraBtnText);
        }
    }

    private void applyNeutral(boolean flag)
    {
        if (flag)
        {
            Button button = (Button)layout.findViewById(com.ximalaya.ting.android.library.R.id.neutral_btn);
            button.setText(neutralBtnText);
            button.setOnClickListener(new c(this));
            return;
        } else
        {
            layout.findViewById(com.ximalaya.ting.android.library.R.id.neutral_btn).setVisibility(8);
            return;
        }
    }

    private void applyOk(boolean flag)
    {
        if (flag)
        {
            Button button = (Button)layout.findViewById(com.ximalaya.ting.android.library.R.id.ok_btn);
            button.setText(okBtnText);
            button.setOnClickListener(new com.ximalaya.ting.android.library.view.dialog.a(this));
            button.setVisibility(0);
            return;
        } else
        {
            layout.findViewById(com.ximalaya.ting.android.library.R.id.ok_btn).setVisibility(8);
            return;
        }
    }

    private void initMain()
    {
        dialog.show();
        dialog.getWindow().setGravity(17);
        dialog.getWindow().setContentView(layout);
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(outsideTouchCancel);
        if (android.os.Build.VERSION.SDK_INT >= 14)
        {
            dialog.getWindow().addFlags(2);
            dialog.getWindow().setDimAmount(0.5F);
        }
        if (onKeyListener != null)
        {
            dialog.setOnKeyListener(onKeyListener);
        }
        ((TextView)layout.findViewById(com.ximalaya.ting.android.library.R.id.title_tv)).setText(title);
        if (urlCallback != null)
        {
            setTextWithHtml((TextView)layout.findViewById(com.ximalaya.ting.android.library.R.id.msg_tv), message, urlCallback);
            return;
        } else
        {
            ((TextView)layout.findViewById(com.ximalaya.ting.android.library.R.id.msg_tv)).setText(message);
            return;
        }
    }

    private void refreshBorder(int i)
    {
        if (i == 0)
        {
            layout.findViewById(com.ximalaya.ting.android.library.R.id.btn_separator_border_1).setVisibility(8);
            layout.findViewById(com.ximalaya.ting.android.library.R.id.btn_separator_border_2).setVisibility(8);
        } else
        {
            if (i == 1)
            {
                layout.findViewById(com.ximalaya.ting.android.library.R.id.btn_separator_border_1).setVisibility(0);
                layout.findViewById(com.ximalaya.ting.android.library.R.id.btn_separator_border_2).setVisibility(8);
                return;
            }
            if (i == 2)
            {
                layout.findViewById(com.ximalaya.ting.android.library.R.id.btn_separator_border_1).setVisibility(0);
                layout.findViewById(com.ximalaya.ting.android.library.R.id.btn_separator_border_2).setVisibility(0);
                return;
            }
        }
    }

    private static void setTextWithHtml(TextView textview, CharSequence charsequence, UrlCallback urlcallback)
    {
        int i = 0;
        textview.setText(Html.fromHtml(charsequence.toString()));
        textview.setMovementMethod(LinkMovementMethod.getInstance());
        Object obj = textview.getText();
        if (obj instanceof Spannable)
        {
            int j = ((CharSequence) (obj)).length();
            charsequence = (Spannable)textview.getText();
            URLSpan aurlspan[] = (URLSpan[])charsequence.getSpans(0, j, android/text/style/URLSpan);
            obj = new SpannableStringBuilder(((CharSequence) (obj)));
            ((SpannableStringBuilder) (obj)).clearSpans();
            for (int k = aurlspan.length; i < k; i++)
            {
                URLSpan urlspan = aurlspan[i];
                ((SpannableStringBuilder) (obj)).setSpan(new a(urlspan.getURL(), urlcallback), charsequence.getSpanStart(urlspan), charsequence.getSpanEnd(urlspan), 34);
            }

            textview.setText(((CharSequence) (obj)));
        }
    }

    public DialogBuilder setCancelBtn(int i)
    {
        cancelBtnText = dialog.getContext().getString(i);
        return this;
    }

    public DialogBuilder setCancelBtn(int i, DialogCallback dialogcallback)
    {
        cancelBtnText = dialog.getContext().getString(i);
        cancelCallback = dialogcallback;
        return this;
    }

    public DialogBuilder setCancelBtn(DialogCallback dialogcallback)
    {
        cancelCallback = dialogcallback;
        return this;
    }

    public DialogBuilder setCancelBtn(String s)
    {
        cancelBtnText = s;
        return this;
    }

    public DialogBuilder setCancelBtn(String s, DialogCallback dialogcallback)
    {
        cancelBtnText = s;
        cancelCallback = dialogcallback;
        return this;
    }

    public DialogBuilder setCancelable(boolean flag)
    {
        cancelable = flag;
        if (!flag)
        {
            setOutsideTouchCancel(false);
            setOutsideTouchExecCallback(false);
        }
        return this;
    }

    public DialogBuilder setExtraBtn(String s, DialogCallback dialogcallback)
    {
        extraBtnText = s;
        extraCallback = dialogcallback;
        return this;
    }

    public DialogBuilder setMessage(int i)
    {
        message = dialog.getContext().getString(i);
        return this;
    }

    public DialogBuilder setMessage(CharSequence charsequence)
    {
        message = charsequence;
        return this;
    }

    public DialogBuilder setNeutralBtn(int i, DialogCallback dialogcallback)
    {
        neutralBtnText = dialog.getContext().getString(i);
        neutralCallback = dialogcallback;
        return this;
    }

    public DialogBuilder setNeutralBtn(String s, DialogCallback dialogcallback)
    {
        neutralBtnText = s;
        neutralCallback = dialogcallback;
        return this;
    }

    public DialogBuilder setOkBtn(int i)
    {
        cancelBtnText = dialog.getContext().getString(i);
        return this;
    }

    public DialogBuilder setOkBtn(int i, DialogCallback dialogcallback)
    {
        okBtnText = dialog.getContext().getString(i);
        okCallback = dialogcallback;
        return this;
    }

    public DialogBuilder setOkBtn(DialogCallback dialogcallback)
    {
        okCallback = dialogcallback;
        return this;
    }

    public DialogBuilder setOkBtn(String s)
    {
        okBtnText = s;
        return this;
    }

    public DialogBuilder setOkBtn(String s, DialogCallback dialogcallback)
    {
        okBtnText = s;
        okCallback = dialogcallback;
        return this;
    }

    public DialogBuilder setOnKeyListener(android.content.DialogInterface.OnKeyListener onkeylistener)
    {
        onKeyListener = onkeylistener;
        return this;
    }

    public DialogBuilder setOutsideTouchCancel(boolean flag)
    {
        if (!flag)
        {
            outsideTouchExecCallback = false;
        }
        outsideTouchCancel = flag;
        return this;
    }

    public DialogBuilder setOutsideTouchExecCallback(boolean flag)
    {
        outsideTouchExecCallback = flag;
        return this;
    }

    public DialogBuilder setTextClickable()
    {
        ((TextView)layout.findViewById(com.ximalaya.ting.android.library.R.id.msg_tv)).setMovementMethod(LinkMovementMethod.getInstance());
        return this;
    }

    public DialogBuilder setTitle(int i)
    {
        title = dialog.getContext().getString(i);
        return this;
    }

    public DialogBuilder setTitle(String s)
    {
        title = s;
        return this;
    }

    public DialogBuilder setUrlCallback(UrlCallback urlcallback)
    {
        urlCallback = urlcallback;
        return this;
    }

    public void showConfirm()
    {
        initMain();
        applyExtra();
        applyOk(true);
        applyCancel(true);
        applyNeutral(false);
        refreshBorder(1);
    }

    public void showMultiButton()
    {
        initMain();
        applyExtra();
        applyOk(true);
        applyCancel(true);
        applyNeutral(true);
        refreshBorder(2);
    }

    public void showWarning()
    {
        initMain();
        applyExtra();
        applyNeutral(false);
        applyCancel(false);
        applyOk(true);
        layout.findViewById(com.ximalaya.ting.android.library.R.id.ok_btn).setBackgroundResource(com.ximalaya.ting.android.library.R.drawable.round_bottom_btn_bg_selector);
        refreshBorder(0);
    }





}
