// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.dialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MyProgressDialog extends ProgressDialog
{

    private boolean indeterminate;
    private View layout;
    private String message;
    private String title;

    public MyProgressDialog(Context context)
    {
        super(context);
        indeterminate = false;
    }

    public MyProgressDialog(Context context, int i)
    {
        super(context, i);
        indeterminate = false;
    }

    public void setMessage(CharSequence charsequence)
    {
        if (charsequence == null)
        {
            charsequence = "";
        } else
        {
            charsequence = charsequence.toString();
        }
        message = charsequence;
        if (layout != null)
        {
            ((TextView)layout.findViewById(com.ximalaya.ting.android.library.R.id.msg_tv)).setText(message);
        }
    }

    public void setTitle(CharSequence charsequence)
    {
        if (charsequence == null)
        {
            charsequence = "";
        } else
        {
            charsequence = charsequence.toString();
        }
        title = charsequence;
        if (layout != null)
        {
            ((TextView)layout.findViewById(com.ximalaya.ting.android.library.R.id.title_tv)).setText(message);
        }
    }

    public void show()
    {
        super.show();
        layout = LayoutInflater.from(getContext()).inflate(com.ximalaya.ting.android.library.R.layout.progress_dialog, null);
        getWindow().setContentView(layout);
        getWindow().setGravity(17);
        if (!TextUtils.isEmpty(title) || !TextUtils.isEmpty(message)) goto _L2; else goto _L1
_L1:
        TextView textview;
        layout.findViewById(com.ximalaya.ting.android.library.R.id.title_tv).setVisibility(0);
        layout.findViewById(com.ximalaya.ting.android.library.R.id.title_border).setVisibility(0);
        textview = (TextView)layout.findViewById(com.ximalaya.ting.android.library.R.id.title_tv);
        Object obj;
        if (TextUtils.isEmpty(title))
        {
            obj = "\u6E29\u99A8\u63D0\u793A";
        } else
        {
            try
            {
                obj = title;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((Exception) (obj)).printStackTrace();
                return;
            }
        }
_L7:
        textview.setText(((CharSequence) (obj)));
        ((TextView)layout.findViewById(com.ximalaya.ting.android.library.R.id.msg_tv)).setText("\u52A0\u8F7D\u4E2D...");
_L4:
        ((ProgressBar)layout.findViewById(com.ximalaya.ting.android.library.R.id.progress_bar)).setIndeterminate(indeterminate);
        if (android.os.Build.VERSION.SDK_INT >= 14)
        {
            getWindow().addFlags(2);
            getWindow().setDimAmount(0.5F);
            return;
        }
          goto _L3
_L2:
        layout.findViewById(com.ximalaya.ting.android.library.R.id.title_tv).setVisibility(0);
        layout.findViewById(com.ximalaya.ting.android.library.R.id.title_border).setVisibility(0);
        textview = (TextView)layout.findViewById(com.ximalaya.ting.android.library.R.id.title_tv);
        if (!TextUtils.isEmpty(title))
        {
            break MISSING_BLOCK_LABEL_283;
        }
        obj = "\u6E29\u99A8\u63D0\u793A";
_L5:
        textview.setText(((CharSequence) (obj)));
        ((TextView)layout.findViewById(com.ximalaya.ting.android.library.R.id.msg_tv)).setText(message);
          goto _L4
        obj = title;
          goto _L5
_L3:
        return;
        if (true) goto _L7; else goto _L6
_L6:
    }
}
