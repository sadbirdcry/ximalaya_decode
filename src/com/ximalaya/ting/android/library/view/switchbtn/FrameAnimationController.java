// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.switchbtn;

import android.os.Handler;
import android.os.Message;

public class FrameAnimationController
{
    private static class a extends Handler
    {

        public void handleMessage(Message message)
        {
            message.what;
            JVM INSTR tableswitch 1000 1000: default 24
        //                       1000 25;
               goto _L1 _L2
_L1:
            return;
_L2:
            if (message.obj != null)
            {
                ((Runnable)message.obj).run();
                return;
            }
            if (true) goto _L1; else goto _L3
_L3:
        }

        private a()
        {
        }

    }


    public static final int ANIMATION_FRAME_DURATION = 16;
    private static final int MSG_ANIMATE = 1000;
    private static final Handler mHandler = new a();

    private FrameAnimationController()
    {
        throw new UnsupportedOperationException();
    }

    public static void requestAnimationFrame(Runnable runnable)
    {
        Message message = new Message();
        message.what = 1000;
        message.obj = runnable;
        mHandler.sendMessageDelayed(message, 16L);
    }

    public static void requestFrameDelay(Runnable runnable, long l)
    {
        Message message = new Message();
        message.what = 1000;
        message.obj = runnable;
        mHandler.sendMessageDelayed(message, l);
    }

}
