// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view.switchbtn;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.widget.CheckBox;

// Referenced classes of package com.ximalaya.ting.android.library.view.switchbtn:
//            a, FrameAnimationController

public class SwitchButton extends CheckBox
{
    private final class a
        implements Runnable
    {

        final SwitchButton a;

        public void run()
        {
            a.performClick();
        }

        private a()
        {
            a = SwitchButton.this;
            super();
        }

        a(com.ximalaya.ting.android.library.view.switchbtn.a a1)
        {
            this();
        }
    }

    private final class b
        implements Runnable
    {

        final SwitchButton a;

        public void run()
        {
            if (!a.mAnimating)
            {
                return;
            } else
            {
                a.doAnimation();
                FrameAnimationController.requestAnimationFrame(this);
                return;
            }
        }

        private b()
        {
            a = SwitchButton.this;
            super();
        }

        b(com.ximalaya.ting.android.library.view.switchbtn.a a1)
        {
            this();
        }
    }


    private final float EXTENDED_OFFSET_Y;
    private final int MAX_ALPHA;
    private final float VELOCITY;
    private int mAlpha;
    private float mAnimatedVelocity;
    private boolean mAnimating;
    private float mAnimationPosition;
    private Bitmap mBottom;
    private boolean mBroadcasting;
    private float mBtnInitPos;
    private Bitmap mBtnNormal;
    private float mBtnOffPos;
    private float mBtnOnPos;
    private float mBtnPos;
    private Bitmap mBtnPressed;
    private float mBtnWidth;
    private boolean mChecked;
    private int mClickTimeout;
    private Bitmap mCurBtnPic;
    private float mExtendOffsetY;
    private float mFirstDownX;
    private float mFirstDownY;
    private Bitmap mFrame;
    private Bitmap mMask;
    private float mMaskHeight;
    private float mMaskWidth;
    private android.widget.CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener;
    private android.widget.CompoundButton.OnCheckedChangeListener mOnCheckedChangeWidgetListener;
    private Paint mPaint;
    private ViewParent mParent;
    private a mPerformClick;
    private float mRealPos;
    private RectF mSaveLayerRectF;
    private int mTouchSlop;
    private boolean mTurningOn;
    private float mVelocity;
    private PorterDuffXfermode mXfermode;

    public SwitchButton(Context context)
    {
        this(context, null);
    }

    public SwitchButton(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0x101006c);
    }

    public SwitchButton(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        MAX_ALPHA = 255;
        mAlpha = 255;
        mChecked = false;
        VELOCITY = 350F;
        EXTENDED_OFFSET_Y = 15F;
        initView(context);
    }

    private void attemptClaimDrag()
    {
        mParent = getParent();
        if (mParent != null)
        {
            mParent.requestDisallowInterceptTouchEvent(true);
        }
    }

    private void doAnimation()
    {
        mAnimationPosition = mAnimationPosition + (mAnimatedVelocity * 16F) / 1000F;
        if (mAnimationPosition < mBtnOnPos) goto _L2; else goto _L1
_L1:
        stopAnimation();
        mAnimationPosition = mBtnOnPos;
        setCheckedDelayed(true);
_L4:
        moveView(mAnimationPosition);
        return;
_L2:
        if (mAnimationPosition <= mBtnOffPos)
        {
            stopAnimation();
            mAnimationPosition = mBtnOffPos;
            setCheckedDelayed(false);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private float getRealPos(float f)
    {
        return f - mBtnWidth / 2.0F;
    }

    private void initView(Context context)
    {
        mPaint = new Paint();
        mPaint.setColor(-1);
        Resources resources = context.getResources();
        mClickTimeout = ViewConfiguration.getPressedStateDuration() + ViewConfiguration.getTapTimeout();
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        mBottom = BitmapFactory.decodeResource(resources, com.ximalaya.ting.android.library.R.drawable.bottom);
        mBtnPressed = BitmapFactory.decodeResource(resources, com.ximalaya.ting.android.library.R.drawable.btn_pressed);
        mBtnNormal = BitmapFactory.decodeResource(resources, com.ximalaya.ting.android.library.R.drawable.btn_unpressed);
        mFrame = BitmapFactory.decodeResource(resources, com.ximalaya.ting.android.library.R.drawable.frame);
        mMask = BitmapFactory.decodeResource(resources, com.ximalaya.ting.android.library.R.drawable.mask);
        mCurBtnPic = mBtnNormal;
        mBtnWidth = mBtnPressed.getWidth();
        mMaskWidth = mMask.getWidth();
        mMaskHeight = mMask.getHeight();
        mBtnOnPos = mBtnWidth / 2.0F;
        mBtnOffPos = mMaskWidth - mBtnWidth / 2.0F;
        float f;
        if (mChecked)
        {
            f = mBtnOnPos;
        } else
        {
            f = mBtnOffPos;
        }
        mBtnPos = f;
        mRealPos = getRealPos(mBtnPos);
        f = getResources().getDisplayMetrics().density;
        mVelocity = (int)(350F * f + 0.5F);
        mExtendOffsetY = (int)(f * 15F + 0.5F);
        mSaveLayerRectF = new RectF(0.0F, mExtendOffsetY, mMask.getWidth(), (float)mMask.getHeight() + mExtendOffsetY);
        mXfermode = new PorterDuffXfermode(android.graphics.PorterDuff.Mode.SRC_IN);
    }

    private void moveView(float f)
    {
        mBtnPos = f;
        mRealPos = getRealPos(mBtnPos);
        invalidate();
    }

    private void setCheckedDelayed(boolean flag)
    {
        postDelayed(new com.ximalaya.ting.android.library.view.switchbtn.a(this, flag), 10L);
    }

    private void startAnimation(boolean flag)
    {
        mAnimating = true;
        float f;
        if (flag)
        {
            f = -mVelocity;
        } else
        {
            f = mVelocity;
        }
        mAnimatedVelocity = f;
        mAnimationPosition = mBtnPos;
        (new b(null)).run();
    }

    private void stopAnimation()
    {
        mAnimating = false;
    }

    public void initCheckedState(boolean flag)
    {
label0:
        {
            if (mChecked != flag)
            {
                mChecked = flag;
                float f;
                if (flag)
                {
                    f = mBtnOnPos;
                } else
                {
                    f = mBtnOffPos;
                }
                mBtnPos = f;
                mRealPos = getRealPos(mBtnPos);
                invalidate();
                if (!mBroadcasting)
                {
                    break label0;
                }
            }
            return;
        }
        mBroadcasting = true;
        mBroadcasting = false;
    }

    public boolean isChecked()
    {
        return mChecked;
    }

    protected void onDraw(Canvas canvas)
    {
        canvas.saveLayerAlpha(mSaveLayerRectF, mAlpha, 31);
        canvas.drawBitmap(mMask, 0.0F, mExtendOffsetY, mPaint);
        mPaint.setXfermode(mXfermode);
        canvas.drawBitmap(mBottom, mRealPos, mExtendOffsetY, mPaint);
        mPaint.setXfermode(null);
        canvas.drawBitmap(mFrame, 0.0F, mExtendOffsetY, mPaint);
        canvas.drawBitmap(mCurBtnPic, mRealPos, mExtendOffsetY, mPaint);
        canvas.restore();
    }

    protected void onMeasure(int i, int j)
    {
        setMeasuredDimension((int)mMaskWidth, (int)(mMaskHeight + 2.0F * mExtendOffsetY));
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        float f;
        float f1;
        float f2;
        float f4;
        int i;
        boolean flag;
        boolean flag2;
        flag2 = true;
        flag = true;
        i = motionevent.getAction();
        f2 = motionevent.getX();
        f4 = motionevent.getY();
        f = Math.abs(f2 - mFirstDownX);
        f1 = Math.abs(f4 - mFirstDownY);
        i;
        JVM INSTR tableswitch 0 2: default 76
    //                   0 85
    //                   1 257
    //                   2 137;
           goto _L1 _L2 _L3 _L4
_L1:
        invalidate();
        return isEnabled();
_L2:
        attemptClaimDrag();
        mFirstDownX = f2;
        mFirstDownY = f4;
        mCurBtnPic = mBtnPressed;
        if (mChecked)
        {
            f = mBtnOnPos;
        } else
        {
            f = mBtnOffPos;
        }
        mBtnInitPos = f;
        continue; /* Loop/switch isn't completed */
_L4:
        f = motionevent.getEventTime() - motionevent.getDownTime();
        mBtnPos = (mBtnInitPos + motionevent.getX()) - mFirstDownX;
        if (mBtnPos <= mBtnOffPos)
        {
            mBtnPos = mBtnOffPos;
        }
        if (mBtnPos >= mBtnOnPos)
        {
            mBtnPos = mBtnOnPos;
        }
        if (mBtnPos <= (mBtnOnPos - mBtnOffPos) / 2.0F + mBtnOffPos)
        {
            flag = false;
        }
        mTurningOn = flag;
        mRealPos = getRealPos(mBtnPos);
        continue; /* Loop/switch isn't completed */
_L3:
        mCurBtnPic = mBtnNormal;
        float f3 = motionevent.getEventTime() - motionevent.getDownTime();
        if (f1 < (float)mTouchSlop && f < (float)mTouchSlop && f3 < (float)mClickTimeout)
        {
            if (mPerformClick == null)
            {
                mPerformClick = new a(null);
            }
            if (!post(mPerformClick))
            {
                performClick();
            }
        } else
        {
            boolean flag1;
            if (!mTurningOn)
            {
                flag1 = flag2;
            } else
            {
                flag1 = false;
            }
            startAnimation(flag1);
        }
        if (true) goto _L1; else goto _L5
_L5:
    }

    public boolean performClick()
    {
        startAnimation(mChecked);
        return true;
    }

    public void setChecked(boolean flag)
    {
label0:
        {
            if (mChecked != flag)
            {
                mChecked = flag;
                float f;
                if (flag)
                {
                    f = mBtnOnPos;
                } else
                {
                    f = mBtnOffPos;
                }
                mBtnPos = f;
                mRealPos = getRealPos(mBtnPos);
                invalidate();
                if (!mBroadcasting)
                {
                    break label0;
                }
            }
            return;
        }
        mBroadcasting = true;
        if (mOnCheckedChangeListener != null)
        {
            mOnCheckedChangeListener.onCheckedChanged(this, mChecked);
        }
        if (mOnCheckedChangeWidgetListener != null)
        {
            mOnCheckedChangeWidgetListener.onCheckedChanged(this, mChecked);
        }
        mBroadcasting = false;
    }

    public void setEnabled(boolean flag)
    {
        int i;
        if (flag)
        {
            i = 255;
        } else
        {
            i = 127;
        }
        mAlpha = i;
        super.setEnabled(flag);
    }

    public void setOnCheckedChangeListener(android.widget.CompoundButton.OnCheckedChangeListener oncheckedchangelistener)
    {
        mOnCheckedChangeListener = oncheckedchangelistener;
    }

    void setOnCheckedChangeWidgetListener(android.widget.CompoundButton.OnCheckedChangeListener oncheckedchangelistener)
    {
        mOnCheckedChangeWidgetListener = oncheckedchangelistener;
    }

    public void toggle()
    {
        boolean flag;
        if (!mChecked)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        setChecked(flag);
    }


}
