// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.Scroller;
import java.lang.reflect.Field;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.library.view:
//            a, b

public class OverScrollView extends FrameLayout
    implements android.view.View.OnTouchListener
{

    static final int ANIMATED_SCROLL_GAP = 250;
    private static final int INVALID_POINTER = -1;
    static final float MAX_SCROLL_FACTOR = 0.5F;
    static final float OVERSHOOT_TENSION = 0.75F;
    protected View child;
    boolean hasFailedObtainingScrollFields;
    LayoutInflater inflater;
    boolean isInFlingMode;
    private boolean isXDragged;
    private int mActivePointerId;
    private View mChildToScrollTo;
    protected Context mContext;
    private boolean mFillViewport;
    private boolean mIsBeingDragged;
    private boolean mIsLayoutDirty;
    private float mLastMotionX;
    private float mLastMotionY;
    private long mLastScroll;
    private int mMaximumVelocity;
    private int mMinimumVelocity;
    private boolean mScrollViewMovedFocus;
    Field mScrollXField;
    Field mScrollYField;
    private Scroller mScroller;
    private boolean mSmoothScrollingEnabled;
    private final Rect mTempRect;
    private int mTouchSlop;
    private VelocityTracker mVelocityTracker;
    DisplayMetrics metrics;
    private Runnable overScrollerSpringbackTask;
    int prevScrollY;

    public OverScrollView(Context context)
    {
        this(context, null);
        mContext = context;
        initBounce();
    }

    public OverScrollView(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0);
        mContext = context;
        initBounce();
    }

    public OverScrollView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mTempRect = new Rect();
        isInFlingMode = false;
        mIsLayoutDirty = true;
        mChildToScrollTo = null;
        mIsBeingDragged = false;
        mSmoothScrollingEnabled = true;
        mActivePointerId = -1;
        isXDragged = false;
        mContext = context;
        initScrollView();
        setFillViewport(true);
        initBounce();
    }

    private void SetScrollX(int i)
    {
        if (mScrollXField == null)
        {
            break MISSING_BLOCK_LABEL_16;
        }
        mScrollXField.setInt(this, i);
        return;
        Exception exception;
        exception;
    }

    private void SetScrollY(int i)
    {
        if (mScrollYField == null)
        {
            break MISSING_BLOCK_LABEL_16;
        }
        mScrollYField.setInt(this, i);
        return;
        Exception exception;
        exception;
    }

    private boolean canScroll()
    {
        boolean flag1 = false;
        View view = getChildAt(0);
        boolean flag = flag1;
        if (view != null)
        {
            int i = view.getHeight();
            flag = flag1;
            if (getHeight() < i + getPaddingTop() + getPaddingBottom())
            {
                flag = true;
            }
        }
        return flag;
    }

    private int clamp(int i, int j, int k)
    {
        int l;
        if (j >= k || i < 0)
        {
            l = 0;
        } else
        {
            l = i;
            if (j + i > k)
            {
                return k - j;
            }
        }
        return l;
    }

    private void doScrollY(int i)
    {
label0:
        {
            if (i != 0)
            {
                if (!mSmoothScrollingEnabled)
                {
                    break label0;
                }
                smoothScrollBy(0, i);
            }
            return;
        }
        scrollBy(0, i);
    }

    private View findFocusableViewInBounds(boolean flag, int i, int j)
    {
        View view1;
        java.util.ArrayList arraylist;
        boolean flag1;
        int k;
        int i1;
        arraylist = getFocusables(2);
        view1 = null;
        flag1 = false;
        i1 = arraylist.size();
        k = 0;
_L2:
        View view;
        boolean flag2;
        int l;
        if (k >= i1)
        {
            break; /* Loop/switch isn't completed */
        }
        view = (View)arraylist.get(k);
        l = view.getTop();
        int j1 = view.getBottom();
        if (i >= j1 || l >= j)
        {
            break MISSING_BLOCK_LABEL_192;
        }
        if (i < l && j1 < j)
        {
            flag2 = true;
        } else
        {
            flag2 = false;
        }
        if (view1 == null)
        {
            flag1 = flag2;
        } else
        {
label0:
            {
                if (flag && l < view1.getTop() || !flag && j1 > view1.getBottom())
                {
                    l = 1;
                } else
                {
                    l = 0;
                }
                if (!flag1)
                {
                    break label0;
                }
                if (!flag2 || !l)
                {
                    break MISSING_BLOCK_LABEL_192;
                }
            }
        }
_L3:
        k++;
        view1 = view;
        if (true) goto _L2; else goto _L1
        if (flag2)
        {
            flag1 = true;
        } else
        if (!l)
        {
            break MISSING_BLOCK_LABEL_192;
        }
          goto _L3
_L1:
        return view1;
        view = view1;
          goto _L3
    }

    private View findFocusableViewInMyBounds(boolean flag, int i, View view)
    {
        int k = getVerticalFadingEdgeLength() / 2;
        int j = i + k;
        i = (getHeight() + i) - k;
        if (view != null && view.getTop() < i && view.getBottom() > j)
        {
            return view;
        } else
        {
            return findFocusableViewInBounds(flag, j, i);
        }
    }

    private void initBounce()
    {
        metrics = mContext.getResources().getDisplayMetrics();
        mScroller = new Scroller(getContext(), new OvershootInterpolator(0.75F));
        overScrollerSpringbackTask = new a(this);
        prevScrollY = getPaddingTop();
        try
        {
            mScrollXField = android/view/View.getDeclaredField("mScrollX");
            mScrollYField = android/view/View.getDeclaredField("mScrollY");
            return;
        }
        catch (Exception exception)
        {
            hasFailedObtainingScrollFields = true;
        }
    }

    private void initScrollView()
    {
        mScroller = new Scroller(getContext());
        setFocusable(true);
        setDescendantFocusability(0x40000);
        setWillNotDraw(false);
        ViewConfiguration viewconfiguration = ViewConfiguration.get(mContext);
        mTouchSlop = viewconfiguration.getScaledTouchSlop();
        mMinimumVelocity = viewconfiguration.getScaledMinimumFlingVelocity();
        mMaximumVelocity = viewconfiguration.getScaledMaximumFlingVelocity();
        setOnTouchListener(this);
        post(new b(this));
    }

    private boolean isOffScreen(View view)
    {
        boolean flag = false;
        if (!isWithinDeltaOfScreen(view, 0, getHeight()))
        {
            flag = true;
        }
        return flag;
    }

    private boolean isViewDescendantOf(View view, View view1)
    {
        if (view == view1)
        {
            return true;
        }
        view = view.getParent();
        boolean flag;
        if ((view instanceof ViewGroup) && isViewDescendantOf((View)view, view1))
        {
            flag = true;
        } else
        {
            flag = false;
        }
        return flag;
    }

    private boolean isWithinDeltaOfScreen(View view, int i, int j)
    {
        view.getDrawingRect(mTempRect);
        offsetDescendantRectToMyCoords(view, mTempRect);
        return mTempRect.bottom + i >= getScrollY() && mTempRect.top - i <= getScrollY() + j;
    }

    private void onSecondaryPointerUp(MotionEvent motionevent)
    {
        int i = (motionevent.getAction() & 0xff00) >> 8;
        if (motionevent.getPointerId(i) == mActivePointerId)
        {
            if (i == 0)
            {
                i = 1;
            } else
            {
                i = 0;
            }
            mLastMotionY = motionevent.getY(i);
            mActivePointerId = motionevent.getPointerId(i);
            if (mVelocityTracker != null)
            {
                mVelocityTracker.clear();
            }
        }
    }

    private boolean overScrollView()
    {
        int i = getHeight();
        int k = child.getPaddingTop();
        int l = child.getHeight() - child.getPaddingBottom();
        int j = getScrollY();
        if (j < k)
        {
            onOverScroll(j);
            i = k - j;
        } else
        if (j + i > l)
        {
            if (child.getHeight() - child.getPaddingTop() - child.getPaddingBottom() < i)
            {
                i = k - j;
            } else
            {
                i = l - i - j;
            }
            i += onOverScroll(j);
        } else
        {
            isInFlingMode = true;
            return false;
        }
        mScroller.startScroll(0, j, 0, i, 500);
        post(overScrollerSpringbackTask);
        prevScrollY = j;
        return true;
    }

    private boolean scrollAndFocus(int i, int j, int k)
    {
        int i1 = getHeight();
        int l = getScrollY();
        i1 = l + i1;
        Object obj;
        View view;
        boolean flag;
        if (i == 33)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        view = findFocusableViewInBounds(flag, j, k);
        obj = view;
        if (view == null)
        {
            obj = this;
        }
        if (j >= l && k <= i1)
        {
            flag = false;
        } else
        {
            if (flag)
            {
                j -= l;
            } else
            {
                j = k - i1;
            }
            doScrollY(j);
            flag = true;
        }
        if (obj != findFocus() && ((View) (obj)).requestFocus(i))
        {
            mScrollViewMovedFocus = true;
            mScrollViewMovedFocus = false;
        }
        return flag;
    }

    private void scrollToChild(View view)
    {
        view.getDrawingRect(mTempRect);
        offsetDescendantRectToMyCoords(view, mTempRect);
        int i = computeScrollDeltaToGetChildRectOnScreen(mTempRect);
        if (i != 0)
        {
            scrollBy(0, i);
        }
    }

    private boolean scrollToChildRect(Rect rect, boolean flag)
    {
        int i;
        boolean flag1;
label0:
        {
            i = computeScrollDeltaToGetChildRectOnScreen(rect);
            if (i != 0)
            {
                flag1 = true;
            } else
            {
                flag1 = false;
            }
            if (flag1)
            {
                if (!flag)
                {
                    break label0;
                }
                scrollBy(0, i);
            }
            return flag1;
        }
        smoothScrollBy(0, i);
        return flag1;
    }

    public void addView(View view)
    {
        if (getChildCount() > 0)
        {
            throw new IllegalStateException("ScrollView can host only one direct child");
        } else
        {
            super.addView(view);
            initChildPointer();
            return;
        }
    }

    public void addView(View view, int i)
    {
        if (getChildCount() > 0)
        {
            throw new IllegalStateException("ScrollView can host only one direct child");
        } else
        {
            super.addView(view, i);
            initChildPointer();
            return;
        }
    }

    public void addView(View view, int i, android.view.ViewGroup.LayoutParams layoutparams)
    {
        if (getChildCount() > 0)
        {
            throw new IllegalStateException("ScrollView can host only one direct child");
        } else
        {
            super.addView(view, i, layoutparams);
            return;
        }
    }

    public void addView(View view, android.view.ViewGroup.LayoutParams layoutparams)
    {
        if (getChildCount() > 0)
        {
            throw new IllegalStateException("ScrollView can host only one direct child");
        } else
        {
            super.addView(view, layoutparams);
            initChildPointer();
            return;
        }
    }

    public boolean arrowScroll(int i)
    {
        View view;
        View view1;
        int k;
        view1 = findFocus();
        view = view1;
        if (view1 == this)
        {
            view = null;
        }
        view1 = FocusFinder.getInstance().findNextFocus(this, view, i);
        k = getMaxScrollAmount();
        if (view1 == null || !isWithinDeltaOfScreen(view1, k, getHeight())) goto _L2; else goto _L1
_L1:
        view1.getDrawingRect(mTempRect);
        offsetDescendantRectToMyCoords(view1, mTempRect);
        doScrollY(computeScrollDeltaToGetChildRectOnScreen(mTempRect));
        view1.requestFocus(i);
_L8:
        if (view != null && view.isFocused() && isOffScreen(view))
        {
            i = getDescendantFocusability();
            setDescendantFocusability(0x20000);
            requestFocus();
            setDescendantFocusability(i);
        }
        return true;
_L2:
        if (i != 33 || getScrollY() >= k) goto _L4; else goto _L3
_L3:
        int j = getScrollY();
_L6:
        if (j == 0)
        {
            return false;
        }
        break; /* Loop/switch isn't completed */
_L4:
        j = k;
        if (i == 130)
        {
            j = k;
            if (getChildCount() > 0)
            {
                int l = getChildAt(0).getBottom();
                int i1 = getScrollY() + getHeight();
                j = k;
                if (l - i1 < k)
                {
                    j = l - i1;
                }
            }
        }
        if (true) goto _L6; else goto _L5
_L5:
        if (i != 130)
        {
            j = -j;
        }
        doScrollY(j);
        if (true) goto _L8; else goto _L7
_L7:
    }

    public void computeScroll()
    {
        if (hasFailedObtainingScrollFields)
        {
            super.computeScroll();
        } else
        if (mScroller.computeScrollOffset())
        {
            int i = getScrollX();
            int j = getScrollY();
            int l = mScroller.getCurrX();
            int k = mScroller.getCurrY();
            if (getChildCount() > 0)
            {
                View view = getChildAt(0);
                l = clamp(l, getWidth() - getPaddingRight() - getPaddingLeft(), view.getWidth());
                k = clamp(k, getHeight() - getPaddingBottom() - getPaddingTop(), view.getHeight());
                if (l != i || k != j)
                {
                    SetScrollX(l);
                    SetScrollY(k);
                    onScrollChanged(l, k, i, j);
                }
            }
            awakenScrollBars();
            postInvalidate();
            return;
        }
    }

    protected int computeScrollDeltaToGetChildRectOnScreen(Rect rect)
    {
        if (getChildCount() == 0)
        {
            return 0;
        }
        int l = getHeight();
        int i = getScrollY();
        int k = i + l;
        int i1 = getVerticalFadingEdgeLength();
        int j = i;
        if (rect.top > 0)
        {
            j = i + i1;
        }
        i = k;
        if (rect.bottom < getChildAt(0).getHeight())
        {
            i = k - i1;
        }
        if (rect.bottom > i && rect.top > j)
        {
            if (rect.height() > l)
            {
                j = (rect.top - j) + 0;
            } else
            {
                j = (rect.bottom - i) + 0;
            }
            i = Math.min(j, getChildAt(0).getBottom() - i);
        } else
        if (rect.top < j && rect.bottom < i)
        {
            if (rect.height() > l)
            {
                i = 0 - (i - rect.bottom);
            } else
            {
                i = 0 - (j - rect.top);
            }
            i = Math.max(i, -getScrollY());
        } else
        {
            i = 0;
        }
        return i;
    }

    protected int computeVerticalScrollOffset()
    {
        return Math.max(0, super.computeVerticalScrollOffset());
    }

    protected int computeVerticalScrollRange()
    {
        int i = getChildCount();
        int j = getHeight();
        int k = getPaddingBottom();
        int l = getPaddingTop();
        if (i == 0)
        {
            return j - k - l;
        } else
        {
            return getChildAt(0).getBottom();
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyevent)
    {
        return super.dispatchKeyEvent(keyevent) || executeKeyEvent(keyevent);
    }

    public boolean executeKeyEvent(KeyEvent keyevent)
    {
        char c = '!';
        boolean flag1 = false;
        mTempRect.setEmpty();
        boolean flag;
        if (!canScroll())
        {
            flag = flag1;
            if (isFocused())
            {
                flag = flag1;
                if (keyevent.getKeyCode() != 4)
                {
                    View view = findFocus();
                    keyevent = view;
                    if (view == this)
                    {
                        keyevent = null;
                    }
                    keyevent = FocusFinder.getInstance().findNextFocus(this, keyevent, 130);
                    if (keyevent != null && keyevent != this && keyevent.requestFocus(130))
                    {
                        flag = true;
                    } else
                    {
                        flag = false;
                    }
                }
            }
        } else
        {
            flag = flag1;
            if (keyevent.getAction() == 0)
            {
                switch (keyevent.getKeyCode())
                {
                default:
                    return false;

                case 19: // '\023'
                    if (!keyevent.isAltPressed())
                    {
                        return arrowScroll(33);
                    } else
                    {
                        return fullScroll(33);
                    }

                case 20: // '\024'
                    if (!keyevent.isAltPressed())
                    {
                        return arrowScroll(130);
                    } else
                    {
                        return fullScroll(130);
                    }

                case 62: // '>'
                    break;
                }
                if (!keyevent.isShiftPressed())
                {
                    c = '\202';
                }
                pageScroll(c);
                return false;
            }
        }
        return flag;
    }

    public void fling(int i)
    {
        if (getChildCount() > 0)
        {
            int j = getHeight();
            int k = getPaddingBottom();
            int l = getPaddingTop();
            int i1 = getChildAt(0).getHeight();
            mScroller.fling(getScrollX(), getScrollY(), 0, i, 0, 0, 0, Math.max(0, i1 - (j - k - l)));
            Object obj;
            View view;
            boolean flag;
            if (i > 0)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            view = findFocusableViewInMyBounds(flag, mScroller.getFinalY(), findFocus());
            obj = view;
            if (view == null)
            {
                obj = this;
            }
            if (obj != findFocus())
            {
                if (flag)
                {
                    i = 130;
                } else
                {
                    i = 33;
                }
                if (((View) (obj)).requestFocus(i))
                {
                    mScrollViewMovedFocus = true;
                    mScrollViewMovedFocus = false;
                }
            }
            invalidate();
        }
    }

    public boolean fullScroll(int i)
    {
        int j;
        int k;
        if (i == 130)
        {
            j = 1;
        } else
        {
            j = 0;
        }
        k = getHeight();
        mTempRect.top = 0;
        mTempRect.bottom = k;
        if (j != 0)
        {
            j = getChildCount();
            if (j > 0)
            {
                View view = getChildAt(j - 1);
                mTempRect.bottom = view.getBottom();
                mTempRect.top = mTempRect.bottom - k;
            }
        }
        return scrollAndFocus(i, mTempRect.top, mTempRect.bottom);
    }

    protected float getBottomFadingEdgeStrength()
    {
        if (getChildCount() == 0)
        {
            return 0.0F;
        }
        int i = getVerticalFadingEdgeLength();
        int j = getHeight();
        int k = getPaddingBottom();
        j = getChildAt(0).getBottom() - getScrollY() - (j - k);
        if (j < i)
        {
            return (float)j / (float)i;
        } else
        {
            return 1.0F;
        }
    }

    public int getMaxScrollAmount()
    {
        return (int)(0.5F * (float)(getBottom() - getTop()));
    }

    protected float getTopFadingEdgeStrength()
    {
        if (getChildCount() == 0)
        {
            return 0.0F;
        }
        int i = getVerticalFadingEdgeLength();
        if (getScrollY() < i)
        {
            return (float)getScrollY() / (float)i;
        } else
        {
            return 1.0F;
        }
    }

    public boolean inChild(int i, int j)
    {
        boolean flag1 = false;
        boolean flag = flag1;
        if (getChildCount() > 0)
        {
            int k = getScrollY();
            View view = getChildAt(0);
            flag = flag1;
            if (j >= view.getTop() - k)
            {
                flag = flag1;
                if (j < view.getBottom() - k)
                {
                    flag = flag1;
                    if (i >= view.getLeft())
                    {
                        flag = flag1;
                        if (i < view.getRight())
                        {
                            flag = true;
                        }
                    }
                }
            }
        }
        return flag;
    }

    public void initChildPointer()
    {
        child = getChildAt(0);
        child.setPadding(0, 1500, 0, 1500);
    }

    public boolean isFillViewport()
    {
        return mFillViewport;
    }

    public boolean isOverScrolled()
    {
        return getScrollY() < child.getPaddingTop() || getScrollY() > child.getBottom() - child.getPaddingBottom() - getHeight();
    }

    public boolean isSmoothScrollingEnabled()
    {
        return mSmoothScrollingEnabled;
    }

    protected void measureChild(View view, int i, int j)
    {
        android.view.ViewGroup.LayoutParams layoutparams = view.getLayoutParams();
        view.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight(), layoutparams.width), android.view.View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    protected void measureChildWithMargins(View view, int i, int j, int k, int l)
    {
        android.view.ViewGroup.MarginLayoutParams marginlayoutparams = (android.view.ViewGroup.MarginLayoutParams)view.getLayoutParams();
        i = getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight() + marginlayoutparams.leftMargin + marginlayoutparams.rightMargin + j, marginlayoutparams.width);
        j = marginlayoutparams.topMargin;
        view.measure(i, android.view.View.MeasureSpec.makeMeasureSpec(marginlayoutparams.bottomMargin + j, 0));
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent)
    {
        int i;
        boolean flag;
        flag = true;
        i = motionevent.getAction();
        if (i == 2 && mIsBeingDragged)
        {
            return true;
        }
        i & 0xff;
        JVM INSTR tableswitch 0 6: default 72
    //                   0 187
    //                   1 268
    //                   2 77
    //                   3 268
    //                   4 72
    //                   5 72
    //                   6 286;
           goto _L1 _L2 _L3 _L4 _L3 _L1 _L1 _L5
_L1:
        return mIsBeingDragged;
_L4:
        int j = mActivePointerId;
        if (j != -1)
        {
            j = motionevent.findPointerIndex(j);
            float f = motionevent.getY(j);
            float f2 = motionevent.getX(j);
            j = (int)Math.abs(f - mLastMotionY);
            int k = (int)Math.abs(f2 - mLastMotionX);
            if (j > mTouchSlop && Math.abs(j) > Math.abs(k) && !isXDragged)
            {
                mIsBeingDragged = true;
                mLastMotionY = f;
                mLastMotionX = f2;
                isXDragged = true;
            }
        }
        continue; /* Loop/switch isn't completed */
_L2:
        float f1 = motionevent.getY();
        float f3 = motionevent.getX();
        isXDragged = false;
        if (!inChild((int)motionevent.getX(), (int)f1))
        {
            mIsBeingDragged = false;
        } else
        {
            mLastMotionY = f1;
            mLastMotionX = f3;
            mActivePointerId = motionevent.getPointerId(0);
            if (mScroller.isFinished())
            {
                flag = false;
            }
            mIsBeingDragged = flag;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        mIsBeingDragged = false;
        isXDragged = false;
        mActivePointerId = -1;
        continue; /* Loop/switch isn't completed */
_L5:
        onSecondaryPointerUp(motionevent);
        if (true) goto _L1; else goto _L6
_L6:
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l)
    {
        super.onLayout(flag, i, j, k, l);
        mIsLayoutDirty = false;
        if (mChildToScrollTo != null && isViewDescendantOf(mChildToScrollTo, this))
        {
            scrollToChild(mChildToScrollTo);
        }
        mChildToScrollTo = null;
        scrollTo(getScrollX(), getScrollY());
    }

    protected void onMeasure(int i, int j)
    {
        super.onMeasure(i, j);
        break MISSING_BLOCK_LABEL_6;
        if (mFillViewport && android.view.View.MeasureSpec.getMode(j) != 0 && getChildCount() > 0)
        {
            View view = getChildAt(0);
            j = getMeasuredHeight();
            if (view.getMeasuredHeight() < j)
            {
                android.widget.FrameLayout.LayoutParams layoutparams = (android.widget.FrameLayout.LayoutParams)view.getLayoutParams();
                view.measure(getChildMeasureSpec(i, getPaddingLeft() + getPaddingRight(), layoutparams.width), android.view.View.MeasureSpec.makeMeasureSpec(j - getPaddingTop() - getPaddingBottom(), 0x40000000));
                return;
            }
        }
        return;
    }

    protected int onOverScroll(int i)
    {
        return 0;
    }

    protected boolean onRequestFocusInDescendants(int i, Rect rect)
    {
        View view;
        int j;
        if (i == 2)
        {
            j = 130;
        } else
        {
            j = i;
            if (i == 1)
            {
                j = 33;
            }
        }
        if (rect == null)
        {
            view = FocusFinder.getInstance().findNextFocus(this, null, j);
        } else
        {
            view = FocusFinder.getInstance().findNextFocusFromRect(this, rect, j);
        }
        if (view == null || isOffScreen(view))
        {
            return false;
        } else
        {
            return view.requestFocus(j, rect);
        }
    }

    protected void onScrollChanged(int i, int j, int k, int l)
    {
        int i1;
        int j1;
        int k1;
        i1 = getHeight();
        j1 = child.getPaddingTop();
        k1 = child.getHeight() - child.getPaddingBottom();
        if (!isInFlingMode || j >= j1 && j <= k1 - i1) goto _L2; else goto _L1
_L1:
        if (j >= j1) goto _L4; else goto _L3
_L3:
        mScroller.startScroll(0, j, 0, j1 - j, 1000);
_L5:
        post(overScrollerSpringbackTask);
        isInFlingMode = false;
        return;
_L4:
        if (j > k1 - i1)
        {
            mScroller.startScroll(0, j, 0, k1 - i1 - j, 1000);
        }
        if (true) goto _L5; else goto _L2
_L2:
        super.onScrollChanged(i, j, k, l);
        return;
    }

    protected void onSizeChanged(int i, int j, int k, int l)
    {
        super.onSizeChanged(i, j, k, l);
        View view;
        for (view = findFocus(); view == null || this == view || !isWithinDeltaOfScreen(view, 0, l);)
        {
            return;
        }

        view.getDrawingRect(mTempRect);
        offsetDescendantRectToMyCoords(view, mTempRect);
        doScrollY(computeScrollDeltaToGetChildRectOnScreen(mTempRect));
    }

    public boolean onTouch(View view, MotionEvent motionevent)
    {
        mScroller.forceFinished(true);
        removeCallbacks(overScrollerSpringbackTask);
        if (motionevent.getAction() == 1)
        {
            return overScrollView();
        }
        if (motionevent.getAction() == 3)
        {
            return overScrollView();
        } else
        {
            return false;
        }
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        if (motionevent.getAction() != 0 || motionevent.getEdgeFlags() == 0) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        if (mVelocityTracker == null)
        {
            mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(motionevent);
        motionevent.getAction() & 0xff;
        JVM INSTR tableswitch 0 6: default 88
    //                   0 90
    //                   1 213
    //                   2 153
    //                   3 303
    //                   4 88
    //                   5 88
    //                   6 349;
           goto _L3 _L4 _L5 _L6 _L7 _L3 _L3 _L8
_L3:
        break; /* Loop/switch isn't completed */
_L8:
        break MISSING_BLOCK_LABEL_349;
_L10:
        return true;
_L4:
        float f;
        boolean flag;
        f = motionevent.getY();
        flag = inChild((int)motionevent.getX(), (int)f);
        mIsBeingDragged = flag;
        if (!flag) goto _L1; else goto _L9
_L9:
        if (!mScroller.isFinished())
        {
            mScroller.abortAnimation();
        }
        mLastMotionY = f;
        mActivePointerId = motionevent.getPointerId(0);
          goto _L10
_L6:
        if (mIsBeingDragged)
        {
            float f1 = motionevent.getY(motionevent.findPointerIndex(mActivePointerId));
            int i = (int)(mLastMotionY - f1);
            mLastMotionY = f1;
            if (isOverScrolled())
            {
                scrollBy(0, i / 2);
            } else
            {
                scrollBy(0, i);
            }
        }
          goto _L10
_L5:
        if (mIsBeingDragged)
        {
            motionevent = mVelocityTracker;
            motionevent.computeCurrentVelocity(1000, mMaximumVelocity);
            int j = (int)motionevent.getYVelocity(mActivePointerId);
            if (getChildCount() > 0 && Math.abs(j) > mMinimumVelocity)
            {
                fling(-j);
            }
            mActivePointerId = -1;
            mIsBeingDragged = false;
            if (mVelocityTracker != null)
            {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
            }
        }
          goto _L10
_L7:
        if (mIsBeingDragged && getChildCount() > 0)
        {
            mActivePointerId = -1;
            mIsBeingDragged = false;
            if (mVelocityTracker != null)
            {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
            }
        }
          goto _L10
        onSecondaryPointerUp(motionevent);
          goto _L10
    }

    public boolean pageScroll(int i)
    {
        int k;
        View view;
        int j;
        if (i == 130)
        {
            j = 1;
        } else
        {
            j = 0;
        }
        k = getHeight();
        if (j == 0) goto _L2; else goto _L1
_L1:
        mTempRect.top = getScrollY() + k;
        j = getChildCount();
        if (j > 0)
        {
            view = getChildAt(j - 1);
            if (mTempRect.top + k > view.getBottom())
            {
                mTempRect.top = view.getBottom() - k;
            }
        }
_L4:
        mTempRect.bottom = mTempRect.top + k;
        return scrollAndFocus(i, mTempRect.top, mTempRect.bottom);
_L2:
        mTempRect.top = getScrollY() - k;
        if (mTempRect.top < 0)
        {
            mTempRect.top = 0;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void requestChildFocus(View view, View view1)
    {
        if (!mScrollViewMovedFocus)
        {
            if (!mIsLayoutDirty)
            {
                scrollToChild(view1);
            } else
            {
                mChildToScrollTo = view1;
            }
        }
        super.requestChildFocus(view, view1);
    }

    public boolean requestChildRectangleOnScreen(View view, Rect rect, boolean flag)
    {
        rect.offset(view.getLeft() - view.getScrollX(), view.getTop() - view.getScrollY());
        return scrollToChildRect(rect, flag);
    }

    public void requestLayout()
    {
        mIsLayoutDirty = true;
        super.requestLayout();
    }

    public void scrollTo(int i, int j)
    {
        if (getChildCount() > 0)
        {
            View view = getChildAt(0);
            i = clamp(i, getWidth() - getPaddingRight() - getPaddingLeft(), view.getWidth());
            j = clamp(j, getHeight() - getPaddingBottom() - getPaddingTop(), view.getHeight());
            if (i != getScrollX() || j != getScrollY())
            {
                super.scrollTo(i, j);
            }
        }
    }

    public void setFillViewport(boolean flag)
    {
        if (flag != mFillViewport)
        {
            mFillViewport = flag;
            requestLayout();
        }
    }

    public void setSmoothScrollingEnabled(boolean flag)
    {
        mSmoothScrollingEnabled = flag;
    }

    public final void smoothScrollBy(int i, int j)
    {
        if (getChildCount() == 0)
        {
            return;
        }
        if (AnimationUtils.currentAnimationTimeMillis() - mLastScroll > 250L)
        {
            i = getHeight();
            int k = getPaddingBottom();
            int l = getPaddingTop();
            k = Math.max(0, getChildAt(0).getHeight() - (i - k - l));
            i = getScrollY();
            j = Math.max(0, Math.min(i + j, k));
            mScroller.startScroll(getScrollX(), i, 0, j - i);
            invalidate();
        } else
        {
            if (!mScroller.isFinished())
            {
                mScroller.abortAnimation();
            }
            scrollBy(i, j);
        }
        mLastScroll = AnimationUtils.currentAnimationTimeMillis();
    }

    public final void smoothScrollTo(int i, int j)
    {
        smoothScrollBy(i - getScrollX(), j - getScrollY());
    }

    public final void smoothScrollToBottom()
    {
        smoothScrollTo(0, child.getHeight() - child.getPaddingTop() - getHeight());
    }

    public final void smoothScrollToTop()
    {
        smoothScrollTo(0, child.getPaddingTop());
    }

}
