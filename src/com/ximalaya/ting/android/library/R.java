// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library;


public final class R
{
    public static final class attr
    {

        public static final int backgroundColor = 0x7f010014;
        public static final int backgroundSoundColor = 0x7f010012;
        public static final int barPadding = 0x7f010010;
        public static final int barWidth = 0x7f01000f;
        public static final int click_remove_id = 0x7f010035;
        public static final int collapsed_height = 0x7f010025;
        public static final int cutterDrawable = 0x7f010015;
        public static final int debugDraw = 0x7f01001f;
        public static final int drag_enabled = 0x7f01002f;
        public static final int drag_handle_id = 0x7f010033;
        public static final int drag_scroll_start = 0x7f010026;
        public static final int drag_start_mode = 0x7f010032;
        public static final int drop_animation_duration = 0x7f01002e;
        public static final int fling_handle_id = 0x7f010034;
        public static final int float_alpha = 0x7f01002b;
        public static final int float_background_color = 0x7f010028;
        public static final int layoutDirection = 0x7f01001e;
        public static final int layout_newLine = 0x7f010021;
        public static final int layout_weight = 0x7f010022;
        public static final int max = 0x7f01001b;
        public static final int max_drag_scroll_speed = 0x7f010027;
        public static final int playedColor = 0x7f010013;
        public static final int progressStyle = 0x7f01001d;
        public static final int pstsActivateTextColor = 0x7f01000c;
        public static final int pstsDeactivateTextColor = 0x7f01000d;
        public static final int pstsDividerColor = 0x7f010002;
        public static final int pstsDividerPadding = 0x7f010005;
        public static final int pstsIndicatorColor = 0x7f010000;
        public static final int pstsIndicatorHeight = 0x7f010003;
        public static final int pstsScrollOffset = 0x7f010007;
        public static final int pstsShouldExpand = 0x7f010009;
        public static final int pstsTabBackground = 0x7f010008;
        public static final int pstsTabPaddingLeftRight = 0x7f010006;
        public static final int pstsTabSwitch = 0x7f01000b;
        public static final int pstsTextAllCaps = 0x7f01000a;
        public static final int pstsUnderlineColor = 0x7f010001;
        public static final int pstsUnderlineHeight = 0x7f010004;
        public static final int recordingColor = 0x7f010011;
        public static final int remove_animation_duration = 0x7f01002d;
        public static final int remove_enabled = 0x7f010031;
        public static final int remove_mode = 0x7f010029;
        public static final int roundColor = 0x7f010016;
        public static final int roundProgressColor = 0x7f010017;
        public static final int roundWidth = 0x7f010018;
        public static final int seekBarRotation = 0x7f01000e;
        public static final int slide_shuffle_speed = 0x7f01002c;
        public static final int sort_enabled = 0x7f010030;
        public static final int stuckShadowDrawable = 0x7f010024;
        public static final int stuckShadowHeight = 0x7f010023;
        public static final int textColor = 0x7f010019;
        public static final int textIsDisplayable = 0x7f01001c;
        public static final int textSize = 0x7f01001a;
        public static final int track_drag_sort = 0x7f01002a;
        public static final int use_default_controller = 0x7f010036;
        public static final int weightDefault = 0x7f010020;

        public attr()
        {
        }
    }

    public static final class color
    {

        public static final int activate_text_color = 0x7f070001;
        public static final int background_tab_pressed = 0x7f070000;
        public static final int border = 0x7f070008;
        public static final int deactivate_text_color = 0x7f070002;
        public static final int gray_btn_pressed = 0x7f070007;
        public static final int menu_item = 0x7f07000a;
        public static final int orange = 0x7f070004;
        public static final int orange_new = 0x7f070005;
        public static final int text_color = 0x7f070009;
        public static final int transparent = 0x7f070003;
        public static final int white = 0x7f070006;

        public color()
        {
        }
    }

    public static final class dimen
    {

        public static final int activity_horizontal_margin = 0x7f080000;
        public static final int activity_vertical_margin = 0x7f080001;

        public dimen()
        {
        }
    }

    public static final class drawable
    {

        public static final int background_tab = 0x7f020051;
        public static final int bottom = 0x7f0200e4;
        public static final int btn_bg = 0x7f0200ea;
        public static final int btn_bg_pressed = 0x7f0200eb;
        public static final int btn_bg_selector = 0x7f0200ec;
        public static final int btn_pressed = 0x7f0200f7;
        public static final int btn_unpressed = 0x7f020105;
        public static final int frame = 0x7f020272;
        public static final int ic_launcher = 0x7f0202af;
        public static final int mask = 0x7f020334;
        public static final int menu_item_selector = 0x7f020344;
        public static final int new_img = 0x7f020378;
        public static final int round_bg_white = 0x7f0204e6;
        public static final int round_bottom_btn_bg = 0x7f0204e7;
        public static final int round_bottom_btn_bg_pressed = 0x7f0204e8;
        public static final int round_bottom_btn_bg_selector = 0x7f0204e9;
        public static final int round_bottom_btn_left_bg = 0x7f0204ea;
        public static final int round_bottom_btn_left_bg_pressed = 0x7f0204eb;
        public static final int round_bottom_btn_left_bg_pressed_v12 = 0x7f0204ec;
        public static final int round_bottom_btn_left_bg_selector = 0x7f0204ed;
        public static final int round_bottom_btn_left_bg_selector_v12 = 0x7f0204ee;
        public static final int round_bottom_btn_left_bg_v12 = 0x7f0204ef;
        public static final int round_bottom_btn_right_bg = 0x7f0204f0;
        public static final int round_bottom_btn_right_bg_pressed = 0x7f0204f1;
        public static final int round_bottom_btn_right_bg_pressed_v12 = 0x7f0204f2;
        public static final int round_bottom_btn_right_bg_selector = 0x7f0204f3;
        public static final int round_bottom_btn_right_bg_selector_v12 = 0x7f0204f4;
        public static final int round_bottom_btn_right_bg_v12 = 0x7f0204f5;
        public static final int ting = 0x7f020598;
        public static final int wifi_device_selected = 0x7f0205ce;
        public static final int yellow_bt = 0x7f0205d9;

        public drawable()
        {
        }
    }

    public static final class id
    {

        public static final int CW270 = 0x7f0a0001;
        public static final int CW90 = 0x7f0a0000;
        public static final int FILL = 0x7f0a0003;
        public static final int STROKE = 0x7f0a0002;
        public static final int btn_border = 0x7f0a015a;
        public static final int btn_layout = 0x7f0a00b0;
        public static final int btn_separator_border_1 = 0x7f0a015c;
        public static final int btn_separator_border_2 = 0x7f0a015e;
        public static final int cancel_btn = 0x7f0a015b;
        public static final int clickRemove = 0x7f0a0006;
        public static final int context_ll = 0x7f0a01ff;
        public static final int extra_btn_tv = 0x7f0a0161;
        public static final int extra_layout = 0x7f0a0160;
        public static final int flingRemove = 0x7f0a0007;
        public static final int group_item = 0x7f0a043c;
        public static final int listview = 0x7f0a005c;
        public static final int ltr = 0x7f0a0004;
        public static final int msg_tv = 0x7f0a0159;
        public static final int neutral_btn = 0x7f0a015d;
        public static final int new_feature = 0x7f0a018c;
        public static final int notificationImage = 0x7f0a05d0;
        public static final int notificationPercent = 0x7f0a05d1;
        public static final int notificationProgress = 0x7f0a05d3;
        public static final int notificationTitle = 0x7f0a05d2;
        public static final int ok_btn = 0x7f0a015f;
        public static final int onDown = 0x7f0a0008;
        public static final int onLongPress = 0x7f0a000a;
        public static final int onMove = 0x7f0a0009;
        public static final int percentage_layout = 0x7f0a05cf;
        public static final int progress_bar = 0x7f0a0093;
        public static final int rl_dialog_content = 0x7f0a0157;
        public static final int rtl = 0x7f0a0005;
        public static final int text_layout = 0x7f0a043b;
        public static final int title_border = 0x7f0a00d1;
        public static final int title_tv = 0x7f0a0158;

        public id()
        {
        }
    }

    public static final class layout
    {

        public static final int activity_main = 0x7f030037;
        public static final int alert_dialog = 0x7f03003e;
        public static final int alert_dialog_with_button = 0x7f03003f;
        public static final int menu_dialog = 0x7f030166;
        public static final int menu_dialog_item = 0x7f030167;
        public static final int notification_item = 0x7f03017f;
        public static final int progress_dialog = 0x7f030199;

        public layout()
        {
        }
    }

    public static final class string
    {

        public static final int action_settings = 0x7f090002;
        public static final int app_name = 0x7f090000;
        public static final int hello_world = 0x7f090001;

        public string()
        {
        }
    }

    public static final class style
    {

        public static final int AppBaseTheme = 0x7f0b0000;
        public static final int AppTheme = 0x7f0b0001;
        public static final int menuDialog = 0x7f0b0002;
        public static final int round_bottom_btn_left_bg = 0x7f0b0003;
        public static final int round_bottom_btn_right_bg = 0x7f0b0004;

        public style()
        {
        }
    }

    public static final class styleable
    {

        public static final int CircleProgressBar[] = {
            0x7f010016, 0x7f010017, 0x7f010018, 0x7f010019, 0x7f01001a, 0x7f01001b, 0x7f01001c, 0x7f01001d, 0x7f01005b, 0x7f01005c, 
            0x7f01005d, 0x7f01005e, 0x7f01005f, 0x7f010060, 0x7f010061, 0x7f010062, 0x7f010063
        };
        public static final int CircleProgressBar_max = 5;
        public static final int CircleProgressBar_progressStyle = 7;
        public static final int CircleProgressBar_roundColor = 0;
        public static final int CircleProgressBar_roundProgressColor = 1;
        public static final int CircleProgressBar_roundWidth = 2;
        public static final int CircleProgressBar_textColor = 3;
        public static final int CircleProgressBar_textIsDisplayable = 6;
        public static final int CircleProgressBar_textSize = 4;
        public static final int DragSortListView[] = {
            0x7f010025, 0x7f010026, 0x7f010027, 0x7f010028, 0x7f010029, 0x7f01002a, 0x7f01002b, 0x7f01002c, 0x7f01002d, 0x7f01002e, 
            0x7f01002f, 0x7f010030, 0x7f010031, 0x7f010032, 0x7f010033, 0x7f010034, 0x7f010035, 0x7f010036
        };
        public static final int DragSortListView_click_remove_id = 16;
        public static final int DragSortListView_collapsed_height = 0;
        public static final int DragSortListView_drag_enabled = 10;
        public static final int DragSortListView_drag_handle_id = 14;
        public static final int DragSortListView_drag_scroll_start = 1;
        public static final int DragSortListView_drag_start_mode = 13;
        public static final int DragSortListView_drop_animation_duration = 9;
        public static final int DragSortListView_fling_handle_id = 15;
        public static final int DragSortListView_float_alpha = 6;
        public static final int DragSortListView_float_background_color = 3;
        public static final int DragSortListView_max_drag_scroll_speed = 2;
        public static final int DragSortListView_remove_animation_duration = 8;
        public static final int DragSortListView_remove_enabled = 12;
        public static final int DragSortListView_remove_mode = 4;
        public static final int DragSortListView_slide_shuffle_speed = 7;
        public static final int DragSortListView_sort_enabled = 11;
        public static final int DragSortListView_track_drag_sort = 5;
        public static final int DragSortListView_use_default_controller = 17;
        public static final int FlowLayout[] = {
            0x10100af, 0x10100c4, 0x7f01001e, 0x7f01001f, 0x7f010020
        };
        public static final int FlowLayout_LayoutParams[] = {
            0x10100b3, 0x7f010021, 0x7f010022
        };
        public static final int FlowLayout_LayoutParams_android_layout_gravity = 0;
        public static final int FlowLayout_LayoutParams_layout_newLine = 1;
        public static final int FlowLayout_LayoutParams_layout_weight = 2;
        public static final int FlowLayout_android_gravity = 0;
        public static final int FlowLayout_android_orientation = 1;
        public static final int FlowLayout_debugDraw = 3;
        public static final int FlowLayout_layoutDirection = 2;
        public static final int FlowLayout_weightDefault = 4;
        public static final int PagerSlidingTabStrip[] = {
            0x7f010000, 0x7f010001, 0x7f010002, 0x7f010003, 0x7f010004, 0x7f010005, 0x7f010006, 0x7f010007, 0x7f010008, 0x7f010009, 
            0x7f01000a, 0x7f01000b, 0x7f01000c, 0x7f01000d
        };
        public static final int PagerSlidingTabStrip_pstsActivateTextColor = 12;
        public static final int PagerSlidingTabStrip_pstsDeactivateTextColor = 13;
        public static final int PagerSlidingTabStrip_pstsDividerColor = 2;
        public static final int PagerSlidingTabStrip_pstsDividerPadding = 5;
        public static final int PagerSlidingTabStrip_pstsIndicatorColor = 0;
        public static final int PagerSlidingTabStrip_pstsIndicatorHeight = 3;
        public static final int PagerSlidingTabStrip_pstsScrollOffset = 7;
        public static final int PagerSlidingTabStrip_pstsShouldExpand = 9;
        public static final int PagerSlidingTabStrip_pstsTabBackground = 8;
        public static final int PagerSlidingTabStrip_pstsTabPaddingLeftRight = 6;
        public static final int PagerSlidingTabStrip_pstsTabSwitch = 11;
        public static final int PagerSlidingTabStrip_pstsTextAllCaps = 10;
        public static final int PagerSlidingTabStrip_pstsUnderlineColor = 1;
        public static final int PagerSlidingTabStrip_pstsUnderlineHeight = 4;
        public static final int StickyScrollView[] = {
            0x7f010023, 0x7f010024
        };
        public static final int StickyScrollView_stuckShadowDrawable = 1;
        public static final int StickyScrollView_stuckShadowHeight = 0;
        public static final int VerticalSeekBar[] = {
            0x7f01000e
        };
        public static final int VerticalSeekBar_seekBarRotation = 0;
        public static final int WaveformView[] = {
            0x7f01000f, 0x7f010010, 0x7f010011, 0x7f010012, 0x7f010013, 0x7f010014, 0x7f010015
        };
        public static final int WaveformView_backgroundColor = 5;
        public static final int WaveformView_backgroundSoundColor = 3;
        public static final int WaveformView_barPadding = 1;
        public static final int WaveformView_barWidth = 0;
        public static final int WaveformView_cutterDrawable = 6;
        public static final int WaveformView_playedColor = 4;
        public static final int WaveformView_recordingColor = 2;


        public styleable()
        {
        }
    }


    public R()
    {
    }
}
