// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.model;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

public abstract class BaseAdModel
{

    public static final int LINK_TYPE_FILE = 2;
    public static final int LINK_TYPE_NONE = 0;
    public static final int LINK_TYPE_OPEN_THIRD_BROWSER = 3;
    public static final int LINK_TYPE_WEB = 1;
    public String cover;
    public String link;

    public BaseAdModel()
    {
    }

    public abstract String getICover();

    public abstract String getILink();

    public abstract int getILinkType();

    public void openInThirdBrowser(Context context, String s)
    {
        try
        {
            if (!TextUtils.isEmpty(getILink()))
            {
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(s)));
            }
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
    }
}
