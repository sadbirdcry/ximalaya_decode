// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.model;


// Referenced classes of package com.ximalaya.ting.android.library.model:
//            BaseAdModel

public class AppAd extends BaseAdModel
{

    private long adid;
    private boolean auto;
    private int clickType;
    private String description;
    private int displayType;
    private long endAt;
    private String intro;
    private boolean isAutoNotifyInstall;
    private int linkType;
    private String name;
    private String packageName;
    private int position;
    private long startAt;
    private String thirdStatUrl;

    public AppAd()
    {
        isAutoNotifyInstall = true;
    }

    public long getAdid()
    {
        return adid;
    }

    public int getClickType()
    {
        return clickType;
    }

    public String getCover()
    {
        return cover;
    }

    public String getDescription()
    {
        return description;
    }

    public int getDisplayType()
    {
        return displayType;
    }

    public long getEndAt()
    {
        return endAt;
    }

    public String getICover()
    {
        return cover;
    }

    public String getILink()
    {
        return link;
    }

    public int getILinkType()
    {
        return linkType;
    }

    public String getIntro()
    {
        return intro;
    }

    public boolean getIsAuto()
    {
        return auto;
    }

    public boolean getIsAutoNotifyInstall()
    {
        return isAutoNotifyInstall;
    }

    public String getLink()
    {
        return link;
    }

    public int getLinkType()
    {
        return linkType;
    }

    public String getName()
    {
        return name;
    }

    public String getPackageName()
    {
        return packageName;
    }

    public int getPosition()
    {
        return position;
    }

    public long getStartAt()
    {
        return startAt;
    }

    public String getThirdStatUrl()
    {
        return thirdStatUrl;
    }

    public void setAdid(long l)
    {
        adid = l;
    }

    public void setClickType(int i)
    {
        clickType = i;
    }

    public void setCover(String s)
    {
        cover = s;
    }

    public void setDescription(String s)
    {
        description = s;
    }

    public void setDisplayType(int i)
    {
        displayType = i;
    }

    public void setEndAt(long l)
    {
        endAt = l;
    }

    public void setIntro(String s)
    {
        intro = s;
    }

    public void setIsAuto(boolean flag)
    {
        auto = flag;
    }

    public void setIsAutoNotifyInstall(boolean flag)
    {
        isAutoNotifyInstall = flag;
    }

    public void setLink(String s)
    {
        link = s;
    }

    public void setLinkType(int i)
    {
        linkType = i;
    }

    public void setName(String s)
    {
        name = s;
    }

    public void setPackageName(String s)
    {
        packageName = s;
    }

    public void setPosition(int i)
    {
        position = i;
    }

    public void setStartAt(long l)
    {
        startAt = l;
    }

    public void setThirdStatUrl(String s)
    {
        thirdStatUrl = s;
    }
}
