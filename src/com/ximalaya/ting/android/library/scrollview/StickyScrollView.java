// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.scrollview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ScrollView;
import java.util.ArrayList;
import java.util.Iterator;

// Referenced classes of package com.ximalaya.ting.android.library.scrollview:
//            a

public class StickyScrollView extends ScrollView
{

    private static final int DEFAULT_SHADOW_HEIGHT = 10;
    public static final String FLAG_HASTRANSPARANCY = "-hastransparancy";
    public static final String FLAG_NONCONSTANT = "-nonconstant";
    public static final String STICKY_TAG = "sticky";
    private boolean clipToPaddingHasBeenSet;
    private boolean clippingToPadding;
    private View currentlyStickingView;
    private boolean hasNotDoneActionDown;
    private final Runnable invalidateRunnable;
    private Drawable mShadowDrawable;
    private int mShadowHeight;
    private boolean redirectTouchesToStickyView;
    private int stickyViewLeftOffset;
    private float stickyViewTopOffset;
    private ArrayList stickyViews;

    public StickyScrollView(Context context)
    {
        this(context, null);
    }

    public StickyScrollView(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0x1010080);
    }

    public StickyScrollView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        invalidateRunnable = new a(this);
        hasNotDoneActionDown = true;
        setup();
        attributeset = context.obtainStyledAttributes(attributeset, com.ximalaya.ting.android.library.R.styleable.StickyScrollView, i, 0);
        mShadowHeight = attributeset.getDimensionPixelSize(0, (int)(context.getResources().getDisplayMetrics().density * 10F + 0.5F));
        i = attributeset.getResourceId(1, -1);
        if (i != -1)
        {
            mShadowDrawable = context.getResources().getDrawable(i);
        }
        attributeset.recycle();
    }

    private void doTheStickyThing()
    {
        View view;
        View view1;
        Iterator iterator;
        view = null;
        iterator = stickyViews.iterator();
        view1 = null;
_L6:
        if (iterator.hasNext())
        {
            View view2 = (View)iterator.next();
            int l = getTopForViewRelativeOnlyChild(view2);
            int j1 = getScrollY();
            int i;
            if (clippingToPadding)
            {
                i = 0;
            } else
            {
                i = getPaddingTop();
            }
            l = (l - j1) + i;
            if (l <= 0)
            {
                if (view1 != null)
                {
                    int k1 = getTopForViewRelativeOnlyChild(view1);
                    int j2 = getScrollY();
                    if (clippingToPadding)
                    {
                        i = 0;
                    } else
                    {
                        i = getPaddingTop();
                    }
                    if (l <= i + (k1 - j2))
                    {
                        continue; /* Loop/switch isn't completed */
                    }
                }
                view1 = view2;
                continue; /* Loop/switch isn't completed */
            }
            if (view != null)
            {
                int l1 = getTopForViewRelativeOnlyChild(view);
                int k2 = getScrollY();
                int j;
                if (clippingToPadding)
                {
                    j = 0;
                } else
                {
                    j = getPaddingTop();
                }
                if (l >= j + (l1 - k2))
                {
                    continue; /* Loop/switch isn't completed */
                }
            }
            view = view2;
            continue; /* Loop/switch isn't completed */
        }
        if (view1 == null) goto _L2; else goto _L1
_L1:
        float f;
        if (view == null)
        {
            f = 0.0F;
        } else
        {
            int i1 = getTopForViewRelativeOnlyChild(view);
            int i2 = getScrollY();
            int k;
            if (clippingToPadding)
            {
                k = 0;
            } else
            {
                k = getPaddingTop();
            }
            f = Math.min(0, (k + (i1 - i2)) - view1.getHeight());
        }
        stickyViewTopOffset = f;
        if (view1 != currentlyStickingView)
        {
            if (currentlyStickingView != null)
            {
                stopStickingCurrentlyStickingView();
            }
            stickyViewLeftOffset = getLeftForViewRelativeOnlyChild(view1);
            startStickingView(view1);
        }
_L4:
        return;
_L2:
        if (currentlyStickingView == null) goto _L4; else goto _L3
_L3:
        stopStickingCurrentlyStickingView();
        return;
        if (true) goto _L6; else goto _L5
_L5:
    }

    private void findStickyViews(View view)
    {
        if (view instanceof ViewGroup)
        {
            view = (ViewGroup)view;
            int i = 0;
            while (i < view.getChildCount()) 
            {
                String s = getStringTagForView(view.getChildAt(i));
                if (s != null && s.contains("sticky"))
                {
                    stickyViews.add(view.getChildAt(i));
                } else
                if (view.getChildAt(i) instanceof ViewGroup)
                {
                    findStickyViews(view.getChildAt(i));
                }
                i++;
            }
        } else
        {
            String s1 = (String)view.getTag();
            if (s1 != null && s1.contains("sticky"))
            {
                stickyViews.add(view);
            }
        }
    }

    private int getBottomForViewRelativeOnlyChild(View view)
    {
        int i;
        for (i = view.getBottom(); view.getParent() != getChildAt(0); i += view.getBottom())
        {
            view = (View)view.getParent();
        }

        return i;
    }

    private int getLeftForViewRelativeOnlyChild(View view)
    {
        int i;
        for (i = view.getLeft(); view.getParent() != getChildAt(0); i += view.getLeft())
        {
            view = (View)view.getParent();
        }

        return i;
    }

    private int getRightForViewRelativeOnlyChild(View view)
    {
        int i;
        for (i = view.getRight(); view.getParent() != getChildAt(0); i += view.getRight())
        {
            view = (View)view.getParent();
        }

        return i;
    }

    private String getStringTagForView(View view)
    {
        return String.valueOf(view.getTag());
    }

    private int getTopForViewRelativeOnlyChild(View view)
    {
        int i;
        for (i = view.getTop(); view.getParent() != getChildAt(0); i += view.getTop())
        {
            view = (View)view.getParent();
        }

        return i;
    }

    private void hideView(View view)
    {
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            view.setAlpha(0.0F);
            return;
        } else
        {
            AlphaAnimation alphaanimation = new AlphaAnimation(1.0F, 0.0F);
            alphaanimation.setDuration(0L);
            alphaanimation.setFillAfter(true);
            view.startAnimation(alphaanimation);
            return;
        }
    }

    private void notifyHierarchyChanged()
    {
        if (currentlyStickingView != null)
        {
            stopStickingCurrentlyStickingView();
        }
        stickyViews.clear();
        findStickyViews(getChildAt(0));
        doTheStickyThing();
        invalidate();
    }

    private void showView(View view)
    {
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            view.setAlpha(1.0F);
            return;
        } else
        {
            AlphaAnimation alphaanimation = new AlphaAnimation(0.0F, 1.0F);
            alphaanimation.setDuration(0L);
            alphaanimation.setFillAfter(true);
            view.startAnimation(alphaanimation);
            return;
        }
    }

    private void startStickingView(View view)
    {
        currentlyStickingView = view;
        if (getStringTagForView(currentlyStickingView).contains("-hastransparancy"))
        {
            hideView(currentlyStickingView);
        }
        if (((String)currentlyStickingView.getTag()).contains("-nonconstant"))
        {
            post(invalidateRunnable);
        }
    }

    private void stopStickingCurrentlyStickingView()
    {
        if (getStringTagForView(currentlyStickingView).contains("-hastransparancy"))
        {
            showView(currentlyStickingView);
        }
        currentlyStickingView = null;
        removeCallbacks(invalidateRunnable);
    }

    public void addView(View view)
    {
        super.addView(view);
        findStickyViews(view);
    }

    public void addView(View view, int i)
    {
        super.addView(view, i);
        findStickyViews(view);
    }

    public void addView(View view, int i, int j)
    {
        super.addView(view, i, j);
        findStickyViews(view);
    }

    public void addView(View view, int i, android.view.ViewGroup.LayoutParams layoutparams)
    {
        super.addView(view, i, layoutparams);
        findStickyViews(view);
    }

    public void addView(View view, android.view.ViewGroup.LayoutParams layoutparams)
    {
        super.addView(view, layoutparams);
        findStickyViews(view);
    }

    protected void dispatchDraw(Canvas canvas)
    {
        super.dispatchDraw(canvas);
        if (currentlyStickingView != null)
        {
            canvas.save();
            float f = getPaddingLeft() + stickyViewLeftOffset;
            float f1 = getScrollY();
            float f2 = stickyViewTopOffset;
            int i;
            if (clippingToPadding)
            {
                i = getPaddingTop();
            } else
            {
                i = 0;
            }
            canvas.translate(f, (float)i + (f2 + f1));
            if (clippingToPadding)
            {
                f = -stickyViewTopOffset;
            } else
            {
                f = 0.0F;
            }
            canvas.clipRect(0.0F, f, getWidth() - stickyViewLeftOffset, currentlyStickingView.getHeight() + mShadowHeight + 1);
            if (mShadowDrawable != null)
            {
                i = currentlyStickingView.getWidth();
                int j = currentlyStickingView.getHeight();
                int k = currentlyStickingView.getHeight();
                int l = mShadowHeight;
                mShadowDrawable.setBounds(0, j, i, k + l);
                mShadowDrawable.draw(canvas);
            }
            if (clippingToPadding)
            {
                f = -stickyViewTopOffset;
            } else
            {
                f = 0.0F;
            }
            canvas.clipRect(0.0F, f, getWidth(), currentlyStickingView.getHeight());
            if (getStringTagForView(currentlyStickingView).contains("-hastransparancy"))
            {
                showView(currentlyStickingView);
                currentlyStickingView.draw(canvas);
                hideView(currentlyStickingView);
            } else
            {
                currentlyStickingView.draw(canvas);
            }
            canvas.restore();
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionevent)
    {
        boolean flag1;
        flag1 = true;
        if (motionevent.getAction() == 0)
        {
            redirectTouchesToStickyView = true;
        }
        if (!redirectTouchesToStickyView) goto _L2; else goto _L1
_L1:
        boolean flag;
        if (currentlyStickingView != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        redirectTouchesToStickyView = flag;
        if (redirectTouchesToStickyView)
        {
            if (motionevent.getY() <= (float)currentlyStickingView.getHeight() + stickyViewTopOffset && motionevent.getX() >= (float)getLeftForViewRelativeOnlyChild(currentlyStickingView) && motionevent.getX() <= (float)getRightForViewRelativeOnlyChild(currentlyStickingView))
            {
                flag = flag1;
            } else
            {
                flag = false;
            }
            redirectTouchesToStickyView = flag;
        }
_L4:
        if (redirectTouchesToStickyView)
        {
            motionevent.offsetLocation(0.0F, -1F * (((float)getScrollY() + stickyViewTopOffset) - (float)getTopForViewRelativeOnlyChild(currentlyStickingView)));
        }
        return super.dispatchTouchEvent(motionevent);
_L2:
        if (currentlyStickingView == null)
        {
            redirectTouchesToStickyView = false;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void notifyStickyAttributeChanged()
    {
        notifyHierarchyChanged();
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l)
    {
        super.onLayout(flag, i, j, k, l);
        if (!clipToPaddingHasBeenSet)
        {
            clippingToPadding = true;
        }
        notifyHierarchyChanged();
    }

    protected void onScrollChanged(int i, int j, int k, int l)
    {
        super.onScrollChanged(i, j, k, l);
        doTheStickyThing();
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        if (redirectTouchesToStickyView)
        {
            motionevent.offsetLocation(0.0F, ((float)getScrollY() + stickyViewTopOffset) - (float)getTopForViewRelativeOnlyChild(currentlyStickingView));
        }
        if (motionevent.getAction() == 0)
        {
            hasNotDoneActionDown = false;
        }
        if (hasNotDoneActionDown)
        {
            MotionEvent motionevent1 = MotionEvent.obtain(motionevent);
            motionevent1.setAction(0);
            super.onTouchEvent(motionevent1);
            hasNotDoneActionDown = false;
        }
        if (motionevent.getAction() == 1 || motionevent.getAction() == 3)
        {
            hasNotDoneActionDown = true;
        }
        return super.onTouchEvent(motionevent);
    }

    public void setClipToPadding(boolean flag)
    {
        super.setClipToPadding(flag);
        clippingToPadding = flag;
        clipToPaddingHasBeenSet = true;
    }

    public void setShadowHeight(int i)
    {
        mShadowHeight = i;
    }

    public void setup()
    {
        stickyViews = new ArrayList();
    }





}
