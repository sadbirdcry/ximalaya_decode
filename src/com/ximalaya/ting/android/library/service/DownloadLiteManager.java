// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.service;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.ximalaya.ting.android.library.util.FreeFlowUtilBase;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.ToolUtilLib;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.HashMap;

// Referenced classes of package com.ximalaya.ting.android.library.service:
//            a

public class DownloadLiteManager
{
    public static interface DownloadCallback
    {

        public abstract void onError(String s);

        public abstract void onProgressUpdate(String s, int i);

        public abstract void onSuccess(String s);
    }

    private class a
    {

        final DownloadLiteManager a;
        private String b;
        private String c;
        private String d;
        private int e;

        static String a(a a1)
        {
            return a1.c;
        }

        static String b(a a1)
        {
            return a1.d;
        }

        static String c(a a1)
        {
            return a1.b;
        }

        static int d(a a1)
        {
            return a1.e;
        }

        public a(String s, String s1, String s2, int i)
        {
            a = DownloadLiteManager.this;
            super();
            b = s;
            c = s1;
            d = s2;
            e = i;
        }
    }

    private class b extends Thread
    {

        final DownloadLiteManager a;
        private a b;
        private Handler c;

        public Handler a()
        {
            return c;
        }

        public void a(Handler handler)
        {
            c = handler;
        }

        public void run()
        {
            boolean flag = ToolUtilLib.checkSdcard();
            if (!flag)
            {
                a.disconnect();
                return;
            }
            long l;
            Thread.sleep(500L);
            l = a.doDownloadFile(b, this);
            if (a.isCancel || l <= 0L)
            {
                break MISSING_BLOCK_LABEL_181;
            }
            if (c != null)
            {
                Message message = Message.obtain();
                message.what = 1;
                Bundle bundle = new Bundle();
                bundle.putString("msg_data_filepath", Uri.fromFile(new File(a.a(b), a.b(b))).toString());
                message.setData(bundle);
                c.sendMessage(message);
            }
            if (a.mCallback != null)
            {
                a.mCallback.onSuccess(a.c(b));
            }
            if (a.threads != null)
            {
                a.threads.remove(a.c(b));
            }
            a.disconnect();
            return;
            Object obj;
            obj;
            if (!a.isCancel)
            {
                Logger.e("DownloadLiteManager", ((SocketTimeoutException) (obj)).getMessage());
            }
            if (a.mCallback != null)
            {
                a.mCallback.onError(a.c(b));
            }
            if (c != null)
            {
                Message message1 = Message.obtain();
                message1.what = 2;
                c.sendMessage(message1);
            }
            if (a.threads != null)
            {
                a.threads.remove(a.c(b));
            }
            a.disconnect();
            return;
            message1;
            if (!a.isCancel)
            {
                Logger.e("DownloadLiteManager", message1.getMessage());
            }
            if (a.mCallback != null)
            {
                a.mCallback.onError(a.c(b));
            }
            if (c != null)
            {
                Message message2 = Message.obtain();
                message2.what = 2;
                c.sendMessage(message2);
            }
            if (a.threads != null)
            {
                a.threads.remove(a.c(b));
            }
            a.disconnect();
            return;
            message2;
            if (!a.isCancel)
            {
                Logger.e("DownloadLiteManager", message2.getMessage());
            }
            if (a.mCallback != null)
            {
                a.mCallback.onError(a.c(b));
            }
            if (c != null)
            {
                Message message3 = Message.obtain();
                message3.what = 2;
                c.sendMessage(message3);
            }
            if (a.threads != null)
            {
                a.threads.remove(a.c(b));
            }
            a.disconnect();
            return;
            message3;
            a.disconnect();
            throw message3;
        }

        public b(a a1, Handler handler)
        {
            a = DownloadLiteManager.this;
            super();
            b = a1;
            c = handler;
        }
    }


    public static final int MESSAGE_COMPLETED = 1;
    public static final int MESSAGE_ERROR = 2;
    public static final int MESSAGE_STATUS_UPDATE = 0;
    public static final String MSG_DATA_FILEPATH = "msg_data_filepath";
    private static final int TIMEOUT = 30000;
    private static DownloadLiteManager downloadMgr;
    private HttpURLConnection httpURLConnection;
    private volatile boolean isCancel;
    private DownloadCallback mCallback;
    private HashMap threads;

    private DownloadLiteManager()
    {
        isCancel = false;
    }

    private void disconnect()
    {
        if (httpURLConnection == null)
        {
            break MISSING_BLOCK_LABEL_14;
        }
        httpURLConnection.disconnect();
        return;
        Exception exception;
        exception;
        Logger.e("disconnectError", exception.getMessage(), exception);
        return;
    }

    private long doDownloadFile(a a1, b b1)
        throws IOException
    {
        isCancel = false;
        long l1 = 0L;
        Object obj;
        httpURLConnection = FreeFlowUtilBase.getHttpURLConnection(new URL(a.c(a1)));
        obj = new File(a.a(a1));
        if (((File) (obj)).exists()) goto _L2; else goto _L1
_L1:
        boolean flag = ((File) (obj)).mkdirs();
        if (flag) goto _L2; else goto _L3
_L3:
        long l = 0L;
        if (httpURLConnection != null)
        {
            httpURLConnection.disconnect();
        }
        if (false)
        {
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (a a1)
            {
                Logger.e("disconnectError", a1.getMessage(), a1);
            }
            return 0L;
        }
        if (true)
        {
            break MISSING_BLOCK_LABEL_97;
        }
        throw new NullPointerException();
_L5:
        return l;
_L2:
        obj = new FileOutputStream(new File(a.a(a1), a.b(a1)), true);
        int i;
        long l2;
        i = (0x10000 * a.d(a1)) / 8;
        FileChannel filechannel = ((FileOutputStream) (obj)).getChannel();
        if (filechannel.size() <= 0L)
        {
            break MISSING_BLOCK_LABEL_240;
        }
        l2 = filechannel.size();
        l1 = l2;
        if (l2 < (long)i)
        {
            break MISSING_BLOCK_LABEL_240;
        }
        if (httpURLConnection != null)
        {
            httpURLConnection.disconnect();
        }
        if (false)
        {
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (a a1)
            {
                Logger.e("disconnectError", a1.getMessage(), a1);
            }
            break MISSING_BLOCK_LABEL_237;
        }
        l = l2;
        if (obj == null) goto _L5; else goto _L4
_L4:
        ((FileOutputStream) (obj)).close();
        return l2;
        return l2;
        httpURLConnection.setRequestProperty("RANGE", String.format("bytes=%d-%d", new Object[] {
            Long.valueOf(l1), Integer.valueOf(i)
        }));
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(30000);
        if ((long)httpURLConnection.getContentLength() == 0L)
        {
            break MISSING_BLOCK_LABEL_324;
        }
        i = httpURLConnection.getResponseCode();
        if (i != 416)
        {
            break MISSING_BLOCK_LABEL_379;
        }
        if (httpURLConnection != null)
        {
            httpURLConnection.disconnect();
        }
        if (false)
        {
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (a a1)
            {
                Logger.e("disconnectError", a1.getMessage(), a1);
            }
            break MISSING_BLOCK_LABEL_376;
        }
        l = l1;
        if (obj == null) goto _L5; else goto _L6
_L6:
        ((FileOutputStream) (obj)).close();
        return l1;
        return l1;
        l2 = (long)httpURLConnection.getContentLength() + l1;
        if (httpURLConnection.getResponseCode() != 404)
        {
            break MISSING_BLOCK_LABEL_462;
        }
        Logger.throwRuntimeException("\u4E0B\u8F7D\u66F4\u65B0\u6587\u4EF6\uFF0C\u670D\u52A1\u5668\u8FD4\u56DE404");
        l = 0L;
        if (httpURLConnection != null)
        {
            httpURLConnection.disconnect();
        }
        if (false)
        {
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (a a1)
            {
                Logger.e("disconnectError", a1.getMessage(), a1);
            }
            break MISSING_BLOCK_LABEL_460;
        }
        if (obj == null) goto _L5; else goto _L7
_L7:
        ((FileOutputStream) (obj)).close();
        return 0L;
        return 0L;
        InputStream inputstream = httpURLConnection.getInputStream();
        byte abyte0[] = new byte[1024];
        i = 0;
_L13:
        if (isCancel) goto _L9; else goto _L8
_L8:
        int j = inputstream.read(abyte0);
        if (j == -1) goto _L9; else goto _L10
_L10:
        ((FileOutputStream) (obj)).write(abyte0, 0, j);
        l = (long)j + l1;
        if (i == 0)
        {
            break MISSING_BLOCK_LABEL_544;
        }
        if ((100L * l) / l2 - (long)5 < (long)i)
        {
            break MISSING_BLOCK_LABEL_754;
        }
        i += 5;
        if (b1.a() != null)
        {
            Message message = Message.obtain();
            message.what = 0;
            message.arg1 = i;
            message.arg2 = (int)(l2 / 1024L);
            b1.a().sendMessage(message);
        }
        if (mCallback == null) goto _L12; else goto _L11
_L11:
        mCallback.onProgressUpdate(a.c(a1), i);
        l1 = l;
          goto _L13
_L9:
        try
        {
            if (httpURLConnection != null)
            {
                httpURLConnection.disconnect();
            }
        }
        // Misplaced declaration of an exception variable
        catch (a a1)
        {
            Logger.e("disconnectError", a1.getMessage(), a1);
            return l1;
        }
        if (inputstream == null)
        {
            break MISSING_BLOCK_LABEL_650;
        }
        inputstream.close();
        l = l1;
        if (obj == null) goto _L5; else goto _L14
_L14:
        ((FileOutputStream) (obj)).close();
        return l1;
        a1;
        b1 = null;
        obj = null;
_L15:
        if (httpURLConnection != null)
        {
            httpURLConnection.disconnect();
        }
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_706;
        }
        ((InputStream) (obj)).close();
        if (b1 != null)
        {
            try
            {
                b1.close();
            }
            // Misplaced declaration of an exception variable
            catch (b b1)
            {
                Logger.e("disconnectError", b1.getMessage(), b1);
            }
        }
        throw a1;
        a1;
        b1 = ((b) (obj));
        obj = null;
        continue; /* Loop/switch isn't completed */
        a1;
        b1 = ((b) (obj));
        obj = inputstream;
        if (true) goto _L15; else goto _L12
_L12:
        l1 = l;
          goto _L13
        l1 = l;
          goto _L13
    }

    private String genFileName(String s)
    {
        return s.substring(s.lastIndexOf("/") + 1, s.length());
    }

    public static DownloadLiteManager getInstance()
    {
        if (downloadMgr != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/library/service/DownloadLiteManager;
        JVM INSTR monitorenter ;
        downloadMgr = new DownloadLiteManager();
        com/ximalaya/ting/android/library/service/DownloadLiteManager;
        JVM INSTR monitorexit ;
_L2:
        return downloadMgr;
        Exception exception;
        exception;
        com/ximalaya/ting/android/library/service/DownloadLiteManager;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void cancel()
    {
        isCancel = true;
        if (httpURLConnection != null)
        {
            (new com.ximalaya.ting.android.library.service.a(this)).start();
        }
    }

    public void download(String s, String s1, String s2, int i, Handler handler)
    {
        if (TextUtils.isEmpty(s2))
        {
            s2 = genFileName(s);
        }
        s1 = new b(new a(s, s1, s2, i), handler);
        if (threads == null)
        {
            threads = new HashMap();
        }
        threads.put(s, s1);
        s1.start();
    }

    public void setCallback(DownloadCallback downloadcallback)
    {
        mCallback = downloadcallback;
    }

    public void setNotifyHandler(String s, Handler handler)
    {
        if (threads != null)
        {
            s = (b)threads.get(s);
            if (s != null)
            {
                s.a(handler);
            }
        }
    }





}
