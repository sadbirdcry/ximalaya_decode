// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import android.widget.RemoteViews;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.library.constants.LibraryConstants;
import com.ximalaya.ting.android.library.model.AppAd;
import com.ximalaya.ting.android.library.util.FreeFlowUtilBase;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.util.ToolUtilLib;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.library.service:
//            b

public class DownloadService extends Service
{
    public class DownloadTask
    {

        public long id;
        public boolean isAutoNotifyInstall;
        public String keepDownResultKey;
        public String name;
        public Notification notification;
        public String path;
        final DownloadService this$0;
        public String url;

        public boolean equals(Object obj)
        {
            boolean flag1 = false;
            boolean flag = flag1;
            if (obj != null)
            {
                flag = flag1;
                if (obj instanceof DownloadTask)
                {
                    obj = (DownloadTask)obj;
                    flag = flag1;
                    if (url != null)
                    {
                        flag = flag1;
                        if (((DownloadTask) (obj)).url != null)
                        {
                            flag = url.equals(((DownloadTask) (obj)).url);
                        }
                    }
                }
            }
            return flag;
        }

        public String getFilePath()
        {
            return (new StringBuilder()).append(path).append(File.separator).append(name).toString();
        }

        public int hashCode()
        {
            int i;
            if (url == null)
            {
                i = 0;
            } else
            {
                i = url.hashCode();
            }
            return i + 31;
        }

        public DownloadTask()
        {
            this$0 = DownloadService.this;
            super();
        }
    }

    private class a extends Thread
    {

        final DownloadService a;
        private DownloadTask b;

        public void run()
        {
            Message message;
            Message message1;
            Message message2;
            Message message3;
            message3 = null;
            message = null;
            message1 = message;
            message2 = message3;
            if (!a.showInNotification) goto _L2; else goto _L1
_L1:
            message1 = message;
            message2 = message3;
            DownloadService downloadservice = a;
            message1 = message;
            message2 = message3;
            StringBuilder stringbuilder = (new StringBuilder()).append("\u5F00\u59CB\u4E0B\u8F7D");
            message1 = message;
            message2 = message3;
            if (!TextUtils.isEmpty(b.name)) goto _L4; else goto _L3
_L3:
            Object obj = "";
_L6:
            message1 = message;
            message2 = message3;
            downloadservice.showToast(stringbuilder.append(((String) (obj))).toString());
_L2:
            message1 = message;
            message2 = message3;
            boolean flag = ToolUtilLib.checkSdcard();
            if (!flag)
            {
                if (false && a.mHandler != null && a.showInNotification)
                {
                    a.mHandler.sendMessage(null);
                }
                a.mDownloadTaskList.remove(b);
                return;
            }
            break; /* Loop/switch isn't completed */
_L4:
            message1 = message;
            message2 = message3;
            obj = b.name;
            if (true) goto _L6; else goto _L5
_L5:
            message1 = message;
            message2 = message3;
            if (a.downloadUpdateFile(b) <= 0L) goto _L8; else goto _L7
_L7:
            message1 = message;
            message2 = message3;
            message = Message.obtain();
            message1 = message;
            message2 = message;
            obj = message;
            message.what = 3;
            message1 = message;
            message2 = message;
            obj = message;
            message.obj = b;
            message3 = message;
            message1 = message;
            message2 = message;
            obj = message;
            if (TextUtils.isEmpty(b.keepDownResultKey))
            {
                break MISSING_BLOCK_LABEL_295;
            }
            message1 = message;
            message2 = message;
            obj = message;
            SharedPreferencesUtil.getInstance(a.getApplicationContext()).saveBoolean(b.keepDownResultKey, true);
            message3 = message;
_L10:
            if (message3 != null && a.mHandler != null && a.showInNotification)
            {
                a.mHandler.sendMessage(message3);
            }
            a.mDownloadTaskList.remove(b);
            return;
_L8:
            message1 = message;
            message2 = message3;
            message = Message.obtain();
            message1 = message;
            message2 = message;
            obj = message;
            message.what = 4;
            message1 = message;
            message2 = message;
            obj = message;
            message.obj = b;
            message3 = message;
            if (true) goto _L10; else goto _L9
_L9:
            obj;
            obj = message1;
            message = Message.obtain();
            obj = message;
            message.what = 5;
            obj = message;
            message.obj = b;
            if (message != null && a.mHandler != null && a.showInNotification)
            {
                a.mHandler.sendMessage(message);
            }
            a.mDownloadTaskList.remove(b);
            return;
            obj;
            obj = message2;
            message = Message.obtain();
            obj = message;
            message.what = 4;
            obj = message;
            message.obj = b;
            if (message != null && a.mHandler != null && a.showInNotification)
            {
                a.mHandler.sendMessage(message);
            }
            a.mDownloadTaskList.remove(b);
            return;
            Exception exception;
            exception;
            obj = null;
_L12:
            if (obj != null && a.mHandler != null && a.showInNotification)
            {
                a.mHandler.sendMessage(((Message) (obj)));
            }
            a.mDownloadTaskList.remove(b);
            throw exception;
            exception;
            continue; /* Loop/switch isn't completed */
            exception;
            if (true) goto _L12; else goto _L11
_L11:
        }

        public a(DownloadTask downloadtask)
        {
            a = DownloadService.this;
            super();
            b = downloadtask;
        }
    }

    private class b extends Handler
    {

        final DownloadService a;

        public void handleMessage(Message message)
        {
            Notification notification;
            String s;
            DownloadTask downloadtask;
            downloadtask = (DownloadTask)message.obj;
            Notification notification1 = downloadtask.notification;
            if (TextUtils.isEmpty(downloadtask.name))
            {
                s = downloadtask.url;
            } else
            {
                s = downloadtask.name;
            }
            notification = notification1;
            if (notification1 == null)
            {
                notification = new Notification(com.ximalaya.ting.android.library.R.drawable.ting, (new StringBuilder()).append(s).append("\u4E0B\u8F7D\u5931\u8D25\uFF01").toString(), System.currentTimeMillis());
            }
            notification.flags = 16;
            notification.contentView = null;
            notification.when = System.currentTimeMillis();
            message.what;
            JVM INSTR tableswitch 3 5: default 124
        //                       3 151
        //                       4 349
        //                       5 364;
               goto _L1 _L2 _L3 _L4
_L1:
            a.mNotificationManager.notify((int)downloadtask.id, notification);
_L6:
            return;
_L2:
            message = new File(downloadtask.getFilePath());
            if (message == null)
            {
                continue; /* Loop/switch isn't completed */
            }
            s = message.getName().toUpperCase();
            if (TextUtils.isEmpty(s) || !s.endsWith(".APK")) goto _L6; else goto _L5
_L5:
            message = Uri.fromFile(message);
            s = MimeTypeMap.getFileExtensionFromUrl(message.toString());
            s = MimeTypeMap.getSingleton().getMimeTypeFromExtension(s);
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.addFlags(0x10000000);
            if (!TextUtils.isEmpty(s))
            {
                intent.setDataAndType(message, s);
                if (downloadtask.isAutoNotifyInstall)
                {
                    Intent intent1 = new Intent("android.intent.action.VIEW");
                    intent1.setDataAndType(message, s);
                    intent1.addFlags(0x10000000);
                    a.startActivity(intent1);
                }
            }
            message = PendingIntent.getActivity(a, 0, intent, 0);
            notification.tickerText = (new StringBuilder()).append(downloadtask.name).append("\u4E0B\u8F7D\u5B8C\u6210").toString();
            notification.setLatestEventInfo(a, downloadtask.name, "\u4E0B\u8F7D\u6210\u529F\uFF0C\u70B9\u51FB\u6253\u5F00", message);
            continue; /* Loop/switch isn't completed */
_L3:
            notification.setLatestEventInfo(a, s, "\u4E0B\u8F7D\u5931\u8D25", null);
            continue; /* Loop/switch isn't completed */
_L4:
            notification.setLatestEventInfo(a, s, "\u4E0B\u8F7D\u5931\u8D25\uFF0C\u8BF7\u6C42\u8D85\u65F6", null);
            if (true) goto _L1; else goto _L7
_L7:
        }

        private b()
        {
            a = DownloadService.this;
            super();
        }

        b(com.ximalaya.ting.android.library.service.b b1)
        {
            this();
        }
    }


    public static String APPAD = "AppAd";
    public static String OPENG_APP_ACTION = "openAppAction";
    public static String SHALLKEEPKEY = "keepDownResultKey";
    public static String SHOWINNOTIFICATION = "showInNotification";
    private static final String TAG = com/ximalaya/ting/android/library/service/DownloadService.getSimpleName();
    private List mDownloadTaskList;
    private Handler mHandler;
    private NotificationManager mNotificationManager;
    private String openAppAction;
    private boolean showInNotification;

    public DownloadService()
    {
        showInNotification = true;
        openAppAction = LibraryConstants.OPEN_APP_ACTION;
        mDownloadTaskList = new ArrayList();
    }

    private void changeNotification(DownloadTask downloadtask, int i)
    {
        RemoteViews remoteviews = downloadtask.notification.contentView;
        remoteviews.setTextViewText(com.ximalaya.ting.android.library.R.id.notificationPercent, (new StringBuilder()).append(i).append("%").toString());
        remoteviews.setTextViewText(com.ximalaya.ting.android.library.R.id.notificationTitle, downloadtask.name);
        remoteviews.setProgressBar(com.ximalaya.ting.android.library.R.id.notificationProgress, 100, i, false);
        mNotificationManager.notify((int)downloadtask.id, downloadtask.notification);
    }

    public static String getFilePath(String s)
    {
        return (new StringBuilder()).append(LibraryConstants.ADS_DIR).append(File.separator).append(s).toString();
    }

    private void showToast(String s)
    {
        mHandler.post(new com.ximalaya.ting.android.library.service.b(this, s));
    }

    public void createNotification(DownloadTask downloadtask)
    {
        Notification notification = new Notification(com.ximalaya.ting.android.library.R.drawable.ting, (new StringBuilder()).append(downloadtask.name).append("\u5F00\u59CB\u4E0B\u8F7D").toString(), System.currentTimeMillis());
        notification.flags = 16;
        Object obj = new RemoteViews(getPackageName(), com.ximalaya.ting.android.library.R.layout.notification_item);
        ((RemoteViews) (obj)).setTextViewText(com.ximalaya.ting.android.library.R.id.notificationTitle, "\u6B63\u5728\u4E0B\u8F7D");
        ((RemoteViews) (obj)).setTextViewText(com.ximalaya.ting.android.library.R.id.notificationPercent, "0%");
        ((RemoteViews) (obj)).setProgressBar(com.ximalaya.ting.android.library.R.id.notificationProgress, 100, 0, false);
        notification.contentView = ((RemoteViews) (obj));
        obj = new Intent(openAppAction);
        ((Intent) (obj)).addFlags(0x20000000);
        notification.contentIntent = PendingIntent.getActivity(this, 0, ((Intent) (obj)), 0);
        downloadtask.notification = notification;
        mNotificationManager.notify((int)downloadtask.id, notification);
    }

    public long downloadUpdateFile(DownloadTask downloadtask)
        throws IOException
    {
        HttpURLConnection httpurlconnection;
        Object obj;
        long l;
        l = 0L;
        httpurlconnection = FreeFlowUtilBase.getHttpURLConnection(new URL(downloadtask.url));
        if (TextUtils.isEmpty(downloadtask.name))
        {
            String s = httpurlconnection.getURL().toString();
            downloadtask.name = URLDecoder.decode(s.substring(s.lastIndexOf("/") + 1, s.length()), "utf-8");
            downloadtask.name = downloadtask.name.substring(downloadtask.name.lastIndexOf("/") + 1, downloadtask.name.length());
        }
        if (downloadtask.name.lastIndexOf(".apk") == -1 || downloadtask.name.lastIndexOf(".apk") != downloadtask.name.length() - 4)
        {
            downloadtask.name = (new StringBuilder()).append(downloadtask.name).append(".apk").toString();
        }
        if (TextUtils.isEmpty(downloadtask.path))
        {
            downloadtask.path = LibraryConstants.ADS_DIR;
        }
        obj = new File(downloadtask.path);
        if (((File) (obj)).exists() || ((File) (obj)).mkdirs()) goto _L2; else goto _L1
_L1:
        long l1 = 0L;
_L4:
        return l1;
_L2:
        if (showInNotification)
        {
            createNotification(downloadtask);
        }
        obj = new FileOutputStream(new File(downloadtask.getFilePath()), true);
        FileChannel filechannel = ((FileOutputStream) (obj)).getChannel();
        if (filechannel.size() > 0L)
        {
            l = filechannel.size();
            httpurlconnection.setRequestProperty("RANGE", (new StringBuilder()).append("bytes=").append(l).append("-").toString());
        }
        l1 = httpurlconnection.getContentLength();
        if (l < l1)
        {
            break; /* Loop/switch isn't completed */
        }
        ((FileOutputStream) (obj)).close();
        l1 = l;
        if (httpurlconnection != null)
        {
            httpurlconnection.disconnect();
            return l;
        }
        if (true) goto _L4; else goto _L3
_L3:
        httpurlconnection.setConnectTimeout(30000);
        httpurlconnection.setReadTimeout(30000);
        if (l1 == 0L || httpurlconnection.getResponseCode() == 416)
        {
            ((FileOutputStream) (obj)).close();
            if (httpurlconnection != null)
            {
                httpurlconnection.disconnect();
            }
            l1 = l;
            if (showInNotification)
            {
                changeNotification(downloadtask, 100);
                return l;
            }
        } else
        {
            long l4 = httpurlconnection.getContentLength();
            if (httpurlconnection.getResponseCode() == 404)
            {
                Logger.throwRuntimeException("\u4E0B\u8F7D\u66F4\u65B0\u6587\u4EF6\uFF0C\u670D\u52A1\u5668\u8FD4\u56DE404");
                ((FileOutputStream) (obj)).close();
                if (httpurlconnection != null)
                {
                    httpurlconnection.disconnect();
                }
                return 0L;
            }
            InputStream inputstream = httpurlconnection.getInputStream();
            byte abyte0[] = new byte[1024];
            long l2 = l;
            int i = 0;
            do
            {
                int j = inputstream.read(abyte0);
                if (j == -1)
                {
                    break;
                }
                ((FileOutputStream) (obj)).write(abyte0, 0, j);
                long l3 = l2 + (long)j;
                l2 = l3;
                if (!showInNotification)
                {
                    continue;
                }
                if (i != 0)
                {
                    l2 = l3;
                    if ((100L * l3) / (l4 + l) - (long)5 < (long)i)
                    {
                        continue;
                    }
                }
                i += 5;
                changeNotification(downloadtask, i);
                l2 = l3;
            } while (true);
            if (httpurlconnection != null)
            {
                httpurlconnection.disconnect();
            }
            inputstream.close();
            ((FileOutputStream) (obj)).close();
            return l2;
        }
        if (true) goto _L4; else goto _L5
_L5:
    }

    public IBinder onBind(Intent intent)
    {
        return null;
    }

    public void onCreate()
    {
        super.onCreate();
        mNotificationManager = (NotificationManager)getSystemService("notification");
        mHandler = new b(null);
    }

    public int onStartCommand(Intent intent, int i, int j)
    {
        if (intent != null)
        {
            showInNotification = intent.getBooleanExtra(SHOWINNOTIFICATION, true);
            Object obj = intent.getStringExtra(SHALLKEEPKEY);
            if (intent.hasExtra(OPENG_APP_ACTION))
            {
                openAppAction = intent.getStringExtra(OPENG_APP_ACTION);
            }
            DownloadTask downloadtask;
            if (intent.hasExtra(APPAD))
            {
                AppAd appad = (AppAd)JSON.parseObject(intent.getStringExtra(APPAD), com/ximalaya/ting/android/library/model/AppAd);
                downloadtask = new DownloadTask();
                downloadtask.keepDownResultKey = ((String) (obj));
                downloadtask.id = System.currentTimeMillis();
                downloadtask.url = appad.getLink();
                downloadtask.name = appad.getName();
                downloadtask.isAutoNotifyInstall = appad.getIsAutoNotifyInstall();
                Logger.e(TAG, (new StringBuilder()).append("isAutoNotifyInstall(appad):").append(downloadtask.isAutoNotifyInstall).toString());
            } else
            {
                String s = intent.getStringExtra("file_name");
                String s1 = intent.getStringExtra("download_url");
                String s2 = intent.getStringExtra("save_path");
                boolean flag = intent.getBooleanExtra("isAutoNotifyInstall", false);
                Logger.e(TAG, (new StringBuilder()).append("isAutoNotifyInstall:").append(flag).toString());
                Logger.e(TAG, (new StringBuilder()).append("mFileName:").append(s).toString());
                Logger.e(TAG, (new StringBuilder()).append("mDownloadUrl:").append(s1).toString());
                if (!TextUtils.isEmpty(s1))
                {
                    downloadtask = new DownloadTask();
                    downloadtask.id = System.currentTimeMillis();
                    downloadtask.url = s1;
                    downloadtask.name = s;
                    downloadtask.path = s2;
                    downloadtask.keepDownResultKey = ((String) (obj));
                    downloadtask.isAutoNotifyInstall = flag;
                } else
                {
                    downloadtask = null;
                }
            }
            if (downloadtask != null)
            {
                for (obj = mDownloadTaskList.iterator(); ((Iterator) (obj)).hasNext();)
                {
                    if (downloadtask.equals((DownloadTask)((Iterator) (obj)).next()))
                    {
                        return super.onStartCommand(intent, i, j);
                    }
                }

                mDownloadTaskList.add(downloadtask);
                (new a(downloadtask)).start();
            }
        }
        return super.onStartCommand(intent, i, j);
    }






}
