// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.service;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.ToolUtilLib;
import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;

// Referenced classes of package com.ximalaya.ting.android.library.service:
//            DownloadLiteManager

private class c extends Thread
{

    final DownloadLiteManager a;
    private a b;
    private Handler c;

    public Handler a()
    {
        return c;
    }

    public void a(Handler handler)
    {
        c = handler;
    }

    public void run()
    {
        boolean flag = ToolUtilLib.checkSdcard();
        if (!flag)
        {
            DownloadLiteManager.access$000(a);
            return;
        }
        long l;
        Thread.sleep(500L);
        l = DownloadLiteManager.access$100(a, b, this);
        if (DownloadLiteManager.access$200(a) || l <= 0L)
        {
            break MISSING_BLOCK_LABEL_181;
        }
        if (c != null)
        {
            Message message = Message.obtain();
            message.what = 1;
            Bundle bundle = new Bundle();
            bundle.putString("msg_data_filepath", Uri.fromFile(new File(a(b), b(b))).toString());
            message.setData(bundle);
            c.sendMessage(message);
        }
        if (DownloadLiteManager.access$500(a) != null)
        {
            DownloadLiteManager.access$500(a).onSuccess(c(b));
        }
        if (DownloadLiteManager.access$700(a) != null)
        {
            DownloadLiteManager.access$700(a).remove(c(b));
        }
        DownloadLiteManager.access$000(a);
        return;
        Object obj;
        obj;
        if (!DownloadLiteManager.access$200(a))
        {
            Logger.e("DownloadLiteManager", ((SocketTimeoutException) (obj)).getMessage());
        }
        if (DownloadLiteManager.access$500(a) != null)
        {
            DownloadLiteManager.access$500(a).onError(c(b));
        }
        if (c != null)
        {
            Message message1 = Message.obtain();
            message1.what = 2;
            c.sendMessage(message1);
        }
        if (DownloadLiteManager.access$700(a) != null)
        {
            DownloadLiteManager.access$700(a).remove(c(b));
        }
        DownloadLiteManager.access$000(a);
        return;
        message1;
        if (!DownloadLiteManager.access$200(a))
        {
            Logger.e("DownloadLiteManager", message1.getMessage());
        }
        if (DownloadLiteManager.access$500(a) != null)
        {
            DownloadLiteManager.access$500(a).onError(c(b));
        }
        if (c != null)
        {
            Message message2 = Message.obtain();
            message2.what = 2;
            c.sendMessage(message2);
        }
        if (DownloadLiteManager.access$700(a) != null)
        {
            DownloadLiteManager.access$700(a).remove(c(b));
        }
        DownloadLiteManager.access$000(a);
        return;
        message2;
        if (!DownloadLiteManager.access$200(a))
        {
            Logger.e("DownloadLiteManager", message2.getMessage());
        }
        if (DownloadLiteManager.access$500(a) != null)
        {
            DownloadLiteManager.access$500(a).onError(c(b));
        }
        if (c != null)
        {
            Message message3 = Message.obtain();
            message3.what = 2;
            c.sendMessage(message3);
        }
        if (DownloadLiteManager.access$700(a) != null)
        {
            DownloadLiteManager.access$700(a).remove(c(b));
        }
        DownloadLiteManager.access$000(a);
        return;
        message3;
        DownloadLiteManager.access$000(a);
        throw message3;
    }

    public wnloadCallback(DownloadLiteManager downloadlitemanager, wnloadCallback wnloadcallback, Handler handler)
    {
        a = downloadlitemanager;
        super();
        b = wnloadcallback;
        c = handler;
    }
}
