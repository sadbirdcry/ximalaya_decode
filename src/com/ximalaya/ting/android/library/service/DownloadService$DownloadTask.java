// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.library.service;

import android.app.Notification;
import java.io.File;

// Referenced classes of package com.ximalaya.ting.android.library.service:
//            DownloadService

public class this._cls0
{

    public long id;
    public boolean isAutoNotifyInstall;
    public String keepDownResultKey;
    public String name;
    public Notification notification;
    public String path;
    final DownloadService this$0;
    public String url;

    public boolean equals(Object obj)
    {
        boolean flag1 = false;
        boolean flag = flag1;
        if (obj != null)
        {
            flag = flag1;
            if (obj instanceof this._cls0)
            {
                obj = (this._cls0)obj;
                flag = flag1;
                if (url != null)
                {
                    flag = flag1;
                    if (((url) (obj)).url != null)
                    {
                        flag = url.equals(((url) (obj)).url);
                    }
                }
            }
        }
        return flag;
    }

    public String getFilePath()
    {
        return (new StringBuilder()).append(path).append(File.separator).append(name).toString();
    }

    public int hashCode()
    {
        int i;
        if (url == null)
        {
            i = 0;
        } else
        {
            i = url.hashCode();
        }
        return i + 31;
    }

    public ()
    {
        this$0 = DownloadService.this;
        super();
    }
}
