// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.appwidget;

import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public abstract class BaseAppWidgetprovider extends AppWidgetProvider
{

    protected Context a;

    public BaseAppWidgetprovider()
    {
    }

    public abstract void a();

    public abstract void a(int i);

    public abstract void a(int i, int j);

    public abstract void b();

    public void onReceive(Context context, Intent intent)
    {
        a = context.getApplicationContext();
        super.onReceive(context, intent);
        context = intent.getAction();
        Log.e("", (new StringBuilder()).append("Xm widget action ").append(context).toString());
        if ("com.ximalaya.ting.android.action.ACTION_PLAY_START".equals(context))
        {
            b();
        } else
        {
            if ("com.ximalaya.ting.android.action.ACTION_PLAY_PAUSE".equals(context))
            {
                a();
                return;
            }
            if ("com.ximalaya.ting.android.action.ACTION_PLAY_CHANGE_SOUND".equals(context))
            {
                a(0);
                return;
            }
            if ("com.ximalaya.ting.android.ACTION_PLAY_PROGRESS_UPDATE".equals(context))
            {
                a(intent.getIntExtra("ACTION_EXTRA_POSITION", 0), intent.getIntExtra("ACTION_EXTRA_DURATION", 0));
                return;
            }
        }
    }
}
