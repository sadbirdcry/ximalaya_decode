// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.appwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;
import com.ximalaya.ting.android.activity.login.WelcomeActivity;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.appwidget:
//            BaseAppWidgetprovider, a

public class WidgetProviderSmall extends BaseAppWidgetprovider
{

    private RemoteViews b;

    public WidgetProviderSmall()
    {
    }

    private void a(RemoteViews remoteviews)
    {
        AppWidgetManager.getInstance(a).updateAppWidget(new ComponentName(a, getClass()), remoteviews);
    }

    static void a(WidgetProviderSmall widgetprovidersmall, RemoteViews remoteviews)
    {
        widgetprovidersmall.a(remoteviews);
    }

    public void a()
    {
        Log.e("", "Xm on play pause ");
        if (b == null)
        {
            b = new RemoteViews(a.getPackageName(), 0x7f030043);
        }
        b.setImageViewResource(0x7f0a0172, 0x7f0203b3);
        if (android.os.Build.VERSION.SDK_INT >= 15)
        {
            b.setContentDescription(0x7f0a0172, "\u5F00\u59CB\u64AD\u653E");
        }
        a(b);
    }

    public void a(int i)
    {
        Log.e("", (new StringBuilder()).append("Xm on widget sound change ").append(i).toString());
        if (a != null) goto _L2; else goto _L1
_L1:
        Object obj;
        return;
_L2:
        if ((obj = PlayListControl.getPlayListManager().getCurSound()) == null) goto _L1; else goto _L3
_L3:
        if (b == null)
        {
            b = new RemoteViews(a.getPackageName(), 0x7f030043);
        }
        Object obj1;
        if (((SoundInfo) (obj)).category == 0)
        {
            b.setTextViewText(0x7f0a016f, ((SoundInfo) (obj)).title);
        } else
        {
            b.setTextViewText(0x7f0a016f, ((SoundInfo) (obj)).radioName);
        }
        obj1 = LocalMediaService.getInstance();
        if (obj1 == null) goto _L5; else goto _L4
_L4:
        ((LocalMediaService) (obj1)).getPlayServiceState();
        JVM INSTR tableswitch 1 3: default 128
    //                   1 228
    //                   2 261
    //                   3 228;
           goto _L6 _L7 _L8 _L7
_L6:
        break; /* Loop/switch isn't completed */
_L8:
        break MISSING_BLOCK_LABEL_261;
_L5:
        obj1 = b;
        if (TextUtils.isEmpty(((SoundInfo) (obj)).coverLarge))
        {
            obj = ((SoundInfo) (obj)).coverSmall;
        } else
        {
            obj = ((SoundInfo) (obj)).coverLarge;
        }
        ImageManager2.from(a).displayImage(b, 0x7f0a016e, ((String) (obj)), 0x7f020598, ToolUtil.dp2px(a, 100F), ToolUtil.dp2px(a, 100F), ToolUtil.dp2px(a, 10F), new a(this, ((RemoteViews) (obj1))));
        a(b);
        return;
_L7:
        b.setImageViewResource(0x7f0a0172, 0x7f0203b0);
        if (android.os.Build.VERSION.SDK_INT >= 15)
        {
            b.setContentDescription(0x7f0a0172, "\u6682\u505C");
        }
          goto _L5
        b.setImageViewResource(0x7f0a0172, 0x7f0203b3);
        if (android.os.Build.VERSION.SDK_INT >= 15)
        {
            b.setContentDescription(0x7f0a0172, "\u5F00\u59CB\u64AD\u653E");
        }
          goto _L5
    }

    public void a(int i, int j)
    {
        if (b == null)
        {
            b = new RemoteViews(a.getPackageName(), 0x7f030043);
        }
        b.setProgressBar(0x7f0a0170, j, i, false);
        a(b);
    }

    public void b()
    {
        if (b == null)
        {
            b = new RemoteViews(a.getPackageName(), 0x7f030043);
        }
        b.setImageViewResource(0x7f0a0172, 0x7f0203b0);
        if (android.os.Build.VERSION.SDK_INT >= 15)
        {
            b.setContentDescription(0x7f0a0172, "\u6682\u505C");
        }
        a(b);
    }

    public void onUpdate(Context context, AppWidgetManager appwidgetmanager, int ai[])
    {
        super.onUpdate(context, appwidgetmanager, ai);
        int j = ai.length;
        for (int i = 0; i < j; i++)
        {
            int k = ai[i];
            RemoteViews remoteviews = new RemoteViews(context.getPackageName(), 0x7f030043);
            Intent intent = new Intent(a, com/ximalaya/ting/android/service/play/LocalMediaService);
            intent.setAction("com.ximalaya.ting.android.ACTION_CONTROL_PLAY_OR_PAUSE");
            remoteviews.setOnClickPendingIntent(0x7f0a0172, PendingIntent.getService(context, 0, intent, 0x8000000));
            intent = new Intent(a, com/ximalaya/ting/android/service/play/LocalMediaService);
            intent.setAction("com.ximalaya.ting.android.ACTION_CONTROL_PLAY_PRE");
            remoteviews.setOnClickPendingIntent(0x7f0a0171, PendingIntent.getService(context, 0, intent, 0x8000000));
            intent = new Intent(a, com/ximalaya/ting/android/service/play/LocalMediaService);
            intent.setAction("com.ximalaya.ting.android.ACTION_CONTROL_PLAY_NEXT");
            remoteviews.setOnClickPendingIntent(0x7f0a0173, PendingIntent.getService(context, 0, intent, 0x8000000));
            intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.setComponent(new ComponentName(a, com/ximalaya/ting/android/activity/login/WelcomeActivity));
            remoteviews.setOnClickPendingIntent(0x7f0a016e, PendingIntent.getActivity(context, 0, intent, 0x8000000));
            appwidgetmanager.updateAppWidget(k, remoteviews);
        }

        a(0);
    }
}
