// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.localalarm;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.umeng.analytics.MobclickAgent;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.broadcast.AlarmReceiver;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.actpushstrage.ActPushStrategy;
import com.ximalaya.ting.android.model.actpushstrage.TimeRange;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

// Referenced classes of package com.ximalaya.ting.android.service.localalarm:
//            a

public class LocalAlarm
{

    public LocalAlarm()
    {
    }

    public static void cancelAlarmIfExists(Context context, int i, Intent intent)
    {
        try
        {
            intent = PendingIntent.getBroadcast(context, i, intent, 0);
            ((AlarmManager)context.getSystemService("alarm")).cancel(intent);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
    }

    private static void pushStrategyHandler(Context context, String s)
    {
        if (!TextUtils.isEmpty(s))
        {
            try
            {
                s = (ActPushStrategy)JSON.parseObject(s, com/ximalaya/ting/android/model/actpushstrage/ActPushStrategy);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s = null;
            }
            if (s != null && s.isOpen())
            {
                s = s.getTimeRange();
                if (s != null && s.size() > 0)
                {
                    s = (TimeRange)s.get((new Random()).nextInt(s.size()));
                    if (s != null)
                    {
                        Object obj = s.getDescripe();
                        if (!TextUtils.isEmpty(((CharSequence) (obj))))
                        {
                            SharedPreferencesUtil.getInstance(context).saveString("local_msg", ((String) (obj)));
                        }
                        obj = new Intent(context, com/ximalaya/ting/android/broadcast/AlarmReceiver);
                        ((Intent) (obj)).setAction("com.ximalaya.ting.android.action.LOCAL_NOTIFY");
                        ((Intent) (obj)).addFlags(0x10000000);
                        PendingIntent pendingintent = PendingIntent.getBroadcast(context, 100, ((Intent) (obj)), 0);
                        cancelAlarmIfExists(context, 100, ((Intent) (obj)));
                        int i = Calendar.getInstance().get(11);
                        int j = s.getBegin();
                        long l = ((new Random()).nextInt((s.getEnd() - s.getBegin()) * 60) + ((24 - i) + j) * 60) * 60 * 1000;
                        ((AlarmManager)context.getSystemService("alarm")).setRepeating(0, System.currentTimeMillis() + l, l, pendingintent);
                        return;
                    }
                }
            }
        }
    }

    public static void startAlarm(Context context)
    {
        byte byte0 = 24;
        MobclickAgent.updateOnlineConfig(context);
        Object obj = new Intent(context, com/ximalaya/ting/android/broadcast/AlarmReceiver);
        ((Intent) (obj)).setAction("com.ximalaya.ting.android.action.LOCAL_NOTIFY");
        ((Intent) (obj)).addFlags(0x10000000);
        PendingIntent pendingintent = PendingIntent.getBroadcast(context, 100, ((Intent) (obj)), 0);
        cancelAlarmIfExists(context, 100, ((Intent) (obj)));
        obj = MobclickAgent.getConfigParams(context, "local_notify_lower_limit");
        int j = 12;
        if (!TextUtils.isEmpty(((CharSequence) (obj))))
        {
            j = Integer.valueOf(((String) (obj))).intValue();
        }
        obj = MobclickAgent.getConfigParams(context, "local_notify_top_limit");
        int i;
        long l;
        if (!TextUtils.isEmpty(((CharSequence) (obj))))
        {
            i = Integer.valueOf(((String) (obj))).intValue();
        } else
        {
            i = 24;
        }
        if (i == 0)
        {
            i = byte0;
        }
        l = (j + (new Random()).nextInt(i - j)) * 60 * 60 * 1000;
        ((AlarmManager)context.getSystemService("alarm")).setRepeating(0, System.currentTimeMillis() + l, l, pendingintent);
    }

    public static void startAlarmWithNotification(Context context, boolean flag)
    {
        String s1;
label0:
        {
            if (!SharedPreferencesUtil.getInstance(context).getBoolean("IS_ACTIVATED_TAG", false))
            {
                if (flag)
                {
                    String s = SharedPreferencesUtil.getInstance(context).getString("local_msg");
                    if (!TextUtils.isEmpty(s))
                    {
                        NotificationManager notificationmanager = (NotificationManager)context.getSystemService("notification");
                        Object obj = new Random(System.currentTimeMillis());
                        int i = ((Random) (obj)).nextInt();
                        Intent intent = new Intent();
                        intent.setClass(context, com/ximalaya/ting/android/activity/MainTabActivity2);
                        intent.setFlags(0x10000000);
                        obj = PendingIntent.getActivity(context, ((Random) (obj)).nextInt(), intent, 0x8000000);
                        notificationmanager.notify(i, ToolUtil.createNotification(context, context.getString(0x7f090000), s, s, ((PendingIntent) (obj))));
                    }
                }
                MobclickAgent.updateOnlineConfig(context);
                s1 = MobclickAgent.getConfigParams(context, "activation_push_strategy");
                if (!TextUtils.isEmpty(s1))
                {
                    break label0;
                }
                MobclickAgent.setOnlineConfigureListener(new a(context));
            }
            return;
        }
        pushStrategyHandler(context, s1);
    }

}
