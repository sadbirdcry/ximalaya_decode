// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Binder;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import com.ximalaya.ting.android.a;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

// Referenced classes of package com.ximalaya.ting.android.service:
//            a, b, c, d, 
//            e, f

public class LocalRecorderService extends Service
{
    public class LocalBinder extends Binder
    {

        final LocalRecorderService this$0;

        public LocalRecorderService getService()
        {
            return LocalRecorderService.this;
        }

        public LocalBinder()
        {
            this$0 = LocalRecorderService.this;
            super();
        }
    }

    public static interface OnRecordingListener
    {

        public abstract void onPhoneRing_StopRecord();

        public abstract void onSoundPlayCompletion();

        public abstract void startPlayRecordSound(int i);

        public abstract void updateAmplitude();

        public abstract void updatePlayProgress(int i);

        public abstract void updateRecordTime(long l);
    }

    class a extends PhoneStateListener
    {

        final LocalRecorderService a;

        public void onCallStateChanged(int i, String s)
        {
            super.onCallStateChanged(i, s);
            i;
            JVM INSTR tableswitch 0 2: default 32
        //                       0 33
        //                       1 129
        //                       2 89;
               goto _L1 _L2 _L3 _L4
_L1:
            return;
_L2:
            if (a.mMediaPlayer != null && !a.mMediaPlayer.isPlaying() && a.telPauseFlag)
            {
                try
                {
                    a.start();
                    a.telPauseFlag = false;
                    return;
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s.printStackTrace();
                }
                return;
            }
            continue; /* Loop/switch isn't completed */
_L4:
            if (a.mMediaPlayer != null && a.mMediaPlayer.isPlaying())
            {
                a.pause();
                a.telPauseFlag = true;
                return;
            }
            if (true) goto _L1; else goto _L3
_L3:
            if (a.mMediaPlayer != null && a.mMediaPlayer.isPlaying())
            {
                a.pause();
                a.telPauseFlag = true;
            }
            if (LocalRecorderService.status == 1 && a.onRecordingListener != null)
            {
                a.onRecordingListener.onPhoneRing_StopRecord();
                return;
            }
            if (true) goto _L1; else goto _L5
_L5:
        }

        a()
        {
            a = LocalRecorderService.this;
            super();
        }
    }


    public static int playTime = 0;
    private static LocalRecorderService service = null;
    public static volatile int status = 0;
    public volatile int lastPlaySoundDur;
    public volatile int lastSoundDur;
    private List list;
    private final IBinder mBinder = new LocalBinder();
    private BroadcastReceiver mBroReceiver;
    android.media.MediaPlayer.OnCompletionListener mCompleteListener;
    private MediaPlayer mMediaPlayer;
    private MediaRecorder mMediaRecorder;
    android.media.MediaPlayer.OnPreparedListener mPrepareListener;
    private int mtime;
    private OnRecordingListener onRecordingListener;
    private int pllH;
    private int pllW;
    private long startTime;
    private TelephonyManager tManager;
    private TimerTask task;
    private boolean telPauseFlag;
    private File temfi;
    private Timer timer;

    public LocalRecorderService()
    {
        mMediaPlayer = null;
        temfi = null;
        list = new ArrayList();
        mtime = 0;
        telPauseFlag = false;
        lastSoundDur = 0;
        lastPlaySoundDur = 0;
        task = null;
        mBroReceiver = new com.ximalaya.ting.android.service.a(this);
        pllH = 100;
        mCompleteListener = new b(this);
        mPrepareListener = new c(this);
    }

    public static LocalRecorderService getInstance()
    {
        return service;
    }

    private int getPosition()
    {
        if (mMediaPlayer != null && isPlaying())
        {
            double d3 = mMediaPlayer.getCurrentPosition();
            double d2 = mMediaPlayer.getDuration();
            double d1 = d3;
            if (d2 - d3 <= 200D)
            {
                d1 = d2;
            }
            return (int)((d1 / d2) * 100D);
        } else
        {
            return 0;
        }
    }

    private void initData()
    {
        tManager = (TelephonyManager)getSystemService("phone");
        tManager.listen(new a(), 32);
        registerReceiver(mBroReceiver, new IntentFilter("android.intent.action.HEADSET_PLUG"));
    }

    private void showNotification()
    {
    }

    public void createMediaPlayer()
    {
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnPreparedListener(mPrepareListener);
        mMediaPlayer.setOnCompletionListener(mCompleteListener);
        try
        {
            mMediaPlayer.setDataSource(temfi.getAbsolutePath());
            mMediaPlayer.prepareAsync();
            return;
        }
        catch (Exception exception)
        {
            return;
        }
    }

    public int getDuration()
    {
        if (mMediaPlayer != null && (status == 3 || status == 4 || status == 5 || status == 7))
        {
            return mMediaPlayer.getDuration();
        } else
        {
            return 0;
        }
    }

    public List getMaxAmplitude()
    {
        int i;
        int j;
        int k;
        j = 200;
        i = 10;
        if (status != 1 || mMediaRecorder == null)
        {
            return list;
        }
        try
        {
            k = (int)(Math.log10(mMediaRecorder.getMaxAmplitude()) * 20D);
            break MISSING_BLOCK_LABEL_44;
        }
        catch (Exception exception) { }
          goto _L1
_L3:
        j = (int)(((double)((i - 40) * pllH) * 1.0D) / 60D);
        i = j;
        if (j >= 15)
        {
            break MISSING_BLOCK_LABEL_92;
        }
        i = (int)(Math.random() * 10D) + 15;
        if (list.size() >= pllW)
        {
            list.remove(0);
        }
        list.add(Integer.valueOf(i));
_L1:
        return list;
        if (k >= 10)
        {
            i = k;
        }
        if (i > 200)
        {
            i = j;
        }
        if (true) goto _L3; else goto _L2
_L2:
    }

    public File getRecordingFile()
    {
        if (temfi != null)
        {
            return temfi;
        } else
        {
            return null;
        }
    }

    public void initRecording()
        throws Exception
    {
        if (mMediaRecorder != null)
        {
            mMediaRecorder.release();
        }
        File file = new File(com.ximalaya.ting.android.a.ae);
        if (!file.exists())
        {
            file.mkdirs();
        }
        mMediaRecorder = new MediaRecorder();
        mMediaRecorder.setAudioSource(1);
        if (android.os.Build.VERSION.SDK_INT >= 10)
        {
            mMediaRecorder.setAudioSamplingRate(44100);
            mMediaRecorder.setAudioEncodingBitRate(0x1f400);
            mMediaRecorder.setOutputFormat(2);
            mMediaRecorder.setAudioEncoder(3);
            temfi = File.createTempFile("tempRecord", ".aac", file);
        } else
        {
            mMediaRecorder.setAudioSamplingRate(8000);
            mMediaRecorder.setAudioEncodingBitRate(12200);
            mMediaRecorder.setOutputFormat(1);
            mMediaRecorder.setAudioEncoder(1);
            temfi = File.createTempFile("tempRecord", ".amr", file);
        }
        mMediaRecorder.setOutputFile(temfi.getAbsolutePath());
        mMediaRecorder.setOnErrorListener(new d(this));
        mMediaRecorder.setOnInfoListener(new e(this));
        mMediaRecorder.prepare();
    }

    public boolean isPlaying()
    {
        if (mMediaPlayer != null)
        {
            return mMediaPlayer.isPlaying();
        } else
        {
            return false;
        }
    }

    public boolean isRecorder()
    {
        return status == 1;
    }

    public IBinder onBind(Intent intent)
    {
        return mBinder;
    }

    public void onCreate()
    {
        service = this;
        showNotification();
        initData();
        task = new f(this);
    }

    public void onDestroy()
    {
        if (mMediaRecorder == null) goto _L2; else goto _L1
_L1:
        if (status != 1) goto _L4; else goto _L3
_L3:
        stopre();
_L5:
        mMediaRecorder.release();
        mMediaRecorder = null;
_L2:
        if (mMediaPlayer != null && status == 2)
        {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        if (task != null)
        {
            task.cancel();
        }
        task = null;
        if (timer != null)
        {
            timer.cancel();
        }
        timer = null;
        if (mBroReceiver != null)
        {
            unregisterReceiver(mBroReceiver);
            mBroReceiver = null;
        }
        service = null;
        super.onDestroy();
        return;
_L4:
        mMediaRecorder.reset();
          goto _L5
        Exception exception;
        exception;
          goto _L2
    }

    public int onStartCommand(Intent intent, int i, int j)
    {
        return 1;
    }

    public void pause()
    {
        if (mMediaPlayer != null && mMediaPlayer.isPlaying())
        {
            mMediaPlayer.pause();
            status = 5;
        }
    }

    public long recordingDur()
    {
        return (System.currentTimeMillis() - startTime) / 1000L;
    }

    public long seek(long l)
    {
        mMediaPlayer.seekTo((int)l);
        return l;
    }

    public void setOnRecordingListener(OnRecordingListener onrecordinglistener)
    {
        onRecordingListener = onrecordinglistener;
    }

    public void setWith(int i)
    {
        pllW = i;
    }

    public void start()
    {
        playTime = getDuration();
        status = 2;
        if (mMediaPlayer != null)
        {
            mMediaPlayer.start();
        }
    }

    public void startre()
        throws Exception
    {
        initRecording();
        mMediaRecorder.start();
        if (timer == null)
        {
            timer = new Timer();
            timer.schedule(task, 0L, 200L);
        }
        startTime = System.currentTimeMillis();
        status = 1;
    }

    public void stop()
    {
        status = 3;
        mMediaPlayer.stop();
        startTime = -1L;
    }

    public long stopre()
    {
        if (status != 0)
        {
            long l;
            try
            {
                mMediaRecorder.stop();
                mMediaRecorder.reset();
            }
            catch (Exception exception) { }
            status = 0;
            lastSoundDur = 0;
            list.clear();
            l = recordingDur();
            startTime = -1L;
            return l;
        } else
        {
            return -1L;
        }
    }





/*
    static boolean access$102(LocalRecorderService localrecorderservice, boolean flag)
    {
        localrecorderservice.telPauseFlag = flag;
        return flag;
    }

*/




/*
    static long access$302(LocalRecorderService localrecorderservice, long l)
    {
        localrecorderservice.startTime = l;
        return l;
    }

*/



/*
    static int access$404(LocalRecorderService localrecorderservice)
    {
        int i = localrecorderservice.mtime + 1;
        localrecorderservice.mtime = i;
        return i;
    }

*/

}
