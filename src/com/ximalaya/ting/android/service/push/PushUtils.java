// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.push;

import android.content.Context;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import java.util.Calendar;

public class PushUtils
{

    public static final String ACTION_LOGIN = "com.baidu.pushdemo.action.LOGIN";
    public static final String ACTION_MESSAGE = "com.baiud.pushdemo.action.MESSAGE";
    public static final String ACTION_RESPONSE = "bccsclient.action.RESPONSE";
    public static final String ACTION_SHOW_MESSAGE = "bccsclient.action.SHOW_MESSAGE";
    public static final String EXTRA_ACCESS_TOKEN = "access_token";
    public static final String EXTRA_MESSAGE = "message";
    public static final String RESPONSE_CONTENT = "content";
    public static final String RESPONSE_ERRCODE = "errcode";
    public static final String RESPONSE_METHOD = "method";
    public static final String TAG = "PushDemoActivity";

    public PushUtils()
    {
    }

    public static boolean canPush(Context context)
    {
        boolean flag1 = false;
        com/ximalaya/ting/android/service/push/PushUtils;
        JVM INSTR monitorenter ;
        boolean flag;
        context = SharedPreferencesUtil.getInstance(context);
        flag = context.getBoolean("is_push_all", true);
        if (flag) goto _L2; else goto _L1
_L1:
        flag = flag1;
_L4:
        com/ximalaya/ting/android/service/push/PushUtils;
        JVM INSTR monitorexit ;
        return flag;
_L2:
        if (!context.getBoolean("isPush", true))
        {
            flag = true;
            continue; /* Loop/switch isn't completed */
        }
        int ai[];
        int i;
        i = Calendar.getInstance().get(11);
        ai = new int[2];
        ai[0] = context.getInt("start", 22);
        ai[1] = context.getInt("end", 8);
        int j = (ai[0] + ai[1]) - 24;
        if (j > 0)
        {
            flag = flag1;
            if (i > ai[0] - 1)
            {
                continue; /* Loop/switch isn't completed */
            }
            flag = flag1;
            if (i < j)
            {
                continue; /* Loop/switch isn't completed */
            }
        }
        if (j < 0 && i > ai[0] - 1)
        {
            int k = ai[0];
            int l = ai[1];
            flag = flag1;
            if (i < k + l)
            {
                continue; /* Loop/switch isn't completed */
            }
        }
        flag = true;
        if (true) goto _L4; else goto _L3
_L3:
        context;
        throw context;
    }
}
