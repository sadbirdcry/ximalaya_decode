// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.push;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.a.d;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.AppConfig;
import com.ximalaya.ting.android.modelmanage.MsgManager;
import com.ximalaya.ting.android.modelmanage.PushStat;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.Random;
import java.util.Timer;

// Referenced classes of package com.ximalaya.ting.android.service.push:
//            PushUtils, PushModel, a, b, 
//            MsgCount, c

public class PushMessageReceiver extends BroadcastReceiver
{

    public static final int PUSH_BAIDU = 1;
    public static final int PUSH_GETUI = 0;
    public static final String TAG = "PushMessageReceiver";
    private MsgManager mMsgManager;

    public PushMessageReceiver()
    {
    }

    private void onGetMsg(Context context, Intent intent, String s, int i)
    {
        if (PushUtils.canPush(context)) goto _L2; else goto _L1
_L1:
        PushModel pushmodel;
        return;
_L2:
        if ((pushmodel = (PushModel)JSON.parseObject(s, com/ximalaya/ting/android/service/push/PushModel)) == null) goto _L1; else goto _L3
_L3:
        if (mMsgManager == null)
        {
            mMsgManager = MsgManager.getInstance(context);
        }
        if (mMsgManager.isShowed(pushmodel.bid)) goto _L1; else goto _L4
_L4:
        mMsgManager.addId(pushmodel.bid);
        if (AppConfig.getInstance().pushReceiveDelay <= 0) goto _L6; else goto _L5
_L5:
        int j = (new Random()).nextInt(AppConfig.getInstance().pushReceiveDelay) + 1;
_L17:
        if (i != 0) goto _L8; else goto _L7
_L7:
        ToolUtil.onEvent(context.getApplicationContext(), "PUSH_GETUI_REACH");
        intent = new PushStat(context, pushmodel.bid, pushmodel.msgId, "getui");
        intent.save();
        (new Timer()).schedule(new a(this, intent), j * 1000);
_L15:
        Object obj;
        Object obj1;
        intent = (NotificationManager)context.getSystemService("notification");
        obj = new Random(System.currentTimeMillis());
        j = ((Random) (obj)).nextInt();
        obj1 = new Intent();
        ((Intent) (obj1)).setClass(context, com/ximalaya/ting/android/activity/MainTabActivity2);
        ((Intent) (obj1)).setFlags(0x200000);
        ((Intent) (obj1)).putExtra("NOTIFICATION", true);
        ((Intent) (obj1)).putExtra("push_message", s);
        ((Intent) (obj1)).putExtra("push_provider", i);
        s = PendingIntent.getActivity(context, ((Random) (obj)).nextInt(), ((Intent) (obj1)), 0x8000000);
        obj = context.getString(0x7f090000);
        obj1 = pushmodel.message;
        pushmodel.messageType;
        JVM INSTR lookupswitch 6: default 747
    //                   10: 567
    //                   15: 509
    //                   16: 451
    //                   17: 625
    //                   18: 683
    //                   25: 683;
           goto _L9 _L10 _L11 _L12 _L13 _L14 _L14
_L9:
        obj = pushmodel.message;
        obj1 = pushmodel.message;
        intent.notify(j, ToolUtil.createNotification(context, pushmodel.title, ((String) (obj)), ((String) (obj1)), s));
        return;
_L8:
        if (i == 1)
        {
            try
            {
                ToolUtil.onEvent(context.getApplicationContext(), "PUSH_BAIDU_REACH");
                intent = new PushStat(context, pushmodel.bid, pushmodel.msgId, "baidu");
                intent.save();
                (new Timer()).schedule(new b(this, intent), j * 1000);
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                Logger.e("\u63A8\u9001\u89E3\u6790\u5F02\u5E38", context.getMessage());
                return;
            }
        }
          goto _L15
_L12:
        StringBuilder stringbuilder = (new StringBuilder()).append("\u4F60\u65B0\u589E\u4E86");
        i = MsgCount.sCount_Fans + 1;
        MsgCount.sCount_Fans = i;
        intent.notify(4096, ToolUtil.createNotification(context, ((String) (obj)), ((String) (obj1)), stringbuilder.append(i).append("\u4E2A\u7C89\u4E1D").toString(), s));
        return;
_L11:
        StringBuilder stringbuilder1 = (new StringBuilder()).append("\u4F60\u6536\u5230\u4E86");
        i = MsgCount.sCount_Comments + 1;
        MsgCount.sCount_Comments = i;
        intent.notify(4098, ToolUtil.createNotification(context, ((String) (obj)), ((String) (obj1)), stringbuilder1.append(i).append("\u6761\u8BC4\u8BBA").toString(), s));
        return;
_L10:
        StringBuilder stringbuilder2 = (new StringBuilder()).append("\u4F60\u6536\u5230\u4E86");
        i = MsgCount.sCount_Letter + 1;
        MsgCount.sCount_Letter = i;
        intent.notify(4097, ToolUtil.createNotification(context, ((String) (obj)), ((String) (obj1)), stringbuilder2.append(i).append("\u6761\u79C1\u4FE1").toString(), s));
        return;
_L13:
        StringBuilder stringbuilder3 = (new StringBuilder()).append("\u4F60\u6709");
        i = MsgCount.sCount_Events + 1;
        MsgCount.sCount_Events = i;
        intent.notify(4099, ToolUtil.createNotification(context, ((String) (obj)), ((String) (obj1)), stringbuilder3.append(i).append("\u6761\u65B0\u9C9C\u4E8B").toString(), s));
        return;
_L14:
        StringBuilder stringbuilder4 = (new StringBuilder()).append("\u4F60\u6536\u5230\u4E86");
        i = MsgCount.sCount_ZoneComments + 1;
        MsgCount.sCount_ZoneComments = i;
        intent.notify(4098, ToolUtil.createNotification(context, ((String) (obj)), ((String) (obj1)), stringbuilder4.append(i).append("\u6761\u5708\u5B50\u56DE\u590D").toString(), s));
        return;
_L6:
        j = 0;
        if (true) goto _L17; else goto _L16
_L16:
    }

    public void onReceive(Context context, Intent intent)
    {
        Logger.d("PushMessageReceiver", (new StringBuilder()).append(">>> Receive intent: \r\n").append(intent).toString());
        if (intent.getAction().equals(d.c) && PushUtils.canPush(context)) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj = intent.getExtras();
        Log.d("PushMessageReceiver", (new StringBuilder()).append("onReceive() action=").append(((Bundle) (obj)).getInt("action")).toString());
        switch (((Bundle) (obj)).getInt("action"))
        {
        default:
            return;

        case 10001: 
            obj = ((Bundle) (obj)).getByteArray("payload");
            if (obj != null)
            {
                obj = new String(((byte []) (obj)));
                Log.d("PushMessageReceiver", (new StringBuilder()).append("\u83B7\u53D6\u4E2A\u63A8\u4FE1\u606F:").append(((String) (obj))).toString());
                onGetMsg(context, intent, ((String) (obj)), 0);
                return;
            }
            break;

        case 10002: 
            intent = ((Bundle) (obj)).getString("clientid");
            SharedPreferencesUtil.getInstance(context).saveString("gettui_client_id", intent);
            (new c(this, context, intent)).myexec(new Object[0]);
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
    }
}
