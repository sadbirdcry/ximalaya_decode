// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.sound.AlbumSoundModel;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            Playlist

class a extends Playlist
{

    private int a;
    private int b;

    public a(List list)
    {
        super(list);
        b = 20;
        if (mParams == null)
        {
            break MISSING_BLOCK_LABEL_39;
        }
        list = (String)mParams.get("pageSize");
        b = Integer.parseInt(list);
        return;
        list;
        list.printStackTrace();
        return;
    }

    public boolean hasMore()
    {
        for (int i = mPageId - 1; i == 0 || i == 1 || a <= 0 || i * b < a;)
        {
            return true;
        }

        return false;
    }

    public List loadMore()
    {
        if (mParams == null)
        {
            return null;
        }
        Object obj;
        List list;
        if ((new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/my/album/track").toString().equals(mDataSourceUrl))
        {
            obj = new RequestParams();
            String s;
            for (Iterator iterator = mParams.keySet().iterator(); iterator.hasNext(); ((RequestParams) (obj)).put(s, (String)mParams.get(s)))
            {
                s = (String)iterator.next();
            }

            ((RequestParams) (obj)).put("pageId", mPageId);
            obj = f.a().a(mDataSourceUrl, ((RequestParams) (obj)), false);
        } else
        {
            obj = (new StringBuilder()).append(mDataSourceUrl).append((String)mParams.get("albumId")).append("/").append((String)mParams.get("isAsc")).append("/").append(mPageId).append("/").append(b).toString();
            obj = f.a().a(((String) (obj)), null, false);
        }
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            break MISSING_BLOCK_LABEL_344;
        }
        obj = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
        if (!"0".equals(((JSONObject) (obj)).get("ret").toString()))
        {
            break MISSING_BLOCK_LABEL_344;
        }
        obj = ((JSONObject) (obj)).getJSONObject("tracks");
        list = JSON.parseArray(((JSONObject) (obj)).getString("list"), com/ximalaya/ting/android/model/sound/AlbumSoundModel);
        if (a == 0)
        {
            a = ((JSONObject) (obj)).getIntValue("totalCount");
        }
        if (list == null)
        {
            break MISSING_BLOCK_LABEL_344;
        }
        mPageId = mPageId + 1;
        obj = ModelHelper.albumSoundlistToSoundInfoList(list);
        return ((List) (obj));
        Exception exception;
        exception;
        Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
        return null;
    }
}
