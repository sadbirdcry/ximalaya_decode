// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.content.Context;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import java.util.HashMap;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            PlayTools, LocalMediaService

final class i
    implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
{

    final boolean a;
    final SoundInfo b;
    final Context c;
    final String d;
    final LocalMediaService e;
    final String f;
    final int g;
    final HashMap h;
    final int i;
    final List j;
    final int k;
    final boolean l;

    i(boolean flag, SoundInfo soundinfo, Context context, String s, LocalMediaService localmediaservice, String s1, int i1, 
            HashMap hashmap, int j1, List list, int k1, boolean flag1)
    {
        a = flag;
        b = soundinfo;
        c = context;
        d = s;
        e = localmediaservice;
        f = s1;
        g = i1;
        h = hashmap;
        i = j1;
        j = list;
        k = k1;
        l = flag1;
        super();
    }

    public void onExecute()
    {
        if (a)
        {
            if (b.category == 0)
            {
                PlayTools.startPlayerActivity(c, d);
            } else
            {
                PlayTools.startPlayerActivity(c, true, d);
            }
        }
        e.doPlayAndUpdatePlaylist(f, g, h, i, j, k, l);
    }
}
