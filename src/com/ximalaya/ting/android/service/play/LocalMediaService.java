// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import com.alibaba.fastjson.JSON;
import com.baidu.alliance.audio.SDKManager;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.DummyActivity;
import com.ximalaya.ting.android.communication.DownLoadTools;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.HistoryManage;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MemorySpaceUtil;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.NotifyDialogUtil;
import com.ximalaya.ting.android.util.StorageUtils;
import com.ximalaya.ting.android.util.Utilities;
import java.io.File;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            PlayListControl, Playlist, PlayTools, TingMediaPlayer, 
//            d, e, h, k, 
//            SoundListenRecord, c

public class LocalMediaService extends Service
{
    public class LocalBinder extends Binder
    {

        final LocalMediaService this$0;

        public LocalMediaService getService()
        {
            return LocalMediaService.this;
        }

        public LocalBinder()
        {
            this$0 = LocalMediaService.this;
            super();
        }
    }

    public static interface OnPlayServiceUpdateListener
    {

        public abstract void onPlayCanceled();

        public abstract void onSoundChanged(int i);

        public abstract void onSoundInfoChanged(int i, SoundInfo soundinfo);
    }


    public static final int MAX_WAIT_TIME_START_SERVICE = 10;
    private static final Object PlayServiceUpdateListenerListLock = new Object();
    private static LocalMediaService service = null;
    private Context appContext;
    private boolean canSwitch;
    public long lastPlayTrackID;
    private final IBinder mBinder = new LocalBinder();
    private boolean mFirst;
    public List mPlayServiceUpdateListenerList;
    private Timer mTimer;
    private TingMediaPlayer mp;
    private PlayListControl playListManager;

    public LocalMediaService()
    {
        mp = null;
        playListManager = null;
        mFirst = true;
        canSwitch = true;
        lastPlayTrackID = -1L;
        mPlayServiceUpdateListenerList = new ArrayList();
    }

    private void doActionPlayNext()
    {
        if (mFirst && (playListManager.getPlaylist() == null || playListManager.getPlaylist().size() == 0))
        {
            PlayTools.initPlayList(appContext, false);
        }
        if (playListManager.getPlaylist() == null || playListManager.getPlaylist().size() == 0)
        {
            PlayTools.startMainApp(appContext);
            return;
        } else
        {
            playNext(true);
            return;
        }
    }

    private void doActionPlayOrPause()
    {
        Log.e("", (new StringBuilder()).append("Xm doActionPlayOrPause ").append(mFirst).toString());
        if (mFirst && (playListManager.getPlaylist() == null || playListManager.getPlaylist().size() == 0))
        {
            PlayTools.initPlayList(appContext, true);
        }
        if (playListManager.getPlaylist() == null || playListManager.getPlaylist().size() == 0)
        {
            PlayTools.startMainApp(appContext);
            return;
        }
        int i = getPlayServiceState();
        Log.e("", (new StringBuilder()).append("Xm doActionPlayOrPause status ").append(i).toString());
        switch (i)
        {
        default:
            return;

        case 0: // '\0'
            CustomToast.showToast(appContext, "\u6CA1\u6709\u64AD\u653E\u5386\u53F2", 0);
            return;

        case 1: // '\001'
        case 3: // '\003'
            pause();
            return;

        case 2: // '\002'
            start();
            break;
        }
    }

    private void doActionPlayPre()
    {
        if (mFirst && (playListManager.getPlaylist() == null || playListManager.getPlaylist().size() == 0))
        {
            PlayTools.initPlayList(appContext, false);
        }
        if (playListManager.getPlaylist() == null || playListManager.getPlaylist().size() == 0)
        {
            PlayTools.startMainApp(appContext);
            return;
        } else
        {
            playPrev(true);
            return;
        }
    }

    private boolean doPlayAndStatistics(int i, long l, long l1, int j, int i1)
    {
        SoundInfo soundinfo = playListManager.getCurSound();
        if (playListManager.swtichTo(i)) goto _L2; else goto _L1
_L1:
        boolean flag = false;
_L4:
        return flag;
_L2:
        boolean flag1;
        flag1 = doPlay();
        flag = flag1;
        if (!flag1) goto _L4; else goto _L3
_L3:
        flag = flag1;
        if (l <= 0L) goto _L4; else goto _L5
_L5:
        flag = flag1;
        if (l1 <= 0L) goto _L4; else goto _L6
_L6:
        if (l != l1)
        {
            break; /* Loop/switch isn't completed */
        }
        flag = flag1;
        if (mp.mediaPlayerState != 7) goto _L4; else goto _L7
_L7:
        Intent intent = new Intent();
        intent.addFlags(0x10000000);
        intent.setAction("ximalaya.action.player.CHANGE_SOUND");
        intent.putExtra("position1", j);
        intent.putExtra("duration1", i1);
        intent.putExtra("trackId1", l);
        intent.putExtra("trackId2", l1);
        if (soundinfo != null)
        {
            intent.putExtra("ACTION_EXTRA_SOUNDINFO", JSON.toJSONString(soundinfo));
        }
        sendBroadcast(intent);
        return flag1;
    }

    private boolean doPlayAndStatistics(int i, long l, long l1, int j, int i1, 
            long l2, long l3, int j1)
    {
        SoundInfo soundinfo = playListManager.getCurSound();
        if (playListManager.swtichTo(i)) goto _L2; else goto _L1
_L1:
        boolean flag = false;
_L4:
        return flag;
_L2:
        boolean flag1;
        flag1 = doPlay();
        flag = flag1;
        if (!flag1) goto _L4; else goto _L3
_L3:
        if (l > 0L)
        {
            break; /* Loop/switch isn't completed */
        }
        flag = flag1;
        if (l2 <= 0L) goto _L4; else goto _L5
_L5:
        if (l != l1)
        {
            break; /* Loop/switch isn't completed */
        }
        flag = flag1;
        if (l2 == l3) goto _L4; else goto _L6
_L6:
        Intent intent = new Intent();
        intent.addFlags(0x10000000);
        intent.setAction("ximalaya.action.player.CHANGE_SOUND");
        intent.putExtra("position1", j);
        intent.putExtra("duration1", i1);
        intent.putExtra("trackId1", l);
        intent.putExtra("trackId2", l1);
        intent.putExtra("programScheduleId1", l2);
        intent.putExtra("programScheduleId2", l3);
        intent.putExtra("category1", j1);
        if (soundinfo != null)
        {
            intent.putExtra("ACTION_EXTRA_SOUNDINFO", JSON.toJSONString(soundinfo));
        }
        sendBroadcast(intent);
        return flag1;
    }

    public static LocalMediaService getInstance()
    {
        return service;
    }

    public static Intent getIntent(Context context)
    {
        return new Intent(context, com/ximalaya/ting/android/service/play/LocalMediaService);
    }

    private void installPlayer(SoundInfo soundinfo, String s, boolean flag, boolean flag1)
    {
        if (XiMaoBTManager.getInstance(appContext).isConnected() && XiMaoBTManager.getInstance(appContext).isAudio())
        {
            CustomToast.showToast(appContext, "\u6B63\u5728\u4E0A\u4F20\u667A\u80FD\u8BED\u97F3\uFF0C\u8BF7\u7A0D\u540E\u64AD\u653E", 0);
            return;
        }
        if (XiMaoBTManager.getInstance(appContext).isConnected())
        {
            XiMaoComm.requestA2DPCon(appContext);
        }
        lastPlayTrackID = soundinfo.trackId;
        PlayListControl.getPlayListManager().curPlaySrc = s;
        updateOnSoundChanged();
        flag1 = isProxyAvaliable(soundinfo);
        soundinfo.history_listener = HistoryManage.getInstance(appContext).getHistoryListenerPosition(soundinfo.trackId);
        mp.installAudioSource(s, soundinfo.history_listener, flag, flag1);
    }

    private boolean isSwitchTooFast()
    {
        if (canSwitch)
        {
            canSwitch = false;
            if (mTimer != null)
            {
                mTimer.cancel();
            }
            mTimer = new Timer();
            mTimer.schedule(new d(this), 2000L);
            return false;
        } else
        {
            return true;
        }
    }

    public static final void releaseOnExit()
    {
        if (service != null)
        {
            if (service.mp != null && service.mp.mPlayerStatusUpdateListenerList != null)
            {
                service.mp.mPlayerStatusUpdateListenerList.clear();
                service.mp.mPlayerStatusUpdateListenerList = null;
            }
            if (service.mPlayServiceUpdateListenerList != null)
            {
                for (; service.mPlayServiceUpdateListenerList.size() > 0; ((OnPlayServiceUpdateListener)service.mPlayServiceUpdateListenerList.remove(0)).onPlayCanceled()) { }
                service.mPlayServiceUpdateListenerList = null;
            }
            service.stopPlayTask();
            service.stopSelf();
            service = null;
        }
    }

    public static final void releaseOnLogout()
    {
        if (service == null)
        {
            return;
        } else
        {
            service.stopPlayTask();
            return;
        }
    }

    private void showToast(String s)
    {
        if (!isForegroundIsMyApplication())
        {
            CustomToast.showToast(appContext, s, 0);
        }
    }

    public static final boolean startLocalMediaService(Context context)
    {
        return startLocalMediaService(context, false);
    }

    public static final boolean startLocalMediaService(Context context, boolean flag)
    {
        context.startService(getIntent(context));
        if (!flag) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        int i = 0;
_L4:
        Thread.sleep(200L, 0);
        context = getInstance();
        if (context == null && i < 10)
        {
            break MISSING_BLOCK_LABEL_75;
        }
_L3:
        if (getInstance() != null)
        {
            return true;
        }
          goto _L1
        context;
        Logger.throwRuntimeException((new StringBuilder()).append("loading\u9875\u9762\u542F\u52A8service\u5931\u8D25").append(Logger.getLineInfo()).toString());
          goto _L3
        i++;
          goto _L4
    }

    public void addSoundPatchAdCallback(TingMediaPlayer.SoundAdCallback soundadcallback)
    {
        if (mp != null)
        {
            mp.addSoundAdCallback(soundadcallback);
        }
    }

    public boolean doPlay()
    {
        return doPlay(true);
    }

    public final boolean doPlay(int i)
    {
        Object obj;
        int j;
        int l;
        int i1;
        int j1;
        long l1;
        long l2;
        long l3;
        long l4;
        boolean flag;
        if (i < 0)
        {
            return false;
        }
        l1 = lastPlayTrackID;
        l3 = -1L;
        j = 0;
        l = 0;
        l2 = 0L;
        l4 = 0L;
        i1 = 0;
        obj = PlayListControl.getPlayListManager().getCurSound();
        if (obj != null)
        {
            l1 = ((SoundInfo) (obj)).trackId;
            j = getCurPositionAssist();
            l = getDurationAssist();
            l2 = ((SoundInfo) (obj)).programScheduleId;
            i1 = ((SoundInfo) (obj)).category;
        }
        obj = PlayListControl.getPlayListManager().get(i);
        if (obj != null)
        {
            l3 = ((SoundInfo) (obj)).trackId;
            l4 = ((SoundInfo) (obj)).programScheduleId;
        }
        obj = PlayListControl.getAudioSourceForPlayer(((SoundInfo) (obj)));
        j1 = NetworkUtils.getNetType(appContext);
        flag = SharedPreferencesUtil.getInstance(appContext).getBoolean("is_download_enabled_in_3g", false);
        if (1 != j1 && !PlayTools.isLocalAudioSource(((String) (obj)))) goto _L2; else goto _L1
_L1:
        doPlayAndStatistics(i, l1, l3, j, l, l2, l4, i1);
_L4:
        return false;
_L2:
        if (MyApplication.a() != null)
        {
            if (!flag)
            {
                if (!isForegroundIsMyApplication())
                {
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    intent.setData(Uri.parse("iting://open"));
                    intent.addFlags(0x10000000);
                    MyApplication.a().startActivity(intent);
                }
                NetworkUtils.showChangeNetWorkSetConfirm(new e(this, i, l1, l3, j, l, l2, l4, i1), null);
                showToast("\u559C\u9A6C\u62C9\u96C5\u5C06\u8981\u4F7F\u7528\u4F60\u7684\u79FB\u52A8\u6D41\u91CF\u64AD\u653E\u58F0\u97F3\uFF0C\u8BF7\u6253\u5F00\u7A0B\u5E8F\u70B9\u51FB\u786E\u8BA4");
            } else
            {
                doPlayAndStatistics(i, l1, l3, j, l, l2, l4, i1);
            }
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public boolean doPlay(boolean flag)
    {
        boolean flag1 = true;
        SoundInfo soundinfo = playListManager.getCurSound();
        Object obj;
        if (soundinfo != null)
        {
            if (!Utilities.isBlank(((String) (obj = PlayListControl.getAudioSourceForPlayer(soundinfo)))))
            {
                if (!PlayTools.isLocalAudioSource(((String) (obj))) && DownloadHandler.getInstance(appContext).isDownloadCompleted(soundinfo))
                {
                    Logger.logToSd((new StringBuilder()).append("<<<<LocalFileMissed>>>>>, Play path: ").append(((String) (obj))).toString());
                    Logger.logToSd((new StringBuilder()).append("<<<<LocalFileMissed>>>>>, SoundInfo: ").append(soundinfo).toString());
                    obj = DownloadHandler.getInstance(MyApplication.b());
                    if (obj != null && ((DownloadHandler) (obj)).isDownloadCompleted(soundinfo))
                    {
                        obj = ((DownloadHandler) (obj)).getDownloadFile(soundinfo);
                        StringBuilder stringbuilder = (new StringBuilder()).append("<<<<LocalFileMissed>>>>>, local file exist : ");
                        if (((File) (obj)).exists())
                        {
                            flag = flag1;
                        } else
                        {
                            flag = false;
                        }
                        Logger.logToSd(stringbuilder.append(flag).toString());
                    }
                    DownLoadTools.showDialogOnDownloadFileMiss(soundinfo);
                    if (mp != null)
                    {
                        mp.stopPlayAd();
                        return false;
                    }
                } else
                if (Utilities.isNotBlank(PlayListControl.getPlayListManager().curPlaySrc) && PlayListControl.getPlayListManager().curPlaySrc.equalsIgnoreCase(((String) (obj))))
                {
                    return true;
                } else
                {
                    installPlayer(soundinfo, ((String) (obj)), flag, true);
                    return true;
                }
            }
        }
        return false;
    }

    public final boolean doPlayAndUpdatePlaylist(String s, int i, HashMap hashmap, int j, List list, int l)
    {
        return doPlayAndUpdatePlaylist(s, i, hashmap, j, list, l, true);
    }

    public final boolean doPlayAndUpdatePlaylist(String s, int i, HashMap hashmap, int j, List list, int l, boolean flag)
    {
        if (list == null || list.size() <= 0 || l < 0 || l >= list.size())
        {
            return false;
        }
        PlayListControl playlistcontrol = PlayListControl.getPlayListManager();
        long l3 = lastPlayTrackID;
        int k1 = 0;
        int j1 = 0;
        long l2 = 0L;
        int i1 = 0;
        SoundInfo soundinfo = playlistcontrol.getCurSound();
        SoundInfo soundinfo1 = (SoundInfo)list.get(l);
        if (soundinfo != null && soundinfo1.category != 0 && soundinfo1.radioId == soundinfo.radioId)
        {
            return true;
        }
        if (soundinfo != null)
        {
            l3 = soundinfo.trackId;
            k1 = getCurPositionAssist();
            i1 = getDurationAssist();
            JSON.toJSONString(soundinfo);
            l2 = soundinfo.programScheduleId;
            int l1 = soundinfo.category;
            j1 = i1;
            i1 = l1;
        }
        if (!isMediaPlayerPaused() && !isPlayNormal() || !((SoundInfo)list.get(l)).equals(soundinfo))
        {
            stopPlayTask();
        }
        playlistcontrol.makePlaylist(list, s, i, hashmap);
        playlistcontrol.listType = j;
        if (playlistcontrol.details.size() + list.size() > 50)
        {
            playlistcontrol.details.clear();
        }
        if (playListManager.swtichTo(l))
        {
            s = playlistcontrol.getCurSound();
            long l4;
            long l5;
            if (s != null)
            {
                l5 = ((SoundInfo) (s)).trackId;
                l4 = ((SoundInfo) (s)).programScheduleId;
                Logger.log("SWITCH", (new StringBuilder()).append("=").append(playlistcontrol.curIndex).append("========CurTrackId: ").append(l5).toString());
            } else
            {
                l4 = 0L;
                l5 = -1L;
            }
            if (flag && (l3 > 0L || l2 > 0L) && (l3 != l5 || l2 != l4 || mp.mediaPlayerState == 7))
            {
                s = new Intent();
                s.addFlags(0x10000000);
                s.setAction("ximalaya.action.player.CHANGE_SOUND");
                s.putExtra("position1", k1);
                s.putExtra("duration1", j1);
                s.putExtra("trackId1", l3);
                s.putExtra("trackId2", l5);
                s.putExtra("programScheduleId1", l2);
                s.putExtra("programScheduleId2", l4);
                s.putExtra("category1", i1);
                if (soundinfo != null)
                {
                    s.putExtra("ACTION_EXTRA_SOUNDINFO", JSON.toJSONString(soundinfo));
                }
                sendBroadcast(s);
            }
            return doPlay(flag);
        } else
        {
            return false;
        }
    }

    public final int getBufferredPercent()
    {
        if (mp == null || PlayListControl.getPlayListManager().curPlaySrc == null)
        {
            return 0;
        }
        if (!PlayListControl.getPlayListManager().curPlaySrc.contains("http"))
        {
            return 100;
        } else
        {
            return mp.bufferingPercent;
        }
    }

    public final int getCurPosition()
    {
        if (mp == null)
        {
            return 0;
        } else
        {
            return mp.getCurrentPosition();
        }
    }

    public final int getCurPositionAssist()
    {
        if (mp == null)
        {
            return 0;
        } else
        {
            return mp.mCurrentPosition;
        }
    }

    public final String getCurrentUrl()
    {
        Object obj = null;
        String s = obj;
        if (playListManager != null)
        {
            SoundInfo soundinfo = playListManager.getCurSound();
            s = obj;
            if (soundinfo != null)
            {
                s = soundinfo.playUrl64;
            }
        }
        return s;
    }

    public final int getDuration()
    {
        if (mp == null)
        {
            return 0;
        } else
        {
            return mp.getDuration();
        }
    }

    public final int getDurationAssist()
    {
        if (mp == null)
        {
            return 0;
        } else
        {
            return mp.mDuration;
        }
    }

    public final int getMediaPlayerState()
    {
        if (mp == null)
        {
            return 0;
        } else
        {
            return mp.mediaPlayerState;
        }
    }

    public int getPlayServiceState()
    {
        if (PlayListControl.getPlayListManager().getPlaylist() == null || PlayListControl.getPlayListManager().getPlaylist().size() == 0)
        {
            return 0;
        }
        if (isPlayNormal() && isStarted())
        {
            return 1;
        }
        return !isPlayingAds() ? 2 : 3;
    }

    public boolean hasValidatePlayUrl()
    {
        if (playListManager != null)
        {
            SoundInfo soundinfo = playListManager.getCurSound();
            if (soundinfo != null)
            {
                while (!TextUtils.isEmpty(soundinfo.playUrl64) || !TextUtils.isEmpty(PlayListControl.getAudioSourceForPlayer(soundinfo))) 
                {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    public boolean isEverPlayed()
    {
        if (mp == null)
        {
            return false;
        } else
        {
            return mp.isEverPlayed();
        }
    }

    public boolean isForegroundIsMyApplication()
    {
        if (appContext == null)
        {
            return false;
        }
        ActivityManager activitymanager = (ActivityManager)appContext.getSystemService("activity");
        if (activitymanager == null)
        {
            return false;
        }
        return ((android.app.ActivityManager.RunningTaskInfo)activitymanager.getRunningTasks(1).get(0)).topActivity.getPackageName().equals(appContext.getPackageName());
    }

    public final boolean isMediaPlayerPaused()
    {
        boolean flag1 = false;
        boolean flag = flag1;
        if (mp != null)
        {
            flag = flag1;
            if (mp.mediaPlayerState == 5)
            {
                flag = true;
            }
        }
        return flag;
    }

    public final boolean isNetworkAvaliable()
    {
        return NetworkUtils.isNetworkAvaliable(MyApplication.b());
    }

    public final boolean isNowPlayingAndLocal()
    {
        return isPlayNormal() && Utilities.isNotBlank(PlayListControl.getPlayListManager().curPlaySrc) && !PlayListControl.getPlayListManager().curPlaySrc.contains("http");
    }

    public final boolean isNowPlayingAndOnline()
    {
        return isPlayNormal() && Utilities.isNotBlank(PlayListControl.getPlayListManager().curPlaySrc) && PlayListControl.getPlayListManager().curPlaySrc.contains("http");
    }

    public boolean isPaused()
    {
        return mp.mediaPlayerState == 5 || mp.mediaPlayerState == 3;
    }

    public boolean isPlayCompleted()
    {
        return mp.mediaPlayerState == 7;
    }

    public final boolean isPlayFromLocal()
    {
        return Utilities.isNotBlank(PlayListControl.getPlayListManager().curPlaySrc) && !PlayListControl.getPlayListManager().curPlaySrc.contains("http");
    }

    public final boolean isPlayFromNetwork()
    {
        return Utilities.isNotBlank(PlayListControl.getPlayListManager().curPlaySrc) && PlayListControl.getPlayListManager().curPlaySrc.contains("http");
    }

    public boolean isPlayNormal()
    {
        boolean flag = false;
        if (mp != null)
        {
            flag = mp.isPlayNormal();
        }
        return flag;
    }

    public boolean isPlaying()
    {
        return mp.isPlaying();
    }

    public boolean isPlayingAds()
    {
        boolean flag = false;
        if (mp != null)
        {
            flag = mp.isPlayingAds;
        }
        return flag;
    }

    public boolean isProxyAvaliable(SoundInfo soundinfo)
    {
        if (soundinfo != null)
        {
            double d1 = soundinfo.duration;
            if (!MemorySpaceUtil.isExternalMemoryAvailable())
            {
                return false;
            }
            if (8192D * d1 >= StorageUtils.getInternalSDcardFreeSize())
            {
                NotifyDialogUtil.showDialogOnSpaceShortage();
                return false;
            }
        }
        return true;
    }

    public boolean isStarted()
    {
        return mp.mediaPlayerState == 4;
    }

    public IBinder onBind(Intent intent)
    {
        return mBinder;
    }

    public void onCreate()
    {
        super.onCreate();
        appContext = getApplicationContext();
        mp = TingMediaPlayer.getTingMediaPlayer(this);
        service = this;
        playListManager = PlayListControl.getPlayListManager();
        lastPlayTrackID = -1L;
        setOnPlayerStatusUpdateListener(new h(appContext));
        setOnPlayServiceUpdateListener(new k(appContext));
        MyApplication.e(appContext);
    }

    public void onDestroy()
    {
        super.onDestroy();
        stop();
        lastPlayTrackID = -1L;
        mp.release();
        stopForeground(true);
        SDKManager.getInstance().release();
        appContext = null;
    }

    public int onStartCommand(Intent intent, int i, int j)
    {
        String s;
        SoundListenRecord.getSoundListenRecord(appContext);
        if (intent == null)
        {
            return super.onStartCommand(intent, i, j);
        }
        s = intent.getAction();
        if (!"com.ximalaya.ting.android.ACTION_CONTROL_PLAY_OR_PAUSE".equals(s)) goto _L2; else goto _L1
_L1:
        doActionPlayOrPause();
_L4:
        mFirst = false;
        return super.onStartCommand(intent, i, j);
_L2:
        if ("com.ximalaya.ting.android.ACTION_CONTROL_PLAY_PRE".equals(s))
        {
            doActionPlayPre();
        } else
        if ("com.ximalaya.ting.android.ACTION_CONTROL_PLAY_NEXT".equals(s))
        {
            doActionPlayNext();
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void onTaskRemoved(Intent intent)
    {
        intent = new Intent(this, com/ximalaya/ting/android/activity/DummyActivity);
        intent.addFlags(0x10000000);
        startActivity(intent);
    }

    public void pause()
    {
        while (isPlayingAds() || mp == null) 
        {
            return;
        }
        if (mp.isPlayingSoundAd())
        {
            mp.pausePlaySoundAd();
            return;
        } else
        {
            mp.pause();
            return;
        }
    }

    public final int playNext(boolean flag)
    {
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        int i;
        if (soundinfo != null && soundinfo.category == 1 && flag)
        {
            i = PlayListControl.getPlayListManager().curIndex;
        } else
        {
            if (isSwitchTooFast())
            {
                return PlayListControl.getPlayListManager().curIndex;
            }
            int j = PlayListControl.getPlayListManager().switchNext(flag);
            if (j >= 0)
            {
                doPlay(j);
                return j;
            }
            i = j;
            if (PlayListControl.getPlayListManager().getPlaylist().hasMore())
            {
                (new c(this, flag)).myexec(new Void[0]);
                return j;
            }
        }
        return i;
    }

    public final int playPrev(boolean flag)
    {
        int i;
        if (isSwitchTooFast())
        {
            i = PlayListControl.getPlayListManager().curIndex;
        } else
        {
            int j = playListManager.switchPrev(flag);
            i = j;
            if (j >= 0)
            {
                doPlay(j);
                return j;
            }
        }
        return i;
    }

    public void removeLiveSoundUpdateCallback()
    {
        if (mp != null)
        {
            mp.removeLiveSoundUpdateCallback();
        }
    }

    public void removeOnPlayServiceUpdateListener(OnPlayServiceUpdateListener onplayserviceupdatelistener)
    {
        while (mPlayServiceUpdateListenerList == null || onplayserviceupdatelistener == null || mPlayServiceUpdateListenerList == null) 
        {
            return;
        }
        synchronized (PlayServiceUpdateListenerListLock)
        {
            if (mPlayServiceUpdateListenerList != null)
            {
                mPlayServiceUpdateListenerList.remove(onplayserviceupdatelistener);
                Logger.log((new StringBuilder()).append("\u79FB\u9664mPlayServiceUpdateListenerList\u76D1\u542C\uFF1A").append(mPlayServiceUpdateListenerList.contains(onplayserviceupdatelistener)).toString());
            }
        }
        return;
        onplayserviceupdatelistener;
        obj;
        JVM INSTR monitorexit ;
        throw onplayserviceupdatelistener;
    }

    public void removeOnPlayerUpdateListener(TingMediaPlayer.OnPlayerStatusUpdateListener onplayerstatusupdatelistener)
    {
        if (mp == null || mp.mPlayerStatusUpdateListenerList == null || onplayerstatusupdatelistener == null)
        {
            return;
        } else
        {
            mp.mPlayerStatusUpdateListenerList.remove(onplayerstatusupdatelistener);
            return;
        }
    }

    public void removeSoundPatchAdCallback(TingMediaPlayer.SoundAdCallback soundadcallback)
    {
        if (mp != null)
        {
            mp.removeSoundAdCallback(soundadcallback);
        }
    }

    public void replay()
    {
        if (mp != null)
        {
            mp.replay();
        }
    }

    public void restart()
    {
        if (mp.mediaPlayerState == 8 || mp.mediaPlayerState == 2)
        {
            PlayListControl.getPlayListManager().curPlaySrc = "";
            doPlay();
        }
    }

    public final boolean seekToPosition(int i)
    {
        boolean flag1 = false;
        boolean flag = flag1;
        if (mp != null)
        {
            Logger.logToSd((new StringBuilder()).append("seekToPosition position:").append(i).toString());
            flag = flag1;
            if (mp.seekTo(i) == 0)
            {
                flag = true;
            }
        }
        return flag;
    }

    public final boolean seekToProgress(int i, int j)
    {
        boolean flag1 = false;
        boolean flag = flag1;
        if (mp != null)
        {
            flag = flag1;
            if (mp.seekToProgress(i, j) == 0)
            {
                flag = true;
            }
        }
        return flag;
    }

    public void setLiveSoundUpdateCallback(TingMediaPlayer.LiveSoundUpdateCallback livesoundupdatecallback)
    {
        if (mp != null)
        {
            mp.setLiveSoundUpdateCallback(livesoundupdatecallback);
        }
    }

    public void setMediaPlayerCompleteHandler(TingMediaPlayer.MediaPlayerCompleteHandler mediaplayercompletehandler)
    {
        if (mp == null)
        {
            return;
        } else
        {
            mp.setCompleteHandler(mediaplayercompletehandler);
            return;
        }
    }

    public void setMediaPlayerErrorHandler(TingMediaPlayer.MediaPlayerErrorHandler mediaplayererrorhandler)
    {
        if (mp == null)
        {
            return;
        } else
        {
            mp.setErrorHandler(mediaplayererrorhandler);
            return;
        }
    }

    public void setOnPlayServiceUpdateListener(OnPlayServiceUpdateListener onplayserviceupdatelistener)
    {
        while (mPlayServiceUpdateListenerList == null || onplayserviceupdatelistener == null || mPlayServiceUpdateListenerList == null) 
        {
            return;
        }
        synchronized (PlayServiceUpdateListenerListLock)
        {
            if (mPlayServiceUpdateListenerList != null)
            {
                if (!mPlayServiceUpdateListenerList.contains(onplayserviceupdatelistener))
                {
                    mPlayServiceUpdateListenerList.add(onplayserviceupdatelistener);
                }
                Logger.log((new StringBuilder()).append("\u6DFB\u52A0mPlayServiceUpdateListenerList\u76D1\u542C\uFF1A").append(mPlayServiceUpdateListenerList.contains(onplayserviceupdatelistener)).toString());
            }
        }
        return;
        onplayserviceupdatelistener;
        obj;
        JVM INSTR monitorexit ;
        throw onplayserviceupdatelistener;
    }

    public void setOnPlayerStatusUpdateListener(TingMediaPlayer.OnPlayerStatusUpdateListener onplayerstatusupdatelistener)
    {
        while (mp == null || mp.mPlayerStatusUpdateListenerList == null || onplayerstatusupdatelistener == null || mp.mPlayerStatusUpdateListenerList.contains(onplayerstatusupdatelistener)) 
        {
            return;
        }
        mp.mPlayerStatusUpdateListenerList.add(onplayerstatusupdatelistener);
    }

    public void start()
    {
        if (XiMaoBTManager.getInstance(appContext).isConnected() && XiMaoBTManager.getInstance(appContext).isAudio())
        {
            CustomToast.showToast(appContext, "\u6B63\u5728\u4E0A\u4F20\u667A\u80FD\u8BED\u97F3\uFF0C\u8BF7\u7A0D\u540E\u64AD\u653E", 0);
        } else
        {
            if (XiMaoBTManager.getInstance(appContext).isConnected())
            {
                XiMaoComm.requestA2DPCon(appContext);
            }
            if (!isPlayingAds() && mp != null)
            {
                if (mp.isPausedSoundAd())
                {
                    mp.resumePlaySoundAd();
                    return;
                }
                if (!mp.isPlayingSoundAd())
                {
                    mp.start();
                    return;
                }
            }
        }
    }

    public void stop()
    {
        if (mp != null)
        {
            mp.stop();
        }
    }

    public void stopPlayTask()
    {
        stop();
        stopForeground(true);
        mp.removeNotifications();
        updateOnPlayCanceled();
    }

    public void updateOnPlayCanceled()
    {
        if (mPlayServiceUpdateListenerList == null)
        {
            break MISSING_BLOCK_LABEL_73;
        }
        Object obj = PlayServiceUpdateListenerListLock;
        obj;
        JVM INSTR monitorenter ;
        Object obj1 = mPlayServiceUpdateListenerList;
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_65;
        }
        obj1 = mPlayServiceUpdateListenerList.iterator();
_L2:
        OnPlayServiceUpdateListener onplayserviceupdatelistener;
        do
        {
            if (!((Iterator) (obj1)).hasNext())
            {
                break MISSING_BLOCK_LABEL_65;
            }
            onplayserviceupdatelistener = (OnPlayServiceUpdateListener)((Iterator) (obj1)).next();
        } while (onplayserviceupdatelistener == null);
        onplayserviceupdatelistener.onPlayCanceled();
        if (true) goto _L2; else goto _L1
_L1:
        Object obj2;
        obj2;
        obj;
        JVM INSTR monitorexit ;
        return;
        obj2;
        obj;
        JVM INSTR monitorexit ;
        throw obj2;
    }

    public void updateOnSoundChanged()
    {
        if (mPlayServiceUpdateListenerList == null)
        {
            break MISSING_BLOCK_LABEL_80;
        }
        Object obj = PlayServiceUpdateListenerListLock;
        obj;
        JVM INSTR monitorenter ;
        Object obj1 = mPlayServiceUpdateListenerList;
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_72;
        }
        obj1 = mPlayServiceUpdateListenerList.iterator();
_L2:
        OnPlayServiceUpdateListener onplayserviceupdatelistener;
        do
        {
            if (!((Iterator) (obj1)).hasNext())
            {
                break MISSING_BLOCK_LABEL_72;
            }
            onplayserviceupdatelistener = (OnPlayServiceUpdateListener)((Iterator) (obj1)).next();
        } while (onplayserviceupdatelistener == null);
        onplayserviceupdatelistener.onSoundChanged(playListManager.curIndex);
        if (true) goto _L2; else goto _L1
_L1:
        Object obj2;
        obj2;
        obj;
        JVM INSTR monitorexit ;
        return;
        obj2;
        obj;
        JVM INSTR monitorexit ;
        throw obj2;
    }

    public void updateOnSoundInfoModified(int i, SoundInfo soundinfo)
    {
        while (i < 0 || soundinfo == null || mPlayServiceUpdateListenerList == null) 
        {
            return;
        }
        Object obj = PlayServiceUpdateListenerListLock;
        obj;
        JVM INSTR monitorenter ;
        Object obj1 = mPlayServiceUpdateListenerList;
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_84;
        }
        obj1 = mPlayServiceUpdateListenerList.iterator();
_L2:
        OnPlayServiceUpdateListener onplayserviceupdatelistener;
        do
        {
            if (!((Iterator) (obj1)).hasNext())
            {
                break MISSING_BLOCK_LABEL_84;
            }
            onplayserviceupdatelistener = (OnPlayServiceUpdateListener)((Iterator) (obj1)).next();
        } while (onplayserviceupdatelistener == null);
        onplayserviceupdatelistener.onSoundInfoChanged(i, soundinfo);
        if (true) goto _L2; else goto _L1
_L1:
        soundinfo;
        obj;
        JVM INSTR monitorexit ;
        return;
        soundinfo;
        obj;
        JVM INSTR monitorexit ;
        throw soundinfo;
    }



/*
    static boolean access$002(LocalMediaService localmediaservice, boolean flag)
    {
        localmediaservice.canSwitch = flag;
        return flag;
    }

*/

}
