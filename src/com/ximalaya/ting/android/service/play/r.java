// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.media.AudioManager;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.device.che.MyBluetoothManager2;
import com.ximalaya.ting.android.modelmanage.DexManager;
import com.ximalaya.ting.android.player.XMediaplayerImpl;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            TingMediaPlayer

class r
    implements android.media.AudioManager.OnAudioFocusChangeListener
{

    final TingMediaPlayer a;
    private boolean b;

    r(TingMediaPlayer tingmediaplayer)
    {
        a = tingmediaplayer;
        super();
    }

    public void onAudioFocusChange(int i)
    {
        Logger.d(TingMediaPlayer.TAG, (new StringBuilder()).append("focusChange:").append(i).toString());
        if (i != -1) goto _L2; else goto _L1
_L1:
        if (!DexManager.getInstance(TingMediaPlayer.access$000(a)).isSuichetingOpen() || !MyBluetoothManager2.getInstance(MyApplication.b()).isContinuePlaying()) goto _L4; else goto _L3
_L3:
        return;
_L4:
        a.pause();
        if (TingMediaPlayer.access$100(a) != null)
        {
            TingMediaPlayer.access$100(a).abandonAudioFocus(TingMediaPlayer.access$200(a));
        }
        Logger.d(TingMediaPlayer.TAG, "AUDIOFOCUS_LOSS");
        return;
_L2:
        if (i == -2)
        {
            if (a.mediaPlayerState == 4)
            {
                a.pause();
                b = true;
            }
            Logger.d(TingMediaPlayer.TAG, "AUDIOFOCUS_LOSS_TRANSIENT");
            return;
        }
        if (i == 2)
        {
            Logger.d(TingMediaPlayer.TAG, "AUDIOFOCUS_GAIN_TRANSIENT");
            return;
        }
        if (i == 1)
        {
            if (a.mediaPlayerState != 4 && b)
            {
                a.start();
                b = false;
            }
            if (a.mediaplayer != null)
            {
                a.mediaplayer.setVolume(1.0F, 1.0F);
            }
            Logger.d(TingMediaPlayer.TAG, "AUDIOFOCUS_GAIN");
            return;
        }
        if (i == -3)
        {
            Logger.d(TingMediaPlayer.TAG, "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
            if (a.mediaplayer != null)
            {
                a.mediaplayer.setVolume(0.3F, 0.3F);
            }
            Logger.d(TingMediaPlayer.TAG, (new StringBuilder()).append("after now sound:").append(TingMediaPlayer.access$300(a)).toString());
            return;
        }
        if (i == 3)
        {
            Logger.d(TingMediaPlayer.TAG, "AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK");
            if (a.mediaplayer != null)
            {
                a.mediaplayer.setVolume(0.3F, 0.3F);
            }
            Logger.d(TingMediaPlayer.TAG, (new StringBuilder()).append("after now sound:").append(TingMediaPlayer.access$300(a)).toString());
            return;
        }
        if (true) goto _L3; else goto _L5
_L5:
    }
}
