// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Playlist
{

    protected int mCurrPosition;
    protected List mData;
    protected String mDataSourceUrl;
    protected boolean mIsReverse;
    protected int mPageId;
    protected HashMap mParams;
    protected int mPlayMode;

    public Playlist(List list)
    {
        mIsReverse = false;
        mData = list;
    }

    public void append(List list)
    {
        if (mData == null)
        {
            mData = new ArrayList(list);
            return;
        } else
        {
            mData.addAll(list);
            return;
        }
    }

    public void clear()
    {
        if (mData != null)
        {
            mData.clear();
        }
    }

    public void clearData()
    {
        if (mData != null)
        {
            mData.clear();
        }
    }

    public int getCurrPosition()
    {
        return mCurrPosition;
    }

    public SoundInfo getCurrSound()
    {
        while (mData == null || mData.size() <= 0 || mCurrPosition < 0 || mCurrPosition >= mData.size()) 
        {
            return null;
        }
        return (SoundInfo)mData.get(mCurrPosition);
    }

    public List getData()
    {
        if (mData == null)
        {
            mData = new ArrayList();
        }
        return mData;
    }

    public String getDataSource()
    {
        return mDataSourceUrl;
    }

    public int getPageId()
    {
        return mPageId;
    }

    public HashMap getParams()
    {
        return mParams;
    }

    public SoundInfo getSoundAt(int i)
    {
        if (!isEmpty() && i >= 0 && i < size())
        {
            return (SoundInfo)mData.get(i);
        } else
        {
            return null;
        }
    }

    public abstract boolean hasMore();

    public boolean hasNext()
    {
        while (mData == null || mCurrPosition + 1 >= mData.size()) 
        {
            return false;
        }
        return true;
    }

    public boolean hasPrevious()
    {
        while (mData == null || mCurrPosition - 1 < 0) 
        {
            return false;
        }
        return true;
    }

    public boolean isEmpty()
    {
        return mData == null || mData.size() == 0;
    }

    public boolean isReverse()
    {
        return mIsReverse;
    }

    public int jumpTo(int i)
    {
        return 0;
    }

    public abstract List loadMore();

    public SoundInfo next()
    {
        if (mData != null)
        {
            if (mData.size() <= 0);
        }
        return null;
    }

    public SoundInfo previous()
    {
        return null;
    }

    public boolean remove(SoundInfo soundinfo)
    {
        int i;
        if (!isEmpty())
        {
            if ((i = mData.indexOf(soundinfo)) != -1)
            {
                mData.remove(i);
                if (i < mCurrPosition)
                {
                    mCurrPosition = mCurrPosition - 1;
                }
                return true;
            }
        }
        return false;
    }

    public void setCurrPosition(int i)
    {
        mCurrPosition = i;
    }

    public void setData(List list)
    {
        mData = list;
    }

    public void setDataSource(String s)
    {
        mDataSourceUrl = s;
    }

    public void setIsReverse(boolean flag)
    {
        mIsReverse = flag;
    }

    public void setPageId(int i)
    {
        mPageId = i;
    }

    public void setParams(HashMap hashmap)
    {
        mParams = hashmap;
    }

    public void setPlayMode(int i)
    {
        mPlayMode = i;
    }

    public int size()
    {
        if (mData == null)
        {
            return 0;
        } else
        {
            return mData.size();
        }
    }

    public void update(List list)
    {
        int i;
        SoundInfo soundinfo = getCurrSound();
        SoundInfo soundinfo1;
        if (mData != null)
        {
            mData.clear();
            mData.addAll(list);
        } else
        {
            mData = new ArrayList(list);
        }
        if (soundinfo == null) goto _L2; else goto _L1
_L1:
        i = 0;
_L7:
        if (i >= list.size()) goto _L2; else goto _L3
_L3:
        soundinfo1 = (SoundInfo)list.get(i);
        if (!soundinfo.equals(soundinfo1)) goto _L5; else goto _L4
_L4:
        mCurrPosition = i;
        ModelHelper.updateSoundInfo(soundinfo1, soundinfo);
_L2:
        return;
_L5:
        i++;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public void updateSoundAt(int i, SoundInfo soundinfo)
    {
        while (mData == null || mData.size() < 0 || i < 0 || i >= mData.size()) 
        {
            return;
        }
        mData.set(i, soundinfo);
    }
}
