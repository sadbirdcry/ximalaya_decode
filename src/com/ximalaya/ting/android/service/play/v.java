// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager;
import com.ximalaya.ting.android.fragment.device.che.MyBluetoothManager2;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.DexManager;
import com.ximalaya.ting.android.player.XMediaplayerImpl;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.NetworkUtils;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            TingMediaPlayer, PlayListControl, LocalMediaService

class v extends BroadcastReceiver
{

    final TingMediaPlayer a;

    v(TingMediaPlayer tingmediaplayer)
    {
        a = tingmediaplayer;
        super();
    }

    public void onReceive(Context context, Intent intent)
    {
        String s;
        s = intent.getAction();
        Logger.log("debug_play", (new StringBuilder()).append("===onReceive====").append(s).toString());
        if (!s.equals("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_EXIT")) goto _L2; else goto _L1
_L1:
        intent = MainTabActivity2.mainTabActivity;
        if (intent == null) goto _L4; else goto _L3
_L3:
        intent.finish();
_L6:
        return;
_L4:
        ((MyApplication)context.getApplicationContext()).g();
        return;
_L2:
        if (!s.equals("android.intent.action.HEADSET_PLUG") && !"android.media.AUDIO_BECOMING_NOISY".equals(s))
        {
            break; /* Loop/switch isn't completed */
        }
        if (intent.getIntExtra("state", 0) != 1 && (!DexManager.getInstance(TingMediaPlayer.access$000(a)).isSuichetingOpen() || !MyBluetoothManager2.getInstance(context).isContinuePlaying()))
        {
            if (BluetoothManager.getInstance(TingMediaPlayer.access$000(a)).isNeedContinuePlay())
            {
                BluetoothManager.getInstance(TingMediaPlayer.access$000(a)).setNeedContinuePlay(false);
                return;
            }
            if (a.mediaplayer != null && a.mediaplayer.isPlaying())
            {
                try
                {
                    a.pause();
                    return;
                }
                // Misplaced declaration of an exception variable
                catch (Context context)
                {
                    context.printStackTrace();
                }
                return;
            }
        }
        if (true) goto _L6; else goto _L5
_L5:
        if (s.equals("android.net.conn.CONNECTIVITY_CHANGE"))
        {
            TingMediaPlayer.access$2002(a, (ConnectivityManager)TingMediaPlayer.access$000(a).getSystemService("connectivity"));
            TingMediaPlayer.access$2102(a, TingMediaPlayer.access$2000(a).getActiveNetworkInfo());
            if (TingMediaPlayer.access$2100(a) == null || !TingMediaPlayer.access$2100(a).isAvailable())
            {
                continue; /* Loop/switch isn't completed */
            }
            context = TingMediaPlayer.access$2100(a).getTypeName();
            Logger.d("NetTypeChanged", (new StringBuilder()).append("\u5F53\u524D\u7F51\u7EDC\u540D\u79F0\uFF1A").append(context).toString());
            if (a.bufferingPercent < 100 && 1 == TingMediaPlayer.access$1000(a))
            {
                if (a.mediaplayer != null)
                {
                    if (NetworkUtils.getNetType(TingMediaPlayer.access$000(a)) == 1)
                    {
                        TingMediaPlayer.access$1402(a, a.mediaPlayerState);
                        TingMediaPlayer.access$1502(a, a.getCurrentPosition());
                        context = PlayListControl.getPlayListManager().getCurSound();
                        if (TingMediaPlayer.access$1500(a) == 0 && context != null)
                        {
                            TingMediaPlayer.access$1502(a, (int)((SoundInfo) (context)).history_listener);
                        }
                        TingMediaPlayer.access$1002(a, 2);
                        TingMediaPlayer.access$1600(a, true, TingMediaPlayer.access$1500(a));
                        return;
                    } else
                    {
                        TingMediaPlayer.access$1002(a, 0);
                        return;
                    }
                }
            } else
            {
                TingMediaPlayer.access$1002(a, 0);
                return;
            }
            continue; /* Loop/switch isn't completed */
        }
        break MISSING_BLOCK_LABEL_473;
        if (a.mediaPlayerState != 4) goto _L6; else goto _L7
_L7:
        TingMediaPlayer.access$1002(a, 1);
        return;
        if (!s.equals("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_SWITCH_PLAY_PAUSE"))
        {
            break; /* Loop/switch isn't completed */
        }
        if (a.mediaPlayerState == 5 || a.mediaPlayerState == 3)
        {
            a.start();
            return;
        }
        if (a.mediaPlayerState == 4)
        {
            a.pause();
            return;
        }
        if (a.mediaPlayerState == 7)
        {
            TingMediaPlayer.access$2200(a, true);
            return;
        }
        if (true) goto _L6; else goto _L8
_L8:
        if (!s.equals("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_NEXT"))
        {
            continue; /* Loop/switch isn't completed */
        }
        if (a.notification != null)
        {
            context = LocalMediaService.getInstance();
            if (context != null)
            {
                Logger.log("debug_play", "==playNext==05=");
                context.playNext(true);
                return;
            }
        }
        continue; /* Loop/switch isn't completed */
        if (!s.equals("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_PRE") || a.notification == null) goto _L6; else goto _L9
_L9:
        context = LocalMediaService.getInstance();
        if (context != null)
        {
            context.playPrev(true);
            return;
        }
        if (true) goto _L6; else goto _L10
_L10:
    }
}
