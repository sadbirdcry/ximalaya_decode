// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.feed2.FeedSoundInfoList;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            Playlist

class b extends Playlist
{

    private int a;
    private boolean b;

    public b(List list)
    {
        super(list);
        b = true;
        if (mParams == null)
        {
            break MISSING_BLOCK_LABEL_38;
        }
        list = (String)mParams.get("pageSize");
        a = Integer.parseInt(list);
        return;
        list;
        list.printStackTrace();
        return;
    }

    public boolean hasMore()
    {
        return b;
    }

    public List loadMore()
    {
        Object obj;
        if (mParams == null)
        {
            b = false;
            return null;
        }
        obj = new RequestParams();
        String s;
        for (Iterator iterator = mParams.keySet().iterator(); iterator.hasNext(); ((RequestParams) (obj)).put(s, (String)mParams.get(s)))
        {
            s = (String)iterator.next();
        }

        ((RequestParams) (obj)).put("pageId", mPageId);
        obj = f.a().a(mDataSourceUrl, ((RequestParams) (obj)), false);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            break MISSING_BLOCK_LABEL_247;
        }
        obj = (FeedSoundInfoList)JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a, com/ximalaya/ting/android/model/feed2/FeedSoundInfoList);
        if (((FeedSoundInfoList) (obj)).data != null && !((FeedSoundInfoList) (obj)).data.isEmpty())
        {
            break MISSING_BLOCK_LABEL_153;
        }
        b = false;
        return null;
        if (((FeedSoundInfoList) (obj)).data.size() < a)
        {
            b = false;
        }
        if (((FeedSoundInfoList) (obj)).data == null || ((FeedSoundInfoList) (obj)).data.size() <= 0)
        {
            break MISSING_BLOCK_LABEL_247;
        }
        mPageId = mPageId + 1;
        obj = ModelHelper.toSoundInfoList(((FeedSoundInfoList) (obj)).data);
        return ((List) (obj));
        Exception exception;
        exception;
        Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
        return null;
    }
}
