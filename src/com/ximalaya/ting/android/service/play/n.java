// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.LiveHistoryManage;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import java.util.Date;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            SoundListenRecord, PlayListControl

class n extends BroadcastReceiver
{

    final SoundListenRecord a;

    n(SoundListenRecord soundlistenrecord)
    {
        a = soundlistenrecord;
        super();
    }

    public void onReceive(Context context, Intent intent)
    {
label0:
        {
            {
                context = intent.getAction();
                if (!context.equals("ximalaya.action.player.CHANGE_SOUND"))
                {
                    break label0;
                }
                if (SoundListenRecord.access$000(a) != 0L)
                {
                    SoundListenRecord.access$102(a, (System.currentTimeMillis() - SoundListenRecord.access$000(a)) / 1000L + SoundListenRecord.access$100(a));
                }
                MyLogger.getLogger().d("soundchange broadcast");
                SoundListenRecord.access$202(a, intent.getIntExtra("position1", 0));
                SoundListenRecord.access$302(a, intent.getLongExtra("trackId1", 0L));
                SoundListenRecord.access$402(a, intent.getLongExtra("trackId2", 0L));
                SoundListenRecord.access$502(a, intent.getLongExtra("programScheduleId1", 0L));
                SoundListenRecord.access$602(a, intent.getLongExtra("programScheduleId2", 0L));
                int i = intent.getIntExtra("category1", 0);
                SoundListenRecord.access$702(a, SoundListenRecord.access$100(a));
                SoundListenRecord.access$802(a, intent.getIntExtra("duration1", 0));
                context = intent.getStringExtra("ACTION_EXTRA_SOUNDINFO");
                if (!TextUtils.isEmpty(context))
                {
                    context = (SoundInfo)JSON.parseObject(context, com/ximalaya/ting/android/model/sound/SoundInfo);
                } else
                {
                    context = null;
                }
                if (context != null)
                {
                    SoundListenRecord.access$902(a, ((SoundInfo) (context)).recSrc);
                    SoundListenRecord.access$1002(a, ((SoundInfo) (context)).recTrack);
                } else
                {
                    SoundListenRecord.access$902(a, null);
                    SoundListenRecord.access$1002(a, null);
                }
                MyLogger.getLogger().d((new StringBuilder()).append("lastTrackId=").append(SoundListenRecord.access$300(a)).append(", lastCategory=").append(i).append(", duration=").append(SoundListenRecord.access$700(a)).toString());
                if (i == 0)
                {
                    SoundListenRecord.access$1100(a);
                } else
                {
                    SoundListenRecord.access$1300(a, SoundListenRecord.access$1200(a));
                }
                SoundListenRecord.access$102(a, 0L);
                SoundListenRecord.access$702(a, 0.0F);
                if (SoundListenRecord.access$400(a) > 0L)
                {
                    ThirdAdStatUtil.getInstance().statSoundAd(SoundListenRecord.access$400(a));
                }
            }
            return;
        }
        if (!context.equals("ximalaya.action.player.START"))
        {
            continue; /* Loop/switch isn't completed */
        }
        MyLogger.getLogger().d("start broadcast");
        SoundListenRecord.access$1402(a, false);
        SoundListenRecord.access$1502(a, PlayListControl.getPlayListManager().listType);
        context = PlayListControl.getPlayListManager().getCurSound();
        if (SoundListenRecord.access$1600(a))
        {
            SoundListenRecord.access$1700(a);
        }
        if (SoundListenRecord.access$1800(a))
        {
            SoundListenRecord.access$1900(a);
        }
        SoundListenRecord.access$002(a, System.currentTimeMillis());
        if (context == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (((SoundInfo) (context)).category != 0) goto _L2; else goto _L1
_L1:
        SoundListenRecord.access$402(a, PlayListControl.getPlayListManager().getCurSound().trackId);
        SoundListenRecord.access$702(a, 0.0F);
        SoundListenRecord.access$2000(a, SoundListenRecord.access$400(a));
        if (SoundListenRecord.access$2100(a))
        {
            ThirdAdStatUtil.getInstance().statSoundAd(SoundListenRecord.access$400(a));
        }
        if (SoundListenRecord.access$1200(a) == null || SoundListenRecord.access$1200(a).trackId != ((SoundInfo) (context)).trackId)
        {
            SoundListenRecord.access$2202(System.currentTimeMillis());
        }
_L4:
        SoundListenRecord.access$1202(a, context);
        return;
_L2:
        if (SoundListenRecord.access$1200(a) == null || SoundListenRecord.access$1200(a).programScheduleId != ((SoundInfo) (context)).programScheduleId)
        {
            SoundListenRecord.access$2202(System.currentTimeMillis());
            context.startPlayTime = new Date();
        }
        LiveHistoryManage.getInstance(SoundListenRecord.access$2300(a)).putSound(context);
        if (SoundListenRecord.access$1200(a) != null && SoundListenRecord.access$1200(a).category == 1 && SoundListenRecord.access$1200(a).programScheduleId == ((SoundInfo) (context)).programScheduleId)
        {
            SoundListenRecord.access$702(a, SoundListenRecord.access$100(a));
            SoundListenRecord.access$102(a, 0L);
            SoundListenRecord.access$1300(a, SoundListenRecord.access$1200(a));
            SoundListenRecord.access$2202(System.currentTimeMillis());
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (!context.equals("ximalaya.action.player.PAUSE")) goto _L6; else goto _L5
_L5:
        break MISSING_BLOCK_LABEL_752;
_L6:
        break MISSING_BLOCK_LABEL_341;
        MyLogger.getLogger().d("pause broadcast");
        if (!SoundListenRecord.access$1400(a))
        {
            SoundListenRecord.access$102(a, (System.currentTimeMillis() - SoundListenRecord.access$000(a)) / 1000L + SoundListenRecord.access$100(a));
            MyLogger.getLogger().d((new StringBuilder()).append("time = ").append(SoundListenRecord.access$100(a)).toString());
            SoundListenRecord.access$002(a, 0L);
            SoundListenRecord.access$1402(a, true);
            return;
        }
        if (true) goto _L8; else goto _L7
_L8:
        break MISSING_BLOCK_LABEL_341;
_L7:
    }
}
