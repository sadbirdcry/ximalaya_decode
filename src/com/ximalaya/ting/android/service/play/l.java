// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.util.ApiUtil;
import java.util.HashMap;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            f, Playlist, g, a, 
//            b, RankPlayList, CategoryDetailPlayList, BasePlaylist

final class l
{

    l()
    {
    }

    public Playlist a(List list, String s, int i, HashMap hashmap)
    {
        if (e.O.equals(s))
        {
            list = new f(list);
        } else
        if (e.B.equals(s))
        {
            list = new g(list);
        } else
        if ((new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/my/album/track/").toString().equals(s) || (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString().equals(s))
        {
            list = new a(list);
        } else
        if ("mobile/api/1/feed/album/list".equals(s))
        {
            list = new b(list);
        } else
        if ("mobile/discovery/v1/rankingList/track".equals(s))
        {
            list = new RankPlayList(list);
        } else
        if ("mobile/discovery/v1/category/track".equals(s))
        {
            list = new CategoryDetailPlayList(list);
        } else
        {
            list = new BasePlaylist(list);
        }
        list.setDataSource(s);
        list.setPageId(i);
        list.setParams(hashmap);
        return list;
    }
}
