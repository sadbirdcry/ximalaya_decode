// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a.d;
import com.ximalaya.ting.android.applink.SdlConnectManager;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.BaseBtController;
import com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.model.RecordModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.model.LinkedDeviceModel;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.xdcs.PlayEvent;
import com.ximalaya.ting.android.modelmanage.HistoryManage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MD5;
import com.ximalaya.ting.android.util.NetworkUtils;
import java.util.Calendar;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            n, PlayListControl, p, q, 
//            LocalMediaService, m, o

public class SoundListenRecord
{

    private static final Object INSTANCE_LOCK = new Object();
    private static long listenStartTime = 0L;
    private static volatile SoundListenRecord soundListenRecordManager = null;
    private Context appContext;
    private long currentProgramScheduleId;
    private long currentTrackId;
    private int duration;
    private boolean isPaused;
    private boolean isreReiste;
    private long lastProgramScheduleId;
    private SoundInfo lastSoundInfo;
    private long lastTime;
    private long lastTrackId;
    private int listType;
    private float listenedPosition;
    private float listenedTime;
    protected BroadcastReceiver mPlayerEvtReceiver;
    private String mRecSrc;
    private String mRecTrack;
    private boolean shallSent;
    private long time;

    private SoundListenRecord(Context context)
    {
        listType = 0;
        lastTrackId = 0L;
        currentTrackId = 0L;
        listenedTime = 0.0F;
        listenedPosition = 0.0F;
        lastProgramScheduleId = 0L;
        currentProgramScheduleId = 0L;
        duration = 0;
        lastSoundInfo = null;
        appContext = null;
        lastTime = System.currentTimeMillis();
        time = 0L;
        isPaused = true;
        shallSent = true;
        isreReiste = false;
        mPlayerEvtReceiver = new n(this);
        MyLogger.getLogger().print();
        appContext = context;
        registerReceivers();
        shallSent = true;
    }

    private int getPlaySource()
    {
        switch (listType)
        {
        case 3: // '\003'
        case 4: // '\004'
        case 6: // '\006'
        case 8: // '\b'
        case 9: // '\t'
        case 13: // '\r'
        case 14: // '\016'
        case 15: // '\017'
        case 31: // '\037'
        default:
            return 99;

        case 16: // '\020'
            return 1;

        case 18: // '\022'
            return 2;

        case 1: // '\001'
            return 6;

        case 19: // '\023'
            return 7;

        case 7: // '\007'
            return 8;

        case 11: // '\013'
            return 9;

        case 20: // '\024'
            return 10;

        case 12: // '\f'
            return 11;

        case 21: // '\025'
            return 12;

        case 5: // '\005'
            return 13;

        case 22: // '\026'
            return 14;

        case 23: // '\027'
            return 15;

        case 2: // '\002'
            return 16;

        case 17: // '\021'
            return 17;

        case 10: // '\n'
            return 18;

        case 24: // '\030'
            return 19;

        case 26: // '\032'
            return 20;

        case 25: // '\031'
            return 21;

        case 27: // '\033'
            return 22;

        case 28: // '\034'
            return 26;

        case 29: // '\035'
            return 27;

        case 30: // '\036'
            return 28;

        case 32: // ' '
            return 29;
        }
    }

    public static final SoundListenRecord getSoundListenRecord(Context context)
    {
        if (soundListenRecordManager == null)
        {
            synchronized (INSTANCE_LOCK)
            {
                if (soundListenRecordManager == null)
                {
                    soundListenRecordManager = new SoundListenRecord(context);
                }
            }
        }
        return soundListenRecordManager;
        context;
        obj;
        JVM INSTR monitorexit ;
        throw context;
    }

    private boolean isClassLoaded(String s)
    {
        try
        {
            s = com/ximalaya/ting/android/service/play/SoundListenRecord.getClassLoader().loadClass(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s = null;
        }
        return s != null;
    }

    private boolean isRequireRadioPlayStat()
    {
        boolean flag1 = true;
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        boolean flag;
        if (soundinfo == null || soundinfo.category == 0)
        {
            flag = false;
        } else
        {
            flag = flag1;
            if (lastSoundInfo != null)
            {
                flag = flag1;
                if (lastSoundInfo.category != 0)
                {
                    flag = flag1;
                    if (lastSoundInfo.radioId == soundinfo.radioId)
                    {
                        return false;
                    }
                }
            }
        }
        return flag;
    }

    private boolean isRequireSoundPlayStat()
    {
        boolean flag1 = true;
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        boolean flag;
        if (soundinfo == null || soundinfo.category != 0)
        {
            flag = false;
        } else
        {
            flag = flag1;
            if (lastSoundInfo != null)
            {
                flag = flag1;
                if (lastSoundInfo.category == 0)
                {
                    flag = flag1;
                    if (lastSoundInfo.trackId == soundinfo.trackId)
                    {
                        return false;
                    }
                }
            }
        }
        return flag;
    }

    private void radioPlayStat()
    {
        (new p(this)).start();
    }

    private void radioProgramPlayStat(SoundInfo soundinfo)
    {
        MyLogger.getLogger().d((new StringBuilder()).append("SoundInfo=").append(soundinfo).toString());
        if (soundinfo == null || soundinfo.programScheduleId == 0L)
        {
            return;
        }
        RequestParams requestparams = new RequestParams();
        requestparams.put("radioId", soundinfo.radioId);
        requestparams.put("programScheduleId", soundinfo.programScheduleId);
        requestparams.put("programId", soundinfo.programId);
        requestparams.put("started_at", listenStartTime);
        requestparams.put("duration", (new StringBuilder()).append(listenedTime).append("").toString());
        if (soundinfo.category == 1)
        {
            String s = soundinfo.startTime;
            soundinfo = s;
            if (TextUtils.isEmpty(s))
            {
                soundinfo = "00:00";
            }
            soundinfo = soundinfo.split(":");
            int i = Integer.parseInt(soundinfo[0]);
            int j = Integer.parseInt(soundinfo[1]);
            soundinfo = Calendar.getInstance();
            soundinfo.set(11, i);
            soundinfo.set(12, j);
            soundinfo.set(13, 0);
            soundinfo.set(14, 0);
            float f = (float)(System.currentTimeMillis() - soundinfo.getTimeInMillis()) / 1000F;
            requestparams.put("played_secs", (new StringBuilder()).append(f).append("").toString());
        } else
        {
            requestparams.put("played_secs", (new StringBuilder()).append(listenedPosition / 1000F).append("").toString());
        }
        (new q(this, requestparams)).start();
    }

    private void registerReceivers()
    {
        if (appContext == null)
        {
            return;
        } else
        {
            IntentFilter intentfilter = new IntentFilter();
            intentfilter.addAction("ximalaya.action.player.CHANGE_SOUND");
            intentfilter.addAction("ximalaya.action.player.START");
            intentfilter.addAction("ximalaya.action.player.PAUSE");
            appContext.registerReceiver(mPlayerEvtReceiver, intentfilter);
            isreReiste = true;
            return;
        }
    }

    private void saveHistory(long l)
    {
        HistoryManage.getInstance(appContext).putSound(PlayListControl.getPlayListManager().getSoundInCurList(l));
    }

    private void soundListenAction()
    {
        MyLogger.getLogger().d((new StringBuilder()).append("lastSoundInfo=").append(lastSoundInfo).toString());
        Logger.d("PlayStatistics", (new StringBuilder()).append("").append(lastSoundInfo).toString());
        if (lastTrackId <= 0L || lastSoundInfo == null) goto _L2; else goto _L1
_L1:
        RequestParams requestparams;
        requestparams = new RequestParams();
        requestparams.put("trackId", (new StringBuilder()).append(lastTrackId).append("").toString());
        requestparams.put("duration", (new StringBuilder()).append(listenedTime).append("").toString());
        requestparams.put("played_secs", (new StringBuilder()).append(listenedPosition / 1000F).append("").toString());
        requestparams.put("started_at", (new StringBuilder()).append(listenStartTime).append("").toString());
        if (!TextUtils.isEmpty(mRecSrc))
        {
            requestparams.put("rec_src", mRecSrc);
        }
        if (!TextUtils.isEmpty(mRecTrack))
        {
            requestparams.put("rec_track", mRecTrack);
        }
        Object obj;
        boolean flag;
        boolean flag1;
        if (LocalMediaService.getInstance() != null && LocalMediaService.getInstance().isPlayFromNetwork())
        {
            requestparams.put("play_type", "0");
        } else
        {
            requestparams.put("play_type", "1");
        }
        requestparams.put("play_source", (new StringBuilder()).append(getPlaySource()).append("").toString());
        if (!NetworkUtils.isNetworkAvaliable(appContext)) goto _L4; else goto _L3
_L3:
        if (!BluetoothManager.getInstance(appContext).isA2dpConnSimple() || BluetoothManager.getInstance(appContext).getBtDeviceController() == null) goto _L6; else goto _L5
_L5:
label0:
        {
            flag1 = true;
            flag = flag1;
            if (android.os.Build.VERSION.SDK_INT <= 11)
            {
                break label0;
            }
            obj = BluetoothAdapter.getDefaultAdapter();
            if (((BluetoothAdapter) (obj)).isEnabled())
            {
                flag = flag1;
                if (((BluetoothAdapter) (obj)).getProfileConnectionState(2) == 2)
                {
                    break label0;
                }
            }
            flag = false;
        }
        if (flag)
        {
            obj = BluetoothManager.getInstance(appContext).getBtDeviceController().getRecordModel();
            if (obj != null)
            {
                Logger.d("record", (new StringBuilder()).append(((RecordModel) (obj)).getType()).append(":").append(((RecordModel) (obj)).getDevice()).append(":").append(((RecordModel) (obj)).getDeviceName()).toString());
                requestparams.put("connect_type", ((RecordModel) (obj)).getType());
                requestparams.put("connect_device", ((RecordModel) (obj)).getDevice());
                requestparams.put("connect_deviceName", ((RecordModel) (obj)).getDeviceName());
                Logger.d("record", (new StringBuilder()).append("BlueDevice").append(((RecordModel) (obj)).getType()).append(":").append(((RecordModel) (obj)).getDevice()).append(":").append(((RecordModel) (obj)).getDeviceName()).toString());
            }
        }
_L7:
        (new m(this, requestparams)).start();
_L2:
        return;
_L6:
        if (MyDeviceManager.getInstance(appContext).isDlnaInit() && DlnaManager.getInstance(appContext).getLinkedDeviceModel() != null)
        {
            if (DlnaManager.getInstance(appContext).getLinkedDeviceModel().getNowDeviceItem().getDlnaType() == com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss)
            {
                Logger.d("PlayStatistics", "WIFI\u8BBE\u5907\u64AD\u653E");
                requestparams.put("connect_type", "2");
                requestparams.put("connect_device", "2");
                requestparams.put("connect_deviceName", DlnaManager.getInstance(appContext).getLinkedDeviceModel().getUDNString());
            }
            continue; /* Loop/switch isn't completed */
        }
        Object obj1 = SdlConnectManager.getInstance(appContext);
        if (((SdlConnectManager) (obj1)).isAppLinkRunning())
        {
            requestparams.put("connect_type", "1");
            requestparams.put("connect_device", "4");
            requestparams.put("connect_deviceName", "ford");
            continue; /* Loop/switch isn't completed */
        }
        try
        {
            if (((SdlConnectManager) (obj1)).isSdlRunning())
            {
                requestparams.put("connect_type", "1");
                requestparams.put("connect_device", "5");
                requestparams.put("connect_deviceName", "changcheng");
            }
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            ((Exception) (obj1)).printStackTrace();
        }
        if (true) goto _L7; else goto _L4
_L4:
        PlayEvent playevent = new PlayEvent();
        playevent.setProps(requestparams.getStringParamsMap());
        playevent.setTs(System.currentTimeMillis());
        playevent.setType("PLAY");
        DataCollectUtil.getInstance(appContext).produceEvent(playevent);
        return;
    }

    private void soundPlayStat()
    {
        (new o(this)).start();
    }

    public void onDestory()
    {
        shallSent = true;
        MyLogger.getLogger().print();
        if (lastTime != 0L)
        {
            time = (System.currentTimeMillis() - lastTime) / 1000L + time;
        }
        listenedTime = time;
        Object obj = LocalMediaService.getInstance();
        if (obj != null)
        {
            listenedPosition = ((LocalMediaService) (obj)).getCurPosition();
            duration = ((LocalMediaService) (obj)).getDuration();
        }
        obj = PlayListControl.getPlayListManager().getCurSound();
        if (obj != null)
        {
            if (((SoundInfo) (obj)).category == 0)
            {
                currentTrackId = ((SoundInfo) (obj)).trackId;
                lastTrackId = currentTrackId;
                saveHistory(currentTrackId);
                soundListenAction();
            } else
            {
                radioProgramPlayStat(((SoundInfo) (obj)));
            }
        }
        removeReceivers();
        soundListenRecordManager = null;
    }

    public void removeReceivers()
    {
        if (appContext == null || !isreReiste)
        {
            return;
        } else
        {
            appContext.unregisterReceiver(mPlayerEvtReceiver);
            isreReiste = false;
            return;
        }
    }

    public String signature(Long long1, Long long2, Long long3, Float float1, Float float2, String s)
    {
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append(((MyApplication)appContext.getApplicationContext()).i());
        stringbuffer.append(((MyApplication)(MyApplication)appContext.getApplicationContext()).m());
        stringbuffer.append(long1);
        stringbuffer.append(long2);
        stringbuffer.append(long3);
        stringbuffer.append(float1);
        stringbuffer.append(float2);
        stringbuffer.append(s);
        stringbuffer.append(d.b);
        long1 = MD5.md5(stringbuffer.toString());
        long1.replaceAll("-", "").toLowerCase();
        return long1;
    }




/*
    static long access$002(SoundListenRecord soundlistenrecord, long l)
    {
        soundlistenrecord.lastTime = l;
        return l;
    }

*/



/*
    static String access$1002(SoundListenRecord soundlistenrecord, String s)
    {
        soundlistenrecord.mRecTrack = s;
        return s;
    }

*/


/*
    static long access$102(SoundListenRecord soundlistenrecord, long l)
    {
        soundlistenrecord.time = l;
        return l;
    }

*/




/*
    static SoundInfo access$1202(SoundListenRecord soundlistenrecord, SoundInfo soundinfo)
    {
        soundlistenrecord.lastSoundInfo = soundinfo;
        return soundinfo;
    }

*/




/*
    static boolean access$1402(SoundListenRecord soundlistenrecord, boolean flag)
    {
        soundlistenrecord.isPaused = flag;
        return flag;
    }

*/


/*
    static int access$1502(SoundListenRecord soundlistenrecord, int i)
    {
        soundlistenrecord.listType = i;
        return i;
    }

*/







/*
    static float access$202(SoundListenRecord soundlistenrecord, float f)
    {
        soundlistenrecord.listenedPosition = f;
        return f;
    }

*/



/*
    static long access$2202(long l)
    {
        listenStartTime = l;
        return l;
    }

*/




/*
    static long access$302(SoundListenRecord soundlistenrecord, long l)
    {
        soundlistenrecord.lastTrackId = l;
        return l;
    }

*/



/*
    static long access$402(SoundListenRecord soundlistenrecord, long l)
    {
        soundlistenrecord.currentTrackId = l;
        return l;
    }

*/


/*
    static long access$502(SoundListenRecord soundlistenrecord, long l)
    {
        soundlistenrecord.lastProgramScheduleId = l;
        return l;
    }

*/


/*
    static long access$602(SoundListenRecord soundlistenrecord, long l)
    {
        soundlistenrecord.currentProgramScheduleId = l;
        return l;
    }

*/



/*
    static float access$702(SoundListenRecord soundlistenrecord, float f)
    {
        soundlistenrecord.listenedTime = f;
        return f;
    }

*/


/*
    static int access$802(SoundListenRecord soundlistenrecord, int i)
    {
        soundlistenrecord.duration = i;
        return i;
    }

*/


/*
    static String access$902(SoundListenRecord soundlistenrecord, String s)
    {
        soundlistenrecord.mRecSrc = s;
        return s;
    }

*/
}
