// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;


public class OnPlayerStatusUpdateListenerProxy
    implements TingMediaPlayer.OnPlayerStatusUpdateListener
{

    public OnPlayerStatusUpdateListenerProxy()
    {
    }

    public void onBufferUpdated(int i)
    {
    }

    public void onLogoPlayFinished()
    {
    }

    public void onPlayCompleted()
    {
        onPlayStateChange();
    }

    public void onPlayPaused()
    {
        onPlayStateChange();
    }

    public void onPlayProgressUpdate(int i, int j)
    {
    }

    public void onPlayStarted()
    {
        onPlayStateChange();
    }

    public void onPlayStateChange()
    {
    }

    public void onPlayerBuffering(boolean flag)
    {
    }

    public void onSoundPrepared(int i)
    {
    }

    public void onStartPlayLogo()
    {
    }
}
