// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.media.MediaPlayer;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            TingMediaPlayer

public static interface 
{

    public abstract void onAdFetchFail();

    public abstract void onAdFetched(List list);

    public abstract void onCompletePlayAd(MediaPlayer mediaplayer, List list);

    public abstract void onErroredPlayAd(MediaPlayer mediaplayer, List list);

    public abstract void onPausePlayAd(MediaPlayer mediaplayer);

    public abstract void onResumePlayAd(MediaPlayer mediaplayer);

    public abstract void onStartFetchAd();

    public abstract void onStartPlayAd(MediaPlayer mediaplayer, List list, int i);
}
