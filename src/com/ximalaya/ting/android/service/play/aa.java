// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.os.Handler;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.HistoryManage;
import com.ximalaya.ting.android.player.XMediaplayerImpl;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            TingMediaPlayer, PlayListControl

class aa
    implements Runnable
{

    final TingMediaPlayer a;

    aa(TingMediaPlayer tingmediaplayer)
    {
        a = tingmediaplayer;
        super();
    }

    public void run()
    {
        Object obj;
        int j;
        boolean flag;
        for (flag = false; a.mediaplayer == null || a.mediaPlayerState == 9;)
        {
            return;
        }

        int i;
        boolean flag1;
        try
        {
            i = a.getCurrentPosition();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            ((Exception) (obj)).printStackTrace();
            i = 0;
        }
        j = a.getDuration();
        TingMediaPlayer.access$902(a, i);
        obj = PlayListControl.getPlayListManager().getCurSound();
        if (obj != null && i > 0 && j > 0)
        {
            obj.history_listener = i;
            obj.history_duration = j;
        }
        if (i <= 0 || j <= 0) goto _L2; else goto _L1
_L1:
        if ((double)i >= 1.5D * (double)j)
        {
            break MISSING_BLOCK_LABEL_289;
        }
        flag1 = a.mediaplayer.isPlaying();
        flag = flag1;
_L3:
        if (flag)
        {
            a.mCurrentPosition = i;
            a.updateOnPlayProgressUpdate(i, j);
        }
        if (a.mediaPlayerState == 4 && !ToolUtil.isConnectToNetwork(TingMediaPlayer.access$000(a)))
        {
            TingMediaPlayer.access$1002(a, 1);
        }
        Exception exception;
        if (obj != null)
        {
            if (((SoundInfo) (obj)).history_listener + 10000L >= ((SoundInfo) (obj)).history_duration)
            {
                HistoryManage.getInstance(TingMediaPlayer.access$000(a)).removeHistoryListenerPositon(((SoundInfo) (obj)).trackId);
            } else
            {
                HistoryManage.getInstance(TingMediaPlayer.access$000(a)).saveHistoryListenerPosition(((SoundInfo) (obj)).trackId, ((SoundInfo) (obj)).history_listener);
            }
        }
        TingMediaPlayer.access$1100(a);
_L2:
        a.mUiHandler.postDelayed(TingMediaPlayer.access$1200(a), 1000L);
        return;
        exception;
        exception.printStackTrace();
          goto _L3
        Logger.logToSd((new StringBuilder()).append("mUpdateTimeTask error lastSeekPosition:").append(TingMediaPlayer.access$900(a)).append(" duration:").append(j).toString());
        MyLogger.getLogger().d((new StringBuilder()).append("mUpdateTimeTask error lastSeekPosition:").append(TingMediaPlayer.access$900(a)).append(" duration:").append(j).toString());
        a.seekTo(TingMediaPlayer.access$900(a));
          goto _L2
    }
}
