// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.finding2.rank.RankTrackListModel;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            Playlist

public class RankPlayList extends Playlist
{

    private static final int PAGE_SIZE = 30;
    private boolean mHasMore;

    public RankPlayList(List list)
    {
        super(list);
        mHasMore = true;
    }

    public boolean hasMore()
    {
        return mHasMore;
    }

    public List loadMore()
    {
        Object obj;
        if (mParams == null)
        {
            return null;
        }
        obj = new RequestParams();
        String s;
        for (Iterator iterator = mParams.keySet().iterator(); iterator.hasNext(); ((RequestParams) (obj)).put(s, (String)mParams.get(s)))
        {
            s = (String)iterator.next();
        }

        ((RequestParams) (obj)).put("pageId", mPageId);
        ((RequestParams) (obj)).put("pageSize", 30);
        obj = f.a().a(mDataSourceUrl, ((RequestParams) (obj)), true);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            break MISSING_BLOCK_LABEL_248;
        }
        obj = (RankTrackListModel)JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a, com/ximalaya/ting/android/model/finding2/rank/RankTrackListModel);
        if (((RankTrackListModel) (obj)).getList() != null && !((RankTrackListModel) (obj)).getList().isEmpty())
        {
            break MISSING_BLOCK_LABEL_156;
        }
        mHasMore = false;
        return null;
        if (((RankTrackListModel) (obj)).getList().size() < 30)
        {
            mHasMore = false;
        }
        if (((RankTrackListModel) (obj)).getList() == null || ((RankTrackListModel) (obj)).getList().size() <= 0)
        {
            break MISSING_BLOCK_LABEL_248;
        }
        mPageId = mPageId + 1;
        obj = ModelHelper.toSoundInfoListForRank(((RankTrackListModel) (obj)).getList());
        return ((List) (obj));
        Exception exception;
        exception;
        Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
        return null;
    }
}
