// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.content.Context;
import android.content.Intent;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            PlayListControl

class k
    implements LocalMediaService.OnPlayServiceUpdateListener
{

    private Context a;

    public k(Context context)
    {
        a = context;
    }

    private Intent a(String s)
    {
        s = new Intent(s);
        s.addFlags(0x10000000);
        return s;
    }

    public void onPlayCanceled()
    {
        a.sendBroadcast(a("com.ximalaya.ting.android.action.ACTION_PLAY_PAUSE"));
    }

    public void onSoundChanged(int i)
    {
        Logger.e("PlayUpdateListener", (new StringBuilder()).append("onSoundChanged ").append(i).toString());
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        Intent intent = a("com.ximalaya.ting.android.action.ACTION_PLAY_CHANGE_SOUND");
        intent.putExtra("ACTION_EXTRA_SOUNDINFO", JSON.toJSONString(soundinfo));
        a.sendBroadcast(intent);
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
    }
}
