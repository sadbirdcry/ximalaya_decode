// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.Utilities;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            PlayListControl, TingMediaPlayer

class s
    implements com.ximalaya.ting.android.util.ImageManager2.DisplayCallback
{

    final TingMediaPlayer a;

    s(TingMediaPlayer tingmediaplayer)
    {
        a = tingmediaplayer;
        super();
    }

    public void onCompleteDisplay(String s1, Bitmap bitmap)
    {
        if (!TextUtils.isEmpty(s1))
        {
            if ((bitmap = PlayListControl.getPlayListManager().getCurSound()) != null)
            {
                if (Utilities.isBlank(((SoundInfo) (bitmap)).coverLarge))
                {
                    bitmap = ((SoundInfo) (bitmap)).coverSmall;
                } else
                {
                    bitmap = ((SoundInfo) (bitmap)).coverLarge;
                }
                if (android.os.Build.VERSION.SDK_INT >= 16 && a.notification != null && a.notification.bigContentView != null && s1.equals(bitmap))
                {
                    s1 = (NotificationManager)TingMediaPlayer.access$000(a).getSystemService("notification");
                    try
                    {
                        s1.notify(3, a.notification);
                        return;
                    }
                    // Misplaced declaration of an exception variable
                    catch (String s1)
                    {
                        Logger.e(s1);
                    }
                    return;
                }
            }
        }
    }
}
