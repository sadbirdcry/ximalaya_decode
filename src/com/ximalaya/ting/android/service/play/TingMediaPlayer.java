// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.widget.RemoteViews;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.e.b;
import com.ximalaya.ting.android.fragment.livefm.LivePlayerFragment;
import com.ximalaya.ting.android.fragment.play.PlayerFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.ad.AbstractSoundAdModel;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.player.XMediaPlayerWrapper;
import com.ximalaya.ting.android.player.XMediaplayerImpl;
import com.ximalaya.ting.android.transaction.b.e;
import com.ximalaya.ting.android.transaction.c.j;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.PlanTerminateUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            r, aa, s, u, 
//            v, w, LocalMediaService, PlayListControl, 
//            Playlist, t, PlayTools, ab, 
//            ae, ad, ac, x, 
//            y, z

public class TingMediaPlayer
    implements com.ximalaya.ting.android.player.XMediaPlayer.OnBufferingUpdateListener, com.ximalaya.ting.android.player.XMediaPlayer.OnCompletionListener, com.ximalaya.ting.android.player.XMediaPlayer.OnErrorListener, com.ximalaya.ting.android.player.XMediaPlayer.OnInfoListener, com.ximalaya.ting.android.player.XMediaPlayer.OnPreparedListener, com.ximalaya.ting.android.player.XMediaPlayer.OnSeekCompleteListener
{
    public static interface LiveSoundUpdateCallback
    {

        public abstract void updateLiveSound();
    }

    public static interface MediaPlayerCompleteHandler
    {

        public abstract boolean handleComplete();
    }

    public static interface MediaPlayerErrorHandler
    {

        public abstract boolean handleError(int i, int k);
    }

    public static interface OnPlayerStatusUpdateListener
    {

        public abstract void onBufferUpdated(int i);

        public abstract void onLogoPlayFinished();

        public abstract void onPlayCompleted();

        public abstract void onPlayPaused();

        public abstract void onPlayProgressUpdate(int i, int k);

        public abstract void onPlayStarted();

        public abstract void onPlayerBuffering(boolean flag);

        public abstract void onSoundPrepared(int i);

        public abstract void onStartPlayLogo();
    }

    public static class SimpleOnPlayerStatusUpdateListener
        implements OnPlayerStatusUpdateListener
    {

        public void onBufferUpdated(int i)
        {
        }

        public void onLogoPlayFinished()
        {
        }

        public void onPlayCompleted()
        {
        }

        public void onPlayPaused()
        {
        }

        public void onPlayProgressUpdate(int i, int k)
        {
        }

        public void onPlayStarted()
        {
        }

        public void onPlayerBuffering(boolean flag)
        {
        }

        public void onSoundPrepared(int i)
        {
        }

        public void onStartPlayLogo()
        {
        }

        public SimpleOnPlayerStatusUpdateListener()
        {
        }
    }

    public static interface SoundAdCallback
    {

        public abstract void onAdFetchFail();

        public abstract void onAdFetched(List list);

        public abstract void onCompletePlayAd(MediaPlayer mediaplayer1, List list);

        public abstract void onErroredPlayAd(MediaPlayer mediaplayer1, List list);

        public abstract void onPausePlayAd(MediaPlayer mediaplayer1);

        public abstract void onResumePlayAd(MediaPlayer mediaplayer1);

        public abstract void onStartFetchAd();

        public abstract void onStartPlayAd(MediaPlayer mediaplayer1, List list, int i);
    }

    class a
        implements android.media.MediaPlayer.OnBufferingUpdateListener, android.media.MediaPlayer.OnCompletionListener, android.media.MediaPlayer.OnErrorListener, android.media.MediaPlayer.OnPreparedListener
    {

        final TingMediaPlayer a;
        private List b;
        private int c;
        private String d;
        private boolean e;
        private boolean f;

        public void onBufferingUpdate(MediaPlayer mediaplayer1, int i)
        {
            if (b != null && b.size() <= c)
            {
                ((AbstractSoundAdModel)b.get(c)).onBufferedPlayAd((AbstractSoundAdModel)b.get(c));
            }
        }

        public void onCompletion(MediaPlayer mediaplayer1)
        {
            if (b != null && c < b.size())
            {
                ((AbstractSoundAdModel)b.get(c)).onCompletePlayAd((AbstractSoundAdModel)b.get(c));
            }
            if (b == null || b.size() <= c + 1)
            {
                break MISSING_BLOCK_LABEL_390;
            }
            int i;
            mediaplayer1.reset();
            i = c;
_L1:
            if (i >= b.size())
            {
                break MISSING_BLOCK_LABEL_468;
            }
            c = c + 1;
            a.mPlayingSoundAd = (AbstractSoundAdModel)b.get(c);
            if (!TextUtils.isEmpty(a.mPlayingSoundAd.getISoundUrl()))
            {
                mediaplayer1.setDataSource(((AbstractSoundAdModel)b.get(c)).getISoundUrl());
                mediaplayer1.prepareAsync();
                a.mPlayingSoundAd.onPreparePlayAd((AbstractSoundAdModel)b.get(c));
                return;
            }
            Exception exception;
            Iterator iterator1;
            if (c != b.size() - 1)
            {
                break MISSING_BLOCK_LABEL_375;
            }
            if (a.mAdCallbacks != null)
            {
                for (Iterator iterator = a.mAdCallbacks.iterator(); iterator.hasNext(); ((SoundAdCallback)iterator.next()).onCompletePlayAd(mediaplayer1, b)) { }
            }
            try
            {
                a.installAudioSourceReal(d, e, f);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Exception exception)
            {
                exception.printStackTrace();
                a.installAudioSourceReal(d, e, f);
                if (a.mAdCallbacks != null)
                {
                    for (iterator1 = a.mAdCallbacks.iterator(); iterator1.hasNext(); ((SoundAdCallback)iterator1.next()).onErroredPlayAd(mediaplayer1, b)) { }
                }
            }
            break MISSING_BLOCK_LABEL_382;
            i++;
              goto _L1
            a.stopPlayAd();
            return;
            a.installAudioSourceReal(d, e, f);
            if (a.mAdCallbacks != null)
            {
                for (Iterator iterator2 = a.mAdCallbacks.iterator(); iterator2.hasNext(); ((SoundAdCallback)iterator2.next()).onCompletePlayAd(mediaplayer1, b)) { }
            }
            a.stopPlayAd();
        }

        public boolean onError(MediaPlayer mediaplayer1, int i, int k)
        {
            if (b != null && c < b.size())
            {
                ((AbstractSoundAdModel)b.get(c)).onCompletePlayAd((AbstractSoundAdModel)b.get(c));
            }
            a.installAudioSourceReal(d, e, f);
            if (a.mAdCallbacks != null)
            {
                for (Iterator iterator = a.mAdCallbacks.iterator(); iterator.hasNext(); ((SoundAdCallback)iterator.next()).onErroredPlayAd(mediaplayer1, b)) { }
            }
            a.stopPlayAd();
            return true;
        }

        public void onPrepared(MediaPlayer mediaplayer1)
        {
            if (b != null && c < b.size())
            {
                ((AbstractSoundAdModel)b.get(c)).onPreparedPlayAd((AbstractSoundAdModel)b.get(c));
            }
            mediaplayer1.start();
            a.mediaPlayerState = 10;
            if (a.mAdCallbacks != null)
            {
                for (Iterator iterator = a.mAdCallbacks.iterator(); iterator.hasNext(); ((SoundAdCallback)iterator.next()).onStartPlayAd(mediaplayer1, b, c)) { }
            }
            if (b != null && c < b.size())
            {
                ((AbstractSoundAdModel)b.get(c)).onStartPlayAd((AbstractSoundAdModel)b.get(c));
            }
        }

        public a(List list, int i, String s1, boolean flag, boolean flag1)
        {
            a = TingMediaPlayer.this;
            super();
            c = 0;
            b = list;
            c = i;
            d = s1;
            e = flag;
            f = flag1;
        }
    }


    private static final Object INSTANCE_LOCK = new Object();
    protected static final String TAG = com/ximalaya/ting/android/service/play/TingMediaPlayer.getSimpleName();
    private static volatile TingMediaPlayer tingMediaPlayer = null;
    private MediaPlayer adsPlayer;
    private Context appContext;
    private AudioManager audioManager;
    public volatile int bufferingPercent;
    private ConnectivityManager connectivityManager;
    public boolean installToStart;
    public boolean isPlayingAds;
    private int lastSeekPosition;
    private Object mAdCallbackLock;
    private CopyOnWriteArrayList mAdCallbacks;
    private BroadcastReceiver mBroadcastReceiver;
    private BroadcastReceiver mBroadcastReceiverPhoneCall;
    private MediaPlayerCompleteHandler mCompleteHandler;
    private String mCurrentAudioSrc;
    public int mCurrentPosition;
    public int mDuration;
    private MediaPlayerErrorHandler mErrorHandler;
    private XMediaplayerImpl mLastMediaPlayer;
    private LiveSoundUpdateCallback mLiveSoundUpdateCallback;
    private PhoneStateListener mPhoneStateListener;
    public List mPlayerStatusUpdateListenerList;
    private AbstractSoundAdModel mPlayingSoundAd;
    private volatile j mProxy;
    private com.ximalaya.ting.android.util.ImageManager2.DisplayCallback mRemoteViewCallback;
    private boolean mSoundAdPlayerPaused;
    private MediaPlayer mSoundPatchAdPlayer;
    protected final Handler mUiHandler = new Handler(Looper.getMainLooper());
    private Runnable mUpdateTimeTask;
    private boolean mUseProxy;
    public volatile int mediaPlayerState;
    public volatile XMediaplayerImpl mediaplayer;
    private int netChangedReplayState;
    private NetworkInfo netInfo;
    public Notification notification;
    private android.media.AudioManager.OnAudioFocusChangeListener pafcl;
    private int saveLastPlayerStatus;
    private int saveLastPositon;
    private long startPlayPosition;
    private boolean telPauseFlag;
    private TelephonyManager telephonyManager;
    private TelephonyManager telephonyManager1;
    private TelephonyManager telephonyManager2;

    private TingMediaPlayer(Context context)
    {
        appContext = null;
        mProxy = null;
        mediaplayer = null;
        mediaPlayerState = 0;
        notification = null;
        telPauseFlag = false;
        bufferingPercent = 0;
        installToStart = true;
        startPlayPosition = 0L;
        netChangedReplayState = 0;
        saveLastPositon = 0;
        saveLastPlayerStatus = 4;
        lastSeekPosition = 0;
        isPlayingAds = false;
        adsPlayer = null;
        mCurrentPosition = 0;
        mDuration = 0;
        pafcl = new r(this);
        mAdCallbackLock = new Object();
        mUpdateTimeTask = new aa(this);
        mRemoteViewCallback = new s(this);
        mPlayerStatusUpdateListenerList = Collections.synchronizedList(new CopyOnWriteArrayList());
        mBroadcastReceiverPhoneCall = new u(this);
        mBroadcastReceiver = new v(this);
        mPhoneStateListener = new w(this);
        appContext = context;
        mediaplayer = newXMediaplayerImpl();
        setupMediaPlayer();
        initListener();
    }

    private void callStateIdle()
    {
        if (isPausedSoundAd())
        {
            resumePlaySoundAd();
        }
        if (!isPlayingSoundAd());
        if (mediaplayer != null)
        {
            try
            {
                if (!mediaplayer.isPlaying() && telPauseFlag)
                {
                    start();
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        telPauseFlag = false;
    }

    private void callStateRinging()
    {
        if (isPlayingSoundAd())
        {
            pausePlaySoundAd();
        }
        if (mediaplayer == null)
        {
            break MISSING_BLOCK_LABEL_48;
        }
        if (mediaplayer.isPlaying())
        {
            if (mediaPlayerState == 4)
            {
                telPauseFlag = true;
            }
            pause();
        }
        return;
        Exception exception;
        exception;
    }

    private void createNotificationPlayerController()
    {
        this;
        JVM INSTR monitorenter ;
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        int i = android.os.Build.VERSION.SDK_INT;
        Object obj;
        if (PlayListControl.getPlayListManager().getPlaylist() == null)
        {
            break MISSING_BLOCK_LABEL_892;
        }
        obj = PlayListControl.getPlayListManager().getPlaylist().getData();
_L28:
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo == null) goto _L1; else goto _L3
_L3:
        if (i < 14)
        {
            break MISSING_BLOCK_LABEL_742;
        }
        if (i < 16) goto _L5; else goto _L4
_L4:
        android.app.Notification.Builder builder = new android.app.Notification.Builder(appContext);
        builder.setSmallIcon(0x7f020599);
        notification = builder.build();
_L24:
        Intent intent;
        notification.icon = 0x7f020598;
        notification.tickerText = "\u559C\u9A6C\u62C9\u96C5";
        notification.when = System.currentTimeMillis();
        intent = new Intent(appContext, com/ximalaya/ting/android/activity/MainTabActivity2);
        intent.addFlags(0x10000000);
        notification.contentView = new RemoteViews(appContext.getPackageName(), 0x7f030181);
        if (soundinfo.category != 0) goto _L7; else goto _L6
_L6:
        notification.contentView.setTextViewText(0x7f0a05d8, soundinfo.title);
        notification.contentView.setTextViewText(0x7f0a05d9, soundinfo.nickname);
        intent.putExtra(com.ximalaya.ting.android.a.t, com/ximalaya/ting/android/fragment/play/PlayerFragment);
_L25:
        if (i < 16) goto _L9; else goto _L8
_L8:
        RemoteViews remoteviews = new RemoteViews(appContext.getPackageName(), 0x7f030182);
        notification.bigContentView = remoteviews;
        if (soundinfo.category != 0) goto _L11; else goto _L10
_L10:
        notification.bigContentView.setTextViewText(0x7f0a05d8, soundinfo.title);
        notification.bigContentView.setTextViewText(0x7f0a05d9, soundinfo.nickname);
_L26:
        notification.bigContentView.setOnClickPendingIntent(0x7f0a05da, returnPendingIntentByTag("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_EXIT"));
        if (!Utilities.isBlank(soundinfo.coverLarge)) goto _L13; else goto _L12
_L12:
        String s1 = soundinfo.coverSmall;
_L27:
        mUiHandler.post(new t(this, s1));
_L9:
        if (obj == null) goto _L15; else goto _L14
_L14:
        if (((List) (obj)).size() <= 0) goto _L15; else goto _L16
_L16:
        int k;
        int l;
        k = PlayListControl.getPlayListManager().curIndex;
        l = PlayListControl.getPlayListManager().getSize();
        if (soundinfo.category != 0) goto _L18; else goto _L17
_L17:
        if (k != 0) goto _L20; else goto _L19
_L19:
        disablePreBtn(true);
        if (l != 1) goto _L22; else goto _L21
_L21:
        disableNextBtn(true);
_L15:
        notification.contentView.setOnClickPendingIntent(0x7f0a05d7, returnPendingIntentByTag("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_SWITCH_PLAY_PAUSE"));
        if (i < 16)
        {
            break MISSING_BLOCK_LABEL_463;
        }
        notification.bigContentView.setOnClickPendingIntent(0x7f0a05d7, returnPendingIntentByTag("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_SWITCH_PLAY_PAUSE"));
        intent.addFlags(0x10000000);
        obj = PendingIntent.getActivity(appContext, (int)notification.when, intent, 0x8000000);
        notification.contentIntent = ((PendingIntent) (obj));
        localmediaservice.startForeground(3, notification);
          goto _L1
_L5:
        notification = new Notification();
        if (true) goto _L24; else goto _L23
_L23:
        JVM INSTR monitorexit ;
        throw obj;
_L7:
        notification.contentView.setTextViewText(0x7f0a05d8, soundinfo.radioName);
        notification.contentView.setTextViewText(0x7f0a05d9, soundinfo.programName);
        intent.putExtra(com.ximalaya.ting.android.a.t, com/ximalaya/ting/android/fragment/livefm/LivePlayerFragment);
          goto _L25
_L11:
        notification.bigContentView.setTextViewText(0x7f0a05d8, soundinfo.radioName);
        notification.bigContentView.setTextViewText(0x7f0a05d9, soundinfo.programName);
          goto _L26
_L13:
        s1 = soundinfo.coverLarge;
          goto _L27
_L22:
        disableNextBtn(false);
          goto _L15
_L20:
        if (k != l - 1)
        {
            break MISSING_BLOCK_LABEL_675;
        }
        disableNextBtn(true);
        if (l < 2)
        {
            break MISSING_BLOCK_LABEL_667;
        }
        disablePreBtn(false);
          goto _L15
        disablePreBtn(true);
          goto _L15
        disablePreBtn(false);
        disableNextBtn(false);
          goto _L15
_L18:
        if (soundinfo.category != 1)
        {
            break MISSING_BLOCK_LABEL_729;
        }
        if (l <= 1)
        {
            break MISSING_BLOCK_LABEL_716;
        }
        disablePreBtn(false);
        disableNextBtn(true);
          goto _L15
        disablePreBtn(true);
        disableNextBtn(true);
          goto _L15
        disablePreBtn(false);
        disableNextBtn(false);
          goto _L15
        try
        {
            notification = new Notification();
            notification.icon = 0x7f020598;
            notification.tickerText = "\u559C\u9A6C\u62C9\u96C5";
            notification.when = System.currentTimeMillis();
            obj = new Intent(appContext, com/ximalaya/ting/android/activity/MainTabActivity2);
            ((Intent) (obj)).putExtra(com.ximalaya.ting.android.a.t, com/ximalaya/ting/android/fragment/play/PlayerFragment);
            notification.contentView = new RemoteViews(appContext.getPackageName(), 0x7f030183);
            notification.contentView.setTextViewText(0x7f0a05d8, soundinfo.title);
            ((Intent) (obj)).addFlags(0x10000000);
            obj = PendingIntent.getActivity(appContext, 0, ((Intent) (obj)), 0x8000000);
            notification.contentIntent = ((PendingIntent) (obj));
            localmediaservice.startForeground(3, notification);
        }
        catch (Exception exception) { }
        finally
        {
            this;
        }
          goto _L1
        obj = null;
          goto _L28
    }

    private void disableNextBtn(boolean flag)
    {
        int k = android.os.Build.VERSION.SDK_INT;
        String s1;
        int i;
        if (k >= 14 && notification != null && notification.contentView != null)
        {
            if (flag)
            {
                i = 0x7f0203aa;
                s1 = "com.ximalaya.ting.android.action.ACTION_NOTIFICATION_DO_NOTHING";
            } else
            {
                i = 0x7f0203ad;
                s1 = "com.ximalaya.ting.android.action.ACTION_NOTIFICATION_NEXT";
            }
            notification.contentView.setImageViewResource(0x7f0a05d6, i);
            notification.contentView.setOnClickPendingIntent(0x7f0a05d6, returnPendingIntentByTag(s1));
            if (k > 14)
            {
                RemoteViews remoteviews = notification.contentView;
                if (flag)
                {
                    s1 = "\u5DF2\u505C\u7528\u7684\u4E0B\u4E00\u9996";
                } else
                {
                    s1 = "\u4E0B\u4E00\u9996";
                }
                remoteviews.setContentDescription(0x7f0a05d6, s1);
            }
        }
        if (k >= 16 && notification != null && notification.bigContentView != null)
        {
            if (flag)
            {
                i = 0x7f0203a3;
                s1 = "com.ximalaya.ting.android.action.ACTION_NOTIFICATION_DO_NOTHING";
            } else
            {
                i = 0x7f0203a4;
                s1 = "com.ximalaya.ting.android.action.ACTION_NOTIFICATION_NEXT";
            }
            notification.bigContentView.setImageViewResource(0x7f0a05d6, i);
            notification.bigContentView.setOnClickPendingIntent(0x7f0a05d6, returnPendingIntentByTag(s1));
            remoteviews = notification.bigContentView;
            if (flag)
            {
                s1 = "\u5DF2\u505C\u7528\u7684\u4E0B\u4E00\u9996";
            } else
            {
                s1 = "\u4E0B\u4E00\u9996";
            }
            remoteviews.setContentDescription(0x7f0a05d6, s1);
        }
    }

    private void disablePreBtn(boolean flag)
    {
        if (android.os.Build.VERSION.SDK_INT >= 16 && notification != null && notification.bigContentView != null)
        {
            String s1;
            String s2;
            RemoteViews remoteviews;
            int i;
            if (flag)
            {
                i = 0x7f020445;
                s1 = "com.ximalaya.ting.android.action.ACTION_NOTIFICATION_DO_NOTHING";
            } else
            {
                i = 0x7f020446;
                s1 = "com.ximalaya.ting.android.action.ACTION_NOTIFICATION_PRE";
            }
            notification.bigContentView.setImageViewResource(0x7f0a05db, i);
            remoteviews = notification.bigContentView;
            if (flag)
            {
                s2 = "\u5DF2\u505C\u7528\u7684\u4E0A\u4E00\u9996";
            } else
            {
                s2 = "\u4E0A\u4E00\u9996";
            }
            remoteviews.setContentDescription(0x7f0a05db, s2);
            notification.bigContentView.setOnClickPendingIntent(0x7f0a05db, returnPendingIntentByTag(s1));
        }
    }

    private void disableWifiLock()
    {
        com.ximalaya.ting.android.e.a a1 = com.ximalaya.ting.android.e.a.a(appContext);
        if (a1 != null)
        {
            a1.b();
        }
    }

    private void enableWifiLock()
    {
        Object obj;
        obj = LocalMediaService.getInstance();
        if (obj == null || !((LocalMediaService) (obj)).isPlayFromNetwork() || NetworkUtils.getNetType(appContext) != 1)
        {
            break MISSING_BLOCK_LABEL_42;
        }
        obj = com.ximalaya.ting.android.e.a.a(appContext);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_42;
        }
        ((com.ximalaya.ting.android.e.a) (obj)).a();
        return;
        Exception exception;
        exception;
    }

    private int getNowSound()
    {
        return audioManager.getStreamVolume(1);
    }

    public static final TingMediaPlayer getTingMediaPlayer(Context context)
    {
        if (tingMediaPlayer == null)
        {
            synchronized (INSTANCE_LOCK)
            {
                if (tingMediaPlayer == null)
                {
                    tingMediaPlayer = new TingMediaPlayer(context);
                }
            }
        }
        return tingMediaPlayer;
        context;
        obj;
        JVM INSTR monitorexit ;
        throw context;
    }

    private void initListener()
    {
        if (appContext == null)
        {
            return;
        } else
        {
            audioManager = (AudioManager)appContext.getSystemService("audio");
            initTelephonyManager();
            IntentFilter intentfilter = new IntentFilter();
            intentfilter.addAction("android.intent.action.HEADSET_PLUG");
            intentfilter.addAction("android.media.AUDIO_BECOMING_NOISY");
            intentfilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            intentfilter.addAction("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_SWITCH_PLAY_PAUSE");
            intentfilter.addAction("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_NEXT");
            intentfilter.addAction("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_PRE");
            intentfilter.addAction("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_EXIT");
            appContext.registerReceiver(mBroadcastReceiver, intentfilter);
            intentfilter = new IntentFilter();
            intentfilter.addAction("android.intent.action.HEADSET_PLUG");
            appContext.registerReceiver(mBroadcastReceiverPhoneCall, intentfilter);
            return;
        }
    }

    private void initTelephonyManager()
    {
        telephonyManager = (TelephonyManager)appContext.getSystemService("phone");
        telephonyManager.listen(mPhoneStateListener, 32);
        try
        {
            telephonyManager1 = (TelephonyManager)appContext.getSystemService("phone1");
            telephonyManager1.listen(mPhoneStateListener, 32);
        }
        catch (Exception exception1) { }
        try
        {
            telephonyManager2 = (TelephonyManager)appContext.getSystemService("phone2");
            telephonyManager2.listen(mPhoneStateListener, 32);
            return;
        }
        catch (Exception exception)
        {
            return;
        }
    }

    private void installAudioSourceReal(String s1, boolean flag, boolean flag1)
    {
        Object obj;
        Object obj1;
        MyLogger.getLogger().print();
        if (TextUtils.isEmpty(s1))
        {
            return;
        }
        Logger.log("installAudioSourceReal updateOnPlayerBuffering 003 true");
        updateOnPlayerBuffering(true);
        int i;
        try
        {
            mediaplayer.reset();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            mLastMediaPlayer = mediaplayer;
            releaseMediaPlayer(mLastMediaPlayer);
            mediaplayer = newXMediaplayerImpl();
            setupMediaPlayer();
        }
        mCurrentAudioSrc = s1;
        mediaPlayerState = 0;
        obj = PlayListControl.getPlayListManager().getCurSound();
        if (obj != null)
        {
            if (((SoundInfo) (obj)).category == 1 || ((SoundInfo) (obj)).category == 2)
            {
                startPlayPosition = 0L;
            }
            Logger.logToSd((new StringBuilder()).append("\u5F00\u59CB\u64AD\u653Ename:").append(((SoundInfo) (obj)).title).append(" trackId:").append(((SoundInfo) (obj)).trackId).append(" url:").append(mCurrentAudioSrc).toString());
            MyLogger.getLogger().d((new StringBuilder()).append("\u5F00\u59CB\u64AD\u653Ename:").append(((SoundInfo) (obj)).programName).append(" programScheduleId:").append(((SoundInfo) (obj)).programScheduleId).append(" url:").append(mCurrentAudioSrc).toString());
        }
        if (!s1.contains("http") || !mediaplayer.isUseSystemPlayer()) goto _L2; else goto _L1
_L1:
        i = android.os.Build.VERSION.SDK_INT;
        if (((SoundInfo) (obj)).category != 0) goto _L4; else goto _L3
_L3:
        if (mProxy == null || !flag1) goto _L6; else goto _L5
_L5:
        if (com.ximalaya.ting.android.a.Z && i >= 9) goto _L7; else goto _L6
_L6:
        if (!FreeFlowUtil.getInstance().isNeedFreeFlowProxy()) goto _L4; else goto _L7
_L7:
        obj = mProxy.a(s1);
        mUseProxy = true;
_L10:
        flag1 = PlayTools.isLocalAudioSource(((String) (obj)));
        if (!flag1) goto _L9; else goto _L8
_L8:
        obj1 = null;
        s1 = new FileInputStream(((String) (obj)));
        mediaplayer.setDataSource(s1.getFD(), ((String) (obj)));
        mediaplayer.prepareAsync();
        if (s1 == null)
        {
            break MISSING_BLOCK_LABEL_329;
        }
        try
        {
            s1.close();
        }
        // Misplaced declaration of an exception variable
        catch (String s1) { }
        mediaPlayerState = 1;
        mediaPlayerState = 2;
        installToStart = flag;
        createNotificationPlayerController();
        PlayListControl.savePlayList();
        return;
_L4:
        try
        {
            mUseProxy = false;
        }
        // Misplaced declaration of an exception variable
        catch (String s1)
        {
            s1.printStackTrace();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s1)
        {
            s1.printStackTrace();
            return;
        }
        obj = s1;
          goto _L10
_L2:
        mUseProxy = false;
        obj = s1;
          goto _L10
        s1;
        obj = obj1;
_L11:
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_434;
        }
        try
        {
            ((FileInputStream) (obj)).close();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
        throw s1;
_L9:
        mediaplayer.setDataSource(((String) (obj)));
        mediaplayer.prepareAsync();
        break MISSING_BLOCK_LABEL_329;
        s1;
        s1 = null;
_L12:
        if (s1 == null)
        {
            break MISSING_BLOCK_LABEL_329;
        }
        s1.close();
        break MISSING_BLOCK_LABEL_329;
        Exception exception1;
        exception1;
        obj = s1;
        s1 = exception1;
          goto _L11
        Exception exception;
        exception;
          goto _L12
    }

    private void makeNotificationAtNetworkChanged()
    {
        String s1 = PlayListControl.getPlayListManager().curPlaySrc;
        if (s1 != null && s1.contains("http") && mediaplayer != null && mediaplayer.isPlaying() && netChangedReplayState == 1 && bufferingPercent > 0 && bufferingPercent < 100 && ((long)bufferingPercent * (long)getDuration()) / 100L - (long)getCurrentPosition() <= 5000L)
        {
            int i = NetworkUtils.getNetType(appContext);
            pause();
            if (appContext != null)
            {
                if (MyApplication.a() != null && !MyApplication.a().isFinishing())
                {
                    if (i == 0 && !isForegroundIsMyApplication())
                    {
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.VIEW");
                        intent.setData(Uri.parse("iting://open"));
                        intent.addFlags(0x10000000);
                        MyApplication.a().startActivity(intent);
                    }
                    mUiHandler.post(new ab(this));
                    return;
                }
                if (i == -1)
                {
                    ToolUtil.makePlayNotification(appContext, "\u559C\u9A6C\u62C9\u96C5", "\u6E29\u99A8\u63D0\u793A", "\u6CA1\u6709\u53EF\u7528\u7F51\u7EDC, \u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u7EE7\u7EED\u64AD\u653E");
                    showToast("\u6CA1\u6709\u53EF\u7528\u7F51\u7EDC, \u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u7EE7\u7EED\u64AD\u653E");
                    return;
                }
                if (FreeFlowUtil.getInstance().isNeedFreeFlowProxy())
                {
                    replay(true);
                    return;
                }
                if (i == 0)
                {
                    ToolUtil.makePlayNotification(appContext, "\u559C\u9A6C\u62C9\u96C5", "\u6E29\u99A8\u63D0\u793A", "\u559C\u9A6C\u62C9\u96C5\u5C06\u8981\u4F7F\u7528\u4F60\u7684\u79FB\u52A8\u6D41\u91CF\u64AD\u653E\u58F0\u97F3,\u8981\u7EE7\u7EED\u5417\uFF1F");
                    showToast("\u559C\u9A6C\u62C9\u96C5\u5C06\u8981\u4F7F\u7528\u4F60\u7684\u79FB\u52A8\u6D41\u91CF\u64AD\u653E\u58F0\u97F3\uFF0C\u8BF7\u6253\u5F00\u7A0B\u5E8F\u70B9\u51FB\u786E\u8BA4");
                    return;
                }
            }
        }
    }

    private XMediaplayerImpl newXMediaplayerImpl()
    {
        XMediaPlayerWrapper xmediaplayerwrapper = new XMediaPlayerWrapper(SharedPreferencesUtil.getInstance(appContext).getBoolean("use_sys_player", false));
        if (FreeFlowUtil.getInstance().isNeedFreeFlowProxy())
        {
            FreeFlowUtil.getInstance().setProxyForXMediaPlayer(xmediaplayerwrapper);
        }
        return xmediaplayerwrapper;
    }

    private void playAd(List list, String s1, boolean flag, boolean flag1)
    {
        pause();
        stopPlayAd();
        if (list == null || list.size() <= 0) goto _L2; else goto _L1
_L1:
        int i = 0;
_L7:
        if (i >= list.size())
        {
            break MISSING_BLOCK_LABEL_319;
        }
        mPlayingSoundAd = (AbstractSoundAdModel)list.get(i);
        if (TextUtils.isEmpty(mPlayingSoundAd.getISoundUrl())) goto _L4; else goto _L3
_L3:
        if (mPlayingSoundAd != null && !TextUtils.isEmpty(mPlayingSoundAd.getISoundUrl())) goto _L6; else goto _L5
_L4:
        i++;
          goto _L7
_L5:
        if (mAdCallbacks != null)
        {
            for (Iterator iterator = mAdCallbacks.iterator(); iterator.hasNext(); ((SoundAdCallback)iterator.next()).onCompletePlayAd(null, list)) { }
        }
        installAudioSourceReal(s1, flag, flag1);
_L8:
        return;
_L6:
        mSoundPatchAdPlayer = new MediaPlayer();
        a a1 = new a(list, i, s1, flag, flag1);
        mSoundPatchAdPlayer.setOnPreparedListener(a1);
        mSoundPatchAdPlayer.setOnCompletionListener(a1);
        mSoundPatchAdPlayer.setOnErrorListener(a1);
        mSoundPatchAdPlayer.setOnBufferingUpdateListener(a1);
        try
        {
            mSoundPatchAdPlayer.setDataSource(mPlayingSoundAd.getISoundUrl());
            mSoundPatchAdPlayer.prepareAsync();
            mPlayingSoundAd.onPreparePlayAd(mPlayingSoundAd);
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        stopPlayAd();
        installAudioSourceReal(s1, flag, flag1);
        if (mAdCallbacks != null)
        {
            s1 = mAdCallbacks.iterator();
            while (s1.hasNext()) 
            {
                ((SoundAdCallback)s1.next()).onErroredPlayAd(mSoundPatchAdPlayer, list);
            }
        }
        if (true) goto _L8; else goto _L2
_L2:
        installAudioSourceReal(s1, flag, flag1);
        return;
        i = 0;
          goto _L3
    }

    private void playNext(boolean flag)
    {
        if (PlanTerminateUtil.isStopPlayWhenCurrSoundFinish(appContext))
        {
            PlanTerminateUtil.clearAutoFinishData(appContext);
        } else
        if (LocalMediaService.getInstance() != null)
        {
            LocalMediaService.getInstance().playNext(flag);
            return;
        }
    }

    private final void reinstallAudioSource(boolean flag)
    {
        this;
        JVM INSTR monitorenter ;
        String s1 = PlayListControl.getPlayListManager().curPlaySrc;
        if (mediaplayer == null) goto _L2; else goto _L1
_L1:
        boolean flag1 = Utilities.isBlank(s1);
        if (!flag1) goto _L3; else goto _L2
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
_L3:
        MyLogger.getLogger().print();
        installAudioSourceReal(s1, installToStart, flag);
        if (true) goto _L2; else goto _L4
_L4:
        Exception exception;
        exception;
        throw exception;
    }

    private void releaseMediaPlayer(XMediaplayerImpl xmediaplayerimpl)
    {
        if (xmediaplayerimpl == null)
        {
            return;
        } else
        {
            xmediaplayerimpl.setOnCompletionListener(null);
            xmediaplayerimpl.setOnPreparedListener(null);
            xmediaplayerimpl.setOnBufferingUpdateListener(null);
            xmediaplayerimpl.setOnSeekCompleteListener(null);
            xmediaplayerimpl.setOnInfoListener(null);
            xmediaplayerimpl.setOnErrorListener(null);
            xmediaplayerimpl.release();
            return;
        }
    }

    private void removeListener()
    {
        if (appContext != null)
        {
            appContext.unregisterReceiver(mBroadcastReceiver);
            appContext.unregisterReceiver(mBroadcastReceiverPhoneCall);
            if (telephonyManager != null)
            {
                telephonyManager.listen(mPhoneStateListener, 0);
            }
            if (telephonyManager1 != null)
            {
                telephonyManager1.listen(mPhoneStateListener, 0);
            }
            if (telephonyManager2 != null)
            {
                telephonyManager2.listen(mPhoneStateListener, 0);
                return;
            }
        }
    }

    private final void replay(boolean flag)
    {
        replay(flag, 0);
    }

    private final void replay(boolean flag, int i)
    {
        MyLogger.getLogger().print();
        if (MyApplication.i)
        {
            return;
        } else
        {
            startPlayPosition = i;
            reinstallAudioSource(flag);
            return;
        }
    }

    private void resetStatus()
    {
        telPauseFlag = false;
        bufferingPercent = 0;
        netChangedReplayState = 0;
        saveLastPositon = 0;
        saveLastPlayerStatus = 4;
        lastSeekPosition = 0;
        isPlayingAds = false;
        stopUpdateProgress();
        if (adsPlayer != null)
        {
            adsPlayer.release();
            adsPlayer = null;
        }
        startPlayPosition = 0L;
        mCurrentPosition = 0;
        mDuration = 0;
        updateOnPlayProgressUpdate(0, 100);
        Logger.log("resetStatus updateOnPlayerBuffering 005:false");
        updateOnPlayerBuffering(false);
    }

    private PendingIntent returnPendingIntentByTag(String s1)
    {
        if (s1.equals("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_SWITCH_PLAY_PAUSE"))
        {
            return PendingIntent.getBroadcast(appContext, 0, new Intent("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_SWITCH_PLAY_PAUSE"), 0x8000000);
        }
        if (s1.equals("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_NEXT"))
        {
            return PendingIntent.getBroadcast(appContext, 0, new Intent("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_NEXT"), 0x8000000);
        }
        if (s1.equals("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_DO_NOTHING"))
        {
            return PendingIntent.getBroadcast(appContext, 0, new Intent("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_DO_NOTHING"), 0x8000000);
        }
        if (s1.equals("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_PRE"))
        {
            return PendingIntent.getBroadcast(appContext, 0, new Intent("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_PRE"), 0x8000000);
        }
        if (s1.equals("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_EXIT"))
        {
            return PendingIntent.getBroadcast(appContext, 0, new Intent("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_EXIT"), 0x8000000);
        } else
        {
            return PendingIntent.getBroadcast(appContext, 0, new Intent("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_DO_NOTHING"), 0x8000000);
        }
    }

    private final void setupMediaPlayer()
    {
        if (mediaplayer == null)
        {
            return;
        } else
        {
            mediaplayer.setWakeMode(appContext, 1);
            mediaplayer.setAudioStreamType(3);
            mediaplayer.setOnCompletionListener(this);
            mediaplayer.setOnPreparedListener(this);
            mediaplayer.setOnBufferingUpdateListener(this);
            mediaplayer.setOnSeekCompleteListener(this);
            mediaplayer.setOnInfoListener(this);
            mediaplayer.setOnErrorListener(this);
            mProxy = j.a();
            return;
        }
    }

    private void showNoWifiConfirm()
    {
        (new DialogBuilder(MyApplication.a())).setTitle("\u6D41\u91CF\u63D0\u9192").setMessage(0x7f090150).setOkBtn("\u603B\u662F\u5141\u8BB8", new ae(this)).setNeutralBtn("\u5141\u8BB8\u672C\u6B21", new ad(this)).setCancelBtn(new ac(this)).showMultiButton();
    }

    private void showToast(String s1)
    {
        if (!isForegroundIsMyApplication())
        {
            CustomToast.showToast(appContext, s1, 0);
        }
    }

    private void startUpdateProgress()
    {
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo != null && soundinfo.category != 1)
        {
            mUiHandler.postDelayed(mUpdateTimeTask, 1000L);
        }
    }

    private void stopUpdateProgress()
    {
        mUiHandler.removeCallbacks(mUpdateTimeTask);
    }

    private void updateNoification(int i)
    {
        this;
        JVM INSTR monitorenter ;
        int k = android.os.Build.VERSION.SDK_INT;
        if (k < 14) goto _L2; else goto _L1
_L1:
        if (notification == null) goto _L2; else goto _L3
_L3:
        NotificationManager notificationmanager = (NotificationManager)appContext.getSystemService("notification");
        i;
        JVM INSTR tableswitch 4 5: default 60
    //                   4 264
    //                   5 160;
           goto _L4 _L5 _L6
_L4:
        notification.contentView.setImageViewResource(0x7f0a05d7, 0x7f0203b3);
        if (k < 16) goto _L8; else goto _L7
_L7:
        if (notification.bigContentView != null)
        {
            notification.bigContentView.setImageViewResource(0x7f0a05d7, 0x7f0203b3);
            notification.bigContentView.setContentDescription(0x7f0a05d7, "\u5F00\u59CB\u64AD\u653E");
        }
_L8:
        if (k <= 14)
        {
            break MISSING_BLOCK_LABEL_148;
        }
        notification.contentView.setContentDescription(0x7f0a05d7, "\u5F00\u59CB\u64AD\u653E");
_L12:
        notificationmanager.notify(3, notification);
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
_L6:
        notification.contentView.setImageViewResource(0x7f0a05d7, 0x7f0203b3);
        if (k < 16) goto _L10; else goto _L9
_L9:
        if (notification.bigContentView != null)
        {
            notification.bigContentView.setImageViewResource(0x7f0a05d7, 0x7f0203b3);
            notification.bigContentView.setContentDescription(0x7f0a05d7, "\u5F00\u59CB\u64AD\u653E");
        }
_L10:
        if (k <= 14) goto _L12; else goto _L11
_L11:
        notification.contentView.setContentDescription(0x7f0a05d7, "\u5F00\u59CB\u64AD\u653E");
          goto _L12
        Exception exception1;
        exception1;
        Logger.e(exception1);
          goto _L12
        Exception exception;
        exception;
        throw exception;
_L5:
        notification.contentView.setImageViewResource(0x7f0a05d7, 0x7f0203b0);
        if (k < 16) goto _L14; else goto _L13
_L13:
        if (notification.bigContentView != null)
        {
            notification.bigContentView.setImageViewResource(0x7f0a05d7, 0x7f0203b0);
            notification.bigContentView.setContentDescription(0x7f0a05d7, "\u6682\u505C");
        }
_L14:
        if (k <= 14) goto _L12; else goto _L15
_L15:
        notification.contentView.setContentDescription(0x7f0a05d7, "\u6682\u505C");
          goto _L12
    }

    public void addSoundAdCallback(SoundAdCallback soundadcallback)
    {
        synchronized (mAdCallbackLock)
        {
            if (mAdCallbacks == null)
            {
                mAdCallbacks = new CopyOnWriteArrayList();
            }
            mAdCallbacks.add(soundadcallback);
        }
        return;
        soundadcallback;
        obj;
        JVM INSTR monitorexit ;
        throw soundadcallback;
    }

    public final int getCurrentPosition()
    {
        if (mediaplayer == null) goto _L2; else goto _L1
_L1:
        mediaPlayerState;
        JVM INSTR tableswitch 0 8: default 60
    //                   0 68
    //                   1 68
    //                   2 60
    //                   3 68
    //                   4 68
    //                   5 68
    //                   6 68
    //                   7 68
    //                   8 81;
           goto _L2 _L3 _L3 _L2 _L3 _L3 _L3 _L3 _L3 _L4
_L4:
        break MISSING_BLOCK_LABEL_81;
_L2:
        int i = 0;
_L5:
        if (i < 0)
        {
            return 0;
        } else
        {
            return i;
        }
_L3:
        i = mediaplayer.getCurrentPosition();
          goto _L5
        i = mCurrentPosition;
          goto _L5
    }

    public final int getDuration()
    {
        if (mediaplayer == null) goto _L2; else goto _L1
_L1:
        mediaPlayerState;
        JVM INSTR tableswitch 3 7: default 44
    //                   3 49
    //                   4 49
    //                   5 49
    //                   6 44
    //                   7 49;
           goto _L2 _L3 _L3 _L3 _L2 _L3
_L2:
        return mDuration;
_L3:
        SoundInfo soundinfo;
        soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo != null)
        {
            break; /* Loop/switch isn't completed */
        }
        mDuration = 0;
_L5:
        Logger.logToSd((new StringBuilder()).append("getDuration:").append(mDuration).toString());
        if (true) goto _L2; else goto _L4
_L4:
        if (soundinfo.category == 0)
        {
            mDuration = mediaplayer.getDuration();
        } else
        {
            mDuration = getLiveDuration() * 1000;
        }
          goto _L5
        if (true) goto _L2; else goto _L6
_L6:
    }

    public final int getLiveDuration()
    {
        String s1;
        String s2;
label0:
        {
            SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
            s2 = soundinfo.startTime;
            String s3 = soundinfo.endTime;
            if (!TextUtils.isEmpty(s2))
            {
                s1 = s3;
                if (!TextUtils.isEmpty(s3))
                {
                    break label0;
                }
            }
            s2 = "00:00";
            soundinfo.startTime = "00:00";
            s1 = "24:00";
            soundinfo.endTime = "24:00";
        }
        String as1[] = s2.split(":");
        String as[] = s1.split(":");
        int i = Integer.parseInt(as1[0]);
        int k = Integer.parseInt(as1[1]);
        int l = Integer.parseInt(as[0]);
        int i1 = Integer.parseInt(as[1]);
        if (i > l)
        {
            return (i1 - k) * 60 + ((l - i) + 24) * 3600;
        } else
        {
            return (i1 - k) * 60 + (l - i) * 3600;
        }
    }

    public String getmCurrentAudioSrc()
    {
        return mCurrentAudioSrc;
    }

    public final void installAudioSource(String s1)
    {
        this;
        JVM INSTR monitorenter ;
        installAudioSource(s1, 0, true);
        this;
        JVM INSTR monitorexit ;
        return;
        s1;
        throw s1;
    }

    public final void installAudioSource(String s1, int i)
    {
        this;
        JVM INSTR monitorenter ;
        installAudioSource(s1, 0, true);
        this;
        JVM INSTR monitorexit ;
        return;
        s1;
        throw s1;
    }

    public final void installAudioSource(String s1, int i, boolean flag)
    {
        this;
        JVM INSTR monitorenter ;
        long l = i;
        installAudioSource(s1, l, flag, true);
        this;
        JVM INSTR monitorexit ;
        return;
        s1;
        throw s1;
    }

    public final void installAudioSource(String s1, long l, boolean flag, boolean flag1)
    {
        this;
        JVM INSTR monitorenter ;
        Object obj = mediaplayer;
        if (obj != null) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        resetStatus();
        if (l <= 0L)
        {
            break MISSING_BLOCK_LABEL_48;
        }
        if (SharedPreferencesUtil.getInstance(appContext).getBoolean("historySwitch", true))
        {
            startPlayPosition = l;
        }
        stopUpdateProgress();
        MyLogger.getLogger().print();
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_184;
        }
        pause();
        stopPlayAd();
        obj = PlayListControl.getPlayListManager().getCurSound();
        if (!com.ximalaya.ting.android.a.m || obj == null)
        {
            break MISSING_BLOCK_LABEL_172;
        }
        x x1;
        if (((SoundInfo) (obj)).category != 0 || !e.a(appContext))
        {
            break MISSING_BLOCK_LABEL_172;
        }
        x1 = new x(this, s1, flag, flag1);
        (new e(appContext, ((SoundInfo) (obj)).trackId)).a(x1);
          goto _L1
        Exception exception;
        exception;
        installAudioSourceReal(s1, flag, flag1);
        exception.printStackTrace();
          goto _L1
        s1;
        throw s1;
        installAudioSourceReal(s1, flag, flag1);
          goto _L1
        installAudioSourceReal(s1, flag, flag1);
          goto _L1
    }

    public final void installAudioSource(String s1, boolean flag)
    {
        this;
        JVM INSTR monitorenter ;
        installAudioSource(s1, 0, flag);
        this;
        JVM INSTR monitorexit ;
        return;
        s1;
        throw s1;
    }

    public boolean isEverPlayed()
    {
        return installToStart;
    }

    public boolean isForegroundIsMyApplication()
    {
        if (appContext == null)
        {
            return false;
        }
        ActivityManager activitymanager = (ActivityManager)appContext.getSystemService("activity");
        if (activitymanager == null)
        {
            return false;
        }
        return ((android.app.ActivityManager.RunningTaskInfo)activitymanager.getRunningTasks(1).get(0)).topActivity.getPackageName().equals(appContext.getPackageName());
    }

    public boolean isPausedSoundAd()
    {
        return mSoundAdPlayerPaused;
    }

    public final boolean isPlayNormal()
    {
        this;
        JVM INSTR monitorenter ;
        boolean flag;
        boolean flag1;
        flag1 = false;
        flag = flag1;
        if (mediaplayer == null) goto _L2; else goto _L1
_L1:
        int i = mediaPlayerState;
        flag = flag1;
        i;
        JVM INSTR tableswitch 0 7: default 72
    //                   0 79
    //                   1 79
    //                   2 75
    //                   3 79
    //                   4 79
    //                   5 79
    //                   6 79
    //                   7 79;
           goto _L3 _L4 _L4 _L2 _L4 _L4 _L4 _L4 _L4
_L3:
        flag = flag1;
_L2:
        this;
        JVM INSTR monitorexit ;
        return flag;
_L4:
        flag = true;
        if (true) goto _L2; else goto _L5
_L5:
        Exception exception;
        exception;
        throw exception;
    }

    public final boolean isPlaying()
    {
        boolean flag = false;
        this;
        JVM INSTR monitorenter ;
        XMediaplayerImpl xmediaplayerimpl = mediaplayer;
        if (xmediaplayerimpl != null) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return flag;
_L2:
        boolean flag1 = mediaplayer.isPlaying();
        flag = flag1;
        continue; /* Loop/switch isn't completed */
        Object obj;
        obj;
        throw obj;
        obj;
        if (true) goto _L1; else goto _L3
_L3:
    }

    public boolean isPlayingSoundAd()
    {
label0:
        {
            boolean flag1 = false;
            boolean flag = flag1;
            boolean flag2;
            try
            {
                if (mSoundPatchAdPlayer == null)
                {
                    break label0;
                }
                flag2 = mSoundPatchAdPlayer.isPlaying();
            }
            catch (Exception exception)
            {
                return false;
            }
            flag = flag1;
            if (flag2)
            {
                flag = true;
            }
        }
        return flag;
    }

    public boolean isUseSystemPlayer()
    {
        if (mediaplayer != null)
        {
            return mediaplayer.isUseSystemPlayer();
        } else
        {
            return false;
        }
    }

    public void onBufferingUpdate(XMediaplayerImpl xmediaplayerimpl, int i)
    {
        if (netChangedReplayState == 0 && i != bufferingPercent && i <= 100 && i >= 0 && !TextUtils.isEmpty(PlayListControl.getPlayListManager().curPlaySrc))
        {
            Logger.log((new StringBuilder()).append("TingMediaPlayer onBufferingUpdate percent\uFF1A").append(i).toString());
            bufferingPercent = i;
            updateOnBufferUpdated(i);
        }
    }

    public void onCompletion(XMediaplayerImpl xmediaplayerimpl)
    {
        MyLogger.getLogger().d("onCompletion");
        this;
        JVM INSTR monitorenter ;
        xmediaplayerimpl = PlayListControl.getPlayListManager().getCurSound();
        if (xmediaplayerimpl == null)
        {
            break MISSING_BLOCK_LABEL_38;
        }
        if (((SoundInfo) (xmediaplayerimpl)).category != 1)
        {
            break MISSING_BLOCK_LABEL_38;
        }
        replay(false);
        this;
        JVM INSTR monitorexit ;
        return;
        if (mCompleteHandler == null || !mCompleteHandler.handleComplete())
        {
            break MISSING_BLOCK_LABEL_65;
        }
        this;
        JVM INSTR monitorexit ;
        return;
        xmediaplayerimpl;
        this;
        JVM INSTR monitorexit ;
        throw xmediaplayerimpl;
        if (mediaPlayerState == 4)
        {
            mediaPlayerState = 7;
            stopUpdateProgress();
            updateOnPlayCompleted();
            updateNoification(7);
            if (LocalMediaService.getInstance() != null)
            {
                Logger.log("debug_play", "==playNext==03=");
                playNext(false);
            }
        }
        this;
        JVM INSTR monitorexit ;
    }

    public boolean onError(XMediaplayerImpl xmediaplayerimpl, int i, int k)
    {
        Logger.logToSd((new StringBuilder()).append("mediaPlayerState onError:").append(mediaPlayerState).append(" what:").append(i).append(" extra:").append(k).toString());
        MyLogger.getLogger().d((new StringBuilder()).append("mediaPlayerState onError:").append(mediaPlayerState).append(" what:").append(i).append(" extra:").append(k).toString());
        if (ToolUtil.isConnectToNetwork(appContext)) goto _L2; else goto _L1
_L1:
        if (mediaPlayerState != 4) goto _L4; else goto _L3
_L3:
        netChangedReplayState = 1;
_L9:
        mediaPlayerState = 8;
_L6:
        return true;
_L4:
        if (mediaPlayerState == 2)
        {
            updateOnPlayerBuffering(false);
        }
        continue; /* Loop/switch isn't completed */
_L2:
        if (!xmediaplayerimpl.isUseSystemPlayer() && !TextUtils.isEmpty(mCurrentAudioSrc) && k == -1004)
        {
            f.a().a(mCurrentAudioSrc, null, null, new y(this));
            mediaPlayerState = 8;
            return true;
        }
        mediaPlayerState = 8;
        xmediaplayerimpl = PlayListControl.getPlayListManager().getCurSound();
        long l;
        long l1;
        if (xmediaplayerimpl != null)
        {
            l1 = ((SoundInfo) (xmediaplayerimpl)).history_listener;
            l = ((SoundInfo) (xmediaplayerimpl)).history_duration;
        } else
        {
            l = 0L;
            l1 = 0L;
        }
        if (i != 1)
        {
            break; /* Loop/switch isn't completed */
        }
        if (k == -1007 || k == -1010)
        {
            if (((SoundInfo) (xmediaplayerimpl)).category != 1)
            {
                playNext(false);
                return true;
            }
        } else
        {
            if (l1 == 0L && mUseProxy)
            {
                replay(false);
                return true;
            }
            if (l1 > 0L && 1000L + l1 < l)
            {
                xmediaplayerimpl.history_duration = ((SoundInfo) (xmediaplayerimpl)).history_duration + 1000L;
                replay(true, (int)l1 + 1000);
                return true;
            } else
            {
                playNext(false);
                return true;
            }
        }
        if (true) goto _L6; else goto _L5
_L5:
        if (i == 100)
        {
            if (mediaplayer != null)
            {
                releaseMediaPlayer(mediaplayer);
            }
            mediaplayer = newXMediaplayerImpl();
            setupMediaPlayer();
            if (l1 == 0L)
            {
                replay(false);
                return true;
            }
            if (l1 > 0L && l1 < l)
            {
                replay(true, (int)l1);
                return true;
            } else
            {
                playNext(false);
                return true;
            }
        }
        if (mErrorHandler == null || !mErrorHandler.handleError(i, k)) goto _L6; else goto _L7
_L7:
        return true;
        if (true) goto _L9; else goto _L8
_L8:
    }

    public boolean onInfo(XMediaplayerImpl xmediaplayerimpl, int i, int k)
    {
        boolean flag;
        boolean flag1;
        flag1 = false;
        Logger.log((new StringBuilder()).append("ForbidSeek TingMediaPlayer  onInfo222, what=").append(i).append(", extra=").append(k).toString());
        flag = flag1;
        i;
        JVM INSTR tableswitch 701 702: default 64
    //                   701 101
    //                   702 68;
           goto _L1 _L2 _L3
_L3:
        break; /* Loop/switch isn't completed */
_L1:
        flag = flag1;
_L5:
        Logger.log((new StringBuilder()).append("onInfo updateOnPlayerBuffering 005:").append(flag).toString());
        updateOnPlayerBuffering(flag);
        return flag;
_L2:
        flag = true;
        if (true) goto _L5; else goto _L4
_L4:
    }

    public void onPrepared(XMediaplayerImpl xmediaplayerimpl)
    {
        boolean flag;
        flag = false;
        Logger.logToSd((new StringBuilder()).append("\u65AD\u70B9\u7EED\u542C00startPlayPosition").append(startPlayPosition).append("  getDuration:").append(getDuration()).toString());
        MyLogger.getLogger().d((new StringBuilder()).append("mediaPlayerState onPrepared:").append(mediaPlayerState).append(" startPlayPosition:").append(startPlayPosition).toString());
        Logger.log("onPrepared updateOnPlayerBuffering 004 false");
        updateOnPlayerBuffering(false);
        mediaPlayerState = 3;
        if (!TextUtils.isEmpty(PlayListControl.getPlayListManager().curPlaySrc) && !PlayListControl.getPlayListManager().curPlaySrc.contains("http"))
        {
            bufferingPercent = 100;
            updateOnBufferUpdated(100);
        }
        xmediaplayerimpl = PlayListControl.getPlayListManager().getCurSound();
        mDuration = getDuration();
        updateOnSoundPrepared(mDuration);
        if (netChangedReplayState != 2) goto _L2; else goto _L1
_L1:
        netChangedReplayState = 3;
        if (((SoundInfo) (xmediaplayerimpl)).category == 0) goto _L4; else goto _L3
_L3:
        start();
_L6:
        return;
_L4:
        if (mCurrentAudioSrc != null && installToStart && !mCurrentAudioSrc.contains("http"))
        {
            start(saveLastPositon);
            return;
        } else
        {
            Logger.logToSd((new StringBuilder()).append("onPrepared seekTo saveLastPositon:").append(saveLastPositon).toString());
            MyLogger.getLogger().d((new StringBuilder()).append("onPrepared seekTo saveLastPositon:").append(saveLastPositon).toString());
            seekTo(saveLastPositon);
            return;
        }
_L2:
        if (startPlayPosition > 0L && startPlayPosition < (long)(getDuration() - 60000))
        {
            Logger.logToSd((new StringBuilder()).append("\u65AD\u70B9\u7EED\u542C01startPlayPosition").append(startPlayPosition).append("  getDuration:").append(getDuration()).toString());
            if (((SoundInfo) (xmediaplayerimpl)).category != 0)
            {
                start();
            } else
            if (mCurrentAudioSrc != null && installToStart && !mCurrentAudioSrc.contains("http"))
            {
                start(startPlayPosition);
            } else
            {
                seekTo(startPlayPosition);
            }
            startPlayPosition = 0L;
            return;
        }
        if (!installToStart)
        {
            continue; /* Loop/switch isn't completed */
        }
        xmediaplayerimpl = PlayListControl.getPlayListManager().getCurSound();
        if (xmediaplayerimpl == null)
        {
            break; /* Loop/switch isn't completed */
        }
        xmediaplayerimpl = PlayListControl.getAudioSourceForPlayer(xmediaplayerimpl);
        MyLogger.getLogger().d((new StringBuilder()).append("audioSrc = ").append(xmediaplayerimpl).toString());
        MyLogger.getLogger().d((new StringBuilder()).append("mCurrentAudioSrc = ").append(mCurrentAudioSrc).toString());
        if (mCurrentAudioSrc == null || mCurrentAudioSrc.equals(xmediaplayerimpl))
        {
            break; /* Loop/switch isn't completed */
        }
        MyLogger.getLogger().print();
        installAudioSourceReal(xmediaplayerimpl, true, mUseProxy);
_L7:
        if (flag)
        {
            start();
            return;
        }
        if (true) goto _L6; else goto _L5
_L5:
        flag = true;
          goto _L7
        if (true) goto _L6; else goto _L8
_L8:
    }

    public void onSeekComplete(XMediaplayerImpl xmediaplayerimpl)
    {
        Logger.log((new StringBuilder()).append("ForbidSeek onInfo222 mediaPlayerState onSeekComplete:").append(mediaPlayerState).toString());
        if (netChangedReplayState == 3)
        {
            if (saveLastPlayerStatus != 5)
            {
                if (installToStart)
                {
                    start();
                } else
                {
                    installToStart = true;
                }
            } else
            {
                mediaPlayerState = 5;
            }
            netChangedReplayState = 0;
            return;
        }
        if (installToStart)
        {
            start();
            return;
        } else
        {
            installToStart = true;
            return;
        }
    }

    public final int pause()
    {
        byte byte1 = -1;
        this;
        JVM INSTR monitorenter ;
        byte byte0 = byte1;
        if (mediaplayer == null) goto _L2; else goto _L1
_L1:
        int i = mediaPlayerState;
        byte0 = byte1;
        i;
        JVM INSTR tableswitch 4 7: default 52
    //                   4 58
    //                   5 58
    //                   6 54
    //                   7 58;
           goto _L3 _L4 _L4 _L2 _L4
_L3:
        byte0 = byte1;
_L2:
        this;
        JVM INSTR monitorexit ;
        return byte0;
_L4:
        b.a(MyApplication.b()).b();
        disableWifiLock();
        mediaplayer.pause();
        if (audioManager != null)
        {
            audioManager.requestAudioFocus(pafcl, 3, -1);
        }
        mediaPlayerState = 5;
        stopUpdateProgress();
        updateOnPlayPaused();
        updateNoification(5);
        byte0 = 0;
        if (true) goto _L2; else goto _L5
_L5:
        Exception exception;
        exception;
        throw exception;
    }

    public void pausePlaySoundAd()
    {
        mSoundAdPlayerPaused = true;
        if (isPlayingSoundAd())
        {
            try
            {
                mSoundPatchAdPlayer.pause();
                mediaPlayerState = 11;
                if (mAdCallbacks != null)
                {
                    for (Iterator iterator = mAdCallbacks.iterator(); iterator.hasNext(); ((SoundAdCallback)iterator.next()).onPausePlayAd(mSoundPatchAdPlayer)) { }
                }
            }
            catch (IllegalStateException illegalstateexception)
            {
                illegalstateexception.printStackTrace();
            }
        }
    }

    public final void release()
    {
        this;
        JVM INSTR monitorenter ;
        if (tingMediaPlayer != null)
        {
            synchronized (INSTANCE_LOCK)
            {
                if (tingMediaPlayer != null)
                {
                    b.a(MyApplication.b()).b();
                    disableWifiLock();
                    removeListener();
                    tingMediaPlayer = null;
                }
            }
        }
        obj = mediaplayer;
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_83;
        }
        stop();
        if (mediaplayer != null)
        {
            releaseMediaPlayer(mediaplayer);
            mediaPlayerState = 9;
            mediaplayer = null;
        }
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        Object obj1;
        obj1;
        this;
        JVM INSTR monitorexit ;
        throw obj1;
        obj1;
        if (mediaplayer != null)
        {
            releaseMediaPlayer(mediaplayer);
            mediaPlayerState = 9;
            mediaplayer = null;
        }
        throw obj1;
        obj1;
        if (mediaplayer == null) goto _L2; else goto _L1
_L1:
        releaseMediaPlayer(mediaplayer);
        mediaPlayerState = 9;
        mediaplayer = null;
          goto _L2
    }

    public void removeLiveSoundUpdateCallback()
    {
        mLiveSoundUpdateCallback = null;
    }

    public void removeNotifications()
    {
        if (notification == null)
        {
            return;
        } else
        {
            ((NotificationManager)appContext.getSystemService("notification")).cancel(3);
            notification = null;
            return;
        }
    }

    public void removeProxy()
    {
        if (mediaplayer != null)
        {
            mediaplayer.removeProxy();
        }
    }

    public void removeSoundAdCallback(SoundAdCallback soundadcallback)
    {
        synchronized (mAdCallbackLock)
        {
            if (mAdCallbacks != null)
            {
                mAdCallbacks.remove(soundadcallback);
            }
        }
        return;
        soundadcallback;
        obj;
        JVM INSTR monitorexit ;
        throw soundadcallback;
    }

    public final void replay()
    {
        replay(false);
    }

    public void resumePlaySoundAd()
    {
        if (mSoundAdPlayerPaused && mSoundPatchAdPlayer != null)
        {
            try
            {
                mSoundPatchAdPlayer.start();
                mSoundAdPlayerPaused = false;
                mediaPlayerState = 10;
                if (mAdCallbacks != null)
                {
                    for (Iterator iterator = mAdCallbacks.iterator(); iterator.hasNext(); ((SoundAdCallback)iterator.next()).onResumePlayAd(mSoundPatchAdPlayer)) { }
                }
            }
            catch (IllegalStateException illegalstateexception)
            {
                illegalstateexception.printStackTrace();
            }
        }
    }

    public final int seekTo(long l)
    {
        byte byte0 = 0;
        this;
        JVM INSTR monitorenter ;
        if (mediaplayer == null) goto _L2; else goto _L1
_L1:
        int i = mediaPlayerState;
        i;
        JVM INSTR tableswitch 3 7: default 56
    //                   3 64
    //                   4 64
    //                   5 64
    //                   6 56
    //                   7 64;
           goto _L2 _L3 _L3 _L3 _L2 _L3
_L2:
        byte0 = -1;
_L9:
        this;
        JVM INSTR monitorexit ;
        return byte0;
_L3:
        if (Math.abs((long)getDuration() - l) >= 2L || LocalMediaService.getInstance() == null) goto _L5; else goto _L4
_L4:
        Logger.log("debug_play", "==playNext==04=");
        LocalMediaService.getInstance().playNext(false);
        byte0 = 1;
        continue; /* Loop/switch isn't completed */
_L5:
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo == null) goto _L7; else goto _L6
_L6:
        Logger.logToSd((new StringBuilder()).append("\u5F00\u59CBseek url:").append(soundinfo.playUrl64).append(" seekPosition").append(l).append(" startPlayPosition").append(startPlayPosition).toString());
        MyLogger.getLogger().d((new StringBuilder()).append("\u5F00\u59CBseek url:").append(soundinfo.playUrl64).append(" seekPosition").append(l).append(" startPlayPosition").append(startPlayPosition).toString());
_L7:
        stopUpdateProgress();
        mCurrentPosition = (int)l;
        mediaplayer.seekTo((int)l);
        lastSeekPosition = (int)l;
        updateNoification(5);
        if (true) goto _L9; else goto _L8
_L8:
        Exception exception;
        exception;
        throw exception;
    }

    public final int seekToProgress(int i, int k)
    {
        this;
        JVM INSTR monitorenter ;
        if (i >= 0 && k >= 0 && i <= k) goto _L2; else goto _L1
_L1:
        i = -1;
_L4:
        this;
        JVM INSTR monitorexit ;
        return i;
_L2:
        double d = i;
        d = (d * (double)getDuration()) / (double)k;
        Logger.logToSd((new StringBuilder()).append("seekToProgress progress:").append(i).append(" max:").append(k).toString());
        MyLogger.getLogger().d((new StringBuilder()).append("seekToProgress progress:").append(i).append(" max:").append(k).toString());
        i = seekTo((int)d);
        if (true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    public void setCompleteHandler(MediaPlayerCompleteHandler mediaplayercompletehandler)
    {
        mCompleteHandler = mediaplayercompletehandler;
    }

    public void setErrorHandler(MediaPlayerErrorHandler mediaplayererrorhandler)
    {
        mErrorHandler = mediaplayererrorhandler;
    }

    public void setLiveSoundUpdateCallback(LiveSoundUpdateCallback livesoundupdatecallback)
    {
        mLiveSoundUpdateCallback = livesoundupdatecallback;
    }

    public void setProxy()
    {
        if (mediaplayer != null)
        {
            FreeFlowUtil.getInstance().setProxyForXMediaPlayer(mediaplayer);
        }
    }

    public void setmCurrentAudioSrc(String s1)
    {
        mCurrentAudioSrc = s1;
    }

    public final void start()
    {
        this;
        JVM INSTR monitorenter ;
        start(-1L);
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public final void start(long l)
    {
        this;
        JVM INSTR monitorenter ;
        SoundInfo soundinfo;
        MyLogger.getLogger().print();
        soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo == null) goto _L2; else goto _L1
_L1:
        if (soundinfo.category == 0 || soundinfo.validDate == null || mLiveSoundUpdateCallback == null || ToolUtil.isLiveSoundInValidDate(soundinfo.validDate)) goto _L2; else goto _L3
_L3:
        mLiveSoundUpdateCallback.updateLiveSound();
_L5:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (mediaplayer == null) goto _L5; else goto _L4
_L4:
        mediaPlayerState;
        JVM INSTR tableswitch 3 7: default 409
    //                   3 108
    //                   4 108
    //                   5 108
    //                   6 409
    //                   7 108;
           goto _L5 _L6 _L6 _L6 _L5 _L6
_L6:
        b.a(MyApplication.b()).a();
        enableWifiLock();
        installToStart = true;
        mediaplayer.start();
        soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo == null) goto _L8; else goto _L7
_L7:
        Logger.logToSd((new StringBuilder()).append("\u5F00\u59CB\u64AD\u653Eurl:").append(soundinfo.playUrl64).append(" seekPosition").append(l).append(" startPlayPosition").append(startPlayPosition).toString());
        MyLogger.getLogger().d((new StringBuilder()).append("\u5F00\u59CB\u64AD\u653Eurl:").append(soundinfo.playUrl64).append(" seekPosition").append(l).append(" startPlayPosition").append(startPlayPosition).toString());
_L8:
        mediaPlayerState = 4;
        if (Looper.myLooper() != Looper.getMainLooper()) goto _L10; else goto _L9
_L9:
        Logger.log("ForbidSeek updateOnPlayStarted");
        updateOnPlayStarted();
_L14:
        startUpdateProgress();
        if (audioManager != null)
        {
            audioManager.requestAudioFocus(pafcl, 3, 1);
        }
        if (notification != null) goto _L12; else goto _L11
_L11:
        createNotificationPlayerController();
_L15:
        mCurrentPosition = (int)l;
        if (l <= 0L) goto _L5; else goto _L13
_L13:
        Logger.logToSd((new StringBuilder()).append("start seekTo seekPosition:").append(l).toString());
        MyLogger.getLogger().d((new StringBuilder()).append("start seekTo seekPosition:").append(l).toString());
        seekTo(l);
          goto _L5
        Exception exception;
        exception;
        throw exception;
_L10:
        mUiHandler.post(new z(this));
          goto _L14
_L12:
        updateNoification(4);
          goto _L15
    }

    public final int stop()
    {
        this;
        JVM INSTR monitorenter ;
        if (mediaplayer != null)
        {
            switch (mediaPlayerState)
            {
            case 3: // '\003'
            case 4: // '\004'
            case 5: // '\005'
            case 6: // '\006'
            case 7: // '\007'
                break MISSING_BLOCK_LABEL_56;
            }
        }
_L1:
        stopPlayAd();
        this;
        JVM INSTR monitorexit ;
        return 0;
        b.a(MyApplication.b()).b();
        disableWifiLock();
        mediaplayer.stop();
        if (audioManager != null)
        {
            audioManager.abandonAudioFocus(pafcl);
        }
        mediaPlayerState = 6;
        PlayListControl.getPlayListManager().stop();
        updateNoification(6);
          goto _L1
        Exception exception;
        exception;
        throw exception;
    }

    public void stopPlayAd()
    {
        if (mSoundPatchAdPlayer == null) goto _L2; else goto _L1
_L1:
        mSoundPatchAdPlayer.stop();
        mSoundPatchAdPlayer.release();
        mSoundPatchAdPlayer = null;
        if (mPlayingSoundAd != null)
        {
            mPlayingSoundAd.onAdPlayerRelease(mPlayingSoundAd);
            mPlayingSoundAd = null;
        }
_L4:
        mediaPlayerState = 6;
_L2:
        return;
        Object obj;
        obj;
        ((Exception) (obj)).printStackTrace();
        if (true) goto _L4; else goto _L3
_L3:
        obj;
        mediaPlayerState = 6;
        throw obj;
    }

    public final void updateLiveDuration()
    {
        mDuration = getLiveDuration() * 1000;
    }

    public void updateNotificationBigView()
    {
        SoundInfo soundinfo;
        for (soundinfo = PlayListControl.getPlayListManager().getCurSound(); soundinfo == null || notification == null || android.os.Build.VERSION.SDK_INT < 16 || notification.bigContentView == null || Utilities.isBlank(soundinfo.coverLarge);)
        {
            return;
        }

        ImageManager2.from(appContext).displayImage(notification.bigContentView, 0x7f0a05d5, soundinfo.coverLarge, -1, mRemoteViewCallback);
    }

    public void updateOnBufferUpdated(int i)
    {
        if (mPlayerStatusUpdateListenerList != null)
        {
            Iterator iterator = mPlayerStatusUpdateListenerList.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                OnPlayerStatusUpdateListener onplayerstatusupdatelistener = (OnPlayerStatusUpdateListener)iterator.next();
                if (onplayerstatusupdatelistener != null)
                {
                    Logger.log((new StringBuilder()).append("TingMediaPlayer onBufferingUpdate updateOnBufferUpdated percent\uFF1A").append(i).toString());
                    onplayerstatusupdatelistener.onBufferUpdated(i);
                }
            } while (true);
        }
    }

    public void updateOnLogoPlayFinished()
    {
        isPlayingAds = false;
        if (mPlayerStatusUpdateListenerList != null)
        {
            Iterator iterator = mPlayerStatusUpdateListenerList.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                OnPlayerStatusUpdateListener onplayerstatusupdatelistener = (OnPlayerStatusUpdateListener)iterator.next();
                if (onplayerstatusupdatelistener != null)
                {
                    onplayerstatusupdatelistener.onLogoPlayFinished();
                }
            } while (true);
        }
    }

    public void updateOnPlayCompleted()
    {
        if (mPlayerStatusUpdateListenerList != null)
        {
            Iterator iterator = mPlayerStatusUpdateListenerList.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                OnPlayerStatusUpdateListener onplayerstatusupdatelistener = (OnPlayerStatusUpdateListener)iterator.next();
                if (onplayerstatusupdatelistener != null)
                {
                    onplayerstatusupdatelistener.onPlayCompleted();
                }
            } while (true);
        }
        if (appContext != null)
        {
            Intent intent = new Intent("ximalaya.action.player.PAUSE");
            intent.addFlags(0x10000000);
            appContext.sendBroadcast(intent);
        }
    }

    public void updateOnPlayPaused()
    {
        if (mPlayerStatusUpdateListenerList != null)
        {
            Iterator iterator = mPlayerStatusUpdateListenerList.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                OnPlayerStatusUpdateListener onplayerstatusupdatelistener = (OnPlayerStatusUpdateListener)iterator.next();
                if (onplayerstatusupdatelistener != null)
                {
                    onplayerstatusupdatelistener.onPlayPaused();
                }
            } while (true);
        }
        if (appContext != null)
        {
            Intent intent = new Intent("ximalaya.action.player.PAUSE");
            intent.addFlags(0x10000000);
            appContext.sendBroadcast(intent);
        }
    }

    public void updateOnPlayProgressUpdate(int i, int k)
    {
        if (i >= 0 && k >= 0 && i <= k && mPlayerStatusUpdateListenerList != null)
        {
            Iterator iterator = mPlayerStatusUpdateListenerList.iterator();
            while (iterator.hasNext()) 
            {
                OnPlayerStatusUpdateListener onplayerstatusupdatelistener = (OnPlayerStatusUpdateListener)iterator.next();
                if (onplayerstatusupdatelistener != null)
                {
                    onplayerstatusupdatelistener.onPlayProgressUpdate(i, k);
                }
            }
        }
    }

    public void updateOnPlayStarted()
    {
        if (mPlayerStatusUpdateListenerList != null)
        {
            Iterator iterator = mPlayerStatusUpdateListenerList.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                OnPlayerStatusUpdateListener onplayerstatusupdatelistener = (OnPlayerStatusUpdateListener)iterator.next();
                if (onplayerstatusupdatelistener != null)
                {
                    onplayerstatusupdatelistener.onPlayStarted();
                }
            } while (true);
        }
        if (appContext != null)
        {
            Intent intent = new Intent("ximalaya.action.player.START");
            intent.addFlags(0x10000000);
            appContext.sendBroadcast(intent);
        }
    }

    public void updateOnPlayerBuffering(boolean flag)
    {
        if (mPlayerStatusUpdateListenerList != null)
        {
            Iterator iterator = mPlayerStatusUpdateListenerList.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                OnPlayerStatusUpdateListener onplayerstatusupdatelistener = (OnPlayerStatusUpdateListener)iterator.next();
                if (onplayerstatusupdatelistener != null)
                {
                    Logger.log((new StringBuilder()).append("updateOnPlayerBuffering 000").append(flag).toString());
                    onplayerstatusupdatelistener.onPlayerBuffering(flag);
                }
            } while (true);
        }
    }

    public void updateOnSoundPrepared(int i)
    {
        if (mPlayerStatusUpdateListenerList != null)
        {
            Iterator iterator = mPlayerStatusUpdateListenerList.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                OnPlayerStatusUpdateListener onplayerstatusupdatelistener = (OnPlayerStatusUpdateListener)iterator.next();
                if (onplayerstatusupdatelistener != null)
                {
                    onplayerstatusupdatelistener.onSoundPrepared(i);
                }
            } while (true);
        }
    }

    public void updateOnStartPlayLogo()
    {
        isPlayingAds = true;
        if (mPlayerStatusUpdateListenerList != null)
        {
            Iterator iterator = mPlayerStatusUpdateListenerList.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                OnPlayerStatusUpdateListener onplayerstatusupdatelistener = (OnPlayerStatusUpdateListener)iterator.next();
                if (onplayerstatusupdatelistener != null)
                {
                    onplayerstatusupdatelistener.onStartPlayLogo();
                }
            } while (true);
        }
    }






/*
    static int access$1002(TingMediaPlayer tingmediaplayer, int i)
    {
        tingmediaplayer.netChangedReplayState = i;
        return i;
    }

*/





/*
    static int access$1402(TingMediaPlayer tingmediaplayer, int i)
    {
        tingmediaplayer.saveLastPlayerStatus = i;
        return i;
    }

*/



/*
    static int access$1502(TingMediaPlayer tingmediaplayer, int i)
    {
        tingmediaplayer.saveLastPositon = i;
        return i;
    }

*/








/*
    static ConnectivityManager access$2002(TingMediaPlayer tingmediaplayer, ConnectivityManager connectivitymanager)
    {
        tingmediaplayer.connectivityManager = connectivitymanager;
        return connectivitymanager;
    }

*/



/*
    static NetworkInfo access$2102(TingMediaPlayer tingmediaplayer, NetworkInfo networkinfo)
    {
        tingmediaplayer.netInfo = networkinfo;
        return networkinfo;
    }

*/








/*
    static AbstractSoundAdModel access$702(TingMediaPlayer tingmediaplayer, AbstractSoundAdModel abstractsoundadmodel)
    {
        tingmediaplayer.mPlayingSoundAd = abstractsoundadmodel;
        return abstractsoundadmodel;
    }

*/




/*
    static int access$902(TingMediaPlayer tingmediaplayer, int i)
    {
        tingmediaplayer.lastSeekPosition = i;
        return i;
    }

*/
}
