// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.media.MediaPlayer;
import android.text.TextUtils;
import com.ximalaya.ting.android.model.ad.AbstractSoundAdModel;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            TingMediaPlayer

class f
    implements android.media.eringUpdateListener, android.media.letionListener, android.media.rListener, android.media.aredListener
{

    final TingMediaPlayer a;
    private List b;
    private int c;
    private String d;
    private boolean e;
    private boolean f;

    public void onBufferingUpdate(MediaPlayer mediaplayer, int i)
    {
        if (b != null && b.size() <= c)
        {
            ((AbstractSoundAdModel)b.get(c)).onBufferedPlayAd((AbstractSoundAdModel)b.get(c));
        }
    }

    public void onCompletion(MediaPlayer mediaplayer)
    {
        if (b != null && c < b.size())
        {
            ((AbstractSoundAdModel)b.get(c)).onCompletePlayAd((AbstractSoundAdModel)b.get(c));
        }
        if (b == null || b.size() <= c + 1)
        {
            break MISSING_BLOCK_LABEL_390;
        }
        int i;
        mediaplayer.reset();
        i = c;
_L1:
        if (i >= b.size())
        {
            break MISSING_BLOCK_LABEL_468;
        }
        c = c + 1;
        TingMediaPlayer.access$702(a, (AbstractSoundAdModel)b.get(c));
        if (!TextUtils.isEmpty(TingMediaPlayer.access$700(a).getISoundUrl()))
        {
            mediaplayer.setDataSource(((AbstractSoundAdModel)b.get(c)).getISoundUrl());
            mediaplayer.prepareAsync();
            TingMediaPlayer.access$700(a).onPreparePlayAd((AbstractSoundAdModel)b.get(c));
            return;
        }
        Exception exception;
        Iterator iterator1;
        if (c != b.size() - 1)
        {
            break MISSING_BLOCK_LABEL_375;
        }
        if (TingMediaPlayer.access$400(a) != null)
        {
            for (Iterator iterator = TingMediaPlayer.access$400(a).iterator(); iterator.hasNext(); ((undAdCallback)iterator.next()).onCompletePlayAd(mediaplayer, b)) { }
        }
        try
        {
            TingMediaPlayer.access$600(a, d, e, f);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Exception exception)
        {
            exception.printStackTrace();
            TingMediaPlayer.access$600(a, d, e, f);
            if (TingMediaPlayer.access$400(a) != null)
            {
                for (iterator1 = TingMediaPlayer.access$400(a).iterator(); iterator1.hasNext(); ((undAdCallback)iterator1.next()).onErroredPlayAd(mediaplayer, b)) { }
            }
        }
        break MISSING_BLOCK_LABEL_382;
        i++;
          goto _L1
        a.stopPlayAd();
        return;
        TingMediaPlayer.access$600(a, d, e, f);
        if (TingMediaPlayer.access$400(a) != null)
        {
            for (Iterator iterator2 = TingMediaPlayer.access$400(a).iterator(); iterator2.hasNext(); ((undAdCallback)iterator2.next()).onCompletePlayAd(mediaplayer, b)) { }
        }
        a.stopPlayAd();
    }

    public boolean onError(MediaPlayer mediaplayer, int i, int j)
    {
        if (b != null && c < b.size())
        {
            ((AbstractSoundAdModel)b.get(c)).onCompletePlayAd((AbstractSoundAdModel)b.get(c));
        }
        TingMediaPlayer.access$600(a, d, e, f);
        if (TingMediaPlayer.access$400(a) != null)
        {
            for (Iterator iterator = TingMediaPlayer.access$400(a).iterator(); iterator.hasNext(); ((undAdCallback)iterator.next()).onErroredPlayAd(mediaplayer, b)) { }
        }
        a.stopPlayAd();
        return true;
    }

    public void onPrepared(MediaPlayer mediaplayer)
    {
        if (b != null && c < b.size())
        {
            ((AbstractSoundAdModel)b.get(c)).onPreparedPlayAd((AbstractSoundAdModel)b.get(c));
        }
        mediaplayer.start();
        a.mediaPlayerState = 10;
        if (TingMediaPlayer.access$400(a) != null)
        {
            for (Iterator iterator = TingMediaPlayer.access$400(a).iterator(); iterator.hasNext(); ((undAdCallback)iterator.next()).onStartPlayAd(mediaplayer, b, c)) { }
        }
        if (b != null && c < b.size())
        {
            ((AbstractSoundAdModel)b.get(c)).onStartPlayAd((AbstractSoundAdModel)b.get(c));
        }
    }

    public undAdCallback(TingMediaPlayer tingmediaplayer, List list, int i, String s, boolean flag, boolean flag1)
    {
        a = tingmediaplayer;
        super();
        c = 0;
        b = list;
        c = i;
        d = s;
        e = flag;
        f = flag1;
    }
}
