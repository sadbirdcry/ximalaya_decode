// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.sound.RecordingModel;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.HashMap;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            Playlist

class g extends Playlist
{

    private int a;
    private int b;

    public g(List list)
    {
        super(list);
        a = 0;
        b = 30;
        if (mParams == null)
        {
            break MISSING_BLOCK_LABEL_44;
        }
        list = (String)mParams.get("pageSize");
        b = Integer.parseInt(list);
        return;
        list;
        list.printStackTrace();
        return;
    }

    public boolean hasMore()
    {
        while (mPageId == 0 || mPageId == 1 || a <= 0 || mPageId * b < a) 
        {
            return true;
        }
        return false;
    }

    public List loadMore()
    {
        Object obj;
        if (mParams == null)
        {
            return null;
        }
        obj = (String)mParams.get("uid");
        String s = mDataSourceUrl;
        obj = (new StringBuilder()).append(s).append("/").append(((String) (obj))).append("/").append(mPageId + 1).append("/").append(b).toString();
        obj = f.a().a(((String) (obj)), null, false);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            break MISSING_BLOCK_LABEL_182;
        }
        obj = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
        if (((JSONObject) (obj)).getIntValue("ret") != 0)
        {
            break MISSING_BLOCK_LABEL_182;
        }
        a = ((JSONObject) (obj)).getInteger("totalCount").intValue();
        obj = JSON.parseArray(((JSONObject) (obj)).getString("list"), com/ximalaya/ting/android/model/sound/RecordingModel);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_182;
        }
        if (((List) (obj)).size() <= 0)
        {
            break MISSING_BLOCK_LABEL_182;
        }
        mPageId = mPageId + 1;
        obj = ModelHelper.recordingModelToSoundInfoList(((List) (obj)));
        return ((List) (obj));
        Exception exception;
        exception;
        exception.printStackTrace();
        return null;
    }
}
