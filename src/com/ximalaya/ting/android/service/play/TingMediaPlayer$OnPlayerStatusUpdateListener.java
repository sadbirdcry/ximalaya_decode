// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;


// Referenced classes of package com.ximalaya.ting.android.service.play:
//            TingMediaPlayer

public static interface Y
{

    public abstract void onBufferUpdated(int i);

    public abstract void onLogoPlayFinished();

    public abstract void onPlayCompleted();

    public abstract void onPlayPaused();

    public abstract void onPlayProgressUpdate(int i, int j);

    public abstract void onPlayStarted();

    public abstract void onPlayerBuffering(boolean flag);

    public abstract void onSoundPrepared(int i);

    public abstract void onStartPlayLogo();
}
