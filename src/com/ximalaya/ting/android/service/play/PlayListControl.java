// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.FileUtils;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            l, PlayTools, Playlist, PlayListRecord, 
//            LocalMediaService

public final class PlayListControl
{

    private static final Object INSTANCE_LOCK = new Object();
    public static final int MAX_HOLD_SOUND_DETAIL_SIZE = 50;
    public static final int SORT_ASC = 2;
    public static final int SORT_DEFAULT = 1;
    public static final int SORT_DESC = 3;
    private static volatile PlayListControl playlistManager = null;
    public int curIndex;
    public String curPlaySrc;
    public ConcurrentHashMap details;
    public int lastPlayPositon;
    public int listType;
    private boolean mIsManualPlay;
    private Playlist mPlaylist;
    private l mPlaylistFactory;
    public List todayRadioSounds;
    public List tomorrowSounds;
    public List yesterdayRadioSounds;

    private PlayListControl()
    {
        mIsManualPlay = false;
        listType = 0;
        curIndex = -1;
        details = new ConcurrentHashMap();
        mPlaylistFactory = new l();
        mPlaylist = mPlaylistFactory.a(null, null, 0, null);
    }

    public static final String getAudioSourceForPlayer(SoundInfo soundinfo)
    {
        if (soundinfo != null) goto _L2; else goto _L1
_L1:
        return null;
_L2:
        Object obj = DownloadHandler.getInstance(MyApplication.b());
        if (obj == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (!((DownloadHandler) (obj)).isDownloadCompleted(soundinfo))
        {
            continue; /* Loop/switch isn't completed */
        }
        obj = ((DownloadHandler) (obj)).getDownloadFile(soundinfo);
        if (obj == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (!((File) (obj)).exists())
        {
            continue; /* Loop/switch isn't completed */
        }
        obj = ((File) (obj)).getCanonicalPath();
        return ((String) (obj));
        IOException ioexception;
        ioexception;
        ioexception.printStackTrace();
        if (Utilities.isBlank(soundinfo.playUrl32) && Utilities.isBlank(soundinfo.playUrl64) && Utilities.isBlank(soundinfo.playPathAacv224) && Utilities.isBlank(soundinfo.playPathAacv164)) goto _L1; else goto _L3
_L3:
        if (MyApplication.b() == null)
        {
            if (Utilities.isNotBlank(soundinfo.playPathAacv224))
            {
                return soundinfo.playPathAacv224;
            }
            if (Utilities.isNotBlank(soundinfo.playUrl32))
            {
                return soundinfo.playUrl32;
            }
            if (Utilities.isNotBlank(soundinfo.playPathAacv164))
            {
                return soundinfo.playPathAacv164;
            } else
            {
                return soundinfo.playUrl64;
            }
        }
        try
        {
            if (1 == NetworkUtils.getNetType(MyApplication.b()))
            {
                if (Utilities.isNotBlank(soundinfo.playPathAacv164))
                {
                    return soundinfo.playPathAacv164;
                }
                if (Utilities.isNotBlank(soundinfo.playUrl64))
                {
                    return soundinfo.playUrl64;
                }
                if (Utilities.isNotBlank(soundinfo.playPathAacv224))
                {
                    return soundinfo.playPathAacv224;
                } else
                {
                    return soundinfo.playUrl32;
                }
            }
            if (Utilities.isNotBlank(soundinfo.playPathAacv224))
            {
                return soundinfo.playPathAacv224;
            }
            if (Utilities.isNotBlank(soundinfo.playUrl32))
            {
                return soundinfo.playUrl32;
            }
            if (Utilities.isNotBlank(soundinfo.playPathAacv164))
            {
                return soundinfo.playPathAacv164;
            }
            soundinfo = soundinfo.playUrl64;
        }
        // Misplaced declaration of an exception variable
        catch (SoundInfo soundinfo)
        {
            Logger.throwRuntimeException("getAudioSourceForPlayer error");
            return null;
        }
        return soundinfo;
    }

    public static final String getAudioSourceForPlayer2(SoundInfo soundinfo)
    {
        while (soundinfo == null || Utilities.isBlank(soundinfo.playUrl32) && Utilities.isBlank(soundinfo.playUrl64)) 
        {
            return null;
        }
        if (MyApplication.b() == null)
        {
            if (Utilities.isBlank(soundinfo.playUrl32))
            {
                return soundinfo.playUrl32;
            } else
            {
                return soundinfo.playUrl64;
            }
        }
        File file;
        if (!Utilities.isNotBlank(soundinfo.playUrl64))
        {
            break MISSING_BLOCK_LABEL_108;
        }
        file = PlayTools.getDownloadFile(soundinfo.playUrl64);
        if (file == null)
        {
            break MISSING_BLOCK_LABEL_108;
        }
        DownloadHandler downloadhandler;
        if (!file.exists())
        {
            break MISSING_BLOCK_LABEL_108;
        }
        downloadhandler = DownloadHandler.getInstance(MyApplication.b());
        if (downloadhandler == null)
        {
            break MISSING_BLOCK_LABEL_108;
        }
        if (downloadhandler.isDownloadCompleted(soundinfo.playUrl64))
        {
            return file.getCanonicalPath();
        }
        if (!Utilities.isNotBlank(soundinfo.playUrl32))
        {
            break MISSING_BLOCK_LABEL_164;
        }
        file = PlayTools.getDownloadFile(soundinfo.playUrl32);
        if (file == null)
        {
            break MISSING_BLOCK_LABEL_164;
        }
        DownloadHandler downloadhandler1;
        try
        {
            if (!file.exists())
            {
                break MISSING_BLOCK_LABEL_164;
            }
            downloadhandler1 = DownloadHandler.getInstance(MyApplication.b());
        }
        // Misplaced declaration of an exception variable
        catch (SoundInfo soundinfo)
        {
            Logger.throwRuntimeException("getAudioSourceForPlayer error");
            return null;
        }
        if (downloadhandler1 == null)
        {
            break MISSING_BLOCK_LABEL_164;
        }
        if (downloadhandler1.isDownloadCompleted(soundinfo.playUrl32))
        {
            return file.getCanonicalPath();
        }
        if (1 == NetworkUtils.getNetType(MyApplication.b()))
        {
            if (TextUtils.isEmpty(soundinfo.playUrl64))
            {
                return soundinfo.playUrl32;
            } else
            {
                return soundinfo.playUrl64;
            }
        }
        if (TextUtils.isEmpty(soundinfo.playUrl32))
        {
            return soundinfo.playUrl64;
        }
        soundinfo = soundinfo.playUrl32;
        return soundinfo;
    }

    public static final String getAudioSourceForPlayer3(SoundInfo soundinfo)
    {
        while (soundinfo == null || Utilities.isBlank(soundinfo.playUrl32) && Utilities.isBlank(soundinfo.playUrl64)) 
        {
            return null;
        }
        if (MyApplication.b() == null)
        {
            if (Utilities.isBlank(soundinfo.playUrl32))
            {
                return soundinfo.playUrl32;
            } else
            {
                return soundinfo.playUrl64;
            }
        }
        File file;
        if (!Utilities.isNotBlank(soundinfo.playUrl64))
        {
            break MISSING_BLOCK_LABEL_138;
        }
        file = PlayTools.getDownloadFileWithExpandedName(soundinfo.playUrl64);
        if (file == null)
        {
            break MISSING_BLOCK_LABEL_86;
        }
        if (file.exists())
        {
            return file.getCanonicalPath();
        }
        File file1 = PlayTools.getDownloadFileWithoutExpandedName(soundinfo.playUrl64);
        if (file1 == null)
        {
            break MISSING_BLOCK_LABEL_138;
        }
        DownloadHandler downloadhandler;
        if (!file1.exists())
        {
            break MISSING_BLOCK_LABEL_138;
        }
        downloadhandler = DownloadHandler.getInstance(MyApplication.b());
        if (downloadhandler == null)
        {
            break MISSING_BLOCK_LABEL_138;
        }
        if (downloadhandler.isDownloadCompleted(soundinfo.playUrl64))
        {
            FileUtils.copyFile(file1, file);
            return file.getCanonicalPath();
        }
        if (!Utilities.isNotBlank(soundinfo.playUrl32))
        {
            break MISSING_BLOCK_LABEL_230;
        }
        file = PlayTools.getDownloadFileWithExpandedName(soundinfo.playUrl32);
        if (file == null)
        {
            break MISSING_BLOCK_LABEL_172;
        }
        if (file.exists())
        {
            return file.getCanonicalPath();
        }
        file1 = PlayTools.getDownloadFileWithoutExpandedName(soundinfo.playUrl32);
        if (file1 == null)
        {
            break MISSING_BLOCK_LABEL_230;
        }
        DownloadHandler downloadhandler1;
        try
        {
            if (!file1.exists())
            {
                break MISSING_BLOCK_LABEL_230;
            }
            downloadhandler1 = DownloadHandler.getInstance(MyApplication.b());
        }
        // Misplaced declaration of an exception variable
        catch (SoundInfo soundinfo)
        {
            Logger.throwRuntimeException("getAudioSourceForPlayer error");
            return null;
        }
        if (downloadhandler1 == null)
        {
            break MISSING_BLOCK_LABEL_230;
        }
        if (downloadhandler1.isDownloadCompleted(soundinfo.playUrl32))
        {
            file1.renameTo(file);
            FileUtils.copyFile(file1, file);
            return file.getCanonicalPath();
        }
        if (1 == NetworkUtils.getNetType(MyApplication.b()))
        {
            return soundinfo.playUrl64;
        }
        soundinfo = soundinfo.playUrl32;
        return soundinfo;
    }

    public static final PlayListControl getPlayListManager()
    {
        if (playlistManager == null)
        {
            synchronized (INSTANCE_LOCK)
            {
                if (playlistManager == null)
                {
                    playlistManager = new PlayListControl();
                }
            }
        }
        return playlistManager;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public static final boolean readPlayList(Context context)
    {
        boolean flag;
        boolean flag1;
        flag1 = false;
        Logger.log("APP_START", "readPlayList(+)");
        if (playlistManager != null && playlistManager.mPlaylist != null && playlistManager.mPlaylist.size() > 0)
        {
            return false;
        }
        context = SharedPreferencesUtil.getInstance(context).getString("play_list_record");
        flag = flag1;
        if (!Utilities.isNotBlank(context)) goto _L2; else goto _L1
_L1:
        PlayListRecord playlistrecord = (PlayListRecord)JSON.parseObject(context, com/ximalaya/ting/android/service/play/PlayListRecord);
        if (playlistrecord == null)
        {
            break MISSING_BLOCK_LABEL_338;
        }
        playlistManager = getPlayListManager();
        playlistManager.curIndex = playlistrecord.curIndex;
        playlistManager.curPlaySrc = playlistrecord.curPlaySrc;
        playlistManager.lastPlayPositon = playlistrecord.lastPlayPositon;
        playlistManager.listType = playlistrecord.listType;
        flag = TextUtils.isEmpty(playlistrecord.params);
        if (flag) goto _L4; else goto _L3
_L3:
        HashMap hashmap;
        JSONObject jsonobject;
        jsonobject = JSONObject.parseObject(playlistrecord.params);
        hashmap = new HashMap();
        Iterator iterator = jsonobject.keySet().iterator();
_L6:
        context = hashmap;
        if (!iterator.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        context = (String)iterator.next();
        hashmap.put(context, jsonobject.getString(context));
        if (true) goto _L6; else goto _L5
        Exception exception1;
        exception1;
        Exception exception;
        context = hashmap;
        exception = exception1;
_L7:
        exception.printStackTrace();
_L5:
        playlistManager.makePlaylist(playlistrecord.playlist, playlistrecord.dataSourceUrl, playlistrecord.pageId, context);
        playlistManager.mPlaylist.setCurrPosition(playlistrecord.curIndex);
        playlistManager.mPlaylist.setIsReverse(playlistrecord.isReverse);
        flag = true;
_L2:
        Logger.log("APP_START", (new StringBuilder()).append("readPlayList(-): ").append(flag).toString());
        return flag;
        context;
        Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(context.getMessage()).append(Logger.getLineInfo()).toString());
        flag = flag1;
          goto _L2
        exception;
        context = null;
          goto _L7
_L4:
        context = null;
          goto _L5
        flag = false;
          goto _L2
    }

    public static final void releaseOnExit()
    {
        if (playlistManager != null)
        {
            playlistManager.reset();
            playlistManager.details = null;
            playlistManager = null;
        }
    }

    public static final void releaseOnLogout()
    {
        if (playlistManager != null)
        {
            playlistManager.reset();
        }
    }

    private final void remove(SoundInfo soundinfo)
    {
        if (mPlaylist == null)
        {
            return;
        } else
        {
            mPlaylist.remove(soundinfo);
            return;
        }
    }

    private static final void saveLivePlayRecord()
    {
        Object obj = new PlayListRecord();
        SoundInfo soundinfo = getPlayListManager().getCurSound();
        SoundInfo soundinfo1 = new SoundInfo();
        soundinfo1.radioId = soundinfo.radioId;
        soundinfo1.radioName = soundinfo.radioName;
        soundinfo1.category = 1;
        soundinfo1.liveUrl = soundinfo.liveUrl;
        soundinfo1.playUrl32 = soundinfo.liveUrl;
        soundinfo1.playUrl64 = soundinfo.liveUrl;
        soundinfo1.coverSmall = soundinfo.coverSmall;
        soundinfo1.coverLarge = soundinfo.coverLarge;
        soundinfo1.plays_counts = soundinfo.plays_counts;
        ArrayList arraylist = new ArrayList();
        arraylist.add(soundinfo1);
        obj.playlist = arraylist;
        obj.curIndex = 0;
        obj.curPlaySrc = soundinfo.liveUrl;
        obj.lastPlayPositon = 0;
        obj.listType = playlistManager.listType;
        obj.dataSourceUrl = null;
        obj.pageId = 0;
        obj.params = null;
        obj.isReverse = false;
        try
        {
            obj = JSON.toJSONString(obj);
            SharedPreferencesUtil.getInstance(null).saveString("play_list_record", ((String) (obj)));
            return;
        }
        catch (Exception exception)
        {
            return;
        }
    }

    public static final void savePlayList()
    {
        com/ximalaya/ting/android/service/play/PlayListControl;
        JVM INSTR monitorenter ;
        if (playlistManager == null || playlistManager.mPlaylist == null || playlistManager.mPlaylist.isEmpty()) goto _L2; else goto _L1
_L1:
        int i = playlistManager.curIndex;
        if (i >= 0) goto _L3; else goto _L2
_L2:
        com/ximalaya/ting/android/service/play/PlayListControl;
        JVM INSTR monitorexit ;
        return;
_L3:
        Object obj = getPlayListManager().getCurSound();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_77;
        }
        if (((SoundInfo) (obj)).category == 0)
        {
            break MISSING_BLOCK_LABEL_77;
        }
        saveLivePlayRecord();
          goto _L2
        Exception exception;
        exception;
        throw exception;
        StringBuffer stringbuffer;
        exception = new PlayListRecord();
        exception.curIndex = playlistManager.curIndex;
        exception.curPlaySrc = playlistManager.curPlaySrc;
        exception.listType = playlistManager.listType;
        exception.dataSourceUrl = playlistManager.getPlaylist().getDataSource();
        exception.pageId = playlistManager.getPlaylist().getPageId();
        exception.isReverse = playlistManager.getPlaylist().isReverse();
        if (playlistManager.getPlaylist().getParams() == null)
        {
            break MISSING_BLOCK_LABEL_285;
        }
        HashMap hashmap = playlistManager.getPlaylist().getParams();
        stringbuffer = new StringBuffer();
        stringbuffer.append("{");
        for (Iterator iterator = hashmap.keySet().iterator(); iterator.hasNext(); stringbuffer.append(","))
        {
            String s = (String)iterator.next();
            stringbuffer.append(s).append(":").append((String)hashmap.get(s));
        }

        stringbuffer.deleteCharAt(stringbuffer.length() - 1);
        stringbuffer.append("}");
        exception.params = stringbuffer.toString();
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice == null)
        {
            break MISSING_BLOCK_LABEL_336;
        }
        exception.lastPlayPositon = localmediaservice.getCurPosition();
_L4:
        exception.playlist = playlistManager.mPlaylist.getData();
        try
        {
            exception = JSON.toJSONString(exception);
            SharedPreferencesUtil.getInstance(null).saveString("play_list_record", exception);
        }
        // Misplaced declaration of an exception variable
        catch (Exception exception) { }
          goto _L2
        exception.lastPlayPositon = 0;
          goto _L4
    }

    public final void doBeforeDelAllDownload()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (curPlaySrc != null && localmediaservice != null && !curPlaySrc.contains("http"))
        {
            localmediaservice.stopPlayTask();
            reset();
        }
    }

    public final void doBeforeDelete(SoundInfo soundinfo)
    {
        if (soundinfo != null)
        {
            boolean flag1 = false;
            SoundInfo soundinfo1 = getCurSound();
            byte byte0 = -1;
            LocalMediaService localmediaservice = LocalMediaService.getInstance();
            int j = byte0;
            boolean flag = flag1;
            if (soundinfo1 != null)
            {
                j = byte0;
                flag = flag1;
                if (localmediaservice != null)
                {
                    j = byte0;
                    flag = flag1;
                    if (soundinfo1.trackId == soundinfo.trackId)
                    {
                        j = byte0;
                        flag = flag1;
                        if (Utilities.isNotBlank(curPlaySrc))
                        {
                            j = byte0;
                            flag = flag1;
                            if (!curPlaySrc.contains("http"))
                            {
                                flag = true;
                                j = getPlayListManager().curIndex;
                                localmediaservice.stopPlayTask();
                            }
                        }
                    }
                }
            }
            if (listType == 2 || listType == 17 || listType == 14)
            {
                remove(soundinfo);
            }
            curIndex = mPlaylist.getData().indexOf(soundinfo1);
            if (flag && !mPlaylist.isEmpty())
            {
                int i = j % mPlaylist.size();
                Logger.log("doPlay", (new StringBuilder()).append("doBeforeDelete==Index: ").append(i).toString());
                localmediaservice.doPlay(i);
                return;
            }
        }
    }

    public final void doBeforeDelete(List list)
    {
        if (list != null && list.size() > 0)
        {
            int i = -1;
            SoundInfo soundinfo = getCurSound();
            LocalMediaService localmediaservice = LocalMediaService.getInstance();
            List list1 = mPlaylist.getData();
            boolean flag;
            if (soundinfo != null && localmediaservice != null && list.contains(soundinfo) && Utilities.isNotBlank(curPlaySrc) && !curPlaySrc.contains("http"))
            {
                flag = true;
                i = list1.indexOf(list.get(0));
                localmediaservice.stopPlayTask();
            } else
            {
                flag = false;
            }
            if (listType == 2 || listType == 17)
            {
                list1.removeAll(list);
            }
            curIndex = list1.indexOf(soundinfo);
            if (flag && !mPlaylist.isEmpty())
            {
                i %= mPlaylist.size();
                Logger.log("doPlay", (new StringBuilder()).append("doBeforeDelete==Index: ").append(i).toString());
                localmediaservice.doPlay(i);
                return;
            }
        }
    }

    public final SoundInfo get(int i)
    {
        while (mPlaylist == null || mPlaylist.isEmpty() || i < 0 || i >= mPlaylist.size()) 
        {
            return null;
        }
        return mPlaylist.getSoundAt(i);
    }

    public final SoundInfo getCurSound()
    {
        if (mPlaylist == null || mPlaylist.isEmpty())
        {
            return null;
        } else
        {
            return mPlaylist.getCurrSound();
        }
    }

    public Playlist getPlaylist()
    {
        return mPlaylist;
    }

    public final int getSize()
    {
        int i = 0;
        if (mPlaylist != null)
        {
            i = mPlaylist.size();
        }
        return i;
    }

    public final SoundInfo getSoundInCurList(long l1)
    {
label0:
        {
            if (mPlaylist == null)
            {
                break label0;
            }
            Object obj = mPlaylist.getData();
            if (obj == null)
            {
                break label0;
            }
            obj = ((List) (obj)).iterator();
            SoundInfo soundinfo;
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break label0;
                }
                soundinfo = (SoundInfo)((Iterator) (obj)).next();
            } while (soundinfo.trackId != l1);
            return soundinfo;
        }
        return null;
    }

    public boolean isManualPlay()
    {
        return mIsManualPlay;
    }

    public void makePlaylist(List list, String s, int i, HashMap hashmap)
    {
        mPlaylist = mPlaylistFactory.a(list, s, i, hashmap);
    }

    public final void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
    }

    public final void reset()
    {
        listType = 0;
        stop();
        mPlaylist.clearData();
        details.clear();
    }

    public void setIsManualPlay(boolean flag)
    {
        mIsManualPlay = flag;
    }

    public void stop()
    {
        curIndex = -1;
        curPlaySrc = "";
        if (mPlaylist != null)
        {
            mPlaylist.setCurrPosition(curIndex);
        }
    }

    public final int switchNext(boolean flag)
    {
        boolean flag1 = false;
        int i = 0;
        this;
        JVM INSTR monitorenter ;
        int j;
        mIsManualPlay = flag;
        j = curIndex;
        if (mPlaylist == null) goto _L2; else goto _L1
_L1:
        boolean flag2 = mPlaylist.isEmpty();
        if (!flag2) goto _L3; else goto _L2
_L2:
        i = -1;
_L16:
        this;
        JVM INSTR monitorexit ;
        return i;
_L3:
        SoundInfo soundinfo;
        int k;
        k = mPlaylist.size();
        if (curIndex < 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        soundinfo = mPlaylist.getCurrSound();
        if (soundinfo == null) goto _L5; else goto _L4
_L4:
        if (soundinfo.category == 0) goto _L5; else goto _L6
_L6:
        i = ((flag1) ? 1 : 0);
          goto _L7
_L5:
        i = SharedPreferencesUtil.getInstance(null).getInt("play_mode", 0);
          goto _L7
_L14:
        j = (curIndex + 1) % k;
        i = j;
        if (mPlaylist.size() != 1)
        {
            continue; /* Loop/switch isn't completed */
        }
        curPlaySrc = "";
        i = j;
        continue; /* Loop/switch isn't completed */
        Exception exception;
        exception;
        throw exception;
_L11:
        if (curIndex + 1 < k)
        {
            i = curIndex + 1;
            continue; /* Loop/switch isn't completed */
        }
        if (getPlaylist().hasMore()) goto _L9; else goto _L8
_L8:
        ToolUtil.showToast("\u5DF2\u7ECF\u662F\u6700\u540E\u4E00\u9996\u4E86");
        i = -1;
        continue; /* Loop/switch isn't completed */
_L13:
        j = (new Random(System.currentTimeMillis())).nextInt(k);
        i = j;
        if (mPlaylist.size() != 1)
        {
            continue; /* Loop/switch isn't completed */
        }
        curPlaySrc = null;
        i = j;
        continue; /* Loop/switch isn't completed */
_L12:
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_245;
        }
        i = (curIndex + 1) % k;
        continue; /* Loop/switch isn't completed */
        curPlaySrc = null;
        i = j;
        continue; /* Loop/switch isn't completed */
_L9:
        i = -1;
        continue; /* Loop/switch isn't completed */
_L7:
        i;
        JVM INSTR tableswitch 0 3: default 292
    //                   0 144
    //                   1 228
    //                   2 186
    //                   3 101;
           goto _L10 _L11 _L12 _L13 _L14
_L10:
        i = j;
        if (true) goto _L16; else goto _L15
_L15:
    }

    public final int switchPrev(boolean flag)
    {
        boolean flag1 = false;
        int i = 0;
        this;
        JVM INSTR monitorenter ;
        int j;
        mIsManualPlay = flag;
        j = curIndex;
        if (mPlaylist == null) goto _L2; else goto _L1
_L1:
        boolean flag2 = mPlaylist.isEmpty();
        if (!flag2) goto _L3; else goto _L2
_L2:
        i = -1;
_L15:
        this;
        JVM INSTR monitorexit ;
        return i;
_L3:
        SoundInfo soundinfo;
        int k;
        k = mPlaylist.size();
        if (curIndex < 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        soundinfo = mPlaylist.getCurrSound();
        if (soundinfo == null) goto _L5; else goto _L4
_L4:
        if (soundinfo.category == 0) goto _L5; else goto _L6
_L6:
        i = ((flag1) ? 1 : 0);
          goto _L7
_L5:
        i = SharedPreferencesUtil.getInstance(null).getInt("play_mode", 0);
          goto _L7
_L13:
        i = ((curIndex - 1) + k) % k;
        continue; /* Loop/switch isn't completed */
_L10:
        if (curIndex > 0)
        {
            i = curIndex - 1;
            continue; /* Loop/switch isn't completed */
        }
        ToolUtil.showToast("\u5DF2\u7ECF\u662F\u7B2C\u4E00\u9996\u4E86");
        i = j;
        continue; /* Loop/switch isn't completed */
_L12:
        i = (new Random(System.currentTimeMillis())).nextInt(k);
        continue; /* Loop/switch isn't completed */
_L11:
        if (!flag) goto _L9; else goto _L8
_L8:
        i = (curIndex - 1) % k;
        continue; /* Loop/switch isn't completed */
        Exception exception;
        exception;
        throw exception;
_L7:
        i;
        JVM INSTR tableswitch 0 3: default 220
    //                   0 117
    //                   1 165
    //                   2 146
    //                   3 101;
           goto _L9 _L10 _L11 _L12 _L13
_L9:
        i = j;
        if (true) goto _L15; else goto _L14
_L14:
    }

    public boolean swtichTo(int i)
    {
        while (mPlaylist == null || mPlaylist.isEmpty() || i < 0 || i >= mPlaylist.size()) 
        {
            return false;
        }
        mPlaylist.setCurrPosition(i);
        curIndex = i;
        return true;
    }

    public final void updateCurSoundInfo(int i, SoundInfo soundinfo)
    {
        if (soundinfo.category == 0)
        {
            if (i == curIndex && getCurSound().trackId == soundinfo.trackId)
            {
                mPlaylist.updateSoundAt(i, soundinfo);
            }
        } else
        if (i == curIndex && getCurSound().programScheduleId == soundinfo.programScheduleId)
        {
            mPlaylist.updateSoundAt(i, soundinfo);
            return;
        }
    }

    public void updateLivePlaylist(List list)
    {
        int i;
        SoundInfo soundinfo = getCurSound();
        List list1 = mPlaylist.getData();
        if (list1 != null)
        {
            list1.clear();
            list1.addAll(list);
        } else
        {
            new ArrayList(list);
        }
        if (soundinfo == null) goto _L2; else goto _L1
_L1:
        i = 0;
_L7:
        if (i >= list.size()) goto _L2; else goto _L3
_L3:
        if (!soundinfo.equals((SoundInfo)list.get(i))) goto _L5; else goto _L4
_L4:
        curIndex = i;
        Logger.log((new StringBuilder()).append("LivePlay updateLivePlaylist curIndex =").append(curIndex).toString());
_L2:
        mPlaylist.setCurrPosition(curIndex);
        return;
_L5:
        i++;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public void updatePlaylist(List list)
    {
        if (mPlaylist != null)
        {
            mPlaylist.update(list);
            curIndex = mPlaylist.getCurrPosition();
        }
        savePlayList();
    }

}
