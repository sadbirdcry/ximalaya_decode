// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.sound.SoundDetailModel;
import com.ximalaya.ting.android.model.sound.SoundDetails;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.concurrent.ConcurrentHashMap;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            PlayListControl, PlayTools

public class LoadSoundDetailAndPlay extends MyAsyncTask
{

    private Context actContext;
    private boolean isShowPlayActivity;
    private int listType;
    private ProgressDialog mProgressDialog;
    private SoundDetails mSoundDetails;
    private SoundInfo mSoundInfo;
    private long mTrackId;
    private SoundDetailModel sdm;
    private String xdcsData;

    public LoadSoundDetailAndPlay(int i, Context context, boolean flag, String s)
    {
        sdm = null;
        mSoundDetails = null;
        mTrackId = 0L;
        actContext = null;
        mSoundInfo = null;
        mProgressDialog = null;
        listType = 0;
        actContext = context;
        listType = i;
        isShowPlayActivity = flag;
        xdcsData = s;
    }

    protected transient SoundDetails doInBackground(SoundInfo asoundinfo[])
    {
        mSoundInfo = asoundinfo[0];
        if (actContext != null && mSoundInfo != null && mSoundInfo.trackId > 0L)
        {
            mTrackId = mSoundInfo.trackId;
            if (PlayListControl.getPlayListManager().details.containsKey(Long.valueOf(mTrackId)))
            {
                mSoundDetails = (SoundDetails)PlayListControl.getPlayListManager().details.get(Long.valueOf(mTrackId));
                return mSoundDetails;
            }
            asoundinfo = e.C + '/' + mTrackId;
            asoundinfo = f.a().a(asoundinfo, null, false);
            if (((com.ximalaya.ting.android.b.n.a) (asoundinfo)).b == 1)
            {
                asoundinfo = ((com.ximalaya.ting.android.b.n.a) (asoundinfo)).a;
            } else
            {
                asoundinfo = null;
            }
            Logger.log((new StringBuilder()).append("result:").append(asoundinfo).toString());
            try
            {
                if ("0".equals(JSON.parseObject(asoundinfo).get("ret").toString()))
                {
                    sdm = (SoundDetailModel)JSON.parseObject(asoundinfo, com/ximalaya/ting/android/model/sound/SoundDetailModel);
                }
            }
            // Misplaced declaration of an exception variable
            catch (SoundInfo asoundinfo[])
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(asoundinfo.getMessage()).append(Logger.getLineInfo()).toString());
            }
            if (sdm != null)
            {
                return ModelHelper.toSoundDetail(sdm);
            }
        }
        return null;
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((SoundInfo[])aobj);
    }

    protected void onCancelled()
    {
        super.onCancelled();
    }

    protected void onPostExecute(SoundDetails sounddetails)
    {
        if (mProgressDialog != null)
        {
            mProgressDialog.dismiss();
        }
        if (sounddetails != null)
        {
            if (!PlayListControl.getPlayListManager().details.containsKey(Long.valueOf(mTrackId)))
            {
                PlayListControl.getPlayListManager().details.put(Long.valueOf(mTrackId), sounddetails);
            }
            PlayTools.gotoPlay(listType, new SoundInfo(sounddetails), actContext, isShowPlayActivity, xdcsData);
        } else
        {
            Toast.makeText(actContext, "\u67E5\u8BE2\u4E0D\u5230\u58F0\u97F3\u6E90", 0).show();
        }
        super.onPostExecute(sounddetails);
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((SoundDetails)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        if (actContext != null)
        {
            mProgressDialog = ToolUtil.createProgressDialog(actContext, 0, true, true);
            mProgressDialog.setMessage("\u52A0\u8F7D\u58F0\u97F3\u8BE6\u60C5\u4E2D...");
            mProgressDialog.show();
        }
    }
}
