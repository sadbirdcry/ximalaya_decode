// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            TingMediaPlayer

class ab
    implements Runnable
{

    final TingMediaPlayer a;

    ab(TingMediaPlayer tingmediaplayer)
    {
        a = tingmediaplayer;
        super();
    }

    public void run()
    {
        int i = NetworkUtils.getNetType(TingMediaPlayer.access$000(a));
        if (i == -1)
        {
            ToolUtil.makePlayNotification(TingMediaPlayer.access$000(a), "\u559C\u9A6C\u62C9\u96C5", "\u6E29\u99A8\u63D0\u793A", "\u6CA1\u6709\u53EF\u7528\u7F51\u7EDC, \u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u7EE7\u7EED\u64AD\u653E");
            (new DialogBuilder(MyApplication.a())).setMessage("\u6CA1\u6709\u53EF\u7528\u7F51\u7EDC, \u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u7EE7\u7EED\u64AD\u653E").showWarning();
        } else
        if (i == 0 && !SharedPreferencesUtil.getInstance(MyApplication.b()).getBoolean("is_download_enabled_in_3g", false))
        {
            ToolUtil.makePlayNotification(TingMediaPlayer.access$000(a), "\u559C\u9A6C\u62C9\u96C5", "\u6E29\u99A8\u63D0\u793A", "\u559C\u9A6C\u62C9\u96C5\u5C06\u8981\u4F7F\u7528\u4F60\u7684\u79FB\u52A8\u6D41\u91CF\u64AD\u653E\u58F0\u97F3,\u8981\u7EE7\u7EED\u5417\uFF1F");
            TingMediaPlayer.access$1300(a);
            return;
        }
    }
}
