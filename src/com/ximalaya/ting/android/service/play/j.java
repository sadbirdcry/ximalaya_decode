// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.content.Context;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.util.Logger;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            PlayTools, LocalMediaService

final class j
    implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
{

    final SoundInfo a;
    final boolean b;
    final Context c;
    final String d;
    final LocalMediaService e;
    final int f;

    j(SoundInfo soundinfo, boolean flag, Context context, String s, LocalMediaService localmediaservice, int i)
    {
        a = soundinfo;
        b = flag;
        c = context;
        d = s;
        e = localmediaservice;
        f = i;
        super();
    }

    public void onExecute()
    {
        Logger.log("doPlay", "gotoPlay==Index0: 0");
        ArrayList arraylist = new ArrayList();
        arraylist.add(a);
        if (b)
        {
            if (a.category == 0)
            {
                PlayTools.startPlayerActivity(c, d);
            } else
            {
                PlayTools.startPlayerActivity(c, true, d);
            }
        }
        e.doPlayAndUpdatePlaylist(null, 0, null, f, arraylist, 0);
    }
}
