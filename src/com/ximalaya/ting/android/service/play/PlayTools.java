// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.sound.SoundDetails;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            PlayListControl, LocalMediaService, j, i, 
//            LoadSoundDetailAndPlay, Playlist

public final class PlayTools
{

    public static final String ACTION_LAUNCH_FROM_WIDGET = "com.ximalaya.ting.android.launch_from_widget";
    private static final String sClsName = "com.ximalaya.ting.android.activity.login.WelcomeActivity";
    private static final String sPkgName = "com.ximalaya.ting.android";

    public PlayTools()
    {
    }

    public static final String getAudioSourceExpandedName(String s)
    {
        if (Utilities.isBlank(s) || !s.contains("http"))
        {
            return null;
        } else
        {
            return s.substring(s.lastIndexOf("."));
        }
    }

    public static final File getDownloadFile(String s)
    {
        return getDownloadFileWithoutExpandedName(s);
    }

    public static final File getDownloadFileWithExpandedName(String s)
    {
        if (Utilities.isBlank(s) || !s.contains("http"))
        {
            return null;
        } else
        {
            String s1 = s.substring(s.lastIndexOf("."));
            return new File(a.aj + "/" + ToolUtil.md5(s) + s1);
        }
    }

    public static final File getDownloadFileWithoutExpandedName(String s)
    {
        if (Utilities.isBlank(s) || !s.contains("http"))
        {
            return null;
        } else
        {
            return new File(a.aj + "/" + ToolUtil.md5(s));
        }
    }

    private static String getRecentPath(Context context)
    {
        if (context == null)
        {
            return "";
        } else
        {
            return ((MyApplication)(MyApplication)context.getApplicationContext()).a;
        }
    }

    public static final void gotoPlay(int k, SoundInfo soundinfo, Context context, String s)
    {
        gotoPlay(k, soundinfo, context, true, s);
    }

    public static final void gotoPlay(int k, SoundInfo soundinfo, Context context, boolean flag, String s)
    {
        ToolUtil.onEvent(context, "play", getRecentPath(context));
        PlayListControl.getPlayListManager().setIsManualPlay(true);
        while (soundinfo == null || context == null || Utilities.isBlank(soundinfo.playUrl32) && Utilities.isBlank(soundinfo.playUrl64)) 
        {
            return;
        }
        if (!NetworkUtils.isNetworkAvaliable(context.getApplicationContext()))
        {
            CustomToast.showToast(context, "\u7F51\u7EDC\u4E0D\u7ED9\u529B", 1000);
        }
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice == null && !LocalMediaService.startLocalMediaService(context.getApplicationContext()))
        {
            Toast.makeText(context, "\u591A\u5A92\u4F53\u670D\u52A1\u542F\u52A8\u5931\u8D25\uFF0C\u8BF7\u91CD\u542F\u5E94\u7528\u6216\u8005\u8BBE\u5907", 0).show();
            return;
        }
        boolean flag1 = DownloadHandler.getInstance(context.getApplicationContext()).isDownloadCompleted(soundinfo);
        int l = NetworkUtils.getNetType(context);
        boolean flag2 = SharedPreferencesUtil.getInstance(context).getBoolean("is_download_enabled_in_3g", false);
        if (1 == l || flag2 || flag1)
        {
            Logger.log("doPlay", "gotoPlay==Index0: 0");
            ArrayList arraylist = new ArrayList();
            arraylist.add(soundinfo);
            if (flag)
            {
                if (soundinfo.category == 0)
                {
                    startPlayerActivity(context, s);
                } else
                {
                    startPlayerActivity(context, true, s);
                }
            }
            localmediaservice.doPlayAndUpdatePlaylist(null, 0, null, k, arraylist, 0);
            return;
        } else
        {
            NetworkUtils.showChangeNetWorkSetConfirm(new j(soundinfo, flag, context, s, localmediaservice, k), null);
            return;
        }
    }

    public static final void gotoPlay(int k, String s, int l, HashMap hashmap, List list, int i1, Context context, boolean flag, 
            String s1)
    {
        gotoPlay(k, s, l, hashmap, list, i1, context, flag, true, s1);
    }

    public static final void gotoPlay(int k, String s, int l, HashMap hashmap, List list, int i1, Context context, boolean flag, 
            boolean flag1, String s1)
    {
        ToolUtil.onEvent(context, "play", getRecentPath(context));
        PlayListControl.getPlayListManager().setIsManualPlay(true);
        if (list == null || list.size() <= 0 || i1 < 0 || i1 >= list.size() || context == null)
        {
            return;
        }
        SoundInfo soundinfo = (SoundInfo)list.get(i1);
        boolean flag2 = DownloadHandler.getInstance(context.getApplicationContext()).isDownloadCompleted(soundinfo);
        if (!NetworkUtils.isNetworkAvaliable(context.getApplicationContext()) && !flag2)
        {
            CustomToast.showToast(context, "\u7F51\u7EDC\u4E0D\u7ED9\u529B", 1000);
        }
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice == null && !LocalMediaService.startLocalMediaService(context.getApplicationContext()))
        {
            Toast.makeText(context, "\u591A\u5A92\u4F53\u670D\u52A1\u542F\u52A8\u5931\u8D25\uFF0C\u8BF7\u91CD\u542F\u5E94\u7528\u6216\u8005\u8BBE\u5907", 0).show();
            return;
        }
        int j1 = NetworkUtils.getNetType(context);
        boolean flag3 = SharedPreferencesUtil.getInstance(context).getBoolean("is_download_enabled_in_3g", false);
        if (1 == j1 || flag3 || flag2)
        {
            if (flag)
            {
                if (soundinfo.category == 0)
                {
                    startPlayerActivity(context, s1);
                } else
                {
                    startPlayerActivity(context, true, s1);
                }
            }
            localmediaservice.doPlayAndUpdatePlaylist(s, l, hashmap, k, list, i1, flag1);
            return;
        } else
        {
            NetworkUtils.showChangeNetWorkSetConfirm(new i(flag, soundinfo, context, s1, localmediaservice, s, l, hashmap, k, list, i1, flag1), null);
            return;
        }
    }

    public static final void gotoPlay(int k, List list, int l, Context context, String s)
    {
        gotoPlay(k, null, 0, null, list, l, context, true, true, s);
    }

    public static final void gotoPlay(int k, List list, int l, Context context, boolean flag, String s)
    {
        gotoPlay(k, null, 0, null, list, l, context, flag, true, s);
    }

    public static final void gotoPlay(int k, List list, int l, Context context, boolean flag, boolean flag1, String s)
    {
        gotoPlay(k, null, 0, null, list, l, context, flag, flag1, s);
    }

    public static final void gotoPlay(Context context)
    {
        ToolUtil.onEvent(context, "nowplaying_icon");
        startPlayerActivity(context, null);
    }

    public static final void gotoPlayLocals(int k, List list, int l, Context context, String s)
    {
        PlayListControl.getPlayListManager().setIsManualPlay(true);
        ToolUtil.onEvent(context, "play", getRecentPath(context));
        if (list == null || list.size() <= 0 || l < 0 || l >= list.size() || context == null)
        {
            return;
        }
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice == null && !LocalMediaService.startLocalMediaService(context.getApplicationContext()))
        {
            Toast.makeText(context, "\u591A\u5A92\u4F53\u670D\u52A1\u542F\u52A8\u5931\u8D25\uFF0C\u8BF7\u91CD\u542F\u5E94\u7528\u6216\u8005\u8BBE\u5907", 0).show();
            return;
        } else
        {
            Logger.log("doPlay", (new StringBuilder()).append("gotoPlayLocals==Index: ").append(l).toString());
            startPlayerActivity(context, s);
            localmediaservice.doPlayAndUpdatePlaylist(null, 0, null, k, list, l);
            return;
        }
    }

    public static final void gotoPlayWithoutUrl(int k, SoundInfo soundinfo, Context context, boolean flag, String s)
    {
        if (soundinfo == null || soundinfo.trackId <= 0L || context == null)
        {
            return;
        }
        PlayListControl.getPlayListManager().setIsManualPlay(true);
        if (!NetworkUtils.isNetworkAvaliable(context.getApplicationContext()))
        {
            CustomToast.showToast(context, "\u7F51\u7EDC\u4E0D\u7ED9\u529B", 1000);
        }
        if (PlayListControl.getPlayListManager().details.containsKey(Long.valueOf(soundinfo.trackId)))
        {
            gotoPlay(k, new SoundInfo((SoundDetails)PlayListControl.getPlayListManager().details.get(Long.valueOf(soundinfo.trackId))), context, flag, s);
            return;
        } else
        {
            (new LoadSoundDetailAndPlay(k, context, flag, s)).myexec(new SoundInfo[] {
                soundinfo
            });
            return;
        }
    }

    public static final void initPlayList(Context context, boolean flag)
    {
        HashMap hashmap = null;
        PlayListControl playlistcontrol = PlayListControl.getPlayListManager();
        if (playlistcontrol == null || playlistcontrol.getPlaylist() == null || playlistcontrol.getPlaylist().size() <= 0)
        {
            String s;
            ArrayList arraylist;
            int k;
            int l;
            int i1;
            boolean flag1;
            if (PlayListControl.readPlayList(context))
            {
                l = PlayListControl.getPlayListManager().listType;
                i1 = PlayListControl.getPlayListManager().curIndex;
                arraylist = new ArrayList(PlayListControl.getPlayListManager().getPlaylist().getData());
                s = PlayListControl.getPlayListManager().getPlaylist().getDataSource();
                k = PlayListControl.getPlayListManager().getPlaylist().getPageId();
                hashmap = PlayListControl.getPlayListManager().getPlaylist().getParams();
                flag1 = PlayListControl.getPlayListManager().getPlaylist().isReverse();
                PlayListControl.getPlayListManager().reset();
            } else
            {
                flag1 = false;
                k = 0;
                s = null;
                l = 0;
                i1 = 0;
                arraylist = null;
            }
            ToolUtil.onEvent(context, "play", getRecentPath(context));
            if (arraylist != null && arraylist.size() > 0 && i1 >= 0 && i1 < arraylist.size() && context != null)
            {
                if (!NetworkUtils.isNetworkAvaliable(context.getApplicationContext()))
                {
                    CustomToast.showToast(context, "\u7F51\u7EDC\u4E0D\u7ED9\u529B", 1000);
                }
                if (LocalMediaService.getInstance() == null && !LocalMediaService.startLocalMediaService(context.getApplicationContext()))
                {
                    Toast.makeText(context, "\u591A\u5A92\u4F53\u670D\u52A1\u542F\u52A8\u5931\u8D25\uFF0C\u8BF7\u91CD\u542F\u5E94\u7528\u6216\u8005\u8BBE\u5907", 0).show();
                    return;
                } else
                {
                    LocalMediaService.getInstance().doPlayAndUpdatePlaylist(s, k, hashmap, l, arraylist, i1, flag);
                    PlayListControl.getPlayListManager().getPlaylist().setIsReverse(flag1);
                    return;
                }
            }
        }
    }

    public static final boolean isLocalAudioSource(String s)
    {
        return Utilities.isNotBlank(s) && !s.contains("http");
    }

    public static final void startMainApp(Context context)
    {
        if (context == null)
        {
            return;
        } else
        {
            Intent intent = new Intent();
            intent.setAction("com.ximalaya.ting.android.launch_from_widget");
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.addFlags(0x10000000);
            intent.addFlags(32768);
            intent.setClassName("com.ximalaya.ting.android", "com.ximalaya.ting.android.activity.login.WelcomeActivity");
            context.startActivity(intent);
            return;
        }
    }

    public static final void startPlayerActivity(Context context, String s)
    {
        startPlayerActivity(context, false, s);
    }

    public static final void startPlayerActivity(Context context, boolean flag, String s)
    {
        if (!(context instanceof MainTabActivity2)) goto _L2; else goto _L1
_L1:
        if (!flag) goto _L4; else goto _L3
_L3:
        ((MainTabActivity2)context).showLivePlayFragment(s);
_L6:
        return;
_L4:
        ((MainTabActivity2)context).showPlayFragment(s);
        return;
_L2:
        if (context instanceof Activity)
        {
            ((Activity)context).finish();
            return;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }
}
