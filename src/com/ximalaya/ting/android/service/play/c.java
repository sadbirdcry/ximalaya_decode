// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service.play;

import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.service.play:
//            PlayListControl, Playlist, LocalMediaService

class c extends MyAsyncTask
{

    final boolean a;
    final LocalMediaService b;

    c(LocalMediaService localmediaservice, boolean flag)
    {
        b = localmediaservice;
        a = flag;
        super();
    }

    protected transient List a(Void avoid[])
    {
        return PlayListControl.getPlayListManager().getPlaylist().loadMore();
    }

    protected void a(List list)
    {
        super.onPostExecute(list);
        if (list != null && list.size() > 0)
        {
            PlayListControl.getPlayListManager().getPlaylist().append(list);
            int i = PlayListControl.getPlayListManager().switchNext(a);
            if (i >= 0)
            {
                b.doPlay(i);
            }
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((List)obj);
    }
}
