// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.RemoteViews;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.model.check_version.MsgCarry;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.UpgradeFileUtil;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

// Referenced classes of package com.ximalaya.ting.android.service:
//            g, h

public class UpdateService extends Service
{

    private static final int TIMEOUT = 10000;
    private String apk_name;
    RemoteViews contentView;
    private String down_url;
    private Notification notification;
    private NotificationManager notificationManager;
    private int notification_id;
    private PendingIntent pendingIntent;
    private Intent updateIntent;

    public UpdateService()
    {
        notification_id = 0;
    }

    public static void downloadFile(Handler handler, String s, String s1)
        throws Exception
    {
        s = FreeFlowUtil.getHttpURLConnection(new URL(s));
        s.setConnectTimeout(10000);
        s.setReadTimeout(10000);
        int i1 = s.getContentLength();
        if (s.getResponseCode() == 404)
        {
            if (MyApplication.b() != null)
            {
                CustomToast.showToast(MyApplication.b(), "\u65E0\u6CD5\u8FDE\u63A5\u65E0\u670D\u52A1\u5668", 1);
            }
            throw new Exception("fail!");
        }
        InputStream inputstream = s.getInputStream();
        s1 = new FileOutputStream(s1, false);
        byte abyte0[] = new byte[1024];
        int j = 0;
        int i = 0;
        do
        {
            int k = inputstream.read(abyte0);
            if (k == -1)
            {
                break;
            }
            s1.write(abyte0, 0, k);
            k = i + k;
            if (j != 0)
            {
                i = k;
                if ((k * 100) / i1 - 5 < j)
                {
                    continue;
                }
            }
            int l = j + 5;
            j = l;
            i = k;
            if (handler != null)
            {
                Message message = new Message();
                message.what = 2;
                message.obj = new MsgCarry(null, null, l);
                handler.sendMessage(message);
                j = l;
                i = k;
            }
        } while (true);
        if (s != null)
        {
            s.disconnect();
        }
        inputstream.close();
        s1.close();
    }

    public void createNotification()
    {
        notificationManager = (NotificationManager)getSystemService("notification");
        notification = new Notification(0x7f020598, "\u66F4\u65B0", System.currentTimeMillis());
        contentView = new RemoteViews(getPackageName(), 0x7f03017f);
        contentView.setTextViewText(0x7f0a05d2, "\u6B63\u5728\u4E0B\u8F7D");
        contentView.setTextViewText(0x7f0a05d1, "0%");
        contentView.setProgressBar(0x7f0a05d3, 100, 0, false);
        notification.contentView = contentView;
        updateIntent = new Intent(this, com/ximalaya/ting/android/activity/MainTabActivity2);
        updateIntent.addFlags(0x20000000);
        pendingIntent = PendingIntent.getActivity(this, 0, updateIntent, 0);
        notification.contentIntent = pendingIntent;
        notificationManager.notify(notification_id, notification);
    }

    public void createThread()
    {
        g g1 = new g(this);
        (new Thread(new h(this, new Message(), g1))).start();
    }

    public long downloadUpdateFile(String s, String s1)
        throws Exception
    {
        s = FreeFlowUtil.getHttpURLConnection(new URL(s));
        s.setConnectTimeout(10000);
        s.setReadTimeout(10000);
        int l = s.getContentLength();
        if (s.getResponseCode() == 404)
        {
            Logger.throwRuntimeException("\u4E0B\u8F7D\u66F4\u65B0\u6587\u4EF6\uFF0C\u670D\u52A1\u5668\u8FD4\u56DE404");
            return 0L;
        }
        InputStream inputstream = s.getInputStream();
        s1 = new FileOutputStream(s1, false);
        byte abyte0[] = new byte[1024];
        int j = 0;
        int i = 0;
        do
        {
            int k = inputstream.read(abyte0);
            if (k == -1)
            {
                break;
            }
            s1.write(abyte0, 0, k);
            k = i + k;
            if (j != 0)
            {
                i = k;
                if ((k * 100) / l - 5 < j)
                {
                    continue;
                }
            }
            j += 5;
            contentView.setTextViewText(0x7f0a05d1, (new StringBuilder()).append(j).append("%").toString());
            contentView.setProgressBar(0x7f0a05d3, 100, j, false);
            notificationManager.notify(notification_id, notification);
            i = k;
        } while (true);
        if (s != null)
        {
            s.disconnect();
        }
        inputstream.close();
        s1.close();
        return (long)i;
    }

    public IBinder onBind(Intent intent)
    {
        return null;
    }

    public int onStartCommand(Intent intent, int i, int j)
    {
        if (intent != null)
        {
            apk_name = intent.getStringExtra("apk_name");
            down_url = intent.getStringExtra("download_url");
            UpgradeFileUtil.createFile(apk_name);
            createNotification();
            createThread();
        }
        return super.onStartCommand(intent, i, j);
    }



/*
    static PendingIntent access$002(UpdateService updateservice, PendingIntent pendingintent)
    {
        updateservice.pendingIntent = pendingintent;
        return pendingintent;
    }

*/






}
