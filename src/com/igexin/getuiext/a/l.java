// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.a;

import android.content.Context;
import android.content.Intent;
import com.igexin.getuiext.data.Consts;
import com.igexin.getuiext.service.GetuiExtService;
import com.igexin.getuiext.util.h;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.igexin.getuiext.a:
//            a, m

public class l
    implements a
{

    private final String a = "GetuiExt-QueryUpdateAction";

    public l()
    {
    }

    private void a(Context context, String s, String s1, String s2, String s3, boolean flag)
    {
        int i;
        try
        {
            s = new JSONObject(s);
            i = s.getInt("result");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return;
        }
        if (!flag) goto _L2; else goto _L1
_L1:
        if (Consts.APPID == null)
        {
            Consts.APPID = h.a(context);
        }
        if (Consts.APPID != null)
        {
            Intent intent = new Intent((new StringBuilder()).append("com.igexin.sdk.action.").append(Consts.APPID).toString());
            intent.putExtra("action", 10003);
            intent.putExtra("result", i);
            context.sendBroadcast(intent);
        }
          goto _L2
_L4:
        Intent intent1 = new Intent(context, com/igexin/getuiext/service/GetuiExtService);
        intent1.putExtra("action", "handleUpdate");
        intent1.putExtra("name", s.getString("name"));
        intent1.putExtra("logo_url", s.getString("logo"));
        intent1.putExtra("versionCode", s.getString("versionCode"));
        intent1.putExtra("versionName", s.getString("versionName"));
        intent1.putExtra("fullSize", s.getLong("fullsize"));
        intent1.putExtra("diffSize", s.getLong("diffsize"));
        intent1.putExtra("updateType", s.getString("updateType"));
        intent1.putExtra("diffFileurl", s.getString("diffFileurl"));
        intent1.putExtra("fullFileurl", s.getString("fullFileurl"));
        intent1.putExtra("originalUrl", s.getString("originalUrl"));
        intent1.putExtra("diffChecksum", s.getString("diffChecksum"));
        intent1.putExtra("fullChecksum", s.getString("fullChecksum"));
        intent1.putExtra("description", s.getString("description"));
        if (s.has("recommendType"))
        {
            intent1.putExtra("recommendType", s.getString("recommendType"));
        }
        if (s.has("recommendApps"))
        {
            intent1.putExtra("recommendApps", s.getString("recommendApps"));
        }
        if (s.has("recommendImg"))
        {
            intent1.putExtra("recommendImg", s.getString("recommendImg"));
        }
        if (s.has("recommendTxt"))
        {
            intent1.putExtra("recommendTxt", s.getString("recommendTxt"));
        }
        intent1.putExtra("pkgname", s1);
        intent1.putExtra("sendId", s2);
        intent1.putExtra("taskid", s3);
        intent1.putExtra("what", 11001);
        context.startService(intent1);
        return;
_L2:
        if (i == 0)
        {
            return;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    static void a(l l1, Context context, String s, String s1, String s2, String s3, boolean flag)
    {
        l1.a(context, s, s1, s2, s3, flag);
    }

    public void a(Context context, Intent intent)
    {
        String s2 = intent.getStringExtra("pkgName");
        String s3 = intent.getStringExtra("versionCode");
        boolean flag = intent.getBooleanExtra("manual", false);
        String s = intent.getStringExtra("taskid");
        String s1 = intent.getStringExtra("sendId");
        intent = h.a(context, s2, s3, intent.getBooleanExtra("cleanInstall", false));
        if (intent == null)
        {
            return;
        } else
        {
            (new m(this, context, s1, s, flag)).execute(new JSONObject[] {
                intent
            });
            return;
        }
    }
}
