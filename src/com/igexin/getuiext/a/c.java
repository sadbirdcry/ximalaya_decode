// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.a;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.igexin.getuiext.data.Consts;
import com.igexin.getuiext.data.a;
import com.igexin.getuiext.service.GetuiExtService;
import com.igexin.getuiext.util.g;
import com.igexin.getuiext.util.h;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.zip.GZIPOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.igexin.getuiext.a:
//            a, d

public class c
    implements com.igexin.getuiext.a.a
{

    private final String a = "GetuiExt-BindCIDAction";

    public c()
    {
    }

    private static void a(long l)
    {
        a.b = l;
        com.igexin.getuiext.b.c.d().a().b();
    }

    private void a(Context context, long l)
    {
        Object obj;
        int j;
        obj = new ArrayList();
        a(context, ((List) (obj)));
        j = ((List) (obj)).size();
        if (j > 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        JSONObject jsonobject = new JSONObject();
        Object obj1;
        byte abyte0[];
        Object obj2;
        byte abyte1[];
        int i;
        try
        {
            jsonobject.put("action", "reportApps");
            jsonobject.put("cid", Consts.CID);
            jsonobject.put("app_id", Consts.APPID);
            jsonobject.put("selfpkg", context.getPackageName());
            context = new JSONArray();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return;
        }
        i = 0;
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        obj1 = new JSONObject();
        obj2 = (com.igexin.getuiext.data.a.c)((List) (obj)).get(i);
        ((JSONObject) (obj1)).put("pkgname", ((com.igexin.getuiext.data.a.c) (obj2)).b);
        ((JSONObject) (obj1)).put("versionCode", String.valueOf(((com.igexin.getuiext.data.a.c) (obj2)).d));
        ((JSONObject) (obj1)).put("checksum", ((com.igexin.getuiext.data.a.c) (obj2)).f);
        context.put(obj1);
        i++;
        if (true) goto _L4; else goto _L3
_L4:
        break MISSING_BLOCK_LABEL_95;
_L3:
        obj1 = g.a(String.valueOf(System.currentTimeMillis()));
        obj = new ByteArrayOutputStream();
        obj2 = new GZIPOutputStream(((java.io.OutputStream) (obj)));
        ((GZIPOutputStream) (obj2)).write(context.toString().getBytes());
        ((GZIPOutputStream) (obj2)).close();
        i = ((ByteArrayOutputStream) (obj)).size();
        context = new byte[i + 16];
        abyte1 = ((String) (obj1)).substring(0, 8).getBytes();
        abyte0 = ((String) (obj1)).substring(24, 32).getBytes();
        System.arraycopy(abyte1, 0, context, 0, 8);
        System.arraycopy(((ByteArrayOutputStream) (obj)).toByteArray(), 0, context, 8, i);
        System.arraycopy(abyte0, 0, context, i + 8, 8);
        ((ByteArrayOutputStream) (obj)).close();
        jsonobject.put("apps", new String(context, "ISO-8859-1"));
        context = com.igexin.getuiext.util.c.a("http://sdk.open.inc2.igexin.com/api.php", jsonobject, Consts.DEFAULT_RETRY_TIMES);
        if (context == null) goto _L1; else goto _L5
_L5:
        if (!"1".equals((new JSONObject(context)).getString("result"))) goto _L1; else goto _L6
_L6:
        a.a = l;
        com.igexin.getuiext.b.c.d().a().a();
        return;
    }

    private void a(Context context, List list)
    {
        d d1 = new d(this);
        List list1 = context.getPackageManager().getInstalledPackages(0);
        int j = list1.size();
        int i = 0;
        while (i < j) 
        {
            try
            {
                PackageInfo packageinfo = (PackageInfo)list1.get(i);
                ApplicationInfo applicationinfo = packageinfo.applicationInfo;
                if ((applicationinfo.flags & 1) <= 0)
                {
                    com.igexin.getuiext.data.a.c c1 = new com.igexin.getuiext.data.a.c();
                    c1.b = applicationinfo.packageName;
                    c1.d = packageinfo.versionCode;
                    c1.f = h.b(context, applicationinfo.packageName, applicationinfo.sourceDir);
                    list.add(c1);
                }
            }
            catch (Exception exception) { }
            i++;
        }
        Collections.sort(list, d1);
    }

    public void a(Context context, Intent intent)
    {
        intent = intent.getStringExtra("cid");
        Consts.CID = intent;
        if (System.currentTimeMillis() - a.b < 0x5265c00L)
        {
            context.stopService(new Intent(context, com/igexin/getuiext/service/GetuiExtService));
            return;
        }
        JSONObject jsonobject = new JSONObject();
        long l;
        long l1;
        try
        {
            jsonobject.put("action", "bindApp");
            jsonobject.put("cid", intent);
            jsonobject.put("app_id", Consts.APPID);
            jsonobject.put("pkgname", context.getPackageName());
            jsonobject.put("inc_version", Consts.VERSION);
            intent = com.igexin.getuiext.util.c.a("http://sdk.open.inc2.igexin.com/api.php", jsonobject, Consts.DEFAULT_RETRY_TIMES);
        }
        // Misplaced declaration of an exception variable
        catch (Intent intent)
        {
            break MISSING_BLOCK_LABEL_169;
        }
        if (intent == null)
        {
            break MISSING_BLOCK_LABEL_169;
        }
        if (!"1".equals((new JSONObject(intent)).getString("result")))
        {
            break MISSING_BLOCK_LABEL_169;
        }
        l = System.currentTimeMillis();
        l1 = a.a;
        a(l);
        if (l - l1 > 0xf731400L)
        {
            try
            {
                a(context, l);
            }
            // Misplaced declaration of an exception variable
            catch (Intent intent) { }
        }
        context.stopService(new Intent(context, com/igexin/getuiext/service/GetuiExtService));
        return;
    }
}
