// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.a;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Bundle;
import android.widget.RemoteViews;
import com.igexin.getuiext.data.Consts;
import com.igexin.getuiext.data.a.e;
import com.igexin.getuiext.data.a.f;
import com.igexin.getuiext.service.a;
import com.igexin.getuiext.service.c;
import com.igexin.getuiext.util.h;
import java.io.IOException;
import java.util.Random;
import org.apache.http.MethodNotSupportedException;

// Referenced classes of package com.igexin.getuiext.a:
//            a, h

public class g
    implements com.igexin.getuiext.a.a
{

    private final String a = "GetuiExt-HandleQueryUpdateResponseAction";
    private Random b;

    public g()
    {
        b = null;
    }

    private String a(String s, Intent intent)
    {
        if (s != null)
        {
            if (s.equals("app"))
            {
                return intent.getStringExtra("recommendApps");
            }
            if (s.equals("img"))
            {
                return intent.getStringExtra("recommendImg");
            }
            if (s.equals("txt"))
            {
                return intent.getStringExtra("recommendTxt");
            }
        }
        return null;
    }

    private void b(Context context, Intent intent)
    {
        e e1;
        boolean flag;
        flag = false;
        e1 = new e();
        e1.n = f.a(intent.getStringExtra("updateType"));
        com.igexin.getuiext.a.h.a[e1.n.ordinal()];
        JVM INSTR tableswitch 1 2: default 60
    //                   1 890
    //                   2 908;
           goto _L1 _L2 _L3
_L1:
        Object obj;
        NotificationManager notificationmanager;
        Notification notification;
        PendingIntent pendingintent;
        android.graphics.Bitmap bitmap;
        int i;
        int k;
        String s = intent.getStringExtra("name");
        if (s != null && s.equals("null"))
        {
            s = "\u672A\u77E5\u5E94\u7528";
        }
        e1.a = s;
        obj = intent.getStringExtra("logo_url");
        if (obj != null && ((String) (obj)).equals("null"))
        {
            obj = null;
        }
        e1.f = ((String) (obj));
        e1.b = intent.getStringExtra("pkgname");
        e1.c = intent.getStringExtra("versionName");
        e1.d = Integer.parseInt(intent.getStringExtra("versionCode"));
        e1.i = intent.getLongExtra("fullSize", 0L);
        e1.o = intent.getLongExtra("diffSize", 0L);
        e1.p = intent.getStringExtra("diffChecksum");
        e1.q = intent.getStringExtra("fullChecksum");
        e1.j = intent.getStringExtra("sendId");
        e1.h = intent.getStringExtra("originalUrl");
        e1.s = intent.getStringExtra("recommendType");
        e1.t = a(e1.s, intent);
        e1.k = intent.getStringExtra("taskid");
        e1.r = intent.getStringExtra("description");
        e1.m = h.a(context, e1.b);
        c.a().a(e1.b, e1);
        i = (int)System.currentTimeMillis();
        pendingintent = a(context, e1, e1.s, e1.t, i);
        notificationmanager = (NotificationManager)context.getSystemService("notification");
        notification = new Notification();
        notification.icon = 0x1080081;
        notification.tickerText = (new StringBuilder()).append(s).append("\u6709\u66F4\u65B0").toString();
        notification.defaults = 4;
        notification.flags = 16;
        int j = ((AudioManager)context.getSystemService("audio")).getRingerMode();
        ApplicationInfo applicationinfo;
        int l;
        long l1;
        if (j == 1)
        {
            notification.sound = null;
            notification.defaults = notification.defaults | 2;
        } else
        if (j == 2)
        {
            notification.defaults = notification.defaults | 2;
            notification.defaults = notification.defaults | 1;
        }
        applicationinfo = context.getApplicationInfo();
        j = context.getResources().getIdentifier("notification_inc", "layout", applicationinfo.packageName);
        if (j == 0) goto _L5; else goto _L4
_L4:
        notification.contentView = new RemoteViews(applicationinfo.packageName, j);
        k = context.getResources().getIdentifier("notification_icon", "id", applicationinfo.packageName);
        l = context.getResources().getIdentifier("notification_update_icon", "id", applicationinfo.packageName);
        bitmap = com.igexin.getuiext.util.f.a(context, "inc-default.png");
        if (obj != null) goto _L7; else goto _L6
_L6:
        if (bitmap != null)
        {
            notification.contentView.setImageViewBitmap(k, bitmap);
        } else
        {
            notification.contentView.setImageViewResource(k, 0x1080093);
        }
_L8:
        notification.contentView.setOnClickPendingIntent(l, pendingintent);
        try
        {
            notification.contentView.setImageViewBitmap(l, BitmapFactory.decodeStream(context.getAssets().open("inc_update.png")));
        }
        // Misplaced declaration of an exception variable
        catch (Intent intent) { }
        notification.contentView.setTextViewText(context.getResources().getIdentifier("notification_name", "id", applicationinfo.packageName), s);
        notification.contentView.setTextViewText(context.getResources().getIdentifier("notification_version", "id", applicationinfo.packageName), (new StringBuilder()).append("V").append(e1.c).toString());
        notification.contentView.setTextViewText(context.getResources().getIdentifier("notification_fullsize", "id", applicationinfo.packageName), (new StringBuilder()).append(String.format("%.2f", new Object[] {
            Float.valueOf((float)e1.i / 1048576F)
        })).append("M").toString());
        if (!flag)
        {
            notification.contentView.setTextViewText(context.getResources().getIdentifier("notification_diffsize", "id", applicationinfo.packageName), (new StringBuilder()).append("\u53EA\u9700:").append(String.format("%.2f", new Object[] {
                Float.valueOf((float)e1.o / 1048576F)
            })).append("M").toString());
        }
        notification.contentView.setTextViewText(context.getResources().getIdentifier("notification_update_text", "id", applicationinfo.packageName), "\u66F4\u65B0");
_L5:
        if (j == 0)
        {
            notification.setLatestEventInfo(context, "", "", pendingintent);
        } else
        {
            notification.contentIntent = pendingintent;
        }
        intent = new h();
        l1 = intent.b();
        if (System.currentTimeMillis() - l1 < 0x5265c00L)
        {
            return;
        } else
        {
            notificationmanager.notify(i, notification);
            intent.a();
            com.igexin.getuiext.service.a.a(context, e1, 1);
            return;
        }
_L2:
        e1.g = intent.getStringExtra("diffFileurl");
        flag = false;
          goto _L1
_L3:
        flag = true;
        e1.g = intent.getStringExtra("fullFileurl");
          goto _L1
_L7:
        intent = null;
        obj = BitmapFactory.decodeStream(com.igexin.getuiext.util.c.a("GET", ((String) (obj)), null));
        intent = ((Intent) (obj));
_L9:
        if (intent != null)
        {
            notification.contentView.setImageViewBitmap(k, intent);
        } else
        if (bitmap != null)
        {
            notification.contentView.setImageViewBitmap(k, bitmap);
        } else
        {
            notification.contentView.setImageViewResource(k, 0x1080093);
        }
          goto _L8
        MethodNotSupportedException methodnotsupportedexception;
        methodnotsupportedexception;
          goto _L9
    }

    PendingIntent a(Context context, e e1, String s, String s1, int i)
    {
        Intent intent = new Intent("com.igexin.increment");
        intent.addFlags(0x10000000);
        intent.putExtra("action", "download");
        Bundle bundle = new Bundle();
        bundle.putString("name", e1.a);
        bundle.putLong("diffSize", e1.o);
        bundle.putLong("fullSize", e1.i);
        bundle.putString("versionName", e1.c);
        bundle.putString("description", e1.r);
        bundle.putString("url", e1.g);
        bundle.putString("logo_url", e1.f);
        bundle.putInt("notifID", i);
        bundle.putString("updateType", e1.n.toString());
        bundle.putString("pkgname", e1.b);
        bundle.putInt("versionCode", e1.d);
        bundle.putInt("verifyCode", Consts.verifyCode);
        bundle.putString("originalUrl", e1.h);
        bundle.putInt("previous_version_code", e1.m);
        bundle.putString("recommendType", s);
        bundle.putString("promotion_attrs", s1);
        bundle.putString("taskid", e1.k);
        bundle.putString("sendId", e1.j);
        intent.putExtras(bundle);
        if (b == null)
        {
            b = new Random();
            b.setSeed(System.currentTimeMillis());
        }
        return PendingIntent.getBroadcast(context, b.nextInt(1000), intent, 0x8000000);
    }

    public void a(Context context, Intent intent)
    {
        b(context, intent);
    }
}
