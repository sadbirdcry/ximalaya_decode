// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class a extends SQLiteOpenHelper
{

    public static long a = 0L;
    public static long b = 0L;
    public static a c;
    SQLiteDatabase d;

    public a(Context context)
    {
        super(context, "increment.db", null, 3);
        d = null;
        c = this;
        c();
    }

    private void a(String s, int i, String s1, long l)
    {
        try
        {
            ContentValues contentvalues = new ContentValues();
            contentvalues.put("id", Integer.valueOf(i));
            contentvalues.put("name", s1);
            contentvalues.put("value", Long.valueOf(l));
            d.replace(s, null, contentvalues);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
    }

    private void c()
    {
        Object obj1;
        obj1 = null;
        d = getReadableDatabase();
        Object obj = d.rawQuery("select id, value from config order by id", null);
        if (obj == null) goto _L2; else goto _L1
_L1:
        obj1 = obj;
        if (!((Cursor) (obj)).moveToNext()) goto _L2; else goto _L3
_L3:
        obj1 = obj;
        ((Cursor) (obj)).getInt(0);
        JVM INSTR tableswitch 1 3: default 144
    //                   1 72
    //                   2 25
    //                   3 99;
           goto _L1 _L4 _L1 _L5
_L4:
        obj1 = obj;
        a = ((Cursor) (obj)).getLong(1);
          goto _L1
        obj;
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
_L7:
        return;
_L5:
        obj1 = obj;
        b = ((Cursor) (obj)).getLong(1);
          goto _L1
        Exception exception;
        exception;
_L8:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        throw exception;
_L2:
        if (obj == null) goto _L7; else goto _L6
_L6:
        ((Cursor) (obj)).close();
        return;
        exception;
        obj = null;
          goto _L8
    }

    public Cursor a(String s)
    {
        return a(s, null, null, null, null, null, null);
    }

    public Cursor a(String s, String as[], String s1, String as1[], String s2, String s3, String s4)
    {
        d = getReadableDatabase();
        try
        {
            s = d.query(s, as, s1, as1, s2, s3, s4);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return null;
        }
        return s;
    }

    public void a()
    {
        d = getWritableDatabase();
        a("config", 1, "lastUploadAppListTime", a);
    }

    public void a(String s, ContentValues contentvalues)
    {
        d = getWritableDatabase();
        try
        {
            d.insert(s, null, contentvalues);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
    }

    public void a(String s, String s1, String as[])
    {
        d = getWritableDatabase();
        try
        {
            d.delete(s, s1, as);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
    }

    public void b()
    {
        d = getWritableDatabase();
        a("config", 3, "lastBindCIDTime", b);
    }

    public void onCreate(SQLiteDatabase sqlitedatabase)
    {
        try
        {
            sqlitedatabase.execSQL("create table if not exists config (id integer primary key,name text,value text)");
            sqlitedatabase.execSQL("create table if not exists appinfo(download_id integer primary key, name text, pkgName text,  versionCode integer, versionName text, logo text, fullSize long, diffSize long, url text, updateType text, diffChecksum text, fullChecksum text)");
            sqlitedatabase.execSQL("create table if not exists biinfo (id integer primary key, value text, bitype text)");
            return;
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            return;
        }
    }

    public void onUpgrade(SQLiteDatabase sqlitedatabase, int i, int j)
    {
        try
        {
            sqlitedatabase.execSQL("DROP TABLE IF EXISTS config");
            sqlitedatabase.execSQL("DROP TABLE IF EXISTS appinfo");
            sqlitedatabase.execSQL("DROP TABLE IF EXISTS biinfo");
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        onCreate(sqlitedatabase);
    }

}
