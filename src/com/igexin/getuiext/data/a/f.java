// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.data.a;

import java.util.Locale;

public final class f extends Enum
{

    public static final f a;
    public static final f b;
    private static final f c[];

    private f(String s, int i)
    {
        super(s, i);
    }

    public static f a(String s)
    {
        if (s != null && s.toUpperCase(Locale.US).equals("DIFF"))
        {
            return b;
        } else
        {
            return a;
        }
    }

    public static f[] a()
    {
        return (f[])c.clone();
    }

    static 
    {
        a = new f("FULL", 0);
        b = new f("DIFF", 1);
        c = (new f[] {
            a, b
        });
    }
}
