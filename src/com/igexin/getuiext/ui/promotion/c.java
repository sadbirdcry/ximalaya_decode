// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.ui.promotion;

import android.content.Context;
import android.view.View;
import com.igexin.getuiext.ui.a;
import com.igexin.getuiext.ui.f;

// Referenced classes of package com.igexin.getuiext.ui.promotion:
//            b, m

public abstract class c
    implements android.view.View.OnClickListener
{

    protected b a;
    protected Context b;
    protected int c;
    protected int d;
    protected f e;
    private m f;
    private String g;
    private String h;

    public c(Context context, m m)
    {
        b = context.getApplicationContext();
        f = m;
        e = com.igexin.getuiext.ui.f.a(context);
        c = com.igexin.getuiext.ui.a.a(context, 2.0F);
        d = com.igexin.getuiext.ui.a.a(context, 3F);
    }

    public abstract View a(int i, int j);

    public m a()
    {
        return f;
    }

    protected void a(View view)
    {
        if (a != null)
        {
            if (!a.a(this))
            {
                b(view);
                a.b(this);
            }
            return;
        } else
        {
            b(view);
            return;
        }
    }

    public void a(String s)
    {
        g = s;
    }

    public String b()
    {
        return g;
    }

    protected abstract void b(View view);

    public void b(String s)
    {
        h = s;
    }

    public String c()
    {
        return h;
    }

    public abstract void c(String s);

    public void onClick(View view)
    {
        a(view);
    }
}
