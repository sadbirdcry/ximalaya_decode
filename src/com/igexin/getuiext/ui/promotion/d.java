// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.ui.promotion;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.igexin.getuiext.service.a;
import com.igexin.getuiext.service.c;
import com.igexin.getuiext.ui.b;
import com.igexin.getuiext.ui.f;
import com.igexin.getuiext.util.h;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

// Referenced classes of package com.igexin.getuiext.ui.promotion:
//            c, f, UrlImageView, e, 
//            m

public class d extends com.igexin.getuiext.ui.promotion.c
{

    private final String f = "GetuiExt-PromotionApp";
    private final int g = 201;
    private final int h = 300;
    private final int i = 310;
    private final int j = 320;
    private final int k = 4;
    private ArrayList l;
    private SparseArray m;
    private RelativeLayout n;

    public d(Context context, m m1)
    {
        super(context, m1);
        l = new ArrayList();
        m = new SparseArray();
    }

    public View a(int i1, int j1)
    {
        i1 = l.size();
        if (i1 == 0)
        {
            return null;
        }
        if (i1 > 4)
        {
            i1 = 4;
        }
        n = new RelativeLayout(super.b);
        j1 = d + c;
        n.setPadding(j1, j1, j1, j1);
        Object obj = new TextView(super.b);
        ((TextView) (obj)).setTextSize(2, 12F);
        ((TextView) (obj)).setTextColor(0xff000000);
        ((TextView) (obj)).setText("\u731C\u4F60\u559C\u6B22");
        ((TextView) (obj)).setId(201);
        android.widget.RelativeLayout.LayoutParams layoutparams = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        layoutparams.topMargin = c * 3;
        n.addView(((View) (obj)), layoutparams);
        obj = new LinearLayout(super.b);
        ((LinearLayout) (obj)).setOrientation(0);
        layoutparams = new android.widget.RelativeLayout.LayoutParams(-1, -2);
        layoutparams.addRule(3, 201);
        layoutparams.topMargin = c + c;
        n.addView(((View) (obj)), layoutparams);
        ((LinearLayout) (obj)).setWeightSum(4F);
        for (j1 = 0; j1 < i1; j1++)
        {
            com.igexin.getuiext.ui.promotion.f f1 = (com.igexin.getuiext.ui.promotion.f)l.get(j1);
            com.igexin.getuiext.service.c.a().a(f1.b, f1);
            RelativeLayout relativelayout = new RelativeLayout(super.b);
            relativelayout.setTag(f1);
            relativelayout.setOnClickListener(this);
            Object obj1 = new android.widget.LinearLayout.LayoutParams(0, -2);
            obj1.weight = 1.0F;
            ((LinearLayout) (obj)).addView(relativelayout, ((android.view.ViewGroup.LayoutParams) (obj1)));
            Object obj2 = new UrlImageView(super.b);
            ((UrlImageView) (obj2)).a(f1.f);
            ((UrlImageView) (obj2)).setId(j1 + 300);
            obj1 = new android.widget.RelativeLayout.LayoutParams(d * 16, d * 16);
            ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(14);
            relativelayout.addView(((View) (obj2)), ((android.view.ViewGroup.LayoutParams) (obj1)));
            obj1 = new TextView(super.b);
            ((TextView) (obj1)).setTextSize(2, 6F);
            ((TextView) (obj1)).setPadding(d, d, d, d);
            ((TextView) (obj1)).setTextColor(0xff000000);
            ((TextView) (obj1)).setText("\u70B9\u51FB\u4E0B\u8F7D");
            ((TextView) (obj1)).setGravity(17);
            ((TextView) (obj1)).setId(j1 + 310);
            ((TextView) (obj1)).setBackgroundDrawable(e.b("inc_setup_bg.png"));
            android.widget.RelativeLayout.LayoutParams layoutparams1 = new android.widget.RelativeLayout.LayoutParams(-2, -2);
            layoutparams1.addRule(8, ((UrlImageView) (obj2)).getId());
            layoutparams1.bottomMargin = d;
            layoutparams1.addRule(14);
            relativelayout.addView(((View) (obj1)), layoutparams1);
            obj2 = new ProgressBar(super.b, null, 0x1010078);
            ((ProgressBar) (obj2)).setId(j1 + 320);
            ((ProgressBar) (obj2)).getProgressDrawable().setColorFilter(-256, android.graphics.PorterDuff.Mode.MULTIPLY);
            ((ProgressBar) (obj2)).setMax(100);
            layoutparams1 = new android.widget.RelativeLayout.LayoutParams(d * 12, d * 2);
            layoutparams1.addRule(3, ((TextView) (obj1)).getId());
            layoutparams1.addRule(14);
            relativelayout.addView(((View) (obj2)), layoutparams1);
            ((ProgressBar) (obj2)).setVisibility(4);
            obj1 = new TextView(super.b);
            ((TextView) (obj1)).setTextSize(2, 15F);
            ((TextView) (obj1)).setTextColor(0xff000000);
            ((TextView) (obj1)).setText(f1.a);
            ((TextView) (obj1)).setSingleLine();
            ((TextView) (obj1)).setEllipsize(android.text.TextUtils.TruncateAt.END);
            layoutparams1 = new android.widget.RelativeLayout.LayoutParams(-2, -2);
            layoutparams1.addRule(3, ((ProgressBar) (obj2)).getId());
            layoutparams1.addRule(14);
            relativelayout.addView(((View) (obj1)), layoutparams1);
            f1.j = b();
            f1.k = c();
            f1.m = j1 + 1;
            com.igexin.getuiext.service.a.a(super.b, com.igexin.getuiext.service.a.a(f1, 1), "3");
        }

        return n;
    }

    public void a(int i1, int j1, String s)
    {
        com.igexin.getuiext.ui.promotion.f f1 = (com.igexin.getuiext.ui.promotion.f)m.get(i1);
        if (f1 != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        int k1;
        k1 = l.indexOf(f1);
        if (j1 >= 100)
        {
            continue; /* Loop/switch isn't completed */
        }
        s = (ProgressBar)n.findViewById(k1 + 320);
        if (s == null) goto _L1; else goto _L3
_L3:
        s.setProgress(j1);
        return;
        if (s == null || s.equals("")) goto _L1; else goto _L4
_L4:
        f1.e = s;
        m.remove(i1);
        f1.n = com.igexin.getuiext.ui.d.d;
        n.findViewById(k1 + 320).setVisibility(4);
        ((TextView)n.findViewById(k1 + 310)).setText("\u70B9\u51FB\u5B89\u88C5");
        return;
    }

    protected void b(View view)
    {
        view = ((View) (view.getTag()));
        if (!(view instanceof com.igexin.getuiext.ui.promotion.f)) goto _L2; else goto _L1
_L1:
        TextView textview;
        ProgressBar progressbar;
        view = (com.igexin.getuiext.ui.promotion.f)view;
        int i1 = l.indexOf(view);
        textview = (TextView)n.findViewById(i1 + 310);
        progressbar = (ProgressBar)n.findViewById(i1 + 320);
        com.igexin.getuiext.ui.promotion.e.a[((com.igexin.getuiext.ui.promotion.f) (view)).n.ordinal()];
        JVM INSTR tableswitch 1 5: default 108
    //                   1 109
    //                   2 165
    //                   3 192
    //                   4 219
    //                   5 108;
           goto _L2 _L3 _L4 _L5 _L6 _L2
_L2:
        return;
_L3:
        int j1 = com.igexin.getuiext.ui.b.a(b, view, false);
        view.l = j1;
        m.put(j1, view);
        com.igexin.getuiext.service.a.a(super.b, view, 2);
        view.n = com.igexin.getuiext.ui.d.b;
        progressbar.setVisibility(0);
        textview.setText("\u70B9\u51FB\u6682\u505C");
        return;
_L4:
        com.igexin.getuiext.ui.b.a(b, ((com.igexin.getuiext.ui.promotion.f) (view)).l, view);
        view.n = com.igexin.getuiext.ui.d.c;
        textview.setText("\u70B9\u51FB\u7EE7\u7EED");
        return;
_L5:
        com.igexin.getuiext.ui.b.a(b, ((com.igexin.getuiext.ui.promotion.f) (view)).l, view);
        view.n = com.igexin.getuiext.ui.d.b;
        textview.setText("\u70B9\u51FB\u6682\u505C");
        return;
_L6:
        if (!TextUtils.isEmpty(((com.igexin.getuiext.ui.promotion.f) (view)).e))
        {
            com.igexin.getuiext.ui.b.a(b, ((com.igexin.getuiext.ui.promotion.f) (view)).e, ((com.igexin.getuiext.ui.promotion.f) (view)).l, view);
            ((NotificationManager)b.getSystemService("notification")).cancel(((com.igexin.getuiext.ui.promotion.f) (view)).l << 11);
            return;
        }
        if (true) goto _L2; else goto _L7
_L7:
    }

    public void c(String s)
    {
        l.clear();
        s = new JSONArray(s);
        int j1 = s.length();
        int i1 = 0;
        while (i1 < j1) 
        {
            JSONObject jsonobject = s.getJSONObject(i1);
            String s1 = jsonobject.getString("pkgname");
            if (!com.igexin.getuiext.util.h.c(b, s1))
            {
                com.igexin.getuiext.ui.promotion.f f1 = new com.igexin.getuiext.ui.promotion.f();
                f1.b = s1;
                f1.f = jsonobject.getString("logo_url");
                f1.a = jsonobject.getString("name");
                f1.g = jsonobject.getString("url");
                f1.i = jsonobject.getLong("size");
                l.add(f1);
            }
            i1++;
        }
    }
}
