// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.ui.promotion;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.igexin.getuiext.data.a.d;
import com.igexin.getuiext.service.a;
import com.igexin.getuiext.ui.b;
import org.json.JSONArray;
import org.json.JSONObject;

// Referenced classes of package com.igexin.getuiext.ui.promotion:
//            c, a, UrlImageView, j, 
//            m

public class i extends c
{

    private String f;
    private String g;
    private String h;
    private com.igexin.getuiext.ui.promotion.a i;
    private d j;
    private int k;

    public i(Context context, m m)
    {
        super(context, m);
        i = a.c;
        k = -1;
    }

    public View a(int l, int i1)
    {
        RelativeLayout relativelayout = new RelativeLayout(super.b);
        UrlImageView urlimageview = new UrlImageView(b);
        urlimageview.setPadding(d, d, d, d);
        urlimageview.setOnClickListener(this);
        urlimageview.setScaleType(android.widget.ImageView.ScaleType.FIT_CENTER);
        android.widget.RelativeLayout.LayoutParams layoutparams;
        if (g != null)
        {
            urlimageview.setImageBitmap(BitmapFactory.decodeFile(g));
        } else
        {
            urlimageview.a(f);
        }
        layoutparams = new android.widget.RelativeLayout.LayoutParams(l, i1);
        layoutparams.addRule(13);
        relativelayout.addView(urlimageview, layoutparams);
        urlimageview.setOnClickListener(this);
        com.igexin.getuiext.service.a.a(b, a(), 1, b(), c());
        return relativelayout;
    }

    protected void b(View view)
    {
        com.igexin.getuiext.service.a.a(b, a(), 2, b(), c());
        if (h == null) goto _L2; else goto _L1
_L1:
        com.igexin.getuiext.ui.promotion.j.a[i.ordinal()];
        JVM INSTR tableswitch 1 2: default 60
    //                   1 61
    //                   2 110;
           goto _L2 _L3 _L4
_L2:
        return;
_L3:
        if (j.g != null)
        {
            if (k == -1)
            {
                k = com.igexin.getuiext.ui.b.a(b, j, false);
                return;
            } else
            {
                Toast.makeText(b, "\u5E94\u7528\u4E0B\u8F7D\u5DF2\u7ECF\u5F00\u59CB\uFF0C\u8BF7\u67E5\u770B\u901A\u77E5\u680F\u8FDB\u5EA6\u3002", 0).show();
                return;
            }
        }
        if (true) goto _L2; else goto _L4
_L4:
        if (!h.startsWith("http://") && !h.startsWith("https://"))
        {
            h = (new StringBuilder()).append("http://").append(h).toString();
        }
        view = new Intent("android.intent.action.VIEW");
        view.setData(Uri.parse(h));
        view.setFlags(0x10000000);
        try
        {
            b.startActivity(view);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            return;
        }
    }

    public void c(String s)
    {
        s = (new JSONArray(s)).getJSONObject(0);
        if (s.has("imgUrl"))
        {
            f = s.getString("imgUrl");
        }
        if (s.has("linkUrl"))
        {
            h = s.getString("linkUrl");
        }
        if (s.has("imgPath"))
        {
            g = s.getString("imgPath");
        }
        if (s.has("action"))
        {
            i = com.igexin.getuiext.ui.promotion.a.a(s.getString("action"));
        }
        if (i.equals(com.igexin.getuiext.ui.promotion.a.a))
        {
            j = new d();
            j.a = s.getString("linkAppName");
            j.b = s.getString("linkAppPkg");
            j.f = s.getString("linkAppLogo");
            j.g = h;
        }
    }
}
