// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.ui.promotion;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.igexin.getuiext.data.a.d;
import com.igexin.getuiext.service.a;
import com.igexin.getuiext.ui.b;
import org.json.JSONArray;
import org.json.JSONObject;

// Referenced classes of package com.igexin.getuiext.ui.promotion:
//            c, a, l, m

public class k extends c
{

    private String f;
    private int g;
    private int h;
    private int i;
    private String j;
    private int k;
    private int l;
    private com.igexin.getuiext.ui.promotion.a m;
    private String n;
    private d o;
    private int p;

    public k(Context context, m m1)
    {
        super(context, m1);
        g = 15;
        h = 0xff000000;
        j = "";
        k = 12;
        l = 0xff000000;
        m = a.c;
        p = -1;
    }

    public View a(int i1, int j1)
    {
        ScrollView scrollview = new ScrollView(super.b);
        scrollview.setVerticalFadingEdgeEnabled(true);
        scrollview.setVerticalScrollBarEnabled(true);
        RelativeLayout relativelayout = new RelativeLayout(super.b);
        i1 = c + d;
        relativelayout.setPadding(i1, i1, i1, i1);
        scrollview.addView(relativelayout, -1, -1);
        relativelayout.setOnClickListener(this);
        TextView textview = new TextView(super.b);
        textview.setTextColor(h);
        textview.setTextSize(2, g);
        textview.setGravity(i);
        textview.setId(101);
        android.widget.RelativeLayout.LayoutParams layoutparams;
        if (TextUtils.isEmpty(f))
        {
            textview.setVisibility(8);
        } else
        {
            textview.setText(f);
        }
        relativelayout.addView(textview, new android.widget.RelativeLayout.LayoutParams(-1, -2));
        textview = new TextView(super.b);
        textview.setTextColor(l);
        textview.setTextSize(2, k);
        textview.setText(j);
        layoutparams = new android.widget.RelativeLayout.LayoutParams(-1, -1);
        layoutparams.addRule(3, 101);
        layoutparams.topMargin = d * 2;
        relativelayout.addView(textview, layoutparams);
        com.igexin.getuiext.service.a.a(b, a(), 1, b(), c());
        return scrollview;
    }

    protected void b(View view)
    {
        com.igexin.getuiext.service.a.a(b, a(), 2, b(), c());
        if (n == null) goto _L2; else goto _L1
_L1:
        com.igexin.getuiext.ui.promotion.l.a[m.ordinal()];
        JVM INSTR tableswitch 1 2: default 60
    //                   1 61
    //                   2 110;
           goto _L2 _L3 _L4
_L2:
        return;
_L3:
        if (o.g != null)
        {
            if (p == -1)
            {
                p = com.igexin.getuiext.ui.b.a(b, o, false);
                return;
            } else
            {
                Toast.makeText(b, "\u5E94\u7528\u4E0B\u8F7D\u5DF2\u7ECF\u5F00\u59CB\uFF0C\u8BF7\u67E5\u770B\u901A\u77E5\u680F\u8FDB\u5EA6\u3002", 0).show();
                return;
            }
        }
        if (true) goto _L2; else goto _L4
_L4:
        if (!n.startsWith("http://") && !n.startsWith("https://"))
        {
            n = (new StringBuilder()).append("http://").append(n).toString();
        }
        view = new Intent("android.intent.action.VIEW");
        view.setData(Uri.parse(n));
        view.setFlags(0x10000000);
        try
        {
            b.startActivity(view);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            return;
        }
    }

    public void c(String s)
    {
        s = (new JSONArray(s)).getJSONObject(0);
        if (s.has("title"))
        {
            f = s.getString("title");
        }
        if (s.has("titleFontSize"))
        {
            g = s.getInt("titleFontSize");
        }
        if (s.has("titleFontColor"))
        {
            try
            {
                h = Color.parseColor(s.getString("titleFontColor"));
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                h = 0xff000000;
            }
        }
        if (s.has("titleAlign"))
        {
            Object obj = s.getString("titleAlign");
            if ("right".equals(obj))
            {
                i = 5;
            } else
            if ("center".equals(obj))
            {
                i = 17;
            } else
            {
                i = 3;
            }
        }
        if (s.has("content"))
        {
            j = s.getString("content");
        }
        if (s.has("contentFontSize"))
        {
            k = s.getInt("contentFontSize");
        }
        if (s.has("contentFontColor"))
        {
            try
            {
                l = Color.parseColor(s.getString("contentFontColor"));
            }
            catch (Exception exception)
            {
                l = 0xff000000;
            }
        }
        if (s.has("action"))
        {
            if ("download".equals(s.getString("action")))
            {
                m = com.igexin.getuiext.ui.promotion.a.a;
            } else
            {
                m = com.igexin.getuiext.ui.promotion.a.b;
            }
        }
        if (s.has("linkUrl"))
        {
            n = s.getString("linkUrl");
        }
        if (m.equals(com.igexin.getuiext.ui.promotion.a.a))
        {
            o = new d();
            o.a = s.getString("linkAppName");
            o.f = s.getString("linkAppLogo");
            o.b = s.getString("linkAppPkg");
            o.g = n;
        }
    }
}
