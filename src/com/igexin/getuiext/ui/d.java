// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.ui;


public final class d extends Enum
{

    public static final d a;
    public static final d b;
    public static final d c;
    public static final d d;
    public static final d e;
    public static final d f;
    private static final d g[];

    private d(String s, int i)
    {
        super(s, i);
    }

    public static d[] a()
    {
        return (d[])g.clone();
    }

    static 
    {
        a = new d("SLEEP", 0);
        b = new d("DOWNLOADING", 1);
        c = new d("PAUSE", 2);
        d = new d("DOWNLOADED", 3);
        e = new d("INSTALLED", 4);
        f = new d("ERROR", 5);
        g = (new d[] {
            a, b, c, d, e, f
        });
    }
}
