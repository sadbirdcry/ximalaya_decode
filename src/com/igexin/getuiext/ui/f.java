// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.ui;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.NinePatch;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import java.io.IOException;

// Referenced classes of package com.igexin.getuiext.ui:
//            e

public class f
{

    private static f b;
    private Context a;
    private AssetManager c;

    private f(Context context)
    {
        a = context;
        a();
    }

    public static f a(Context context)
    {
        if (b == null)
        {
            b = new f(context);
        }
        return b;
    }

    private void a()
    {
        c = a.getAssets();
    }

    public int a(String s, String s1)
    {
        ApplicationInfo applicationinfo = a.getApplicationInfo();
        return a.getResources().getIdentifier(s, s1, applicationinfo.packageName);
    }

    public NinePatchDrawable a(String s)
    {
        s = BitmapFactory.decodeStream(c.open(s));
        byte abyte0[] = s.getNinePatchChunk();
        if (!NinePatch.isNinePatchChunk(abyte0))
        {
            break MISSING_BLOCK_LABEL_56;
        }
        s = new NinePatchDrawable(a.getResources(), s, abyte0, e.a(abyte0).a, null);
        return s;
        s;
        s.printStackTrace();
        return null;
    }

    public Drawable b(String s)
    {
        try
        {
            s = Drawable.createFromStream(c.open(s), null);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return null;
        }
        return s;
    }

    public Bitmap c(String s)
    {
        try
        {
            s = BitmapFactory.decodeStream(c.open(s));
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return null;
        }
        return s;
    }
}
