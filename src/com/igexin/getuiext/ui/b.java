// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import com.igexin.download.SdkDownLoader;
import com.igexin.getuiext.b.a;
import com.igexin.getuiext.b.c;
import com.igexin.getuiext.data.Consts;
import com.igexin.getuiext.data.a.d;
import com.igexin.getuiext.data.a.e;
import com.igexin.getuiext.ui.promotion.f;

// Referenced classes of package com.igexin.getuiext.ui:
//            c

public class b
{

    public static int a(Context context, d d1, boolean flag)
    {
        int i = -1;
        context = context.getApplicationContext();
        if (d1 != null)
        {
            Object obj = SdkDownLoader.getInstantiate(context);
            ((SdkDownLoader) (obj)).setDownloadDir("/libs/tmp/");
            i = ((SdkDownLoader) (obj)).newTask(d1.g, d1.a, d1.f, flag, "GETUI_INC");
            obj = c.d();
            ((c) (obj)).a(context);
            ((c) (obj)).c().a(i, d1);
            a(context, d1);
        }
        return i;
    }

    public static void a(Context context, int i, com.igexin.getuiext.data.a.a a1)
    {
        context = context.getApplicationContext();
        Intent intent = new Intent("com.igexin.download.action.notify.click");
        intent.putExtra("action", "run");
        intent.putExtra("id", i);
        intent.putExtra("verifyCode", Consts.verifyCode);
        context.sendBroadcast(intent);
        if (a1 != null)
        {
            if (a1 instanceof e)
            {
                com.igexin.getuiext.service.a.a(context, a1, 9);
            } else
            if (a1 instanceof f)
            {
                com.igexin.getuiext.service.a.a(context, a1, 7);
                return;
            }
        }
    }

    public static void a(Context context, int i, d d1)
    {
        context = context.getApplicationContext();
        Intent intent = new Intent("com.igexin.download.action.notify.click");
        intent.putExtra("action", "pause");
        intent.putExtra("id", i);
        intent.putExtra("verifyCode", Consts.verifyCode);
        context.sendBroadcast(intent);
        if (d1 != null)
        {
            if (d1 instanceof e)
            {
                com.igexin.getuiext.service.a.a(context, d1, 8);
            } else
            if (d1 instanceof f)
            {
                com.igexin.getuiext.service.a.a(context, d1, 6);
                return;
            }
        }
    }

    private static void a(Context context, d d1)
    {
        (new com.igexin.getuiext.ui.c(d1, context)).execute(new Void[0]);
    }

    public static void a(Context context, String s)
    {
        s = context.getPackageManager().getLaunchIntentForPackage(s);
        if (s != null)
        {
            context.startActivity(s);
        }
    }

    public static void a(Context context, String s, int i, com.igexin.getuiext.data.a.a a1)
    {
        Intent intent = new Intent("com.igexin.increment");
        intent.putExtra("action", "install");
        intent.putExtra("filepath", s);
        intent.putExtra("pkgname", a1.b);
        intent.putExtra("verifyCode", Consts.verifyCode);
        context.sendBroadcast(intent);
    }
}
