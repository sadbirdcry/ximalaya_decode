// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.activity;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.igexin.getuiext.data.a.e;
import com.igexin.getuiext.data.a.f;
import com.igexin.getuiext.ui.a;
import com.igexin.getuiext.ui.b;
import com.igexin.getuiext.ui.d;
import com.igexin.getuiext.ui.promotion.UrlImageView;
import com.igexin.getuiext.ui.promotion.c;
import com.igexin.getuiext.ui.promotion.g;
import com.igexin.getuiext.ui.promotion.m;
import com.igexin.getuiext.util.h;
import org.json.JSONException;

// Referenced classes of package com.igexin.getuiext.activity:
//            a

public class GetuiExtActivity extends Activity
    implements android.view.View.OnClickListener, android.view.View.OnTouchListener
{

    private int a;
    private int b;
    private int c;
    private int d;
    private com.igexin.getuiext.ui.f e;
    private UpgradeProgressReceiver f;
    private c g;
    private RelativeLayout h;
    private e i;
    private int j;
    private d k;
    private ProgressBar l;
    private ImageView m;
    private TextView n;
    private RelativeLayout o;
    private String p;
    private int q;

    public GetuiExtActivity()
    {
        k = d.a;
    }

    static int a(GetuiExtActivity getuiextactivity)
    {
        return getuiextactivity.j;
    }

    private e a(Bundle bundle)
    {
        e e1 = new e();
        e1.f = bundle.getString("logo_url");
        e1.j = bundle.getString("sendId");
        e1.k = bundle.getString("taskid");
        e1.a = bundle.getString("name");
        e1.g = bundle.getString("url");
        e1.r = bundle.getString("description");
        e1.o = bundle.getLong("diffSize");
        e1.i = bundle.getLong("fullSize");
        e1.h = bundle.getString("originalUrl");
        e1.b = bundle.getString("pkgname");
        e1.n = com.igexin.getuiext.data.a.f.a(bundle.getString("updateType"));
        e1.d = bundle.getInt("versionCode");
        e1.c = bundle.getString("versionName");
        e1.m = bundle.getInt("previous_version_code");
        return e1;
    }

    static d a(GetuiExtActivity getuiextactivity, d d1)
    {
        getuiextactivity.k = d1;
        return d1;
    }

    static String a(GetuiExtActivity getuiextactivity, String s)
    {
        getuiextactivity.p = s;
        return s;
    }

    static ProgressBar b(GetuiExtActivity getuiextactivity)
    {
        return getuiextactivity.l;
    }

    private c b(Bundle bundle)
    {
        Object obj = m.d;
        String s = bundle.getString("recommendType");
        if ("img".equals(s))
        {
            obj = m.b;
        } else
        if ("app".equals(s))
        {
            obj = m.a;
        } else
        if ("txt".equals(s))
        {
            obj = m.c;
        }
        if (obj != m.d)
        {
            obj = com.igexin.getuiext.ui.promotion.g.a(this, ((m) (obj)));
            ((c) (obj)).b(i.k);
            ((c) (obj)).a(i.j);
            try
            {
                ((c) (obj)).c(bundle.getString("promotion_attrs"));
            }
            // Misplaced declaration of an exception variable
            catch (Bundle bundle)
            {
                return null;
            }
            return ((c) (obj));
        } else
        {
            return null;
        }
    }

    private void b()
    {
        e = com.igexin.getuiext.ui.f.a(getApplicationContext());
        a = com.igexin.getuiext.ui.a.a(this, 10F);
        b = com.igexin.getuiext.ui.a.a(this, 3F);
        c = com.igexin.getuiext.ui.a.a(this, 2.0F);
        d = com.igexin.getuiext.ui.a.a(this, 1.0F);
    }

    static c c(GetuiExtActivity getuiextactivity)
    {
        return getuiextactivity.g;
    }

    private void c()
    {
        Object obj = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(((DisplayMetrics) (obj)));
        int i1 = ((DisplayMetrics) (obj)).widthPixels;
        obj = new android.view.WindowManager.LayoutParams(2003);
        i1 = (int)((double)i1 * 0.90000000000000002D);
        obj.width = i1;
        q = i1;
        obj.height = -2;
        getWindow().setAttributes(((android.view.WindowManager.LayoutParams) (obj)));
    }

    private void d()
    {
        View view;
label0:
        {
            if (g != null)
            {
                view = g.a(com.igexin.getuiext.ui.a.a(this, 386F), com.igexin.getuiext.ui.a.a(this, 146F));
                if (view != null)
                {
                    break label0;
                }
                Log.w("GetuiExt-UpgradeActivity", "No Promotion Applications, Hide Promotion View");
            }
            return;
        }
        android.widget.RelativeLayout.LayoutParams layoutparams = new android.widget.RelativeLayout.LayoutParams(-1, com.igexin.getuiext.ui.a.a(this, 146F));
        layoutparams.addRule(3, 1012);
        layoutparams.addRule(5, 1012);
        layoutparams.setMargins(a, b, a, b);
        h.addView(view, layoutparams);
    }

    private void e()
    {
        Intent intent = new Intent();
        int i1 = android.os.Build.VERSION.SDK_INT;
        if (i1 >= 9)
        {
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts("package", i.b, null));
        } else
        {
            String s;
            if (i1 == 8)
            {
                s = "pkg";
            } else
            {
                s = "com.android.settings.ApplicationPkgName";
            }
            intent.setAction("android.intent.action.VIEW");
            intent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            intent.putExtra(s, i.b);
        }
        startActivity(intent);
        if (i != null)
        {
            com.igexin.getuiext.service.a.a(this, i, 10);
        }
    }

    private RelativeLayout f()
    {
        RelativeLayout relativelayout = new RelativeLayout(this);
        relativelayout.setId(1012);
        Object obj = new ImageView(this);
        ((ImageView) (obj)).setScaleType(android.widget.ImageView.ScaleType.FIT_XY);
        ((ImageView) (obj)).setImageBitmap(e.c("inc_icon_close.png"));
        relativelayout.addView(((View) (obj)), new android.widget.RelativeLayout.LayoutParams(a * 3, a * 3));
        Object obj1 = (android.widget.RelativeLayout.LayoutParams)((ImageView) (obj)).getLayoutParams();
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(11);
        ((ImageView) (obj)).setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj1)));
        ((ImageView) (obj)).setId(1001);
        ((ImageView) (obj)).setOnClickListener(this);
        obj = new TextView(this);
        ((TextView) (obj)).setTextSize(2, 18F);
        ((TextView) (obj)).setTextColor(0xff000000);
        ((TextView) (obj)).setText((new StringBuilder()).append(i.a).append("\u6B63\u5728\u5347\u7EA7").toString());
        ((TextView) (obj)).setEllipsize(android.text.TextUtils.TruncateAt.END);
        ((TextView) (obj)).setSingleLine();
        relativelayout.addView(((View) (obj)), 0, new android.widget.RelativeLayout.LayoutParams(-1, -2));
        obj1 = (android.widget.RelativeLayout.LayoutParams)((TextView) (obj)).getLayoutParams();
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(10);
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(0, 1001);
        obj1.topMargin = a;
        obj1.leftMargin = b + c + c;
        ((TextView) (obj)).setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj1)));
        ((TextView) (obj)).setId(1000);
        obj = new TextView(this);
        ((TextView) (obj)).setBackgroundColor(0xff00ff00);
        obj1 = new android.widget.RelativeLayout.LayoutParams(-1, d);
        obj1.topMargin = b + b;
        relativelayout.addView(((View) (obj)), 2, ((android.view.ViewGroup.LayoutParams) (obj1)));
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(3, 1000);
        ((TextView) (obj)).setId(1002);
        obj = new RelativeLayout(this);
        ((RelativeLayout) (obj)).setPadding(b, b, b, b);
        ((RelativeLayout) (obj)).setId(1003);
        obj1 = new android.widget.RelativeLayout.LayoutParams(-1, -2);
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(3, 1002);
        obj1.topMargin = c + b;
        relativelayout.addView(((View) (obj)), ((android.view.ViewGroup.LayoutParams) (obj1)));
        obj1 = new UrlImageView(this);
        ((UrlImageView) (obj1)).a(i.f);
        ((UrlImageView) (obj1)).setId(1005);
        android.widget.RelativeLayout.LayoutParams layoutparams = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        layoutparams.addRule(15);
        ((RelativeLayout) (obj)).addView(((View) (obj1)), layoutparams);
        if (f.b.equals(i.n))
        {
            obj1 = new ImageView(this);
            ((ImageView) (obj1)).setImageBitmap(e.c("inc_province.png"));
            layoutparams = new android.widget.RelativeLayout.LayoutParams(-2, -2);
            layoutparams.addRule(8, 1005);
            layoutparams.addRule(6, 1005);
            layoutparams.addRule(5, 1005);
            layoutparams.addRule(7, 1005);
            ((RelativeLayout) (obj)).addView(((View) (obj1)), layoutparams);
        }
        obj1 = new RelativeLayout(this);
        ((RelativeLayout) (obj1)).setId(1011);
        layoutparams = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        layoutparams.addRule(11);
        layoutparams.addRule(15);
        layoutparams.rightMargin = b + c;
        ((RelativeLayout) (obj)).addView(((View) (obj1)), layoutparams);
        m = new ImageView(this);
        m.setImageBitmap(e.c("inc_pause.png"));
        m.setId(1016);
        layoutparams = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        layoutparams.addRule(14);
        ((RelativeLayout) (obj1)).addView(m, layoutparams);
        m.setOnClickListener(this);
        n = new TextView(this);
        n.setTextColor(0xff000000);
        n.setTextSize(2, 12F);
        n.setText("\u6682\u505C");
        n.setId(1017);
        layoutparams = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        layoutparams.addRule(3, 1016);
        layoutparams.setMargins(0, b + b, 0, 0);
        layoutparams.addRule(14);
        ((RelativeLayout) (obj1)).addView(n, layoutparams);
        obj1 = new RelativeLayout(this);
        ((RelativeLayout) (obj1)).setPadding(b, b, b, b);
        ((RelativeLayout) (obj1)).setId(1004);
        layoutparams = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        layoutparams.addRule(1, 1005);
        layoutparams.addRule(15);
        layoutparams.addRule(0, 1011);
        layoutparams.leftMargin = b + b;
        layoutparams.rightMargin = layoutparams.leftMargin;
        ((RelativeLayout) (obj)).addView(((View) (obj1)), layoutparams);
        obj = new TextView(this);
        ((TextView) (obj)).setTextColor(0xff000000);
        ((TextView) (obj)).setTextSize(2, 15F);
        ((TextView) (obj)).setText(i.a);
        ((TextView) (obj)).setEms(6);
        ((TextView) (obj)).setEllipsize(android.text.TextUtils.TruncateAt.END);
        ((TextView) (obj)).setSingleLine();
        ((TextView) (obj)).setId(1006);
        ((RelativeLayout) (obj1)).addView(((View) (obj)), new android.widget.RelativeLayout.LayoutParams(-2, -2));
        obj = new TextView(this);
        ((TextView) (obj)).setTextSize(2, 12F);
        ((TextView) (obj)).setTextColor(0xff000000);
        ((TextView) (obj)).setSingleLine();
        ((TextView) (obj)).setEllipsize(android.text.TextUtils.TruncateAt.END);
        ((TextView) (obj)).setText((new StringBuilder()).append("\u7248\u672C:").append(i.c).toString());
        ((TextView) (obj)).setId(1007);
        layoutparams = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        layoutparams.leftMargin = c + b;
        layoutparams.addRule(1, 1006);
        layoutparams.addRule(4, 1006);
        ((RelativeLayout) (obj1)).addView(((View) (obj)), layoutparams);
        obj = new TextView(this);
        ((TextView) (obj)).setTextSize(2, 12F);
        ((TextView) (obj)).setTextColor(0xff000000);
        ((TextView) (obj)).setId(1008);
        if (i.n.equals(f.b))
        {
            ((TextView) (obj)).getPaint().setFlags(17);
        }
        float f1 = (float)i.i / 1048576F;
        ((TextView) (obj)).setText((new StringBuilder()).append("\u5927\u5C0F:").append(String.format("%.2f", new Object[] {
            Float.valueOf(f1)
        })).append("M").toString());
        layoutparams = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        layoutparams.addRule(3, 1006);
        layoutparams.addRule(5, 1006);
        ((RelativeLayout) (obj1)).addView(((View) (obj)), layoutparams);
        obj = new TextView(this);
        ((TextView) (obj)).setTextSize(2, 12F);
        ((TextView) (obj)).setTextColor(0xff000000);
        ((TextView) (obj)).setSingleLine();
        ((TextView) (obj)).setId(1009);
        f1 = (float)i.o / 1048576F;
        ((TextView) (obj)).setText((new StringBuilder()).append("\u53EA\u9700:").append(String.format("%.2f", new Object[] {
            Float.valueOf(f1)
        })).append("M").toString());
        layoutparams = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        layoutparams.addRule(1, 1008);
        layoutparams.addRule(4, 1008);
        layoutparams.leftMargin = c + b;
        ((RelativeLayout) (obj1)).addView(((View) (obj)), layoutparams);
        if (i.n.equals(f.a))
        {
            ((TextView) (obj)).setVisibility(8);
        }
        l = new ProgressBar(this, null, 0x1010078);
        l.setId(1010);
        l.getProgressDrawable().setColorFilter(-256, android.graphics.PorterDuff.Mode.MULTIPLY);
        l.setMax(100);
        l.setProgress(0);
        obj = new android.widget.RelativeLayout.LayoutParams(-1, b);
        ((android.widget.RelativeLayout.LayoutParams) (obj)).addRule(3, 1008);
        ((android.widget.RelativeLayout.LayoutParams) (obj)).addRule(5, 1008);
        obj.topMargin = b;
        ((RelativeLayout) (obj1)).addView(l, ((android.view.ViewGroup.LayoutParams) (obj)));
        obj = new TextView(this);
        ((TextView) (obj)).setTextSize(2, 12F);
        ((TextView) (obj)).setTextColor(0xff000000);
        ((TextView) (obj)).setLines(2);
        ((TextView) (obj)).setEllipsize(android.text.TextUtils.TruncateAt.END);
        ((TextView) (obj)).setText((new StringBuilder()).append("\u66F4\u65B0\u8BF4\u660E\uFF1A").append(i.r).toString());
        ((TextView) (obj)).setId(1013);
        obj1 = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(5, 1003);
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(3, 1003);
        obj1.topMargin = c + b;
        obj1.leftMargin = ((android.widget.RelativeLayout.LayoutParams) (obj1)).topMargin;
        relativelayout.addView(((View) (obj)), ((android.view.ViewGroup.LayoutParams) (obj1)));
        o = new RelativeLayout(this);
        o.setBackgroundColor(Color.parseColor("#8a8a8a"));
        o.setId(1018);
        obj = new android.widget.RelativeLayout.LayoutParams(-1, 0);
        ((android.widget.RelativeLayout.LayoutParams) (obj)).addRule(3, 1013);
        obj.topMargin = a;
        relativelayout.addView(o, ((android.view.ViewGroup.LayoutParams) (obj)));
        obj = new TextView(this);
        ((TextView) (obj)).setTextColor(-1);
        ((TextView) (obj)).setText("\u8FD8\u53EF\u4EE5\u8FDB\u884C\u4E0B\u5217\u64CD\u4F5C\uFF1A");
        ((TextView) (obj)).setTextSize(2, 12F);
        ((TextView) (obj)).setId(1022);
        obj1 = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        obj1.topMargin = b + b;
        obj1.bottomMargin = b + b;
        obj1.leftMargin = b + b;
        o.addView(((View) (obj)), ((android.view.ViewGroup.LayoutParams) (obj1)));
        obj = new Button(this);
        ((Button) (obj)).setBackgroundDrawable(e.b("inc_btn_normal.png"));
        int i1 = q / 9;
        ((Button) (obj)).setText("\u7BA1\u7406\u6B64\u5E94\u7528");
        ((Button) (obj)).setSingleLine();
        ((Button) (obj)).setTextSize(2, 12F);
        ((Button) (obj)).setId(1019);
        ((Button) (obj)).setOnClickListener(this);
        ((Button) (obj)).setOnTouchListener(this);
        obj1 = new android.widget.RelativeLayout.LayoutParams(i1 * 3, i1);
        obj1.leftMargin = i1;
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(3, 1022);
        o.addView(((View) (obj)), ((android.view.ViewGroup.LayoutParams) (obj1)));
        obj = new Button(this);
        ((Button) (obj)).setBackgroundDrawable(e.b("inc_btn_emphasize_normal.png"));
        ((Button) (obj)).setText("WiFi\u4E0B\u4E0B\u8F7D");
        ((Button) (obj)).setSingleLine();
        ((Button) (obj)).setTextSize(2, 12F);
        ((Button) (obj)).setId(1020);
        ((Button) (obj)).setOnClickListener(this);
        ((Button) (obj)).setOnTouchListener(this);
        obj1 = new android.widget.RelativeLayout.LayoutParams(i1 * 3, i1);
        obj1.rightMargin = i1;
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(3, 1022);
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(11);
        o.addView(((View) (obj)), ((android.view.ViewGroup.LayoutParams) (obj1)));
        obj = new View(this);
        obj1 = new android.widget.RelativeLayout.LayoutParams(i1, d);
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(1, 1019);
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(0, 1020);
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(3, 1022);
        o.addView(((View) (obj)), ((android.view.ViewGroup.LayoutParams) (obj1)));
        obj = new Button(this);
        ((Button) (obj)).setBackgroundColor(0);
        ((Button) (obj)).getPaint().setFlags(9);
        ((Button) (obj)).setText("\u4E0B\u6B21\u518D\u8BF4");
        ((Button) (obj)).setTextColor(-1);
        ((Button) (obj)).setId(1021);
        ((Button) (obj)).setTextSize(2, 12F);
        obj1 = new android.widget.RelativeLayout.LayoutParams(-2, -2);
        obj1.rightMargin = i1;
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(3, 1020);
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(11);
        o.addView(((View) (obj)), ((android.view.ViewGroup.LayoutParams) (obj1)));
        ((Button) (obj)).setOnClickListener(this);
        obj = new RelativeLayout(this);
        ((RelativeLayout) (obj)).setBackgroundDrawable(e.a("inc_more.9.png"));
        ((RelativeLayout) (obj)).setId(1014);
        obj1 = new android.widget.RelativeLayout.LayoutParams(-1, -2);
        ((android.widget.RelativeLayout.LayoutParams) (obj1)).addRule(3, 1018);
        relativelayout.addView(((View) (obj)), ((android.view.ViewGroup.LayoutParams) (obj1)));
        obj1 = new ImageView(this);
        ((ImageView) (obj1)).setImageBitmap(e.c("inc_click_down.png"));
        ((ImageView) (obj1)).setId(1015);
        ((ImageView) (obj1)).setOnClickListener(this);
        ((ImageView) (obj1)).setScaleType(android.widget.ImageView.ScaleType.CENTER_INSIDE);
        layoutparams = new android.widget.RelativeLayout.LayoutParams(32, 32);
        layoutparams.addRule(14);
        ((RelativeLayout) (obj)).addView(((View) (obj1)), layoutparams);
        return relativelayout;
    }

    public void a()
    {
        switch (com.igexin.getuiext.activity.a.a[k.ordinal()])
        {
        default:
            return;

        case 1: // '\001'
            n.setText("\u66F4\u65B0");
            m.setImageBitmap(e.c("inc_update.png"));
            return;

        case 2: // '\002'
            n.setText("\u6682\u505C");
            m.setImageBitmap(e.c("inc_pause.png"));
            return;

        case 3: // '\003'
            n.setText("\u7EE7\u7EED");
            m.setImageBitmap(e.c("inc_update.png"));
            return;

        case 4: // '\004'
            n.setText("\u5B89\u88C5");
            m.setImageBitmap(e.c("inc_setup.png"));
            return;

        case 5: // '\005'
            n.setText("\u6253\u5F00");
            m.setImageBitmap(e.c("inc_open.png"));
            return;

        case 6: // '\006'
            n.setText("\u9519\u8BEF");
            break;
        }
        m.setImageBitmap(e.c("inc_icon_close.png"));
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 6: default 64
    //                   1001: 65
    //                   1015: 276
    //                   1016: 70
    //                   1019: 350
    //                   1020: 355
    //                   1021: 395;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7
_L1:
        return;
_L2:
        finish();
        return;
_L4:
        com.igexin.getuiext.activity.a.a[k.ordinal()];
        JVM INSTR tableswitch 1 6: default 120
    //                   1 125
    //                   2 157
    //                   3 179
    //                   4 198
    //                   5 244
    //                   6 262;
           goto _L8 _L9 _L10 _L11 _L12 _L13 _L14
_L8:
        a();
        return;
_L9:
        k = d.b;
        j = com.igexin.getuiext.ui.b.a(this, i, false);
        com.igexin.getuiext.service.a.a(this, i, 3);
          goto _L8
_L10:
        com.igexin.getuiext.ui.b.a(this, j, i);
        k = d.c;
          goto _L8
_L11:
        com.igexin.getuiext.ui.b.a(this, j, i);
        k = d.b;
_L12:
        ((NotificationManager)getSystemService("notification")).cancel(j << 11);
        if (p != null)
        {
            com.igexin.getuiext.ui.b.a(this, p, j, i);
        }
          goto _L8
_L13:
        com.igexin.getuiext.ui.b.a(this, i.b);
        finish();
          goto _L8
_L14:
        Toast.makeText(this, "\u4E0B\u8F7D\u5931\u8D25", 0).show();
          goto _L8
_L3:
        android.widget.RelativeLayout.LayoutParams layoutparams = (android.widget.RelativeLayout.LayoutParams)o.getLayoutParams();
        view = (ImageView)view;
        if (layoutparams.height == 0)
        {
            layoutparams.height = -2;
            view.setImageBitmap(e.c("inc_click_up.png"));
        } else
        {
            layoutparams.height = 0;
            view.setImageBitmap(e.c("inc_click_down.png"));
        }
        o.setLayoutParams(layoutparams);
        return;
_L5:
        e();
        return;
_L6:
        if (j == -1 && !com.igexin.getuiext.util.h.b(this))
        {
            com.igexin.getuiext.ui.b.a(this, i, true);
        }
        com.igexin.getuiext.service.a.a(this, i, 11);
        finish();
        return;
_L7:
        com.igexin.getuiext.service.a.a(this, i, 12);
        finish();
        return;
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        b();
        bundle = getIntent().getExtras();
        i = a(bundle);
        g = b(bundle);
        c();
        com.igexin.getuiext.service.a.a(this, i, 2);
        j = -1;
        if (com.igexin.getuiext.util.h.b(this))
        {
            k = d.b;
            j = com.igexin.getuiext.ui.b.a(this, i, true);
            com.igexin.getuiext.service.a.a(this, i, 3);
        }
        h = new RelativeLayout(this);
        bundle = e.a("inc_bg.9.png");
        if (bundle != null)
        {
            h.setBackgroundDrawable(bundle);
        }
        setContentView(h, new android.widget.RelativeLayout.LayoutParams(-1, -2));
        bundle = f();
        h.addView(bundle, -1, -2);
        if (g != null)
        {
            d();
        }
    }

    protected void onDestroy()
    {
        com.igexin.getuiext.service.a.a(this, i, 7);
        super.onDestroy();
    }

    protected void onPause()
    {
        super.onPause();
        unregisterReceiver(f);
        f = null;
    }

    protected void onResume()
    {
        super.onResume();
        if (f == null)
        {
            f = new UpgradeProgressReceiver();
        }
        IntentFilter intentfilter = new IntentFilter("com.igexin.getuiext.ui.upgrade_progress");
        registerReceiver(f, intentfilter);
        a();
    }

    public boolean onTouch(View view, MotionEvent motionevent)
    {
        int i1 = motionevent.getAction();
        view.getId();
        JVM INSTR tableswitch 1019 1020: default 32
    //                   1019 34
    //                   1020 87;
           goto _L1 _L2 _L3
_L1:
        return false;
_L2:
        if (i1 == 0)
        {
            view.setBackgroundDrawable(e.b("inc_btn_pressed.png"));
        } else
        if (i1 == 3 || i1 == 1 || i1 == 4)
        {
            view.setBackgroundDrawable(e.b("inc_btn_normal.png"));
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (i1 == 0)
        {
            view.setBackgroundDrawable(e.b("inc_btn_emphasize_pressed.png"));
        } else
        if (i1 == 3 || i1 == 1 || i1 == 4)
        {
            view.setBackgroundDrawable(e.b("inc_btn_emphasize_normal.png"));
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    private class UpgradeProgressReceiver extends BroadcastReceiver
    {

        final GetuiExtActivity a;

        private void a(int i1, int j1, String s)
        {
            if (i1 != com.igexin.getuiext.activity.GetuiExtActivity.a(a)) goto _L2; else goto _L1
_L1:
            if (j1 != -1) goto _L4; else goto _L3
_L3:
            com.igexin.getuiext.activity.GetuiExtActivity.a(a, d.f);
            a.a();
_L6:
            return;
_L4:
            com.igexin.getuiext.activity.GetuiExtActivity.b(a).setProgress(j1);
            if (j1 >= 100)
            {
                com.igexin.getuiext.activity.GetuiExtActivity.a(a, d.d);
                com.igexin.getuiext.activity.GetuiExtActivity.a(a, s);
                a.a();
                return;
            }
            continue; /* Loop/switch isn't completed */
_L2:
            if (com.igexin.getuiext.activity.GetuiExtActivity.c(a) instanceof com.igexin.getuiext.ui.promotion.d)
            {
                ((com.igexin.getuiext.ui.promotion.d)com.igexin.getuiext.activity.GetuiExtActivity.c(a)).a(i1, j1, s);
                return;
            }
            if (true) goto _L6; else goto _L5
_L5:
        }

        public void onReceive(Context context, Intent intent)
        {
            if ("com.igexin.getuiext.ui.upgrade_progress".equals(intent.getAction()))
            {
                int i1 = intent.getIntExtra("progress", 0);
                context = intent.getStringExtra("file_path");
                a(intent.getIntExtra("download_id", -1), i1, context);
            }
        }

        public UpgradeProgressReceiver()
        {
            a = GetuiExtActivity.this;
            super();
        }
    }

}
