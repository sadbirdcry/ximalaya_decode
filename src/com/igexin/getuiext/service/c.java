// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.service;

import android.graphics.Bitmap;
import com.igexin.getuiext.data.a.d;
import java.util.HashMap;
import java.util.WeakHashMap;

public final class c
{

    private static c c = new c();
    private HashMap a;
    private WeakHashMap b;

    private c()
    {
        a = new HashMap();
        b = new WeakHashMap();
    }

    public static c a()
    {
        return c;
    }

    public Bitmap a(String s)
    {
        return (Bitmap)b.get(s);
    }

    public void a(String s, Bitmap bitmap)
    {
        b.put(s, bitmap);
    }

    public void a(String s, d d1)
    {
        a.put(s, d1);
    }

    public d b(String s)
    {
        return (d)a.get(s);
    }

    public void c(String s)
    {
        a.remove(s);
    }

}
