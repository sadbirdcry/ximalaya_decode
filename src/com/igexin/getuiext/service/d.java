// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.os.Environment;
import android.text.TextUtils;
import android.util.SparseArray;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.igexin.download.DownloadInfo;
import com.igexin.download.IDownloadCallback;
import com.igexin.download.SdkDownLoader;
import com.igexin.getuiext.activity.GetuiExtActivity;
import com.igexin.getuiext.b.a;
import com.igexin.getuiext.b.c;
import com.igexin.getuiext.data.Consts;
import com.igexin.getuiext.data.a.e;
import com.igexin.getuiext.ui.f;
import com.igexin.getuiext.util.h;
import java.util.Random;
import java.util.Timer;

// Referenced classes of package com.igexin.getuiext.service:
//            c, f, a, e, 
//            g

public class d
    implements IDownloadCallback
{

    private Context a;
    private String b;
    private NotificationManager c;
    private SparseArray d;
    private SparseArray e;
    private SparseArray f;
    private SparseArray g;
    private Timer h;
    private boolean i;
    private f j;
    private com.igexin.getuiext.service.c k;

    public d(Context context, String s)
    {
        d = new SparseArray();
        e = new SparseArray();
        f = new SparseArray();
        g = new SparseArray();
        h = null;
        i = true;
        a = context;
        b = s;
        c = (NotificationManager)context.getSystemService("notification");
        j = com.igexin.getuiext.ui.f.a(context);
        k = com.igexin.getuiext.service.c.a();
    }

    private int a(int l)
    {
        return l << 11;
    }

    private Notification a(DownloadInfo downloadinfo, Notification notification, com.igexin.getuiext.data.a.d d1)
    {
        if (!downloadinfo.mNotice)
        {
            downloadinfo.mNotice = true;
            Intent intent = new Intent("com.igexin.increment");
            intent.putExtra("action", "install");
            intent.putExtra("filepath", downloadinfo.mFileName);
            intent.putExtra("pkgname", d1.b);
            intent.putExtra("verifyCode", Consts.verifyCode);
            d1 = new Random();
            d1.setSeed(System.currentTimeMillis());
            notification.contentIntent = PendingIntent.getBroadcast(a, d1.nextInt(1000), intent, 0x40000000);
            d1 = new Intent("com.igexin.getuiext.ui.upgrade_progress");
            d1.putExtra("download_id", downloadinfo.mId);
            d1.putExtra("file_path", downloadinfo.mFileName);
            d1.putExtra("progress", 100);
            a.sendBroadcast(d1);
        }
        return notification;
    }

    static SparseArray a(d d1)
    {
        return d1.e;
    }

    static Timer a(d d1, Timer timer)
    {
        d1.h = timer;
        return timer;
    }

    private void a(DownloadInfo downloadinfo)
    {
        Notification notification;
        Object obj;
        com.igexin.getuiext.data.a.d d1;
        notification = (Notification)d.get(downloadinfo.mId);
        if (notification == null || notification.contentView == null)
        {
            notification = new Notification();
            int l = j.a("notification_inc", "layout");
            notification.contentView = new RemoteViews(a.getPackageName(), l);
            Intent intent = new Intent(a, com/igexin/getuiext/activity/GetuiExtActivity);
            notification.contentIntent = PendingIntent.getActivity(a, 0, intent, 0x8000000);
            d.put(downloadinfo.mId, notification);
        }
        d1 = (com.igexin.getuiext.data.a.d)g.get(downloadinfo.mId);
        obj = d1;
        if (d1 != null) goto _L2; else goto _L1
_L1:
        obj = (com.igexin.getuiext.data.a.d)com.igexin.getuiext.b.c.d().c().a(downloadinfo.mId);
        if (obj != null) goto _L4; else goto _L3
_L3:
        return;
_L4:
        g.put(downloadinfo.mId, obj);
_L2:
        int i1;
        int j1;
        int k1;
        int k2;
        int l2;
        int i3;
        int j3;
        int k3;
        int l3;
        i1 = j.a("notification_right", "id");
        k1 = j.a("download_layout", "id");
        int l1 = j.a("notification_icon", "id");
        int i2 = j.a("download_app_name", "id");
        int j2 = j.a("download_app_version", "id");
        j1 = j.a("downlaod_progress_horizontal", "id");
        k2 = j.a("setup_layout", "id");
        l2 = j.a("setup_icon", "id");
        i3 = j.a("setup_app_name", "id");
        j3 = j.a("setup_app_version", "id");
        k3 = j.a("setup_message", "id");
        l3 = j.a("setup_text", "id");
        Bitmap bitmap = k.a(((com.igexin.getuiext.data.a.d) (obj)).b);
        if (bitmap == null)
        {
            a(((com.igexin.getuiext.data.a.d) (obj)).f, ((com.igexin.getuiext.data.a.d) (obj)).b);
            notification.contentView.setImageViewResource(l1, a.getApplicationInfo().icon);
        } else
        if (bitmap.getHeight() < 100 || bitmap.getWidth() < 100)
        {
            notification.contentView.setImageViewBitmap(l1, bitmap);
        }
        notification.contentView.setTextViewText(i2, ((com.igexin.getuiext.data.a.d) (obj)).a);
        notification.contentView.setTextViewText(j2, ((com.igexin.getuiext.data.a.d) (obj)).c);
        downloadinfo.mStatus;
        JVM INSTR lookupswitch 3: default 436
    //                   192: 686
    //                   193: 1345
    //                   200: 1101;
           goto _L5 _L6 _L7 _L8
_L7:
        continue; /* Loop/switch isn't completed */
_L5:
        if (downloadinfo.mStatus >= 400 && downloadinfo.mStatus < 600)
        {
            if (notification.contentView != null)
            {
                notification.flags = 16;
                notification.icon = 0x108008a;
                notification.contentView = null;
                notification.setLatestEventInfo(a, ((com.igexin.getuiext.data.a.d) (obj)).a, "\u4E0B\u8F7D\u5931\u8D25\uFF01", PendingIntent.getActivity(a, com.igexin.getuiext.util.h.c(), new Intent(), 0x40000000));
                e.remove(downloadinfo.mId);
                f.remove(downloadinfo.mId);
                g.remove(downloadinfo.mId);
                com.igexin.getuiext.b.c.d().c().b(downloadinfo.mId);
                SdkDownLoader.getInstantiate(a).deleteTask(downloadinfo.mId);
                obj = new Intent("com.igexin.getuiext.ui.upgrade_progress");
                ((Intent) (obj)).putExtra("download_id", downloadinfo.mId);
                ((Intent) (obj)).putExtra("file_path", "");
                ((Intent) (obj)).putExtra("progress", -1);
                a.sendBroadcast(((Intent) (obj)));
            }
            d.put(downloadinfo.mId, notification);
            c.notify(a(downloadinfo.mId), notification);
            return;
        }
          goto _L3
_L6:
        notification.contentView.setViewVisibility(i1, 8);
        notification.contentView.setViewVisibility(k1, 0);
        if (downloadinfo.mNotify)
        {
            downloadinfo.mNotify = false;
            i1 = ((AudioManager)a.getSystemService("audio")).getRingerMode();
            Object obj1;
            if (i1 == 1)
            {
                notification.sound = null;
                notification.defaults = notification.defaults | 2;
            } else
            if (i1 == 2)
            {
                notification.defaults = notification.defaults | 2;
                notification.defaults = notification.defaults | 1;
            }
        } else
        {
            notification.defaults = 0;
            notification.sound = null;
            notification.vibrate = null;
        }
        notification.icon = 0x1080081;
        notification.tickerText = "\u6B63\u5728\u4E0B\u8F7D";
        notification.flags = notification.flags | 0x20;
        obj1 = new Intent();
        notification.contentIntent = PendingIntent.getBroadcast(a, 0, ((Intent) (obj1)), 0x8000000);
        if (!(obj instanceof e)) goto _L10; else goto _L9
_L9:
        obj1 = ((e)obj).n;
        com.igexin.getuiext.service.f.a[((com.igexin.getuiext.data.a.f) (obj1)).ordinal()];
        JVM INSTR tableswitch 1 2: default 856
    //                   1 999
    //                   2 1045;
           goto _L11 _L12 _L13
_L11:
        i1 = 0;
_L15:
        notification.contentView.setProgressBar(j1, 100, i1, false);
        obj = new Intent("com.igexin.getuiext.ui.upgrade_progress");
        ((Intent) (obj)).putExtra("download_id", downloadinfo.mId);
        ((Intent) (obj)).putExtra("file_path", "");
        ((Intent) (obj)).putExtra("progress", i1);
        a.sendBroadcast(((Intent) (obj)));
        d.put(downloadinfo.mId, notification);
        try
        {
            c.notify(a(downloadinfo.mId), notification);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (DownloadInfo downloadinfo)
        {
            return;
        }
_L12:
        long l4;
        long l5;
        l4 = ((com.igexin.getuiext.data.a.d) (obj)).i;
        l5 = ((e)obj).o;
        if (l4 == 0L) goto _L11; else goto _L14
_L14:
        i1 = (int)((((l4 - l5) + (long)downloadinfo.mCurrentBytes) * 100L) / l4);
          goto _L15
_L13:
        if (downloadinfo.mTotalBytes == 0) goto _L11; else goto _L16
_L16:
        i1 = (int)(((long)downloadinfo.mCurrentBytes * 100L) / (long)downloadinfo.mTotalBytes);
          goto _L15
_L10:
        if (downloadinfo.mTotalBytes == 0) goto _L11; else goto _L17
_L17:
        i1 = (int)(((long)downloadinfo.mCurrentBytes * 100L) / (long)downloadinfo.mTotalBytes);
          goto _L15
_L8:
        notification = a(downloadinfo, notification, ((com.igexin.getuiext.data.a.d) (obj)));
        notification.flags = 16;
        notification.icon = 0x1080082;
        notification.tickerText = "\u4E0B\u8F7D\u6210\u529F";
        notification.contentView.setViewVisibility(k1, 8);
        notification.contentView.setViewVisibility(k2, 0);
        notification.contentView.setImageViewBitmap(l2, j.c("inc_setup.png"));
        notification.contentView.setTextViewText(i3, ((com.igexin.getuiext.data.a.d) (obj)).a);
        notification.contentView.setTextViewText(j3, ((com.igexin.getuiext.data.a.d) (obj)).c);
        notification.contentView.setTextViewText(k3, "\u4E0B\u8F7D\u5B8C\u6210");
        notification.contentView.setTextViewText(l3, "\u5B89\u88C5");
        SdkDownLoader.getInstantiate(a).deleteTask(downloadinfo.mId);
        f.remove(downloadinfo.mId);
        com.igexin.getuiext.b.c.d().c().b(downloadinfo.mId);
        obj = (com.igexin.getuiext.data.a.a)g.get(downloadinfo.mId);
        g.remove(downloadinfo.mId);
        if (obj != null && ((com.igexin.getuiext.data.a.a) (obj)).b != null && !((com.igexin.getuiext.data.a.a) (obj)).b.equals(""))
        {
            a(((com.igexin.getuiext.data.a.a) (obj)).b);
        }
        d.put(downloadinfo.mId, notification);
        c.notify(a(downloadinfo.mId), notification);
        return;
        if (downloadinfo.mControl != 1) goto _L3; else goto _L18
_L18:
        c.cancel(a(downloadinfo.mId));
        d.remove(downloadinfo.mId);
        f.remove(downloadinfo.mId);
        e.remove(downloadinfo.mId);
        downloadinfo.mNotify = true;
        return;
    }

    private void a(String s)
    {
        s = com.igexin.getuiext.service.c.a().b(s);
        if (s != null)
        {
            if (s instanceof com.igexin.getuiext.ui.promotion.f)
            {
                com.igexin.getuiext.service.a.a(a, s, 3);
            } else
            if (s instanceof e)
            {
                com.igexin.getuiext.service.a.a(a, s, 4);
                return;
            }
        }
    }

    private void a(String s, String s1)
    {
        if (TextUtils.isEmpty(s) || TextUtils.isEmpty(s1))
        {
            return;
        } else
        {
            (new com.igexin.getuiext.service.e(this)).execute(new String[] {
                s, s1
            });
            return;
        }
    }

    static Timer b(d d1)
    {
        return d1.h;
    }

    static SparseArray c(d d1)
    {
        return d1.f;
    }

    public String getName()
    {
        return b;
    }

    public void update(DownloadInfo downloadinfo)
    {
        boolean flag = Environment.getExternalStorageState().equals("mounted");
        if (!flag && !i)
        {
            return;
        }
        if (!flag)
        {
            Toast.makeText(a, "\u5F53\u524DSD\u5361\u4E0D\u53EF\u7528\uFF0C\u8BF7\u68C0\u67E5\u8BBE\u7F6E\u3002", 0).show();
            i = false;
        } else
        {
            i = true;
        }
        downloadinfo.refreshSpeed();
        if (f.get(downloadinfo.mId) == null)
        {
            f.put(downloadinfo.mId, downloadinfo);
        }
        if (downloadinfo.mStatus != 200) goto _L2; else goto _L1
_L1:
        e.remove(downloadinfo.mId);
_L4:
        if (h == null)
        {
            h = new Timer();
            h.schedule(new g(this, null), 100L, 5000L);
        }
        a(downloadinfo);
        return;
_L2:
        if (e.get(downloadinfo.mId) == null)
        {
            e.put(downloadinfo.mId, Long.valueOf(System.currentTimeMillis()));
        }
        if (downloadinfo.mDownSpeed != 0L)
        {
            e.put(downloadinfo.mId, Long.valueOf(System.currentTimeMillis()));
        }
        if (true) goto _L4; else goto _L3
_L3:
    }
}
