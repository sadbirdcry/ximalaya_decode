// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.igexin.getuiext.data.Consts;
import com.igexin.getuiext.util.h;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.igexin.getuiext.service:
//            GetuiExtService

public class PayloadReceiver extends BroadcastReceiver
{

    public PayloadReceiver()
    {
    }

    public void onReceive(Context context, Intent intent)
    {
        Object obj;
        String s1;
        obj = intent.getAction();
        s1 = h.a(context);
        if (s1 != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        intent = intent.getExtras();
        if (!(new StringBuilder()).append("com.igexin.sdk.action.").append(s1).toString().equals(obj))
        {
            continue; /* Loop/switch isn't completed */
        }
        if (intent.getInt("action") != 10002) goto _L1; else goto _L3
_L3:
        intent = intent.getString("clientid");
        if (intent == null) goto _L1; else goto _L4
_L4:
        obj = new Intent(context, com/igexin/getuiext/service/GetuiExtService);
        ((Intent) (obj)).putExtra("action", "bindApp");
        ((Intent) (obj)).putExtra("cid", intent);
        ((Intent) (obj)).putExtra("what", 11001);
        context.startService(((Intent) (obj)));
        return;
        if (intent.getInt("action") != 10001) goto _L1; else goto _L5
_L5:
        obj = intent.getString("packagename");
        if (!context.getPackageName().equals(obj) || !intent.getString("appid").equals(Consts.INC_APPID)) goto _L1; else goto _L6
_L6:
        long l = (new h()).b();
        if (System.currentTimeMillis() - l < 0x5265c00L) goto _L1; else goto _L7
_L7:
        byte abyte0[];
        abyte0 = intent.getByteArray("payload");
        intent = intent.getString("taskid");
        if (abyte0 == null || "".equals(intent)) goto _L1; else goto _L8
_L8:
        String s = new String(abyte0);
        String s2;
        String s3;
        String s4;
        String s5;
        Object obj1;
        boolean flag;
        try
        {
            obj1 = new JSONObject(s);
            s = ((JSONObject) (obj1)).getString("action");
            s2 = ((JSONObject) (obj1)).getString("pkgname");
            s3 = ((JSONObject) (obj1)).getString("versionCode");
            s4 = ((JSONObject) (obj1)).getString("sendId");
            s5 = ((JSONObject) (obj1)).getString("context");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return;
        }
        flag = false;
        if (((JSONObject) (obj1)).has("cleanInstall"))
        {
            flag = ((JSONObject) (obj1)).getBoolean("cleanInstall");
        }
        if (s == null || s2 == null || s3 == null) goto _L1; else goto _L9
_L9:
        obj1 = new Intent(context, com/igexin/getuiext/service/GetuiExtService);
        ((Intent) (obj1)).putExtra("action", s);
        ((Intent) (obj1)).putExtra("pkgName", s2);
        ((Intent) (obj1)).putExtra("versionCode", s3);
        ((Intent) (obj1)).putExtra("taskid", intent);
        ((Intent) (obj1)).putExtra("sendId", s4);
        ((Intent) (obj1)).putExtra("context", s5);
        ((Intent) (obj1)).putExtra("what", 11001);
        ((Intent) (obj1)).putExtra("cleanInstall", flag);
        context.startService(((Intent) (obj1)));
        return;
    }
}
