// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.service;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.igexin.getuiext.data.Consts;
import com.igexin.getuiext.data.a.b;
import com.igexin.getuiext.util.c;
import com.igexin.getuiext.util.d;
import com.igexin.getuiext.util.g;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.igexin.getuiext.service:
//            a, GetuiExtService

public class i extends Handler
{

    final GetuiExtService a;

    public i(GetuiExtService getuiextservice, Looper looper)
    {
        a = getuiextservice;
        super(looper);
    }

    private boolean a(String s)
    {
        ArrayList arraylist = com.igexin.getuiext.service.a.a(s);
        if (arraylist != null && arraylist.size() > 0)
        {
            StringBuffer stringbuffer = new StringBuffer();
            int k = arraylist.size();
            for (int j = 0; j < k; j++)
            {
                if (j > 0)
                {
                    stringbuffer.append("\n");
                }
                stringbuffer.append(((b)arraylist.get(j)).b);
            }

            try
            {
                JSONObject jsonobject = new JSONObject();
                jsonobject.put("action", "upload_BI");
                jsonobject.put("BIType", s);
                jsonobject.put("cid", Consts.CID);
                jsonobject.put("BIData", new String(d.a(stringbuffer.toString().getBytes(), 0), "UTF-8"));
                s = c.a("http://sdk.open.phone.igexin.com/api.php?format=json&t=1", g.a(jsonobject.toString().getBytes()), Consts.DEFAULT_RETRY_TIMES);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s = null;
            }
            if (s != null)
            {
                try
                {
                    s = (new JSONObject(s)).getString("result");
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s = null;
                }
                if (s != null && s.equals("ok"))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void a(String s, int j)
    {
        com.igexin.getuiext.service.a.a(s, j);
        if (a("2"))
        {
            com.igexin.getuiext.service.a.b("2");
        }
        if (a("3"))
        {
            com.igexin.getuiext.service.a.b("3");
        }
        if (a("13"))
        {
            com.igexin.getuiext.service.a.b("13");
        }
    }

    public void handleMessage(Message message)
    {
        if (message.what == 11002)
        {
            message = (Intent)message.obj;
            if (message != null)
            {
                a(message.getStringExtra("BIData"), Integer.valueOf(message.getStringExtra("BIType")).intValue());
            }
        }
    }
}
