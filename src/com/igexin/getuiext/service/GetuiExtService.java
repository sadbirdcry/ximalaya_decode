// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import com.igexin.download.SdkDownLoader;
import com.igexin.getuiext.b.c;
import com.igexin.getuiext.data.Consts;
import com.igexin.getuiext.util.h;
import java.util.Random;

// Referenced classes of package com.igexin.getuiext.service:
//            h, i, d, j, 
//            GetuiExtReceiver

public class GetuiExtService extends Service
{

    private GetuiExtReceiver a;
    private j b;
    private Context c;
    private volatile Looper d;
    private volatile com.igexin.getuiext.service.h e;
    private volatile Looper f;
    private volatile i g;

    public GetuiExtService()
    {
    }

    static Context a(GetuiExtService getuiextservice)
    {
        return getuiextservice.c;
    }

    private void a()
    {
        HandlerThread handlerthread = new HandlerThread("GetuiExt-GetuiExtService");
        handlerthread.start();
        d = handlerthread.getLooper();
        e = new com.igexin.getuiext.service.h(this, d);
        handlerthread = new HandlerThread("BI");
        handlerthread.start();
        f = handlerthread.getLooper();
        g = new i(this, f);
    }

    private void b()
    {
        SdkDownLoader sdkdownloader = SdkDownLoader.getInstantiate(this);
        d d2 = (d)sdkdownloader.getCallback("GETUI_INC");
        d d1 = d2;
        if (d2 == null)
        {
            d1 = new d(this, "GETUI_INC");
        }
        sdkdownloader.registerDownloadCallback(d1);
    }

    private void c()
    {
        b = new j(this);
        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentfilter.addDataScheme("package");
        registerReceiver(b, intentfilter);
    }

    private void d()
    {
        a = new GetuiExtReceiver();
        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction("com.igexin.increment");
        intentfilter.addAction("com.igexin.download.action.notify.click");
        intentfilter.addAction("android.intent.action.USER_PRESENT");
        registerReceiver(a, intentfilter);
    }

    public IBinder onBind(Intent intent)
    {
        return null;
    }

    public void onCreate()
    {
        super.onCreate();
        c = this;
        Object obj = new Random();
        ((Random) (obj)).setSeed(System.currentTimeMillis());
        Consts.verifyCode = ((Random) (obj)).nextInt();
        com.igexin.getuiext.b.c.d().a(c);
        obj = h.a(c);
        if (obj != null)
        {
            Consts.APPID = ((String) (obj));
        }
        a();
        try
        {
            d();
            c();
            b();
            return;
        }
        catch (Exception exception)
        {
            stopSelf();
        }
    }

    public void onDestroy()
    {
        if (a != null)
        {
            unregisterReceiver(a);
        }
        if (b != null)
        {
            unregisterReceiver(b);
        }
        if (d != null)
        {
            d.quit();
        }
        if (f != null)
        {
            f.quit();
        }
        super.onDestroy();
    }

    public void onStart(Intent intent, int k)
    {
label0:
        {
            if (intent != null)
            {
                if (intent.getIntExtra("what", -1) != 11002)
                {
                    break label0;
                }
                Message message = g.obtainMessage(11002);
                message.arg1 = k;
                message.obj = intent;
                g.sendMessage(message);
            }
            return;
        }
        Message message1 = e.obtainMessage();
        message1.arg1 = k;
        message1.obj = intent;
        e.sendMessage(message1);
    }
}
