// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import java.io.File;
import java.util.List;

// Referenced classes of package com.igexin.getuiext.util:
//            BsPatchUtil

public class e
{

    public static final String a = (new StringBuilder()).append(Environment.getExternalStorageDirectory().getAbsolutePath()).append("/libs/tmp/").toString();

    public static String a(Context context, String s)
    {
        if (s == null)
        {
            return null;
        }
        context = context.getPackageManager().getInstalledApplications(0);
        int j = context.size();
        for (int i = 0; i < j; i++)
        {
            ApplicationInfo applicationinfo = (ApplicationInfo)context.get(i);
            if (s.equals(applicationinfo.packageName))
            {
                return applicationinfo.sourceDir;
            }
        }

        return null;
    }

    public static void a(Context context, String s, String s1, String s2)
    {
        if ((new File(s)).exists())
        {
            s2 = a(context, s2);
            if (s2 != null && (new BsPatchUtil()).a(s2, s1, s) == 0)
            {
                b(context, s1);
            }
        }
    }

    public static void b(Context context, String s)
    {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(0x10000000);
        intent.setDataAndType(Uri.fromFile(new File(s)), "application/vnd.android.package-archive");
        context.startActivity(intent);
    }

}
