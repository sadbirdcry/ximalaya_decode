// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Locale;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

// Referenced classes of package com.igexin.getuiext.util:
//            b

public class c
{

    public static InputStream a(String s, String s1, HashMap hashmap)
    {
        String s2 = s;
        if (s == null)
        {
            s2 = "POST";
        }
        if ("POST".equals(s2.toUpperCase(Locale.US)))
        {
            return b(s1, hashmap);
        }
        if ("GET".equalsIgnoreCase(s2))
        {
            return a(s1, hashmap);
        } else
        {
            throw new MethodNotSupportedException("Method you passed in httpMethod current can't be supported. Please contact Devin");
        }
    }

    private static InputStream a(String s, HashMap hashmap)
    {
        DefaultHttpClient defaulthttpclient;
        defaulthttpclient = new DefaultHttpClient(a());
        String s1 = s;
        if (hashmap != null)
        {
            s1 = (new StringBuilder()).append(s).append("?").append(com.igexin.getuiext.util.b.a(hashmap)).toString();
        }
        s = new HttpGet(s1);
        s = defaulthttpclient.execute(s);
        if (s.getStatusLine().getStatusCode() != 200)
        {
            break MISSING_BLOCK_LABEL_93;
        }
        s = s.getEntity().getContent();
        return s;
        s;
        return null;
    }

    public static String a(String s, int i)
    {
        return a(s, ((String) (null)), i);
    }

    public static String a(String s, String s1, int i)
    {
        if (s != null) goto _L2; else goto _L1
_L1:
        return null;
_L2:
        HttpPost httppost = new HttpPost(s);
        DefaultHttpClient defaulthttpclient;
        if (s1 != null)
        {
            try
            {
                s = s1.getBytes("UTF-8");
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s = null;
            }
        } else
        {
            s = null;
        }
        if (s != null)
        {
            httppost.setEntity(new InputStreamEntity(new ByteArrayInputStream(s), s.length));
        }
        defaulthttpclient = new DefaultHttpClient(a());
        s = null;
_L5:
        if (i <= 0) goto _L1; else goto _L3
_L3:
        s1 = defaulthttpclient.execute(httppost);
        s = s1;
_L6:
        if (s == null) goto _L1; else goto _L4
_L4:
        if (s.getStatusLine().getStatusCode() == 200)
        {
            try
            {
                s = EntityUtils.toString(s.getEntity(), "UTF-8");
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return null;
            }
            return s;
        }
        i--;
          goto _L5
        s1;
          goto _L6
    }

    public static String a(String s, JSONObject jsonobject, int i)
    {
        if (jsonobject != null)
        {
            jsonobject = jsonobject.toString();
        } else
        {
            jsonobject = null;
        }
        return a(s, ((String) (jsonobject)), i);
    }

    public static String a(String s, byte abyte0[], int i)
    {
        if (s != null) goto _L2; else goto _L1
_L1:
        return null;
_L2:
        HttpPost httppost;
        DefaultHttpClient defaulthttpclient;
        httppost = new HttpPost(s);
        if (abyte0 != null)
        {
            httppost.setEntity(new InputStreamEntity(new ByteArrayInputStream(abyte0), abyte0.length));
        }
        defaulthttpclient = new DefaultHttpClient(a());
        s = null;
_L5:
        if (i <= 0) goto _L1; else goto _L3
_L3:
        abyte0 = defaulthttpclient.execute(httppost);
        s = abyte0;
_L6:
        if (s == null) goto _L1; else goto _L4
_L4:
        if (s.getStatusLine().getStatusCode() == 200)
        {
            try
            {
                s = EntityUtils.toString(s.getEntity(), "UTF-8");
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return null;
            }
            return s;
        }
        i--;
          goto _L5
        abyte0;
          goto _L6
    }

    private static HttpParams a()
    {
        BasicHttpParams basichttpparams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basichttpparams, 0x1d4c0);
        HttpConnectionParams.setSoTimeout(basichttpparams, 0x1d4c0);
        HttpConnectionParams.setSocketBufferSize(basichttpparams, 10240);
        return basichttpparams;
    }

    private static InputStream b(String s, HashMap hashmap)
    {
        DefaultHttpClient defaulthttpclient;
        defaulthttpclient = new DefaultHttpClient(a());
        s = new HttpPost(s);
        s.setEntity(new UrlEncodedFormEntity(com.igexin.getuiext.util.b.b(hashmap)));
        s = defaulthttpclient.execute(s);
        if (s.getStatusLine().getStatusCode() != 200)
        {
            break MISSING_BLOCK_LABEL_75;
        }
        s = s.getEntity().getContent();
        return s;
        s;
        return null;
    }
}
