// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.igexin.getuiext.data.a;
import java.util.ArrayList;

public class b
{

    private a a;

    public b(a a1)
    {
        a = null;
        a = a1;
    }

    public ArrayList a(String s)
    {
        ArrayList arraylist = null;
        Cursor cursor = a.a("biinfo", null, "bitype=?", new String[] {
            s
        }, null, null, null);
        if (cursor != null)
        {
            arraylist = new ArrayList();
            for (int i = 0; i < 20 && cursor.moveToNext(); i++)
            {
                com.igexin.getuiext.data.a.b b1 = new com.igexin.getuiext.data.a.b();
                int j = cursor.getInt(cursor.getColumnIndex("id"));
                String s1 = cursor.getString(cursor.getColumnIndex("value"));
                b1.a = j;
                b1.b = s1;
                b1.c = s;
                arraylist.add(b1);
            }

            cursor.close();
        }
        return arraylist;
    }

    public void a(String s, int i)
    {
        Object obj = a.a("biinfo");
        if (obj != null && ((Cursor) (obj)).getCount() >= 20)
        {
            ((Cursor) (obj)).moveToFirst();
            int j = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndex("id"));
            a.a("biinfo", "id = ?", new String[] {
                String.valueOf(j)
            });
        }
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        obj = new ContentValues();
        ((ContentValues) (obj)).put("value", s);
        ((ContentValues) (obj)).put("bitype", String.valueOf(i));
        a.a("biinfo", ((ContentValues) (obj)));
    }

    public void b(String s)
    {
        a.a("biinfo", "bitype = ?", new String[] {
            s
        });
    }
}
