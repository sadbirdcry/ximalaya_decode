// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.getuiext.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.igexin.getuiext.data.a.d;
import com.igexin.getuiext.data.a.e;
import com.igexin.getuiext.data.a.f;

public class a
{

    private com.igexin.getuiext.data.a a;

    public a(com.igexin.getuiext.data.a a1)
    {
        a = a1;
    }

    public com.igexin.getuiext.data.a.a a(int i)
    {
        e e1 = null;
        Object obj = null;
        Cursor cursor = a.a("appinfo", null, "download_id = ?", new String[] {
            String.valueOf(i)
        }, null, null, null);
        if (cursor != null)
        {
            e1 = obj;
            if (cursor.moveToFirst())
            {
                e1 = new e();
                e1.a = cursor.getString(cursor.getColumnIndexOrThrow("name"));
                e1.b = cursor.getString(cursor.getColumnIndexOrThrow("pkgName"));
                e1.d = cursor.getInt(cursor.getColumnIndexOrThrow("versionCode"));
                e1.c = cursor.getString(cursor.getColumnIndexOrThrow("versionName"));
                e1.o = cursor.getLong(cursor.getColumnIndexOrThrow("diffSize"));
                e1.i = cursor.getLong(cursor.getColumnIndexOrThrow("fullSize"));
                e1.f = cursor.getString(cursor.getColumnIndexOrThrow("logo"));
                e1.g = cursor.getString(cursor.getColumnIndexOrThrow("url"));
                e1.n = f.a(cursor.getString(cursor.getColumnIndexOrThrow("updateType")));
                e1.p = cursor.getString(cursor.getColumnIndexOrThrow("diffChecksum"));
                e1.q = cursor.getString(cursor.getColumnIndexOrThrow("fullChecksum"));
            }
            cursor.close();
        }
        return e1;
    }

    public void a(int i, com.igexin.getuiext.data.a.a a1)
    {
        if (a1 != null)
        {
            ContentValues contentvalues = new ContentValues();
            contentvalues.put("download_id", Integer.valueOf(i));
            if (a1.a != null)
            {
                contentvalues.put("name", a1.a);
            }
            if (a1.b != null)
            {
                contentvalues.put("pkgName", a1.b);
            }
            if (a1.c != null)
            {
                contentvalues.put("versionName", a1.c);
            }
            contentvalues.put("versionCode", Integer.valueOf(a1.d));
            if (a1 instanceof d)
            {
                if (((d)a1).f != null)
                {
                    contentvalues.put("logo", ((d)a1).f);
                }
                contentvalues.put("fullSize", Long.valueOf(((d)a1).i));
                if (((d)a1).g != null)
                {
                    contentvalues.put("url", ((d)a1).g);
                }
                if (a1 instanceof e)
                {
                    a1 = (e)a1;
                    contentvalues.put("diffSize", Long.valueOf(((e) (a1)).o));
                    if (((e) (a1)).n != null)
                    {
                        contentvalues.put("updateType", ((e) (a1)).n.name());
                    }
                    if (((e) (a1)).p != null)
                    {
                        contentvalues.put("diffChecksum", ((e) (a1)).p);
                    }
                    if (((e) (a1)).q != null)
                    {
                        contentvalues.put("fullChecksum", ((e) (a1)).q);
                    }
                }
            }
            a.a("appinfo", contentvalues);
        }
    }

    public void b(int i)
    {
        a.a("appinfo", "download_id = ?", new String[] {
            String.valueOf(i)
        });
    }
}
