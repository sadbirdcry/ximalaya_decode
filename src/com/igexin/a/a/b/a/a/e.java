// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.a.a.b.a.a;

import com.igexin.a.a.b.c;
import com.igexin.a.a.b.d;
import com.igexin.a.a.b.f;
import com.igexin.a.a.c.a;
import com.igexin.a.a.d.a.g;
import java.io.EOFException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

// Referenced classes of package com.igexin.a.a.b.a.a:
//            f, d, b, g

public final class e extends f
{

    static volatile e l;
    Selector e;
    Selector f;
    AtomicBoolean g;
    AtomicBoolean h;
    volatile boolean i;
    final long j = 15000L;
    int k;
    volatile long m;
    volatile long n;
    volatile long o;
    ConcurrentLinkedQueue p;
    List q;
    com.igexin.a.a.b.a.a.d r;
    ByteBuffer s;
    boolean t;
    int u;
    final Comparator v = new com.igexin.a.a.b.a.a.f(this);

    public e(int i1, String s1, c c1)
    {
        super(i1, s1, c1);
        g = new AtomicBoolean(false);
        h = new AtomicBoolean(true);
        p = new ConcurrentLinkedQueue();
        q = new ArrayList(16);
        s = ByteBuffer.allocate(61440);
    }

    public static e a(String s1, c c1)
    {
        if (l != null && !l.F && !l.x)
        {
            if (!l.a.equals(s1))
            {
                throw new IllegalArgumentException();
            } else
            {
                return l;
            }
        } else
        {
            s1 = new e(-2047, s1, c1);
            l = s1;
            return s1;
        }
    }

    public static e h()
    {
        return l;
    }

    final void a(SocketChannel socketchannel)
    {
_L2:
        int i1 = socketchannel.read(s);
        if (i1 >= 0)
        {
            break MISSING_BLOCK_LABEL_80;
        }
        com.igexin.a.a.c.a.a("socketread|-1|");
        u = u + 1;
        if (u > 20)
        {
            u = 0;
            throw new EOFException("NioConnection Read EOF!");
        }
        break MISSING_BLOCK_LABEL_224;
        if (i1 == 0)
        {
            d d1;
            int j1;
            try
            {
                com.igexin.a.a.c.a.a("socketread|0|");
                return;
            }
            // Misplaced declaration of an exception variable
            catch (SocketChannel socketchannel)
            {
                com.igexin.a.a.c.a.a((new StringBuilder()).append("exceptionsocketread|").append(socketchannel.getMessage()).toString());
                throw socketchannel;
            }
            // Misplaced declaration of an exception variable
            catch (SocketChannel socketchannel)
            {
                com.igexin.a.a.c.a.a((new StringBuilder()).append("exceptionsocketread|").append(socketchannel.getMessage()).toString());
            }
            break MISSING_BLOCK_LABEL_216;
        }
        t = true;
        s.flip();
        j1 = s.remaining();
        if (!d.f)
        {
            break MISSING_BLOCK_LABEL_171;
        }
        d1 = com.igexin.a.a.b.d.c();
        d1.d = d1.d + (long)j1;
_L3:
        if (b != null)
        {
            b.c(this, d, s);
        }
        s.clear();
        if (i1 > 0) goto _L2; else goto _L1
_L1:
        return;
        d1 = com.igexin.a.a.b.d.c();
        d1.b = d1.b + (long)j1;
          goto _L3
        throw new ClosedChannelException();
    }

    public void a_()
    {
        boolean flag;
        flag = false;
        super.a_();
        if (d != null) goto _L2; else goto _L1
_L1:
        if (l() != null)
        {
            l().release();
        }
        g();
        if (l() != null)
        {
            l().acquire();
        }
_L5:
        return;
        Exception exception;
        exception;
        if (l() != null)
        {
            l().acquire();
        }
        throw exception;
_L2:
        if (g.get() || r.b)
        {
            if (g.get())
            {
                b b1 = new b();
                b1.a = 1;
                com.igexin.a.a.b.d.c().a(b1);
            }
            throw new ClosedChannelException();
        }
        if (!p.isEmpty())
        {
            r.b(true);
        }
        if (m < 0L)
        {
            m = 0L;
        }
        n = System.currentTimeMillis();
        h.set(false);
        int i1;
        if (l() != null)
        {
            com.igexin.a.a.c.a.a("wakelock|niosockettask|off");
            l().release();
        }
        if (m <= 0L)
        {
            break MISSING_BLOCK_LABEL_404;
        }
        Q.b(n + m + com.igexin.a.a.d.e.z);
        i1 = e.select(m);
        Q.f();
_L3:
        if (l() != null)
        {
            l().acquire();
            com.igexin.a.a.c.a.a("wakelock|niosockettask|on");
        }
        m = -1L;
        o = System.currentTimeMillis() - n;
        if (o >= 30L || i1 != 0)
        {
            break MISSING_BLOCK_LABEL_475;
        }
        k = k + 1;
        Thread.yield();
        if (k <= 59)
        {
            break MISSING_BLOCK_LABEL_480;
        }
        com.igexin.a.a.c.a.a("exceptionrebuildselector");
        f = Selector.open();
        for (Iterator iterator = e.keys().iterator(); iterator.hasNext();)
        {
            SelectionKey selectionkey = (SelectionKey)iterator.next();
            Exception exception1;
            if (selectionkey.isValid() && selectionkey.interestOps() != 0)
            {
                r.e = r.a.register(f, selectionkey.interestOps(), selectionkey.attachment());
            } else
            {
                selectionkey.cancel();
            }
        }

        break MISSING_BLOCK_LABEL_446;
        i1 = e.select();
          goto _L3
        exception1;
        if (l() != null)
        {
            l().acquire();
            com.igexin.a.a.c.a.a("wakelock|niosockettask|on");
        }
        throw exception1;
        k = 0;
        e.selectNow();
        e.close();
        e = f;
        return;
        k = 0;
        if (i1 <= 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        t = false;
        Set set = e.selectedKeys();
        Iterator iterator2 = set.iterator();
        while (iterator2.hasNext()) 
        {
            SelectionKey selectionkey1 = (SelectionKey)iterator2.next();
            set.remove(selectionkey1);
            if (selectionkey1.isValid() && selectionkey1.isWritable())
            {
                m = b((SocketChannel)selectionkey1.channel());
            }
            if (selectionkey1.isValid() && selectionkey1.isReadable())
            {
                a((SocketChannel)selectionkey1.channel());
            }
        }
        if (true) goto _L5; else goto _L4
_L4:
        if (q.isEmpty()) goto _L5; else goto _L6
_L6:
        Iterator iterator1;
        long l1;
        l1 = System.currentTimeMillis();
        iterator1 = q.iterator();
        i1 = ((flag) ? 1 : 0);
_L15:
        com.igexin.a.a.b.a.a.g g1;
        if (!iterator1.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        g1 = (com.igexin.a.a.b.a.a.g)iterator1.next();
        if (g1.P == null) goto _L8; else goto _L7
_L7:
        if (!g1.P.b()) goto _L10; else goto _L9
_L9:
        if (!t && !i1) goto _L12; else goto _L11
_L11:
        g1.o();
        g1.P.b(g1);
_L13:
        iterator1.remove();
        continue; /* Loop/switch isn't completed */
_L12:
        if (g1.P.a(l1, g1))
        {
            g1.o();
            g1.P.a(g1);
            i1 = 1;
            continue; /* Loop/switch isn't completed */
        }
        long l2 = g1.P.b(l1, g1);
        if (m < 0L || m < l2)
        {
            m = l2;
        }
        continue; /* Loop/switch isn't completed */
_L10:
        g1.o();
        if (true) goto _L13; else goto _L8
_L8:
        g1.o();
        iterator1.remove();
        if (true) goto _L15; else goto _L14
_L14:
        if (!i1) goto _L5; else goto _L16
_L16:
        throw new SocketTimeoutException("SocketTask do timeOut!");
    }

    public final int b()
    {
        return -2047;
    }

    final long b(SocketChannel socketchannel)
    {
        if (p.isEmpty()) goto _L2; else goto _L1
_L1:
        int i1 = 0;
        long l1 = System.currentTimeMillis();
_L19:
        int j1 = 0;
        Object obj1;
        obj1 = (com.igexin.a.a.b.a.a.g)p.peek();
        obj1.d = d;
        if (((com.igexin.a.a.b.a.a.g) (obj1)).f == null) goto _L4; else goto _L3
_L3:
        Object obj = ((com.igexin.a.a.b.a.a.g) (obj1)).f;
_L9:
        boolean flag = ((ByteBuffer) (obj)).hasRemaining();
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_404;
        }
        int k1 = socketchannel.write(((ByteBuffer) (obj)));
        i1 = k1;
_L10:
        if (i1 <= 0) goto _L6; else goto _L5
_L5:
        if (!d.f) goto _L8; else goto _L7
_L7:
        obj1 = com.igexin.a.a.b.d.c();
        obj1.e = ((d) (obj1)).e + (long)i1;
          goto _L9
_L4:
        obj = ByteBuffer.wrap((byte[])(byte[])b.d(((f) (obj1)), d, ((com.igexin.a.a.b.a.a.g) (obj1)).c));
        obj1.f = ((ByteBuffer) (obj));
          goto _L9
        IOException ioexception;
        ioexception;
        i1 = -1;
        com.igexin.a.a.c.a.a((new StringBuilder()).append("exceptionsocketwrite|").append(ioexception.getMessage()).append("|").append(((ByteBuffer) (obj)).toString()).toString());
          goto _L10
        socketchannel;
        obj = r;
        Exception exception;
        d d1;
        boolean flag1;
        if (!p.isEmpty())
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        ((com.igexin.a.a.b.a.a.d) (obj)).b(flag1);
        throw socketchannel;
        exception;
        com.igexin.a.a.c.a.a((new StringBuilder()).append("exceptionsocketwrite|").append(exception.getMessage()).append("|").append(((ByteBuffer) (obj)).toString()).toString());
          goto _L10
_L8:
        d1 = com.igexin.a.a.b.d.c();
        d1.c = d1.c + (long)i1;
          goto _L9
_L6:
        if (i1 >= 0)
        {
            break MISSING_BLOCK_LABEL_312;
        }
        com.igexin.a.a.c.a.a((new StringBuilder()).append("socketwrite|-1|").append(((ByteBuffer) (obj)).toString()).toString());
        throw new ClosedChannelException();
        if (i1 != 0 || j1 >= 140)
        {
            break MISSING_BLOCK_LABEL_367;
        }
        wait(200L);
        com.igexin.a.a.c.a.a((new StringBuilder()).append("socketwrite|0|").append(((ByteBuffer) (obj)).toString()).toString());
        j1++;
          goto _L9
        com.igexin.a.a.c.a.a((new StringBuilder()).append("socketwrite|-2|").append(((ByteBuffer) (obj)).toString()).toString());
        throw new SocketTimeoutException("write data error!");
        obj = (com.igexin.a.a.b.a.a.g)p.poll();
        if (((com.igexin.a.a.b.a.a.g) (obj)).K <= 0 || ((com.igexin.a.a.b.a.a.g) (obj)).P == null) goto _L12; else goto _L11
_L11:
        ((com.igexin.a.a.b.a.a.g) (obj)).a(l1);
        q.add(obj);
        Collections.sort(q, v);
_L17:
        if (!p.isEmpty())
        {
            continue; /* Loop/switch isn't completed */
        }
        flag1 = q.isEmpty();
        if (!flag1) goto _L14; else goto _L13
_L13:
        l1 = -1L;
_L15:
        socketchannel = r;
        if (!p.isEmpty())
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        socketchannel.b(flag1);
        return l1;
_L12:
        ((com.igexin.a.a.b.a.a.g) (obj)).o();
        com.igexin.a.a.b.d.c().a(obj);
        break; /* Loop/switch isn't completed */
_L14:
        l1 = TimeUnit.SECONDS.toMillis(((com.igexin.a.a.b.a.a.g)q.get(0)).K);
          goto _L15
_L2:
        return -1L;
        if (true) goto _L17; else goto _L16
_L16:
        if (true) goto _L19; else goto _L18
_L18:
    }

    public final void d()
    {
        super.d();
        z = true;
        A = true;
        U = true;
    }

    protected void e()
    {
    }

    public void f()
    {
        try
        {
            r.a();
        }
        catch (Exception exception1) { }
        if (e != null)
        {
            try
            {
                e.selectNow();
                e.close();
            }
            catch (Exception exception) { }
        }
        e = null;
        g = null;
        if (!p.isEmpty())
        {
            com.igexin.a.a.b.a.a.g g1;
            for (Iterator iterator = p.iterator(); iterator.hasNext(); com.igexin.a.a.b.d.c().a(g1))
            {
                g1 = (com.igexin.a.a.b.a.a.g)iterator.next();
                g1.o();
            }

            p.clear();
        }
        p = null;
        if (!q.isEmpty())
        {
            com.igexin.a.a.b.a.a.g g2;
            for (Iterator iterator1 = q.iterator(); iterator1.hasNext(); com.igexin.a.a.b.d.c().a(g2))
            {
                g2 = (com.igexin.a.a.b.a.a.g)iterator1.next();
                g2.o();
            }

            q.clear();
        }
        s.clear();
        s = null;
        h = null;
        g = null;
        if (l == this)
        {
            l = null;
        }
        super.f();
    }

    final void g()
    {
        if (e == null)
        {
            e = Selector.open();
        }
        if (r == null)
        {
            r = new com.igexin.a.a.b.a.a.d(e);
        }
        if (!r.b())
        {
            r.a(a);
        }
        if (g.get())
        {
            b b1 = new b();
            b1.a = 2;
            com.igexin.a.a.b.d.c().a(b1);
            throw new ClosedChannelException();
        }
        if (e.select(15000L) > 0)
        {
            Set set = e.selectedKeys();
            Iterator iterator = set.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                SelectionKey selectionkey = (SelectionKey)iterator.next();
                set.remove(selectionkey);
                if (selectionkey.isValid() && selectionkey.isConnectable())
                {
                    SocketChannel socketchannel = (SocketChannel)selectionkey.channel();
                    if (socketchannel.finishConnect())
                    {
                        i = true;
                        d = new com.igexin.a.a.b.e();
                        d.a(r);
                        Object obj1 = socketchannel.socket();
                        Object obj = ((Socket) (obj1)).getInetAddress();
                        obj1 = ((Socket) (obj1)).getLocalAddress();
                        StringBuilder stringbuilder = (new StringBuilder()).append("connected|");
                        if (obj != null)
                        {
                            obj = socketchannel.socket().getInetAddress().getHostAddress();
                        } else
                        {
                            obj = "0.0.0.0";
                        }
                        stringbuilder = stringbuilder.append(((String) (obj))).append(":").append(socketchannel.socket().getPort()).append("|");
                        if (obj1 != null)
                        {
                            obj = socketchannel.socket().getLocalAddress().getHostAddress();
                        } else
                        {
                            obj = "0.0.0.0";
                        }
                        com.igexin.a.a.c.a.a(stringbuilder.append(((String) (obj))).append(":").append(socketchannel.socket().getLocalPort()).toString());
                    }
                }
            } while (true);
            if (d == null)
            {
                return;
            } else
            {
                e.selectNow();
                e.close();
                e = null;
                e = Selector.open();
                r.a(e);
                r.e = r.c().register(e, 1);
                return;
            }
        } else
        {
            throw new SocketTimeoutException();
        }
    }

    public void i()
    {
        if (e == null)
        {
            throw new NullPointerException();
        }
        if (!e.isOpen())
        {
            throw new IllegalStateException();
        }
        if (h.compareAndSet(false, true))
        {
            e.wakeup();
        }
    }
}
