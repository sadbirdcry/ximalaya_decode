// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.a.a.b.a.a;

import com.igexin.a.a.b.b;
import com.igexin.a.a.b.g;
import com.igexin.a.a.d.a.a;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

public final class d
    implements b, a
{

    SocketChannel a;
    boolean b;
    long c;
    SelectionKey d;
    SelectionKey e;
    Selector f;

    public d(Selector selector)
    {
        f = selector;
        System.setProperty("java.net.preferIPv6Addresses", "false");
    }

    public void a()
    {
        try
        {
            a(false);
        }
        catch (Exception exception) { }
        a = null;
        f = null;
    }

    final void a(Selector selector)
    {
        f = selector;
    }

    public final void a(boolean flag)
    {
        if (flag)
        {
            b = true;
            return;
        }
        if (a != null)
        {
            a.close();
        }
        if (d != null)
        {
            d.cancel();
            d.attach(null);
        }
        d = null;
        if (e != null)
        {
            e.cancel();
            e.attach(null);
        }
        e = null;
        return;
        Exception exception;
        exception;
        if (d != null)
        {
            d.cancel();
            d.attach(null);
        }
        d = null;
        if (e != null)
        {
            e.cancel();
            e.attach(null);
        }
        e = null;
        throw exception;
    }

    public final boolean a(String s)
    {
        if (b)
        {
            throw new IllegalStateException();
        }
        SocketChannel socketchannel = c();
        s = g.a(s);
        Object obj = new InetSocketAddress(s[1], Integer.parseInt(s[2]));
        d = socketchannel.register(f, 8);
        c = System.currentTimeMillis();
        socketchannel.connect(((java.net.SocketAddress) (obj)));
        Socket socket = socketchannel.socket();
        s = socket.getLocalAddress();
        obj = (new StringBuilder()).append("connecting|").append(((InetSocketAddress) (obj)).getAddress().getHostAddress()).append(":").append(((InetSocketAddress) (obj)).getPort()).append("|");
        if (s != null)
        {
            s = s.getHostAddress();
        } else
        {
            s = "0.0.0.0";
        }
        com.igexin.a.a.c.a.a(((StringBuilder) (obj)).append(s).append(":").append(socket.getLocalPort()).toString());
        a = socketchannel;
        return true;
    }

    final void b(boolean flag)
    {
        if (e == null || !e.isValid())
        {
            return;
        }
        int i = e.interestOps();
        if (flag)
        {
            i |= 4;
        } else
        {
            i &= -5;
        }
        e.interestOps(i);
    }

    public final boolean b()
    {
        while (a == null || b || !a.isOpen() || !a.isConnected() && !a.isConnectionPending()) 
        {
            return false;
        }
        return true;
    }

    SocketChannel c()
    {
        if (a != null && a.isOpen())
        {
            return a;
        } else
        {
            SocketChannel socketchannel = SocketChannel.open();
            socketchannel.configureBlocking(false);
            Socket socket = socketchannel.socket();
            socket.setTcpNoDelay(false);
            socket.setSoLinger(true, 0);
            socket.setSoTimeout(15000);
            return socketchannel;
        }
    }
}
