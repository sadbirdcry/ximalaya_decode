// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.a.a.b;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

// Referenced classes of package com.igexin.a.a.b:
//            a

public final class g
{

    public static final int a(int i, byte abyte0[], int j)
    {
        abyte0[j] = (byte)(i >> 24 & 0xff);
        abyte0[j + 1] = (byte)(i >> 16 & 0xff);
        abyte0[j + 2] = (byte)(i >> 8 & 0xff);
        abyte0[j + 3] = (byte)(i & 0xff);
        return 4;
    }

    public static final int a(long l, byte abyte0[], int i)
    {
        abyte0[i] = (byte)(int)(l >> 56 & 255L);
        abyte0[i + 1] = (byte)(int)(l >> 48 & 255L);
        abyte0[i + 2] = (byte)(int)(l >> 40 & 255L);
        abyte0[i + 3] = (byte)(int)(l >> 32 & 255L);
        abyte0[i + 4] = (byte)(int)(l >> 24 & 255L);
        abyte0[i + 5] = (byte)(int)(l >> 16 & 255L);
        abyte0[i + 6] = (byte)(int)(l >> 8 & 255L);
        abyte0[i + 7] = (byte)(int)(255L & l);
        return 8;
    }

    public static final int a(byte abyte0[], int i)
    {
        return abyte0[i] & 0xff;
    }

    public static final int a(byte abyte0[], int i, byte abyte1[], int j, int k)
    {
        System.arraycopy(abyte0, i, abyte1, j, k);
        return k;
    }

    public static final String a(String as[])
    {
        StringBuffer stringbuffer = new StringBuffer();
        if (!as[0].equals(""))
        {
            stringbuffer.append(as[0]).append("://");
        }
        if (!as[1].equals(""))
        {
            stringbuffer.append(as[1]);
        }
        if (!as[2].equals(""))
        {
            stringbuffer.append(':').append(as[2]);
        }
        if (!as[3].equals(""))
        {
            stringbuffer.append(as[3]);
            if (!as[3].equals("/"))
            {
                stringbuffer.append('/');
            }
        }
        if (!as[4].equals(""))
        {
            stringbuffer.append(as[4]);
        }
        if (!as[5].equals(""))
        {
            stringbuffer.append('?').append(as[5]);
        }
        return stringbuffer.toString();
    }

    private static void a(InputStream inputstream, OutputStream outputstream)
    {
        byte abyte0[] = new byte[1024];
        do
        {
            int i = inputstream.read(abyte0);
            if (i != -1)
            {
                outputstream.write(abyte0, 0, i);
            } else
            {
                return;
            }
        } while (true);
    }

    public static void a(InputStream inputstream, OutputStream outputstream, int i)
    {
        outputstream = new a(outputstream, i);
        a(inputstream, outputstream);
        outputstream.a();
    }

    public static final byte[] a(int i)
    {
        boolean flag = false;
        int i1 = 0;
        int l = 0;
        int j = i;
        int j1;
        int k1;
        do
        {
            i = (j & 0x7f) << 24 | i1;
            k1 = j >>> 7;
            j1 = l + 1;
            if (k1 > 0)
            {
                i = i >>> 8 | 0x80000000;
            }
            i1 = i;
            l = j1;
            j = k1;
        } while (k1 > 0);
        byte abyte0[] = new byte[j1];
        l = 24;
        for (int k = ((flag) ? 1 : 0); k < j1; k++)
        {
            abyte0[k] = (byte)(i >>> l);
            l -= 8;
        }

        return abyte0;
    }

    public static byte[] a(byte abyte0[])
    {
        Object obj1;
        ByteArrayOutputStream bytearrayoutputstream;
        obj1 = null;
        bytearrayoutputstream = new ByteArrayOutputStream();
        Object obj = new GZIPOutputStream(bytearrayoutputstream);
        ((GZIPOutputStream) (obj)).write(abyte0);
        ((GZIPOutputStream) (obj)).finish();
        abyte0 = bytearrayoutputstream.toByteArray();
        if (obj != null)
        {
            try
            {
                ((GZIPOutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
                return abyte0;
            }
        }
        obj = abyte0;
        if (bytearrayoutputstream == null)
        {
            break MISSING_BLOCK_LABEL_53;
        }
        bytearrayoutputstream.close();
        obj = abyte0;
_L2:
        return ((byte []) (obj));
        abyte0;
        obj = null;
_L5:
        if (obj != null)
        {
            try
            {
                ((GZIPOutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (byte abyte0[])
            {
                abyte0.printStackTrace();
                return null;
            }
        }
        obj = obj1;
        if (bytearrayoutputstream == null) goto _L2; else goto _L1
_L1:
        bytearrayoutputstream.close();
        return null;
        abyte0;
        obj = null;
_L4:
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_103;
        }
        ((GZIPOutputStream) (obj)).close();
        if (bytearrayoutputstream != null)
        {
            try
            {
                bytearrayoutputstream.close();
            }
            catch (IOException ioexception)
            {
                ioexception.printStackTrace();
            }
        }
        throw abyte0;
        abyte0;
        if (true) goto _L4; else goto _L3
_L3:
        abyte0;
          goto _L5
    }

    public static final String[] a(String s)
    {
        String as[];
        StringBuffer stringbuffer;
        int j;
        int k;
        stringbuffer = new StringBuffer(s.toLowerCase());
        as = new String[6];
        for (int i = 0; i < 6; i++)
        {
            as[i] = "";
        }

        j = s.indexOf(":");
        if (j > 0)
        {
            as[0] = s.substring(0, j);
            stringbuffer.delete(0, j + 1);
        } else
        if (j == 0)
        {
            throw new IllegalArgumentException("url format error - protocol");
        }
        if (stringbuffer.length() < 2 || stringbuffer.charAt(0) != '/' || stringbuffer.charAt(1) != '/') goto _L2; else goto _L1
_L1:
        stringbuffer.delete(0, 2);
        k = stringbuffer.toString().indexOf('/');
        j = k;
        if (k < 0)
        {
            j = stringbuffer.length();
        }
        if (j == 0) goto _L2; else goto _L3
_L3:
        k = stringbuffer.toString().indexOf(':');
        if (k < 0) goto _L5; else goto _L4
_L4:
        if (k > j)
        {
            throw new IllegalArgumentException("url format error - port");
        }
        as[2] = stringbuffer.toString().substring(k + 1, j);
_L12:
        as[1] = stringbuffer.toString().substring(0, k);
        stringbuffer.delete(0, j);
_L2:
        if (stringbuffer.length() <= 0)
        {
            break MISSING_BLOCK_LABEL_333;
        }
        s = stringbuffer.toString();
        j = s.lastIndexOf('/');
        if (j <= 0) goto _L7; else goto _L6
_L6:
        as[3] = s.substring(0, j);
_L9:
        if (j < s.length() - 1)
        {
            s = s.substring(j + 1, s.length());
            j = s.indexOf('?');
            if (j >= 0)
            {
                as[4] = s.substring(0, j);
                as[5] = s.substring(j + 1);
            } else
            {
                as[4] = s;
            }
        }
_L10:
        return as;
_L7:
        if (j != 0) goto _L9; else goto _L8
_L8:
        if (s.indexOf('?') > 0)
        {
            throw new IllegalArgumentException("url format error - path");
        } else
        {
            as[3] = s;
            return as;
        }
        as[3] = "/";
          goto _L10
_L5:
        k = j;
        if (true) goto _L12; else goto _L11
_L11:
    }

    public static final int b(int i, byte abyte0[], int j)
    {
        abyte0[j] = (byte)(i >> 8 & 0xff);
        abyte0[j + 1] = (byte)(i & 0xff);
        return 2;
    }

    public static final int b(byte abyte0[], int i)
    {
        return (abyte0[i] & 0xff) << 8 | abyte0[i + 1] & 0xff;
    }

    public static byte[] b(byte abyte0[])
    {
        Object obj1 = null;
        ByteArrayInputStream bytearrayinputstream = new ByteArrayInputStream(abyte0);
        Object obj;
        GZIPInputStream gzipinputstream;
        int i;
        try
        {
            gzipinputstream = new GZIPInputStream(bytearrayinputstream);
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            abyte0 = null;
            gzipinputstream = null;
            continue; /* Loop/switch isn't completed */
        }
        finally
        {
            gzipinputstream = null;
            obj = null;
        }
        obj = new ByteArrayOutputStream();
_L3:
        i = gzipinputstream.read();
        if (i == -1) goto _L2; else goto _L1
_L1:
        ((ByteArrayOutputStream) (obj)).write(i);
          goto _L3
        abyte0;
        abyte0 = ((byte []) (obj));
_L9:
        if (abyte0 != null)
        {
            try
            {
                abyte0.close();
            }
            // Misplaced declaration of an exception variable
            catch (byte abyte0[])
            {
                return null;
            }
        }
        if (gzipinputstream == null)
        {
            break MISSING_BLOCK_LABEL_70;
        }
        gzipinputstream.close();
        obj = obj1;
        if (bytearrayinputstream == null)
        {
            break MISSING_BLOCK_LABEL_84;
        }
        bytearrayinputstream.close();
        obj = obj1;
_L5:
        return ((byte []) (obj));
_L2:
        abyte0 = ((ByteArrayOutputStream) (obj)).toByteArray();
        if (obj != null)
        {
            try
            {
                ((ByteArrayOutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                return abyte0;
            }
        }
        if (gzipinputstream == null)
        {
            break MISSING_BLOCK_LABEL_107;
        }
        gzipinputstream.close();
        obj = abyte0;
        if (bytearrayinputstream == null) goto _L5; else goto _L4
_L4:
        bytearrayinputstream.close();
        return abyte0;
_L7:
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_137;
        }
        ((ByteArrayOutputStream) (obj)).close();
        if (gzipinputstream == null)
        {
            break MISSING_BLOCK_LABEL_145;
        }
        gzipinputstream.close();
        if (bytearrayinputstream != null)
        {
            try
            {
                bytearrayinputstream.close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        throw abyte0;
        abyte0;
        obj = null;
        continue; /* Loop/switch isn't completed */
        abyte0;
        if (true) goto _L7; else goto _L6
_L6:
        abyte0;
        abyte0 = null;
        if (true) goto _L9; else goto _L8
_L8:
    }

    public static final int c(int i, byte abyte0[], int j)
    {
        abyte0[j] = (byte)i;
        return 1;
    }

    public static final int c(byte abyte0[], int i)
    {
        return (abyte0[i] & 0xff) << 24 | (abyte0[i + 1] & 0xff) << 16 | (abyte0[i + 2] & 0xff) << 8 | abyte0[i + 3] & 0xff;
    }

    public static final long d(byte abyte0[], int i)
    {
        return ((long)abyte0[i] & 255L) << 56 | ((long)abyte0[i + 1] & 255L) << 48 | ((long)abyte0[i + 2] & 255L) << 40 | ((long)abyte0[i + 3] & 255L) << 32 | ((long)abyte0[i + 4] & 255L) << 24 | ((long)abyte0[i + 5] & 255L) << 16 | ((long)abyte0[i + 6] & 255L) << 8 | (long)abyte0[i + 7] & 255L;
    }

    public static byte[] e(byte abyte0[], int i)
    {
        ByteArrayInputStream bytearrayinputstream;
        bytearrayinputstream = new ByteArrayInputStream(abyte0);
        abyte0 = new ByteArrayOutputStream();
        a(bytearrayinputstream, abyte0, i);
        Object obj;
        try
        {
            bytearrayinputstream.close();
        }
        catch (Throwable throwable) { }
        try
        {
            abyte0.close();
        }
        catch (Throwable throwable1) { }
        return abyte0.toByteArray();
        obj;
        throw new RuntimeException("Unexpected I/O error", ((Throwable) (obj)));
        obj;
        try
        {
            bytearrayinputstream.close();
        }
        catch (Throwable throwable2) { }
        try
        {
            abyte0.close();
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[]) { }
        throw obj;
    }
}
