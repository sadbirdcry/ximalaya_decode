// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.a.a.b;


// Referenced classes of package com.igexin.a.a.b:
//            f, e

public abstract class c
{

    protected String a;
    protected c b;
    protected c c;
    protected boolean d;

    public c(String s, boolean flag)
    {
        a = s;
        d = flag;
    }

    public abstract Object a(f f, e e, Object obj);

    protected final void a(c c1)
    {
        if (c1 == null)
        {
            return;
        } else
        {
            c c2 = c1.b;
            c1.b = this;
            c = c1;
            b = c2;
            return;
        }
    }

    public void a(boolean flag)
    {
        if (!d || flag)
        {
            while (b != null) 
            {
                c c1 = b.b;
                b.b = null;
                b = c1;
            }
        }
    }

    public abstract Object c(f f, e e, Object obj);

    public final Object d(f f, e e, Object obj)
    {
        if (obj == null)
        {
            throw new NullPointerException("Nothing to encode!");
        }
        Object obj1 = obj;
        if (b != null)
        {
            obj1 = b.d(f, e, obj);
        }
        return a(f, e, obj1);
    }
}
