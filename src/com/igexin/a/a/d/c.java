// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.a.a.d;

import com.igexin.a.a.c.a;
import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

// Referenced classes of package com.igexin.a.a.d:
//            d, e

public class c
{

    static final boolean h;
    final transient ReentrantLock a = new ReentrantLock();
    final transient Condition b;
    final TreeSet c;
    final AtomicInteger d = new AtomicInteger(0);
    int e;
    e f;
    public final AtomicLong g = new AtomicLong(-1L);

    public c(Comparator comparator, e e1)
    {
        b = a.newCondition();
        c = new TreeSet(comparator);
        f = e1;
    }

    private d e()
    {
        d d1;
        for (d1 = a(); d1 == null || !c.remove(d1);)
        {
            return null;
        }

        return d1;
    }

    public final int a(d d1, long l, TimeUnit timeunit)
    {
        ReentrantLock reentrantlock;
        reentrantlock = a;
        reentrantlock.lock();
        boolean flag = c.contains(d1);
        if (!flag)
        {
            reentrantlock.unlock();
            return -1;
        }
        c.remove(d1);
        d1.G = System.currentTimeMillis() + TimeUnit.MILLISECONDS.convert(l, timeunit);
        flag = a(d1);
        int i;
        if (flag)
        {
            i = 1;
        } else
        {
            i = -2;
        }
        reentrantlock.unlock();
        return i;
        d1;
        reentrantlock.unlock();
        throw d1;
    }

    d a()
    {
        d d1;
        try
        {
            d1 = (d)c.first();
        }
        catch (NoSuchElementException nosuchelementexception)
        {
            return null;
        }
        return d1;
    }

    public final boolean a(d d1)
    {
        ReentrantLock reentrantlock;
        if (d1 == null)
        {
            return false;
        }
        reentrantlock = a;
        reentrantlock.lock();
        d d2;
        d2 = a();
        int i = e + 1;
        e = i;
        d1.H = i;
        if (c.add(d1))
        {
            break MISSING_BLOCK_LABEL_67;
        }
        d1.H = d1.H - 1;
        reentrantlock.unlock();
        return false;
        d1.m();
        if (d2 == null)
        {
            break MISSING_BLOCK_LABEL_92;
        }
        if (c.comparator().compare(d1, d2) >= 0)
        {
            break MISSING_BLOCK_LABEL_101;
        }
        b.signalAll();
        reentrantlock.unlock();
        return true;
        d1;
        com.igexin.a.a.c.a.a("ScheduleQueue|offer|error");
        reentrantlock.unlock();
        return false;
        d1;
        reentrantlock.unlock();
        throw d1;
    }

    final boolean b()
    {
        ReentrantLock reentrantlock;
        reentrantlock = a;
        reentrantlock.lock();
        boolean flag = c.isEmpty();
        reentrantlock.unlock();
        return flag;
        Exception exception;
        exception;
        reentrantlock.unlock();
        throw exception;
    }

    public final d c()
    {
        ReentrantLock reentrantlock;
        reentrantlock = a;
        reentrantlock.lockInterruptibly();
_L1:
        Object obj = a();
        if (obj != null)
        {
            break MISSING_BLOCK_LABEL_50;
        }
        d.set(1);
        e = 0;
        b.await();
          goto _L1
        obj;
        reentrantlock.unlock();
        throw obj;
        long l = ((d) (obj)).a(TimeUnit.NANOSECONDS);
        boolean flag;
        if (((d) (obj)).x || ((d) (obj)).y)
        {
            flag = true;
        } else
        {
            flag = false;
        }
          goto _L2
_L3:
        obj = e();
        if (h || obj != null)
        {
            break MISSING_BLOCK_LABEL_99;
        }
        throw new AssertionError();
        if (!b())
        {
            b.signalAll();
        }
        g.set(-1L);
        reentrantlock.unlock();
        return ((d) (obj));
_L4:
        g.set(((d) (obj)).G);
        if (f.y)
        {
            f.a(((d) (obj)).G);
        }
        b.awaitNanos(l);
          goto _L1
_L2:
        if (l > 0L && !flag) goto _L4; else goto _L3
    }

    public final void d()
    {
        c.clear();
    }

    static 
    {
        boolean flag;
        if (!com/igexin/a/a/d/c.desiredAssertionStatus())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        h = flag;
    }
}
