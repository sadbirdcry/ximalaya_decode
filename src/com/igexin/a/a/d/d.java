// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.a.a.d;

import com.igexin.a.a.d.a.a;
import com.igexin.a.a.d.a.e;
import com.igexin.a.a.d.a.g;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

// Referenced classes of package com.igexin.a.a.d:
//            a, e, c

public abstract class d extends com.igexin.a.a.d.a
    implements a
{

    protected static com.igexin.a.a.d.e Q;
    protected volatile boolean A;
    protected volatile boolean B;
    protected volatile boolean C;
    protected volatile boolean D;
    protected volatile boolean E;
    protected volatile boolean F;
    protected volatile long G;
    volatile int H;
    public long I;
    public int J;
    public int K;
    public int L;
    public int M;
    public Exception N;
    public Object O;
    public g P;
    protected final ReentrantLock R;
    protected final Condition S;
    Thread T;
    protected volatile boolean U;
    android.os.PowerManager.WakeLock V;
    int W;
    protected com.igexin.a.a.d.a.d X;
    private byte a;
    protected volatile boolean x;
    protected volatile boolean y;
    protected volatile boolean z;

    public d(int i)
    {
        this(i, null);
    }

    public d(int i, com.igexin.a.a.d.a.d d1)
    {
        L = i;
        X = d1;
        R = new ReentrantLock();
        S = R.newCondition();
    }

    public final int a(long l1, TimeUnit timeunit)
    {
        if (l1 <= 0L) goto _L2; else goto _L1
_L1:
        Q.m.a(this, l1, timeunit);
        JVM INSTR tableswitch -2 1: default 48
    //                   -2 50
    //                   -1 53
    //                   0 48
    //                   1 71;
           goto _L2 _L3 _L4 _L2 _L5
_L2:
        return 0;
_L3:
        return -2;
_L4:
        G = System.currentTimeMillis() + TimeUnit.MILLISECONDS.convert(l1, timeunit);
        return -1;
_L5:
        return 1;
    }

    public long a(TimeUnit timeunit)
    {
        return timeunit.convert(n(), TimeUnit.MILLISECONDS);
    }

    public final void a(int i)
    {
        a = (byte)(a & 0xf);
        a = (byte)(a | (i & 0xf) << 4);
    }

    public final void a(int i, g g)
    {
        if (i < 0)
        {
            throw new IllegalArgumentException("second must > 0");
        } else
        {
            K = i;
            P = g;
            return;
        }
    }

    public final void a(long l1)
    {
        I = l1;
    }

    public void a(android.os.PowerManager.WakeLock wakelock)
    {
        V = wakelock;
    }

    public final void a(com.igexin.a.a.d.a.d d1)
    {
        X = d1;
    }

    public void a_()
    {
        T = Thread.currentThread();
        B = true;
    }

    public void c()
    {
        if (x || y)
        {
            f();
        }
    }

    public void d()
    {
        E = true;
    }

    protected abstract void e();

    public void f()
    {
        O = null;
        N = null;
        T = null;
    }

    public android.os.PowerManager.WakeLock l()
    {
        return V;
    }

    final void m()
    {
        W = W + 1;
        W = W & 0x40fffffe;
    }

    long n()
    {
        return G - System.currentTimeMillis();
    }

    public final void o()
    {
        x = true;
    }

    public final boolean p()
    {
        return z;
    }

    public final boolean q()
    {
        return y;
    }

    public final boolean r()
    {
        return E;
    }

    protected void s()
    {
    }

    public final void t()
    {
        y = true;
    }

    protected void u()
    {
        if (!A && !C && !D)
        {
            x = true;
            B = false;
        } else
        {
            if (C && !x)
            {
                B = false;
                return;
            }
            if (A && !z && !x)
            {
                B = false;
                return;
            }
        }
    }

    protected void v()
    {
        if (X != null)
        {
            X.a(e.a);
        }
    }
}
