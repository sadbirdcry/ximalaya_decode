// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.a.a.d;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

// Referenced classes of package com.igexin.a.a.d:
//            f

final class h
    implements ThreadFactory
{

    final AtomicInteger a = new AtomicInteger(0);
    final f b;

    public h(f f)
    {
        b = f;
        super();
    }

    public Thread newThread(Runnable runnable)
    {
        return new Thread(runnable, (new StringBuilder()).append("TaskService-pool-").append(a.incrementAndGet()).toString());
    }
}
