// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.a.a.d;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import com.igexin.a.a.c.a;
import com.igexin.a.a.d.a.c;
import com.igexin.a.a.d.a.f;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

// Referenced classes of package com.igexin.a.a.d:
//            c, i, d

public class e extends BroadcastReceiver
    implements Comparator
{

    public static final String i = com/igexin/a/a/d/e.getSimpleName();
    public static final long z;
    private boolean a;
    public String h;
    final i j = new i(this);
    final HashMap k = new HashMap(7);
    final ConcurrentLinkedQueue l = new ConcurrentLinkedQueue();
    final com.igexin.a.a.d.c m = new com.igexin.a.a.d.c(this, this);
    final ReentrantLock n = new ReentrantLock();
    final ReentrantLock o = new ReentrantLock();
    PowerManager p;
    AlarmManager q;
    Intent r;
    PendingIntent s;
    Intent t;
    PendingIntent u;
    Intent v;
    PendingIntent w;
    String x;
    volatile boolean y;

    protected e()
    {
        h = getClass().getSimpleName();
        a = false;
        d.Q = this;
    }

    public final int a(d d1, d d2)
    {
        byte byte0;
        int i1;
        if (d1.M > d2.M)
        {
            byte0 = -1;
        } else
        if (d1.M < d2.M)
        {
            byte0 = 1;
        } else
        if (d1.H < d2.H)
        {
            byte0 = -1;
        } else
        if (d1.H > d2.H)
        {
            byte0 = 1;
        } else
        {
            byte0 = 0;
        }
        if (d1.G != d2.G)
        {
            if (d1.G < d2.G)
            {
                byte0 = -1;
            } else
            {
                byte0 = 1;
            }
        }
        i1 = byte0;
        if (byte0 == 0)
        {
            i1 = d1.hashCode() - d2.hashCode();
        }
        return i1;
    }

    public final void a(long l1)
    {
        if (y)
        {
            String s1 = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date(l1));
            com.igexin.a.a.c.a.a((new StringBuilder()).append("setalarm|").append(s1).toString());
            long l2 = l1;
            if (l1 < 0L)
            {
                l2 = System.currentTimeMillis() + z;
            }
            if (s != null && u != null)
            {
                q.set(0, l2, s);
                q.set(0, z + l2, u);
            }
        }
    }

    public final void a(Context context)
    {
        if (!a)
        {
            p = (PowerManager)context.getSystemService("power");
            y = true;
            q = (AlarmManager)context.getSystemService("alarm");
            context.registerReceiver(this, new IntentFilter((new StringBuilder()).append("AlarmTaskSchedule.").append(context.getPackageName()).toString()));
            context.registerReceiver(this, new IntentFilter((new StringBuilder()).append("AlarmTaskScheduleBak.").append(context.getPackageName()).toString()));
            context.registerReceiver(this, new IntentFilter("android.intent.action.SCREEN_OFF"));
            context.registerReceiver(this, new IntentFilter("android.intent.action.SCREEN_ON"));
            x = (new StringBuilder()).append("AlarmNioTaskSchedule.").append(context.getPackageName()).toString();
            context.registerReceiver(this, new IntentFilter(x));
            r = new Intent((new StringBuilder()).append("AlarmTaskSchedule.").append(context.getPackageName()).toString());
            s = PendingIntent.getBroadcast(context, hashCode(), r, 0x8000000);
            t = new Intent((new StringBuilder()).append("AlarmTaskScheduleBak.").append(context.getPackageName()).toString());
            u = PendingIntent.getBroadcast(context, hashCode() + 1, t, 0x8000000);
            v = new Intent(x);
            w = PendingIntent.getBroadcast(context, hashCode() + 2, v, 0x8000000);
            j.start();
            Thread.yield();
            a = true;
        }
    }

    public final boolean a(c c1)
    {
        ReentrantLock reentrantlock;
        if (c1 == null)
        {
            throw new NullPointerException();
        }
        reentrantlock = n;
        if (!reentrantlock.tryLock())
        {
            break MISSING_BLOCK_LABEL_54;
        }
        boolean flag = k.keySet().contains(Long.valueOf(c1.m()));
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_56;
        }
        reentrantlock.unlock();
        return false;
        k.put(Long.valueOf(c1.m()), c1);
        reentrantlock.unlock();
        return true;
        c1;
        reentrantlock.unlock();
        throw c1;
    }

    final boolean a(f f1, c c1)
    {
        int i1 = f1.b();
        if (i1 > 0x80000000 && i1 < 0)
        {
            d d1 = (d)f1;
            boolean flag;
            if (d1.F)
            {
                flag = c1.a(d1, this);
            } else
            {
                flag = c1.a(f1, this);
            }
            if (flag)
            {
                d1.c();
            }
            return flag;
        }
        if (i1 >= 0 && i1 < 0x7fffffff)
        {
            return c1.a(f1, this);
        } else
        {
            return false;
        }
    }

    public final boolean a(d d1, boolean flag)
    {
        int i1 = 0;
        if (d1 == null)
        {
            throw new NullPointerException();
        }
        if (d1.B || d1.x)
        {
            return false;
        }
        com.igexin.a.a.d.c c1 = m;
        if (flag)
        {
            i1 = c1.d.incrementAndGet();
        }
        d1.M = i1;
        return c1.a(d1);
    }

    public final boolean a(d d1, boolean flag, boolean flag1)
    {
        boolean flag2;
        flag2 = true;
        if (d1 == null)
        {
            throw new NullPointerException();
        }
        if (!d1.y) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        if (!flag || flag1)
        {
            break MISSING_BLOCK_LABEL_121;
        }
        d1.d();
        d1.a_();
        d1.s();
        d1.u();
        if (!d1.F)
        {
            d1.c();
        }
        return true;
        Object obj;
        obj;
        d1.F = true;
        d1.N = ((Exception) (obj));
        d1.o();
        d1.v();
        a(d1);
        g();
        if (d1.F) goto _L1; else goto _L3
_L3:
        d1.c();
        return false;
        obj;
        if (!d1.F)
        {
            d1.c();
        }
        throw obj;
        if (flag1 && flag)
        {
            flag = flag2;
        } else
        {
            flag = false;
        }
        return a(d1, flag);
    }

    public final boolean a(Object obj)
    {
        if (obj != null)
        {
            if (!(obj instanceof f))
            {
                throw new ClassCastException("response Obj is not a TaskResult ");
            }
            obj = (f)obj;
            if (!((f) (obj)).j())
            {
                ((f) (obj)).a(false);
                l.offer(obj);
                return true;
            }
        }
        return false;
    }

    public final void b(long l1)
    {
        String s1 = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date(l1));
        com.igexin.a.a.c.a.a((new StringBuilder()).append("setnioalarm|").append(s1).toString());
        long l2 = l1;
        if (l1 < 0L)
        {
            l2 = System.currentTimeMillis() + z;
        }
        if (w != null)
        {
            q.set(0, l2, w);
        }
    }

    public int compare(Object obj, Object obj1)
    {
        return a((d)obj, (d)obj1);
    }

    public final void f()
    {
        if (w != null)
        {
            q.cancel(w);
        }
    }

    protected final void g()
    {
        if (j != null && !j.isInterrupted())
        {
            j.interrupt();
        }
    }

    final void h()
    {
_L7:
        if (l.isEmpty()) goto _L2; else goto _L1
_L1:
        f f1;
        ReentrantLock reentrantlock;
        boolean flag;
        boolean flag1;
        boolean flag2;
        boolean flag3;
        boolean flag4;
        boolean flag5;
        f1 = (f)l.poll();
        f1.a(true);
        flag = false;
        flag4 = false;
        flag5 = false;
        flag1 = false;
        reentrantlock = n;
        reentrantlock.lock();
        flag2 = flag4;
        flag3 = flag5;
        if (k.isEmpty()) goto _L4; else goto _L3
_L3:
        flag2 = flag4;
        flag3 = flag5;
        long l1 = f1.k();
        if (l1 == 0L) goto _L6; else goto _L5
_L5:
        flag2 = flag4;
        flag3 = flag5;
        Object obj = (c)k.get(Long.valueOf(l1));
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_385;
        }
        flag2 = flag4;
        flag3 = flag5;
        if (!((c) (obj)).l())
        {
            break MISSING_BLOCK_LABEL_385;
        }
        flag2 = flag4;
        flag3 = flag5;
        flag = a(f1, ((c) (obj)));
_L10:
        flag1 = flag;
_L4:
        if (!flag1)
        {
            int i1 = f1.b();
            if (i1 > 0x80000000 && i1 < 0)
            {
                ((d)f1).c();
            }
        }
        reentrantlock.unlock();
          goto _L7
_L6:
        flag2 = flag4;
        flag3 = flag5;
        obj = k.values().iterator();
_L9:
        flag1 = flag;
        flag2 = flag;
        flag3 = flag;
        if (!((Iterator) (obj)).hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        flag2 = flag;
        flag3 = flag;
        c c1 = (c)((Iterator) (obj)).next();
        flag2 = flag;
        flag3 = flag;
        if (!c1.l()) goto _L9; else goto _L8
_L8:
        flag2 = flag;
        flag3 = flag;
        flag1 = a(f1, c1);
        flag = flag1;
        if (!flag1) goto _L9; else goto _L4
        Object obj1;
        obj1;
        if (!flag2)
        {
            int j1 = f1.b();
            if (j1 > 0x80000000 && j1 < 0)
            {
                ((d)f1).c();
            }
        }
        reentrantlock.unlock();
          goto _L7
        obj1;
        if (!flag3)
        {
            int k1 = f1.b();
            if (k1 > 0x80000000 && k1 < 0)
            {
                ((d)f1).c();
            }
        }
        reentrantlock.unlock();
        throw obj1;
_L2:
        return;
        flag = false;
          goto _L10
    }

    public final void onReceive(Context context, Intent intent)
    {
        if (!"android.intent.action.SCREEN_OFF".equals(intent.getAction())) goto _L2; else goto _L1
_L1:
        y = true;
        com.igexin.a.a.c.a.a("screenoff");
        if (m.g.get() > 0L)
        {
            a(m.g.get());
        }
_L4:
        return;
_L2:
        if ("android.intent.action.SCREEN_ON".equals(intent.getAction()))
        {
            y = false;
            com.igexin.a.a.c.a.a("screenon");
            return;
        }
        if (intent.getAction().startsWith("AlarmTaskSchedule.") || intent.getAction().startsWith("AlarmTaskScheduleBak."))
        {
            com.igexin.a.a.c.a.a((new StringBuilder()).append("receivealarm|").append(y).toString());
            g();
            return;
        }
        if (!x.equals(intent.getAction())) goto _L4; else goto _L3
_L3:
        com.igexin.a.a.c.a.a("receivenioalarm");
        if (com.igexin.a.a.b.a.a.e.h() == null) goto _L4; else goto _L5
_L5:
        com.igexin.a.a.b.a.a.e.h().i();
        return;
        context;
    }

    static 
    {
        z = TimeUnit.SECONDS.toMillis(2L);
    }
}
