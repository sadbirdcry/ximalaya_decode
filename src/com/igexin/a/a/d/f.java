// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.a.a.d;

import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

// Referenced classes of package com.igexin.a.a.d:
//            h, d, g, e

final class f
{

    final BlockingQueue a = new SynchronousQueue();
    final HashMap b = new HashMap();
    final ReentrantLock c = new ReentrantLock();
    ThreadFactory d;
    volatile long e;
    volatile int f;
    volatile int g;
    volatile int h;
    final e i;

    public f(e e1)
    {
        i = e1;
        super();
        e = TimeUnit.SECONDS.toNanos(60L);
        f = 0;
        d = new h(this);
        h = 0x7fffffff;
    }

    final d a()
    {
_L7:
        if (g <= f) goto _L2; else goto _L1
_L1:
        Object obj = (d)a.poll(e, TimeUnit.NANOSECONDS);
          goto _L3
_L2:
        boolean flag;
        try
        {
            obj = (d)a.take();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            continue; /* Loop/switch isn't completed */
        }
          goto _L3
_L5:
        flag = a.isEmpty();
        if (flag)
        {
            return null;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (obj == null) goto _L5; else goto _L4
_L4:
        return ((d) (obj));
        if (true) goto _L7; else goto _L6
_L6:
    }

    final void a(d d1)
    {
        ReentrantLock reentrantlock;
        if (d1 == null)
        {
            throw new NullPointerException();
        }
        if (d1.L == 0)
        {
            break MISSING_BLOCK_LABEL_70;
        }
        reentrantlock = c;
        reentrantlock.lock();
        g g1 = (g)b.get(Integer.valueOf(d1.L));
        if (g1 == null)
        {
            break MISSING_BLOCK_LABEL_66;
        }
        g1.a.offer(d1);
        reentrantlock.unlock();
        return;
        reentrantlock.unlock();
        b(d1);
        return;
        d1;
        reentrantlock.unlock();
        throw d1;
    }

    final boolean a(g g1)
    {
        ReentrantLock reentrantlock;
        reentrantlock = c;
        reentrantlock.lock();
        int j;
        j = g - 1;
        g = j;
        if (j != 0) goto _L2; else goto _L1
_L1:
        if (a.isEmpty()) goto _L2; else goto _L3
_L3:
        Thread thread = f(null);
        if (thread == null)
        {
            break MISSING_BLOCK_LABEL_54;
        }
        thread.start();
_L5:
        b.remove(Integer.valueOf(g1.d));
        reentrantlock.unlock();
        return false;
_L2:
        boolean flag = g1.a.isEmpty();
        if (flag) goto _L5; else goto _L4
_L4:
        reentrantlock.unlock();
        return true;
        g1;
        reentrantlock.unlock();
        throw g1;
    }

    final void b(d d1)
    {
        if (g >= f || !c(d1))
        {
            if (a.offer(d1))
            {
                if (g == 0)
                {
                    e(d1);
                }
            } else
            if (!d(d1))
            {
                return;
            }
        }
    }

    final boolean c(d d1)
    {
        Thread thread;
        ReentrantLock reentrantlock;
        thread = null;
        reentrantlock = c;
        reentrantlock.lock();
        if (g < f)
        {
            thread = f(d1);
        }
        reentrantlock.unlock();
        if (thread == null)
        {
            return false;
        } else
        {
            thread.start();
            return true;
        }
        d1;
        reentrantlock.unlock();
        throw d1;
    }

    final boolean d(d d1)
    {
        Thread thread;
        ReentrantLock reentrantlock;
        thread = null;
        reentrantlock = c;
        reentrantlock.lock();
        if (g < h)
        {
            thread = f(d1);
        }
        reentrantlock.unlock();
        if (thread == null)
        {
            return false;
        } else
        {
            thread.start();
            return true;
        }
        d1;
        reentrantlock.unlock();
        throw d1;
    }

    final void e(d d1)
    {
        Object obj;
        ReentrantLock reentrantlock;
        obj = null;
        reentrantlock = c;
        reentrantlock.lock();
        d1 = obj;
        if (g >= Math.max(f, 1))
        {
            break MISSING_BLOCK_LABEL_48;
        }
        d1 = obj;
        if (!a.isEmpty())
        {
            d1 = f(null);
        }
        reentrantlock.unlock();
        if (d1 != null)
        {
            d1.start();
        }
        return;
        d1;
        reentrantlock.unlock();
        throw d1;
    }

    final Thread f(d d1)
    {
        g g1 = new g(this, d1);
        if (d1 != null && d1.L != 0)
        {
            b.put(Integer.valueOf(d1.L), g1);
        }
        d1 = d.newThread(g1);
        if (d1 != null)
        {
            g = g + 1;
        }
        return d1;
    }
}
