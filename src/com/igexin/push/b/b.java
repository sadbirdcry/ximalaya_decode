// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.b;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class b extends SQLiteOpenHelper
{

    SQLiteDatabase a;

    public b(Context context)
    {
        super(context, "pushsdk.db", null, 2);
        a = null;
    }

    private String a(String as[], String as1[], int i)
    {
        int j = 0;
        StringBuffer stringbuffer = new StringBuffer(" ");
        if (as.length == 1)
        {
            for (j = 0; j < i; j++)
            {
                stringbuffer.append((new StringBuilder()).append(as[0]).append(" = '").append(as1[j]).append("'").toString());
                if (j < i - 1)
                {
                    stringbuffer.append(" or ");
                }
            }

        } else
        {
            for (; j < i; j++)
            {
                stringbuffer.append((new StringBuilder()).append(as[j]).append(" = '").append(as1[j]).append("'").toString());
                if (j < i - 1)
                {
                    stringbuffer.append(" and ");
                }
            }

        }
        return stringbuffer.toString();
    }

    public Cursor a(String s, String as[], String as1[], String as2[], String s1)
    {
        a = getReadableDatabase();
        a.beginTransaction();
        if (as != null) goto _L2; else goto _L1
_L1:
        s = a.query(s, as2, null, null, null, null, s1);
_L3:
        a.setTransactionSuccessful();
        a.endTransaction();
        return s;
_L2:
label0:
        {
            if (as.length != 1)
            {
                break MISSING_BLOCK_LABEL_131;
            }
            if (as1.length != 1)
            {
                break label0;
            }
            s = a.query(s, as2, (new StringBuilder()).append(as[0]).append("= ?").toString(), as1, null, null, s1);
        }
          goto _L3
        s = a.query(s, as2, a(as, as1, as1.length), null, null, null, s1);
          goto _L3
        s = a.query(s, as2, a(as, as1, as.length), null, null, null, s1);
          goto _L3
        s;
        s = null;
_L5:
        a.endTransaction();
        return s;
        s;
        a.endTransaction();
        throw s;
        as;
        if (true) goto _L5; else goto _L4
_L4:
    }

    public void a(String s, ContentValues contentvalues)
    {
        a = getWritableDatabase();
        a.beginTransaction();
        try
        {
            a.insert(s, null, contentvalues);
            a.setTransactionSuccessful();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            a.endTransaction();
            return;
        }
        finally
        {
            a.endTransaction();
        }
        a.endTransaction();
        return;
        throw s;
    }

    public void a(String s, ContentValues contentvalues, String as[], String as1[])
    {
        a = getWritableDatabase();
        a.beginTransaction();
        if (as != null) goto _L2; else goto _L1
_L1:
        a.update(s, contentvalues, null, null);
_L3:
        a.setTransactionSuccessful();
        a.endTransaction();
        return;
_L2:
label0:
        {
            if (as.length != 1)
            {
                break MISSING_BLOCK_LABEL_149;
            }
            if (as1.length != 1)
            {
                break label0;
            }
            a.update(s, contentvalues, (new StringBuilder()).append(as[0]).append("='").append(as1[0]).append("'").toString(), null);
        }
          goto _L3
        try
        {
            a.update(s, contentvalues, a(as, as1, as1.length), null);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            a.endTransaction();
            return;
        }
        finally
        {
            a.endTransaction();
        }
          goto _L3
        throw s;
        a.update(s, contentvalues, a(as, as1, as.length), null);
          goto _L3
    }

    public void a(String s, String s1)
    {
        a = getWritableDatabase();
        a.delete(s, s1, null);
    }

    public void onCreate(SQLiteDatabase sqlitedatabase)
    {
        Exception exception1;
        sqlitedatabase.beginTransaction();
        try
        {
            sqlitedatabase.execSQL("create table if not exists config (id integer primary key,value text)");
            sqlitedatabase.execSQL("create table if not exists runtime (id integer primary key,value text)");
            sqlitedatabase.execSQL("create table if not exists message (id integer primary key autoincrement,messageid text,taskid text,appid text,info text,msgextra blob,key text,status integer,createtime integer)");
            sqlitedatabase.execSQL("create table if not exists ral (id integer primary key,data text,type integer,time integer)");
            sqlitedatabase.execSQL("create table if not exists ca (pkgname text primary key,signature text,permissions text, accesstoken blob, expire integer)");
            sqlitedatabase.execSQL("create table if not exists bi(id integer primary key autoincrement, start_service_count integer, login_count integer, loginerror_nonetwork_count integer, loginerror_timeout_count integer, loginerror_connecterror_count integer, loginerror_other_count integer, online_time long, network_time long, running_time long, create_time text, type integer)");
            sqlitedatabase.setTransactionSuccessful();
        }
        catch (Exception exception)
        {
            sqlitedatabase.endTransaction();
            return;
        }
        finally
        {
            sqlitedatabase.endTransaction();
        }
        sqlitedatabase.endTransaction();
        return;
        throw exception1;
    }

    public void onUpgrade(SQLiteDatabase sqlitedatabase, int i, int j)
    {
        try
        {
            sqlitedatabase.execSQL("drop table if exists config");
        }
        catch (Exception exception5) { }
        try
        {
            sqlitedatabase.execSQL("drop table if exists runtime");
        }
        catch (Exception exception4) { }
        try
        {
            sqlitedatabase.execSQL("drop table if exists message");
        }
        catch (Exception exception3) { }
        try
        {
            sqlitedatabase.execSQL("drop table if exists ral");
        }
        catch (Exception exception2) { }
        try
        {
            sqlitedatabase.execSQL("drop table if exists ca");
        }
        catch (Exception exception1) { }
        try
        {
            sqlitedatabase.execSQL("drop table if exists bi");
        }
        catch (Exception exception) { }
        onCreate(sqlitedatabase);
    }
}
