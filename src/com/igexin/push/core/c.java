// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core;


public final class c extends Enum
{

    public static final c a;
    public static final c b;
    public static final c c;
    public static final c d;
    public static final c e;
    public static final c f;
    public static final c g;
    private static final c h[];

    private c(String s, int i)
    {
        super(s, i);
    }

    public static c[] a()
    {
        return (c[])h.clone();
    }

    static 
    {
        a = new c("start", 0);
        b = new c("analyze", 1);
        c = new c("determine", 2);
        d = new c("connectASNL", 3);
        e = new c("check", 4);
        f = new c("retire", 5);
        g = new c("stop", 6);
        h = (new c[] {
            a, b, c, d, e, f, g
        });
    }
}
