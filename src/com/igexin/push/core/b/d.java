// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core.b;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import com.igexin.push.core.f;
import com.igexin.sdk.aidl.a;

// Referenced classes of package com.igexin.push.core.b:
//            b, a, c, e

class d
    implements ServiceConnection
{

    final c a;

    d(c c1)
    {
        a = c1;
        super();
    }

    public void onServiceConnected(ComponentName componentname, IBinder ibinder)
    {
        com.igexin.push.core.b.b.a().a(componentname.getPackageName()).a(com.igexin.sdk.aidl.a.a(ibinder));
        ibinder = new Message();
        Intent intent = new Intent();
        intent.putExtra("pkgname", componentname.getPackageName());
        String s = a.a(componentname.getPackageName());
        if (s != null)
        {
            if (com.igexin.push.core.b.e.a().a(componentname.getPackageName(), s))
            {
                intent.putExtra("action", "connected");
            } else
            {
                intent.putExtra("action", "disconnected");
            }
        } else
        {
            intent.putExtra("action", "disconnected");
        }
        ibinder.what = com.igexin.push.core.a.d;
        ibinder.obj = intent;
        f.a().a(ibinder);
    }

    public void onServiceDisconnected(ComponentName componentname)
    {
        com.igexin.push.core.b.b.a().a(componentname.getPackageName()).a(null);
    }
}
