// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core.b;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.RemoteException;
import com.igexin.a.b.a;
import com.igexin.push.core.g;
import com.igexin.sdk.aidl.ICACallback;
import java.util.List;

// Referenced classes of package com.igexin.push.core.b:
//            e, h, b, a, 
//            d, i

public class c
{

    private static c a;

    public c()
    {
    }

    public static c a()
    {
        if (a == null)
        {
            a = new c();
        }
        return a;
    }

    public int a(String s, i i)
    {
        boolean flag = false;
        if (s == null) goto _L2; else goto _L1
_L1:
        long l;
        s = com.igexin.push.core.b.e.a().a(s);
        l = Long.valueOf(s.c()).longValue();
        if (System.currentTimeMillis() <= l) goto _L4; else goto _L3
_L3:
        byte byte0 = -1;
_L5:
        return byte0;
_L4:
        s = s.e();
        int j = 0;
        do
        {
            if (j >= s.size())
            {
                break MISSING_BLOCK_LABEL_85;
            }
            byte0 = flag;
            if (s.get(j) == i)
            {
                break;
            }
            j++;
        } while (true);
        if (true) goto _L5; else goto _L2
_L2:
        return -1;
        return -2;
    }

    public String a(String s)
    {
        List list = g.i.getPackageManager().getInstalledPackages(64);
        for (int i = 0; i < list.size(); i++)
        {
            PackageInfo packageinfo = (PackageInfo)list.get(i);
            if (!packageinfo.packageName.equals(s))
            {
                continue;
            }
            Signature asignature[] = packageinfo.signatures;
            if (asignature != null && asignature.length > 0)
            {
                return asignature[0].toCharsString();
            }
        }

        return null;
    }

    public void a(Intent intent)
    {
        if (intent == null || !intent.hasExtra("action")) goto _L2; else goto _L1
_L1:
        Object obj = intent.getStringExtra("action");
        if (!((String) (obj)).equals("connected")) goto _L4; else goto _L3
_L3:
        String s = intent.getStringExtra("pkgname");
        obj = com.igexin.push.core.b.b.a().a(s);
        long l2 = System.currentTimeMillis();
        h h1 = com.igexin.push.core.b.e.a().a(s);
        intent = h1.b();
        long l1 = h1.c();
        long l = l1;
        if (l2 > l1)
        {
            intent = com.igexin.a.b.a.a((new StringBuilder()).append(s).append("-").append(l2).toString());
            l = 0x93a80L + l2;
            h1.b(intent);
            h1.a(l);
        }
        com.igexin.push.core.b.e.a().a(s, h1);
        try
        {
            if (((com.igexin.push.core.b.a) (obj)).b() != null)
            {
                ((com.igexin.push.core.b.a) (obj)).b().onAuthenticated(g.g, "com.igexin.sdk.PushService", intent, l);
            }
        }
        // Misplaced declaration of an exception variable
        catch (Intent intent) { }
        if (((com.igexin.push.core.b.a) (obj)).b() != null)
        {
            g.i.unbindService(((com.igexin.push.core.b.a) (obj)).a());
        }
_L2:
        return;
_L4:
        if (!((String) (obj)).equals("disconnected")) goto _L2; else goto _L5
_L5:
        intent = intent.getStringExtra("pkgname");
        intent = com.igexin.push.core.b.b.a().a(intent);
        if (intent.b() == null) goto _L2; else goto _L6
_L6:
        g.i.unbindService(intent.a());
        return;
        intent;
        return;
        intent;
    }

    public void b(Intent intent)
    {
        Object obj;
        String s;
        if (!intent.getStringExtra("action").equals("com.igexin.sdk.action.refreshls"))
        {
            break MISSING_BLOCK_LABEL_100;
        }
        obj = intent.getStringExtra("callback_pkgname");
        s = intent.getStringExtra("callback_classname");
        intent = new com.igexin.push.core.b.a();
        intent.a(((String) (obj)));
        intent.a(new d(this));
        com.igexin.push.core.b.b.a().a(((String) (obj)), intent);
        obj = g.i.createPackageContext(((String) (obj)), 3);
        obj = new Intent(((Context) (obj)), ((Context) (obj)).getClassLoader().loadClass(s));
        g.i.bindService(((Intent) (obj)), intent.a(), 1);
        return;
        intent;
    }
}
