// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core;


public final class d extends Enum
{

    public static final d a;
    public static final d b;
    public static final d c;
    public static final d d;
    private static final d e[];

    private d(String s, int i)
    {
        super(s, i);
    }

    public static d[] a()
    {
        return (d[])e.clone();
    }

    static 
    {
        a = new d("init", 0);
        b = new d("prepare", 1);
        c = new d("active", 2);
        d = new d("passive", 3);
        e = (new d[] {
            a, b, c, d
        });
    }
}
