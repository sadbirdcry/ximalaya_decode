// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core.bean;


public class BaseAction
{

    private String a;
    private String b;
    private String c;
    private boolean d;

    public BaseAction()
    {
        d = true;
    }

    public String getActionId()
    {
        return a;
    }

    public String getDoActionId()
    {
        return c;
    }

    public String getType()
    {
        return b;
    }

    public boolean isSupportExt()
    {
        return d;
    }

    public void setActionId(String s)
    {
        a = s;
    }

    public void setDoActionId(String s)
    {
        c = s;
    }

    public void setSupportExt(boolean flag)
    {
        d = flag;
    }

    public void setType(String s)
    {
        b = s;
    }
}
