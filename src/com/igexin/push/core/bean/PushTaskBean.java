// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core.bean;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.igexin.push.core.bean:
//            BaseAction

public class PushTaskBean
{

    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private List f;
    private byte g[];
    private String h;
    private String i;
    private int j;
    private int k;
    private boolean l;
    private boolean m;
    private boolean n;
    private Map o;
    private int p;

    public PushTaskBean()
    {
        l = false;
        m = false;
        n = false;
    }

    public String getAction()
    {
        return a;
    }

    public List getActionChains()
    {
        return f;
    }

    public String getAppKey()
    {
        return i;
    }

    public String getAppid()
    {
        return b;
    }

    public BaseAction getBaseAction(String s)
    {
        for (Iterator iterator = getActionChains().iterator(); iterator.hasNext();)
        {
            BaseAction baseaction = (BaseAction)iterator.next();
            if (baseaction.getActionId().equals(s))
            {
                return baseaction;
            }
        }

        return null;
    }

    public Map getConditionMap()
    {
        return o;
    }

    public int getCurrentActionid()
    {
        return j;
    }

    public String getId()
    {
        return c;
    }

    public String getMessageId()
    {
        return d;
    }

    public String getMsgAddress()
    {
        return h;
    }

    public byte[] getMsgExtra()
    {
        return g;
    }

    public int getPerActionid()
    {
        return k;
    }

    public int getStatus()
    {
        return p;
    }

    public String getTaskId()
    {
        return e;
    }

    public boolean isCDNType()
    {
        return n;
    }

    public boolean isHttpImg()
    {
        return l;
    }

    public boolean isStop()
    {
        return m;
    }

    public void setAction(String s)
    {
        a = s;
    }

    public void setActionChains(List list)
    {
        f = list;
    }

    public void setAppKey(String s)
    {
        i = s;
    }

    public void setAppid(String s)
    {
        b = s;
    }

    public void setCDNType(boolean flag)
    {
        n = flag;
    }

    public void setConditionMap(Map map)
    {
        o = map;
    }

    public void setCurrentActionid(int i1)
    {
        j = i1;
    }

    public void setHttpImg(boolean flag)
    {
        l = flag;
    }

    public void setId(String s)
    {
        c = s;
    }

    public void setMessageId(String s)
    {
        d = s;
    }

    public void setMsgAddress(String s)
    {
        h = s;
    }

    public void setMsgExtra(byte abyte0[])
    {
        g = abyte0;
    }

    public void setPerActionid(int i1)
    {
        k = i1;
    }

    public void setStatus(int i1)
    {
        p = i1;
    }

    public void setStop(boolean flag)
    {
        m = flag;
    }

    public void setTaskId(String s)
    {
        e = s;
    }
}
