// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core.stub;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Menu;
import com.igexin.push.core.e.a;
import com.igexin.push.core.e.b;
import com.igexin.push.core.f;
import com.igexin.push.core.o;
import com.igexin.sdk.IPushCore;
import java.util.HashMap;
import java.util.Map;

public class PushCore
    implements IPushCore
{

    private f a;
    private Map b;

    public PushCore()
    {
        b = new HashMap();
    }

    public void onActivityConfigurationChanged(Activity activity, Configuration configuration)
    {
        activity = (a)b.get(activity);
        if (activity != null)
        {
            activity.a(configuration);
        }
    }

    public boolean onActivityCreateOptionsMenu(Activity activity, Menu menu)
    {
        activity = (a)b.get(activity);
        if (activity != null)
        {
            return activity.a(menu);
        } else
        {
            return false;
        }
    }

    public void onActivityDestroy(Activity activity)
    {
        a a1 = (a)b.get(activity);
        if (a1 != null)
        {
            a1.h();
            b.remove(activity);
            com.igexin.push.core.e.b.a().c(a1);
        }
    }

    public boolean onActivityKeyDown(Activity activity, int i, KeyEvent keyevent)
    {
        activity = (a)b.get(activity);
        if (activity != null)
        {
            return activity.a(i, keyevent);
        } else
        {
            return false;
        }
    }

    public void onActivityNewIntent(Activity activity, Intent intent)
    {
        activity = (a)b.get(activity);
        if (activity != null)
        {
            activity.a(intent);
        }
    }

    public void onActivityPause(Activity activity)
    {
        activity = (a)b.get(activity);
        if (activity != null)
        {
            activity.f();
        }
    }

    public void onActivityRestart(Activity activity)
    {
        activity = (a)b.get(activity);
        if (activity != null)
        {
            activity.d();
        }
    }

    public void onActivityResume(Activity activity)
    {
        activity = (a)b.get(activity);
        if (activity != null)
        {
            activity.e();
        }
    }

    public void onActivityStart(Activity activity, Intent intent)
    {
label0:
        {
            if (activity != null && intent != null && intent.hasExtra("activityid"))
            {
                long l = intent.getLongExtra("activityid", 0L);
                intent = com.igexin.push.core.e.b.a().a(Long.valueOf(l));
                if (intent == null)
                {
                    break label0;
                }
                intent.a(activity);
                b.put(activity, intent);
                intent.c();
            }
            return;
        }
        activity.finish();
    }

    public void onActivityStop(Activity activity)
    {
        activity = (a)b.get(activity);
        if (activity != null)
        {
            activity.g();
        }
    }

    public IBinder onServiceBind(Intent intent)
    {
        return o.a();
    }

    public void onServiceDestroy()
    {
        a.j();
    }

    public int onServiceStartCommand(Intent intent, int i, int j)
    {
        if (a != null)
        {
            Message message = new Message();
            message.what = com.igexin.push.core.a.c;
            message.obj = intent;
            a.a(message);
        }
        return 1;
    }

    public boolean start(Context context)
    {
        a = f.a();
        a.a(context);
        return true;
    }
}
