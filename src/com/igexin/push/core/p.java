// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core;

import com.igexin.push.a.k;
import com.igexin.push.core.a.f;
import com.igexin.push.core.b.e;
import com.igexin.push.core.b.i;
import com.igexin.push.d.j;
import com.igexin.sdk.aidl.c;

// Referenced classes of package com.igexin.push.core:
//            g, f

final class p extends c
{

    p()
    {
    }

    public byte[] extFunction(byte abyte0[])
    {
        return null;
    }

    public int isStarted(String s)
    {
        s = e.a().b(s);
        int i1 = com.igexin.push.core.b.c.a().a(s, i.c);
        int l = i1;
        if (i1 == 0)
        {
            l = i1;
            if (!g.l)
            {
                l = 1;
            }
        }
        return l;
    }

    public int onASNLConnected(String s, String s1, String s2, long l)
    {
        if (f.a() != null)
        {
            return f.a().f().a(s2);
        } else
        {
            return -1;
        }
    }

    public int onASNLNetworkConnected()
    {
        if (f.a().e().a())
        {
            return -1;
        } else
        {
            f.a().e().b();
            return 0;
        }
    }

    public int onASNLNetworkDisconnected()
    {
        if (f.a().e().a())
        {
            return -1;
        } else
        {
            f.a().e().b(false);
            return 0;
        }
    }

    public int onPSNLConnected(String s, String s1, String s2, long l)
    {
        if (f.a() != null && !s.equals("") && !s1.equals(""))
        {
            return f.a().f().a(s, s1);
        } else
        {
            return -1;
        }
    }

    public int receiveToPSNL(String s, String s1, byte abyte0[])
    {
        while (s1 == null || abyte0 == null || f.a().e().a()) 
        {
            return -1;
        }
        return f.a().f().b(s, s1, abyte0);
    }

    public int sendByASNL(String s, String s1, byte abyte0[])
    {
        while (s1 == null || abyte0 == null || !f.a().e().a()) 
        {
            return -1;
        }
        return f.a().f().a(s, s1, abyte0);
    }

    public int setSilentTime(int l, int i1, String s)
    {
        s = e.a().b(s);
        int j1 = com.igexin.push.core.b.c.a().a(s, i.d);
        if (j1 == 0 && k.o)
        {
            f.a().a(l, i1, s);
        }
        return j1;
    }

    public int startService(String s)
    {
        s = e.a().b(s);
        int l = com.igexin.push.core.b.c.a().a(s, i.a);
        if (l == 0)
        {
            f.a().a(true);
            g.E = s;
        }
        return l;
    }

    public int stopService(String s)
    {
        s = e.a().b(s);
        int l = com.igexin.push.core.b.c.a().a(s, i.b);
        if (l == 0)
        {
            f.a().a(s);
        }
        return l;
    }
}
