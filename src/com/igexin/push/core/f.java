// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import com.igexin.a.a.b.d;
import com.igexin.a.a.c.a;
import com.igexin.a.a.d.a.c;
import com.igexin.push.a.i;
import com.igexin.push.core.b.e;
import com.igexin.push.d.j;
import com.igexin.push.e.b.g;
import com.igexin.push.e.b.h;
import com.igexin.sdk.PushService;
import com.igexin.sdk.a.b;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

// Referenced classes of package com.igexin.push.core:
//            h, a, g, n, 
//            m, c, e

public class f
    implements c
{

    private static f l;
    private Context a;
    private com.igexin.push.core.h b;
    private Handler c;
    private ConcurrentLinkedQueue d;
    private com.igexin.push.core.a.f e;
    private ConnectivityManager f;
    private d g;
    private com.igexin.a.a.b.c h;
    private j i;
    private com.igexin.push.d.c j;
    private com.igexin.push.b.b k;

    private f()
    {
        d = new ConcurrentLinkedQueue();
        b = new com.igexin.push.core.h();
    }

    public static f a()
    {
        if (l == null)
        {
            l = new f();
        }
        return l;
    }

    private void n()
    {
        String s;
        Object obj;
        s = a.getPackageName();
        obj = a.getPackageManager().getInstalledPackages(4);
        if (obj == null) goto _L2; else goto _L1
_L1:
        obj = ((List) (obj)).iterator();
_L4:
        if (((Iterator) (obj)).hasNext())
        {
            Object obj1 = (PackageInfo)((Iterator) (obj)).next();
            if ((((PackageInfo) (obj1)).applicationInfo.flags & 1) != 0 && (((PackageInfo) (obj1)).applicationInfo.flags & 0x80) != 1)
            {
                continue; /* Loop/switch isn't completed */
            }
            ServiceInfo aserviceinfo[] = ((PackageInfo) (obj1)).services;
            if (aserviceinfo == null)
            {
                continue; /* Loop/switch isn't completed */
            }
            int j1 = aserviceinfo.length;
            int i1 = 0;
            do
            {
                if (i1 >= j1)
                {
                    continue; /* Loop/switch isn't completed */
                }
                ServiceInfo serviceinfo = aserviceinfo[i1];
                if (a.o.equals(serviceinfo.name) || a.n.equals(serviceinfo.name) || a.p.equals(serviceinfo.name))
                {
                    obj1 = ((PackageInfo) (obj1)).packageName;
                    if (!s.equals(obj1))
                    {
                        com.igexin.push.core.c.f.a().c().put(obj1, serviceinfo.name);
                    }
                    continue; /* Loop/switch isn't completed */
                }
                i1++;
            } while (true);
        }
_L2:
        return;
        if (true) goto _L4; else goto _L3
_L3:
    }

    private boolean o()
    {
        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        intentfilter.addAction("com.igexin.sdk.action.snlrefresh");
        intentfilter.addAction("com.igexin.sdk.action.snlretire");
        intentfilter.addAction(g.W);
        intentfilter.addAction("com.igexin.sdk.action.execute");
        intentfilter.addAction("com.igexin.sdk.action.doaction");
        intentfilter.addAction("android.intent.action.TIME_SET");
        intentfilter.addAction("android.intent.action.SCREEN_ON");
        intentfilter.addAction("android.intent.action.SCREEN_OFF");
        if (a.registerReceiver(com.igexin.push.core.n.a(), intentfilter) == null)
        {
            com.igexin.a.a.c.a.a("CoreLogic|InternalPublicReceiver|Failed");
        }
        intentfilter = new IntentFilter();
        intentfilter.addDataScheme("package");
        intentfilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentfilter.addAction("android.intent.action.PACKAGE_REMOVED");
        if (a.registerReceiver(com.igexin.push.core.m.a(), intentfilter) == null)
        {
            com.igexin.a.a.c.a.a("CoreLogic|InternalPackageReceiver|Failed");
        }
        return true;
    }

    public void a(com.igexin.push.core.e e1)
    {
        c = e1;
    }

    public boolean a(Context context)
    {
        a = context;
        b.start();
        return true;
    }

    public boolean a(Intent intent)
    {
        if (com.igexin.push.core.g.i != null)
        {
            com.igexin.push.core.g.i.sendBroadcast(intent);
            return true;
        } else
        {
            return false;
        }
    }

    public boolean a(Message message)
    {
        if (com.igexin.push.core.g.j.get())
        {
            c.sendMessage(message);
            return true;
        } else
        {
            d.add(message);
            return true;
        }
    }

    public boolean a(com.igexin.a.a.d.a.f f1, com.igexin.a.a.d.e e1)
    {
        if (e != null)
        {
            return e.a(f1);
        } else
        {
            return false;
        }
    }

    public boolean a(com.igexin.a.a.d.d d1, com.igexin.a.a.d.e e1)
    {
        if (e != null)
        {
            return e.a(d1);
        } else
        {
            return false;
        }
    }

    public boolean a(h h1)
    {
        boolean flag = false;
        if (h1 != null)
        {
            flag = com.igexin.a.a.b.d.c().a(h1, false, true);
        }
        return flag;
    }

    public boolean a(String s)
    {
        s = com.igexin.push.core.a.f.a().g("ss");
        if (com.igexin.push.core.g.i == null || j == null) goto _L2; else goto _L1
_L1:
        (new com.igexin.sdk.a.d(com.igexin.push.core.g.i)).b();
        g.l = false;
        g.p = false;
        com.igexin.push.d.a a1 = new com.igexin.push.d.a();
        a1.a(com.igexin.push.core.c.g);
        j.a(a1);
        if (s == null || !"1".equals(s)) goto _L2; else goto _L3
_L3:
        s = Runtime.getRuntime().exec("ps").getInputStream();
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_217;
        }
        String s1;
        BufferedReader bufferedreader;
        ArrayList arraylist;
        s1 = com.igexin.push.core.g.i.getPackageName();
        bufferedreader = new BufferedReader(new InputStreamReader(s));
        arraylist = new ArrayList();
_L5:
        String s2 = bufferedreader.readLine();
        if (s2 == null)
        {
            break; /* Loop/switch isn't completed */
        }
        String as[] = s2.split("\\s+");
        arraylist.add(as);
        if (s2.indexOf((new StringBuilder()).append(s1).append("/files/gdaemon").toString()) == -1 || as.length <= 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        Process.killProcess(Integer.valueOf(as[1]).intValue());
        break; /* Loop/switch isn't completed */
        if (true) goto _L5; else goto _L4
_L4:
        bufferedreader.close();
        s.close();
_L7:
        c();
_L2:
        return true;
        s;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public boolean a(boolean flag)
    {
        if (com.igexin.push.core.g.i != null && j != null)
        {
            (new com.igexin.sdk.a.d(com.igexin.push.core.g.i)).a();
            g.l = true;
            if (!(new b(com.igexin.push.core.g.i)).b())
            {
                (new com.igexin.sdk.a.c(com.igexin.push.core.g.i)).a();
                g.m = true;
                (new b(com.igexin.push.core.g.i)).a();
            }
            if (flag)
            {
                (new com.igexin.sdk.a.c(com.igexin.push.core.g.i)).a();
                g.m = true;
            }
            com.igexin.push.d.a a1 = new com.igexin.push.d.a();
            a1.a(com.igexin.push.core.c.a);
            j.a(a1);
        }
        return true;
    }

    public void b()
    {
        Object obj1;
        try
        {
            f = (ConnectivityManager)a.getSystemService("connectivity");
            com.igexin.push.core.g.a(a);
            k = new com.igexin.push.b.b(a);
            com.igexin.push.a.i.a().b();
            o();
            g = com.igexin.a.a.b.d.c();
            Object obj = new com.igexin.push.c.a(a, h());
            g.a(((com.igexin.a.a.d.a.b) (obj)));
            g.a(this);
            g.a(a);
            obj = new com.igexin.push.b.a();
            ((com.igexin.push.b.a) (obj)).a(com.igexin.push.core.c.f.a());
            ((com.igexin.push.b.a) (obj)).a(com.igexin.push.core.c.c.a());
            ((com.igexin.push.b.a) (obj)).a(com.igexin.push.core.c.b.a());
            ((com.igexin.push.b.a) (obj)).a(com.igexin.push.core.b.e.a());
            ((com.igexin.push.b.a) (obj)).a(com.igexin.push.a.a.a());
            g.a(((com.igexin.a.a.d.d) (obj)), true, false);
            com.igexin.a.a.b.d.c().a(com.igexin.a.b.a.a(g.C.getBytes()));
            g.af = g.a(com.igexin.push.e.b.d.g(), false, true);
            g.ag = g.a(com.igexin.push.e.b.f.g(), true, true);
            g.ah = g.a(com.igexin.push.e.b.e.g(), false, true);
            g.ai = g.a(com.igexin.push.e.b.g.g(), false, true);
            g.aj = g.a(com.igexin.push.e.b.a.g(), false, true);
            g.ak = g.a(com.igexin.push.e.b.b.g(), false, true);
            e = com.igexin.push.core.a.f.a();
        }
        catch (Exception exception)
        {
            com.igexin.a.a.c.a.a("CoreLogic|init|failed");
            return;
        }
        if (!e.E())
        {
            break MISSING_BLOCK_LABEL_307;
        }
        obj1 = ((WifiManager)a.getSystemService("wifi")).getConnectionInfo();
        if (obj1 != null)
        {
            try
            {
                g.z = ((WifiInfo) (obj1)).getMacAddress();
            }
            catch (Exception exception1) { }
        }
        i = new j();
        i.a(a, g, e);
        j = new com.igexin.push.d.c();
        j.a(a);
        obj1 = new com.igexin.push.d.a();
        ((com.igexin.push.d.a) (obj1)).a(com.igexin.push.core.c.a);
        j.a(((com.igexin.push.d.a) (obj1)));
        com.igexin.push.e.b.g.g().h();
        com.igexin.push.core.g.j.set(true);
        obj1 = d.iterator();
        do
        {
            if (!((Iterator) (obj1)).hasNext())
            {
                break;
            }
            Message message = (Message)((Iterator) (obj1)).next();
            if (c != null)
            {
                c.sendMessage(message);
            }
        } while (true);
        com.igexin.push.core.a.f.a().u();
        int i1 = Process.myPid();
        e.a(i1);
        n();
        com.igexin.push.extension.a.a().a(a);
        return;
    }

    public boolean b(String s)
    {
        if (com.igexin.push.core.g.i != null && j != null)
        {
            (new com.igexin.sdk.a.c(com.igexin.push.core.g.i)).b();
            g.m = false;
            g.p = false;
            s = new com.igexin.push.d.a();
            s.a(com.igexin.push.core.c.g);
            j.a(s);
        }
        return true;
    }

    public void c()
    {
        Intent intent = new Intent(a, com/igexin/sdk/PushService);
        a.stopService(intent);
    }

    public com.igexin.a.a.b.c d()
    {
        if (h == null)
        {
            h = com.igexin.push.c.a.c.a();
        }
        return h;
    }

    public j e()
    {
        return i;
    }

    public com.igexin.push.d.c f()
    {
        return j;
    }

    public com.igexin.push.core.a.f g()
    {
        return e;
    }

    public ConnectivityManager h()
    {
        return f;
    }

    public com.igexin.push.b.b i()
    {
        return k;
    }

    public void j()
    {
        try
        {
            a.unregisterReceiver(com.igexin.push.core.m.a());
            a.unregisterReceiver(com.igexin.push.core.n.a());
            a.unregisterReceiver(com.igexin.a.a.b.d.c());
        }
        catch (Exception exception) { }
        com.igexin.push.extension.a.a().b();
    }

    public String k()
    {
        NetworkInfo networkinfo;
        if (f != null)
        {
            if ((networkinfo = f.getActiveNetworkInfo()) != null)
            {
                if (networkinfo.getType() == 1)
                {
                    return "wifi";
                }
                if (networkinfo.getType() == 0)
                {
                    return "mobile";
                }
            }
        }
        return null;
    }

    public boolean l()
    {
        return true;
    }

    public long m()
    {
        return 0x17258L;
    }
}
