// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core.c;


final class t extends Enum
{

    public static final t a;
    public static final t b;
    public static final t c;
    private static final t d[];

    private t(String s, int i)
    {
        super(s, i);
    }

    public static t[] a()
    {
        return (t[])d.clone();
    }

    static 
    {
        a = new t("NORMAL", 0);
        b = new t("BACKUP", 1);
        c = new t("TRY_NORMAL", 2);
        d = (new t[] {
            a, b, c
        });
    }
}
