// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core.d;

import android.content.Context;
import android.content.Intent;
import android.os.Process;
import com.igexin.a.b.a;
import com.igexin.push.core.bean.e;
import com.igexin.push.core.g;
import com.igexin.sdk.PushService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class f
    implements Runnable
{

    private Context a;
    private e b;
    private int c;

    public f(Context context, e e1)
    {
        c = 0;
        a = context;
        b = e1;
    }

    protected boolean a(String s, String s1, String s2)
    {
        Object obj;
        if (s1 == null || s1.length() == 0)
        {
            return false;
        }
        if (s == null)
        {
            c = 3;
            com.igexin.push.core.a.f.a().f("url is null");
            return false;
        }
        if (!s.startsWith("http://") && !s.startsWith("https://"))
        {
            c = 3;
            com.igexin.push.core.a.f.a().f((new StringBuilder()).append("httpUrl:").append(s).append(" is not a valid url...").toString());
            return false;
        }
        Process.setThreadPriority(10);
        obj = new DefaultHttpClient();
        FileOutputStream fileoutputstream;
        byte abyte0[];
        s = ((HttpClient) (obj)).execute(new HttpGet(s));
        if (s.getStatusLine().getStatusCode() != 200)
        {
            break MISSING_BLOCK_LABEL_481;
        }
        s = s.getEntity().getContent();
        obj = (new StringBuilder()).append(g.ad).append("/").append(s1).toString();
        s1 = new File(((String) (obj)));
        fileoutputstream = new FileOutputStream(s1);
        abyte0 = new byte[1024];
_L1:
        int i = s.read(abyte0);
label0:
        {
            if (i == -1)
            {
                break label0;
            }
            try
            {
                fileoutputstream.write(abyte0, 0, i);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                c = 3;
                com.igexin.push.core.a.f.a().f(s.toString());
                return false;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                c = c + 1;
                return false;
            }
        }
          goto _L1
        fileoutputstream.close();
        s.close();
        if (!com.igexin.a.b.a.a(a, ((String) (obj))).equals(s2))
        {
            break MISSING_BLOCK_LABEL_462;
        }
        s = new File((new StringBuilder()).append(g.ad).append("/").append(b.c()).toString());
        s1.renameTo(s);
        if (b.g() || b.h() != 0L)
        {
            break MISSING_BLOCK_LABEL_493;
        }
        s1 = new File((new StringBuilder()).append(g.ac).append("/").append(b.c()).toString());
        if (s1.exists())
        {
            if (!com.igexin.a.b.a.a(g.i, s1.getAbsolutePath()).equals(b.f()))
            {
                s1.delete();
                com.igexin.push.core.a.f.a().a(s, s1, b.f());
            }
            break MISSING_BLOCK_LABEL_493;
        }
        com.igexin.push.core.a.f.a().a(s, s1, b.f());
        break MISSING_BLOCK_LABEL_493;
        if (s1.exists())
        {
            s1.delete();
        }
        c = 4;
        return false;
        c = c + 1;
        return false;
        return true;
    }

    public void run()
    {
        do
        {
            if (a(b.e(), (new StringBuilder()).append(b.c()).append(".tmp").toString(), b.f()))
            {
                Intent intent = new Intent(a, com/igexin/sdk/PushService);
                intent.putExtra("action", "com.igexin.sdk.action.extdownloadsuccess");
                intent.putExtra("id", b.a());
                intent.putExtra("result", true);
                a.startService(intent);
                return;
            }
        } while (c < 3);
        Intent intent1 = new Intent(a, com/igexin/sdk/PushService);
        intent1.putExtra("action", "com.igexin.sdk.action.extdownloadsuccess");
        intent1.putExtra("id", b.a());
        intent1.putExtra("result", false);
        a.startService(intent1);
    }
}
