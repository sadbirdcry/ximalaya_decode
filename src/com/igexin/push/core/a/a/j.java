// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core.a.a;

import android.content.Context;
import android.content.pm.PackageManager;
import com.igexin.push.core.a.f;
import com.igexin.push.core.b;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.g;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.igexin.push.core.a.a:
//            a

public class j
    implements a
{

    private static final String a;

    public j()
    {
    }

    public b a(PushTaskBean pushtaskbean, BaseAction baseaction)
    {
        return b.a;
    }

    public BaseAction a(JSONObject jsonobject)
    {
        com.igexin.push.core.bean.j j1;
        try
        {
            j1 = new com.igexin.push.core.bean.j();
            j1.setType("startapp");
            j1.setActionId(jsonobject.getString("actionid"));
            j1.setDoActionId(jsonobject.getString("do"));
            if (jsonobject.has("appstartupid"))
            {
                j1.a(jsonobject.getJSONObject("appstartupid").getString("android"));
            }
            if (jsonobject.has("is_autostart"))
            {
                j1.d(jsonobject.getString("is_autostart"));
            }
            if (jsonobject.has("appid"))
            {
                j1.b(jsonobject.getString("appid"));
            }
            if (jsonobject.has("noinstall_action"))
            {
                j1.c(jsonobject.getString("noinstall_action"));
            }
        }
        // Misplaced declaration of an exception variable
        catch (JSONObject jsonobject)
        {
            return null;
        }
        return j1;
    }

    public boolean b(PushTaskBean pushtaskbean, BaseAction baseaction)
    {
        boolean flag1 = false;
        if (pushtaskbean == null || baseaction == null) goto _L2; else goto _L1
_L1:
        com.igexin.push.core.bean.j j1 = (com.igexin.push.core.bean.j)baseaction;
        PackageManager packagemanager = g.i.getPackageManager();
        String s = j1.b();
        boolean flag;
        if (s.equals(""))
        {
            s = g.c;
            flag = true;
        } else
        if (g.c.equals(j1.b()))
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (!flag) goto _L4; else goto _L3
_L3:
        f.a().b(pushtaskbean.getTaskId(), pushtaskbean.getMessageId(), s, null);
        if (!((com.igexin.push.core.bean.j)baseaction).d().equals("true"))
        {
            break MISSING_BLOCK_LABEL_125;
        }
        baseaction = packagemanager.getLaunchIntentForPackage(g.g);
        if (baseaction == null)
        {
            return false;
        }
        g.i.startActivity(baseaction);
        if (j1.getDoActionId() != null)
        {
            f.a().a(pushtaskbean.getTaskId(), pushtaskbean.getMessageId(), j1.getDoActionId());
        }
          goto _L2
_L4:
        f.a().b(pushtaskbean.getTaskId(), pushtaskbean.getMessageId(), s, null);
        if (!j1.d().equals("true")) goto _L6; else goto _L5
_L5:
        flag = flag1;
        if (!f.a().d(j1.a()))
        {
            break MISSING_BLOCK_LABEL_232;
        }
        baseaction = packagemanager.getLaunchIntentForPackage(((com.igexin.push.core.bean.j)baseaction).a());
        if (baseaction == null)
        {
            return false;
        }
        g.i.startActivity(baseaction);
        flag = true;
_L7:
        if (flag)
        {
            try
            {
                if (j1.getDoActionId() != null)
                {
                    f.a().a(pushtaskbean.getTaskId(), pushtaskbean.getMessageId(), j1.getDoActionId());
                }
            }
            // Misplaced declaration of an exception variable
            catch (PushTaskBean pushtaskbean) { }
            break; /* Loop/switch isn't completed */
        }
        if (j1.c() != null)
        {
            f.a().a(pushtaskbean.getTaskId(), pushtaskbean.getMessageId(), j1.c());
        }
        break; /* Loop/switch isn't completed */
_L6:
        flag = true;
        if (true) goto _L7; else goto _L2
_L2:
        return true;
    }

    static 
    {
        a = com.igexin.push.a.j.a;
    }
}
