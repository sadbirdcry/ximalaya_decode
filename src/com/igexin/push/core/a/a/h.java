// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core.a.a;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import com.igexin.push.a.j;
import com.igexin.push.core.b;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.g;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.igexin.push.core.a.a:
//            a

public class h
    implements a
{

    public static HashMap a = new HashMap();
    private static final String b;

    public h()
    {
    }

    private PendingIntent a(String s, String s1, String s2, int i)
    {
        Intent intent = new Intent("com.igexin.sdk.action.doaction");
        intent.putExtra("taskid", s);
        intent.putExtra("messageid", s1);
        intent.putExtra("appid", g.c);
        intent.putExtra("actionid", s2);
        intent.putExtra("accesstoken", g.ax);
        intent.putExtra("notifID", i);
        s = new Random();
        return PendingIntent.getBroadcast(g.i, s.nextInt(1000), intent, 0x8000000);
    }

    public b a(PushTaskBean pushtaskbean, BaseAction baseaction)
    {
        return b.a;
    }

    public BaseAction a(JSONObject jsonobject)
    {
        com.igexin.push.core.bean.h h1;
        h1 = new com.igexin.push.core.bean.h();
        h1.setType("notification");
        h1.setActionId(jsonobject.getString("actionid"));
        h1.setDoActionId(jsonobject.getString("do"));
        String s = jsonobject.getString("title");
        String s2 = jsonobject.getString("text");
        h1.a(s);
        h1.b(s2);
        if (!jsonobject.has("logo") || "".equals(jsonobject.getString("logo"))) goto _L2; else goto _L1
_L1:
        String s3;
        int k;
        s3 = jsonobject.getString("logo");
        if (s3.lastIndexOf(".png") == -1 && s3.lastIndexOf(".jpeg") == -1)
        {
            break MISSING_BLOCK_LABEL_285;
        }
        k = s3.indexOf(".png");
        int i = k;
        String s1;
        if (k == -1)
        {
            try
            {
                i = s3.indexOf(".jpeg");
            }
            // Misplaced declaration of an exception variable
            catch (JSONObject jsonobject)
            {
                return null;
            }
        }
        s1 = s3;
        if (i == -1)
        {
            break MISSING_BLOCK_LABEL_156;
        }
        s1 = s3.substring(0, i);
_L3:
        h1.c(s1);
_L2:
        if (jsonobject.has("is_noclear"))
        {
            h1.a(jsonobject.getBoolean("is_noclear"));
        }
        if (jsonobject.has("is_novibrate"))
        {
            h1.b(jsonobject.getBoolean("is_novibrate"));
        }
        if (jsonobject.has("is_noring"))
        {
            h1.c(jsonobject.getBoolean("is_noring"));
        }
        if (jsonobject.has("is_chklayout"))
        {
            h1.d(jsonobject.getBoolean("is_chklayout"));
        }
        if (jsonobject.has("logo_url"))
        {
            h1.d(jsonobject.getString("logo_url"));
        }
        if (jsonobject.has("banner_url"))
        {
            h1.e(jsonobject.getString("banner_url"));
        }
        break MISSING_BLOCK_LABEL_294;
        s1 = "null";
          goto _L3
        return h1;
    }

    public void a(String s, String s1, com.igexin.push.core.bean.h h1)
    {
        int i = (int)System.currentTimeMillis();
        g.am.put(s, Integer.valueOf(i));
        s = a(s, s1, h1.getDoActionId(), i);
        s1 = (NotificationManager)g.i.getSystemService("notification");
        Notification notification = new Notification();
        notification.tickerText = h1.b();
        notification.defaults = 4;
        notification.ledARGB = 0xff00ff00;
        notification.ledOnMS = 1000;
        notification.ledOffMS = 3000;
        notification.flags = 1;
        int k;
        if (!h1.c())
        {
            notification.flags = notification.flags | 0x10;
        } else
        {
            notification.flags = notification.flags | 0x20;
        }
        if (!h1.e())
        {
            notification.defaults = notification.defaults | 1;
        }
        if (!h1.d())
        {
            notification.defaults = notification.defaults | 2;
        }
        k = g.i.getResources().getIdentifier("push", "drawable", g.g);
        if (h1.f() == null)
        {
            if (k != 0)
            {
                notification.icon = k;
            } else
            {
                notification.icon = 0x1080093;
            }
        } else
        if ("null".equals(h1.f()))
        {
            notification.icon = 0x1080093;
        } else
        if (h1.f().startsWith("@"))
        {
            String s2 = h1.f();
            if (s2.substring(1, s2.length()).endsWith("email"))
            {
                notification.icon = 0x108008f;
            } else
            {
                notification.icon = 0x1080093;
            }
        } else
        {
            int l = g.i.getResources().getIdentifier(h1.f(), "drawable", g.g);
            if (l != 0)
            {
                notification.icon = l;
            } else
            {
                notification.icon = 0x1080093;
            }
        }
        if ((h1.h() != null || h1.g() != null) && h1.i())
        {
            return;
        } else
        {
            notification.setLatestEventInfo(g.i, h1.a(), h1.b(), s);
            s1.notify(i, notification);
            return;
        }
    }

    public boolean b(PushTaskBean pushtaskbean, BaseAction baseaction)
    {
        if (pushtaskbean != null && baseaction != null && (baseaction instanceof com.igexin.push.core.bean.h))
        {
            a(pushtaskbean.getTaskId(), pushtaskbean.getMessageId(), (com.igexin.push.core.bean.h)baseaction);
        }
        return true;
    }

    static 
    {
        b = j.a;
    }
}
