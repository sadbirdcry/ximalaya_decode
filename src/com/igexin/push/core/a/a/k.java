// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core.a.a;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import com.igexin.push.core.a;
import com.igexin.push.core.a.f;
import com.igexin.push.core.b;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.bean.m;
import com.igexin.push.core.g;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.igexin.push.core.a.a:
//            a

public class k
    implements com.igexin.push.core.a.a.a
{

    public k()
    {
    }

    private String a(String s)
    {
        Object obj = g.i.getPackageManager().getInstalledPackages(4);
        if (obj != null)
        {
            obj = ((List) (obj)).iterator();
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break;
                }
                PackageInfo packageinfo = (PackageInfo)((Iterator) (obj)).next();
                if (s.equals(packageinfo.packageName))
                {
                    ServiceInfo aserviceinfo[] = packageinfo.services;
                    int j = aserviceinfo.length;
                    int i = 0;
                    while (i < j) 
                    {
                        ServiceInfo serviceinfo = aserviceinfo[i];
                        if (a.n.equals(serviceinfo.name) || a.o.equals(serviceinfo.name) || a.p.equals(serviceinfo.name))
                        {
                            return serviceinfo.name;
                        }
                        i++;
                    }
                }
            } while (true);
        }
        return null;
    }

    public b a(PushTaskBean pushtaskbean, BaseAction baseaction)
    {
        return b.a;
    }

    public BaseAction a(JSONObject jsonobject)
    {
        m m1;
        if (!com.igexin.push.a.k.v || !jsonobject.has("do") || !jsonobject.has("actionid") || !jsonobject.has("type") || !jsonobject.has("pkgname"))
        {
            break MISSING_BLOCK_LABEL_109;
        }
        m1 = new m();
        m1.setType("wakeupsdk");
        m1.setActionId(jsonobject.getString("actionid"));
        m1.setDoActionId(jsonobject.getString("do"));
        m1.a(jsonobject.getString("pkgname"));
        if (jsonobject.has("is_forcestart"))
        {
            m1.a(jsonobject.getBoolean("is_forcestart"));
        }
        return m1;
        jsonobject;
        return null;
    }

    public boolean b(PushTaskBean pushtaskbean, BaseAction baseaction)
    {
        if (pushtaskbean == null || baseaction == null) goto _L2; else goto _L1
_L1:
        String s;
        String s1;
        m m1;
        m1 = (m)baseaction;
        s = m1.b();
        s1 = a(s);
        if (s1 == null) goto _L4; else goto _L3
_L3:
        if (!s1.equals(a.n)) goto _L6; else goto _L5
_L5:
        if (m1.a())
        {
            Intent intent = new Intent();
            intent.setClassName(s, s1);
            intent.putExtra("action", "com.igexin.action.initialize.slave");
            intent.putExtra("op_app", g.g);
            intent.putExtra("isSlave", true);
            g.i.startService(intent);
        } else
        {
            Intent intent1 = new Intent();
            intent1.setClassName(s, s1);
            g.i.startService(intent1);
        }
_L4:
        if (!baseaction.getDoActionId().equals(""))
        {
            f.a().a(pushtaskbean.getTaskId(), pushtaskbean.getMessageId(), baseaction.getDoActionId());
        }
_L2:
        return true;
_L6:
        if (s1.equals(a.o) || s1.equals(a.p))
        {
            Intent intent2 = new Intent();
            intent2.setClassName(s, s1);
            g.i.startService(intent2);
        }
        if (true) goto _L4; else goto _L7
_L7:
    }
}
