// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core.a.a;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.igexin.push.a.j;
import com.igexin.push.core.a.f;
import com.igexin.push.core.b;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.bean.k;
import com.igexin.push.core.g;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.igexin.push.core.a.a:
//            a

public class i
    implements a
{

    private static final String a;

    public i()
    {
    }

    private void a(k k1, String s)
    {
        String s5 = k1.a();
        if (s5 != null) goto _L2; else goto _L1
_L1:
        int l;
        return;
_L2:
        if ((l = s5.indexOf(s)) == -1) goto _L1; else goto _L3
_L3:
        String s1;
        int i1;
        s1 = "";
        s = null;
        i1 = s5.indexOf("&");
        if (i1 != -1) goto _L5; else goto _L4
_L4:
        String s2 = s5.substring(0, l - 1);
        s5 = s5.substring(l);
        s1 = s2;
        if (s5.indexOf("=") != -1)
        {
            s = s5.substring(s5.indexOf("=") + 1);
            s1 = s2;
        }
_L7:
        k1.a(s1);
        k1.b(s);
        return;
_L5:
        if (s5.charAt(l - 1) == '?')
        {
            s1 = s5.substring(0, l);
            String s3 = s5.substring(i1 + 1);
            s3 = (new StringBuilder()).append(s1).append(s3).toString();
            s5 = s5.substring(l, i1);
            s1 = s3;
            if (s5.indexOf("=") != -1)
            {
                s = s5.substring(s5.indexOf("=") + 1);
                s1 = s3;
            }
        } else
        if (s5.charAt(l - 1) == '&')
        {
            String s4 = s5.substring(0, l - 1);
            s = s5.substring(l);
            s1 = "";
            l = s.indexOf("&");
            if (l != -1)
            {
                s1 = s.substring(l);
                s = s.substring(0, l);
                s = s.substring(s.indexOf("=") + 1);
            } else
            {
                s = s.substring(s.indexOf("=") + 1);
            }
            s1 = (new StringBuilder()).append(s4).append(s1).toString();
        }
        if (true) goto _L7; else goto _L6
_L6:
    }

    public b a(PushTaskBean pushtaskbean, BaseAction baseaction)
    {
        return b.a;
    }

    public BaseAction a(JSONObject jsonobject)
    {
        k k1;
        if (!jsonobject.has("url") || !jsonobject.has("do") || !jsonobject.has("actionid"))
        {
            break MISSING_BLOCK_LABEL_141;
        }
        String s = jsonobject.getString("url");
        if (s.equals(""))
        {
            break MISSING_BLOCK_LABEL_141;
        }
        k1 = new k();
        k1.setType("startweb");
        k1.setActionId(jsonobject.getString("actionid"));
        k1.setDoActionId(jsonobject.getString("do"));
        k1.a(s);
        if (jsonobject.has("is_withcid") && jsonobject.getString("is_withcid").equals("true"))
        {
            k1.a(true);
        }
        if (jsonobject.has("is_withnettype") && jsonobject.getString("is_withnettype").equals("true"))
        {
            k1.b(true);
        }
        return k1;
        jsonobject;
        return null;
    }

    public boolean b(PushTaskBean pushtaskbean, BaseAction baseaction)
    {
        k k1 = (k)baseaction;
        a(k1, "targetpkgname");
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setFlags(0x10000000);
        intent.setPackage(k1.b());
        intent.setData(Uri.parse(k1.c()));
        try
        {
            g.i.startActivity(intent);
        }
        catch (Exception exception) { }
        if (!baseaction.getDoActionId().equals(""))
        {
            f.a().a(pushtaskbean.getTaskId(), pushtaskbean.getMessageId(), baseaction.getDoActionId());
        }
        return true;
    }

    static 
    {
        a = j.a;
    }
}
