// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core.a;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import android.util.Base64;
import android.util.Log;
import com.igexin.a.b.a;
import com.igexin.push.core.a.a.b;
import com.igexin.push.core.a.a.c;
import com.igexin.push.core.a.a.d;
import com.igexin.push.core.a.a.e;
import com.igexin.push.core.a.a.g;
import com.igexin.push.core.a.a.h;
import com.igexin.push.core.a.a.i;
import com.igexin.push.core.a.a.j;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.bean.l;
import com.igexin.push.core.c.r;
import com.igexin.push.d.k;
import com.igexin.push.extension.stub.IPushExtension;
import com.igexin.sdk.PushService;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.UnresolvedAddressException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.igexin.push.core.a:
//            a, i, j, l, 
//            p, h, e, g

public class f extends com.igexin.push.core.a.a
    implements k
{

    private static Map a;
    private static Map b;
    private static f c;

    private f()
    {
        a = new HashMap();
        a.put(Integer.valueOf(0), new com.igexin.push.core.a.i());
        a.put(Integer.valueOf(5), new com.igexin.push.core.a.j());
        a.put(Integer.valueOf(37), new com.igexin.push.core.a.l());
        a.put(Integer.valueOf(9), new p());
        a.put(Integer.valueOf(26), new com.igexin.push.core.a.h());
        a.put(Integer.valueOf(28), new com.igexin.push.core.a.e());
        b = new HashMap();
        b.put("goto", new g());
        b.put("notification", new h());
        b.put("startapp", new j());
        b.put("null", new com.igexin.push.core.a.a.f());
        b.put("wakeupsdk", new com.igexin.push.core.a.a.k());
        b.put("startweb", new i());
        b.put("checkapp", new b());
        b.put("cleanext", new c());
        b.put("enablelog", new e());
        b.put("disablelog", new d());
    }

    private int F()
    {
        if (com.igexin.push.core.g.al.isEmpty() && com.igexin.push.core.g.q)
        {
            Cursor cursor = com.igexin.push.core.f.a().i().a("message", new String[] {
                "status"
            }, new String[] {
                "0"
            }, null, null);
            if (cursor != null)
            {
                while (cursor.moveToNext()) 
                {
                    byte abyte0[] = cursor.getBlob(cursor.getColumnIndex("info"));
                    try
                    {
                        JSONObject jsonobject = new JSONObject(new String(com.igexin.a.b.a.c(abyte0)));
                        String s1 = jsonobject.getString("id");
                        String s2 = jsonobject.getString("appid");
                        String s3 = jsonobject.getString("messageid");
                        String s4 = jsonobject.getString("taskid");
                        String s5 = jsonobject.getString("appkey");
                        String s6 = a().a(s4, s3);
                        PushTaskBean pushtaskbean = new PushTaskBean();
                        pushtaskbean.setAppid(s2);
                        pushtaskbean.setMessageId(s3);
                        pushtaskbean.setTaskId(s4);
                        pushtaskbean.setId(s1);
                        pushtaskbean.setAppKey(s5);
                        pushtaskbean.setCurrentActionid(1);
                        pushtaskbean.setStatus(cursor.getInt(cursor.getColumnIndex("status")));
                        if (jsonobject.has("cdnType"))
                        {
                            pushtaskbean.setCDNType(jsonobject.getBoolean("cdnType"));
                        }
                        if (jsonobject.has("condition"))
                        {
                            b(jsonobject, pushtaskbean);
                        }
                        com.igexin.push.core.g.al.put(s6, pushtaskbean);
                    }
                    catch (JSONException jsonexception)
                    {
                        jsonexception.printStackTrace();
                    }
                }
                cursor.close();
            }
            com.igexin.push.core.g.q = false;
        }
        return com.igexin.push.core.g.al.size();
    }

    public static f a()
    {
        if (c == null)
        {
            c = new f();
        }
        return c;
    }

    private void a(int i1, String s1, String s2)
    {
        s2 = new ContentValues();
        s2.put("status", Integer.valueOf(i1));
        com.igexin.push.core.f.a().i().a("message", s2, new String[] {
            "taskid"
        }, new String[] {
            s1
        });
    }

    private void a(com.igexin.push.c.c.c c1, PushTaskBean pushtaskbean, String s1, String s2)
    {
        c1.a(new com.igexin.push.e.b.c(pushtaskbean, s1, k()));
        com.igexin.push.core.g.ao.put(s2, c1);
    }

    private void a(com.igexin.push.core.bean.e e1)
    {
label0:
        {
            Object obj = new File((new StringBuilder()).append(com.igexin.push.core.g.ac).append("/").append(e1.c()).toString());
            File file = new File((new StringBuilder()).append(com.igexin.push.core.g.ad).append("/").append(e1.c()).toString());
            if (file.exists())
            {
                if (com.igexin.a.b.a.a(com.igexin.push.core.g.i, file.getAbsolutePath()).equals(e1.f()))
                {
                    break label0;
                }
                file.delete();
            }
            if (((File) (obj)).exists() && com.igexin.a.b.a.a(com.igexin.push.core.g.i, ((File) (obj)).getAbsolutePath()).equals(e1.f()) && a(((File) (obj)), file, e1.f()))
            {
                obj = new Intent(com.igexin.push.core.g.i, com/igexin/sdk/PushService);
                ((Intent) (obj)).putExtra("action", "com.igexin.sdk.action.extdownloadsuccess");
                ((Intent) (obj)).putExtra("id", e1.a());
                ((Intent) (obj)).putExtra("result", true);
                com.igexin.push.core.g.i.startService(((Intent) (obj)));
                return;
            } else
            {
                (new Thread(new com.igexin.push.core.d.f(com.igexin.push.core.g.i, e1))).start();
                return;
            }
        }
        obj = new Intent(com.igexin.push.core.g.i, com/igexin/sdk/PushService);
        ((Intent) (obj)).putExtra("action", "com.igexin.sdk.action.extdownloadsuccess");
        ((Intent) (obj)).putExtra("id", e1.a());
        ((Intent) (obj)).putExtra("result", true);
        com.igexin.push.core.g.i.startService(((Intent) (obj)));
    }

    private void a(List list)
    {
        com.igexin.push.core.a.g g1 = new com.igexin.push.core.a.g(this);
        PackageManager packagemanager = com.igexin.push.core.g.i.getPackageManager();
        List list1 = packagemanager.getInstalledPackages(0);
        int i1 = 0;
        while (i1 < list1.size()) 
        {
            try
            {
                PackageInfo packageinfo = (PackageInfo)list1.get(i1);
                ApplicationInfo applicationinfo = packageinfo.applicationInfo;
                if ((applicationinfo.flags & 1) <= 0)
                {
                    l l1 = new l();
                    l1.a(applicationinfo.loadLabel(packagemanager).toString());
                    l1.c(applicationinfo.packageName);
                    l1.b(String.valueOf(packageinfo.versionCode));
                    list.add(l1);
                }
            }
            catch (Exception exception) { }
            i1++;
        }
        Collections.sort(list, g1);
    }

    private boolean a(Class class1, Method method, String s1)
    {
        try
        {
            class1 = ((Class) (method.invoke(class1, new Object[] {
                s1
            })));
        }
        // Misplaced declaration of an exception variable
        catch (Class class1)
        {
            return true;
        }
        return class1 != null;
    }

    private void b(List list)
    {
        if (list.size() > 0)
        {
            for (int i1 = 0; i1 < list.size(); i1++)
            {
                String s1 = (String)list.get(i1);
                PushTaskBean pushtaskbean = (PushTaskBean)com.igexin.push.core.g.al.get(s1);
                pushtaskbean.setStatus(com.igexin.push.core.a.l);
                com.igexin.push.core.g.al.put(s1, pushtaskbean);
            }

        }
    }

    private void b(JSONObject jsonobject, PushTaskBean pushtaskbean)
    {
        try
        {
            JSONObject jsonobject1 = jsonobject.getJSONObject("condition");
            jsonobject = new HashMap();
            if (jsonobject1.has("wifi"))
            {
                jsonobject.put("wifi", jsonobject1.getString("wifi"));
            }
            if (jsonobject1.has("screenOn"))
            {
                jsonobject.put("screenOn", jsonobject1.getString("screenOn"));
            }
            if (jsonobject1.has("ssid"))
            {
                jsonobject.put("ssid", jsonobject1.getString("ssid"));
                if (jsonobject1.has("bssid"))
                {
                    jsonobject.put("bssid", jsonobject1.getString("bssid"));
                }
            }
            if (jsonobject1.has("duration"))
            {
                String s2 = jsonobject1.getString("duration");
                if (s2.contains("-"))
                {
                    int i1 = s2.indexOf("-");
                    String s1 = s2.substring(0, i1);
                    s2 = s2.substring(i1 + 1, s2.length());
                    jsonobject.put("startTime", s1);
                    jsonobject.put("endTime", s2);
                }
            }
            pushtaskbean.setConditionMap(jsonobject);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (JSONObject jsonobject)
        {
            jsonobject.printStackTrace();
        }
    }

    private void h(String s1)
    {
        Object obj1 = null;
        Object obj;
        obj = new File(com.igexin.push.core.g.aa);
        if (!((File) (obj)).exists())
        {
            ((File) (obj)).createNewFile();
        }
        obj = new FileOutputStream(com.igexin.push.core.g.aa);
        ((FileOutputStream) (obj)).write(com.igexin.a.b.a.a(s1).getBytes());
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_55;
        }
        ((FileOutputStream) (obj)).close();
_L7:
        return;
        s1;
        s1 = obj1;
_L5:
        if (s1 != null)
        {
            try
            {
                s1.close();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s1)
            {
                return;
            }
        }
          goto _L1
        s1;
        obj = null;
_L3:
        if (obj != null)
        {
            try
            {
                ((FileOutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        throw s1;
        s1;
        return;
        s1;
        if (true) goto _L3; else goto _L2
_L2:
        s1;
        s1 = ((String) (obj));
        if (true) goto _L5; else goto _L4
_L4:
_L1:
        if (true) goto _L7; else goto _L6
_L6:
    }

    private boolean i(String s1)
    {
        try
        {
            com.igexin.push.core.g.i.getPackageManager().getPackageInfo(s1, 0);
        }
        // Misplaced declaration of an exception variable
        catch (String s1)
        {
            return false;
        }
        return true;
    }

    public void A()
    {
        int i1 = com.igexin.push.core.g.aq - 100;
        Object obj;
        Iterator iterator;
        long l1;
        if (i1 < 0)
        {
            com.igexin.push.core.g.aq = 0;
        } else
        {
            com.igexin.push.core.g.aq = i1;
        }
        obj = new ArrayList();
        l1 = System.currentTimeMillis();
        iterator = com.igexin.push.core.g.ap.entrySet().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
            if (l1 - ((Long)entry.getValue()).longValue() > 0x36ee80L)
            {
                ((List) (obj)).add((String)entry.getKey());
            }
        } while (true);
        String s1;
        for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext(); com.igexin.push.core.g.ap.remove(s1))
        {
            s1 = (String)((Iterator) (obj)).next();
        }

    }

    public void B()
    {
        if (com.igexin.push.core.g.R < System.currentTimeMillis())
        {
            com.igexin.push.core.c.f.a().a(false);
        }
    }

    public void C()
    {
        if (!com.igexin.push.core.g.af)
        {
            com.igexin.push.core.g.af = com.igexin.a.a.b.d.c().a(com.igexin.push.e.b.d.g(), false, true);
        }
        if (!com.igexin.push.core.g.ag)
        {
            com.igexin.push.core.g.ag = com.igexin.a.a.b.d.c().a(com.igexin.push.e.b.f.g(), true, true);
        }
        if (!com.igexin.push.core.g.ah)
        {
            com.igexin.push.core.g.ah = com.igexin.a.a.b.d.c().a(com.igexin.push.e.b.e.g(), false, true);
        }
        if (!com.igexin.push.core.g.ai)
        {
            com.igexin.push.core.g.ai = com.igexin.a.a.b.d.c().a(com.igexin.push.e.b.g.g(), false, true);
        }
        if (!com.igexin.push.core.g.aj)
        {
            com.igexin.push.core.g.aj = com.igexin.a.a.b.d.c().a(com.igexin.push.e.b.a.g(), false, true);
        }
        if (!com.igexin.push.core.g.ak)
        {
            com.igexin.push.core.g.ak = com.igexin.a.a.b.d.c().a(com.igexin.push.e.b.b.g(), false, true);
        }
    }

    public void D()
    {
        Object obj;
        Object obj1;
        obj1 = null;
        obj = (new StringBuilder()).append("/data/data/").append(com.igexin.push.core.g.g).append("/files/").append("init.pid").toString();
        if (!(new File(((String) (obj)))).exists()) goto _L2; else goto _L1
_L1:
        byte abyte0[];
        byte abyte1[];
        abyte1 = com.igexin.push.core.g.u.getBytes();
        abyte0 = new byte[abyte1.length];
        int i1 = 0;
_L4:
        if (i1 >= abyte1.length)
        {
            break; /* Loop/switch isn't completed */
        }
        abyte0[i1] = (byte)(abyte1[i1] ^ com.igexin.push.core.g.ae[i1]);
        i1++;
        if (true) goto _L4; else goto _L3
_L3:
        obj = new FileOutputStream(((String) (obj)));
        ((FileOutputStream) (obj)).write(abyte0);
_L11:
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_124;
        }
        ((FileOutputStream) (obj)).close();
_L6:
        return;
        obj;
        obj = obj1;
_L9:
        if (obj == null) goto _L6; else goto _L5
_L5:
        try
        {
            ((FileOutputStream) (obj)).close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            return;
        }
        Object obj2;
        obj2;
        obj = null;
_L8:
        if (obj != null)
        {
            try
            {
                ((FileOutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        throw obj2;
        obj;
        return;
        obj2;
        if (true) goto _L8; else goto _L7
_L7:
        obj2;
          goto _L9
          goto _L6
_L2:
        obj = null;
        if (true) goto _L11; else goto _L10
_L10:
    }

    public boolean E()
    {
        if ("none".equals(com.igexin.push.a.k.y)) goto _L2; else goto _L1
_L1:
        String as[] = com.igexin.push.a.k.y.split(",");
        int i1 = 0;
_L9:
        if (i1 < as.length)
        {
            Exception exception;
            Class class1;
            Method method;
            boolean flag;
            if (i(as[i1]))
            {
                return false;
            }
            i1++;
            continue; /* Loop/switch isn't completed */
        }
        if ("none".equals(com.igexin.push.a.k.z)) goto _L2; else goto _L3
_L3:
        as = com.igexin.push.a.k.z.split(",");
        class1 = Class.forName("android.os.ServiceManager");
        method = class1.getMethod("getService", new Class[] {
            java/lang/String
        });
        method.setAccessible(true);
        i1 = 0;
_L7:
        if (i1 >= as.length) goto _L5; else goto _L4
_L4:
        flag = a(class1, method, as[i1]);
        if (flag) goto _L2; else goto _L6
_L6:
        i1++;
          goto _L7
_L5:
        return true;
        exception;
_L2:
        return false;
        if (true) goto _L9; else goto _L8
_L8:
    }

    public com.igexin.push.core.bean.f a(JSONObject jsonobject)
    {
        com.igexin.push.core.bean.f f1 = new com.igexin.push.core.bean.f();
        f1.a(jsonobject.getString("version"));
        jsonobject = jsonobject.getJSONArray("extensions");
        if (jsonobject != null && jsonobject.length() > 0)
        {
            HashMap hashmap = new HashMap();
            for (int i1 = 0; i1 < jsonobject.length(); i1++)
            {
                JSONObject jsonobject1 = (JSONObject)jsonobject.get(i1);
                com.igexin.push.core.bean.e e1 = new com.igexin.push.core.bean.e();
                e1.a(jsonobject1.getInt("id"));
                e1.a(jsonobject1.getString("version"));
                e1.b(jsonobject1.getString("name"));
                e1.c(jsonobject1.getString("cls_name"));
                e1.d(jsonobject1.getString("url"));
                e1.e(jsonobject1.getString("checksum"));
                e1.f(jsonobject1.getString("key"));
                if (jsonobject1.has("isdestroy"))
                {
                    e1.a(jsonobject1.getBoolean("isdestroy"));
                }
                if (jsonobject1.has("effective"))
                {
                    String s1 = jsonobject1.getString("effective");
                    long l2 = 0L;
                    long l1 = l2;
                    if (s1 != null)
                    {
                        l1 = l2;
                        if (s1.length() <= 13)
                        {
                            l1 = Long.parseLong(s1);
                        }
                    }
                    e1.a(l1);
                }
                if (jsonobject1.has("loadTime"))
                {
                    e1.b(jsonobject1.getLong("loadTime"));
                }
                hashmap.put(Integer.valueOf(e1.a()), e1);
            }

            f1.a(hashmap);
            return f1;
        } else
        {
            f1.a(new HashMap());
            return f1;
        }
    }

    public String a(String s1, String s2)
    {
        return (new StringBuilder()).append(s1).append(":").append(s2).toString();
    }

    public String a(boolean flag, int i1)
    {
        Object obj;
        Object obj1;
        Object obj2;
        obj = null;
        obj1 = null;
        obj2 = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        if (i1 != -1) goto _L2; else goto _L1
_L1:
        obj2 = (new StringBuilder()).append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("register").append("|").append(com.igexin.push.core.g.v).toString();
        obj = obj2;
_L15:
        obj2 = obj;
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
            obj2 = obj;
        }
_L10:
        return ((String) (obj2));
_L2:
        if (i1 != 0)
        {
            break MISSING_BLOCK_LABEL_1074;
        }
        if (!flag) goto _L4; else goto _L3
_L3:
        obj1 = com.igexin.push.core.f.a().i().a("bi", new String[] {
            "type"
        }, new String[] {
            "1", "2"
        }, null, null);
        obj = obj1;
_L8:
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_1062;
        }
        obj1 = null;
_L7:
        obj2 = obj1;
        int j1;
        int k1;
        int l1;
        int i2;
        int j2;
        int k2;
        if (!((Cursor) (obj)).moveToNext())
        {
            break MISSING_BLOCK_LABEL_1065;
        }
        i1 = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndexOrThrow("start_service_count"));
        j1 = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndexOrThrow("login_count"));
        k1 = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndexOrThrow("loginerror_nonetwork_count"));
        l1 = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndexOrThrow("loginerror_connecterror_count"));
        i2 = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndexOrThrow("online_time"));
        j2 = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndexOrThrow("network_time"));
        k2 = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndexOrThrow("running_time"));
        obj2 = (new StringBuilder()).append(((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndexOrThrow("create_time"))).append(" 00:00:00").toString();
        if (obj1 != null) goto _L6; else goto _L5
_L5:
        obj2 = (new StringBuilder()).append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("startservice").append("|").append(i1).append("\n").append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("login").append("|").append(j1).append("\n").append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("loginerror-nonetwork").append("|").append(k1).append("\n").append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("loginerror-connecterror").append("|").append(l1).append("\n").append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("online").append("|").append(i2).append("\n").append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("network").append("|").append(j2).append("\n").append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("running").append("|").append(k2).toString();
        obj1 = obj2;
          goto _L7
_L4:
        obj1 = com.igexin.push.core.f.a().i().a("bi", new String[] {
            "type"
        }, new String[] {
            "2"
        }, null, null);
        obj = obj1;
          goto _L8
_L6:
        obj2 = (new StringBuilder()).append(((String) (obj1))).append("\n").append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("startservice").append("|").append(i1).append("\n").append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("login").append("|").append(j1).append("\n").append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("loginerror-nonetwork").append("|").append(k1).append("\n").append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("loginerror-connecterror").append("|").append(l1).append("\n").append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("online").append("|").append(i2).append("\n").append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("network").append("|").append(j2).append("\n").append(((String) (obj2))).append("|").append(com.igexin.push.core.g.C).append("|").append("running").append("|").append(k2).toString();
        obj1 = obj2;
          goto _L7
        obj2 = null;
        obj1 = obj;
        obj = obj2;
        continue; /* Loop/switch isn't completed */
        if (i1 != 1)
        {
            break MISSING_BLOCK_LABEL_1220;
        }
        long l2 = com.igexin.push.core.i.a().a;
        if (com.igexin.push.a.k.h > 0)
        {
            l2 = com.igexin.push.a.k.h * 1000;
        }
        String s1 = (new StringBuilder()).append(com.igexin.push.a.k.e).append(",").append(com.igexin.push.a.k.f).toString();
        obj2 = (new StringBuilder()).append(((String) (obj2))).append("|").append(com.igexin.push.core.g.u).append("|").append(com.igexin.push.core.g.c).append("|").append(com.igexin.push.core.g.l).append("|").append(s1).append("|").append(l2).append("|").toString();
        obj = obj2;
        continue; /* Loop/switch isn't completed */
        if (i1 != 4)
        {
            break MISSING_BLOCK_LABEL_1278;
        }
        obj2 = (new StringBuilder()).append(((String) (obj2))).append("|").append(com.igexin.push.core.g.u).append("|").append(com.igexin.push.core.g.c).append("|").toString();
        obj = obj2;
        continue; /* Loop/switch isn't completed */
        if (i1 != 5)
        {
            break MISSING_BLOCK_LABEL_1379;
        }
        obj2 = (new StringBuilder()).append(((String) (obj2))).append("|").append(com.igexin.push.core.g.u).append("|").append(com.igexin.push.core.g.c).toString();
        obj = obj2;
        continue; /* Loop/switch isn't completed */
        obj1;
        obj1 = null;
_L13:
        obj2 = obj1;
        if (obj == null) goto _L10; else goto _L9
_L9:
        ((Cursor) (obj)).close();
        return ((String) (obj1));
        Exception exception;
        exception;
        obj = null;
_L12:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        throw exception;
        exception;
        if (true) goto _L12; else goto _L11
_L11:
        Exception exception1;
        exception1;
          goto _L13
        obj = null;
        if (true) goto _L15; else goto _L14
_L14:
    }

    public void a(int i1)
    {
        Intent intent = new Intent();
        intent.setAction((new StringBuilder()).append("com.igexin.sdk.action.").append(com.igexin.push.core.g.c).toString());
        Bundle bundle = new Bundle();
        bundle.putInt("action", 10008);
        bundle.putInt("pid", i1);
        intent.putExtras(bundle);
        com.igexin.push.core.f.a().a(intent);
    }

    public void a(int i1, int j1, String s1)
    {
        com.igexin.push.a.k.e = i1;
        com.igexin.push.a.k.f = j1;
        com.igexin.push.a.a.a().b();
        com.igexin.push.e.b.g.g().h();
    }

    public void a(int i1, String s1)
    {
        com.igexin.push.a.k.h = i1;
        com.igexin.push.a.a.a().c();
        if (com.igexin.push.core.g.o)
        {
            com.igexin.a.a.c.a.a("setHeartbeatInterval heartbeatReq");
            if (System.currentTimeMillis() - com.igexin.push.core.g.U > 5000L)
            {
                com.igexin.push.core.g.U = System.currentTimeMillis();
                f();
            }
        }
    }

    public void a(Intent intent)
    {
        if (intent != null)
        {
            com.igexin.push.core.f.a().a(false);
            if (intent.hasExtra("op_app"))
            {
                com.igexin.push.core.g.E = intent.getStringExtra("op_app");
            } else
            {
                com.igexin.push.core.g.E = "";
            }
            if (com.igexin.push.core.g.o)
            {
                l();
            }
        }
    }

    public void a(Bundle bundle)
    {
        String s1 = bundle.getString("action");
        if (!s1.equals("setTag")) goto _L2; else goto _L1
_L1:
        if (com.igexin.push.a.k.n)
        {
            c(bundle.getString("tags"));
        }
_L4:
        return;
_L2:
        if (!s1.equals("setSilentTime"))
        {
            break; /* Loop/switch isn't completed */
        }
        if (com.igexin.push.a.k.o)
        {
            a(bundle.getInt("beginHour", 0), bundle.getInt("duration", 0), com.igexin.push.core.g.i.getPackageName());
            return;
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (!s1.equals("sendMessage"))
        {
            break; /* Loop/switch isn't completed */
        }
        if (com.igexin.push.a.k.m)
        {
            a(bundle.getString("taskid"), bundle.getByteArray("extraData"));
            return;
        }
        if (true) goto _L4; else goto _L5
_L5:
        if (s1.equals("stopService"))
        {
            com.igexin.push.core.f.a().a(com.igexin.push.core.g.i.getPackageName());
            return;
        }
        if (!s1.equals("setHeartbeatInterval"))
        {
            break; /* Loop/switch isn't completed */
        }
        if (com.igexin.push.a.k.p)
        {
            a(bundle.getInt("interval", 0), com.igexin.push.core.g.i.getPackageName());
            return;
        }
        if (true) goto _L4; else goto _L6
_L6:
        if (!s1.equals("setSocketTimeout"))
        {
            break; /* Loop/switch isn't completed */
        }
        if (com.igexin.push.a.k.q)
        {
            b(bundle.getInt("timeout", 0), com.igexin.push.core.g.i.getPackageName());
            return;
        }
        if (true) goto _L4; else goto _L7
_L7:
        if (!s1.equals("sendFeedbackMessage"))
        {
            continue; /* Loop/switch isn't completed */
        }
        if (!com.igexin.push.a.k.w || com.igexin.push.core.g.aq > 200) goto _L4; else goto _L8
_L8:
        String s2;
        String s3;
        s1 = bundle.getString("taskid");
        s2 = bundle.getString("messageid");
        bundle = bundle.getString("actionid");
        s3 = (new StringBuilder()).append(s1).append(":").append(s2).append(":").append(bundle).toString();
        if (com.igexin.push.core.g.ap.get(s3) != null) goto _L4; else goto _L9
_L9:
        long l1 = System.currentTimeMillis();
        PushTaskBean pushtaskbean = new PushTaskBean();
        pushtaskbean.setTaskId(s1);
        pushtaskbean.setMessageId(s2);
        pushtaskbean.setAppid(com.igexin.push.core.g.c);
        pushtaskbean.setAppKey(com.igexin.push.core.g.d);
        a(pushtaskbean, ((String) (bundle)));
        com.igexin.push.core.g.aq++;
        com.igexin.push.core.g.ap.put(s3, Long.valueOf(l1));
        return;
        if (!s1.equals("turnOffPush")) goto _L4; else goto _L10
_L10:
        com.igexin.push.core.f.a().b(com.igexin.push.core.g.i.getPackageName());
        return;
    }

    public void a(PushTaskBean pushtaskbean)
    {
        com.igexin.push.c.c.c c1 = new com.igexin.push.c.c.c();
        c1.a();
        c1.c = (new StringBuilder()).append("RCV").append(pushtaskbean.getMessageId()).toString();
        c1.d = com.igexin.push.core.g.u;
        c1.a = (int)System.currentTimeMillis();
        com.igexin.push.core.f.a().e().a((new StringBuilder()).append("C-").append(com.igexin.push.core.g.u).toString(), c1);
        com.igexin.a.a.c.a.a((new StringBuilder()).append("cdnreceive|").append(pushtaskbean.getTaskId()).append("|").append(pushtaskbean.getMessageId()).toString());
    }

    public void a(PushTaskBean pushtaskbean, String s1)
    {
        if (pushtaskbean.isCDNType())
        {
            b(pushtaskbean, s1);
            return;
        } else
        {
            a(pushtaskbean, s1, "ok");
            return;
        }
    }

    public void a(PushTaskBean pushtaskbean, String s1, String s2)
    {
        long l1 = System.currentTimeMillis();
        s2 = (new StringBuilder()).append("{\"action\":\"pushmessage_feedback\",\"appid\":\"").append(pushtaskbean.getAppid()).append("\", \"id\":\"").append(l1).append("\", \"appkey\":\"").append(pushtaskbean.getAppKey()).append("\", \"messageid\":\"").append(pushtaskbean.getMessageId()).append("\",\"taskid\":\"").append(pushtaskbean.getTaskId()).append("\",\"actionid\": \"").append(s1).append("\",\"result\":\"").append(s2).append("\",\"timestamp\":\"").append(System.currentTimeMillis()).append("\"}").toString();
        Object obj = new com.igexin.push.c.c.d();
        ((com.igexin.push.c.c.d) (obj)).a();
        obj.a = (int)l1;
        obj.d = "17258000";
        obj.e = s2;
        obj.g = com.igexin.push.core.g.u;
        com.igexin.push.core.f.a().e().a((new StringBuilder()).append("C-").append(com.igexin.push.core.g.u).toString(), ((com.igexin.push.c.c.e) (obj)));
        obj = com.igexin.push.core.c.c.a();
        if (obj != null)
        {
            ((com.igexin.push.core.c.c) (obj)).a(new com.igexin.push.core.bean.i(l1, s2, (byte)3, l1));
        }
        com.igexin.a.a.c.a.a((new StringBuilder()).append("feedback|").append(pushtaskbean.getTaskId()).append("|").append(pushtaskbean.getMessageId()).append("|").append(s1).toString());
    }

    public void a(com.igexin.push.core.bean.f f1)
    {
        com.igexin.push.core.g.as = 0;
        com.igexin.push.core.g.at = 0;
        com.igexin.push.core.g.av = f1;
        Object obj = f1.b();
        if (com.igexin.push.a.k.x != null)
        {
            Map map = com.igexin.push.a.k.x.b();
            Iterator iterator = map.entrySet().iterator();
            ArrayList arraylist = new ArrayList();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                Object obj2 = (java.util.Map.Entry)iterator.next();
                int i1 = ((Integer)((java.util.Map.Entry) (obj2)).getKey()).intValue();
                obj2 = (com.igexin.push.core.bean.e)((java.util.Map.Entry) (obj2)).getValue();
                if (!((Map) (obj)).containsKey(Integer.valueOf(i1)))
                {
                    com.igexin.push.core.g.aw = true;
                    obj2 = new File((new StringBuilder()).append(com.igexin.push.core.g.ad).append("/").append(((com.igexin.push.core.bean.e) (obj2)).c()).toString());
                    if (((File) (obj2)).exists())
                    {
                        ((File) (obj2)).delete();
                    }
                    arraylist.add(Integer.valueOf(i1));
                }
            } while (true);
            if (arraylist != null && arraylist.size() > 0)
            {
                for (Iterator iterator1 = arraylist.iterator(); iterator1.hasNext(); map.remove(Integer.valueOf(((Integer)iterator1.next()).intValue()))) { }
                com.igexin.push.a.a.a().g();
            }
            obj = ((Map) (obj)).entrySet().iterator();
            boolean flag = true;
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break;
                }
                Object obj1 = (java.util.Map.Entry)((Iterator) (obj)).next();
                int j1 = ((Integer)((java.util.Map.Entry) (obj1)).getKey()).intValue();
                obj1 = (com.igexin.push.core.bean.e)((java.util.Map.Entry) (obj1)).getValue();
                if (map.containsKey(Integer.valueOf(j1)))
                {
                    if (!((com.igexin.push.core.bean.e)map.get(Integer.valueOf(j1))).b().equals(((com.igexin.push.core.bean.e) (obj1)).b()))
                    {
                        com.igexin.push.core.g.aw = true;
                        com.igexin.push.core.g.as++;
                        a(((com.igexin.push.core.bean.e) (obj1)));
                        flag = false;
                    }
                } else
                {
                    com.igexin.push.core.g.as++;
                    a(((com.igexin.push.core.bean.e) (obj1)));
                    flag = false;
                }
            } while (true);
            if (flag)
            {
                com.igexin.push.a.k.x.a(f1.a());
                com.igexin.push.a.a.a().g();
                Process.killProcess(Process.myPid());
            }
        } else
        {
            f1 = ((Map) (obj)).entrySet().iterator();
            while (f1.hasNext()) 
            {
                com.igexin.push.core.bean.e e1 = (com.igexin.push.core.bean.e)((java.util.Map.Entry)f1.next()).getValue();
                com.igexin.push.core.g.as++;
                a(e1);
            }
        }
    }

    public void a(String s1)
    {
        s1 = (new StringBuilder()).append("{\"action\":\"received\",\"id\":\"").append(s1).append("\"}").toString();
        com.igexin.push.c.c.d d1 = new com.igexin.push.c.c.d();
        d1.a();
        d1.a = (int)System.currentTimeMillis();
        d1.d = "17258000";
        d1.e = s1;
        d1.g = com.igexin.push.core.g.u;
        com.igexin.push.core.f.a().e().a((new StringBuilder()).append("C-").append(com.igexin.push.core.g.u).toString(), d1);
    }

    public void a(String s1, com.igexin.push.c.c.a a1, PushTaskBean pushtaskbean)
    {
        s1 = new com.igexin.push.e.a.a(new com.igexin.push.core.d.b(s1, a1, pushtaskbean));
        com.igexin.a.a.b.d.c().a(s1, false, true);
    }

    public void a(String s1, String s2, String s3, String s4)
    {
        s3 = new Intent("com.igexin.sdk.action.execute");
        s3.putExtra("taskid", s1);
        s3.putExtra("messageid", s2);
        s3.putExtra("appid", com.igexin.push.core.g.c);
        s3.putExtra("pkgname", com.igexin.push.core.g.g);
        com.igexin.push.core.f.a().a(s3);
    }

    public void a(String s1, String s2, String s3, String s4, long l1)
    {
        Intent intent = new Intent();
        intent.setAction((new StringBuilder()).append("com.igexin.sdk.action.").append(com.igexin.push.core.g.c).toString());
        Bundle bundle = new Bundle();
        bundle.putInt("action", 10006);
        bundle.putString("appid", s1);
        bundle.putString("taskid", s2);
        bundle.putString("actionid", s3);
        bundle.putString("result", s4);
        bundle.putLong("timestamp", l1);
        intent.putExtras(bundle);
        com.igexin.push.core.f.a().a(intent);
    }

    public void a(String s1, byte abyte0[])
    {
        Object obj;
        long l1;
        if (com.igexin.push.core.g.u == null)
        {
            break MISSING_BLOCK_LABEL_172;
        }
        obj = new JSONObject();
        l1 = System.currentTimeMillis();
        ((JSONObject) (obj)).put("action", "sendmessage");
        ((JSONObject) (obj)).put("id", String.valueOf(l1));
        ((JSONObject) (obj)).put("cid", com.igexin.push.core.g.u);
        ((JSONObject) (obj)).put("appid", com.igexin.push.core.g.c);
        ((JSONObject) (obj)).put("taskid", s1);
        s1 = ((JSONObject) (obj)).toString();
        com.igexin.push.core.c.c.a().a(new com.igexin.push.core.bean.i(l1, s1, (byte)6, l1));
        obj = new com.igexin.push.c.c.d();
        ((com.igexin.push.c.c.d) (obj)).a();
        obj.a = (int)l1;
        obj.d = com.igexin.push.core.g.u;
        obj.e = s1;
        obj.f = abyte0;
        obj.g = com.igexin.push.core.g.u;
        com.igexin.push.core.f.a().e().a((new StringBuilder()).append("C-").append(com.igexin.push.core.g.u).toString(), ((com.igexin.push.c.c.e) (obj)));
        return;
        s1;
    }

    public void a(boolean flag)
    {
    }

    public void a(byte abyte0[])
    {
        boolean flag = false;
        Object obj;
        abyte0 = new JSONObject(new String(com.igexin.a.b.a.c(Base64.decode(abyte0, 0))));
        if (!abyte0.has("result") || !"ok".equals(abyte0.getString("result")))
        {
            break MISSING_BLOCK_LABEL_976;
        }
        long l1 = System.currentTimeMillis();
        com.igexin.push.core.c.f.a().f(l1);
        if (!abyte0.has("config"))
        {
            break MISSING_BLOCK_LABEL_976;
        }
        abyte0 = new JSONObject(abyte0.getString("config"));
        if (abyte0.has("sdk.uploadapplist.enable"))
        {
            String s1 = abyte0.getString("sdk.uploadapplist.enable");
            if (s1.equals("true") || s1.equals("false"))
            {
                com.igexin.push.a.k.l = Boolean.valueOf(s1).booleanValue();
            }
        }
        if (abyte0.has("sdk.feature.sendmessage.enable"))
        {
            String s2 = abyte0.getString("sdk.feature.sendmessage.enable");
            if (s2.equals("true") || s2.equals("false"))
            {
                com.igexin.push.a.k.m = Boolean.valueOf(s2).booleanValue();
            }
        }
        if (abyte0.has("sdk.readlocalcell.enable"))
        {
            String s3 = abyte0.getString("sdk.readlocalcell.enable");
            if (s3.equals("true") || s3.equals("false"))
            {
                com.igexin.push.a.k.k = Boolean.valueOf(s3).booleanValue();
            }
        }
        if (abyte0.has("sdk.ca.enable"))
        {
            String s4 = abyte0.getString("sdk.ca.enable");
            if (s4.equals("true") || s4.equals("false"))
            {
                com.igexin.push.a.k.r = Boolean.valueOf(s4).booleanValue();
            }
        }
        if (abyte0.has("sdk.snl.enable"))
        {
            String s5 = abyte0.getString("sdk.snl.enable");
            if (s5.equals("true") || s5.equals("false"))
            {
                com.igexin.push.a.k.s = Boolean.valueOf(s5).booleanValue();
            }
        }
        if (abyte0.has("sdk.domainbackup.enable"))
        {
            String s6 = abyte0.getString("sdk.domainbackup.enable");
            if (s6.equals("true") || s6.equals("false"))
            {
                com.igexin.push.a.k.j = Boolean.valueOf(s6).booleanValue();
            }
        }
        if (abyte0.has("sdk.feature.setsilenttime.enable"))
        {
            String s7 = abyte0.getString("sdk.feature.setsilenttime.enable");
            if (s7.equals("true") || s7.equals("false"))
            {
                com.igexin.push.a.k.o = Boolean.valueOf(s7).booleanValue();
                if (!com.igexin.push.a.k.o && com.igexin.push.a.k.f != 0)
                {
                    a(12, 0, "server");
                }
            }
        }
        if (!abyte0.has("sdk.snl.maxactiveflow"))
        {
            break MISSING_BLOCK_LABEL_474;
        }
        obj = abyte0.getString("sdk.snl.maxactiveflow");
        try
        {
            com.igexin.push.a.k.t = Integer.parseInt(((String) (obj)));
        }
        catch (Exception exception) { }
        if (abyte0.has("sdk.feature.settag.enable"))
        {
            obj = abyte0.getString("sdk.feature.settag.enable");
            if (((String) (obj)).equals("true") || ((String) (obj)).equals("false"))
            {
                com.igexin.push.a.k.n = Boolean.valueOf(((String) (obj))).booleanValue();
            }
        }
        if (abyte0.has("sdk.feature.setheartbeatinterval.enable"))
        {
            obj = abyte0.getString("sdk.feature.setheartbeatinterval.enable");
            if (((String) (obj)).equals("true") || ((String) (obj)).equals("false"))
            {
                com.igexin.push.a.k.p = Boolean.valueOf(((String) (obj))).booleanValue();
            }
        }
        if (abyte0.has("sdk.feature.setsockettimeout.enable"))
        {
            obj = abyte0.getString("sdk.feature.setsockettimeout.enable");
            if (((String) (obj)).equals("true") || ((String) (obj)).equals("false"))
            {
                com.igexin.push.a.k.q = Boolean.valueOf(((String) (obj))).booleanValue();
            }
        }
        if (abyte0.has("sdk.guard.enable"))
        {
            obj = abyte0.getString("sdk.guard.enable");
            if (((String) (obj)).equals("true") || ((String) (obj)).equals("false"))
            {
                com.igexin.push.a.k.u = Boolean.valueOf(((String) (obj))).booleanValue();
            }
        }
        if (abyte0.has("sdk.wakeupsdk.enable"))
        {
            obj = abyte0.getString("sdk.wakeupsdk.enable");
            if (((String) (obj)).equals("true") || ((String) (obj)).equals("false"))
            {
                com.igexin.push.a.k.v = Boolean.valueOf(((String) (obj))).booleanValue();
            }
        }
        if (abyte0.has("sdk.feature.feedback.enable"))
        {
            obj = abyte0.getString("sdk.feature.feedback.enable");
            if (((String) (obj)).equals("true") || ((String) (obj)).equals("false"))
            {
                com.igexin.push.a.k.w = Boolean.valueOf(((String) (obj))).booleanValue();
            }
        }
        if (abyte0.has("sdk.watchout.app"))
        {
            com.igexin.push.a.k.y = abyte0.getString("sdk.watchout.app");
        }
        if (abyte0.has("sdk.watchout.service"))
        {
            com.igexin.push.a.k.z = abyte0.getString("sdk.watchout.service");
        }
        if (abyte0.has("sdk.daemon.enable"))
        {
            obj = abyte0.getString("sdk.daemon.enable");
            if (((String) (obj)).equals("true") || ((String) (obj)).equals("false"))
            {
                com.igexin.push.a.k.A = Boolean.valueOf(((String) (obj))).booleanValue();
            }
        }
        if (!abyte0.has("ext_infos"))
        {
            break MISSING_BLOCK_LABEL_970;
        }
        abyte0 = abyte0.getString("ext_infos");
        if (abyte0 == null)
        {
            break MISSING_BLOCK_LABEL_970;
        }
        if ("".equals(abyte0))
        {
            break MISSING_BLOCK_LABEL_970;
        }
        abyte0 = new JSONObject(abyte0);
        if (!abyte0.has("version"))
        {
            break MISSING_BLOCK_LABEL_970;
        }
        obj = abyte0.getString("version");
        if (com.igexin.push.a.k.x == null || !((String) (obj)).equals(com.igexin.push.a.k.x.a()))
        {
            flag = true;
        }
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_970;
        }
        try
        {
            abyte0 = a(((JSONObject) (abyte0)));
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            f(abyte0.toString());
            return;
        }
        if (abyte0 == null)
        {
            break MISSING_BLOCK_LABEL_970;
        }
        obj = new Message();
        obj.what = com.igexin.push.core.a.i;
        obj.obj = abyte0;
        com.igexin.push.core.f.a().a(((Message) (obj)));
        com.igexin.push.a.a.a().f();
    }

    public boolean a(long l1)
    {
        Date date = new Date(l1);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int k1 = calendar.get(11);
        int j1 = com.igexin.push.a.k.e + com.igexin.push.a.k.f;
        int i1 = j1;
        if (j1 >= 24)
        {
            i1 = j1 - 24;
        }
        if (com.igexin.push.a.k.f == 0)
        {
            return false;
        }
        if (com.igexin.push.a.k.e < i1)
        {
            if (k1 >= com.igexin.push.a.k.e && k1 < i1)
            {
                return true;
            }
        } else
        if (com.igexin.push.a.k.e > i1)
        {
            if (k1 >= 0 && k1 < i1)
            {
                return true;
            }
            if (k1 >= com.igexin.push.a.k.e && k1 < 24)
            {
                return true;
            }
        }
        return false;
    }

    public boolean a(com.igexin.a.a.d.d d1)
    {
        d1.b();
        JVM INSTR tableswitch -2047 -2045: default 32
    //                   -2047 34
    //                   -2046 32
    //                   -2045 162;
           goto _L1 _L2 _L1 _L3
_L1:
        return true;
_L2:
        com.igexin.a.a.c.a.a("disconnected|network");
        com.igexin.push.core.i.a().a(com.igexin.push.core.k.c);
        com.igexin.push.core.c.r.d();
        if ((d1.N instanceof ClosedChannelException) || (d1.N instanceof SocketTimeoutException) || (d1.N instanceof UnknownHostException) || (d1.N instanceof UnresolvedAddressException) || (d1.N instanceof UnknownServiceException))
        {
            com.igexin.push.core.c.r.a();
        }
        if (!com.igexin.push.core.g.l || !com.igexin.push.core.g.m)
        {
            if (com.igexin.push.core.g.o)
            {
                com.igexin.push.core.g.o = false;
                m();
            }
        } else
        {
            if (com.igexin.push.core.g.o)
            {
                com.igexin.push.core.g.o = false;
                m();
            }
            com.igexin.push.core.f.a().e().c(false);
        }
_L5:
        return false;
_L3:
        com.igexin.a.a.c.a.a("disconnected|user");
        com.igexin.push.core.c.r.d();
        if (com.igexin.push.core.g.o)
        {
            com.igexin.push.core.g.o = false;
            m();
        }
        if (true) goto _L5; else goto _L4
_L4:
    }

    public boolean a(com.igexin.push.c.c.e e1)
    {
        if (e1 != null)
        {
            com.igexin.push.core.a.a a1 = (com.igexin.push.core.a.a)a.get(Integer.valueOf(e1.i));
            if (a1 != null)
            {
                a1.a(e1);
            }
            com.igexin.push.e.b.d.g().h();
            return true;
        } else
        {
            return false;
        }
    }

    public boolean a(File file, File file1, String s1)
    {
        Object obj;
        Object obj1;
        Object obj2 = null;
        boolean flag1 = false;
        byte abyte0[];
        int i1;
        boolean flag;
        try
        {
            obj = new FileInputStream(file);
        }
        // Misplaced declaration of an exception variable
        catch (File file)
        {
            s1 = null;
            file = null;
            obj = obj2;
            continue; /* Loop/switch isn't completed */
        }
        finally
        {
            s1 = null;
            file1 = null;
            obj = null;
        }
        try
        {
_L7:
            obj1 = new FileOutputStream(file1);
        }
        // Misplaced declaration of an exception variable
        catch (File file)
        {
            s1 = null;
            file = ((File) (obj));
            obj = obj2;
            continue; /* Loop/switch isn't completed */
        }
        finally
        {
            s1 = null;
            file1 = null;
            continue; /* Loop/switch isn't completed */
        }
        obj2 = new BufferedOutputStream(((java.io.OutputStream) (obj1)));
        file = new byte[1024];
_L3:
        i1 = ((FileInputStream) (obj)).read(file);
        if (i1 == -1) goto _L2; else goto _L1
_L1:
        abyte0 = new byte[i1];
        System.arraycopy(file, 0, abyte0, 0, i1);
        ((BufferedOutputStream) (obj2)).write(abyte0);
          goto _L3
        file;
        file = ((File) (obj));
        obj = obj1;
        s1 = ((String) (obj2));
_L10:
        if (file1.exists())
        {
            file1.delete();
        }
        if (file != null)
        {
            try
            {
                file.close();
            }
            // Misplaced declaration of an exception variable
            catch (File file)
            {
                return false;
            }
        }
        if (s1 == null)
        {
            break MISSING_BLOCK_LABEL_122;
        }
        s1.close();
        flag = flag1;
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_140;
        }
        ((FileOutputStream) (obj)).close();
        flag = flag1;
_L5:
        return flag;
_L2:
        ((FileInputStream) (obj)).close();
        ((BufferedOutputStream) (obj2)).flush();
        ((BufferedOutputStream) (obj2)).close();
        ((FileOutputStream) (obj1)).close();
        if (com.igexin.a.b.a.a(com.igexin.push.core.g.i, file1.getAbsolutePath()).equals(s1))
        {
            break MISSING_BLOCK_LABEL_224;
        }
        file1.delete();
        if (obj != null)
        {
            try
            {
                ((FileInputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (File file)
            {
                return false;
            }
        }
        if (obj2 == null)
        {
            break MISSING_BLOCK_LABEL_205;
        }
        ((BufferedOutputStream) (obj2)).close();
        flag = flag1;
        if (obj1 == null) goto _L5; else goto _L4
_L4:
        ((FileOutputStream) (obj1)).close();
        return false;
        flag = true;
        if (obj != null)
        {
            try
            {
                ((FileInputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (File file)
            {
                return true;
            }
        }
        if (obj2 == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        ((BufferedOutputStream) (obj2)).close();
        if (obj1 == null) goto _L5; else goto _L6
_L6:
        ((FileOutputStream) (obj1)).close();
        return true;
_L8:
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_280;
        }
        ((FileInputStream) (obj)).close();
        if (s1 == null)
        {
            break MISSING_BLOCK_LABEL_288;
        }
        s1.close();
        if (file1 != null)
        {
            try
            {
                file1.close();
            }
            // Misplaced declaration of an exception variable
            catch (File file1) { }
        }
        throw file;
        file;
        s1 = null;
        file1 = ((File) (obj1));
        continue; /* Loop/switch isn't completed */
        file;
        s1 = ((String) (obj2));
        file1 = ((File) (obj1));
        continue; /* Loop/switch isn't completed */
        obj2;
        obj1 = file;
        file1 = ((File) (obj));
        file = ((File) (obj2));
        obj = obj1;
        if (true) goto _L8; else goto _L7
        file;
        s1 = null;
        file = ((File) (obj));
        obj = obj1;
        if (true) goto _L10; else goto _L9
_L9:
    }

    public boolean a(Object obj)
    {
        com.igexin.push.d.j j1 = com.igexin.push.core.f.a().e();
        if (!(obj instanceof com.igexin.push.c.c.e) || j1 == null) goto _L2; else goto _L1
_L1:
        j1.a((com.igexin.push.c.c.e)obj);
_L4:
        return true;
_L2:
        if (!(obj instanceof com.igexin.a.a.b.a.a.b))
        {
            continue; /* Loop/switch isn't completed */
        }
        if (com.igexin.push.core.g.o)
        {
            com.igexin.push.core.g.o = false;
            m();
            return true;
        }
        continue; /* Loop/switch isn't completed */
        if (obj instanceof com.igexin.a.a.b.a.a.a) goto _L4; else goto _L3
_L3:
        if (obj instanceof com.igexin.push.c.b.a)
        {
            j1.c(false);
            return true;
        }
        if (obj instanceof com.igexin.push.c.b.b)
        {
            j1.c(true);
            return true;
        }
        if (true) goto _L4; else goto _L5
_L5:
    }

    public boolean a(String s1, String s2, String s3)
    {
        Bundle bundle = new Bundle();
        bundle.putString("taskid", s1);
        bundle.putString("messageid", s2);
        bundle.putString("actionid", s3);
        s1 = new Message();
        s1.what = com.igexin.push.core.a.h;
        s1.obj = bundle;
        return com.igexin.push.core.f.a().a(s1);
    }

    public boolean a(JSONObject jsonobject, PushTaskBean pushtaskbean)
    {
        ArrayList arraylist = new ArrayList();
        JSONArray jsonarray = jsonobject.getJSONArray("action_chains");
        int i1 = 0;
_L22:
        if (i1 >= jsonarray.length()) goto _L2; else goto _L1
_L1:
        jsonobject = ((JSONObject)jsonarray.get(i1)).getString("type");
        if (jsonobject == null) goto _L4; else goto _L3
_L3:
        Object obj = com.igexin.push.extension.a.a().c().iterator();
_L8:
        if (!((Iterator) (obj)).hasNext()) goto _L6; else goto _L5
_L5:
        if (!((IPushExtension)((Iterator) (obj)).next()).isActionSupported(jsonobject)) goto _L8; else goto _L7
_L7:
        boolean flag = true;
_L21:
        if (flag) goto _L4; else goto _L9
_L9:
        if (b.get(jsonobject) == null)
        {
            return false;
        }
          goto _L4
_L20:
        if (i1 >= jsonarray.length()) goto _L11; else goto _L10
_L10:
        JSONObject jsonobject1;
        String s1;
        jsonobject1 = (JSONObject)jsonarray.get(i1);
        s1 = jsonobject1.getString("type");
        if (s1 == null) goto _L13; else goto _L12
_L12:
        obj = com.igexin.push.extension.a.a().c();
        jsonobject = null;
        Iterator iterator = ((List) (obj)).iterator();
_L15:
        if (!iterator.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        obj = ((IPushExtension)iterator.next()).parseAction(jsonobject1);
        jsonobject = ((JSONObject) (obj));
        if (obj == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        jsonobject = ((JSONObject) (obj));
        break; /* Loop/switch isn't completed */
        if (true) goto _L15; else goto _L14
_L14:
        if (jsonobject != null) goto _L17; else goto _L16
_L16:
        obj = (com.igexin.push.core.a.a.a)b.get(s1);
        if (obj == null) goto _L17; else goto _L18
_L18:
        obj = ((com.igexin.push.core.a.a.a) (obj)).a(jsonobject1);
        jsonobject = ((JSONObject) (obj));
        if (obj == null) goto _L17; else goto _L19
_L19:
        ((BaseAction) (obj)).setSupportExt(false);
        jsonobject = ((JSONObject) (obj));
          goto _L17
_L24:
        arraylist.add(jsonobject);
_L13:
        i1++;
          goto _L20
        jsonobject;
_L11:
        pushtaskbean.setActionChains(arraylist);
        return true;
_L6:
        flag = false;
          goto _L21
_L4:
        i1++;
          goto _L22
_L2:
        i1 = 0;
          goto _L20
_L17:
        if (jsonobject != null) goto _L24; else goto _L23
_L23:
        return false;
          goto _L20
    }

    public boolean a(JSONObject jsonobject, byte abyte0[], boolean flag)
    {
        if (!jsonobject.has("action") || !jsonobject.getString("action").equals("pushmessage")) goto _L2; else goto _L1
_L1:
        String s1;
        String s2;
        String s3;
        JSONArray jsonarray;
        String s4;
        Object obj;
        s4 = jsonobject.getString("id");
        s3 = jsonobject.getString("appid");
        s1 = jsonobject.getString("messageid");
        s2 = jsonobject.getString("taskid");
        obj = jsonobject.getString("appkey");
        jsonarray = jsonobject.getJSONArray("action_chains");
        com.igexin.a.a.c.a.a((new StringBuilder()).append("pushmessage|").append(s2).append("|").append(s1).append("|").append(s3).append("|").append(flag).toString());
        if (s3 == null || s4 == null || s1 == null || s2 == null || jsonarray == null) goto _L2; else goto _L3
_L3:
        if (!s3.equals(com.igexin.push.core.g.c)) goto _L2; else goto _L4
_L4:
        PushTaskBean pushtaskbean;
        pushtaskbean = new PushTaskBean();
        pushtaskbean.setAppid(s3);
        pushtaskbean.setMessageId(s1);
        pushtaskbean.setTaskId(s2);
        pushtaskbean.setId(s4);
        pushtaskbean.setAppKey(((String) (obj)));
        pushtaskbean.setCurrentActionid(1);
        if (jsonobject.has("cdnType"))
        {
            pushtaskbean.setCDNType(jsonobject.getBoolean("cdnType"));
        }
        s4 = a().a(s2, s1);
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_277;
        }
        a().a(pushtaskbean, "0");
        if (a().a(System.currentTimeMillis()))
        {
            return true;
        }
        obj = new ContentValues();
        ((ContentValues) (obj)).put("messageid", s1);
        ((ContentValues) (obj)).put("taskid", s2);
        ((ContentValues) (obj)).put("appid", s3);
        ((ContentValues) (obj)).put("key", (new StringBuilder()).append("CACHE_").append(s4).toString());
        ((ContentValues) (obj)).put("info", com.igexin.a.b.a.b(jsonobject.toString().getBytes()));
        ((ContentValues) (obj)).put("createtime", Long.valueOf(System.currentTimeMillis()));
        if (abyte0 == null)
        {
            break MISSING_BLOCK_LABEL_392;
        }
        ((ContentValues) (obj)).put("msgextra", abyte0);
        pushtaskbean.setMsgExtra(abyte0);
        boolean flag1;
        if (jsonarray.length() <= 0)
        {
            break MISSING_BLOCK_LABEL_418;
        }
        flag1 = a().a(jsonobject, pushtaskbean);
        if (!flag1)
        {
            return true;
        }
        if (!flag) goto _L6; else goto _L5
_L5:
        abyte0 = com.igexin.push.core.f.a().i().a("message", new String[] {
            "taskid"
        }, new String[] {
            s2
        }, null, null);
        if (abyte0 == null) goto _L8; else goto _L7
_L7:
        if (abyte0.getCount() != 0) goto _L10; else goto _L9
_L9:
        if (!jsonobject.has("condition")) goto _L12; else goto _L11
_L11:
        b(jsonobject, pushtaskbean);
        pushtaskbean.setStatus(com.igexin.push.core.a.k);
        ((ContentValues) (obj)).put("status", Integer.valueOf(com.igexin.push.core.a.k));
_L21:
        com.igexin.push.core.f.a().i().a("message", ((ContentValues) (obj)));
        com.igexin.push.core.g.al.put(s4, pushtaskbean);
        if (!jsonobject.has("condition")) goto _L14; else goto _L13
_L13:
        u();
_L8:
        if (abyte0 != null)
        {
            try
            {
                abyte0.close();
            }
            // Misplaced declaration of an exception variable
            catch (JSONObject jsonobject)
            {
                jsonobject.printStackTrace();
            }
        }
_L2:
        return true;
_L12:
        try
        {
            pushtaskbean.setStatus(com.igexin.push.core.a.l);
            ((ContentValues) (obj)).put("status", Integer.valueOf(com.igexin.push.core.a.l));
            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (JSONObject jsonobject) { }
        finally { }
_L19:
        if (abyte0 == null) goto _L2; else goto _L15
_L15:
        abyte0.close();
          goto _L2
_L14:
        a().a(s2, s1, com.igexin.push.core.g.c, com.igexin.push.core.g.g);
          goto _L8
_L18:
        abyte0 = null;
        if (abyte0 == null)
        {
            break MISSING_BLOCK_LABEL_631;
        }
        abyte0.close();
        throw jsonobject;
_L10:
        if (abyte0 == null) goto _L17; else goto _L16
_L16:
        abyte0.close();
        return true;
_L6:
        if (jsonobject.has("condition"))
        {
            b(jsonobject, pushtaskbean);
        }
        pushtaskbean.setStatus(com.igexin.push.core.a.l);
        com.igexin.push.core.g.al.put(s4, pushtaskbean);
          goto _L2
        jsonobject;
          goto _L18
        jsonobject;
        abyte0 = null;
          goto _L19
_L17:
        return true;
        if (true) goto _L21; else goto _L20
_L20:
    }

    public com.igexin.push.core.b b(String s1, String s2)
    {
        Object obj;
        PushTaskBean pushtaskbean;
        obj = com.igexin.push.core.b.a;
        s2 = (new StringBuilder()).append(s1).append(":").append(s2).toString();
        pushtaskbean = (PushTaskBean)com.igexin.push.core.g.al.get(s2);
        if (pushtaskbean != null) goto _L2; else goto _L1
_L1:
        obj = com.igexin.push.core.b.c;
_L4:
        return ((com.igexin.push.core.b) (obj));
_L2:
        Iterator iterator;
        int i1;
        iterator = pushtaskbean.getActionChains().iterator();
        i1 = 0;
        s2 = ((String) (obj));
_L6:
        Object obj2;
        BaseAction baseaction;
        if (!iterator.hasNext())
        {
            break MISSING_BLOCK_LABEL_246;
        }
        baseaction = (BaseAction)iterator.next();
        obj = com.igexin.push.core.b.c;
        if (baseaction == null)
        {
            return ((com.igexin.push.core.b) (obj));
        }
        Iterator iterator1 = com.igexin.push.extension.a.a().c().iterator();
        do
        {
            if (!iterator1.hasNext())
            {
                break;
            }
            com.igexin.push.core.b b1 = ((IPushExtension)iterator1.next()).prepareExecuteAction(pushtaskbean, baseaction);
            obj = b1;
            if (b1 == com.igexin.push.core.b.c)
            {
                continue;
            }
            obj = b1;
            break;
        } while (true);
        obj2 = obj;
        if (obj != com.igexin.push.core.b.c)
        {
            break; /* Loop/switch isn't completed */
        }
        obj2 = (com.igexin.push.core.a.a.a)b.get(baseaction.getType());
        if (obj2 == null) goto _L4; else goto _L3
_L3:
        obj2 = ((com.igexin.push.core.a.a.a) (obj2)).a(pushtaskbean, baseaction);
        obj = obj2;
        if (obj2 == com.igexin.push.core.b.c) goto _L4; else goto _L5
_L5:
        if (s2 == com.igexin.push.core.b.a)
        {
            s2 = ((String) (obj2));
        }
        if (obj2 == com.igexin.push.core.b.b)
        {
            i1++;
        }
          goto _L6
        Object obj1 = s2;
        if (i1 != 0)
        {
            obj1 = s2;
            if (!com.igexin.push.core.g.a(s1, Integer.valueOf(i1), true))
            {
                obj1 = com.igexin.push.core.b.a;
            }
        }
        return ((com.igexin.push.core.b) (obj1));
    }

    public String b(com.igexin.push.core.bean.f f1)
    {
        JSONObject jsonobject = new JSONObject();
        Object obj;
        Object obj1;
        Object obj2;
        try
        {
            obj2 = f1.a();
            obj1 = f1.b();
        }
        // Misplaced declaration of an exception variable
        catch (com.igexin.push.core.bean.f f1)
        {
            return null;
        }
        obj = "[]";
        if (obj2 == null)
        {
            break MISSING_BLOCK_LABEL_39;
        }
        jsonobject.put("version", obj2);
        f1 = ((com.igexin.push.core.bean.f) (obj));
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_341;
        }
        f1 = ((com.igexin.push.core.bean.f) (obj));
        if (((Map) (obj1)).size() <= 0)
        {
            break MISSING_BLOCK_LABEL_341;
        }
        f1 = "[";
        for (obj = ((Map) (obj1)).entrySet().iterator(); ((Iterator) (obj)).hasNext();)
        {
            obj1 = (com.igexin.push.core.bean.e)((java.util.Map.Entry)((Iterator) (obj)).next()).getValue();
            obj2 = new JSONObject();
            ((JSONObject) (obj2)).put("id", ((com.igexin.push.core.bean.e) (obj1)).a());
            ((JSONObject) (obj2)).put("version", ((com.igexin.push.core.bean.e) (obj1)).b());
            ((JSONObject) (obj2)).put("name", ((com.igexin.push.core.bean.e) (obj1)).c());
            ((JSONObject) (obj2)).put("cls_name", ((com.igexin.push.core.bean.e) (obj1)).d());
            ((JSONObject) (obj2)).put("url", ((com.igexin.push.core.bean.e) (obj1)).e());
            ((JSONObject) (obj2)).put("checksum", ((com.igexin.push.core.bean.e) (obj1)).f());
            ((JSONObject) (obj2)).put("isdestroy", ((com.igexin.push.core.bean.e) (obj1)).g());
            ((JSONObject) (obj2)).put("effective", ((com.igexin.push.core.bean.e) (obj1)).h());
            ((JSONObject) (obj2)).put("loadTime", ((com.igexin.push.core.bean.e) (obj1)).i());
            ((JSONObject) (obj2)).put("key", ((com.igexin.push.core.bean.e) (obj1)).j());
            f1 = (new StringBuilder()).append(f1).append(((JSONObject) (obj2)).toString()).toString();
            f1 = (new StringBuilder()).append(f1).append(",").toString();
        }

        if (f1.endsWith(","))
        {
            f1 = f1.substring(0, f1.length() - 1);
        }
        f1 = (new StringBuilder()).append(f1).append("]").toString();
        jsonobject.put("extensions", new JSONArray(f1));
        f1 = jsonobject.toString();
        return f1;
    }

    public void b()
    {
        d();
    }

    public void b(int i1, String s1)
    {
        com.igexin.push.a.k.i = i1;
        com.igexin.push.a.a.a().d();
    }

    public void b(Intent intent)
    {
        if (intent != null && intent.hasExtra("isSlave") && intent.getBooleanExtra("isSlave", false))
        {
            com.igexin.push.core.f.a().a(true);
            if (intent.hasExtra("op_app"))
            {
                com.igexin.push.core.g.E = intent.getStringExtra("op_app");
            } else
            {
                com.igexin.push.core.g.E = "";
            }
            if (com.igexin.push.core.g.o)
            {
                l();
            }
        }
    }

    public void b(PushTaskBean pushtaskbean, String s1)
    {
        String s2 = (new StringBuilder()).append(pushtaskbean.getMessageId()).append("|").append(s1).toString();
        if (com.igexin.push.core.g.ao.containsKey(s2))
        {
            com.igexin.push.c.c.c c1 = (com.igexin.push.c.c.c)com.igexin.push.core.g.ao.get(s2);
            if (c1.c() < 2)
            {
                com.igexin.push.core.f.a().e().a((new StringBuilder()).append("C-").append(com.igexin.push.core.g.u).toString(), c1);
                c1.a(c1.c() + 1);
                a(c1, pushtaskbean, s1, s2);
            }
        } else
        {
            com.igexin.push.c.c.c c2 = new com.igexin.push.c.c.c();
            long l1 = System.currentTimeMillis();
            c2.a();
            c2.c = (new StringBuilder()).append("FDB").append(pushtaskbean.getMessageId()).append("|").append(pushtaskbean.getTaskId()).append("|").append(s1).append("|").append("ok").append("|").append(l1).toString();
            c2.d = com.igexin.push.core.g.u;
            c2.a = (int)l1;
            com.igexin.push.core.f.a().e().a((new StringBuilder()).append("C-").append(com.igexin.push.core.g.u).toString(), c2);
            a(c2, pushtaskbean, s1, s2);
        }
        com.igexin.a.a.c.a.a((new StringBuilder()).append("cdnfeedback|").append(pushtaskbean.getTaskId()).append("|").append(pushtaskbean.getMessageId()).append("|").append(s1).toString());
    }

    public void b(String s1)
    {
        if (s1 != null)
        {
            long l1 = System.currentTimeMillis();
            s1 = (new StringBuilder()).append("{\"action\":\"bindapp\",\"appid\":\"").append(s1).append("\",\"cid\":\"").append(com.igexin.push.core.g.u).append("\",\"id\":\"").append(l1).append("\",\"type\":\"bind\"}").toString();
            com.igexin.push.core.c.c c1 = com.igexin.push.core.c.c.a();
            if (c1 != null)
            {
                c1.a(new com.igexin.push.core.bean.i(l1, s1, (byte)1, l1));
            }
            if (s1 != null)
            {
                com.igexin.push.c.c.d d1 = new com.igexin.push.c.c.d();
                d1.a();
                d1.a = (int)l1;
                d1.d = "17258000";
                d1.e = s1;
                d1.g = com.igexin.push.core.g.u;
                com.igexin.push.core.f.a().e().a((new StringBuilder()).append("C-").append(com.igexin.push.core.g.u).toString(), d1);
            }
        }
    }

    public void b(String s1, String s2, String s3, String s4)
    {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putInt("action", 10001);
        bundle.putString("taskid", s1);
        bundle.putString("messageid", s2);
        bundle.putString("appid", s3);
        bundle.putString("packagename", com.igexin.push.core.g.g);
        intent.setAction((new StringBuilder()).append("com.igexin.sdk.action.").append(s3).toString());
        if (s4 != null)
        {
            s1 = s4.getBytes();
        } else
        {
            s1 = a(s1, s2);
            s1 = (PushTaskBean)com.igexin.push.core.g.al.get(s1);
            if (s1 != null)
            {
                s1 = s1.getMsgExtra();
            } else
            {
                s1 = null;
            }
        }
        if (s1 == null);
        bundle.putByteArray("payload", s1);
        intent.putExtras(bundle);
        try
        {
            com.igexin.a.a.c.a.a((new StringBuilder()).append("startapp|broadcast|").append(s3).append("|").append(new String(s1, "utf-8")).toString());
        }
        // Misplaced declaration of an exception variable
        catch (String s1) { }
        com.igexin.push.core.g.i.sendBroadcast(intent);
    }

    public boolean b(String s1, String s2, String s3)
    {
        Object obj;
        PushTaskBean pushtaskbean;
        obj = (new StringBuilder()).append(s1).append(":").append(s2).toString();
        obj = (PushTaskBean)com.igexin.push.core.g.al.get(obj);
        pushtaskbean = ((PushTaskBean) (obj));
        if (obj != null)
        {
            break MISSING_BLOCK_LABEL_311;
        }
        Cursor cursor = com.igexin.push.core.f.a().i().a("message", new String[] {
            "taskid", "messageid"
        }, new String[] {
            s1, s2
        }, null, null);
        if (cursor == null) goto _L2; else goto _L1
_L1:
        pushtaskbean = ((PushTaskBean) (obj));
        if (cursor.getCount() <= 0) goto _L4; else goto _L3
_L3:
        pushtaskbean = ((PushTaskBean) (obj));
        if (!cursor.moveToNext())
        {
            break; /* Loop/switch isn't completed */
        }
        pushtaskbean = ((PushTaskBean) (obj));
        byte abyte0[] = cursor.getBlob(cursor.getColumnIndexOrThrow("info"));
        pushtaskbean = ((PushTaskBean) (obj));
        byte abyte1[] = cursor.getBlob(cursor.getColumnIndexOrThrow("msgextra"));
        pushtaskbean = ((PushTaskBean) (obj));
        a(new JSONObject(new String(com.igexin.a.b.a.c(abyte0))), abyte1, false);
        pushtaskbean = ((PushTaskBean) (obj));
        obj = (PushTaskBean)com.igexin.push.core.g.al.get((new StringBuilder()).append(s1).append(":").append(s2).toString());
        if (obj == null)
        {
            if (cursor != null)
            {
                cursor.close();
            }
            return false;
        }
        if (true) goto _L3; else goto _L5
_L4:
        if (cursor != null)
        {
            cursor.close();
        }
        return false;
_L2:
        if (cursor != null)
        {
            cursor.close();
        }
        return false;
_L5:
        pushtaskbean = ((PushTaskBean) (obj));
        if (cursor != null)
        {
            cursor.close();
            pushtaskbean = ((PushTaskBean) (obj));
        }
_L7:
        a().a(pushtaskbean, s3);
        s1 = pushtaskbean.getBaseAction(s3);
        if (s1 == null)
        {
            return false;
        }
        break; /* Loop/switch isn't completed */
        s1;
        s1 = null;
_L10:
        pushtaskbean = ((PushTaskBean) (obj));
        if (s1 != null)
        {
            s1.close();
            pushtaskbean = ((PushTaskBean) (obj));
        }
        if (true) goto _L7; else goto _L6
        s1;
        cursor = null;
_L9:
        if (cursor != null)
        {
            cursor.close();
        }
        throw s1;
_L6:
label0:
        {
            if (!s1.isSupportExt())
            {
                break label0;
            }
            s2 = com.igexin.push.extension.a.a().c().iterator();
            do
            {
                if (!s2.hasNext())
                {
                    break label0;
                }
            } while (!((IPushExtension)s2.next()).executeAction(pushtaskbean, s1));
            return true;
        }
        s2 = (com.igexin.push.core.a.a.a)b.get(s1.getType());
        if (s2 == null || pushtaskbean.isStop())
        {
            return false;
        } else
        {
            return s2.b(pushtaskbean, s1);
        }
        s1;
        if (true) goto _L9; else goto _L8
_L8:
        s1;
        s1 = cursor;
        obj = pushtaskbean;
          goto _L10
    }

    public com.igexin.push.c.c.i c()
    {
        com.igexin.push.c.c.i i1;
        i1 = new com.igexin.push.c.c.i();
        i1.a = com.igexin.push.core.g.t;
        i1.b = 0;
        i1.c = 65280;
        ArrayList arraylist;
        WifiManager wifimanager;
        if (!E())
        {
            break MISSING_BLOCK_LABEL_170;
        }
        arraylist = new ArrayList();
        wifimanager = (WifiManager)com.igexin.push.core.g.i.getSystemService("wifi");
        if (wifimanager == null)
        {
            break MISSING_BLOCK_LABEL_156;
        }
        Object obj1;
        if (!wifimanager.isWifiEnabled())
        {
            break MISSING_BLOCK_LABEL_156;
        }
        obj1 = wifimanager.getConnectionInfo();
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_156;
        }
        Object obj;
        com.igexin.push.c.c.j j1;
        try
        {
            obj = ((WifiInfo) (obj1)).getSSID();
            obj1 = ((WifiInfo) (obj1)).getBSSID();
        }
        catch (Exception exception)
        {
            return i1;
        }
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_124;
        }
        j1 = new com.igexin.push.c.c.j();
        j1.a = 1;
        j1.b = obj;
        arraylist.add(j1);
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_156;
        }
        obj = new com.igexin.push.c.c.j();
        obj.a = 4;
        obj.b = obj1;
        arraylist.add(obj);
        if (arraylist.size() > 0)
        {
            i1.d = arraylist;
        }
        return i1;
    }

    public void c(Intent intent)
    {
        Object obj;
        int i1;
        obj = null;
        i1 = 0;
        if (intent != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (!"android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction()))
        {
            break MISSING_BLOCK_LABEL_145;
        }
        if (com.igexin.a.a.b.d.c() == null) goto _L1; else goto _L3
_L3:
        com.igexin.push.core.i.a().a(com.igexin.push.core.k.d);
        if (com.igexin.push.core.f.a().h().getActiveNetworkInfo() == null || !com.igexin.push.core.f.a().h().getActiveNetworkInfo().isAvailable()) goto _L5; else goto _L4
_L4:
        com.igexin.push.core.g.k = true;
_L8:
        if (com.igexin.push.core.g.o) goto _L7; else goto _L6
_L6:
        com.igexin.push.core.f.a().e().c(true);
_L10:
        if (v())
        {
            u();
            return;
        }
          goto _L1
_L5:
        Exception exception;
        Object obj1;
        Exception exception1;
        String s1;
        String s2;
        ContentValues contentvalues;
        int j1;
        boolean flag;
        try
        {
            com.igexin.push.core.g.k = false;
        }
        // Misplaced declaration of an exception variable
        catch (Intent intent)
        {
            return;
        }
          goto _L8
_L7:
        if (System.currentTimeMillis() - com.igexin.push.core.g.U <= 5000L) goto _L10; else goto _L9
_L9:
        com.igexin.push.core.g.U = System.currentTimeMillis();
        if (f() == -2)
        {
            com.igexin.push.core.g.o = false;
            m();
        }
          goto _L10
        if ("com.igexin.sdk.action.snlrefresh".equals(intent.getAction()) || com.igexin.push.core.g.W.equals(intent.getAction()) || "com.igexin.sdk.action.snlretire".equals(intent.getAction()))
        {
            com.igexin.push.core.f.a().f().a(intent);
            return;
        }
        if (!"com.igexin.sdk.action.execute".equals(intent.getAction())) goto _L12; else goto _L11
_L11:
        obj1 = intent.getStringExtra("taskid");
        s1 = intent.getStringExtra("messageid");
        String s3 = intent.getStringExtra("appid");
        s2 = intent.getStringExtra("pkgname");
        contentvalues = new ContentValues();
        intent = (new StringBuilder()).append("EXEC_").append(((String) (obj1))).toString();
        contentvalues.put("taskid", ((String) (obj1)));
        contentvalues.put("appid", s3);
        contentvalues.put("key", intent);
        contentvalues.put("createtime", Long.valueOf(System.currentTimeMillis()));
        intent = com.igexin.push.core.f.a().i().a("message", new String[] {
            "key"
        }, new String[] {
            intent
        }, null, null);
        if (intent == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (intent.getCount() != 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        com.igexin.push.core.f.a().i().a("message", contentvalues);
        flag = s2.equals(com.igexin.push.core.g.g);
        if (!flag)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (s1 != null && obj1 != null)
        {
            break MISSING_BLOCK_LABEL_403;
        }
        if (intent == null) goto _L1; else goto _L13
_L13:
        intent.close();
        return;
        if (com.igexin.push.core.f.a() != null && b(((String) (obj1)), s1) == com.igexin.push.core.b.a)
        {
            a(((String) (obj1)), s1, "1");
        }
        if (intent == null) goto _L1; else goto _L14
_L14:
        intent.close();
        return;
_L36:
        if (intent == null) goto _L1; else goto _L15
_L15:
        intent.close();
        return;
_L35:
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_465;
        }
        ((Cursor) (obj)).close();
        throw intent;
_L12:
        if (!"com.igexin.sdk.action.doaction".equals(intent.getAction())) goto _L17; else goto _L16
_L16:
        obj = intent.getStringExtra("taskid");
        obj1 = intent.getStringExtra("messageid");
        s1 = intent.getStringExtra("actionid");
        s2 = intent.getStringExtra("accesstoken");
        i1 = intent.getIntExtra("notifID", 0);
        intent = (NotificationManager)com.igexin.push.core.g.i.getSystemService("notification");
        if (i1 == 0) goto _L19; else goto _L18
_L18:
        intent.cancel(i1);
_L22:
        if (!s2.equals(com.igexin.push.core.g.ax)) goto _L1; else goto _L20
_L20:
        b(((String) (obj)), ((String) (obj1)), s1);
        return;
_L19:
        if (com.igexin.push.core.g.am.get(obj) == null) goto _L22; else goto _L21
_L21:
        intent.cancel(((Integer)com.igexin.push.core.g.am.get(obj)).intValue());
          goto _L22
_L17:
        if (!"android.intent.action.TIME_SET".equals(intent.getAction()))
        {
            break MISSING_BLOCK_LABEL_626;
        }
        if (com.igexin.push.a.k.f == 0) goto _L1; else goto _L23
_L23:
        com.igexin.push.e.b.g.g().h();
        return;
        if (!"android.intent.action.SCREEN_ON".equals(intent.getAction()))
        {
            break MISSING_BLOCK_LABEL_655;
        }
        com.igexin.push.core.g.s = 1;
        if (!v()) goto _L1; else goto _L24
_L24:
        u();
        return;
        if ("android.intent.action.SCREEN_OFF".equals(intent.getAction()))
        {
            com.igexin.push.core.g.s = 0;
            return;
        }
        if (!"android.intent.action.PACKAGE_ADDED".equals(intent.getAction()))
        {
            break MISSING_BLOCK_LABEL_821;
        }
        intent = intent.getDataString();
        if (intent == null) goto _L1; else goto _L25
_L25:
        if (!intent.startsWith("package:")) goto _L1; else goto _L26
_L26:
        intent = intent.substring(8);
        try
        {
            obj = com.igexin.push.core.g.i.getPackageManager().getPackageInfo(intent, 4);
        }
        // Misplaced declaration of an exception variable
        catch (Intent intent)
        {
            return;
        }
        if (obj == null) goto _L1; else goto _L27
_L27:
        obj = ((PackageInfo) (obj)).services;
        if (obj == null) goto _L1; else goto _L28
_L28:
        j1 = obj.length;
_L30:
        if (i1 >= j1) goto _L1; else goto _L29
_L29:
        obj1 = obj[i1];
        if (com.igexin.push.core.a.o.equals(((ServiceInfo) (obj1)).name) || com.igexin.push.core.a.n.equals(((ServiceInfo) (obj1)).name) || com.igexin.push.core.a.p.equals(((ServiceInfo) (obj1)).name))
        {
            com.igexin.push.core.c.f.a().c().put(intent, ((ServiceInfo) (obj1)).name);
            return;
        }
        i1++;
          goto _L30
        if (!"android.intent.action.PACKAGE_REMOVED".equals(intent.getAction()))
        {
            continue; /* Loop/switch isn't completed */
        }
        intent = intent.getDataString();
        if (intent == null) goto _L1; else goto _L31
_L31:
        if (!intent.startsWith("package:")) goto _L1; else goto _L32
_L32:
        intent = intent.substring(8);
        if (!com.igexin.push.core.c.f.a().c().containsKey(intent)) goto _L1; else goto _L33
_L33:
        com.igexin.push.core.c.f.a().c().remove(intent);
        return;
        if (!"com.igexin.sdk.action.core.clearmsg".equals(intent.getAction())) goto _L1; else goto _L34
_L34:
        com.igexin.push.core.f.a().i().a("message", null);
        return;
        exception1;
        obj = intent;
        intent = exception1;
          goto _L35
        exception;
          goto _L36
        intent;
        intent = null;
          goto _L36
        intent;
          goto _L35
    }

    public void c(String s1)
    {
        if (com.igexin.push.core.g.u != null)
        {
            long l1 = System.currentTimeMillis();
            s1 = (new StringBuilder()).append("{\"action\":\"set_tag\",\"id\":\"").append(l1).append("\", \"cid\":\"").append(com.igexin.push.core.g.u).append("\", \"appid\":\"").append(com.igexin.push.core.g.c).append("\", \"tags\":\"").append(s1).append("\"}").toString();
            Object obj = com.igexin.push.core.c.c.a();
            if (obj != null)
            {
                ((com.igexin.push.core.c.c) (obj)).a(new com.igexin.push.core.bean.i(l1, s1, (byte)2, l1));
            }
            obj = new com.igexin.push.c.c.d();
            ((com.igexin.push.c.c.d) (obj)).a();
            obj.d = "17258000";
            obj.e = s1;
            com.igexin.a.a.b.d.c().a(com.igexin.push.core.g.a, 3, com.igexin.push.core.f.a().d(), obj, false);
            com.igexin.a.a.c.a.a("settag");
        }
    }

    public int d()
    {
        boolean flag1 = true;
        if (com.igexin.push.core.g.l && com.igexin.push.core.g.m && !a(System.currentTimeMillis()) && n()) goto _L2; else goto _L1
_L1:
        byte byte0 = -1;
_L4:
        return byte0;
_L2:
        if (com.igexin.push.core.g.n)
        {
            com.igexin.push.c.c.f f1;
            ContentValues contentvalues;
            boolean flag2;
            if (!com.igexin.push.core.g.n)
            {
                flag2 = true;
            } else
            {
                flag2 = false;
            }
            com.igexin.push.core.g.n = flag2;
            com.igexin.push.core.g.O = (long)Math.abs((new Random()).nextInt() % 24) * 0x36ee80L + System.currentTimeMillis();
        }
        com.igexin.push.core.c.r.b();
        if (com.igexin.push.core.g.t == 0L)
        {
            com.igexin.a.a.c.a.a("registerReq");
            f1 = new com.igexin.push.c.c.f(com.igexin.push.core.g.w, com.igexin.push.core.g.x, com.igexin.push.core.g.C, com.igexin.push.core.g.c);
            Object obj;
            Exception exception;
            Object obj1;
            Object obj2;
            Exception exception1;
            Object obj3;
            int i1;
            boolean flag;
            if (com.igexin.push.core.f.a().e().a((new StringBuilder()).append("R-").append(com.igexin.push.core.g.C).toString(), f1) < 0)
            {
                i1 = 0;
            } else
            {
                i1 = 1;
            }
        } else
        {
            obj = c();
            com.igexin.a.a.c.a.a((new StringBuilder()).append("loginReqBefore|").append(((com.igexin.push.c.c.i) (obj)).a).toString());
            if (com.igexin.push.core.f.a().e().a((new StringBuilder()).append("S-").append(String.valueOf(com.igexin.push.core.g.t)).toString(), ((com.igexin.push.c.c.e) (obj))) < 0)
            {
                flag = false;
            } else
            {
                flag = true;
            }
            i1 = ((flag) ? 1 : 0);
            if (flag)
            {
                com.igexin.a.a.c.a.a((new StringBuilder()).append("loginReq|").append(com.igexin.push.core.g.u).toString());
                i1 = ((flag) ? 1 : 0);
            }
        }
        byte0 = flag1;
        if (i1 != 0) goto _L4; else goto _L3
_L3:
        obj1 = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());
        obj = com.igexin.push.core.f.a().i().a("bi", new String[] {
            "type"
        }, new String[] {
            "1"
        }, null, null);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_289;
        }
        if (((Cursor) (obj)).getCount() != 0)
        {
            break MISSING_BLOCK_LABEL_695;
        }
        contentvalues = new ContentValues();
        contentvalues.put("loginerror_nonetwork_count", Integer.valueOf(1));
        contentvalues.put("create_time", ((String) (obj1)));
        contentvalues.put("type", "1");
        com.igexin.push.core.f.a().i().a("bi", contentvalues);
_L6:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
_L10:
        return 0;
_L9:
        if (!((Cursor) (obj)).moveToNext()) goto _L6; else goto _L5
_L5:
        obj3 = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndexOrThrow("create_time"));
        obj2 = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndexOrThrow("id"));
        if (!((String) (obj1)).equals(obj3)) goto _L8; else goto _L7
_L7:
        i1 = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndexOrThrow("loginerror_nonetwork_count"));
        obj3 = new ContentValues();
        ((ContentValues) (obj3)).put("loginerror_nonetwork_count", Integer.valueOf(i1 + 1));
        com.igexin.push.core.f.a().i().a("bi", ((ContentValues) (obj3)), new String[] {
            "id"
        }, new String[] {
            obj2
        });
          goto _L9
        obj1;
_L13:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
          goto _L10
_L8:
        obj3 = new ContentValues();
        ((ContentValues) (obj3)).put("type", "2");
        com.igexin.push.core.f.a().i().a("bi", ((ContentValues) (obj3)), new String[] {
            "id"
        }, new String[] {
            obj2
        });
        obj2 = new ContentValues();
        ((ContentValues) (obj2)).put("loginerror_nonetwork_count", Integer.valueOf(i1 + 1));
        ((ContentValues) (obj2)).put("create_time", ((String) (obj1)));
        ((ContentValues) (obj2)).put("type", "1");
        com.igexin.push.core.f.a().i().a("bi", ((ContentValues) (obj2)));
          goto _L9
        exception1;
        obj1 = obj;
        obj = exception1;
_L12:
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
        throw obj;
        obj;
        obj1 = null;
        if (true) goto _L12; else goto _L11
_L11:
        exception;
        exception = null;
          goto _L13
        i1 = 0;
          goto _L9
    }

    public void d(Intent intent)
    {
        if (intent != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        int i1;
        boolean flag1;
        i1 = intent.getIntExtra("id", -1);
        flag1 = intent.getBooleanExtra("result", false);
        if (i1 == -1) goto _L1; else goto _L3
_L3:
        com.igexin.push.core.g.au++;
        if (!flag1)
        {
            continue; /* Loop/switch isn't completed */
        }
        com.igexin.push.core.g.at++;
        Object obj;
        Object obj1;
        File file;
        boolean flag;
        if (com.igexin.push.core.g.av != null)
        {
            obj = com.igexin.push.core.g.av.b();
        } else
        {
            obj = null;
        }
        if (obj == null) goto _L1; else goto _L4
_L4:
        if (com.igexin.push.a.k.x == null)
        {
            break MISSING_BLOCK_LABEL_373;
        }
        intent = com.igexin.push.a.k.x.b();
        if (intent == null) goto _L1; else goto _L5
_L5:
        if (intent.containsKey(Integer.valueOf(i1)))
        {
            flag = true;
            obj1 = (com.igexin.push.core.bean.e)intent.get(Integer.valueOf(i1));
            if (obj1 != null)
            {
                obj1 = new File((new StringBuilder()).append(com.igexin.push.core.g.ad).append("/").append(((com.igexin.push.core.bean.e) (obj1)).c()).toString());
                if (((File) (obj1)).exists())
                {
                    ((File) (obj1)).delete();
                }
            }
            intent.remove(Integer.valueOf(i1));
        } else
        {
            flag = false;
        }
        obj = (com.igexin.push.core.bean.e)((Map) (obj)).get(Integer.valueOf(i1));
        if (obj == null) goto _L1; else goto _L6
_L6:
        obj1 = (new StringBuilder()).append(com.igexin.push.core.g.ad).append("/").append(((com.igexin.push.core.bean.e) (obj)).c()).toString();
        file = new File(((String) (obj1)));
        if (file.exists())
        {
            intent.put(Integer.valueOf(i1), obj);
            if (com.igexin.push.core.g.at == com.igexin.push.core.g.as)
            {
                com.igexin.push.a.k.x.a(com.igexin.push.core.g.av.a());
            }
            if (!flag && com.igexin.push.extension.a.a().a(com.igexin.push.core.g.i, ((String) (obj1)), ((com.igexin.push.core.bean.e) (obj)).d(), ((com.igexin.push.core.bean.e) (obj)).j(), ((com.igexin.push.core.bean.e) (obj)).c()))
            {
                ((com.igexin.push.core.bean.e) (obj)).b(System.currentTimeMillis());
                if (((com.igexin.push.core.bean.e) (obj)).g())
                {
                    file.delete();
                    intent.remove(Integer.valueOf(i1));
                }
            }
            com.igexin.push.a.a.a().g();
        }
        if (com.igexin.push.core.g.au != com.igexin.push.core.g.as || !com.igexin.push.core.g.aw) goto _L1; else goto _L7
_L7:
        Process.killProcess(Process.myPid());
        return;
        intent = new HashMap();
        obj1 = new com.igexin.push.core.bean.f();
        ((com.igexin.push.core.bean.f) (obj1)).a("0");
        ((com.igexin.push.core.bean.f) (obj1)).a(intent);
        com.igexin.push.a.k.x = ((com.igexin.push.core.bean.f) (obj1));
        flag = false;
        break MISSING_BLOCK_LABEL_182;
    }

    public boolean d(String s1)
    {
        Object obj = com.igexin.push.core.g.i.getPackageManager();
        Intent intent = new Intent("android.intent.action.MAIN", null);
        intent.addCategory("android.intent.category.LAUNCHER");
        for (obj = ((PackageManager) (obj)).queryIntentActivities(intent, 0).iterator(); ((Iterator) (obj)).hasNext();)
        {
            if (((ResolveInfo)((Iterator) (obj)).next()).activityInfo.packageName.equals(s1))
            {
                return true;
            }
        }

        return false;
    }

    public void e()
    {
        com.igexin.a.a.b.d.c().a(com.igexin.push.core.g.a.replaceFirst("socket", "disConnect"), 0, null);
    }

    public boolean e(String s1)
    {
        for (Iterator iterator = ((ActivityManager)com.igexin.push.core.g.i.getSystemService("activity")).getRunningAppProcesses().iterator(); iterator.hasNext();)
        {
            if (s1.equals(((android.app.ActivityManager.RunningAppProcessInfo)iterator.next()).processName))
            {
                return true;
            }
        }

        return false;
    }

    public int f()
    {
        return com.igexin.push.core.f.a().e().a((new StringBuilder()).append("H-").append(com.igexin.push.core.g.u).toString(), new com.igexin.push.c.c.h());
    }

    public void f(String s1)
    {
        String s2 = a(true, 4);
        s2 = (new StringBuilder()).append(s2).append("2.3.0.0|sdkconfig-error|").toString();
        s1 = (new StringBuilder()).append(s2).append(s1).toString().getBytes();
        s1 = new com.igexin.push.e.a.c(new com.igexin.push.core.d.g(com.igexin.push.core.g.a(), s1, 0, true));
        com.igexin.a.a.b.d.c().a(s1, false, true);
    }

    public String g(String s1)
    {
        if (com.igexin.push.core.g.c() == null)
        {
            return null;
        } else
        {
            return (String)com.igexin.push.core.g.c().get(s1);
        }
    }

    public void g()
    {
        Object obj = com.igexin.push.core.c.c.a().b().iterator();
        do
        {
            if (!((Iterator) (obj)).hasNext())
            {
                break;
            }
            com.igexin.push.core.bean.i i1 = (com.igexin.push.core.bean.i)((Iterator) (obj)).next();
            if (i1.d() + 10000L > System.currentTimeMillis())
            {
                continue;
            }
            long l1 = System.currentTimeMillis();
            obj = new com.igexin.push.c.c.d();
            ((com.igexin.push.c.c.d) (obj)).a();
            obj.a = (int)l1;
            obj.d = "17258000";
            obj.e = i1.b();
            obj.g = com.igexin.push.core.g.u;
            com.igexin.push.core.f.a().e().a((new StringBuilder()).append("C-").append(com.igexin.push.core.g.u).toString(), ((com.igexin.push.c.c.e) (obj)));
            com.igexin.a.a.c.a.a((new StringBuilder()).append("freshral|").append(i1.b()).toString());
            break;
        } while (true);
    }

    public void h()
    {
        long l1 = System.currentTimeMillis();
        String s1 = (new StringBuilder()).append("{\"action\":\"request_deviceid\",\"id\":\"").append(l1).append("\"}").toString();
        com.igexin.push.c.c.d d1 = new com.igexin.push.c.c.d();
        d1.a();
        d1.a = (int)l1;
        d1.d = "17258000";
        d1.e = s1;
        d1.g = com.igexin.push.core.g.u;
        com.igexin.push.core.f.a().e().a((new StringBuilder()).append("C-").append(com.igexin.push.core.g.u).toString(), d1);
        com.igexin.a.a.c.a.a("deviceidReq");
    }

    public void i()
    {
        String s1;
        long l1;
        long l2;
        s1 = null;
        l2 = -1L;
        l1 = l2;
        Object obj = new com.igexin.push.core.bean.a();
        l1 = l2;
        l2 = ((com.igexin.push.core.bean.a) (obj)).l;
        l1 = l2;
        obj = com.igexin.push.core.bean.a.a(((com.igexin.push.core.bean.a) (obj)));
        s1 = ((String) (obj));
_L2:
        if (s1 != null)
        {
            com.igexin.a.a.c.a.a("addphoneinfo");
            Object obj1 = com.igexin.push.core.c.c.a();
            if (obj1 != null)
            {
                ((com.igexin.push.core.c.c) (obj1)).a(new com.igexin.push.core.bean.i(l2, s1, (byte)5, l2));
            }
            obj1 = new com.igexin.push.c.c.d();
            ((com.igexin.push.c.c.d) (obj1)).a();
            obj1.a = (int)l2;
            obj1.d = "17258000";
            obj1.e = s1;
            obj1.g = com.igexin.push.core.g.u;
            com.igexin.push.core.f.a().e().a((new StringBuilder()).append("C-").append(com.igexin.push.core.g.u).toString(), ((com.igexin.push.c.c.e) (obj1)));
        }
        return;
        JSONException jsonexception;
        jsonexception;
        l2 = l1;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public void j()
    {
        long l1 = System.currentTimeMillis();
        String s1 = (new StringBuilder()).append("{\"action\":\"request_ca_list\",\"id\":\"").append(l1).append("\", \"appid\":\"").append(com.igexin.push.core.g.c).append("\", \"cid\":\"").append(com.igexin.push.core.g.u).append("\"}").toString();
        com.igexin.push.c.c.d d1 = new com.igexin.push.c.c.d();
        d1.a();
        d1.a = (int)l1;
        d1.d = "17258000";
        d1.e = s1;
        d1.g = com.igexin.push.core.g.u;
        com.igexin.push.core.f.a().e().a((new StringBuilder()).append("C-").append(com.igexin.push.core.g.u).toString(), d1);
    }

    public long k()
    {
        return (long)((new Random()).nextInt(6) + 2) * 60000L;
    }

    public void l()
    {
        Intent intent = new Intent();
        intent.setAction((new StringBuilder()).append("com.igexin.sdk.action.").append(com.igexin.push.core.g.c).toString());
        Bundle bundle = new Bundle();
        bundle.putInt("action", 10002);
        bundle.putString("clientid", com.igexin.push.core.g.u);
        intent.putExtras(bundle);
        com.igexin.push.core.f.a().a(intent);
        Log.d("PushService", (new StringBuilder()).append("clientid is ").append(com.igexin.push.core.g.u).toString());
    }

    public void m()
    {
        Intent intent = new Intent();
        intent.setAction((new StringBuilder()).append("com.igexin.sdk.action.").append(com.igexin.push.core.g.c).toString());
        Bundle bundle = new Bundle();
        bundle.putInt("action", 10007);
        bundle.putBoolean("onlineState", com.igexin.push.core.g.o);
        intent.putExtras(bundle);
        com.igexin.push.core.f.a().a(intent);
    }

    public boolean n()
    {
        return System.currentTimeMillis() > com.igexin.push.a.k.g;
    }

    public String o()
    {
        if (!(new File(com.igexin.push.core.g.aa)).exists()) goto _L2; else goto _L1
_L1:
        Object obj;
        Object obj1;
        byte abyte0[] = new byte[1024];
        Exception exception;
        ByteArrayOutputStream bytearrayoutputstream;
        int i1;
        try
        {
            obj = new FileInputStream(com.igexin.push.core.g.aa);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            obj = null;
            obj1 = null;
            continue; /* Loop/switch isn't completed */
        }
        finally
        {
            bytearrayoutputstream = null;
            obj = null;
        }
        bytearrayoutputstream = new ByteArrayOutputStream();
_L5:
        i1 = ((FileInputStream) (obj)).read(abyte0);
        if (i1 == -1) goto _L4; else goto _L3
_L3:
        bytearrayoutputstream.write(abyte0, 0, i1);
          goto _L5
        exception;
        obj1 = obj;
        obj = bytearrayoutputstream;
_L10:
        if (obj1 != null)
        {
            try
            {
                ((FileInputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1) { }
        }
        if (obj == null)
        {
            break; /* Loop/switch isn't completed */
        }
        try
        {
            ((ByteArrayOutputStream) (obj)).close();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            return null;
        }
        obj = null;
_L7:
        return ((String) (obj));
_L4:
        obj1 = new String(bytearrayoutputstream.toByteArray());
        if (obj != null)
        {
            try
            {
                ((FileInputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        obj = obj1;
        if (bytearrayoutputstream == null) goto _L7; else goto _L6
_L6:
        try
        {
            bytearrayoutputstream.close();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            return ((String) (obj1));
        }
        return ((String) (obj1));
_L9:
        if (obj != null)
        {
            try
            {
                ((FileInputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        if (bytearrayoutputstream != null)
        {
            try
            {
                bytearrayoutputstream.close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        throw obj1;
        obj1;
        bytearrayoutputstream = null;
        continue; /* Loop/switch isn't completed */
        obj1;
        if (true) goto _L9; else goto _L8
_L8:
        Exception exception1;
        exception1;
        Object obj2 = null;
        exception1 = ((Exception) (obj));
        obj = obj2;
        if (true) goto _L10; else goto _L2
_L2:
        return null;
    }

    public void p()
    {
        ArrayList arraylist;
        int j1;
        arraylist = new ArrayList();
        a(arraylist);
        j1 = arraylist.size();
        if (j1 > 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        JSONObject jsonobject = new JSONObject();
        JSONArray jsonarray;
        jsonobject.put("action", "reportapplist");
        jsonobject.put("session_last", com.igexin.push.core.g.t);
        jsonarray = new JSONArray();
        int i1 = 0;
_L4:
        if (i1 >= j1)
        {
            break; /* Loop/switch isn't completed */
        }
        JSONObject jsonobject1 = new JSONObject();
        jsonobject1.put("appid", ((l)arraylist.get(i1)).c());
        jsonobject1.put("name", ((l)arraylist.get(i1)).a());
        jsonobject1.put("version", ((l)arraylist.get(i1)).b());
        jsonarray.put(jsonobject1);
        i1++;
        if (true) goto _L4; else goto _L3
_L3:
        byte abyte0[];
        try
        {
            jsonobject.put("applist", jsonarray);
        }
        catch (JSONException jsonexception) { }
        abyte0 = com.igexin.a.b.a.b(jsonobject.toString().getBytes());
        if (abyte0 != null)
        {
            com.igexin.push.e.a.c c1 = new com.igexin.push.e.a.c(new com.igexin.push.core.d.a(com.igexin.push.core.g.a(), abyte0));
            com.igexin.a.a.b.d.c().a(c1, false, true);
            h(q());
            com.igexin.a.a.c.a.a("reportapplist");
            return;
        }
        if (true) goto _L1; else goto _L5
_L5:
    }

    public String q()
    {
        ArrayList arraylist = new ArrayList();
        ArrayList arraylist1 = new ArrayList();
        a(arraylist1);
        int j1 = arraylist1.size();
        if (j1 > 0)
        {
            for (int i1 = 0; i1 < j1; i1++)
            {
                arraylist.add(((l)arraylist1.get(i1)).c());
            }

        }
        return arraylist.toString();
    }

    public boolean r()
    {
        String s1;
        if (com.igexin.push.core.g.i != null);
        s1 = com.igexin.push.core.g.i.getApplicationContext().getPackageName();
        Object aobj[] = com.igexin.push.core.g.i.getPackageManager().getPackageInfo(s1, 4).services;
        if (aobj == null) goto _L2; else goto _L1
_L1:
        int k1 = aobj.length;
        int i1;
        int j1;
        int l1;
        int j2;
        j1 = 0;
        i1 = 0;
        while (j1 < k1) 
        {
            ActivityInfo aactivityinfo[];
            android.content.pm.PackageManager.NameNotFoundException namenotfoundexception;
            int i2;
            int k2;
            int l2;
            try
            {
                l1 = aobj[j1].name.indexOf("DownloadService");
            }
            catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception1)
            {
                j1 = 0;
                j2 = i1;
                i1 = 0;
                continue; /* Loop/switch isn't completed */
            }
            if (l1 != -1)
            {
                i1 = 1;
            }
            j1++;
        }
        k1 = i1;
_L10:
        try
        {
            aobj = com.igexin.push.core.g.i.getPackageManager().getPackageInfo(s1, 8).providers;
        }
        catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception2)
        {
            i1 = 0;
            j1 = 0;
            j2 = k1;
            continue; /* Loop/switch isn't completed */
        }
        if (aobj == null) goto _L4; else goto _L3
_L3:
        l1 = aobj.length;
        j1 = 0;
        i1 = 0;
        while (j1 < l1) 
        {
            try
            {
                i2 = aobj[j1].name.indexOf("DownloadProvider");
            }
            catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception3)
            {
                j1 = i1;
                i1 = 0;
                j2 = k1;
                continue; /* Loop/switch isn't completed */
            }
            if (i2 != -1)
            {
                i1 = 1;
            }
            j1++;
        }
        l1 = i1;
_L8:
        try
        {
            aactivityinfo = com.igexin.push.core.g.i.getPackageManager().getPackageInfo(s1, 2).receivers;
        }
        catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception4)
        {
            i1 = 0;
            j1 = l1;
            j2 = k1;
            continue; /* Loop/switch isn't completed */
        }
        if (aactivityinfo == null) goto _L6; else goto _L5
_L5:
        l2 = aactivityinfo.length;
        k2 = 0;
        boolean flag = false;
        do
        {
            i1 = ((flag) ? 1 : 0);
            j1 = l1;
            j2 = k1;
            if (k2 >= l2)
            {
                break;
            }
            try
            {
                i1 = aactivityinfo[k2].name.indexOf("DownloadReceiver");
            }
            catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception5)
            {
                i1 = ((flag) ? 1 : 0);
                j1 = l1;
                j2 = k1;
                continue; /* Loop/switch isn't completed */
            }
            if (i1 != -1)
            {
                flag = true;
            }
            k2++;
        } while (true);
        break MISSING_BLOCK_LABEL_244;
        namenotfoundexception;
        i1 = 0;
        j1 = 0;
        j2 = 0;
_L7:
        return j2 != 0 && j1 != 0 && i1 != 0;
_L6:
        i1 = 0;
        j1 = l1;
        j2 = k1;
        if (true) goto _L7; else goto _L4
_L4:
        l1 = 0;
        if (true) goto _L8; else goto _L2
_L2:
        k1 = 0;
        if (true) goto _L10; else goto _L9
_L9:
    }

    public void s()
    {
        long l1 = System.currentTimeMillis();
        com.igexin.push.core.f.a().i().a("message", (new StringBuilder()).append("createtime <= ").append(l1 - 0x240c8400L).toString());
    }

    public void t()
    {
        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
        String s3 = simpledateformat.format(new Date());
        File file = new File("/sdcard/libs/");
        String s2 = com.igexin.push.core.g.g;
        String s1 = s2;
        if (s2 == null)
        {
            s1 = "unknowPacageName";
        }
        if (file.exists())
        {
            String as[] = file.list();
            int j1 = as.length;
            int i1 = 0;
            while (i1 < j1) 
            {
                int k1 = as[i1].length();
                if (as[i1].startsWith(s1) && as[i1].endsWith(".log") && k1 > s1.length() + 14 && s1.equals(as[i1].substring(0, k1 - 15)))
                {
                    Object obj = as[i1].substring(s1.length() + 1, k1 - 4);
                    try
                    {
                        obj = simpledateformat.parse(((String) (obj)));
                        if (Math.abs((simpledateformat.parse(s3).getTime() - ((Date) (obj)).getTime()) / 0x5265c00L) > 6L)
                        {
                            File file1 = new File((new StringBuilder()).append("/sdcard/libs/").append(as[i1]).toString());
                            if (file1.exists())
                            {
                                file1.delete();
                            }
                        }
                    }
                    catch (Exception exception) { }
                }
                i1++;
            }
        }
    }

    public void u()
    {
        if (F() <= 0) goto _L2; else goto _L1
_L1:
        ArrayList arraylist;
        Iterator iterator;
        arraylist = new ArrayList();
        iterator = com.igexin.push.core.g.al.entrySet().iterator();
_L5:
        String s1;
        String s3;
        String s4;
        PushTaskBean pushtaskbean;
        Map map;
        if (!iterator.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        s3 = (String)((java.util.Map.Entry)iterator.next()).getKey();
        pushtaskbean = (PushTaskBean)com.igexin.push.core.g.al.get(s3);
        s1 = "";
        if (pushtaskbean == null || pushtaskbean.getStatus() != com.igexin.push.core.a.k)
        {
            continue; /* Loop/switch isn't completed */
        }
        s4 = pushtaskbean.getTaskId();
        map = pushtaskbean.getConditionMap();
        if (map != null) goto _L3; else goto _L2
_L2:
        return;
_L3:
        if (map.containsKey("endTime") && Long.valueOf((String)map.get("endTime")).longValue() < System.currentTimeMillis())
        {
            a(com.igexin.push.core.a.m, s4, s3);
            arraylist.add(s3);
            continue; /* Loop/switch isn't completed */
        }
        if (map.containsKey("wifi"))
        {
            int i1 = Integer.valueOf((String)map.get("wifi")).intValue();
            x();
            if (i1 != com.igexin.push.core.g.r)
            {
                continue; /* Loop/switch isn't completed */
            }
        }
        if (map.containsKey("screenOn"))
        {
            int j1 = Integer.valueOf((String)map.get("screenOn")).intValue();
            w();
            if (j1 != com.igexin.push.core.g.s)
            {
                continue; /* Loop/switch isn't completed */
            }
        }
        if (map.containsKey("ssid"))
        {
            s1 = (String)map.get("ssid");
            y();
            if (!com.igexin.push.core.g.ar.containsValue(s1))
            {
                continue; /* Loop/switch isn't completed */
            }
        }
        if (map.containsKey("bssid"))
        {
            String s5 = (String)map.get("bssid");
            if (!com.igexin.push.core.g.ar.containsKey(s5) || !((String)com.igexin.push.core.g.ar.get(s5)).equals(s1))
            {
                continue; /* Loop/switch isn't completed */
            }
        }
        if (!map.containsKey("startTime") || Long.valueOf((String)map.get("startTime")).longValue() <= System.currentTimeMillis())
        {
            String s2 = pushtaskbean.getMessageId();
            a().a(s4, s2, com.igexin.push.core.g.c, com.igexin.push.core.g.g);
            a(com.igexin.push.core.a.l, s4, s3);
            arraylist.add(s3);
        }
        if (true) goto _L5; else goto _L4
_L4:
        b(arraylist);
        return;
    }

    public boolean v()
    {
        long l1 = System.currentTimeMillis();
        if (com.igexin.push.core.g.L > 0L)
        {
            if (l1 - com.igexin.push.core.g.L > 60000L)
            {
                com.igexin.push.core.g.L = l1;
                return true;
            } else
            {
                return false;
            }
        } else
        {
            com.igexin.push.core.g.L = l1 - 60000L;
            return true;
        }
    }

    public void w()
    {
        if (((PowerManager)com.igexin.push.core.g.i.getSystemService("power")).isScreenOn())
        {
            com.igexin.push.core.g.s = 1;
            return;
        } else
        {
            com.igexin.push.core.g.s = 0;
            return;
        }
    }

    public void x()
    {
        android.net.NetworkInfo.State state = ((ConnectivityManager)com.igexin.push.core.g.i.getSystemService("connectivity")).getNetworkInfo(1).getState();
        if (state == android.net.NetworkInfo.State.CONNECTED || state == android.net.NetworkInfo.State.CONNECTING)
        {
            com.igexin.push.core.g.r = 1;
            return;
        } else
        {
            com.igexin.push.core.g.r = 0;
            return;
        }
    }

    public void y()
    {
        List list = ((WifiManager)com.igexin.push.core.g.i.getSystemService("wifi")).getScanResults();
        com.igexin.push.core.g.ar.clear();
        if (list != null)
        {
            for (int i1 = 0; i1 < list.size(); i1++)
            {
                com.igexin.push.core.g.ar.put(((ScanResult)list.get(i1)).BSSID, ((ScanResult)list.get(i1)).SSID);
            }

        }
    }

    public void z()
    {
        if (com.igexin.push.a.k.u)
        {
            Map map = com.igexin.push.core.c.f.a().c();
            if (map != null && map.size() > 0)
            {
                for (Iterator iterator = map.keySet().iterator(); iterator.hasNext();)
                {
                    String s1 = (String)iterator.next();
                    String s2 = (String)map.get(s1);
                    try
                    {
                        Intent intent = new Intent();
                        intent.setClassName(s1, s2);
                        com.igexin.push.core.g.i.startService(intent);
                    }
                    catch (Exception exception) { }
                }

            }
        }
    }
}
