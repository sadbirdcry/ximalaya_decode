// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.core;


public final class k extends Enum
{

    public static final k a;
    public static final k b;
    public static final k c;
    public static final k d;
    private static final k e[];

    private k(String s, int i)
    {
        super(s, i);
    }

    public static k[] a()
    {
        return (k[])e.clone();
    }

    static 
    {
        a = new k("HEARTBEAT_OK", 0);
        b = new k("HEARTBEAT_TIMEOUT", 1);
        c = new k("NETWORK_ERROR", 2);
        d = new k("NETWORK_SWITCH", 3);
        e = (new k[] {
            a, b, c, d
        });
    }
}
