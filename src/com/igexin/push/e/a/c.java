// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.e.a;

import android.os.Process;
import com.igexin.a.a.d.d;
import java.io.ByteArrayInputStream;
import java.net.URI;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

// Referenced classes of package com.igexin.push.e.a:
//            b

public class c extends d
{

    public b a;
    public HttpRequestBase b;
    public HttpClient c;

    public c(b b1)
    {
        super(0);
        a = b1;
    }

    public final void a_()
    {
        super.a_();
        Process.setThreadPriority(10);
        if (a == null || a.e == null)
        {
            return;
        }
        c = new DefaultHttpClient();
        Object obj;
        if (a.f == null && a.g == null)
        {
            obj = new HttpGet(URI.create(a.e));
            b = ((HttpRequestBase) (obj));
            obj = c.execute(((org.apache.http.client.methods.HttpUriRequest) (obj)));
        } else
        {
            obj = new HttpPost(URI.create(a.e));
            b = ((HttpRequestBase) (obj));
            if (a.g != null)
            {
                ((HttpPost) (obj)).setEntity(new InputStreamEntity(a.g, a.h));
            } else
            {
                ((HttpPost) (obj)).setEntity(new InputStreamEntity(new ByteArrayInputStream(a.f), a.f.length));
            }
            obj = c.execute(((org.apache.http.client.methods.HttpUriRequest) (obj)));
        }
        if (((HttpResponse) (obj)).getStatusLine().getStatusCode() == 200)
        {
            a.a(EntityUtils.toByteArray(((HttpResponse) (obj)).getEntity()));
            com.igexin.a.a.b.d.c().a(a);
            com.igexin.a.a.b.d.c().d();
            return;
        } else
        {
            throw new Exception((new StringBuilder()).append("Http response code is : ").append(((HttpResponse) (obj)).getStatusLine().getStatusCode()).append(" try times = ").append(1).toString());
        }
    }

    public final int b()
    {
        return 0x8000000a;
    }

    public void d()
    {
        z = true;
    }

    protected void e()
    {
    }

    public void f()
    {
        super.f();
        if (b != null)
        {
            b.abort();
        }
        c = null;
    }
}
