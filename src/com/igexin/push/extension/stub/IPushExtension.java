// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.extension.stub;

import android.content.Context;
import com.igexin.push.core.b;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import org.json.JSONObject;

public interface IPushExtension
{

    public abstract boolean executeAction(PushTaskBean pushtaskbean, BaseAction baseaction);

    public abstract boolean init(Context context);

    public abstract boolean isActionSupported(String s);

    public abstract void onDestroy();

    public abstract BaseAction parseAction(JSONObject jsonobject);

    public abstract b prepareExecuteAction(PushTaskBean pushtaskbean, BaseAction baseaction);
}
