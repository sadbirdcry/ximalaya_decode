// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.c.c;

import com.igexin.a.a.d.a;

public abstract class e extends a
{

    public int i;
    public byte j;

    public e()
    {
    }

    protected int a(String s)
    {
        if (!s.equals("UTF-8"))
        {
            if (s.equals("UTF-16"))
            {
                return 2;
            }
            if (s.equals("UTF-16BE"))
            {
                return 16;
            }
            if (s.equals("UTF-16LE"))
            {
                return 17;
            }
            if (s.equals("GBK"))
            {
                return 25;
            }
            if (s.equals("GB2312"))
            {
                return 26;
            }
            if (s.equals("GB18030"))
            {
                return 27;
            }
            if (s.equals("ISO-8859-1"))
            {
                return 33;
            }
        }
        return 1;
    }

    protected String a(byte byte0)
    {
        switch (byte0 & 0x3f)
        {
        default:
            return "UTF-8";

        case 1: // '\001'
            return "UTF-8";

        case 2: // '\002'
            return "UTF-16";

        case 16: // '\020'
            return "UTF-16BE";

        case 17: // '\021'
            return "UTF-16LE";

        case 25: // '\031'
            return "GBK";

        case 26: // '\032'
            return "GB2312";

        case 27: // '\033'
            return "GB18030";

        case 33: // '!'
            return "ISO-8859-1";
        }
    }

    public abstract void a(byte abyte0[]);

    public int b()
    {
        return i;
    }

    public abstract byte[] d();
}
