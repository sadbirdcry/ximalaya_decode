// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.d;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.igexin.push.c.c.l;
import com.igexin.push.core.d;
import com.igexin.push.core.f;
import com.igexin.sdk.aidl.IGexinMsgService;
import com.igexin.sdk.aidl.c;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.igexin.push.d:
//            c, b, j

class g
    implements ServiceConnection
{

    final b a;
    final String b;
    final com.igexin.push.d.c c;

    g(com.igexin.push.d.c c1, b b1, String s)
    {
        c = c1;
        a = b1;
        b = s;
        super();
    }

    public void onServiceConnected(ComponentName componentname, IBinder ibinder)
    {
        if (com.igexin.push.d.c.b(c) != d.c)
        {
            break MISSING_BLOCK_LABEL_136;
        }
        a.a(com.igexin.sdk.aidl.c.a(ibinder));
        com.igexin.push.d.c.e(c).put(b, a);
        if (a.c().onASNLConnected(a.a(), a.b(), b, 0L) == -1)
        {
            com.igexin.push.d.c.e(c).remove(b);
            return;
        }
        try
        {
            if (com.igexin.push.core.g.o)
            {
                a.c().onASNLNetworkConnected();
                return;
            }
        }
        // Misplaced declaration of an exception variable
        catch (ComponentName componentname)
        {
            com.igexin.push.d.c.e(c).remove(b);
        }
    }

    public void onServiceDisconnected(ComponentName componentname)
    {
        if (com.igexin.push.d.c.b(c) == d.c)
        {
            componentname = (b)com.igexin.push.d.c.e(c).get(b);
            com.igexin.push.d.c.e(c).remove(b);
            componentname = com.igexin.push.d.c.a(c, componentname.e());
            if (componentname.size() != 0)
            {
                for (int i = 0; i < componentname.size(); i++)
                {
                    String s = (String)componentname.get(i);
                    if (s.startsWith("S-"))
                    {
                        l l1 = new l();
                        l1.a = Long.valueOf(s.substring(2)).longValue();
                        f.a().e().a((new StringBuilder()).append("S-").append(String.valueOf(l1.a)).toString(), l1);
                    }
                    com.igexin.push.d.c.f(c).remove(s);
                }

            }
        }
    }
}
