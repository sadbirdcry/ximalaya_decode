// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.push.d;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.igexin.push.b.b;
import com.igexin.push.c.c.a;
import com.igexin.push.c.c.c;
import com.igexin.push.c.c.d;
import com.igexin.push.c.c.e;
import com.igexin.push.c.c.f;
import com.igexin.push.c.c.i;
import com.igexin.push.c.c.k;
import com.igexin.push.c.c.l;
import com.igexin.push.c.c.m;
import com.igexin.push.c.c.n;
import com.igexin.push.c.c.o;
import com.igexin.push.core.c.r;
import com.igexin.push.core.g;
import java.text.SimpleDateFormat;
import java.util.Date;

// Referenced classes of package com.igexin.push.d:
//            a, c, k

public class j
{

    private static String a = "SNL";
    private Context b;
    private com.igexin.a.a.b.d c;
    private com.igexin.push.d.k d;
    private boolean e;
    private long f;
    private long g;
    private boolean h;

    public j()
    {
        e = false;
        f = 0L;
        g = 0L;
        h = false;
    }

    private long a(long l1)
    {
        long l2 = l1 / 10L;
        return (long)(Math.random() * (double)l2 * 2D - (double)l2) + l1;
    }

    private String b(e e1)
    {
        if (e1 instanceof f)
        {
            return (new StringBuilder()).append("R-").append(((f)e1).a()).toString();
        }
        if (e1 instanceof o)
        {
            return (new StringBuilder()).append("R-").append(((o)e1).b).toString();
        }
        if (e1 instanceof i)
        {
            return (new StringBuilder()).append("S-").append(String.valueOf(((i)e1).a)).toString();
        }
        if (e1 instanceof k)
        {
            if (((k)e1).e != 0L)
            {
                return (new StringBuilder()).append("S-").append(String.valueOf(((k)e1).e)).toString();
            }
        } else
        {
            if (e1 instanceof l)
            {
                return (new StringBuilder()).append("S-").append(String.valueOf(((l)e1).a)).toString();
            }
            if (e1 instanceof m)
            {
                return (new StringBuilder()).append("S-").append(String.valueOf(((m)e1).e)).toString();
            }
            if (e1 instanceof d)
            {
                return (new StringBuilder()).append("C-").append(((d)e1).g).toString();
            }
            if (e1 instanceof n)
            {
                return (new StringBuilder()).append("C-").append(((n)e1).g).toString();
            }
            if (e1 instanceof a)
            {
                return (new StringBuilder()).append("C-").append(((a)e1).d).toString();
            }
            if (e1 instanceof c)
            {
                return (new StringBuilder()).append("C-").append(((c)e1).d).toString();
            }
        }
        return "";
    }

    private boolean d()
    {
        if (com.igexin.push.a.k.s && f + g >= com.igexin.push.a.k.t)
        {
            com.igexin.push.d.a a1 = new com.igexin.push.d.a();
            a1.a(com.igexin.push.core.c.e);
            com.igexin.push.core.f.a().f().a(a1);
        }
        return false;
    }

    private void e()
    {
        Object obj;
        Object obj1;
        obj1 = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());
        obj = com.igexin.push.core.f.a().i().a("bi", new String[] {
            "type"
        }, new String[] {
            "1"
        }, null, null);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_110;
        }
        if (((Cursor) (obj)).getCount() != 0)
        {
            break MISSING_BLOCK_LABEL_382;
        }
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("loginerror_connecterror_count", Integer.valueOf(1));
        contentvalues.put("create_time", ((String) (obj1)));
        contentvalues.put("type", "1");
        com.igexin.push.core.f.a().i().a("bi", contentvalues);
_L2:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
_L7:
        return;
_L5:
        if (!((Cursor) (obj)).moveToNext()) goto _L2; else goto _L1
_L1:
        Object obj2;
        String s;
        s = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndexOrThrow("create_time"));
        obj2 = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndexOrThrow("id"));
        if (!((String) (obj1)).equals(s)) goto _L4; else goto _L3
_L3:
        int i1;
        i1 = ((Cursor) (obj)).getInt(((Cursor) (obj)).getColumnIndexOrThrow("loginerror_connecterror_count"));
        ContentValues contentvalues1 = new ContentValues();
        contentvalues1.put("loginerror_connecterror_count", Integer.valueOf(i1 + 1));
        com.igexin.push.core.f.a().i().a("bi", contentvalues1, new String[] {
            "id"
        }, new String[] {
            obj2
        });
          goto _L5
        obj1;
_L10:
        if (obj == null) goto _L7; else goto _L6
_L6:
        ((Cursor) (obj)).close();
        return;
_L4:
        ContentValues contentvalues2 = new ContentValues();
        contentvalues2.put("type", "2");
        com.igexin.push.core.f.a().i().a("bi", contentvalues2, new String[] {
            "id"
        }, new String[] {
            obj2
        });
        obj2 = new ContentValues();
        ((ContentValues) (obj2)).put("loginerror_connecterror_count", Integer.valueOf(i1 + 1));
        ((ContentValues) (obj2)).put("create_time", ((String) (obj1)));
        ((ContentValues) (obj2)).put("type", "1");
        com.igexin.push.core.f.a().i().a("bi", ((ContentValues) (obj2)));
          goto _L5
        Exception exception1;
        exception1;
        obj1 = obj;
        obj = exception1;
_L9:
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
        throw obj;
        obj;
        obj1 = null;
        if (true) goto _L9; else goto _L8
_L8:
        Exception exception;
        exception;
        exception = null;
          goto _L10
        i1 = 0;
          goto _L5
    }

    public int a(String s, e e1)
    {
        if (s == null || e1 == null)
        {
            return -1;
        }
        if (e)
        {
            if (e1.i == 6 || e1.i == 4 || e1.i == 36 || e1.i == 0)
            {
                int i1 = 15;
                if (com.igexin.push.a.k.i > 0)
                {
                    i1 = com.igexin.push.a.k.i;
                }
                if (c.a(g.a, 3, com.igexin.push.core.f.a().d(), e1, true, i1, new com.igexin.push.c.b()) == null)
                {
                    return -2;
                }
            } else
            if (c.a(g.a, 3, com.igexin.push.core.f.a().d(), e1, true) == null)
            {
                return -2;
            }
            s = e1.d();
            if (s != null)
            {
                g = g + ((long)s.length + 8L);
            } else
            {
                g = g + 8L;
            }
            d();
            return 0;
        } else
        {
            return com.igexin.push.core.f.a().f().a(s, e1);
        }
    }

    public void a(Context context, com.igexin.a.a.b.d d1, com.igexin.push.d.k k1)
    {
        b = context;
        c = d1;
        d = k1;
    }

    public void a(e e1)
    {
        if (e1 != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        String s;
        if (!e)
        {
            continue; /* Loop/switch isn't completed */
        }
        if ((s = b(e1)).equals("S-") || s.equals("R-")) goto _L1; else goto _L3
_L3:
        if (s.length() <= 0 || s.equals("C-") || s.equals((new StringBuilder()).append("C-").append(g.u).toString()) || s.equals((new StringBuilder()).append("R-").append(g.C).toString()) || s.equals((new StringBuilder()).append("S-").append(g.t).toString()))
        {
            if (d != null)
            {
                d.a(e1);
            }
        } else
        {
            com.igexin.push.core.f.a().f().b(s, e1);
        }
        e1 = e1.d();
        if (e1 != null)
        {
            f = f + ((long)e1.length + 8L);
        } else
        {
            f = f + 8L;
        }
        d();
        return;
        if (d == null) goto _L1; else goto _L4
_L4:
        d.a(e1);
        return;
    }

    public void a(boolean flag)
    {
label0:
        {
            if (e != flag)
            {
                e = flag;
                h = false;
                if (!flag)
                {
                    break label0;
                }
                g = 0L;
                f = 0L;
                c.a(new com.igexin.push.c.b.b());
                c.d();
            }
            return;
        }
        c.a(g.a.replaceFirst("socket", "disConnect"), 0, null);
    }

    public boolean a()
    {
        return e;
    }

    public void b()
    {
        for (g.F = 0L; e || d == null;)
        {
            return;
        }

        d.b();
    }

    public void b(boolean flag)
    {
        if (flag) goto _L2; else goto _L1
_L1:
        com.igexin.a.a.c.a.a("disconnected|network");
        com.igexin.push.core.i.a().a(com.igexin.push.core.k.c);
        r.d();
        r.a();
        e();
        if (g.o)
        {
            g.o = false;
            com.igexin.push.core.a.f.a().m();
        }
        c(true);
_L8:
        if (!e) goto _L4; else goto _L3
_L3:
        com.igexin.push.core.f.a().f().b();
_L6:
        return;
_L2:
        com.igexin.a.a.c.a.a("disconnected|user");
        r.d();
        if (g.o)
        {
            g.o = false;
            com.igexin.push.core.a.f.a().m();
        }
        continue; /* Loop/switch isn't completed */
_L4:
        if (d == null) goto _L6; else goto _L5
_L5:
        d.a(flag);
        return;
        if (true) goto _L8; else goto _L7
_L7:
    }

    public long c()
    {
        return g.F;
    }

    public void c(boolean flag)
    {
        boolean flag1;
        if (flag)
        {
            g.F = 0L;
        }
        flag = com.igexin.push.core.a.f.a().a(System.currentTimeMillis());
        flag1 = com.igexin.push.core.a.f.a().n();
        if (g.k && g.l && g.m && !flag && flag1) goto _L2; else goto _L1
_L1:
        g.F = 0x36ee80L;
_L4:
        com.igexin.push.e.b.f.g().h();
        return;
_L2:
        if (g.F > 0L)
        {
            break; /* Loop/switch isn't completed */
        }
        g.F = 1000L;
_L5:
        if (g.F > 0x36ee80L)
        {
            g.F = 0x36ee80L;
        }
        g.F = a(g.F);
        if (true) goto _L4; else goto _L3
_L3:
        if (g.F <= 60000L)
        {
            g.F += 10000L;
        } else
        {
            g.F += 0x1d4c0L;
        }
          goto _L5
        if (true) goto _L4; else goto _L6
_L6:
    }

}
