// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.download;

import android.database.CrossProcessCursor;
import android.database.Cursor;
import android.database.CursorWindow;
import android.database.CursorWrapper;

// Referenced classes of package com.igexin.download:
//            DownloadProvider

class c extends CursorWrapper
    implements CrossProcessCursor
{

    final DownloadProvider a;
    private CrossProcessCursor b;

    public c(DownloadProvider downloadprovider, Cursor cursor)
    {
        a = downloadprovider;
        super(cursor);
        b = (CrossProcessCursor)cursor;
    }

    public void fillWindow(int i, CursorWindow cursorwindow)
    {
        b.fillWindow(i, cursorwindow);
    }

    public CursorWindow getWindow()
    {
        return b.getWindow();
    }

    public boolean onMove(int i, int j)
    {
        return b.onMove(i, j);
    }
}
