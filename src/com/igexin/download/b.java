// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.download;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

// Referenced classes of package com.igexin.download:
//            DownloadProvider

final class b extends SQLiteOpenHelper
{

    final DownloadProvider a;

    public b(DownloadProvider downloadprovider, Context context)
    {
        a = downloadprovider;
        super(context, DownloadProvider.a(), null, 101);
    }

    public void onCreate(SQLiteDatabase sqlitedatabase)
    {
        DownloadProvider.a(a, sqlitedatabase);
    }

    public void onUpgrade(SQLiteDatabase sqlitedatabase, int i, int j)
    {
        if (i == 31 && j == 100)
        {
            return;
        } else
        {
            DownloadProvider.b(a, sqlitedatabase);
            DownloadProvider.a(a, sqlitedatabase);
            return;
        }
    }
}
