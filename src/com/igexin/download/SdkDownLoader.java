// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.download;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.igexin.download:
//            j, Downloads, IDownloadCallback, DownloadInfo

public class SdkDownLoader
{

    static int a = 3;
    static String b = "/libs/tmp";
    static SdkDownLoader c;
    Handler d;
    String e[] = {
        "_id", "_data", "hint", "status", "total_bytes", "current_bytes"
    };
    private Context f;
    private List g;
    private Object h;
    public Map updateData;

    private SdkDownLoader(Context context)
    {
        g = new ArrayList();
        updateData = new HashMap();
        h = new Object();
        f = context;
        d = new j(this, context.getMainLooper());
    }

    private int a(ContentValues contentvalues)
    {
        ContentResolver contentresolver = f.getContentResolver();
        contentvalues.put("data_10", String.valueOf(System.currentTimeMillis()));
        contentvalues = contentresolver.insert(Downloads.a, contentvalues);
        if (contentvalues != null)
        {
            return Integer.parseInt((String)contentvalues.getPathSegments().get(1));
        } else
        {
            return -1;
        }
    }

    private int a(String s, String s1, ContentValues contentvalues, String s2)
    {
        ContentValues contentvalues1 = new ContentValues();
        if (contentvalues != null)
        {
            contentvalues1.putAll(contentvalues);
        }
        contentvalues1.put("destination", Integer.valueOf(0));
        if (s != null)
        {
            contentvalues1.put("uri", s);
        }
        if (s1 != null)
        {
            contentvalues1.put("hint", s1.replaceAll("\\*", ""));
        }
        if (s2 != null)
        {
            contentvalues1.put("description", s2);
        }
        return a(contentvalues1);
    }

    static Object a(SdkDownLoader sdkdownloader)
    {
        return sdkdownloader.h;
    }

    static List b(SdkDownLoader sdkdownloader)
    {
        return sdkdownloader.g;
    }

    public static SdkDownLoader getInstantiate(Context context)
    {
        if (c == null)
        {
            c = new SdkDownLoader(context);
        }
        return c;
    }

    IDownloadCallback a(String s)
    {
        if (s == null)
        {
            return null;
        }
        for (Iterator iterator = g.iterator(); iterator.hasNext();)
        {
            IDownloadCallback idownloadcallback = (IDownloadCallback)iterator.next();
            if (s.equals(idownloadcallback.getName()))
            {
                return idownloadcallback;
            }
        }

        return null;
    }

    protected void a(Collection collection)
    {
        Object obj = h;
        obj;
        JVM INSTR monitorenter ;
        if (collection == null)
        {
            break MISSING_BLOCK_LABEL_171;
        }
        HashMap hashmap;
        if (collection.size() <= 0)
        {
            break MISSING_BLOCK_LABEL_174;
        }
        hashmap = new HashMap();
        collection = collection.iterator();
_L4:
        DownloadInfo downloadinfo;
        if (!collection.hasNext())
        {
            break MISSING_BLOCK_LABEL_166;
        }
        downloadinfo = (DownloadInfo)collection.next();
        if (!updateData.containsKey(Integer.valueOf(downloadinfo.mId))) goto _L2; else goto _L1
_L1:
        DownloadInfo downloadinfo1 = (DownloadInfo)updateData.get(Integer.valueOf(downloadinfo.mId));
        if (downloadinfo1 == null) goto _L4; else goto _L3
_L3:
        downloadinfo1.copyFrom(downloadinfo);
        hashmap.put(Integer.valueOf(downloadinfo1.mId), downloadinfo1);
          goto _L4
        collection;
        obj;
        JVM INSTR monitorexit ;
        throw collection;
_L2:
        downloadinfo1 = downloadinfo.clone();
        if (downloadinfo1 == null) goto _L4; else goto _L5
_L5:
        hashmap.put(Integer.valueOf(downloadinfo.mId), downloadinfo1);
          goto _L4
        updateData = hashmap;
_L7:
        obj;
        JVM INSTR monitorexit ;
        return;
        updateData.clear();
        if (true) goto _L7; else goto _L6
_L6:
    }

    public boolean deleteTask(int i)
    {
        f.getContentResolver().delete(ContentUris.withAppendedId(Downloads.a, i), null, null);
        return true;
    }

    public boolean deleteTask(int ai[])
    {
        ContentResolver contentresolver = f.getContentResolver();
        String as[] = new String[ai.length];
        for (int i = 0; i < as.length; i++)
        {
            as[i] = String.valueOf(ai[i]);
        }

        contentresolver.delete(Downloads.a, "_id=?", as);
        return true;
    }

    public IDownloadCallback getCallback(String s)
    {
        if (s == null)
        {
            return null;
        }
        for (Iterator iterator = g.iterator(); iterator.hasNext();)
        {
            IDownloadCallback idownloadcallback = (IDownloadCallback)iterator.next();
            if (s.equals(idownloadcallback.getName()))
            {
                return idownloadcallback;
            }
        }

        return null;
    }

    public boolean isRegistered(String s)
    {
        for (Iterator iterator = g.iterator(); iterator.hasNext();)
        {
            String s1 = ((IDownloadCallback)iterator.next()).getName();
            if (s1 != null && s1.equals(s))
            {
                return true;
            }
        }

        return false;
    }

    public int newTask(String s, String s1, String s2, boolean flag, String s3)
    {
        ContentValues contentvalues = new ContentValues();
        if (s2 != null)
        {
            contentvalues.put("data_6", s2);
        }
        if (flag)
        {
            contentvalues.put("data_9", "wifi");
        }
        contentvalues.put("data_8", s3);
        return a(s, s1, contentvalues, null);
    }

    public boolean pauseAllTask()
    {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("control", Integer.valueOf(1));
        contentvalues.put("status", Integer.valueOf(193));
        f.getContentResolver().update(Downloads.a, contentvalues, "status=? OR status=? OR(status=? AND control<>?)", new String[] {
            String.valueOf(192), String.valueOf(190), String.valueOf(193), String.valueOf(1)
        });
        return true;
    }

    public boolean pauseTask(int i)
    {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("control", Integer.valueOf(1));
        f.getContentResolver().update(ContentUris.withAppendedId(Downloads.a, i), contentvalues, null, null);
        return true;
    }

    public boolean queryTask(String s)
    {
        if (Downloads.a != null)
        {
            if ((s = f.getContentResolver().query(Downloads.a, null, "data_8 = ? ", new String[] {
    s
}, null)) != null)
            {
                int i = s.getCount();
                s.close();
                boolean flag;
                if (i > 0)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                return flag;
            }
        }
        return false;
    }

    public void refreshList()
    {
        Message message = new Message();
        message.what = 2;
        d.sendMessage(message);
    }

    public void registerDownloadCallback(IDownloadCallback idownloadcallback)
    {
        if (!g.contains(idownloadcallback))
        {
            g.add(idownloadcallback);
        }
    }

    public void setDownloadDir(String s)
    {
        b = s;
    }

    public boolean startTask(int i)
    {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("control", Integer.valueOf(0));
        contentvalues.put("numfailed", Integer.valueOf(0));
        f.getContentResolver().update(ContentUris.withAppendedId(Downloads.a, i), contentvalues, null, null);
        return true;
    }

    public void unregisterDownloadCallback(IDownloadCallback idownloadcallback)
    {
        g.remove(idownloadcallback);
    }

    public boolean updateTask(int i, String s, String s1)
    {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put(s, s1);
        f.getContentResolver().update(ContentUris.withAppendedId(Downloads.a, i), contentvalues, null, null);
        return true;
    }

}
