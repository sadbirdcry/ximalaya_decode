// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.download;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Binder;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.List;

// Referenced classes of package com.igexin.download:
//            h, DownloadService, Downloads, b, 
//            c

public final class DownloadProvider extends ContentProvider
{

    private static String a = "tmpd8.db";
    private static final UriMatcher b = new UriMatcher(-1);
    private static final String c[] = {
        "_id", "entity", "_data", "mimetype", "visibility", "destination", "control", "status", "lastmod", "createmod", 
        "total_bytes", "current_bytes", "title", "description", "data_8", "data_10"
    };
    private static HashSet d;
    private SQLiteOpenHelper e;

    public DownloadProvider()
    {
        e = null;
    }

    static String a()
    {
        return a;
    }

    private void a(SQLiteDatabase sqlitedatabase)
    {
        try
        {
            sqlitedatabase.execSQL("CREATE TABLE downloads(_id INTEGER PRIMARY KEY AUTOINCREMENT,uri TEXT, method INTEGER, entity TEXT, no_integrity BOOLEAN, hint TEXT, otaupdate BOOLEAN, _data TEXT, mimetype TEXT, destination INTEGER, no_system BOOLEAN, visibility INTEGER, control INTEGER default 0, status INTEGER, numfailed INTEGER, lastmod BIGINT, createmod BIGINT, extras TEXT, cookiedata TEXT, useragent TEXT, referer TEXT, total_bytes INTEGER, current_bytes INTEGER, etag TEXT, uid INTEGER, otheruid INTEGER, title TEXT, description TEXT, scanned BOOLEAN,data_1 TEXT, data_2 TEXT, data_3 TEXT, data_4 TEXT, data_5 TEXT, data_6 TEXT, data_7 TEXT, data_8 TEXT, data_9 TEXT, data_10 BIGINT, iswebicon INTEGER);");
            return;
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            throw sqlitedatabase;
        }
    }

    static void a(DownloadProvider downloadprovider, SQLiteDatabase sqlitedatabase)
    {
        downloadprovider.a(sqlitedatabase);
    }

    private static final void a(String s, ContentValues contentvalues, ContentValues contentvalues1)
    {
        contentvalues = contentvalues.getAsLong(s);
        if (contentvalues != null)
        {
            contentvalues1.put(s, contentvalues);
        }
    }

    private void b(SQLiteDatabase sqlitedatabase)
    {
        try
        {
            sqlitedatabase.execSQL("DROP TABLE IF EXISTS downloads");
            return;
        }
        // Misplaced declaration of an exception variable
        catch (SQLiteDatabase sqlitedatabase)
        {
            throw sqlitedatabase;
        }
    }

    static void b(DownloadProvider downloadprovider, SQLiteDatabase sqlitedatabase)
    {
        downloadprovider.b(sqlitedatabase);
    }

    private static final void b(String s, ContentValues contentvalues, ContentValues contentvalues1)
    {
        contentvalues = contentvalues.getAsInteger(s);
        if (contentvalues != null)
        {
            contentvalues1.put(s, contentvalues);
        }
    }

    private static final void c(String s, ContentValues contentvalues, ContentValues contentvalues1)
    {
        contentvalues = contentvalues.getAsBoolean(s);
        if (contentvalues != null)
        {
            contentvalues1.put(s, contentvalues);
        }
    }

    private static final void d(String s, ContentValues contentvalues, ContentValues contentvalues1)
    {
        contentvalues = contentvalues.getAsString(s);
        if (contentvalues != null)
        {
            contentvalues1.put(s, contentvalues);
        }
    }

    public int delete(Uri uri, String s, String as[])
    {
        h.a(s, d);
        SQLiteDatabase sqlitedatabase = e.getWritableDatabase();
        int i = b.match(uri);
        switch (i)
        {
        default:
            throw new UnsupportedOperationException((new StringBuilder()).append("Cannot delete URI: ").append(uri).toString());

        case 1: // '\001'
        case 2: // '\002'
            break;
        }
        String s1;
        if (s != null)
        {
            if (i == 1)
            {
                s = (new StringBuilder()).append("( ").append(s).append(" )").toString();
            } else
            {
                s = (new StringBuilder()).append("( ").append(s).append(" ) AND ").toString();
            }
        } else
        {
            s = "";
        }
        if (i == 2)
        {
            long l = Long.parseLong((String)uri.getPathSegments().get(1));
            s = (new StringBuilder()).append(s).append(" ( _id = ").append(l).append(" ) ").toString();
        }
        s1 = s;
        if (Binder.getCallingPid() != Process.myPid())
        {
            s1 = s;
            if (Binder.getCallingUid() != 0)
            {
                s1 = (new StringBuilder()).append(s).append(" AND ( uid=").append(Binder.getCallingUid()).append(" OR ").append("otheruid").append("=").append(Binder.getCallingUid()).append(" )").toString();
            }
        }
        i = sqlitedatabase.delete("downloads", s1, as);
        getContext().getContentResolver().notifyChange(uri, null);
        return i;
    }

    public String getType(Uri uri)
    {
        switch (b.match(uri))
        {
        default:
            throw new IllegalArgumentException((new StringBuilder()).append("Unknown URI: ").append(uri).toString());

        case 1: // '\001'
            return "vnd.android.cursor.dir/download";

        case 2: // '\002'
            return "vnd.android.cursor.item/download";

        case 3: // '\003'
            return "vnd.android.cursor.sql/download";
        }
    }

    public Uri insert(Uri uri, ContentValues contentvalues)
    {
        SQLiteDatabase sqlitedatabase = e.getWritableDatabase();
        if (b.match(uri) != 1)
        {
            throw new IllegalArgumentException((new StringBuilder()).append("Unknown/Invalid URI ").append(uri).toString());
        }
        ContentValues contentvalues1 = new ContentValues();
        d("_data", contentvalues, contentvalues1);
        d("uri", contentvalues, contentvalues1);
        d("entity", contentvalues, contentvalues1);
        c("no_integrity", contentvalues, contentvalues1);
        d("hint", contentvalues, contentvalues1);
        d("mimetype", contentvalues, contentvalues1);
        Integer integer = contentvalues.getAsInteger("destination");
        if (integer != null)
        {
            contentvalues1.put("destination", integer);
        }
        Integer integer1 = contentvalues.getAsInteger("visibility");
        long l;
        if (integer1 == null)
        {
            if (integer.intValue() == 0)
            {
                contentvalues1.put("visibility", Integer.valueOf(1));
            } else
            {
                contentvalues1.put("visibility", Integer.valueOf(2));
            }
        } else
        {
            contentvalues1.put("visibility", integer1);
        }
        b("control", contentvalues, contentvalues1);
        if (!contentvalues.containsKey("status"))
        {
            contentvalues1.put("status", Integer.valueOf(190));
        } else
        {
            b("status", contentvalues, contentvalues1);
        }
        contentvalues1.put("lastmod", Long.valueOf(System.currentTimeMillis()));
        contentvalues1.put("createmod", Long.valueOf(System.currentTimeMillis()));
        d("extras", contentvalues, contentvalues1);
        d("data_1", contentvalues, contentvalues1);
        d("data_2", contentvalues, contentvalues1);
        d("data_3", contentvalues, contentvalues1);
        d("data_4", contentvalues, contentvalues1);
        d("data_5", contentvalues, contentvalues1);
        d("data_6", contentvalues, contentvalues1);
        d("data_7", contentvalues, contentvalues1);
        d("data_8", contentvalues, contentvalues1);
        d("data_9", contentvalues, contentvalues1);
        a("data_10", contentvalues, contentvalues1);
        b("iswebicon", contentvalues, contentvalues1);
        d("cookiedata", contentvalues, contentvalues1);
        d("useragent", contentvalues, contentvalues1);
        d("referer", contentvalues, contentvalues1);
        contentvalues1.put("uid", Integer.valueOf(Binder.getCallingUid()));
        if (Binder.getCallingUid() == 0)
        {
            b("uid", contentvalues, contentvalues1);
        }
        d("title", contentvalues, contentvalues1);
        d("description", contentvalues, contentvalues1);
        contentvalues = getContext();
        contentvalues.startService(new Intent(contentvalues, com/igexin/download/DownloadService));
        l = sqlitedatabase.insert("downloads", null, contentvalues1);
        if (l != -1L)
        {
            contentvalues.startService(new Intent(contentvalues, com/igexin/download/DownloadService));
            Uri uri1 = Uri.parse((new StringBuilder()).append(Downloads.a).append("/").append(l).toString());
            contentvalues.getContentResolver().notifyChange(uri, null);
            return uri1;
        } else
        {
            return null;
        }
    }

    public boolean onCreate()
    {
        e = new b(this, getContext());
        String s = (new StringBuilder()).append("downloads.").append(getContext().getPackageName()).toString();
        b.addURI(s, "download", 1);
        b.addURI(s, "download/#", 2);
        b.addURI(s, "download/full/item", 3);
        Downloads.setContentUrl(s);
        return true;
    }

    public ParcelFileDescriptor openFile(Uri uri, String s)
    {
        Object obj = null;
        Object obj1 = query(uri, new String[] {
            "_data"
        }, null, null, null);
        obj = obj1;
        if (obj == null) goto _L2; else goto _L1
_L1:
        int i = ((Cursor) (obj)).getCount();
_L8:
        if (i == 1) goto _L4; else goto _L3
_L3:
        if (i != 0) goto _L6; else goto _L5
_L5:
        try
        {
            throw new FileNotFoundException((new StringBuilder()).append("No entry for ").append(uri).toString());
        }
        catch (Exception exception) { }
        finally { }
_L10:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
            obj = null;
        } else
        {
            obj = null;
        }
          goto _L7
_L2:
        i = 0;
          goto _L8
_L6:
        throw new FileNotFoundException((new StringBuilder()).append("Multiple items at ").append(uri).toString());
_L9:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        throw uri;
_L4:
        ((Cursor) (obj)).moveToFirst();
        exception = ((Cursor) (obj)).getString(0);
        if (obj != null)
        {
            ((Cursor) (obj)).close();
            obj = exception;
        } else
        {
            obj = exception;
        }
_L7:
        if (obj == null)
        {
            throw new FileNotFoundException("No filename found.");
        }
        if (!h.a(((String) (obj))))
        {
            throw new FileNotFoundException("Invalid filename.");
        }
        if (!"r".equals(s))
        {
            throw new FileNotFoundException((new StringBuilder()).append("Bad mode for ").append(uri).append(": ").append(s).toString());
        }
        s = ParcelFileDescriptor.open(new File(((String) (obj))), 0x10000000);
        if (s == null)
        {
            throw new FileNotFoundException("couldn't open file");
        } else
        {
            obj = new ContentValues();
            ((ContentValues) (obj)).put("lastmod", Long.valueOf(System.currentTimeMillis()));
            update(uri, ((ContentValues) (obj)), null, null);
            return s;
        }
        uri;
          goto _L9
        obj;
        obj = null;
          goto _L10
    }

    public Cursor query(Uri uri, String as[], String s, String as1[], String s1)
    {
        SQLiteDatabase sqlitedatabase;
        SQLiteQueryBuilder sqlitequerybuilder;
        boolean flag;
        boolean flag1;
        int k;
        flag = true;
        flag1 = false;
        h.a(s, d);
        sqlitedatabase = e.getReadableDatabase();
        sqlitequerybuilder = new SQLiteQueryBuilder();
        k = b.match(uri);
        k;
        JVM INSTR tableswitch 1 3: default 68
    //                   1 95
    //                   2 266
    //                   3 305;
           goto _L1 _L2 _L3 _L4
_L1:
        throw new IllegalArgumentException((new StringBuilder()).append("Unknown URI: ").append(uri).toString());
_L2:
        sqlitequerybuilder.setTables("downloads");
_L7:
        if (Binder.getCallingPid() != Process.myPid() && Binder.getCallingUid() != 0 && Process.supportsProcesses())
        {
            if (!flag)
            {
                sqlitequerybuilder.appendWhere(" AND ");
            }
            sqlitequerybuilder.appendWhere((new StringBuilder()).append("( uid=").append(Binder.getCallingUid()).append(" OR ").append("otheruid").append("=").append(Binder.getCallingUid()).append(" )").toString());
            if (as == null)
            {
                as = c;
            } else
            {
                int i = 0;
                while (i < as.length) 
                {
                    if (!d.contains(as[i]))
                    {
                        throw new IllegalArgumentException((new StringBuilder()).append("column ").append(as[i]).append(" is not allowed in queries").toString());
                    }
                    i++;
                }
            }
        }
        k;
        JVM INSTR tableswitch 3 3: default 216
    //                   3 384;
           goto _L5 _L6
_L5:
        as = sqlitequerybuilder.query(sqlitedatabase, as, s, as1, null, null, s1);
_L9:
        if (as != null)
        {
            as = new c(this, as);
        }
        if (as != null)
        {
            as.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return as;
_L3:
        sqlitequerybuilder.setTables("downloads");
        sqlitequerybuilder.appendWhere("_id=");
        sqlitequerybuilder.appendWhere((CharSequence)uri.getPathSegments().get(1));
        flag = false;
          goto _L7
_L4:
        sqlitequerybuilder.setTables("downloads");
          goto _L7
_L6:
        if (as1 == null || as1.length <= 0) goto _L5; else goto _L8
_L8:
        String s2 = as1[0];
        int l = as1.length - 1;
        String as2[] = new String[l];
        for (int j = ((flag1) ? 1 : 0); j < l; j++)
        {
            as2[j] = as1[j + 1];
        }

        as = sqlitequerybuilder.query(sqlitedatabase, as, s, as2, null, null, s1, s2);
          goto _L9
    }

    public int update(Uri uri, ContentValues contentvalues, String s, String as[])
    {
        int i = 0;
        h.a(s, d);
        SQLiteDatabase sqlitedatabase = e.getWritableDatabase();
        if (Binder.getCallingPid() != Process.myPid())
        {
            ContentValues contentvalues1 = new ContentValues();
            d("entity", contentvalues, contentvalues1);
            b("visibility", contentvalues, contentvalues1);
            Integer integer = contentvalues.getAsInteger("control");
            boolean flag;
            int j;
            long l;
            if (integer != null)
            {
                contentvalues1.put("control", integer);
                flag = true;
            } else
            {
                flag = false;
            }
            b("control", contentvalues, contentvalues1);
            d("title", contentvalues, contentvalues1);
            d("description", contentvalues, contentvalues1);
        } else
        {
            flag = false;
            contentvalues1 = contentvalues;
        }
        j = b.match(uri);
        switch (j)
        {
        default:
            throw new UnsupportedOperationException((new StringBuilder()).append("Cannot update URI: ").append(uri).toString());

        case 1: // '\001'
        case 2: // '\002'
            break;
        }
        if (s != null)
        {
            if (j == 1)
            {
                contentvalues = (new StringBuilder()).append("( ").append(s).append(" )").toString();
            } else
            {
                contentvalues = (new StringBuilder()).append("( ").append(s).append(" ) AND ").toString();
            }
        } else
        {
            contentvalues = "";
        }
        if (j == 2)
        {
            l = Long.parseLong((String)uri.getPathSegments().get(1));
            contentvalues = (new StringBuilder()).append(contentvalues).append(" ( _id = ").append(l).append(" ) ").toString();
        }
        s = contentvalues;
        if (Binder.getCallingPid() != Process.myPid())
        {
            s = contentvalues;
            if (Binder.getCallingUid() != 0)
            {
                s = (new StringBuilder()).append(contentvalues).append(" AND ( uid=").append(Binder.getCallingUid()).append(" OR ").append("otheruid").append("=").append(Binder.getCallingUid()).append(" )").toString();
            }
        }
        if (contentvalues1.size() > 0)
        {
            i = sqlitedatabase.update("downloads", contentvalues1, s, as);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        if (flag)
        {
            uri = getContext();
            uri.startService(new Intent(uri, com/igexin/download/DownloadService));
        }
        return i;
    }

    static 
    {
        int i = 0;
        d = new HashSet();
        for (; i < c.length; i++)
        {
            d.add(c[i]);
        }

    }
}
