// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.download;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Referenced classes of package com.igexin.download:
//            SdkDownLoader, a, i, Downloads

public class h
{

    public static Random a = new Random(SystemClock.uptimeMillis());
    private static final Pattern b = Pattern.compile("attachment;\\s*filename\\s*=\\s*\"([^\"]*)\"");

    public static a a(Context context, String s, String s1, String s2, String s3, String s4, int j, int k)
    {
        s1 = a(s, s1, s2, s3, j);
        int l = s1.indexOf('.');
        if (l < 0)
        {
            context = a(s4, true);
        } else
        {
            context = a(s4, j, s1, l);
            s1 = s1.substring(0, l);
        }
        s2 = context;
        if (context != null)
        {
            s2 = context;
            if (context.equals(".bin"))
            {
                l = s.indexOf("?");
                s3 = s;
                if (l >= 0)
                {
                    s3 = s.substring(0, l);
                }
                l = s3.lastIndexOf(".");
                int i1 = s3.lastIndexOf("/");
                s2 = context;
                if (l >= 0)
                {
                    s2 = context;
                    if (i1 >= 0)
                    {
                        s2 = context;
                        if (l > i1)
                        {
                            s2 = s3.substring(l);
                        }
                    }
                }
            }
        }
        if (Environment.getExternalStorageState().equals("mounted"))
        {
            s = Environment.getExternalStorageDirectory().getPath();
            context = new File((new StringBuilder()).append(s).append(SdkDownLoader.b).toString());
            if (!context.isDirectory())
            {
                s3 = SdkDownLoader.b.split("/");
                for (l = 0; l < s3.length; l++)
                {
                    if (s3[l] == null || s3[l].length() <= 0)
                    {
                        continue;
                    }
                    s4 = new File((new StringBuilder()).append(s).append("/").append(s3[l]).toString());
                    if (!s4.exists())
                    {
                        s4.mkdir();
                    }
                }

                if (!context.mkdir())
                {
                    return new a(null, null, 492);
                }
            }
            s = new StatFs(context.getPath());
            if ((long)s.getBlockSize() * ((long)s.getAvailableBlocks() - 4L) < (long)k)
            {
                return new a(null, null, 492);
            }
        } else
        {
            return new a(null, null, 492);
        }
        boolean flag = "recovery".equalsIgnoreCase((new StringBuilder()).append(s1).append(s2).toString());
        context = a(j, (new StringBuilder()).append(context.getPath()).append(File.separator).append(s1).toString(), s2, flag);
        if (context != null)
        {
            return new a(context, new FileOutputStream(context), 0);
        } else
        {
            return new a(null, null, 492);
        }
    }

    private static String a(int j, String s, String s1, boolean flag)
    {
        String s2;
        int l;
        l = 1;
        String s3 = (new StringBuilder()).append(s).append(s1).toString();
        if ((new File(s3)).exists())
        {
            break MISSING_BLOCK_LABEL_68;
        }
        s2 = s3;
        if (flag)
        {
            if (j == 1 || j == 2 || j == 3)
            {
                break MISSING_BLOCK_LABEL_68;
            }
            s2 = s3;
        }
        return s2;
        String s4;
        int k;
        s4 = (new StringBuilder()).append(s).append("-").toString();
        k = 1;
        j = l;
          goto _L1
        continue; /* Loop/switch isn't completed */
        k *= 10;
    }

    private static String a(String s, int j, String s1, int k)
    {
        String s2;
label0:
        {
            Object obj = null;
            s2 = obj;
            if (s == null)
            {
                break label0;
            }
            j = s1.lastIndexOf('.');
            String s3 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(s1.substring(j + 1));
            if (s3 != null)
            {
                s2 = obj;
                if (s3.equalsIgnoreCase(s))
                {
                    break label0;
                }
            }
            s = a(s, false);
            s2 = s;
            if (s == null);
        }
        s = s2;
        if (s2 == null)
        {
            s = s1.substring(k);
        }
        return s;
    }

    private static String a(String s, String s1, String s2, String s3, int j)
    {
        Object obj = null;
        String s4 = obj;
        if (true)
        {
            s4 = obj;
            if (s1 != null)
            {
                s4 = obj;
                if (!s1.endsWith("/"))
                {
                    s4 = obj;
                    if (s1.length() > 0)
                    {
                        j = s1.lastIndexOf('/') + 1;
                        if (j > 0)
                        {
                            s4 = s1.substring(j);
                        } else
                        {
                            s4 = s1;
                        }
                    }
                }
            }
        }
        s1 = s4;
        if (s4 == null)
        {
            s1 = s4;
            if (s2 != null)
            {
                s2 = b(s2);
                s1 = s2;
                if (s2 != null)
                {
                    j = s2.lastIndexOf('/') + 1;
                    s1 = s2;
                    if (j > 0)
                    {
                        s1 = s2.substring(j);
                    }
                }
            }
        }
        if (s1 == null && s3 != null)
        {
            s2 = Uri.decode(s3);
            if (s2 != null && !s2.endsWith("/") && s2.indexOf('?') < 0)
            {
                j = s2.lastIndexOf('/') + 1;
                s1 = s2;
                if (j > 0)
                {
                    s1 = s2.substring(j);
                }
            }
        }
        s2 = s1;
        if (s1 == null)
        {
            s = Uri.decode(s);
            s2 = s1;
            if (s != null)
            {
                s2 = s1;
                if (!s.endsWith("/"))
                {
                    s2 = s1;
                    if (s.indexOf('?') < 0)
                    {
                        j = s.lastIndexOf('/') + 1;
                        s2 = s1;
                        if (j > 0)
                        {
                            s2 = s.substring(j);
                        }
                    }
                }
            }
        }
        s = s2;
        if (s2 == null)
        {
            s = "downloadfile";
        }
        return s;
    }

    private static String a(String s, boolean flag)
    {
        String s1;
        String s3;
        s1 = null;
        if (s != null)
        {
            String s2 = MimeTypeMap.getSingleton().getExtensionFromMimeType(s);
            s1 = s2;
            if (s2 != null)
            {
                s1 = (new StringBuilder()).append(".").append(s2).toString();
            }
        }
        s3 = s1;
        if (s1 != null) goto _L2; else goto _L1
_L1:
        if (s == null || !s.toLowerCase().startsWith("text/")) goto _L4; else goto _L3
_L3:
        if (!s.equalsIgnoreCase("text/html")) goto _L6; else goto _L5
_L5:
        s3 = ".html";
_L2:
        return s3;
_L6:
        s3 = s1;
        if (flag)
        {
            return ".txt";
        }
        continue; /* Loop/switch isn't completed */
_L4:
        s3 = s1;
        if (flag)
        {
            return ".bin";
        }
        if (true) goto _L2; else goto _L7
_L7:
    }

    private static void a(i j)
    {
        do
        {
            if (j.a() == 1)
            {
                j.b();
                a(j);
                if (j.a() != 2)
                {
                    throw new IllegalArgumentException("syntax error, unmatched parenthese");
                }
                j.b();
            } else
            {
                b(j);
            }
            if (j.a() != 3)
            {
                return;
            }
            j.b();
        } while (true);
    }

    public static void a(String s, Set set)
    {
        if (s != null)
        {
            try
            {
                s = new i(s, set);
                a(((i) (s)));
                if (s.a() != 9)
                {
                    throw new IllegalArgumentException("syntax error");
                }
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                throw s;
            }
        }
    }

    public static boolean a(Context context)
    {
        context = (ConnectivityManager)context.getSystemService("connectivity");
        if (context != null) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        if ((context = context.getAllNetworkInfo()) != null)
        {
            int j = 0;
            while (j < context.length) 
            {
                if (context[j].getState() == android.net.NetworkInfo.State.CONNECTED)
                {
                    return true;
                }
                j++;
            }
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    public static final boolean a(Context context, long l)
    {
        Cursor cursor = null;
        Object obj;
        long l1;
        long l2;
        long l3;
        boolean flag;
        try
        {
            obj = context.getContentResolver().query(Downloads.a, null, "( status = '200' AND destination = '2' )", null, "lastmod");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            cursor = null;
            l = 0L;
            continue; /* Loop/switch isn't completed */
        }
        finally { }
        cursor = ((Cursor) (obj));
        if (cursor == null)
        {
            cursor.close();
            return false;
        }
        cursor.moveToFirst();
        l1 = 0L;
_L2:
        l2 = l1;
        if (cursor.isAfterLast() || l1 >= l)
        {
            break; /* Loop/switch isn't completed */
        }
        l2 = l1;
        obj = new File(cursor.getString(cursor.getColumnIndex("_data")));
        l2 = l1;
        l1 += ((File) (obj)).length();
        l2 = l1;
        ((File) (obj)).delete();
        l2 = l1;
        l3 = cursor.getLong(cursor.getColumnIndex("_id"));
        l2 = l1;
        context.getContentResolver().delete(ContentUris.withAppendedId(Downloads.a, l3), null, null);
        l2 = l1;
        cursor.moveToNext();
        if (true) goto _L2; else goto _L1
        context;
        l = l2;
_L7:
        cursor.close();
_L3:
        if (l > 0L)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        return flag;
_L1:
        cursor.close();
        l = l1;
          goto _L3
_L5:
        cursor.close();
        throw context;
        context;
        if (true) goto _L5; else goto _L4
_L4:
        context;
        l = 0L;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public static boolean a(String s)
    {
        s = (new File(s)).getParentFile();
        return s.equals(Environment.getDownloadCacheDirectory()) || s.equals(new File((new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/libs/tmp").toString()));
    }

    private static String b(String s)
    {
        s = b.matcher(s);
        if (!s.find())
        {
            break MISSING_BLOCK_LABEL_24;
        }
        s = s.group(1);
        return s;
        s;
        return null;
    }

    private static void b(i j)
    {
        if (j.a() != 4)
        {
            throw new IllegalArgumentException("syntax error, expected column name");
        }
        j.b();
        if (j.a() == 5)
        {
            j.b();
            if (j.a() != 6)
            {
                throw new IllegalArgumentException("syntax error, expected quoted string");
            } else
            {
                j.b();
                return;
            }
        }
        if (j.a() == 7)
        {
            j.b();
            if (j.a() != 8)
            {
                throw new IllegalArgumentException("syntax error, expected NULL");
            } else
            {
                j.b();
                return;
            }
        } else
        {
            throw new IllegalArgumentException("syntax error after column name");
        }
    }

    public static boolean b(Context context)
    {
        context = (ConnectivityManager)context.getSystemService("connectivity");
        if (context != null)
        {
            if ((context = context.getActiveNetworkInfo()) != null && context.getType() == 1)
            {
                return true;
            }
        }
        return false;
    }

    public static boolean c(Context context)
    {
        Object obj = (ConnectivityManager)context.getSystemService("connectivity");
        if (obj != null)
        {
            if ((obj = ((ConnectivityManager) (obj)).getActiveNetworkInfo()) != null && ((NetworkInfo) (obj)).getType() == 0 && ((context = (TelephonyManager)context.getSystemService("phone")) != null && context.isNetworkRoaming()))
            {
                return true;
            }
        }
        return false;
    }

}
