// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.download;

import java.io.Serializable;
import java.util.Random;

// Referenced classes of package com.igexin.download:
//            h, Downloads

public class DownloadInfo
    implements Serializable, Cloneable
{

    long a;
    public int mControl;
    public String mCookies;
    public long mCreateMod;
    public int mCurrentBytes;
    public String mData1;
    public long mData10;
    public String mData2;
    public String mData3;
    public String mData4;
    public String mData5;
    public String mData6;
    public String mData7;
    public String mData8;
    public String mData9;
    public int mDestination;
    public long mDownSpeed;
    public String mETag;
    public String mExtras;
    public String mFileName;
    public int mFuzz;
    public volatile boolean mHasActiveThread;
    public String mHint;
    public int mId;
    public int mIsWebIcon;
    public int mLastBytes;
    public long mLastMod;
    public boolean mMediaScanned;
    public String mMimeType;
    public boolean mNoIntegrity;
    public boolean mNotice;
    public boolean mNotify;
    public int mNumFailed;
    public int mRedirectCount;
    public String mReferer;
    public int mRetryAfter;
    public int mStatus;
    public int mTotalBytes;
    public String mUri;
    public String mUserAgent;
    public int mVisibility;

    public DownloadInfo()
    {
        mNotice = true;
        mNotify = true;
    }

    public DownloadInfo(int i, String s, boolean flag, String s1, String s2, String s3, int j, 
            int k, int l, int i1, int j1, int k1, int l1, long l2, long l3, String s4, String s5, String s6, String s7, 
            int i2, int j2, String s8, String s9, String s10, String s11, String s12, 
            String s13, String s14, String s15, String s16, String s17, long l4, 
            int k2, boolean flag1)
    {
        mNotice = true;
        mNotify = true;
        mId = i;
        mUri = s;
        mNoIntegrity = flag;
        mHint = s1;
        mFileName = s2;
        mMimeType = s3;
        mDestination = j;
        mVisibility = k;
        mControl = l;
        mStatus = i1;
        mNumFailed = j1;
        mRetryAfter = k1;
        mRedirectCount = l1;
        mLastMod = l2;
        mCreateMod = l3;
        mExtras = s4;
        mCookies = s5;
        mUserAgent = s6;
        mReferer = s7;
        mTotalBytes = i2;
        mCurrentBytes = j2;
        mETag = s8;
        mFuzz = h.a.nextInt(1001);
        mData1 = s9;
        mData2 = s10;
        mData3 = s11;
        mData4 = s12;
        mData5 = s13;
        mData6 = s14;
        mData7 = s15;
        mData8 = s16;
        mData9 = s17;
        mData10 = l4;
        mIsWebIcon = k2;
        mMediaScanned = flag1;
    }

    public boolean canUseNetwork(boolean flag, boolean flag1)
    {
        boolean flag2 = true;
        if (!flag)
        {
            flag = false;
        } else
        {
            flag = flag2;
            if (mDestination == 3)
            {
                flag = flag2;
                if (flag1)
                {
                    return false;
                }
            }
        }
        return flag;
    }

    public DownloadInfo clone()
    {
        DownloadInfo downloadinfo;
        try
        {
            downloadinfo = (DownloadInfo)super.clone();
        }
        catch (CloneNotSupportedException clonenotsupportedexception)
        {
            return null;
        }
        return downloadinfo;
    }

    public volatile Object clone()
    {
        return clone();
    }

    public void copyFrom(DownloadInfo downloadinfo)
    {
        mControl = downloadinfo.mControl;
        mStatus = downloadinfo.mStatus;
        mTotalBytes = downloadinfo.mTotalBytes;
        mCurrentBytes = downloadinfo.mCurrentBytes;
        mLastBytes = downloadinfo.mLastBytes;
        a = downloadinfo.a;
        mNotice = downloadinfo.mNotice;
        if (mFileName == null)
        {
            mFileName = downloadinfo.mFileName;
        }
    }

    public boolean hasCompletionNotification()
    {
        while (!Downloads.isStatusCompleted(mStatus) || mVisibility != 1) 
        {
            return false;
        }
        return true;
    }

    public boolean isReadyToRestart(long l)
    {
        if (mControl != 1)
        {
            if (mStatus == 0)
            {
                return true;
            }
            if (mStatus == 190)
            {
                return true;
            }
            if (mStatus == 193 || Downloads.isStatusError(mStatus))
            {
                if (mNumFailed == 0)
                {
                    return true;
                }
                if (restartTime() < l)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isReadyToStart(long l)
    {
        if (mControl != 1)
        {
            if (mStatus == 0)
            {
                return true;
            }
            if (mStatus == 190)
            {
                return true;
            }
            if (mStatus == 192)
            {
                return true;
            }
            if (mStatus == 193)
            {
                if (mNumFailed == 0)
                {
                    return true;
                }
                if (restartTime() < l)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void refreshSpeed()
    {
        if (Downloads.isStatusCompleted(mStatus) || mStatus != 192)
        {
            mDownSpeed = 0L;
        } else
        {
            long l = System.currentTimeMillis() - a;
            a = System.currentTimeMillis();
            if (mCurrentBytes == mLastBytes)
            {
                if (l > 1500L)
                {
                    mDownSpeed = 0L;
                    return;
                }
                if (l > 500L)
                {
                    mDownSpeed = mDownSpeed >> 1;
                    return;
                }
            } else
            {
                if (a != 0L)
                {
                    mDownSpeed = (long)(Math.max(0, mCurrentBytes - mLastBytes) * 1000) / l;
                } else
                {
                    mDownSpeed = 0L;
                }
                mLastBytes = mCurrentBytes;
                return;
            }
        }
    }

    public long restartTime()
    {
        if (mRetryAfter > 0)
        {
            return mLastMod + (long)mRetryAfter;
        } else
        {
            return mLastMod + (long)((mFuzz + 1000) * 30 * (1 << mNumFailed - 1));
        }
    }
}
