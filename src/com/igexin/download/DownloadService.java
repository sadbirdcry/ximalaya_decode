// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.download;

import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.CharArrayBuffer;
import android.database.Cursor;
import android.os.IBinder;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

// Referenced classes of package com.igexin.download:
//            DownloadInfo, Downloads, f, h, 
//            SdkDownLoader, g, d, e

public class DownloadService extends Service
{

    static boolean a = false;
    private d b;
    private ArrayList c;
    private f d;
    private boolean e;
    private e f;
    private boolean g;
    private Object h;
    private CharArrayBuffer i;
    private CharArrayBuffer j;

    public DownloadService()
    {
    }

    private long a(int k, long l)
    {
        DownloadInfo downloadinfo = (DownloadInfo)c.get(k);
        if (Downloads.isStatusCompleted(downloadinfo.mStatus))
        {
            return -1L;
        }
        if (downloadinfo.mStatus != 193)
        {
            return 0L;
        }
        if (downloadinfo.mNumFailed == 0)
        {
            return 0L;
        }
        long l1 = downloadinfo.restartTime();
        if (l1 <= l)
        {
            return 0L;
        } else
        {
            return l1 - l;
        }
    }

    static long a(DownloadService downloadservice, int k, long l)
    {
        return downloadservice.a(k, l);
    }

    static CharArrayBuffer a(DownloadService downloadservice, CharArrayBuffer chararraybuffer)
    {
        downloadservice.i = chararraybuffer;
        return chararraybuffer;
    }

    static f a(DownloadService downloadservice, f f1)
    {
        downloadservice.d = f1;
        return f1;
    }

    static Object a(DownloadService downloadservice, Object obj)
    {
        downloadservice.h = obj;
        return obj;
    }

    private String a(String s, Cursor cursor, String s1)
    {
        int k = cursor.getColumnIndexOrThrow(s1);
        if (s != null) goto _L2; else goto _L1
_L1:
        cursor = cursor.getString(k);
_L4:
        return cursor;
_L2:
        if (j == null)
        {
            j = new CharArrayBuffer(128);
        }
        cursor.copyStringToBuffer(k, j);
        int l = j.sizeCopied;
        if (l != s.length())
        {
            return cursor.getString(k);
        }
        if (i == null || i.sizeCopied < l)
        {
            i = new CharArrayBuffer(l);
        }
        s1 = i.data;
        char ac[] = j.data;
        s.getChars(0, l, s1, 0);
        k = l - 1;
        do
        {
            cursor = s;
            if (k < 0)
            {
                continue;
            }
            if (s1[k] != ac[k])
            {
                return new String(ac, 0, l);
            }
            k--;
        } while (true);
        if (true) goto _L4; else goto _L3
_L3:
    }

    private void a()
    {
        this;
        JVM INSTR monitorenter ;
        e = true;
        if (d == null)
        {
            d = new f(this);
            d.start();
        }
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private void a(Cursor cursor, int k, boolean flag, boolean flag1, long l)
    {
        int j2 = cursor.getColumnIndexOrThrow("status");
        int k2 = cursor.getColumnIndexOrThrow("numfailed");
        int i1 = cursor.getInt(cursor.getColumnIndexOrThrow("method"));
        int j1 = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        String s = cursor.getString(cursor.getColumnIndexOrThrow("uri"));
        String s1;
        String s2;
        String s3;
        String s4;
        String s5;
        String s6;
        String s7;
        String s8;
        String s9;
        String s10;
        String s11;
        String s12;
        String s13;
        String s14;
        String s15;
        String s16;
        String s17;
        int k1;
        int l1;
        int i2;
        int l2;
        int i3;
        int j3;
        long l3;
        long l4;
        long l5;
        boolean flag2;
        boolean flag3;
        if (cursor.getInt(cursor.getColumnIndexOrThrow("no_integrity")) == 1)
        {
            flag2 = true;
        } else
        {
            flag2 = false;
        }
        s1 = cursor.getString(cursor.getColumnIndexOrThrow("hint"));
        s2 = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
        s3 = cursor.getString(cursor.getColumnIndexOrThrow("mimetype"));
        k1 = cursor.getInt(cursor.getColumnIndexOrThrow("destination"));
        l1 = cursor.getInt(cursor.getColumnIndexOrThrow("visibility"));
        i2 = cursor.getInt(cursor.getColumnIndexOrThrow("control"));
        j2 = cursor.getInt(j2);
        k2 = cursor.getInt(k2);
        l3 = cursor.getLong(cursor.getColumnIndexOrThrow("lastmod"));
        l4 = cursor.getLong(cursor.getColumnIndexOrThrow("createmod"));
        s4 = cursor.getString(cursor.getColumnIndexOrThrow("extras"));
        s5 = cursor.getString(cursor.getColumnIndexOrThrow("cookiedata"));
        s6 = cursor.getString(cursor.getColumnIndexOrThrow("useragent"));
        s7 = cursor.getString(cursor.getColumnIndexOrThrow("referer"));
        l2 = cursor.getInt(cursor.getColumnIndexOrThrow("total_bytes"));
        i3 = cursor.getInt(cursor.getColumnIndexOrThrow("current_bytes"));
        s8 = cursor.getString(cursor.getColumnIndexOrThrow("etag"));
        s9 = cursor.getString(cursor.getColumnIndexOrThrow("data_1"));
        s10 = cursor.getString(cursor.getColumnIndexOrThrow("data_2"));
        s11 = cursor.getString(cursor.getColumnIndexOrThrow("data_3"));
        s12 = cursor.getString(cursor.getColumnIndexOrThrow("data_4"));
        s13 = cursor.getString(cursor.getColumnIndexOrThrow("data_5"));
        s14 = cursor.getString(cursor.getColumnIndexOrThrow("data_6"));
        s15 = cursor.getString(cursor.getColumnIndexOrThrow("data_7"));
        s16 = cursor.getString(cursor.getColumnIndexOrThrow("data_8"));
        s17 = cursor.getString(cursor.getColumnIndexOrThrow("data_9"));
        l5 = cursor.getLong(cursor.getColumnIndexOrThrow("data_10"));
        j3 = cursor.getInt(cursor.getColumnIndexOrThrow("iswebicon"));
        if (cursor.getInt(cursor.getColumnIndexOrThrow("scanned")) == 1)
        {
            flag3 = true;
        } else
        {
            flag3 = false;
        }
        cursor = new DownloadInfo(j1, s, flag2, s1, s2, s3, k1, l1, i2, j2, k2, 0xfffffff & i1, i1 >> 28, l3, l4, s4, s5, s6, s7, l2, i3, s8, s9, s10, s11, s12, s13, s14, s15, s16, s17, l5, j3, flag3);
        c.add(k, cursor);
        break MISSING_BLOCK_LABEL_637;
label0:
        while (((DownloadInfo) (cursor)).mStatus == 190) 
        {
            do
            {
                do
                {
                    return;
                } while (!cursor.canUseNetwork(flag, flag1) || "wifi".equals(((DownloadInfo) (cursor)).mData9) && !com.igexin.download.h.b(this) || !cursor.isReadyToStart(l));
                if (!a(SdkDownLoader.a))
                {
                    continue label0;
                }
            } while (((DownloadInfo) (cursor)).mHasActiveThread);
            if (((DownloadInfo) (cursor)).mStatus != 192)
            {
                cursor.mStatus = 192;
                ContentValues contentvalues = new ContentValues();
                contentvalues.put("status", Integer.valueOf(((DownloadInfo) (cursor)).mStatus));
                getContentResolver().update(ContentUris.withAppendedId(Downloads.a, ((DownloadInfo) (cursor)).mId), contentvalues, null, null);
            }
            g g1 = new g(this, cursor);
            cursor.mHasActiveThread = true;
            g1.start();
            cursor.mNotice = false;
            return;
        }
        cursor.mStatus = 190;
        ContentValues contentvalues1 = new ContentValues();
        contentvalues1.put("status", Integer.valueOf(((DownloadInfo) (cursor)).mStatus));
        getContentResolver().update(ContentUris.withAppendedId(Downloads.a, ((DownloadInfo) (cursor)).mId), contentvalues1, null, null);
        return;
    }

    static void a(DownloadService downloadservice)
    {
        downloadservice.a();
    }

    static void a(DownloadService downloadservice, int k)
    {
        downloadservice.b(k);
    }

    static void a(DownloadService downloadservice, Cursor cursor, int k, boolean flag, boolean flag1, long l)
    {
        downloadservice.a(cursor, k, flag, flag1, l);
    }

    private boolean a(int k)
    {
        Cursor cursor = getContentResolver().query(Downloads.a, new String[] {
            "_id"
        }, "status == '192'", null, null);
        if (cursor != null)
        {
            boolean flag;
            if (cursor.getCount() < k)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            cursor.close();
            return flag;
        } else
        {
            return false;
        }
    }

    private boolean a(Cursor cursor, int k)
    {
        DownloadInfo downloadinfo = (DownloadInfo)c.get(k);
        this;
        JVM INSTR monitorenter ;
        Object obj = h;
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_140;
        }
        h.getClass().getMethod("scanFile", new Class[] {
            java/lang/String, java/lang/String
        }).invoke(h, new Object[] {
            downloadinfo.mFileName, downloadinfo.mMimeType
        });
        downloadinfo.mMediaScanned = true;
        if (cursor == null)
        {
            break MISSING_BLOCK_LABEL_136;
        }
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("scanned", Integer.valueOf(1));
        getContentResolver().update(ContentUris.withAppendedId(Downloads.a, cursor.getLong(cursor.getColumnIndexOrThrow("_id"))), contentvalues, null, null);
        this;
        JVM INSTR monitorexit ;
        return true;
_L2:
        this;
        JVM INSTR monitorexit ;
        return false;
        cursor;
        this;
        JVM INSTR monitorexit ;
        throw cursor;
        cursor;
        continue; /* Loop/switch isn't completed */
        cursor;
        continue; /* Loop/switch isn't completed */
        cursor;
        continue; /* Loop/switch isn't completed */
        cursor;
        continue; /* Loop/switch isn't completed */
        cursor;
        continue; /* Loop/switch isn't completed */
        cursor;
        if (true) goto _L2; else goto _L1
_L1:
    }

    static boolean a(DownloadService downloadservice, Cursor cursor, int k)
    {
        return downloadservice.a(cursor, k);
    }

    static boolean a(DownloadService downloadservice, boolean flag)
    {
        downloadservice.g = flag;
        return flag;
    }

    private boolean a(String s)
    {
        return true;
    }

    static CharArrayBuffer b(DownloadService downloadservice, CharArrayBuffer chararraybuffer)
    {
        downloadservice.j = chararraybuffer;
        return chararraybuffer;
    }

    static Object b(DownloadService downloadservice)
    {
        return downloadservice.h;
    }

    private void b(int k)
    {
        DownloadInfo downloadinfo = (DownloadInfo)c.get(k);
        if (downloadinfo.mStatus != 192) goto _L2; else goto _L1
_L1:
        downloadinfo.mStatus = 490;
_L4:
        c.remove(k);
        return;
_L2:
        if (downloadinfo.mDestination != 0 && downloadinfo.mFileName != null)
        {
            (new File(downloadinfo.mFileName)).delete();
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private void b(Cursor cursor, int k, boolean flag, boolean flag1, long l)
    {
        DownloadInfo downloadinfo;
        downloadinfo = (DownloadInfo)c.get(k);
        k = cursor.getColumnIndexOrThrow("status");
        int i1 = cursor.getColumnIndexOrThrow("numfailed");
        downloadinfo.mId = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        downloadinfo.mUri = a(downloadinfo.mUri, cursor, "uri");
        boolean flag2;
        if (cursor.getInt(cursor.getColumnIndexOrThrow("no_integrity")) == 1)
        {
            flag2 = true;
        } else
        {
            flag2 = false;
        }
        downloadinfo.mNoIntegrity = flag2;
        downloadinfo.mHint = a(downloadinfo.mHint, cursor, "hint");
        downloadinfo.mFileName = a(downloadinfo.mFileName, cursor, "_data");
        downloadinfo.mMimeType = a(downloadinfo.mMimeType, cursor, "mimetype");
        downloadinfo.mDestination = cursor.getInt(cursor.getColumnIndexOrThrow("destination"));
        downloadinfo.mVisibility = cursor.getInt(cursor.getColumnIndexOrThrow("visibility"));
        downloadinfo;
        JVM INSTR monitorenter ;
        downloadinfo.mControl = cursor.getInt(cursor.getColumnIndexOrThrow("control"));
        downloadinfo;
        JVM INSTR monitorexit ;
        downloadinfo.mStatus = cursor.getInt(k);
        downloadinfo.mNumFailed = cursor.getInt(i1);
        k = cursor.getInt(cursor.getColumnIndexOrThrow("method"));
        downloadinfo.mRetryAfter = 0xfffffff & k;
        downloadinfo.mRedirectCount = k >> 28;
        downloadinfo.mLastMod = cursor.getLong(cursor.getColumnIndexOrThrow("lastmod"));
        downloadinfo.mCreateMod = cursor.getLong(cursor.getColumnIndexOrThrow("createmod"));
        downloadinfo.mCookies = a(downloadinfo.mCookies, cursor, "cookiedata");
        downloadinfo.mExtras = a(downloadinfo.mExtras, cursor, "extras");
        downloadinfo.mUserAgent = a(downloadinfo.mUserAgent, cursor, "useragent");
        downloadinfo.mReferer = a(downloadinfo.mReferer, cursor, "referer");
        downloadinfo.mTotalBytes = cursor.getInt(cursor.getColumnIndexOrThrow("total_bytes"));
        downloadinfo.mCurrentBytes = cursor.getInt(cursor.getColumnIndexOrThrow("current_bytes"));
        downloadinfo.mETag = a(downloadinfo.mETag, cursor, "etag");
          goto _L1
_L3:
        return;
        cursor;
        downloadinfo;
        JVM INSTR monitorexit ;
        throw cursor;
_L1:
        if (!downloadinfo.canUseNetwork(flag, flag1) || "wifi".equals(downloadinfo.mData9) && !com.igexin.download.h.b(this) || !downloadinfo.isReadyToRestart(l)) goto _L3; else goto _L2
_L2:
        if (!a(SdkDownLoader.a))
        {
            continue; /* Loop/switch isn't completed */
        }
        if (downloadinfo.mHasActiveThread) goto _L3; else goto _L4
_L4:
        downloadinfo.mStatus = 192;
        cursor = new ContentValues();
        cursor.put("status", Integer.valueOf(downloadinfo.mStatus));
        getContentResolver().update(ContentUris.withAppendedId(Downloads.a, downloadinfo.mId), cursor, null, null);
        cursor = new g(this, downloadinfo);
        downloadinfo.mHasActiveThread = true;
        cursor.start();
        downloadinfo.mNotice = false;
        return;
        if (downloadinfo.mStatus == 190) goto _L3; else goto _L5
_L5:
        downloadinfo.mStatus = 190;
        cursor = new ContentValues();
        cursor.put("status", Integer.valueOf(downloadinfo.mStatus));
        getContentResolver().update(ContentUris.withAppendedId(Downloads.a, downloadinfo.mId), cursor, null, null);
        return;
    }

    static void b(DownloadService downloadservice, Cursor cursor, int k, boolean flag, boolean flag1, long l)
    {
        downloadservice.b(cursor, k, flag, flag1, l);
    }

    private boolean b()
    {
        return h != null;
    }

    static boolean b(DownloadService downloadservice, int k)
    {
        return downloadservice.d(k);
    }

    static boolean b(DownloadService downloadservice, boolean flag)
    {
        downloadservice.e = flag;
        return flag;
    }

    static f c(DownloadService downloadservice)
    {
        return downloadservice.d;
    }

    private boolean c(int k)
    {
        return ((DownloadInfo)c.get(k)).hasCompletionNotification();
    }

    static boolean c(DownloadService downloadservice, int k)
    {
        return downloadservice.c(k);
    }

    private boolean d(int k)
    {
        DownloadInfo downloadinfo = (DownloadInfo)c.get(k);
        return !downloadinfo.mMediaScanned && downloadinfo.mDestination == 0 && Downloads.isStatusSuccess(downloadinfo.mStatus) && !a(downloadinfo.mMimeType);
    }

    static boolean d(DownloadService downloadservice)
    {
        return downloadservice.e;
    }

    static ArrayList e(DownloadService downloadservice)
    {
        return downloadservice.c;
    }

    static boolean f(DownloadService downloadservice)
    {
        return downloadservice.b();
    }

    static boolean g(DownloadService downloadservice)
    {
        return downloadservice.g;
    }

    static e h(DownloadService downloadservice)
    {
        return downloadservice.f;
    }

    public IBinder onBind(Intent intent)
    {
        throw new UnsupportedOperationException("Cannot bind to Download Manager Service");
    }

    public void onCreate()
    {
        super.onCreate();
        try
        {
            c = (ArrayList)Class.forName("com.google.android.collect.Lists").getMethod("newArrayList", null).invoke(null, new Object[0]);
        }
        catch (ClassNotFoundException classnotfoundexception) { }
        catch (SecurityException securityexception) { }
        catch (NoSuchMethodException nosuchmethodexception) { }
        catch (IllegalArgumentException illegalargumentexception) { }
        catch (IllegalAccessException illegalaccessexception) { }
        catch (InvocationTargetException invocationtargetexception) { }
        b = new d(this);
        getContentResolver().registerContentObserver(Downloads.a, true, b);
        h = null;
        g = false;
        f = new e(this);
    }

    public void onDestroy()
    {
        getContentResolver().unregisterContentObserver(b);
        super.onDestroy();
    }

    public void onStart(Intent intent, int k)
    {
        super.onStart(intent, k);
        a();
    }

}
