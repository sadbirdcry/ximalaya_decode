// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.Menu;

public interface IPushCore
{

    public abstract void onActivityConfigurationChanged(Activity activity, Configuration configuration);

    public abstract boolean onActivityCreateOptionsMenu(Activity activity, Menu menu);

    public abstract void onActivityDestroy(Activity activity);

    public abstract boolean onActivityKeyDown(Activity activity, int i, KeyEvent keyevent);

    public abstract void onActivityNewIntent(Activity activity, Intent intent);

    public abstract void onActivityPause(Activity activity);

    public abstract void onActivityRestart(Activity activity);

    public abstract void onActivityResume(Activity activity);

    public abstract void onActivityStart(Activity activity, Intent intent);

    public abstract void onActivityStop(Activity activity);

    public abstract IBinder onServiceBind(Intent intent);

    public abstract void onServiceDestroy();

    public abstract int onServiceStartCommand(Intent intent, int i, int j);

    public abstract boolean start(Context context);
}
