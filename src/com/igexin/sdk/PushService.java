// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.sdk;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Process;
import com.igexin.push.a.l;
import com.igexin.push.core.g;
import com.igexin.sdk.a.a;
import com.igexin.sdk.a.d;
import java.util.HashMap;

// Referenced classes of package com.igexin.sdk:
//            IPushCore, PushConsts

public class PushService extends Service
{

    private static String a = "PushSdk";
    private IPushCore b;
    private boolean c;

    public PushService()
    {
        c = false;
    }

    public IBinder onBind(Intent intent)
    {
        if (b != null)
        {
            return b.onServiceBind(intent);
        } else
        {
            return null;
        }
    }

    public void onCreate()
    {
        super.onCreate();
    }

    public void onDestroy()
    {
        if (b != null)
        {
            b.onServiceDestroy();
        }
        Process.killProcess(Process.myPid());
        super.onDestroy();
    }

    public void onLowMemory()
    {
        super.onLowMemory();
    }

    public int onStartCommand(Intent intent, int i, int j)
    {
        super.onStartCommand(intent, i, j);
        if (c) goto _L2; else goto _L1
_L1:
        c = true;
        if (intent == null) goto _L4; else goto _L3
_L3:
        String s = intent.getStringExtra("action");
        if (PushConsts.ACTION_SERVICE_INITIALIZE.equals(s) || "com.igexin.action.initialize.slave".equals(s)) goto _L4; else goto _L5
_L5:
        l.a(this);
        if (!"1".equals(g.c().get("ss")) || (new d(this)).c()) goto _L4; else goto _L6
_L6:
        stopSelf();
_L8:
        return 1;
_L4:
        com.igexin.sdk.a.a.a().a(this);
        b = com.igexin.sdk.a.a.a().b();
        b.start(this);
_L2:
        if (b != null)
        {
            return b.onServiceStartCommand(intent, i, j);
        }
        if (true) goto _L8; else goto _L7
_L7:
    }

}
