// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.sdk.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;

// Referenced classes of package com.igexin.sdk.aidl:
//            ICACallback, b

public abstract class a extends Binder
    implements ICACallback
{

    public static ICACallback a(IBinder ibinder)
    {
        if (ibinder == null)
        {
            return null;
        }
        android.os.IInterface iinterface = ibinder.queryLocalInterface("com.igexin.sdk.aidl.ICACallback");
        if (iinterface != null && (iinterface instanceof ICACallback))
        {
            return (ICACallback)iinterface;
        } else
        {
            return new b(ibinder);
        }
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
    {
        boolean flag = false;
        boolean flag2;
        switch (i)
        {
        default:
            return super.onTransact(i, parcel, parcel1, j);

        case 1598968902: 
            parcel1.writeString("com.igexin.sdk.aidl.ICACallback");
            return true;

        case 1: // '\001'
            parcel.enforceInterface("com.igexin.sdk.aidl.ICACallback");
            boolean flag1 = onAuthenticated(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readLong());
            parcel1.writeNoException();
            if (flag1)
            {
                i = 1;
            } else
            {
                i = 0;
            }
            parcel1.writeInt(i);
            return true;

        case 2: // '\002'
            parcel.enforceInterface("com.igexin.sdk.aidl.ICACallback");
            flag2 = onError(parcel.readInt());
            parcel1.writeNoException();
            i = ((flag) ? 1 : 0);
            break;
        }
        if (flag2)
        {
            i = 1;
        }
        parcel1.writeInt(i);
        return true;
    }
}
