// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.sdk.aidl;

import android.os.IInterface;

public interface IGexinMsgService
    extends IInterface
{

    public abstract byte[] extFunction(byte abyte0[]);

    public abstract int isStarted(String s);

    public abstract int onASNLConnected(String s, String s1, String s2, long l);

    public abstract int onASNLNetworkConnected();

    public abstract int onASNLNetworkDisconnected();

    public abstract int onPSNLConnected(String s, String s1, String s2, long l);

    public abstract int receiveToPSNL(String s, String s1, byte abyte0[]);

    public abstract int sendByASNL(String s, String s1, byte abyte0[]);

    public abstract int setSilentTime(int i, int j, String s);

    public abstract int startService(String s);

    public abstract int stopService(String s);
}
