// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.sdk.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;

// Referenced classes of package com.igexin.sdk.aidl:
//            IGexinMsgService, d

public abstract class c extends Binder
    implements IGexinMsgService
{

    public c()
    {
        attachInterface(this, "com.igexin.sdk.aidl.IGexinMsgService");
    }

    public static IGexinMsgService a(IBinder ibinder)
    {
        if (ibinder == null)
        {
            return null;
        }
        android.os.IInterface iinterface = ibinder.queryLocalInterface("com.igexin.sdk.aidl.IGexinMsgService");
        if (iinterface != null && (iinterface instanceof IGexinMsgService))
        {
            return (IGexinMsgService)iinterface;
        } else
        {
            return new d(ibinder);
        }
    }

    public IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
    {
        switch (i)
        {
        default:
            return super.onTransact(i, parcel, parcel1, j);

        case 1598968902: 
            parcel1.writeString("com.igexin.sdk.aidl.IGexinMsgService");
            return true;

        case 1: // '\001'
            parcel.enforceInterface("com.igexin.sdk.aidl.IGexinMsgService");
            i = startService(parcel.readString());
            parcel1.writeNoException();
            parcel1.writeInt(i);
            return true;

        case 2: // '\002'
            parcel.enforceInterface("com.igexin.sdk.aidl.IGexinMsgService");
            i = stopService(parcel.readString());
            parcel1.writeNoException();
            parcel1.writeInt(i);
            return true;

        case 3: // '\003'
            parcel.enforceInterface("com.igexin.sdk.aidl.IGexinMsgService");
            i = isStarted(parcel.readString());
            parcel1.writeNoException();
            parcel1.writeInt(i);
            return true;

        case 4: // '\004'
            parcel.enforceInterface("com.igexin.sdk.aidl.IGexinMsgService");
            i = setSilentTime(parcel.readInt(), parcel.readInt(), parcel.readString());
            parcel1.writeNoException();
            parcel1.writeInt(i);
            return true;

        case 5: // '\005'
            parcel.enforceInterface("com.igexin.sdk.aidl.IGexinMsgService");
            parcel = extFunction(parcel.createByteArray());
            parcel1.writeNoException();
            parcel1.writeByteArray(parcel);
            return true;

        case 6: // '\006'
            parcel.enforceInterface("com.igexin.sdk.aidl.IGexinMsgService");
            i = onASNLConnected(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readLong());
            parcel1.writeNoException();
            parcel1.writeInt(i);
            return true;

        case 7: // '\007'
            parcel.enforceInterface("com.igexin.sdk.aidl.IGexinMsgService");
            i = onPSNLConnected(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readLong());
            parcel1.writeNoException();
            parcel1.writeInt(i);
            return true;

        case 8: // '\b'
            parcel.enforceInterface("com.igexin.sdk.aidl.IGexinMsgService");
            i = sendByASNL(parcel.readString(), parcel.readString(), parcel.createByteArray());
            parcel1.writeNoException();
            parcel1.writeInt(i);
            return true;

        case 9: // '\t'
            parcel.enforceInterface("com.igexin.sdk.aidl.IGexinMsgService");
            i = receiveToPSNL(parcel.readString(), parcel.readString(), parcel.createByteArray());
            parcel1.writeNoException();
            parcel1.writeInt(i);
            return true;

        case 10: // '\n'
            parcel.enforceInterface("com.igexin.sdk.aidl.IGexinMsgService");
            i = onASNLNetworkConnected();
            parcel1.writeNoException();
            parcel1.writeInt(i);
            return true;

        case 11: // '\013'
            parcel.enforceInterface("com.igexin.sdk.aidl.IGexinMsgService");
            i = onASNLNetworkDisconnected();
            parcel1.writeNoException();
            parcel1.writeInt(i);
            return true;
        }
    }
}
