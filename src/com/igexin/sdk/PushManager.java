// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.igexin.sdk.a.c;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

// Referenced classes of package com.igexin.sdk:
//            PushService, PushConsts, Tag

public class PushManager
{

    private static PushManager a;
    private long b;
    private long c;
    private byte d[];

    public PushManager()
    {
        b = 0L;
        c = 0L;
        d = null;
    }

    private String a(String s)
    {
        Object obj;
        int i;
        int j;
        byte byte0;
        try
        {
            obj = MessageDigest.getInstance("MD5");
            ((MessageDigest) (obj)).update(s.getBytes());
            s = ((MessageDigest) (obj)).digest();
            obj = new StringBuffer("");
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return null;
        }
        i = 0;
        if (i >= s.length)
        {
            break; /* Loop/switch isn't completed */
        }
        byte0 = s[i];
        j = byte0;
        if (byte0 < 0)
        {
            j = byte0 + 256;
        }
        if (j >= 16)
        {
            break MISSING_BLOCK_LABEL_73;
        }
        ((StringBuffer) (obj)).append("0");
        ((StringBuffer) (obj)).append(Integer.toHexString(j));
        i++;
        if (true) goto _L2; else goto _L1
_L2:
        break MISSING_BLOCK_LABEL_31;
_L1:
        s = ((StringBuffer) (obj)).toString();
        return s;
    }

    private byte[] a(Context context)
    {
        Object obj;
        Object obj1;
        Object obj2;
        obj2 = null;
        obj = (new StringBuilder()).append("/data/data/").append(context.getPackageName()).append("/files/").append("init.pid").toString();
        obj1 = obj2;
        if (!(new File(((String) (obj)))).exists()) goto _L2; else goto _L1
_L1:
        context = new byte[1024];
        int i;
        try
        {
            obj1 = new FileInputStream(((String) (obj)));
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context = null;
            obj1 = null;
            continue; /* Loop/switch isn't completed */
        }
        finally
        {
            obj1 = null;
            obj = null;
        }
        obj = new ByteArrayOutputStream();
_L5:
        i = ((FileInputStream) (obj1)).read(context);
        if (i == -1) goto _L4; else goto _L3
_L3:
        ((ByteArrayOutputStream) (obj)).write(context, 0, i);
          goto _L5
        context;
        context = ((Context) (obj));
_L10:
        if (obj1 != null)
        {
            try
            {
                ((FileInputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        obj1 = obj2;
        if (context != null)
        {
            try
            {
                context.close();
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                return null;
            }
            obj1 = obj2;
        }
_L2:
        return ((byte []) (obj1));
_L4:
        context = ((ByteArrayOutputStream) (obj)).toByteArray();
        if (obj1 != null)
        {
            try
            {
                ((FileInputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1) { }
        }
        obj1 = context;
        if (obj == null) goto _L2; else goto _L6
_L6:
        try
        {
            ((ByteArrayOutputStream) (obj)).close();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            return context;
        }
        return context;
_L8:
        if (obj1 != null)
        {
            try
            {
                ((FileInputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1) { }
        }
        if (obj != null)
        {
            try
            {
                ((ByteArrayOutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        throw context;
        context;
        obj = null;
        continue; /* Loop/switch isn't completed */
        context;
        if (true) goto _L8; else goto _L7
_L7:
        context;
        context = null;
        if (true) goto _L10; else goto _L9
_L9:
    }

    public static PushManager getInstance()
    {
        if (a == null)
        {
            a = new PushManager();
        }
        return a;
    }

    public String getClientid(Context context)
    {
        if (d != null)
        {
            context = a(context);
            if (d != null && context != null && d.length == context.length)
            {
                byte abyte0[] = new byte[context.length];
                for (int i = 0; i < abyte0.length; i++)
                {
                    abyte0[i] = (byte)(d[i] ^ context[i]);
                }

                return new String(abyte0);
            }
        }
        return null;
    }

    public String getVersion(Context context)
    {
        return "2.3.0.0";
    }

    public void initialize(Context context)
    {
        Object obj;
        String s;
        String s1;
        try
        {
            obj = context.getApplicationContext().getPackageName();
            Intent intent = new Intent(context.getApplicationContext(), com/igexin/sdk/PushService);
            intent.putExtra("action", PushConsts.ACTION_SERVICE_INITIALIZE);
            intent.putExtra("op_app", ((String) (obj)));
            context.getApplicationContext().startService(intent);
            obj = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return;
        }
        if (obj == null) goto _L2; else goto _L1
_L1:
        if (((ApplicationInfo) (obj)).metaData == null) goto _L2; else goto _L3
_L3:
        s = ((ApplicationInfo) (obj)).metaData.getString("PUSH_APPID");
        s1 = ((ApplicationInfo) (obj)).metaData.getString("PUSH_APPSECRET");
        if (((ApplicationInfo) (obj)).metaData.get("PUSH_APPKEY") == null)
        {
            break MISSING_BLOCK_LABEL_191;
        }
        obj = ((ApplicationInfo) (obj)).metaData.get("PUSH_APPKEY").toString();
_L4:
        if (!s.equals("") && !s1.equals("") && !((String) (obj)).equals(""))
        {
            d = a((new StringBuilder()).append(s).append(s1).append(((String) (obj))).append(context.getPackageName()).toString()).getBytes();
        }
_L2:
        return;
        obj = null;
          goto _L4
    }

    public boolean isPushTurnedOn(Context context)
    {
        return !(new c(context)).c();
    }

    public boolean sendFeedbackMessage(Context context, String s, String s1, int i)
    {
        if (s == null || s1 == null || i < 0x15f91 || i > 0x16377)
        {
            return false;
        } else
        {
            Intent intent = new Intent("com.igexin.sdk.action.pushmanager");
            intent.putExtra("action", "sendFeedbackMessage");
            intent.putExtra("taskid", s);
            intent.putExtra("messageid", s1);
            intent.putExtra("actionid", String.valueOf(i));
            context.sendBroadcast(intent);
            return true;
        }
    }

    public boolean sendMessage(Context context, String s, byte abyte0[])
    {
        long l = System.currentTimeMillis();
        if (s == null || abyte0 == null || (long)abyte0.length > 4096L || l - c < 1000L)
        {
            return false;
        } else
        {
            Intent intent = new Intent("com.igexin.sdk.action.pushmanager");
            intent.putExtra("action", "sendMessage");
            intent.putExtra("taskid", s);
            intent.putExtra("extraData", abyte0);
            context.sendBroadcast(intent);
            return true;
        }
    }

    public boolean setHeartbeatInterval(Context context, int i)
    {
        if (i < 0)
        {
            return false;
        } else
        {
            Intent intent = new Intent("com.igexin.sdk.action.pushmanager");
            intent.putExtra("action", "setHeartbeatInterval");
            intent.putExtra("interval", i);
            context.sendBroadcast(intent);
            return true;
        }
    }

    public boolean setSilentTime(Context context, int i, int j)
    {
        if (i < 0 || i >= 24 || j < 0 || j > 23)
        {
            return false;
        } else
        {
            Intent intent = new Intent("com.igexin.sdk.action.pushmanager");
            intent.putExtra("action", "setSilentTime");
            intent.putExtra("beginHour", i);
            intent.putExtra("duration", j);
            context.sendBroadcast(intent);
            return true;
        }
    }

    public boolean setSocketTimeout(Context context, int i)
    {
        if (i < 0)
        {
            return false;
        } else
        {
            Intent intent = new Intent("com.igexin.sdk.action.pushmanager");
            intent.putExtra("action", "setSocketTimeout");
            intent.putExtra("timeout", i);
            context.sendBroadcast(intent);
            return true;
        }
    }

    public int setTag(Context context, Tag atag[])
    {
        if (atag != null)
        {
            if ((long)atag.length > 200L)
            {
                return 20001;
            }
            long l = System.currentTimeMillis();
            if (b > l - 1000L)
            {
                return 20002;
            }
            Intent intent = new Intent("com.igexin.sdk.action.pushmanager");
            intent.putExtra("action", "setTag");
            StringBuffer stringbuffer = new StringBuffer();
            int j = atag.length;
            for (int i = 0; i < j; i++)
            {
                stringbuffer.append(atag[i].getName());
                stringbuffer.append(",");
            }

            stringbuffer.deleteCharAt(stringbuffer.length() - 1);
            intent.putExtra("tags", stringbuffer.toString());
            context.sendBroadcast(intent);
            b = System.currentTimeMillis();
            return 0;
        } else
        {
            return 20006;
        }
    }

    public void stopService(Context context)
    {
        Intent intent = new Intent("com.igexin.sdk.action.pushmanager");
        intent.putExtra("action", "stopService");
        context.sendBroadcast(intent);
    }

    public void turnOffPush(Context context)
    {
        Intent intent = new Intent("com.igexin.sdk.action.pushmanager");
        intent.putExtra("action", "turnOffPush");
        context.sendBroadcast(intent);
    }

    public void turnOnPush(Context context)
    {
        String s = context.getApplicationContext().getPackageName();
        try
        {
            Intent intent = new Intent(context.getApplicationContext(), com/igexin/sdk/PushService);
            intent.putExtra("action", "com.igexin.action.initialize.slave");
            intent.putExtra("op_app", s);
            intent.putExtra("isSlave", true);
            context.getApplicationContext().startService(intent);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return;
        }
    }
}
