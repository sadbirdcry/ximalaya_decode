// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.sdk;

import java.io.Serializable;

public class Tag
    implements Serializable
{

    private String a;

    public Tag()
    {
    }

    private boolean a(String s)
    {
        int i = s.length() - 1;
        boolean flag = false;
        do
        {
label0:
            {
                if (i >= 0)
                {
                    char c = s.charAt(i);
                    if (c >= '\u4E00' && c <= '\u9FA5' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '+' || c == '-' || c == '*' || c == '_' || c == ' ' || c == ':')
                    {
                        flag = true;
                    } else
                    {
                        flag = false;
                    }
                    if (flag)
                    {
                        break label0;
                    }
                }
                return flag;
            }
            i--;
        } while (true);
    }

    private boolean b(String s)
    {
        return a(s);
    }

    public String getName()
    {
        return a;
    }

    public void setName(String s)
    {
        if (b(s))
        {
            a = s;
        }
    }
}
