// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.sdk;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import com.igexin.sdk.a.a;

// Referenced classes of package com.igexin.sdk:
//            IPushCore

public class PushActivity extends Activity
{

    public PushActivity()
    {
    }

    public void onConfigurationChanged(Configuration configuration)
    {
        super.onConfigurationChanged(configuration);
        if (a.a().b() != null)
        {
            a.a().b().onActivityConfigurationChanged(this, configuration);
        }
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        if (a.a().b() != null)
        {
            return a.a().b().onActivityCreateOptionsMenu(this, menu);
        } else
        {
            return true;
        }
    }

    protected void onDestroy()
    {
        super.onDestroy();
        if (a.a().b() != null)
        {
            a.a().b().onActivityDestroy(this);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (a.a().b() != null && a.a().b().onActivityKeyDown(this, i, keyevent))
        {
            return true;
        } else
        {
            return super.onKeyDown(i, keyevent);
        }
    }

    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        setIntent(intent);
        if (a.a().b() != null)
        {
            a.a().b().onActivityNewIntent(this, intent);
        }
    }

    protected void onPause()
    {
        super.onPause();
        if (a.a().b() != null)
        {
            a.a().b().onActivityPause(this);
        }
    }

    protected void onRestart()
    {
        super.onRestart();
        if (a.a().b() != null)
        {
            a.a().b().onActivityRestart(this);
        }
    }

    protected void onResume()
    {
        super.onResume();
        if (a.a().b() != null)
        {
            a.a().b().onActivityResume(this);
        }
    }

    protected void onStart()
    {
        super.onStart();
        if (a.a().b() != null)
        {
            a.a().b().onActivityStart(this, getIntent());
        }
    }

    protected void onStop()
    {
        super.onStop();
        if (a.a().b() != null)
        {
            a.a().b().onActivityStop(this);
        }
    }
}
