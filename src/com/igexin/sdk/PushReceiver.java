// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.igexin.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.igexin.push.core.a.f;
import com.igexin.sdk.a.d;

// Referenced classes of package com.igexin.sdk:
//            PushService

public class PushReceiver extends BroadcastReceiver
{

    private static String a = "PushSdk";

    public PushReceiver()
    {
    }

    public void onReceive(Context context, Intent intent)
    {
        String s = f.a().g("ss");
        if (s == null || !s.equals("1") || (new d(context)).c())
        {
            String s1 = intent.getAction();
            if ("com.igexin.sdk.action.pushmanager".equals(s1))
            {
                Intent intent1 = new Intent(context.getApplicationContext(), com/igexin/sdk/PushService);
                intent1.putExtra("action", "com.igexin.sdk.action.pushmanager");
                intent1.putExtra("bundle", intent.getExtras());
                context.getApplicationContext().startService(intent1);
            }
            if ("com.igexin.sdk.action.refreshls".equals(s1))
            {
                String s2 = intent.getStringExtra("callback_pkgname");
                intent = intent.getStringExtra("callback_classname");
                Intent intent2 = new Intent(context.getApplicationContext(), com/igexin/sdk/PushService);
                intent2.putExtra("action", "com.igexin.sdk.action.refreshls");
                intent2.putExtra("callback_pkgname", s2);
                intent2.putExtra("callback_classname", intent);
                context.getApplicationContext().startService(intent2);
            }
            if ("android.intent.action.BOOT_COMPLETED".equals(s1))
            {
                context.startService(new Intent(context.getApplicationContext(), com/igexin/sdk/PushService));
                return;
            }
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(s1))
            {
                intent = new Intent(context.getApplicationContext(), com/igexin/sdk/PushService);
                intent.putExtra("action", "android.net.conn.CONNECTIVITY_CHANGE");
                context.startService(intent);
                return;
            }
            if ("android.intent.action.USER_PRESENT".equals(s1))
            {
                intent = new Intent(context.getApplicationContext(), com/igexin/sdk/PushService);
                intent.putExtra("action", "android.intent.action.USER_PRESENT");
                context.startService(intent);
                return;
            }
        }
    }

}
