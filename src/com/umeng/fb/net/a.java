// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.umeng.fb.net;

import android.content.Context;
import android.text.TextUtils;
import com.umeng.fb.model.Reply;
import com.umeng.fb.model.Store;
import com.umeng.fb.model.UserInfo;
import com.umeng.fb.util.Log;
import com.umeng.fb.util.b;
import com.umeng.fb.util.c;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a
{

    public static final String a = "http://fb.umeng.com";
    public static final String b = "http://fb.umeng.com/api/v2/user/getuid";
    public static final String c = "http://fb.umeng.com/api/v2/feedback/reply/new";
    public static final String d = "http://fb.umeng.com/api/v2/feedback/reply/full_show";
    public static final String e = "http://fb.umeng.com/api/v2/feedback/new";
    public static final String f = "http://fb.umeng.com/api/v2/user/update";
    public static final String g = "dev_reply";
    private static final String h = com/umeng/fb/net/a.getName();
    private static final int j = 30000;
    private Context i;

    public a(Context context)
    {
        i = context;
        b();
    }

    private static JSONObject a(String s, Map map)
        throws IOException
    {
        if (TextUtils.isEmpty(s))
        {
            return null;
        }
        s = new StringBuilder(s);
        if (s.charAt(s.length() - 1) != '?' && map.size() > 0)
        {
            s.append('?');
        }
        String s1;
        String s2;
        for (Iterator iterator = map.keySet().iterator(); iterator.hasNext(); s.append((new StringBuilder()).append(URLEncoder.encode(s1, "UTF-8")).append("=").append(URLEncoder.encode(s2, "UTF-8")).append("&").toString()))
        {
            s1 = (String)iterator.next();
            s2 = (new StringBuilder()).append("").append(map.get(s1)).toString();
        }

        if ('&' == s.charAt(s.length() - 1))
        {
            s.deleteCharAt(s.length() - 1);
        }
        return b(s.toString());
    }

    private static JSONObject a(JSONObject jsonobject, String s)
        throws IOException
    {
        HttpURLConnection httpurlconnection;
        httpurlconnection = (HttpURLConnection)(new URL(s)).openConnection();
        httpurlconnection.setDoOutput(true);
        httpurlconnection.setRequestMethod("POST");
        httpurlconnection.setRequestProperty("Content-Type", "application/json");
        String s1 = jsonobject.toString();
        BufferedOutputStream bufferedoutputstream = new BufferedOutputStream(httpurlconnection.getOutputStream());
        bufferedoutputstream.write(s1.getBytes());
        bufferedoutputstream.flush();
        if (httpurlconnection.getResponseCode() != 200)
        {
            throw new RuntimeException((new StringBuilder()).append("Failed : HTTP error code : ").append(httpurlconnection.getResponseCode()).toString());
        }
          goto _L1
        jsonobject;
        jsonobject.printStackTrace();
_L4:
        return null;
_L1:
        BufferedReader bufferedreader;
        StringBuilder stringbuilder;
        bufferedreader = new BufferedReader(new InputStreamReader(httpurlconnection.getInputStream()));
        stringbuilder = new StringBuilder();
_L2:
        String s2 = bufferedreader.readLine();
        if (s2 == null)
        {
            break; /* Loop/switch isn't completed */
        }
        stringbuilder.append(s2);
          goto _L2
        jsonobject;
        jsonobject.printStackTrace();
        if (true) goto _L4; else goto _L3
_L3:
        bufferedreader.close();
        httpurlconnection.disconnect();
        Log.c(h, (new StringBuilder()).append("jsonHttpPost: ").append(s).toString());
        Log.c(h, (new StringBuilder()).append("\t request:\n").append(jsonobject.toString()).toString());
        Log.c(h, (new StringBuilder()).append("\t response:\n").append(stringbuilder.toString()).toString());
        jsonobject = new JSONObject(stringbuilder.toString());
        return jsonobject;
    }

    private static JSONObject b(String s)
        throws IOException
    {
        HttpURLConnection httpurlconnection;
        httpurlconnection = (HttpURLConnection)(new URL(s)).openConnection();
        httpurlconnection.setRequestMethod("GET");
        if (httpurlconnection.getResponseCode() != 200)
        {
            throw new RuntimeException((new StringBuilder()).append("Failed : HTTP error code : ").append(httpurlconnection.getResponseCode()).toString());
        }
          goto _L1
        s;
        s.printStackTrace();
_L4:
        return null;
_L1:
        BufferedReader bufferedreader;
        StringBuilder stringbuilder;
        bufferedreader = new BufferedReader(new InputStreamReader(httpurlconnection.getInputStream()));
        stringbuilder = new StringBuilder();
_L2:
        String s1 = bufferedreader.readLine();
        if (s1 == null)
        {
            break; /* Loop/switch isn't completed */
        }
        stringbuilder.append(s1);
          goto _L2
        s;
        s.printStackTrace();
        if (true) goto _L4; else goto _L3
_L3:
        bufferedreader.close();
        httpurlconnection.disconnect();
        Log.c(h, (new StringBuilder()).append("jsonHttpGet: ").append(s).toString());
        Log.c(h, (new StringBuilder()).append("\t ").append(stringbuilder.toString()).toString());
        s = new JSONObject(stringbuilder.toString());
        return s;
    }

    private static void b()
    {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) < 8)
        {
            System.setProperty("http.keepAlive", "false");
        }
    }

    public String a()
    {
        StringBuilder stringbuilder;
        try
        {
            JSONObject jsonobject = com.umeng.fb.util.c.a(i);
            stringbuilder = new StringBuilder("http://fb.umeng.com/api/v2/user/getuid");
            stringbuilder.append("?");
            String s1;
            String s2;
            for (Iterator iterator = jsonobject.keys(); iterator.hasNext(); stringbuilder.append((new StringBuilder()).append(URLEncoder.encode(s1, "UTF-8")).append("=").append(URLEncoder.encode(s2, "UTF-8")).append("&").toString()))
            {
                s1 = (String)iterator.next();
                s2 = jsonobject.get(s1).toString();
            }

        }
        catch (Exception exception)
        {
            return "";
        }
        String s;
        if ('&' == stringbuilder.charAt(stringbuilder.length() - 1))
        {
            stringbuilder.deleteCharAt(stringbuilder.length() - 1);
        }
        s = b(stringbuilder.toString()).getJSONObject("data").getString("uid");
        Log.c(h, (new StringBuilder()).append("FbClient.getUid: ").append(s).toString());
        Store.getInstance(i).setUid(s);
        return s;
    }

    public List a(String s)
        throws IOException, JSONException
    {
        Object obj = new HashMap();
        ((Map) (obj)).put("feedback_id", s);
        ((Map) (obj)).put("appkey", com.umeng.fb.util.b.p(i));
        s = a("http://fb.umeng.com/api/v2/feedback/reply/full_show", ((Map) (obj)));
        Log.c(h, (new StringBuilder()).append("getDevReply resp: ").append(s).toString());
        s = s.getJSONObject("data").getJSONArray("result");
        obj = new ArrayList();
        int k = 0;
        while (k < s.length()) 
        {
            try
            {
                ((List) (obj)).add(Reply.fromJson(s.getJSONObject(k)));
            }
            catch (JSONException jsonexception)
            {
                jsonexception.printStackTrace();
            }
            k++;
        }
        return ((List) (obj));
    }

    public Map a(String s, Reply reply)
    {
        HashMap hashmap = new HashMap();
        try
        {
            JSONObject jsonobject = com.umeng.fb.util.c.a(i);
            jsonobject.put("content", reply.content);
            jsonobject.put("feedback_id", s);
            jsonobject.put("reply_id", reply.reply_id);
            jsonobject.put("device_uuid", Store.getInstance(i).getDeviceUUID());
            jsonobject.put("type", "user_reply");
            s = a(jsonobject, "http://fb.umeng.com/api/v2/feedback/reply/new");
            Log.c(h, (new StringBuilder()).append("sendUserReply response --").append(s.toString()).toString());
            reply = s.getJSONObject("data").getString("feedback_id");
            long l = s.getJSONObject("data").getLong("created_at");
            hashmap.put("feedback_id", reply);
            hashmap.put("created_at", Long.valueOf(l));
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return hashmap;
        }
        return hashmap;
    }

    public void a(JSONObject jsonobject)
    {
        try
        {
            JSONObject jsonobject1 = com.umeng.fb.util.c.a(i);
            jsonobject1.put("uid", Store.getInstance(i).getUid());
            jsonobject1.put("userinfo", jsonobject.toString());
            Log.c(h, "sendUserInfo url -- http://fb.umeng.com/api/v2/user/update");
            Log.c(h, (new StringBuilder()).append("sendUserInfo request-- ").append(jsonobject1).toString());
            jsonobject = a(jsonobject1, "http://fb.umeng.com/api/v2/user/update");
            Log.c(h, (new StringBuilder()).append("sendUserInfo response -- ").append(jsonobject).toString());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (JSONObject jsonobject)
        {
            jsonobject.printStackTrace();
        }
    }

    public Map b(String s, Reply reply)
    {
        HashMap hashmap = new HashMap();
        try
        {
            JSONObject jsonobject = com.umeng.fb.util.c.a(i);
            jsonobject.put("content", reply.content);
            jsonobject.put("uid", Store.getInstance(i).getUid());
            jsonobject.put("device_uuid", Store.getInstance(i).getDeviceUUID());
            jsonobject.put("feedback_id", s);
            if (Store.getInstance(i).getUserInfo().getRemarkJson() != null)
            {
                jsonobject.put("remark", Store.getInstance(i).getUserInfo().getRemarkJson().toString());
            }
            s = a(jsonobject, "http://fb.umeng.com/api/v2/feedback/new");
            Log.c(h, (new StringBuilder()).append("newFeedback request --").append(jsonobject.toString()).toString());
            reply = s.getJSONObject("data").getString("feedback_id");
            long l = s.getJSONObject("data").getLong("created_at");
            hashmap.put("feedback_id", reply);
            hashmap.put("created_at", Long.valueOf(l));
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return hashmap;
        }
        return hashmap;
    }

}
