// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.umeng.fb.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.umeng.fb.model.Conversation;
import com.umeng.fb.model.Reply;
import com.umeng.fb.res.b;
import com.umeng.fb.res.c;
import com.umeng.fb.res.d;
import com.umeng.fb.res.e;
import com.umeng.fb.res.f;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

// Referenced classes of package com.umeng.fb.fragment:
//            FeedbackFragment

class _cls1 extends BaseAdapter
{
    class ViewHolder
    {

        TextView a;
        TextView b;
        View c;
        View d;
        ImageView e;
        final FeedbackFragment.ReplyListAdapter f;

        ViewHolder()
        {
            f = FeedbackFragment.ReplyListAdapter.this;
            super();
        }
    }


    LayoutInflater a;
    Conversation b;
    final FeedbackFragment c;

    private String a(long l)
    {
        Date date = new Date();
        Date date1 = new Date(l);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        boolean flag;
        if (calendar.get(1) == calendar1.get(1))
        {
            flag = true;
        } else
        {
            flag = false;
        }
        l = TimeUnit.MILLISECONDS.toMinutes(date.getTime() - l);
        if (l < 1L)
        {
            return c.getResources().getString(f.f(com.umeng.fb.fragment.FeedbackFragment.f(c)));
        }
        if (l < 30L)
        {
            return String.format(c.getResources().getString(f.g(com.umeng.fb.fragment.FeedbackFragment.f(c))), new Object[] {
                Long.valueOf(l)
            });
        }
        if (flag)
        {
            return (new SimpleDateFormat(c.getResources().getString(f.h(com.umeng.fb.fragment.FeedbackFragment.f(c))), Locale.CHINA)).format(date1);
        } else
        {
            return (new SimpleDateFormat(c.getResources().getString(f.i(com.umeng.fb.fragment.FeedbackFragment.f(c))), Locale.CHINA)).format(date1);
        }
    }

    public int getCount()
    {
        List list = b.getReplyList();
        if (list == null)
        {
            return 0;
        } else
        {
            return list.size();
        }
    }

    public Object getItem(int i)
    {
        return b.getReplyList().get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        boolean flag1 = true;
        Reply reply;
        if (view == null)
        {
            view = a.inflate(e.b(com.umeng.fb.fragment.FeedbackFragment.f(c)), null);
            viewgroup = new ViewHolder();
            viewgroup.b = (TextView)view.findViewById(d.b(com.umeng.fb.fragment.FeedbackFragment.f(c)));
            viewgroup.a = (TextView)view.findViewById(d.e(com.umeng.fb.fragment.FeedbackFragment.f(c)));
            viewgroup.d = view.findViewById(d.i(com.umeng.fb.fragment.FeedbackFragment.f(c)));
            viewgroup.c = view.findViewById(d.o(com.umeng.fb.fragment.FeedbackFragment.f(c)));
            viewgroup.e = (ImageView)view.findViewById(d.p(com.umeng.fb.fragment.FeedbackFragment.f(c)));
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        reply = (Reply)b.getReplyList().get(i);
        if ("dev_reply".equals(reply.type))
        {
            ((ViewHolder) (viewgroup)).c.setBackgroundColor(c.getResources().getColor(com.umeng.fb.res.b.a(com.umeng.fb.fragment.FeedbackFragment.f(c))));
            ((ViewHolder) (viewgroup)).a.setText(a(reply.created_at));
        } else
        {
            ((ViewHolder) (viewgroup)).c.setBackgroundColor(c.getResources().getColor(com.umeng.fb.res.b.c(com.umeng.fb.fragment.FeedbackFragment.f(c))));
            if ("not_sent".equals(reply.status))
            {
                ((ViewHolder) (viewgroup)).a.setText(f.d(com.umeng.fb.fragment.FeedbackFragment.f(c)));
                class _cls2
                    implements android.view.View.OnClickListener
                {

                    final FeedbackFragment.ReplyListAdapter a;

                    public void onClick(View view1)
                    {
                        FeedbackFragment.h(a.c);
                    }

            _cls2()
            {
                a = FeedbackFragment.ReplyListAdapter.this;
                super();
            }
                }

                ((ViewHolder) (viewgroup)).e.setOnClickListener(new _cls2());
                ((ViewHolder) (viewgroup)).e.setImageResource(com.umeng.fb.res.c.a(com.umeng.fb.fragment.FeedbackFragment.f(c)));
                ((ViewHolder) (viewgroup)).e.setAnimation(null);
                ((ViewHolder) (viewgroup)).e.setVisibility(0);
            } else
            if ("sending".equals(reply.status))
            {
                ((ViewHolder) (viewgroup)).a.setText(f.e(com.umeng.fb.fragment.FeedbackFragment.f(c)));
                ((ViewHolder) (viewgroup)).e.setImageResource(com.umeng.fb.res.c.a(com.umeng.fb.fragment.FeedbackFragment.f(c)));
                ((ViewHolder) (viewgroup)).e.setVisibility(0);
                RotateAnimation rotateanimation = new RotateAnimation(0.0F, -360F, 1, 0.5F, 1, 0.5F);
                rotateanimation.setInterpolator(new LinearInterpolator());
                rotateanimation.setRepeatCount(-1);
                rotateanimation.setDuration(700L);
                ((ViewHolder) (viewgroup)).e.startAnimation(rotateanimation);
                ((ViewHolder) (viewgroup)).e.setOnClickListener(null);
            } else
            {
                ((ViewHolder) (viewgroup)).a.setText(a(reply.created_at));
                ((ViewHolder) (viewgroup)).e.setAnimation(null);
                ((ViewHolder) (viewgroup)).e.setVisibility(8);
                ((ViewHolder) (viewgroup)).e.setOnClickListener(null);
            }
        }
        ((ViewHolder) (viewgroup)).b.setText(reply.content);
        ((ViewHolder) (viewgroup)).d.setVisibility(0);
        if (i + 1 < getCount())
        {
            Reply reply1 = (Reply)b.getReplyList().get(i + 1);
            boolean flag;
            boolean flag2;
            if ("new_feedback".equals(reply.type) && "user_reply".equals(reply1.type))
            {
                flag = true;
            } else
            {
                flag = false;
            }
            flag2 = reply1.type.equals(reply.type);
            if (i + 1 == getCount())
            {
                i = ((flag1) ? 1 : 0);
            } else
            {
                i = 0;
            }
            if ((flag2 | flag | i) != 0)
            {
                ((ViewHolder) (viewgroup)).d.setVisibility(8);
            }
        }
        return view;
    }

    public _cls1.a(FeedbackFragment feedbackfragment, Context context, Conversation conversation)
    {
        c = feedbackfragment;
        super();
        a = LayoutInflater.from(com.umeng.fb.fragment.FeedbackFragment.f(feedbackfragment));
        b = conversation;
        class _cls1
            implements com.umeng.fb.model.Conversation.OnChangeListener
        {

            final FeedbackFragment a;
            final FeedbackFragment.ReplyListAdapter b;

            public void onChange()
            {
                b.notifyDataSetChanged();
            }

            _cls1(FeedbackFragment feedbackfragment)
            {
                b = FeedbackFragment.ReplyListAdapter.this;
                a = feedbackfragment;
                super();
            }
        }

        b.setOnChangeListener(new _cls1(feedbackfragment));
    }
}
