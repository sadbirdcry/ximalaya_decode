// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.umeng.fb.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import com.umeng.fb.FeedbackAgent;
import com.umeng.fb.model.Conversation;
import com.umeng.fb.model.FbSwitch;
import com.umeng.fb.model.Reply;
import com.umeng.fb.model.UserInfo;
import com.umeng.fb.push.FeedbackPushImpl;
import com.umeng.fb.res.a;
import com.umeng.fb.res.b;
import com.umeng.fb.res.c;
import com.umeng.fb.res.d;
import com.umeng.fb.res.e;
import com.umeng.fb.res.f;
import com.umeng.fb.util.Log;
import com.umeng.fb.widget.InterceptTouchSwipeRefreshLayout;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class FeedbackFragment extends Fragment
    implements android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener
{
    class FeedbackPushCallbacks
        implements com.umeng.fb.push.FeedbackPushImpl.IFeedbackPushCallbacks
    {

        final FeedbackFragment a;

        public void onAddPushDevReply()
        {
            FeedbackFragment.j(a).post(FeedbackFragment.k(a));
        }

        FeedbackPushCallbacks()
        {
            a = FeedbackFragment.this;
            super();
        }
    }

    class ReplyListAdapter extends BaseAdapter
    {

        LayoutInflater a;
        Conversation b;
        final FeedbackFragment c;

        private String a(long l1)
        {
            Date date = new Date();
            Date date1 = new Date(l1);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(date1);
            boolean flag;
            if (calendar.get(1) == calendar1.get(1))
            {
                flag = true;
            } else
            {
                flag = false;
            }
            l1 = TimeUnit.MILLISECONDS.toMinutes(date.getTime() - l1);
            if (l1 < 1L)
            {
                return c.getResources().getString(com.umeng.fb.res.f.f(com.umeng.fb.fragment.FeedbackFragment.f(c)));
            }
            if (l1 < 30L)
            {
                return String.format(c.getResources().getString(com.umeng.fb.res.f.g(com.umeng.fb.fragment.FeedbackFragment.f(c))), new Object[] {
                    Long.valueOf(l1)
                });
            }
            if (flag)
            {
                return (new SimpleDateFormat(c.getResources().getString(com.umeng.fb.res.f.h(com.umeng.fb.fragment.FeedbackFragment.f(c))), Locale.CHINA)).format(date1);
            } else
            {
                return (new SimpleDateFormat(c.getResources().getString(com.umeng.fb.res.f.i(com.umeng.fb.fragment.FeedbackFragment.f(c))), Locale.CHINA)).format(date1);
            }
        }

        public int getCount()
        {
            List list = b.getReplyList();
            if (list == null)
            {
                return 0;
            } else
            {
                return list.size();
            }
        }

        public Object getItem(int i1)
        {
            return b.getReplyList().get(i1);
        }

        public long getItemId(int i1)
        {
            return (long)i1;
        }

        public View getView(int i1, View view, ViewGroup viewgroup)
        {
            boolean flag1 = true;
            Reply reply;
            if (view == null)
            {
                view = a.inflate(com.umeng.fb.res.e.b(com.umeng.fb.fragment.FeedbackFragment.f(c)), null);
                viewgroup = new ViewHolder(this);
                viewgroup.b = (TextView)view.findViewById(com.umeng.fb.res.d.b(com.umeng.fb.fragment.FeedbackFragment.f(c)));
                viewgroup.a = (TextView)view.findViewById(com.umeng.fb.res.d.e(com.umeng.fb.fragment.FeedbackFragment.f(c)));
                viewgroup.d = view.findViewById(com.umeng.fb.res.d.i(com.umeng.fb.fragment.FeedbackFragment.f(c)));
                viewgroup.c = view.findViewById(com.umeng.fb.res.d.o(com.umeng.fb.fragment.FeedbackFragment.f(c)));
                viewgroup.e = (ImageView)view.findViewById(com.umeng.fb.res.d.p(com.umeng.fb.fragment.FeedbackFragment.f(c)));
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (ViewHolder)view.getTag();
            }
            reply = (Reply)b.getReplyList().get(i1);
            if ("dev_reply".equals(reply.type))
            {
                ((ViewHolder) (viewgroup)).c.setBackgroundColor(c.getResources().getColor(com.umeng.fb.res.b.a(com.umeng.fb.fragment.FeedbackFragment.f(c))));
                ((ViewHolder) (viewgroup)).a.setText(a(reply.created_at));
            } else
            {
                ((ViewHolder) (viewgroup)).c.setBackgroundColor(c.getResources().getColor(com.umeng.fb.res.b.c(com.umeng.fb.fragment.FeedbackFragment.f(c))));
                if ("not_sent".equals(reply.status))
                {
                    ((ViewHolder) (viewgroup)).a.setText(com.umeng.fb.res.f.d(com.umeng.fb.fragment.FeedbackFragment.f(c)));
                    class _cls2
                        implements android.view.View.OnClickListener
                    {

                        final ReplyListAdapter a;

                        public void onClick(View view1)
                        {
                            FeedbackFragment.h(a.c);
                        }

                _cls2()
                {
                    a = ReplyListAdapter.this;
                    super();
                }
                    }

                    ((ViewHolder) (viewgroup)).e.setOnClickListener(new _cls2());
                    ((ViewHolder) (viewgroup)).e.setImageResource(com.umeng.fb.res.c.a(com.umeng.fb.fragment.FeedbackFragment.f(c)));
                    ((ViewHolder) (viewgroup)).e.setAnimation(null);
                    ((ViewHolder) (viewgroup)).e.setVisibility(0);
                } else
                if ("sending".equals(reply.status))
                {
                    ((ViewHolder) (viewgroup)).a.setText(com.umeng.fb.res.f.e(com.umeng.fb.fragment.FeedbackFragment.f(c)));
                    ((ViewHolder) (viewgroup)).e.setImageResource(com.umeng.fb.res.c.a(com.umeng.fb.fragment.FeedbackFragment.f(c)));
                    ((ViewHolder) (viewgroup)).e.setVisibility(0);
                    RotateAnimation rotateanimation = new RotateAnimation(0.0F, -360F, 1, 0.5F, 1, 0.5F);
                    rotateanimation.setInterpolator(new LinearInterpolator());
                    rotateanimation.setRepeatCount(-1);
                    rotateanimation.setDuration(700L);
                    ((ViewHolder) (viewgroup)).e.startAnimation(rotateanimation);
                    ((ViewHolder) (viewgroup)).e.setOnClickListener(null);
                } else
                {
                    ((ViewHolder) (viewgroup)).a.setText(a(reply.created_at));
                    ((ViewHolder) (viewgroup)).e.setAnimation(null);
                    ((ViewHolder) (viewgroup)).e.setVisibility(8);
                    ((ViewHolder) (viewgroup)).e.setOnClickListener(null);
                }
            }
            ((ViewHolder) (viewgroup)).b.setText(reply.content);
            ((ViewHolder) (viewgroup)).d.setVisibility(0);
            if (i1 + 1 < getCount())
            {
                Reply reply1 = (Reply)b.getReplyList().get(i1 + 1);
                boolean flag;
                boolean flag2;
                if ("new_feedback".equals(reply.type) && "user_reply".equals(reply1.type))
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                flag2 = reply1.type.equals(reply.type);
                if (i1 + 1 == getCount())
                {
                    i1 = ((flag1) ? 1 : 0);
                } else
                {
                    i1 = 0;
                }
                if ((flag2 | flag | i1) != 0)
                {
                    ((ViewHolder) (viewgroup)).d.setVisibility(8);
                }
            }
            return view;
        }

        public ReplyListAdapter(Context context, Conversation conversation)
        {
            c = FeedbackFragment.this;
            super();
            a = LayoutInflater.from(com.umeng.fb.fragment.FeedbackFragment.f(FeedbackFragment.this));
            b = conversation;
            class _cls1
                implements com.umeng.fb.model.Conversation.OnChangeListener
            {

                final FeedbackFragment a;
                final ReplyListAdapter b;

                public void onChange()
                {
                    b.notifyDataSetChanged();
                }

                _cls1()
                {
                    b = ReplyListAdapter.this;
                    a = FeedbackFragment.this;
                    super();
                }
            }

            b.setOnChangeListener(new _cls1());
        }
    }

    class ReplyListAdapter.ViewHolder
    {

        TextView a;
        TextView b;
        View c;
        View d;
        ImageView e;
        final ReplyListAdapter f;

        ReplyListAdapter.ViewHolder(ReplyListAdapter replylistadapter)
        {
            f = replylistadapter;
            super();
        }
    }


    public static final String BUNDLE_KEY_CONVERSATION_ID = "conversation_id";
    private static final String a = com/umeng/fb/fragment/FeedbackFragment.getName();
    private static final String p = ": ";
    private static final int q = 0;
    private static final int r = 1;
    private Button b;
    private EditText c;
    private TextView d;
    private TextView e;
    private TextView f;
    private InterceptTouchSwipeRefreshLayout g;
    private ListView h;
    private Spinner i;
    private ReplyListAdapter j;
    private FeedbackAgent k;
    private Conversation l;
    private FeedbackPushImpl m;
    private String n[];
    private String o[];
    private int s;
    private Context t;
    private Handler u;
    private List v;
    private final Runnable w = new _cls1();
    private final Runnable x = new _cls9();

    public FeedbackFragment()
    {
        s = 0;
        u = new Handler();
    }

    static InterceptTouchSwipeRefreshLayout a(FeedbackFragment feedbackfragment)
    {
        return feedbackfragment.g;
    }

    static String a(FeedbackFragment feedbackfragment, String s1)
    {
        return feedbackfragment.a(s1);
    }

    private String a(String s1)
    {
        UserInfo userinfo = k.getUserInfo();
        if (userinfo != null)
        {
            Map map = userinfo.getContact();
            if (map != null)
            {
                String s2 = "";
                if (s1 == null)
                {
                    Iterator iterator = map.keySet().iterator();
                    s1 = s2;
                    do
                    {
                        if (!iterator.hasNext())
                        {
                            break;
                        }
                        s2 = (String)iterator.next();
                        if (map.get(s2) != null && d(s2) != null)
                        {
                            s1 = (new StringBuilder()).append(s1).append(d(s2)).append(": ").append((String)map.get(s2)).append("\t").toString();
                        }
                    } while (true);
                    s2 = s1;
                    if ("".equals(s1))
                    {
                        s2 = null;
                    }
                    return s2;
                } else
                {
                    return (String)map.get(s1);
                }
            } else
            {
                return null;
            }
        } else
        {
            return null;
        }
    }

    private void a(int i1, View view)
    {
        s = i1;
        if (i1 == 1)
        {
            View view1 = View.inflate(getActivity(), com.umeng.fb.res.e.f(t), null);
            i = (Spinner)view1.findViewById(com.umeng.fb.res.d.l(t));
            ArrayAdapter arrayadapter = ArrayAdapter.createFromResource(getActivity(), com.umeng.fb.res.a.a(t), com.umeng.fb.res.e.g(t));
            arrayadapter.setDropDownViewResource(0x1090009);
            i.setAdapter(arrayadapter);
            ((ViewGroup)view).removeAllViews();
            ((ViewGroup)view).addView(view1);
        } else
        {
            View view2 = View.inflate(getActivity(), com.umeng.fb.res.e.h(t), null);
            ((ViewGroup)view).removeAllViews();
            ((ViewGroup)view).addView(view2);
        }
        b = (Button)view.findViewById(com.umeng.fb.res.d.m(t));
        c = (EditText)view.findViewById(com.umeng.fb.res.d.n(t));
        if (i1 == 1 && i != null)
        {
            if (v == null)
            {
                v = new ArrayList();
            }
            c.requestFocus();
            i.setOnItemSelectedListener(new _cls4());
            i.setSelection(e());
        } else
        {
            c.setInputType(0x20001);
        }
        c.addTextChangedListener(new _cls5());
        b.setOnClickListener(new _cls6(i1, view));
    }

    static void a(FeedbackFragment feedbackfragment, int i1, View view)
    {
        feedbackfragment.a(i1, view);
    }

    static int b(FeedbackFragment feedbackfragment)
    {
        return feedbackfragment.s;
    }

    private void b()
    {
        _cls7 _lcls7 = new _cls7();
        l.sync(_lcls7);
    }

    static void b(FeedbackFragment feedbackfragment, String s1)
    {
        feedbackfragment.c(s1);
    }

    private void b(String s1)
    {
        if (s1 != null)
        {
            d.setText(s1);
            e.setText(getResources().getString(com.umeng.fb.res.f.j(t)));
            return;
        } else
        {
            d.setText(getResources().getString(com.umeng.fb.res.f.k(t)));
            e.setText(getResources().getString(com.umeng.fb.res.f.l(t)));
            return;
        }
    }

    static EditText c(FeedbackFragment feedbackfragment)
    {
        return feedbackfragment.c;
    }

    private void c()
    {
        for (int i1 = 0; i1 < m.devReplyList.size(); i1++)
        {
            Reply reply = (Reply)m.devReplyList.get(i1);
            Iterator iterator = l.getReplyList().iterator();
            boolean flag = false;
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                Reply reply1 = (Reply)iterator.next();
                if (reply.created_at == reply1.created_at)
                {
                    flag = true;
                }
            } while (true);
            if (!flag && reply.feedback_id != null && reply.feedback_id.equals(l.getId()))
            {
                l.addReply(reply);
            }
            Log.c(a, reply.content);
        }

        m.devReplyList.clear();
    }

    private void c(String s1)
    {
        String s2 = n[i.getSelectedItemPosition()];
        if (s1.equals(a(s2)))
        {
            return;
        }
        Object obj = k.getUserInfo();
        UserInfo userinfo = ((UserInfo) (obj));
        if (obj == null)
        {
            userinfo = new UserInfo();
        }
        Map map = userinfo.getContact();
        obj = map;
        if (map == null)
        {
            obj = new HashMap();
        }
        ((Map) (obj)).put(s2, s1);
        userinfo.setContact(((Map) (obj)));
        k.setUserInfo(userinfo);
        b(a(((String) (null))));
        (new Thread(new _cls8())).start();
    }

    private String d(String s1)
    {
        for (int i1 = 0; i1 < n.length; i1++)
        {
            if (n[i1].endsWith(s1))
            {
                return o[i1];
            }
        }

        return null;
    }

    private void d()
    {
        ArrayList arraylist = new ArrayList();
        Iterator iterator = m.devReplyList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            Reply reply = (Reply)iterator.next();
            if (reply.feedback_id != null && !reply.feedback_id.equals(l.getId()))
            {
                arraylist.add(reply);
            }
        } while (true);
        addPushDevReply();
        m.devReplyList = arraylist;
    }

    static String[] d(FeedbackFragment feedbackfragment)
    {
        return feedbackfragment.n;
    }

    private int e()
    {
        for (int i1 = 0; i1 < n.length; i1++)
        {
            if (a(n[i1]) != null)
            {
                return i1;
            }
        }

        return 0;
    }

    static Button e(FeedbackFragment feedbackfragment)
    {
        return feedbackfragment.b;
    }

    static Context f(FeedbackFragment feedbackfragment)
    {
        return feedbackfragment.t;
    }

    static Conversation g(FeedbackFragment feedbackfragment)
    {
        return feedbackfragment.l;
    }

    static void h(FeedbackFragment feedbackfragment)
    {
        feedbackfragment.b();
    }

    static Runnable i(FeedbackFragment feedbackfragment)
    {
        return feedbackfragment.w;
    }

    static Handler j(FeedbackFragment feedbackfragment)
    {
        return feedbackfragment.u;
    }

    static Runnable k(FeedbackFragment feedbackfragment)
    {
        return feedbackfragment.x;
    }

    static void l(FeedbackFragment feedbackfragment)
    {
        feedbackfragment.d();
    }

    public static FeedbackFragment newInstance(String s1)
    {
        Log.c(a, String.format("newInstance(id=%s)", new Object[] {
            s1
        }));
        FeedbackFragment feedbackfragment = new FeedbackFragment();
        Bundle bundle = new Bundle();
        bundle.putString("conversation_id", s1);
        feedbackfragment.setArguments(bundle);
        return feedbackfragment;
    }

    void a()
    {
        if (j.getCount() > 0)
        {
            h.smoothScrollToPosition(j.getCount());
        }
    }

    public void addPushDevReply()
    {
        if (m.devReplyList.size() > 0 && !((Reply)m.devReplyList.get(m.devReplyList.size() - 1)).feedback_id.equals(l.getId()))
        {
            l = k.getConversationById(((Reply)m.devReplyList.get(m.devReplyList.size() - 1)).feedback_id);
            j = null;
            j = new ReplyListAdapter(getActivity(), l);
            h.setAdapter(j);
            m.conversation_id = l.getId();
        }
        c();
        j.notifyDataSetChanged();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        t = getActivity();
        Log.c(a, String.format("onCreateView(savedInstanceState=%s)", new Object[] {
            bundle
        }));
        n = getResources().getStringArray(com.umeng.fb.res.a.b(t));
        o = getResources().getStringArray(com.umeng.fb.res.a.a(t));
        viewgroup = layoutinflater.inflate(com.umeng.fb.res.e.c(t), null, false);
        k = new FeedbackAgent(getActivity());
        m = (FeedbackPushImpl)FeedbackPushImpl.getInstance(getActivity());
        m.setFBPushCallbacks(new FeedbackPushCallbacks());
        bundle = getArguments().getString("conversation_id");
        m.conversation_id = bundle;
        if (TextUtils.isEmpty(bundle))
        {
            return viewgroup;
        }
        l = k.getConversationById(bundle);
        c();
        if (l == null)
        {
            return viewgroup;
        }
        h = (ListView)viewgroup.findViewById(com.umeng.fb.res.d.a(t));
        bundle = viewgroup.findViewById(com.umeng.fb.res.d.g(t));
        View view = layoutinflater.inflate(com.umeng.fb.res.e.d(t), null, false);
        View view1 = view.findViewById(com.umeng.fb.res.d.h(t));
        e = (TextView)view1.findViewById(com.umeng.fb.res.d.b(t));
        d = (TextView)view1.findViewById(com.umeng.fb.res.d.e(t));
        b(a(((String) (null))));
        e.setTextColor(getResources().getColor(com.umeng.fb.res.b.a(t)));
        view.findViewById(com.umeng.fb.res.d.i(t)).setBackgroundColor(getResources().getColor(com.umeng.fb.res.b.a(t)));
        view1.setOnClickListener(new _cls2(bundle));
        h.setHeaderDividersEnabled(true);
        h.addHeaderView(view);
        if (FbSwitch.getInstance(t).getWelcomeInfoSwitch())
        {
            layoutinflater = layoutinflater.inflate(com.umeng.fb.res.e.e(t), null, false);
            f = (TextView)layoutinflater.findViewById(com.umeng.fb.res.d.j(t));
            if (FbSwitch.getInstance(t).getWelcomeInfo() != null)
            {
                f.setText(FbSwitch.getInstance(t).getWelcomeInfo());
            }
            h.addHeaderView(layoutinflater);
        }
        j = new ReplyListAdapter(getActivity(), l);
        h.setAdapter(j);
        g = (InterceptTouchSwipeRefreshLayout)viewgroup.findViewById(com.umeng.fb.res.d.k(t));
        g.setOnRefreshListener(this);
        g.setColorSchemeResources(new int[] {
            com.umeng.fb.res.b.a(t), com.umeng.fb.res.b.b(t), com.umeng.fb.res.b.a(t), com.umeng.fb.res.b.b(t)
        });
        g.setInterceptTouch(new _cls3(bundle));
        a(0, bundle);
        b();
        return viewgroup;
    }

    public void onPause()
    {
        super.onPause();
        m.fbFragmentTag = false;
    }

    public void onRefresh()
    {
        b();
    }

    public void onResume()
    {
        super.onResume();
        m.fbFragmentTag = true;
    }


    private class _cls1
        implements Runnable
    {

        final FeedbackFragment a;

        public void run()
        {
            com.umeng.fb.fragment.FeedbackFragment.a(a).setRefreshing(false);
        }

        _cls1()
        {
            a = FeedbackFragment.this;
            super();
        }
    }


    private class _cls9
        implements Runnable
    {

        final FeedbackFragment a;

        public void run()
        {
            FeedbackFragment.l(a);
        }

        _cls9()
        {
            a = FeedbackFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AdapterView.OnItemSelectedListener
    {

        final FeedbackFragment a;

        public void onItemSelected(AdapterView adapterview, View view, int i1, long l1)
        {
            i1;
            JVM INSTR tableswitch 0 3: default 32
        //                       0 70
        //                       1 85
        //                       2 99
        //                       3 113;
               goto _L1 _L2 _L3 _L4 _L5
_L1:
            com.umeng.fb.fragment.FeedbackFragment.c(a).setText(com.umeng.fb.fragment.FeedbackFragment.a(a, com.umeng.fb.fragment.FeedbackFragment.d(a)[i1]));
            com.umeng.fb.fragment.FeedbackFragment.c(a).requestFocus();
            return;
_L2:
            com.umeng.fb.fragment.FeedbackFragment.c(a).setInputType(33);
            continue; /* Loop/switch isn't completed */
_L3:
            com.umeng.fb.fragment.FeedbackFragment.c(a).setInputType(2);
            continue; /* Loop/switch isn't completed */
_L4:
            com.umeng.fb.fragment.FeedbackFragment.c(a).setInputType(3);
            continue; /* Loop/switch isn't completed */
_L5:
            com.umeng.fb.fragment.FeedbackFragment.c(a).setInputType(0x20001);
            if (true) goto _L1; else goto _L6
_L6:
        }

        public void onNothingSelected(AdapterView adapterview)
        {
            com.umeng.fb.fragment.FeedbackFragment.c(a).setInputType(0x20001);
        }

        _cls4()
        {
            a = FeedbackFragment.this;
            super();
        }
    }


    private class _cls5
        implements TextWatcher
    {

        final FeedbackFragment a;

        public void afterTextChanged(Editable editable)
        {
            if (!TextUtils.isEmpty(com.umeng.fb.fragment.FeedbackFragment.c(a).getText().toString()))
            {
                com.umeng.fb.fragment.FeedbackFragment.e(a).setEnabled(true);
                com.umeng.fb.fragment.FeedbackFragment.e(a).setTextColor(a.getResources().getColor(0x106000c));
                return;
            } else
            {
                com.umeng.fb.fragment.FeedbackFragment.e(a).setEnabled(false);
                com.umeng.fb.fragment.FeedbackFragment.e(a).setTextColor(a.getResources().getColor(com.umeng.fb.res.b.c(com.umeng.fb.fragment.FeedbackFragment.f(a))));
                return;
            }
        }

        public void beforeTextChanged(CharSequence charsequence, int i1, int j1, int k1)
        {
        }

        public void onTextChanged(CharSequence charsequence, int i1, int j1, int k1)
        {
        }

        _cls5()
        {
            a = FeedbackFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.view.View.OnClickListener
    {

        final int a;
        final View b;
        final FeedbackFragment c;

        public void onClick(View view)
        {
            view = com.umeng.fb.fragment.FeedbackFragment.c(c).getEditableText().toString().trim();
            if (TextUtils.isEmpty(view))
            {
                return;
            }
            com.umeng.fb.fragment.FeedbackFragment.c(c).getEditableText().clear();
            if (a == 1)
            {
                com.umeng.fb.fragment.FeedbackFragment.b(c, view);
                com.umeng.fb.fragment.FeedbackFragment.a(c, 0, b);
                return;
            } else
            {
                FeedbackFragment.g(c).addUserReply(view);
                c.a();
                FeedbackFragment.h(c);
                return;
            }
        }

        _cls6(int i1, View view)
        {
            c = FeedbackFragment.this;
            a = i1;
            b = view;
            super();
        }
    }


    private class _cls7
        implements SyncListener
    {

        final FeedbackFragment a;

        public void onReceiveDevReply(List list)
        {
            FeedbackFragment.j(a).post(FeedbackFragment.i(a));
        }

        public void onSendUserReply(List list)
        {
        }

        _cls7()
        {
            a = FeedbackFragment.this;
            super();
        }
    }


    private class _cls8
        implements Runnable
    {

        final FeedbackFragment a;

        public void run()
        {
            (new com.umeng.fb.net.a(a.getActivity())).a(Store.getInstance(a.getActivity()).getUserInfo().toJson());
        }

        _cls8()
        {
            a = FeedbackFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final View a;
        final FeedbackFragment b;

        public void onClick(View view)
        {
            if (com.umeng.fb.fragment.FeedbackFragment.b(b) != 1)
            {
                com.umeng.fb.fragment.FeedbackFragment.a(b, 1, a);
            }
        }

        _cls2(View view)
        {
            b = FeedbackFragment.this;
            a = view;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnTouchListener
    {

        final View a;
        final FeedbackFragment b;

        public boolean onTouch(View view, MotionEvent motionevent)
        {
            if (com.umeng.fb.fragment.FeedbackFragment.b(b) != 0 && motionevent.getAction() == 1)
            {
                com.umeng.fb.fragment.FeedbackFragment.a(b, 0, a);
            }
            view.performClick();
            return false;
        }

        _cls3(View view)
        {
            b = FeedbackFragment.this;
            a = view;
            super();
        }
    }

}
