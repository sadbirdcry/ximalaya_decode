// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.umeng.fb.fragment;

import android.content.res.Resources;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import com.umeng.fb.res.b;

// Referenced classes of package com.umeng.fb.fragment:
//            FeedbackFragment

class a
    implements TextWatcher
{

    final FeedbackFragment a;

    public void afterTextChanged(Editable editable)
    {
        if (!TextUtils.isEmpty(FeedbackFragment.c(a).getText().toString()))
        {
            FeedbackFragment.e(a).setEnabled(true);
            FeedbackFragment.e(a).setTextColor(a.getResources().getColor(0x106000c));
            return;
        } else
        {
            FeedbackFragment.e(a).setEnabled(false);
            FeedbackFragment.e(a).setTextColor(a.getResources().getColor(b.c(FeedbackFragment.f(a))));
            return;
        }
    }

    public void beforeTextChanged(CharSequence charsequence, int i, int j, int k)
    {
    }

    public void onTextChanged(CharSequence charsequence, int i, int j, int k)
    {
    }

    (FeedbackFragment feedbackfragment)
    {
        a = feedbackfragment;
        super();
    }
}
