// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.umeng.fb;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.SystemClock;
import android.text.TextUtils;
import com.umeng.fb.model.Conversation;
import com.umeng.fb.model.FbSwitch;
import com.umeng.fb.model.Reply;
import com.umeng.fb.model.Store;
import com.umeng.fb.model.UserInfo;
import com.umeng.fb.push.FeedbackPushImpl;
import com.umeng.fb.push.IFeedbackPush;
import com.umeng.fb.res.f;
import com.umeng.fb.util.Log;
import java.util.List;
import java.util.Locale;

// Referenced classes of package com.umeng.fb:
//            ConversationActivity

public class FeedbackAgent
{

    private static final String a = com/umeng/fb/FeedbackAgent.getName();
    private static boolean d = false;
    private Context b;
    private Store c;

    public FeedbackAgent(Context context)
    {
        b = context;
        c = Store.getInstance(b);
        a();
    }

    static Context a(FeedbackAgent feedbackagent)
    {
        return feedbackagent.b;
    }

    private void a()
    {
        if (!c.isMigrated())
        {
            c.migrateData();
        }
        if (TextUtils.isEmpty(c.getUid()))
        {
            (new _cls1()).start();
        }
    }

    public void closeFeedbackPush()
    {
        FeedbackPushImpl.getInstance(b).disable();
    }

    public List getAllConversationIds()
    {
        return c.getAllConversationIds();
    }

    public Conversation getConversationById(String s)
    {
        return c.getConversationById(s);
    }

    public Conversation getDefaultConversation()
    {
        List list = getAllConversationIds();
        if (list == null || list.size() < 1)
        {
            Log.c(a, "getDefaultConversation: No conversation saved locally. Create a new one.");
            return Conversation.newInstance(b);
        } else
        {
            Log.c(a, (new StringBuilder()).append("getDefaultConversation: There are ").append(list.size()).append(" saved locally, use the first one by default.").toString());
            return getConversationById((String)list.get(0));
        }
    }

    public UserInfo getUserInfo()
    {
        return c.getUserInfo();
    }

    public long getUserInfoLastUpdateAt()
    {
        return c.getUserInfoLastUpdateAt();
    }

    public void openFeedbackPush()
    {
        FeedbackPushImpl.getInstance(b).enable();
    }

    public void removeWelcomeInfo()
    {
        FbSwitch.getInstance(b).setWelcomeInfoSwitch(false);
    }

    public void setDebug(boolean flag)
    {
        Log.LOG = flag;
    }

    public void setUserInfo(UserInfo userinfo)
    {
        c.saveUserInfo(userinfo);
    }

    public void setWelcomeInfo()
    {
        FbSwitch.getInstance(b).setWelcomeInfoSwitch(true);
    }

    public void setWelcomeInfo(String s)
    {
        FbSwitch.getInstance(b).setWelcomeInfoSwitch(true);
        if (s != null)
        {
            FbSwitch.getInstance(b).setWelcomeInfo(s);
        }
    }

    public void showReplyNotification(List list)
    {
        NotificationManager notificationmanager;
        String s2;
        Object obj;
        int i;
        if (list.size() == 1)
        {
            String s = b.getResources().getString(f.b(b));
            list = String.format(Locale.US, s, new Object[] {
                ((Reply)list.get(0)).content
            });
        } else
        {
            String s1 = b.getResources().getString(f.c(b));
            list = String.format(Locale.US, s1, new Object[] {
                Integer.valueOf(list.size())
            });
        }
        notificationmanager = (NotificationManager)b.getSystemService("notification");
        s2 = b.getString(f.a(b));
        obj = new Intent(b, com/umeng/fb/ConversationActivity);
        ((Intent) (obj)).setFlags(0x20000);
        i = (int)SystemClock.uptimeMillis();
        obj = PendingIntent.getActivity(b, i, ((Intent) (obj)), 0x8000000);
        try
        {
            int j = b.getPackageManager().getPackageInfo(b.getPackageName(), 0).applicationInfo.icon;
            notificationmanager.notify(0, (new android.support.v4.app.NotificationCompat.Builder(b)).setSmallIcon(j).setContentTitle(s2).setTicker(s2).setContentText(list).setAutoCancel(true).setContentIntent(((PendingIntent) (obj))).build());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (List list)
        {
            list.printStackTrace();
        }
    }

    public void startFeedbackActivity()
    {
        try
        {
            Intent intent = new Intent();
            intent.setClass(b, com/umeng/fb/ConversationActivity);
            b.startActivity(intent);
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    public void sync()
    {
        _cls2 _lcls2 = new _cls2();
        getDefaultConversation().sync(_lcls2);
    }

    public void updateUserInfo()
    {
        (new Thread(new _cls3())).start();
    }


    private class _cls1 extends Thread
    {

        final FeedbackAgent a;

        public void run()
        {
            (new a(com.umeng.fb.FeedbackAgent.a(a))).a();
        }

        _cls1()
        {
            a = FeedbackAgent.this;
            super();
        }
    }


    private class _cls2
        implements SyncListener
    {

        final FeedbackAgent a;

        public void onReceiveDevReply(List list)
        {
            if (list == null || list.size() < 1)
            {
                return;
            } else
            {
                a.showReplyNotification(list);
                return;
            }
        }

        public void onSendUserReply(List list)
        {
        }

        _cls2()
        {
            a = FeedbackAgent.this;
            super();
        }
    }


    private class _cls3
        implements Runnable
    {

        final FeedbackAgent a;

        public void run()
        {
            (new a(com.umeng.fb.FeedbackAgent.a(a))).a(Store.getInstance(com.umeng.fb.FeedbackAgent.a(a)).getUserInfo().toJson());
        }

        _cls3()
        {
            a = FeedbackAgent.this;
            super();
        }
    }

}
