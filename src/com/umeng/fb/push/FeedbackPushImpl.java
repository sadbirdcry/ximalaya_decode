// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.umeng.fb.push;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.SystemClock;
import com.umeng.fb.ConversationActivity;
import com.umeng.fb.model.Reply;
import com.umeng.fb.model.Store;
import com.umeng.fb.res.f;
import com.umeng.fb.util.Log;
import com.umeng.message.PushAgent;
import com.umeng.message.entity.UMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.umeng.fb.push:
//            IFeedbackPush, FBMessage

public class FeedbackPushImpl
    implements IFeedbackPush
{
    public static interface IFeedbackPushCallbacks
    {

        public abstract void onAddPushDevReply();
    }


    private static IFeedbackPush b;
    private final String a = com/umeng/fb/push/FeedbackPushImpl.getName();
    private Context c;
    public String conversation_id;
    private Class d;
    public List devReplyList;
    private SharedPreferences e;
    private final String f = "feedback_push";
    public boolean fbFragmentTag;
    private final String g = "alias";
    private final String h = "umeng_feedback";
    private final String i = "feedback_id";
    private final String j = "switch";
    private IFeedbackPushCallbacks k;
    private boolean l;

    private FeedbackPushImpl(Context context)
    {
        d = null;
        fbFragmentTag = false;
        c = context;
        devReplyList = new ArrayList();
        e = c.getSharedPreferences("feedback_push", 0);
    }

    static Context a(FeedbackPushImpl feedbackpushimpl)
    {
        return feedbackpushimpl.c;
    }

    private void a()
    {
        Log.c(a, (new StringBuilder()).append("setAlias UUID ").append(Store.getInstance(c).getDeviceUUID()).toString());
        if (!e.getBoolean("alias", false))
        {
            (new _cls2()).start();
        }
    }

    private void a(List list, String s)
    {
        Object obj;
        NotificationManager notificationmanager;
        String s2;
        int i1;
        int j1;
        if (list.size() == 1)
        {
            String s1 = c.getResources().getString(com.umeng.fb.res.f.b(c));
            list = String.format(Locale.US, s1, new Object[] {
                ((Reply)list.get(0)).content
            });
        } else
        {
            obj = c.getResources().getString(com.umeng.fb.res.f.c(c));
            list = String.format(Locale.US, ((String) (obj)), new Object[] {
                Integer.valueOf(list.size())
            });
        }
        notificationmanager = (NotificationManager)c.getSystemService("notification");
        s2 = c.getString(com.umeng.fb.res.f.a(c));
        i1 = c.getPackageManager().getPackageInfo(c.getPackageName(), 0).applicationInfo.icon;
        if (d == null)
        {
            break MISSING_BLOCK_LABEL_262;
        }
        obj = new Intent(c, d);
_L1:
        ((Intent) (obj)).setFlags(0x20000);
        ((Intent) (obj)).putExtra("conversation_id", s);
        j1 = (int)SystemClock.uptimeMillis();
        s = PendingIntent.getActivity(c, j1, ((Intent) (obj)), 0x8000000);
        notificationmanager.notify(0, (new android.support.v4.app.NotificationCompat.Builder(c)).setSmallIcon(i1).setContentTitle(s2).setTicker(s2).setContentText(list).setAutoCancel(true).setContentIntent(s).build());
        return;
        try
        {
            obj = new Intent(c, com/umeng/fb/ConversationActivity);
        }
        // Misplaced declaration of an exception variable
        catch (List list)
        {
            list.printStackTrace();
            return;
        }
          goto _L1
    }

    private boolean a(String s)
    {
        try
        {
            s = (new JSONObject(s)).optString("feedback_id", null);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return false;
        }
        return s != null;
    }

    static SharedPreferences b(FeedbackPushImpl feedbackpushimpl)
    {
        return feedbackpushimpl.e;
    }

    public static IFeedbackPush getInstance(Context context)
    {
        if (b == null)
        {
            b = new FeedbackPushImpl(context);
        }
        return b;
    }

    public boolean dealFBMessage(FBMessage fbmessage)
    {
        Log.c(a, (new StringBuilder()).append("received push message  - ").append(fbmessage.custom).toString());
        if (!a(fbmessage.custom))
        {
            return false;
        }
        if (e == null)
        {
            e = c.getSharedPreferences("feedback_push", 0);
        }
        l = e.getBoolean("switch", false);
        if (!l)
        {
            return true;
        }
        try
        {
            Object obj = new JSONObject(fbmessage.custom);
            fbmessage = ((JSONObject) (obj)).getString("feedback_id");
            obj = Reply.fromJson(((JSONObject) (obj)));
            obj.feedback_id = fbmessage;
            devReplyList.add(obj);
        }
        // Misplaced declaration of an exception variable
        catch (FBMessage fbmessage)
        {
            fbmessage.printStackTrace();
            return false;
        }
        if (fbFragmentTag)
        {
            if (conversation_id != null && conversation_id.endsWith(fbmessage))
            {
                k.onAddPushDevReply();
            } else
            {
                a(devReplyList, fbmessage);
            }
        } else
        {
            a(devReplyList, fbmessage);
        }
        return true;
    }

    public void disable()
    {
        e.edit().putBoolean("switch", false).apply();
        l = false;
    }

    public void enable()
    {
        e.edit().putBoolean("switch", true).apply();
        l = true;
    }

    public void init(Class class1, boolean flag)
    {
        d = class1;
        init(flag);
    }

    public void init(boolean flag)
    {
        a();
        if (flag)
        {
            return;
        }
        try
        {
            _cls1 _lcls1 = new _cls1();
            PushAgent.getInstance(c).setMessageHandler(_lcls1);
        }
        catch (Exception exception1)
        {
            exception1.printStackTrace();
        }
        try
        {
            Class.forName("com.umeng.message.PushAgent");
            return;
        }
        catch (Exception exception)
        {
            return;
        }
    }

    public boolean onFBMessage(Intent intent)
    {
        intent = intent.getStringExtra("body");
        try
        {
            intent = new UMessage(new JSONObject(intent));
            Log.c(a, (new StringBuilder()).append("received push message in onFBMessage - ").append(((UMessage) (intent)).custom).toString());
        }
        // Misplaced declaration of an exception variable
        catch (Intent intent)
        {
            intent.printStackTrace();
            return false;
        }
        return dealFBMessage(new FBMessage(((UMessage) (intent)).custom));
    }

    public void setFBPushCallbacks(IFeedbackPushCallbacks ifeedbackpushcallbacks)
    {
        k = ifeedbackpushcallbacks;
    }

    private class _cls2 extends Thread
    {

        final FeedbackPushImpl a;

        public void run()
        {
            int i1 = 0;
_L2:
            if (i1 >= 10)
            {
                break MISSING_BLOCK_LABEL_64;
            }
            if (!PushAgent.getInstance(FeedbackPushImpl.a(a)).addAlias(Store.getInstance(FeedbackPushImpl.a(a)).getDeviceUUID(), "umeng_feedback"))
            {
                break MISSING_BLOCK_LABEL_65;
            }
            FeedbackPushImpl.b(a).edit().putBoolean("alias", true).apply();
            return;
            try
            {
                sleep(500L);
            }
            catch (InterruptedException interruptedexception)
            {
                try
                {
                    interruptedexception.printStackTrace();
                }
                catch (JSONException jsonexception)
                {
                    jsonexception.printStackTrace();
                    return;
                }
            }
            i1++;
            if (true) goto _L2; else goto _L1
_L1:
        }

        _cls2()
        {
            a = FeedbackPushImpl.this;
            super();
        }
    }


    private class _cls1 extends UmengMessageHandler
    {

        final FeedbackPushImpl a;

        public void dealWithCustomMessage(Context context, UMessage umessage)
        {
            a.dealFBMessage(new FBMessage(umessage.custom));
        }

        _cls1()
        {
            a = FeedbackPushImpl.this;
            super();
        }
    }

}
