// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.umeng.fb.push;

import android.content.Context;
import android.content.Intent;

// Referenced classes of package com.umeng.fb.push:
//            FeedbackPushImpl, IFeedbackPush, FBMessage

public class FeedbackPush
{

    private static FeedbackPush b;
    private final String a = com/umeng/fb/push/FeedbackPush.getName();
    private IFeedbackPush c;

    private FeedbackPush(Context context)
    {
        try
        {
            Class.forName("com.umeng.message.PushAgent");
            c = FeedbackPushImpl.getInstance(context);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
    }

    public static FeedbackPush getInstance(Context context)
    {
        if (b == null)
        {
            b = new FeedbackPush(context);
        }
        return b;
    }

    public boolean dealFBMessage(FBMessage fbmessage)
    {
        return c.dealFBMessage(fbmessage);
    }

    public void disable()
    {
        c.disable();
    }

    public void enable()
    {
        c.enable();
    }

    public void init(Class class1, boolean flag)
    {
        c.init(class1, flag);
    }

    public void init(boolean flag)
    {
        c.init(flag);
    }

    public boolean onFBMessage(Intent intent)
    {
        return c.onFBMessage(intent);
    }
}
