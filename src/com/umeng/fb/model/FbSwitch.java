// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.umeng.fb.model;

import android.content.Context;
import android.content.SharedPreferences;

public class FbSwitch
{

    private static FbSwitch a;
    private static Context b;
    private final String c = "fb_push_switch";
    private final String d = "fb_welcome_info";
    private final String e = "fb_welcome_info_switch";
    private final String f = "fb_push_switch_key";
    private final String g = "fb_welcome_info_key";
    private final String h = "fb_welcome_info_key";
    private SharedPreferences i;
    private SharedPreferences j;
    private SharedPreferences k;

    private FbSwitch(Context context)
    {
        b = context;
        i = b.getSharedPreferences("fb_push_switch", 0);
        j = b.getSharedPreferences("fb_welcome_info", 0);
        k = b.getSharedPreferences("fb_welcome_info_switch", 0);
    }

    public static FbSwitch getInstance(Context context)
    {
        if (a == null)
        {
            a = new FbSwitch(context);
        }
        return a;
    }

    public boolean getFbPushSwitch()
    {
        return i.getBoolean("fb_push_switch_key", true);
    }

    public String getWelcomeInfo()
    {
        return j.getString("fb_welcome_info_key", null);
    }

    public boolean getWelcomeInfoSwitch()
    {
        return k.getBoolean("fb_welcome_info_key", true);
    }

    public void setFbPushSwitch(boolean flag)
    {
        i.edit().putBoolean("fb_push_switch_key", flag).apply();
    }

    public void setWelcomeInfo(String s)
    {
        setWelcomeInfoSwitch(true);
        if (s != null)
        {
            j.edit().putString("fb_welcome_info_key", s).apply();
        }
    }

    public void setWelcomeInfoSwitch(boolean flag)
    {
        k.edit().putBoolean("fb_welcome_info_key", flag).apply();
    }
}
