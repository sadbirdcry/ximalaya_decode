// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.umeng.fb.model;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import com.umeng.fb.SyncListener;
import com.umeng.fb.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;

// Referenced classes of package com.umeng.fb.model:
//            Reply, Store

public class Conversation
{
    public static interface OnChangeListener
    {

        public abstract void onChange();
    }


    private static final String a = com/umeng/fb/model/Conversation.getName();
    private List b;
    private Context c;
    private String d;
    private boolean e;
    private OnChangeListener f;

    private Conversation(Context context)
    {
        b = new ArrayList();
        e = false;
        c = context.getApplicationContext();
    }

    protected static Conversation a(Context context, JSONArray jsonarray, String s)
        throws JSONException
    {
        context = new Conversation(context);
        for (int i = 0; i < jsonarray.length(); i++)
        {
            Reply reply = Reply.fromJson(jsonarray.getJSONObject(i));
            ((Conversation) (context)).b.add(reply);
            if ("new_feedback".equals(reply.type))
            {
                context.e = true;
            }
        }

        context.d = s;
        Collections.sort(((Conversation) (context)).b);
        Log.c(a, (new StringBuilder()).append("fromJson: json = ").append(jsonarray.toString()).append("\n").append("fromJson: conversation = ").append(context.toString()).toString());
        return context;
    }

    private void a()
    {
        Log.c(a, (new StringBuilder()).append("onChange: ").append(toString()).toString());
        Store.getInstance(c).saveConversation(d, this);
        if (f != null)
        {
            f.onChange();
        }
    }

    static void a(Conversation conversation)
    {
        conversation.a();
    }

    static boolean a(Conversation conversation, Reply reply)
    {
        return conversation.a(reply);
    }

    private boolean a(Reply reply)
    {
        for (Iterator iterator = b.iterator(); iterator.hasNext();)
        {
            Reply reply1 = (Reply)iterator.next();
            if (!TextUtils.isEmpty(reply1.reply_id) && "dev_reply".equals(reply1.type) && (reply1.reply_id.equals(reply.reply_id) || reply1.reply_id.equals((new StringBuilder()).append("RP").append(reply.created_at).append("1111").toString())))
            {
                return true;
            }
        }

        return false;
    }

    private static String b()
    {
        return (new StringBuilder()).append("R").append(UUID.randomUUID().toString()).toString();
    }

    static String b(Conversation conversation)
    {
        return conversation.d;
    }

    static Context c(Conversation conversation)
    {
        return conversation.c;
    }

    private static String c()
    {
        return (new StringBuilder()).append("C").append(UUID.randomUUID().toString()).toString();
    }

    static List d(Conversation conversation)
    {
        return conversation.b;
    }

    public static Conversation newInstance(Context context)
    {
        return newInstance(context, c());
    }

    public static Conversation newInstance(Context context, String s)
    {
        Conversation conversation = new Conversation(context);
        conversation.b = new ArrayList();
        conversation.d = s;
        Store.getInstance(context).saveConversation(conversation.d, conversation);
        return conversation;
    }

    public void addReply(Reply reply)
    {
        b.add(reply);
        Collections.sort(b);
        a();
    }

    public void addUserReply(String s)
    {
        String s1 = b();
        if (e || b.size() > 0)
        {
            s = new Reply(s, s1, "user_reply", (new Date()).getTime());
        } else
        {
            s = new Reply(s, s1, "new_feedback", (new Date()).getTime());
            e = true;
        }
        s.status = "sending";
        addReply(s);
    }

    public String getId()
    {
        return d;
    }

    public List getReplyList()
    {
        return b;
    }

    public void setOnChangeListener(OnChangeListener onchangelistener)
    {
        f = onchangelistener;
    }

    public void sync(SyncListener synclistener)
    {
        Log.c(a, (new StringBuilder()).append("sync id=").append(d).append(":\t ").append(this).toString());
        (new Thread(new _cls1(new Handler(), synclistener))).start();
    }

    public JSONArray toJson()
    {
        JSONArray jsonarray = new JSONArray();
        for (Iterator iterator = b.iterator(); iterator.hasNext(); jsonarray.put(((Reply)iterator.next()).toJson())) { }
        return jsonarray;
    }

    public String toString()
    {
        return toJson().toString();
    }


    private class _cls1
        implements Runnable
    {

        final Handler a;
        final SyncListener b;
        final Conversation c;

        public void run()
        {
            ArrayList arraylist;
            ArrayList arraylist1;
            arraylist = new ArrayList();
            arraylist1 = new ArrayList();
            int i = 0;
            long l = 0L;
            while (i < c.getReplyList().size()) 
            {
                Reply reply = (Reply)c.getReplyList().get(i);
                long l1;
                if ("user_reply".equals(reply.type) || "new_feedback".equals(reply.type))
                {
                    l1 = l;
                    if (!"sent".equals(reply.status))
                    {
                        arraylist.add(reply);
                        class _cls1
                            implements Runnable
                        {

                            final Reply a;
                            final _cls1 b;

                            public void run()
                            {
                                a.status = "sending";
                                com.umeng.fb.model.Conversation.a(b.c);
                            }

                _cls1(Reply reply)
                {
                    b = _cls1.this;
                    a = reply;
                    super();
                }
                        }

                        a.post(new _cls1(reply));
                        Map map;
                        if ("new_feedback".equals(reply.type))
                        {
                            map = (new a(Conversation.c(c))).b(Conversation.b(c), reply);
                        } else
                        {
                            map = (new a(Conversation.c(c))).a(Conversation.b(c), reply);
                        }
                        if (map.size() == 2)
                        {
                            class _cls2
                                implements Runnable
                            {

                                final Reply a;
                                final Map b;
                                final _cls1 c;

                                public void run()
                                {
                                    a.created_at = ((Long)b.get("created_at")).longValue();
                                    a.status = "sent";
                                    com.umeng.fb.model.Conversation.a(c.c);
                                }

                _cls2(Reply reply, Map map)
                {
                    c = _cls1.this;
                    a = reply;
                    b = map;
                    super();
                }
                            }

                            a.post(new _cls2(reply, map));
                            l1 = l;
                        } else
                        {
                            class _cls3
                                implements Runnable
                            {

                                final Reply a;
                                final _cls1 b;

                                public void run()
                                {
                                    a.status = "not_sent";
                                    com.umeng.fb.model.Conversation.a(b.c);
                                }

                _cls3(Reply reply)
                {
                    b = _cls1.this;
                    a = reply;
                    super();
                }
                            }

                            a.post(new _cls3(reply));
                            l1 = l;
                        }
                    }
                } else
                {
                    l1 = l;
                    if ("dev_reply".equals(reply.type))
                    {
                        l1 = l;
                        if (l == 0L)
                        {
                            l1 = reply.created_at;
                        }
                    }
                }
                i++;
                l = l1;
            }
            try
            {
                Iterator iterator = (new a(Conversation.c(c))).a(Conversation.b(c)).iterator();
                do
                {
                    if (!iterator.hasNext())
                    {
                        break;
                    }
                    Reply reply1 = (Reply)iterator.next();
                    if ("dev_reply".equals(reply1.type) && !com.umeng.fb.model.Conversation.a(c, reply1))
                    {
                        arraylist1.add(reply1);
                    }
                } while (true);
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
                return;
            }
            class _cls4
                implements Runnable
            {

                final List a;
                final List b;
                final _cls1 c;

                public void run()
                {
                    Conversation.d(c.c).addAll(a);
                    Collections.sort(Conversation.d(c.c));
                    com.umeng.fb.model.Conversation.a(c.c);
                    if (c.b != null)
                    {
                        c.b.onReceiveDevReply(a);
                        c.b.onSendUserReply(b);
                    }
                }

                _cls4(List list, List list1)
                {
                    c = _cls1.this;
                    a = list;
                    b = list1;
                    super();
                }
            }

            a.post(new _cls4(arraylist1, arraylist));
            return;
        }

        _cls1(Handler handler, SyncListener synclistener)
        {
            c = Conversation.this;
            a = handler;
            b = synclistener;
            super();
        }
    }

}
