// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.umeng.fb.model;

import android.os.Handler;
import com.umeng.fb.SyncListener;
import com.umeng.fb.net.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.umeng.fb.model:
//            Conversation, Reply

class b
    implements Runnable
{

    final Handler a;
    final SyncListener b;
    final Conversation c;

    public void run()
    {
        ArrayList arraylist;
        ArrayList arraylist1;
        arraylist = new ArrayList();
        arraylist1 = new ArrayList();
        int i = 0;
        long l = 0L;
        while (i < c.getReplyList().size()) 
        {
            Reply reply = (Reply)c.getReplyList().get(i);
            long l1;
            if ("user_reply".equals(reply.type) || "new_feedback".equals(reply.type))
            {
                l1 = l;
                if (!"sent".equals(reply.status))
                {
                    arraylist.add(reply);
                    class _cls1
                        implements Runnable
                    {

                        final Reply a;
                        final Conversation._cls1 b;

                        public void run()
                        {
                            a.status = "sending";
                            com.umeng.fb.model.Conversation.a(b.c);
                        }

            _cls1(Reply reply)
            {
                b = Conversation._cls1.this;
                a = reply;
                super();
            }
                    }

                    a.post(new _cls1(reply));
                    Map map;
                    if ("new_feedback".equals(reply.type))
                    {
                        map = (new a(Conversation.c(c))).b(Conversation.b(c), reply);
                    } else
                    {
                        map = (new a(Conversation.c(c))).a(Conversation.b(c), reply);
                    }
                    if (map.size() == 2)
                    {
                        class _cls2
                            implements Runnable
                        {

                            final Reply a;
                            final Map b;
                            final Conversation._cls1 c;

                            public void run()
                            {
                                a.created_at = ((Long)b.get("created_at")).longValue();
                                a.status = "sent";
                                com.umeng.fb.model.Conversation.a(c.c);
                            }

            _cls2(Reply reply, Map map)
            {
                c = Conversation._cls1.this;
                a = reply;
                b = map;
                super();
            }
                        }

                        a.post(new _cls2(reply, map));
                        l1 = l;
                    } else
                    {
                        class _cls3
                            implements Runnable
                        {

                            final Reply a;
                            final Conversation._cls1 b;

                            public void run()
                            {
                                a.status = "not_sent";
                                com.umeng.fb.model.Conversation.a(b.c);
                            }

            _cls3(Reply reply)
            {
                b = Conversation._cls1.this;
                a = reply;
                super();
            }
                        }

                        a.post(new _cls3(reply));
                        l1 = l;
                    }
                }
            } else
            {
                l1 = l;
                if ("dev_reply".equals(reply.type))
                {
                    l1 = l;
                    if (l == 0L)
                    {
                        l1 = reply.created_at;
                    }
                }
            }
            i++;
            l = l1;
        }
        try
        {
            Iterator iterator = (new a(Conversation.c(c))).a(Conversation.b(c)).iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                Reply reply1 = (Reply)iterator.next();
                if ("dev_reply".equals(reply1.type) && !com.umeng.fb.model.Conversation.a(c, reply1))
                {
                    arraylist1.add(reply1);
                }
            } while (true);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            return;
        }
        class _cls4
            implements Runnable
        {

            final List a;
            final List b;
            final Conversation._cls1 c;

            public void run()
            {
                Conversation.d(c.c).addAll(a);
                Collections.sort(Conversation.d(c.c));
                com.umeng.fb.model.Conversation.a(c.c);
                if (c.b != null)
                {
                    c.b.onReceiveDevReply(a);
                    c.b.onSendUserReply(b);
                }
            }

            _cls4(List list, List list1)
            {
                c = Conversation._cls1.this;
                a = list;
                b = list1;
                super();
            }
        }

        a.post(new _cls4(arraylist1, arraylist));
        return;
    }

    _cls4(Conversation conversation, Handler handler, SyncListener synclistener)
    {
        c = conversation;
        a = handler;
        b = synclistener;
        super();
    }
}
