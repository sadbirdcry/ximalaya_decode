// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.umeng.analytics.social;

import android.text.TextUtils;

// Referenced classes of package com.umeng.analytics.social:
//            b

public class UMPlatformData
{
    public static class GENDER extends Enum
    {

        public static final GENDER FEMALE;
        public static final GENDER MALE;
        private static final GENDER a[];
        public int value;

        public static GENDER valueOf(String s)
        {
            return (GENDER)Enum.valueOf(com/umeng/analytics/social/UMPlatformData$GENDER, s);
        }

        public static GENDER[] values()
        {
            return (GENDER[])a.clone();
        }

        static 
        {
            class _cls1 extends GENDER
            {

                public String toString()
                {
                    return String.format(Locale.US, "Male:%d", new Object[] {
                        Integer.valueOf(value)
                    });
                }

                _cls1(String s, int i, int j)
                {
                    super(s, i, j, null);
                }
            }

            MALE = new _cls1("MALE", 0, 0);
            class _cls2 extends GENDER
            {

                public String toString()
                {
                    return String.format(Locale.US, "Female:%d", new Object[] {
                        Integer.valueOf(value)
                    });
                }

                _cls2(String s, int i, int j)
                {
                    super(s, i, j, null);
                }
            }

            FEMALE = new _cls2("FEMALE", 1, 1);
            a = (new GENDER[] {
                MALE, FEMALE
            });
        }

        private GENDER(String s, int i, int j)
        {
            super(s, i);
            value = j;
        }

    }

    public static class UMedia extends Enum
    {

        public static final UMedia DOUBAN;
        public static final UMedia RENREN;
        public static final UMedia SINA_WEIBO;
        public static final UMedia TENCENT_QQ;
        public static final UMedia TENCENT_QZONE;
        public static final UMedia TENCENT_WEIBO;
        public static final UMedia WEIXIN_CIRCLE;
        public static final UMedia WEIXIN_FRIENDS;
        private static final UMedia a[];

        public static UMedia valueOf(String s)
        {
            return (UMedia)Enum.valueOf(com/umeng/analytics/social/UMPlatformData$UMedia, s);
        }

        public static UMedia[] values()
        {
            return (UMedia[])a.clone();
        }

        static 
        {
            class _cls1 extends UMedia
            {

                public String toString()
                {
                    return "sina";
                }

                _cls1(String s, int i)
                {
                    super(s, i, null);
                }
            }

            SINA_WEIBO = new _cls1("SINA_WEIBO", 0);
            class _cls2 extends UMedia
            {

                public String toString()
                {
                    return "tencent";
                }

                _cls2(String s, int i)
                {
                    super(s, i, null);
                }
            }

            TENCENT_WEIBO = new _cls2("TENCENT_WEIBO", 1);
            class _cls3 extends UMedia
            {

                public String toString()
                {
                    return "qzone";
                }

                _cls3(String s, int i)
                {
                    super(s, i, null);
                }
            }

            TENCENT_QZONE = new _cls3("TENCENT_QZONE", 2);
            class _cls4 extends UMedia
            {

                public String toString()
                {
                    return "qq";
                }

                _cls4(String s, int i)
                {
                    super(s, i, null);
                }
            }

            TENCENT_QQ = new _cls4("TENCENT_QQ", 3);
            class _cls5 extends UMedia
            {

                public String toString()
                {
                    return "wxsesion";
                }

                _cls5(String s, int i)
                {
                    super(s, i, null);
                }
            }

            WEIXIN_FRIENDS = new _cls5("WEIXIN_FRIENDS", 4);
            class _cls6 extends UMedia
            {

                public String toString()
                {
                    return "wxtimeline";
                }

                _cls6(String s, int i)
                {
                    super(s, i, null);
                }
            }

            WEIXIN_CIRCLE = new _cls6("WEIXIN_CIRCLE", 5);
            class _cls7 extends UMedia
            {

                public String toString()
                {
                    return "renren";
                }

                _cls7(String s, int i)
                {
                    super(s, i, null);
                }
            }

            RENREN = new _cls7("RENREN", 6);
            class _cls8 extends UMedia
            {

                public String toString()
                {
                    return "douban";
                }

                _cls8(String s, int i)
                {
                    super(s, i, null);
                }
            }

            DOUBAN = new _cls8("DOUBAN", 7);
            a = (new UMedia[] {
                SINA_WEIBO, TENCENT_WEIBO, TENCENT_QZONE, TENCENT_QQ, WEIXIN_FRIENDS, WEIXIN_CIRCLE, RENREN, DOUBAN
            });
        }

        private UMedia(String s, int i)
        {
            super(s, i);
        }

    }


    private UMedia a;
    private String b;
    private String c;
    private String d;
    private GENDER e;

    public UMPlatformData(UMedia umedia, String s)
    {
        b = "";
        c = "";
        if (umedia == null || TextUtils.isEmpty(s))
        {
            com.umeng.analytics.social.b.b("MobclickAgent", "parameter is not valid");
            return;
        } else
        {
            a = umedia;
            b = s;
            return;
        }
    }

    public GENDER getGender()
    {
        return e;
    }

    public UMedia getMeida()
    {
        return a;
    }

    public String getName()
    {
        return d;
    }

    public String getUsid()
    {
        return b;
    }

    public String getWeiboId()
    {
        return c;
    }

    public boolean isValid()
    {
        return a != null && !TextUtils.isEmpty(b);
    }

    public void setGender(GENDER gender)
    {
        e = gender;
    }

    public void setName(String s)
    {
        d = s;
    }

    public void setWeiboId(String s)
    {
        c = s;
    }

    public String toString()
    {
        return (new StringBuilder()).append("UMPlatformData [meida=").append(a).append(", usid=").append(b).append(", weiboId=").append(c).append(", name=").append(d).append(", gender=").append(e).append("]").toString();
    }
}
