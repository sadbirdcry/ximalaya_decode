// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.umeng.analytics.social;


// Referenced classes of package com.umeng.analytics.social:
//            UMPlatformData

public static class <init> extends Enum
{

    public static final DOUBAN DOUBAN;
    public static final DOUBAN RENREN;
    public static final DOUBAN SINA_WEIBO;
    public static final DOUBAN TENCENT_QQ;
    public static final DOUBAN TENCENT_QZONE;
    public static final DOUBAN TENCENT_WEIBO;
    public static final DOUBAN WEIXIN_CIRCLE;
    public static final DOUBAN WEIXIN_FRIENDS;
    private static final DOUBAN a[];

    public static <init> valueOf(String s)
    {
        return (<init>)Enum.valueOf(com/umeng/analytics/social/UMPlatformData$UMedia, s);
    }

    public static <init>[] values()
    {
        return (<init>[])a.clone();
    }

    static 
    {
        class _cls1 extends UMPlatformData.UMedia
        {

            public String toString()
            {
                return "sina";
            }

            _cls1(String s, int i)
            {
                super(s, i, null);
            }
        }

        SINA_WEIBO = new _cls1("SINA_WEIBO", 0);
        class _cls2 extends UMPlatformData.UMedia
        {

            public String toString()
            {
                return "tencent";
            }

            _cls2(String s, int i)
            {
                super(s, i, null);
            }
        }

        TENCENT_WEIBO = new _cls2("TENCENT_WEIBO", 1);
        class _cls3 extends UMPlatformData.UMedia
        {

            public String toString()
            {
                return "qzone";
            }

            _cls3(String s, int i)
            {
                super(s, i, null);
            }
        }

        TENCENT_QZONE = new _cls3("TENCENT_QZONE", 2);
        class _cls4 extends UMPlatformData.UMedia
        {

            public String toString()
            {
                return "qq";
            }

            _cls4(String s, int i)
            {
                super(s, i, null);
            }
        }

        TENCENT_QQ = new _cls4("TENCENT_QQ", 3);
        class _cls5 extends UMPlatformData.UMedia
        {

            public String toString()
            {
                return "wxsesion";
            }

            _cls5(String s, int i)
            {
                super(s, i, null);
            }
        }

        WEIXIN_FRIENDS = new _cls5("WEIXIN_FRIENDS", 4);
        class _cls6 extends UMPlatformData.UMedia
        {

            public String toString()
            {
                return "wxtimeline";
            }

            _cls6(String s, int i)
            {
                super(s, i, null);
            }
        }

        WEIXIN_CIRCLE = new _cls6("WEIXIN_CIRCLE", 5);
        class _cls7 extends UMPlatformData.UMedia
        {

            public String toString()
            {
                return "renren";
            }

            _cls7(String s, int i)
            {
                super(s, i, null);
            }
        }

        RENREN = new _cls7("RENREN", 6);
        class _cls8 extends UMPlatformData.UMedia
        {

            public String toString()
            {
                return "douban";
            }

            _cls8(String s, int i)
            {
                super(s, i, null);
            }
        }

        DOUBAN = new _cls8("DOUBAN", 7);
        a = (new a[] {
            SINA_WEIBO, TENCENT_WEIBO, TENCENT_QZONE, TENCENT_QQ, WEIXIN_FRIENDS, WEIXIN_CIRCLE, RENREN, DOUBAN
        });
    }

    private _cls8(String s, int i)
    {
        super(s, i);
    }

    _cls8(String s, int i, _cls8 _pcls8)
    {
        this(s, i);
    }
}
