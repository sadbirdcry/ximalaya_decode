// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.umeng.analytics.social;


// Referenced classes of package com.umeng.analytics.social:
//            UMPlatformData

public static class <init> extends Enum
{

    public static final FEMALE FEMALE;
    public static final FEMALE MALE;
    private static final FEMALE a[];
    public int value;

    public static <init> valueOf(String s)
    {
        return (<init>)Enum.valueOf(com/umeng/analytics/social/UMPlatformData$GENDER, s);
    }

    public static <init>[] values()
    {
        return (<init>[])a.clone();
    }

    static 
    {
        class _cls1 extends UMPlatformData.GENDER
        {

            public String toString()
            {
                return String.format(Locale.US, "Male:%d", new Object[] {
                    Integer.valueOf(value)
                });
            }

            _cls1(String s, int i, int j)
            {
                super(s, i, j, null);
            }
        }

        MALE = new _cls1("MALE", 0, 0);
        class _cls2 extends UMPlatformData.GENDER
        {

            public String toString()
            {
                return String.format(Locale.US, "Female:%d", new Object[] {
                    Integer.valueOf(value)
                });
            }

            _cls2(String s, int i, int j)
            {
                super(s, i, j, null);
            }
        }

        FEMALE = new _cls2("FEMALE", 1, 1);
        a = (new a[] {
            MALE, FEMALE
        });
    }

    private _cls2(String s, int i, int j)
    {
        super(s, i);
        value = j;
    }

    value(String s, int i, int j, value value1)
    {
        this(s, i, j);
    }
}
