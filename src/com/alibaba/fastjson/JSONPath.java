// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;

import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.parser.deserializer.ASMJavaBeanDeserializer;
import com.alibaba.fastjson.parser.deserializer.FieldDeserializer;
import com.alibaba.fastjson.parser.deserializer.JavaBeanDeserializer;
import com.alibaba.fastjson.serializer.ASMJavaBeanSerializer;
import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.JavaBeanSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.util.IOUtils;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Referenced classes of package com.alibaba.fastjson:
//            JSONPathException, JSONException

public class JSONPath
    implements ObjectSerializer
{
    static class ArrayAccessSegement
        implements Segement
    {

        private final int index;

        public Object eval(JSONPath jsonpath, Object obj, Object obj1)
        {
            return jsonpath.getArrayItem(obj1, index);
        }

        public boolean setValue(JSONPath jsonpath, Object obj, Object obj1)
        {
            return jsonpath.setArrayItem(jsonpath, obj, index, obj1);
        }

        public ArrayAccessSegement(int i)
        {
            index = i;
        }
    }

    static interface Filter
    {

        public abstract boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2);
    }

    public static class FilterSegement
        implements Segement
    {

        private final Filter filter;

        public Object eval(JSONPath jsonpath, Object obj, Object obj1)
        {
            Object obj2;
            if (obj1 == null)
            {
                obj2 = null;
            } else
            {
                obj2 = new ArrayList();
                if (obj1 instanceof Iterable)
                {
                    Iterator iterator = ((Iterable)obj1).iterator();
                    do
                    {
                        if (!iterator.hasNext())
                        {
                            break;
                        }
                        Object obj3 = iterator.next();
                        if (filter.apply(jsonpath, obj, obj1, obj3))
                        {
                            ((List) (obj2)).add(obj3);
                        }
                    } while (true);
                    return obj2;
                }
                obj2 = obj1;
                if (!filter.apply(jsonpath, obj, obj1, obj1))
                {
                    return null;
                }
            }
            return obj2;
        }

        public FilterSegement(Filter filter1)
        {
            filter = filter1;
        }
    }

    static class IntBetweenSegement
        implements Filter
    {

        private final long endValue;
        private final boolean not;
        private final String propertyName;
        private final long startValue;

        public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
        {
            jsonpath = ((JSONPath) (jsonpath.getPropertyValue(obj2, propertyName, false)));
            if (jsonpath == null)
            {
                return false;
            }
            if (jsonpath instanceof Number)
            {
                long l = ((Number)jsonpath).longValue();
                if (l >= startValue && l <= endValue)
                {
                    boolean flag;
                    if (!not)
                    {
                        flag = true;
                    } else
                    {
                        flag = false;
                    }
                    return flag;
                }
            }
            return not;
        }

        public IntBetweenSegement(String s, long l, long l1, boolean flag)
        {
            propertyName = s;
            startValue = l;
            endValue = l1;
            not = flag;
        }
    }

    static class IntInSegement
        implements Filter
    {

        private final boolean not;
        private final String propertyName;
        private final long values[];

        public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
        {
            jsonpath = ((JSONPath) (jsonpath.getPropertyValue(obj2, propertyName, false)));
            if (jsonpath == null)
            {
                return false;
            }
            if (jsonpath instanceof Number)
            {
                long l = ((Number)jsonpath).longValue();
                jsonpath = values;
                int j = jsonpath.length;
                for (int i = 0; i < j; i++)
                {
                    if (jsonpath[i] == l)
                    {
                        boolean flag;
                        if (!not)
                        {
                            flag = true;
                        } else
                        {
                            flag = false;
                        }
                        return flag;
                    }
                }

            }
            return not;
        }

        public IntInSegement(String s, long al[], boolean flag)
        {
            propertyName = s;
            values = al;
            not = flag;
        }
    }

    static class IntObjInSegement
        implements Filter
    {

        private final boolean not;
        private final String propertyName;
        private final Long values[];

        public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
        {
            boolean flag;
            flag = true;
            jsonpath = ((JSONPath) (jsonpath.getPropertyValue(obj2, propertyName, false)));
            if (jsonpath == null)
            {
                jsonpath = values;
                int k = jsonpath.length;
                for (int i = 0; i < k; i++)
                {
                    if (jsonpath[i] == null)
                    {
                        return !not;
                    }
                }

                return not;
            }
            if (!(jsonpath instanceof Number)) goto _L2; else goto _L1
_L1:
            int j;
            int l;
            long l1;
            l1 = ((Number)jsonpath).longValue();
            jsonpath = values;
            l = jsonpath.length;
            j = 0;
_L4:
            if (j >= l)
            {
                break; /* Loop/switch isn't completed */
            }
            obj = jsonpath[j];
              goto _L3
_L6:
            j++;
            if (true) goto _L4; else goto _L2
_L3:
            if (obj == null || ((Long) (obj)).longValue() != l1) goto _L6; else goto _L5
_L5:
            if (not)
            {
                flag = false;
            }
            return flag;
_L2:
            return not;
        }

        public IntObjInSegement(String s, Long along[], boolean flag)
        {
            propertyName = s;
            values = along;
            not = flag;
        }
    }

    static class IntOpSegement
        implements Filter
    {

        private final Operator op;
        private final String propertyName;
        private final long value;

        public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
        {
            boolean flag;
            flag = true;
            jsonpath = ((JSONPath) (jsonpath.getPropertyValue(obj2, propertyName, false)));
            if (jsonpath != null) goto _L2; else goto _L1
_L1:
            flag = false;
_L4:
            return flag;
_L2:
            long l;
            if (!(jsonpath instanceof Number))
            {
                return false;
            }
            l = ((Number)jsonpath).longValue();
            if (op == Operator.EQ)
            {
                boolean flag1;
                if (l == value)
                {
                    flag1 = true;
                } else
                {
                    flag1 = false;
                }
                return flag1;
            }
            if (op != Operator.NE)
            {
                break; /* Loop/switch isn't completed */
            }
            if (l == value)
            {
                return false;
            }
            if (true) goto _L4; else goto _L3
_L3:
            if (op != Operator.GE)
            {
                break; /* Loop/switch isn't completed */
            }
            if (l < value)
            {
                return false;
            }
            if (true) goto _L4; else goto _L5
_L5:
            if (op != Operator.GT)
            {
                break; /* Loop/switch isn't completed */
            }
            if (l <= value)
            {
                return false;
            }
            if (true) goto _L4; else goto _L6
_L6:
            if (op != Operator.LE)
            {
                break; /* Loop/switch isn't completed */
            }
            if (l > value)
            {
                return false;
            }
            if (true) goto _L4; else goto _L7
_L7:
            if (op == Operator.LT)
            {
                if (l >= value)
                {
                    return false;
                }
            } else
            {
                return false;
            }
            if (true) goto _L4; else goto _L8
_L8:
        }

        public IntOpSegement(String s, long l, Operator operator)
        {
            propertyName = s;
            value = l;
            op = operator;
        }
    }

    static class JSONPathParser
    {

        private char ch;
        private int level;
        private final String path;
        private int pos;

        static boolean isDigitFirst(char c)
        {
            return c == '-' || c == '+' || c >= '0' && c <= '9';
        }

        void accept(char c)
        {
            if (ch != c)
            {
                throw new JSONPathException((new StringBuilder()).append("expect '").append(c).append(", but '").append(ch).append("'").toString());
            }
            if (!isEOF())
            {
                next();
            }
        }

        Segement buildArraySegement(String s)
        {
            int ai1[];
            int i;
            i = 0;
            int k = 0;
            int k1 = s.length();
            char c = s.charAt(0);
            char c1 = s.charAt(k1 - 1);
            int i1 = s.indexOf(',');
            if (s.length() > 2 && c == '\'' && c1 == '\'')
            {
                if (i1 == -1)
                {
                    return new PropertySegement(s.substring(1, k1 - 1));
                }
                s = s.split(",");
                String as[] = new String[s.length];
                for (i = k; i < s.length; i++)
                {
                    String s1 = s[i];
                    as[i] = s1.substring(1, s1.length() - 1);
                }

                return new MultiPropertySegement(as);
            }
            k = s.indexOf(':');
            if (i1 == -1 && k == -1)
            {
                return new ArrayAccessSegement(Integer.parseInt(s));
            }
            if (i1 != -1)
            {
                s = s.split(",");
                int ai[] = new int[s.length];
                for (; i < s.length; i++)
                {
                    ai[i] = Integer.parseInt(s[i]);
                }

                return new MultiIndexSegement(ai);
            }
            if (k == -1)
            {
                break MISSING_BLOCK_LABEL_444;
            }
            s = s.split(":");
            ai1 = new int[s.length];
            i = 0;
_L2:
            String s2;
            if (i >= s.length)
            {
                break MISSING_BLOCK_LABEL_308;
            }
            s2 = s[i];
            if (!s2.isEmpty())
            {
                break MISSING_BLOCK_LABEL_297;
            }
            if (i != 0)
            {
                break; /* Loop/switch isn't completed */
            }
            ai1[i] = 0;
_L3:
            i++;
            if (true) goto _L2; else goto _L1
_L1:
            throw new UnsupportedOperationException();
            ai1[i] = Integer.parseInt(s2);
              goto _L3
            int j1 = ai1[0];
            int j;
            int l;
            if (ai1.length > 1)
            {
                j = ai1[1];
            } else
            {
                j = -1;
            }
            if (ai1.length == 3)
            {
                l = ai1[2];
            } else
            {
                l = 1;
            }
            if (j >= 0 && j < j1)
            {
                throw new UnsupportedOperationException((new StringBuilder()).append("end must greater than or equals start. start ").append(j1).append(",  end ").append(j).toString());
            }
            if (l <= 0)
            {
                throw new UnsupportedOperationException((new StringBuilder()).append("step must greater than zero : ").append(l).toString());
            } else
            {
                return new RangeSegement(j1, j, l);
            }
            throw new UnsupportedOperationException();
        }

        public Segement[] explain()
        {
            if (path == null || path.isEmpty())
            {
                throw new IllegalArgumentException();
            }
            Segement asegement[] = new Segement[8];
            do
            {
                Segement segement = readSegement();
                int i;
                if (segement == null)
                {
                    if (level == asegement.length)
                    {
                        return asegement;
                    } else
                    {
                        Segement asegement1[] = new Segement[level];
                        System.arraycopy(asegement, 0, asegement1, 0, level);
                        return asegement1;
                    }
                }
                i = level;
                level = i + 1;
                asegement[i] = segement;
            } while (true);
        }

        boolean isEOF()
        {
            return pos >= path.length();
        }

        void next()
        {
            String s = path;
            int i = pos;
            pos = i + 1;
            ch = s.charAt(i);
        }

        Segement parseArrayAccess(boolean flag)
        {
            boolean flag4 = true;
            if (flag)
            {
                accept('[');
            }
            Object obj;
            Object obj1;
            Object obj2;
            Object obj3;
            String s;
            int i;
            boolean flag1;
            int j;
            boolean flag2;
            int k;
            boolean flag3;
            long l;
            if (ch == '?')
            {
                next();
                accept('(');
                if (ch == '@')
                {
                    next();
                    accept('.');
                }
                i = 1;
            } else
            {
                i = 0;
            }
            if (i == 0 && !IOUtils.firstIdentifier(ch)) goto _L2; else goto _L1
_L1:
            s = readName();
            skipWhitespace();
            if (i == 0 || ch != ')') goto _L4; else goto _L3
_L3:
            next();
            if (flag)
            {
                accept(']');
            }
            obj = new FilterSegement(new NotNullSegement(s));
_L9:
            return ((Segement) (obj));
_L4:
            if (flag && ch == ']')
            {
                next();
                return new FilterSegement(new NotNullSegement(s));
            }
            obj1 = readOp();
            skipWhitespace();
            if (obj1 == Operator.BETWEEN || obj1 == Operator.NOT_BETWEEN)
            {
                if (obj1 == Operator.NOT_BETWEEN)
                {
                    flag = flag4;
                } else
                {
                    flag = false;
                }
                obj = readValue();
                if (!"and".equalsIgnoreCase(readName()))
                {
                    throw new JSONPathException(path);
                }
                obj1 = readValue();
                if (obj == null || obj1 == null)
                {
                    throw new JSONPathException(path);
                }
                if (JSONPath.isInt(obj.getClass()) && JSONPath.isInt(obj1.getClass()))
                {
                    return new FilterSegement(new IntBetweenSegement(s, ((Number)obj).longValue(), ((Number)obj1).longValue(), flag));
                } else
                {
                    throw new JSONPathException(path);
                }
            }
            if (obj1 == Operator.IN || obj1 == Operator.NOT_IN)
            {
                if (obj1 == Operator.NOT_IN)
                {
                    flag4 = true;
                } else
                {
                    flag4 = false;
                }
                accept('(');
                obj = new ArrayList();
                ((List) (obj)).add(readValue());
                do
                {
                    skipWhitespace();
                    if (ch != ',')
                    {
                        accept(')');
                        if (i != 0)
                        {
                            accept(')');
                        }
                        if (flag)
                        {
                            accept(']');
                        }
                        obj1 = ((List) (obj)).iterator();
                        flag1 = true;
                        flag2 = true;
                        i = 1;
                        while (((Iterator) (obj1)).hasNext()) 
                        {
                            obj2 = ((Iterator) (obj1)).next();
                            if (obj2 == null)
                            {
                                if (i != 0)
                                {
                                    i = 0;
                                }
                            } else
                            {
                                obj2 = obj2.getClass();
                                flag3 = flag2;
                                k = i;
                                if (i != 0)
                                {
                                    flag3 = flag2;
                                    k = i;
                                    if (obj2 != java/lang/Byte)
                                    {
                                        flag3 = flag2;
                                        k = i;
                                        if (obj2 != java/lang/Short)
                                        {
                                            flag3 = flag2;
                                            k = i;
                                            if (obj2 != java/lang/Integer)
                                            {
                                                flag3 = flag2;
                                                k = i;
                                                if (obj2 != java/lang/Long)
                                                {
                                                    flag3 = false;
                                                    k = 0;
                                                }
                                            }
                                        }
                                    }
                                }
                                flag2 = flag3;
                                i = k;
                                if (flag1)
                                {
                                    flag2 = flag3;
                                    i = k;
                                    if (obj2 != java/lang/String)
                                    {
                                        flag1 = false;
                                        flag2 = flag3;
                                        i = k;
                                    }
                                }
                            }
                        }
                        break;
                    }
                    next();
                    ((List) (obj)).add(readValue());
                } while (true);
                if (((List) (obj)).size() == 1 && ((List) (obj)).get(0) == null)
                {
                    if (flag4)
                    {
                        return new FilterSegement(new NotNullSegement(s));
                    } else
                    {
                        return new FilterSegement(new NullSegement(s));
                    }
                }
                if (i != 0)
                {
                    if (((List) (obj)).size() == 1)
                    {
                        l = ((Number)((List) (obj)).get(0)).longValue();
                        if (flag4)
                        {
                            obj = Operator.NE;
                        } else
                        {
                            obj = Operator.EQ;
                        }
                        return new FilterSegement(new IntOpSegement(s, l, ((Operator) (obj))));
                    }
                    obj1 = new long[((List) (obj)).size()];
                    for (i = 0; i < obj1.length; i++)
                    {
                        obj1[i] = ((Number)((List) (obj)).get(i)).longValue();
                    }

                    return new FilterSegement(new IntInSegement(s, ((long []) (obj1)), flag4));
                }
                if (flag1)
                {
                    if (((List) (obj)).size() == 1)
                    {
                        obj1 = (String)((List) (obj)).get(0);
                        if (flag4)
                        {
                            obj = Operator.NE;
                        } else
                        {
                            obj = Operator.EQ;
                        }
                        return new FilterSegement(new StringOpSegement(s, ((String) (obj1)), ((Operator) (obj))));
                    } else
                    {
                        obj1 = new String[((List) (obj)).size()];
                        ((List) (obj)).toArray(((Object []) (obj1)));
                        return new FilterSegement(new StringInSegement(s, ((String []) (obj1)), flag4));
                    }
                }
                if (flag2)
                {
                    obj1 = new Long[((List) (obj)).size()];
                    for (i = 0; i < obj1.length; i++)
                    {
                        obj2 = (Number)((List) (obj)).get(i);
                        if (obj2 != null)
                        {
                            obj1[i] = Long.valueOf(((Number) (obj2)).longValue());
                        }
                    }

                    return new FilterSegement(new IntObjInSegement(s, ((Long []) (obj1)), flag4));
                } else
                {
                    throw new UnsupportedOperationException();
                }
            }
            if (ch != '\'' && ch != '"') goto _L6; else goto _L5
_L5:
            obj2 = readString();
            if (i != 0)
            {
                accept(')');
            }
            if (flag)
            {
                accept(']');
            }
            if (obj1 == Operator.RLIKE)
            {
                return new FilterSegement(new RlikeSegement(s, ((String) (obj2)), false));
            }
            if (obj1 == Operator.NOT_RLIKE)
            {
                return new FilterSegement(new RlikeSegement(s, ((String) (obj2)), true));
            }
            obj = obj2;
            if (obj1 != Operator.LIKE)
            {
                if (obj1 != Operator.NOT_LIKE)
                {
                    break MISSING_BLOCK_LABEL_1696;
                }
            }
            for (obj = obj2; ((String) (obj)).indexOf("%%") != -1; obj = ((String) (obj)).replaceAll("%%", "%")) { }
            if (obj1 == Operator.NOT_LIKE)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            i = ((String) (obj)).indexOf('%');
            if (i != -1) goto _L8; else goto _L7
_L7:
            if (obj1 == Operator.LIKE)
            {
                obj1 = Operator.EQ;
            } else
            {
                obj1 = Operator.NE;
            }
_L10:
            return new FilterSegement(new StringOpSegement(s, ((String) (obj)), ((Operator) (obj1))));
_L8:
            obj1 = ((String) (obj)).split("%");
            obj2 = null;
            if (i == 0)
            {
                if (((String) (obj)).charAt(((String) (obj)).length() - 1) == '%')
                {
                    obj2 = new String[obj1.length - 1];
                    System.arraycopy(obj1, 1, obj2, 0, obj2.length);
                    obj1 = null;
                    obj = null;
                } else
                {
                    obj = obj1[obj1.length - 1];
                    if (obj1.length > 2)
                    {
                        obj2 = new String[obj1.length - 2];
                        System.arraycopy(obj1, 1, obj2, 0, obj2.length);
                        obj1 = obj;
                        obj = null;
                    } else
                    {
                        obj1 = obj;
                        obj = null;
                    }
                }
            } else
            if (((String) (obj)).charAt(((String) (obj)).length() - 1) == '%')
            {
                obj2 = obj1;
                obj = null;
                obj1 = null;
            } else
            if (obj1.length == 1)
            {
                obj = obj1[0];
                obj1 = null;
            } else
            if (obj1.length == 2)
            {
                obj = obj1[0];
                obj1 = obj1[1];
            } else
            {
                obj = obj1[0];
                obj3 = obj1[obj1.length - 1];
                obj2 = new String[obj1.length - 2];
                System.arraycopy(obj1, 1, obj2, 0, obj2.length);
                obj1 = obj3;
            }
            return new FilterSegement(new MatchSegement(s, ((String) (obj)), ((String) (obj1)), ((String []) (obj2)), flag));
_L6:
            if (isDigitFirst(ch))
            {
                l = readLongValue();
                if (i != 0)
                {
                    accept(')');
                }
                if (flag)
                {
                    accept(']');
                }
                return new FilterSegement(new IntOpSegement(s, l, ((Operator) (obj1))));
            }
            if (ch == 'n' && "null".equals(readName()))
            {
                if (i != 0)
                {
                    accept(')');
                }
                accept(']');
                if (obj1 == Operator.EQ)
                {
                    return new FilterSegement(new NullSegement(s));
                }
                if (obj1 == Operator.NE)
                {
                    return new FilterSegement(new NotNullSegement(s));
                } else
                {
                    throw new UnsupportedOperationException();
                }
            } else
            {
                throw new UnsupportedOperationException();
            }
_L2:
            j = pos;
            for (; ch != ']' && ch != '/' && !isEOF(); next()) { }
            if (flag)
            {
                i = pos - 1;
            } else
            if (ch == '/')
            {
                i = pos - 1;
            } else
            {
                i = pos;
            }
            obj1 = buildArraySegement(path.substring(j - 1, i));
            obj = obj1;
            if (flag)
            {
                obj = obj1;
                if (!isEOF())
                {
                    accept(']');
                    return ((Segement) (obj1));
                }
            }
              goto _L9
            obj = obj2;
              goto _L10
        }

        protected long readLongValue()
        {
            int i = pos;
            if (ch == '+' || ch == '-')
            {
                next();
            }
            while (ch >= '0' && ch <= '9') 
            {
                next();
            }
            int j = pos;
            return Long.parseLong(path.substring(i - 1, j - 1));
        }

        String readName()
        {
            skipWhitespace();
            if (!IOUtils.firstIdentifier(ch))
            {
                throw new JSONPathException((new StringBuilder()).append("illeal jsonpath syntax. ").append(path).toString());
            }
            StringBuffer stringbuffer = new StringBuffer();
            do
            {
label0:
                {
                    if (!isEOF())
                    {
                        if (ch == '\\')
                        {
                            next();
                            stringbuffer.append(ch);
                            next();
                            continue;
                        }
                        if (IOUtils.isIdent(ch))
                        {
                            break label0;
                        }
                    }
                    if (isEOF() && IOUtils.isIdent(ch))
                    {
                        stringbuffer.append(ch);
                    }
                    return stringbuffer.toString();
                }
                stringbuffer.append(ch);
                next();
            } while (true);
        }

        protected Operator readOp()
        {
            Object obj;
label0:
            {
label1:
                {
                    obj = null;
                    Operator operator;
                    if (ch == '=')
                    {
                        next();
                        obj = Operator.EQ;
                    } else
                    if (ch == '!')
                    {
                        next();
                        accept('=');
                        obj = Operator.NE;
                    } else
                    if (ch == '<')
                    {
                        next();
                        if (ch == '=')
                        {
                            next();
                            obj = Operator.LE;
                        } else
                        {
                            obj = Operator.LT;
                        }
                    } else
                    if (ch == '>')
                    {
                        next();
                        if (ch == '=')
                        {
                            next();
                            obj = Operator.GE;
                        } else
                        {
                            obj = Operator.GT;
                        }
                    }
                    operator = ((Operator) (obj));
                    if (obj == null)
                    {
                        obj = readName();
                        if (!"not".equalsIgnoreCase(((String) (obj))))
                        {
                            break label0;
                        }
                        skipWhitespace();
                        obj = readName();
                        if (!"like".equalsIgnoreCase(((String) (obj))))
                        {
                            break label1;
                        }
                        operator = Operator.NOT_LIKE;
                    }
                    return operator;
                }
                if ("rlike".equalsIgnoreCase(((String) (obj))))
                {
                    return Operator.NOT_RLIKE;
                }
                if ("in".equalsIgnoreCase(((String) (obj))))
                {
                    return Operator.NOT_IN;
                }
                if ("between".equalsIgnoreCase(((String) (obj))))
                {
                    return Operator.NOT_BETWEEN;
                } else
                {
                    throw new UnsupportedOperationException();
                }
            }
            if ("like".equalsIgnoreCase(((String) (obj))))
            {
                return Operator.LIKE;
            }
            if ("rlike".equalsIgnoreCase(((String) (obj))))
            {
                return Operator.RLIKE;
            }
            if ("in".equalsIgnoreCase(((String) (obj))))
            {
                return Operator.IN;
            }
            if ("between".equalsIgnoreCase(((String) (obj))))
            {
                return Operator.BETWEEN;
            } else
            {
                throw new UnsupportedOperationException();
            }
        }

        Segement readSegement()
        {
            if (level == 0 && path.length() == 1)
            {
                if (isDigitFirst(ch))
                {
                    return new ArrayAccessSegement(ch - 48);
                }
                if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z')
                {
                    return new PropertySegement(Character.toString(ch));
                }
            }
              goto _L1
_L4:
            if (ch != '$') goto _L3; else goto _L2
_L2:
            next();
_L1:
            if (!isEOF())
            {
                skipWhitespace();
                if (ch == '@')
                {
                    next();
                    return SelfSegement.instance;
                }
            } else
            {
                return null;
            }
            if (true) goto _L4; else goto _L3
_L3:
            if (ch == '.' || ch == '/')
            {
                next();
                if (ch == '*')
                {
                    if (!isEOF())
                    {
                        next();
                    }
                    return WildCardSegement.instance;
                }
                if (isDigitFirst(ch))
                {
                    return parseArrayAccess(false);
                }
                String s = readName();
                if (ch == '(')
                {
                    next();
                    if (ch == ')')
                    {
                        if (!isEOF())
                        {
                            next();
                        }
                        if ("size".equals(s))
                        {
                            return SizeSegement.instance;
                        } else
                        {
                            throw new UnsupportedOperationException();
                        }
                    } else
                    {
                        throw new UnsupportedOperationException();
                    }
                } else
                {
                    return new PropertySegement(s);
                }
            }
            if (ch == '[')
            {
                return parseArrayAccess(true);
            }
            if (level == 0)
            {
                return new PropertySegement(readName());
            } else
            {
                throw new UnsupportedOperationException();
            }
        }

        String readString()
        {
            char c = ch;
            next();
            int j = pos;
            for (; ch != c && !isEOF(); next()) { }
            String s = path;
            int i;
            if (isEOF())
            {
                i = pos;
            } else
            {
                i = pos - 1;
            }
            s = s.substring(j - 1, i);
            accept(c);
            return s;
        }

        protected Object readValue()
        {
            skipWhitespace();
            if (isDigitFirst(ch))
            {
                return Long.valueOf(readLongValue());
            }
            if (ch == '"' || ch == '\'')
            {
                return readString();
            }
            if (ch == 'n')
            {
                if ("null".equals(readName()))
                {
                    return null;
                } else
                {
                    throw new JSONPathException(path);
                }
            } else
            {
                throw new UnsupportedOperationException();
            }
        }

        public final void skipWhitespace()
        {
            while (ch < IOUtils.whitespaceFlags.length && IOUtils.whitespaceFlags[ch]) 
            {
                next();
            }
        }

        public JSONPathParser(String s)
        {
            path = s;
            next();
        }
    }

    static class MatchSegement
        implements Filter
    {

        private final String containsValues[];
        private final String endsWithValue;
        private final int minLength;
        private final boolean not;
        private final String propertyName;
        private final String startsWithValue;

        public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
        {
            jsonpath = ((JSONPath) (jsonpath.getPropertyValue(obj2, propertyName, false)));
            if (jsonpath != null)
            {
                jsonpath = jsonpath.toString();
                if (jsonpath.length() < minLength)
                {
                    return not;
                }
                int i;
                if (startsWithValue != null)
                {
                    if (!jsonpath.startsWith(startsWithValue))
                    {
                        return not;
                    }
                    i = startsWithValue.length() + 0;
                } else
                {
                    i = 0;
                }
                if (containsValues != null)
                {
                    obj = containsValues;
                    int k = obj.length;
                    boolean flag = false;
                    int j = i;
                    for (i = ((flag) ? 1 : 0); i < k; i++)
                    {
                        obj1 = obj[i];
                        j = jsonpath.indexOf(((String) (obj1)), j);
                        if (j == -1)
                        {
                            return not;
                        }
                        j += ((String) (obj1)).length();
                    }

                }
                if (endsWithValue != null && !jsonpath.endsWith(endsWithValue))
                {
                    return not;
                }
                if (!not)
                {
                    return true;
                }
            }
            return false;
        }

        public MatchSegement(String s, String s1, String s2, String as[], boolean flag)
        {
            boolean flag1 = false;
            super();
            propertyName = s;
            startsWithValue = s1;
            endsWithValue = s2;
            containsValues = as;
            not = flag;
            int i;
            int j;
            int k;
            if (s1 != null)
            {
                j = s1.length() + 0;
            } else
            {
                j = 0;
            }
            i = j;
            if (s2 != null)
            {
                i = j + s2.length();
            }
            k = i;
            if (as != null)
            {
                int l = as.length;
                j = ((flag1) ? 1 : 0);
                do
                {
                    k = i;
                    if (j >= l)
                    {
                        break;
                    }
                    k = as[j].length();
                    j++;
                    i = k + i;
                } while (true);
            }
            minLength = k;
        }
    }

    static class MultiIndexSegement
        implements Segement
    {

        private final int indexes[];

        public Object eval(JSONPath jsonpath, Object obj, Object obj1)
        {
            obj = new ArrayList(indexes.length);
            for (int i = 0; i < indexes.length; i++)
            {
                ((List) (obj)).add(jsonpath.getArrayItem(obj1, indexes[i]));
            }

            return obj;
        }

        public MultiIndexSegement(int ai[])
        {
            indexes = ai;
        }
    }

    static class MultiPropertySegement
        implements Segement
    {

        private final String propertyNames[];

        public Object eval(JSONPath jsonpath, Object obj, Object obj1)
        {
            obj = new ArrayList(propertyNames.length);
            String as[] = propertyNames;
            int j = as.length;
            for (int i = 0; i < j; i++)
            {
                ((List) (obj)).add(jsonpath.getPropertyValue(obj1, as[i], true));
            }

            return obj;
        }

        public MultiPropertySegement(String as[])
        {
            propertyNames = as;
        }
    }

    static class NotNullSegement
        implements Filter
    {

        private final String propertyName;

        public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
        {
            boolean flag = false;
            if (jsonpath.getPropertyValue(obj2, propertyName, false) != null)
            {
                flag = true;
            }
            return flag;
        }

        public NotNullSegement(String s)
        {
            propertyName = s;
        }
    }

    static class NullSegement
        implements Filter
    {

        private final String propertyName;

        public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
        {
            boolean flag = false;
            if (jsonpath.getPropertyValue(obj2, propertyName, false) == null)
            {
                flag = true;
            }
            return flag;
        }

        public NullSegement(String s)
        {
            propertyName = s;
        }
    }

    static final class Operator extends Enum
    {

        private static final Operator $VALUES[];
        public static final Operator BETWEEN;
        public static final Operator EQ;
        public static final Operator GE;
        public static final Operator GT;
        public static final Operator IN;
        public static final Operator LE;
        public static final Operator LIKE;
        public static final Operator LT;
        public static final Operator NE;
        public static final Operator NOT_BETWEEN;
        public static final Operator NOT_IN;
        public static final Operator NOT_LIKE;
        public static final Operator NOT_RLIKE;
        public static final Operator RLIKE;

        public static Operator valueOf(String s)
        {
            return (Operator)Enum.valueOf(com/alibaba/fastjson/JSONPath$Operator, s);
        }

        public static Operator[] values()
        {
            return (Operator[])$VALUES.clone();
        }

        static 
        {
            EQ = new Operator("EQ", 0);
            NE = new Operator("NE", 1);
            GT = new Operator("GT", 2);
            GE = new Operator("GE", 3);
            LT = new Operator("LT", 4);
            LE = new Operator("LE", 5);
            LIKE = new Operator("LIKE", 6);
            NOT_LIKE = new Operator("NOT_LIKE", 7);
            RLIKE = new Operator("RLIKE", 8);
            NOT_RLIKE = new Operator("NOT_RLIKE", 9);
            IN = new Operator("IN", 10);
            NOT_IN = new Operator("NOT_IN", 11);
            BETWEEN = new Operator("BETWEEN", 12);
            NOT_BETWEEN = new Operator("NOT_BETWEEN", 13);
            $VALUES = (new Operator[] {
                EQ, NE, GT, GE, LT, LE, LIKE, NOT_LIKE, RLIKE, NOT_RLIKE, 
                IN, NOT_IN, BETWEEN, NOT_BETWEEN
            });
        }

        private Operator(String s, int i)
        {
            super(s, i);
        }
    }

    static class PropertySegement
        implements Segement
    {

        private final String propertyName;

        public Object eval(JSONPath jsonpath, Object obj, Object obj1)
        {
            return jsonpath.getPropertyValue(obj1, propertyName, true);
        }

        public void setValue(JSONPath jsonpath, Object obj, Object obj1)
        {
            jsonpath.setPropertyValue(obj, propertyName, obj1);
        }

        public PropertySegement(String s)
        {
            propertyName = s;
        }
    }

    static class RangeSegement
        implements Segement
    {

        private final int end;
        private final int start;
        private final int step;

        public Object eval(JSONPath jsonpath, Object obj, Object obj1)
        {
            int k = SizeSegement.instance.eval(jsonpath, obj, obj1).intValue();
            int i;
            int j;
            if (start >= 0)
            {
                i = start;
            } else
            {
                i = start + k;
            }
            if (end >= 0)
            {
                j = end;
            } else
            {
                j = end + k;
            }
            obj = new ArrayList((j - i) / step + 1);
            for (; i <= j && i < k; i += step)
            {
                ((List) (obj)).add(jsonpath.getArrayItem(obj1, i));
            }

            return obj;
        }

        public RangeSegement(int i, int j, int k)
        {
            start = i;
            end = j;
            step = k;
        }
    }

    static class RlikeSegement
        implements Filter
    {

        private final boolean not;
        private final Pattern pattern;
        private final String propertyName;

        public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
        {
            jsonpath = ((JSONPath) (jsonpath.getPropertyValue(obj2, propertyName, false)));
            if (jsonpath != null)
            {
                jsonpath = jsonpath.toString();
                boolean flag = pattern.matcher(jsonpath).matches();
                if (not)
                {
                    if (!flag)
                    {
                        return true;
                    }
                } else
                {
                    return flag;
                }
            }
            return false;
        }

        public RlikeSegement(String s, String s1, boolean flag)
        {
            propertyName = s;
            pattern = Pattern.compile(s1);
            not = flag;
        }
    }

    static interface Segement
    {

        public abstract Object eval(JSONPath jsonpath, Object obj, Object obj1);
    }

    static class SelfSegement
        implements Segement
    {

        public static final SelfSegement instance = new SelfSegement();

        public Object eval(JSONPath jsonpath, Object obj, Object obj1)
        {
            return obj1;
        }


        SelfSegement()
        {
        }
    }

    static class SizeSegement
        implements Segement
    {

        public static final SizeSegement instance = new SizeSegement();

        public Integer eval(JSONPath jsonpath, Object obj, Object obj1)
        {
            return Integer.valueOf(jsonpath.evalSize(obj1));
        }

        public volatile Object eval(JSONPath jsonpath, Object obj, Object obj1)
        {
            return eval(jsonpath, obj, obj1);
        }


        SizeSegement()
        {
        }
    }

    static class StringInSegement
        implements Filter
    {

        private final boolean not;
        private final String propertyName;
        private final String values[];

        public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
        {
            int i;
            int j;
            jsonpath = ((JSONPath) (jsonpath.getPropertyValue(obj2, propertyName, false)));
            obj = values;
            j = obj.length;
            i = 0;
_L7:
            if (i >= j)
            {
                break; /* Loop/switch isn't completed */
            }
            obj1 = obj[i];
            if (obj1 != jsonpath) goto _L2; else goto _L1
_L1:
            if (not) goto _L4; else goto _L3
_L3:
            return true;
_L4:
            return false;
_L2:
            if (obj1 == null || !((String) (obj1)).equals(jsonpath))
            {
                break; /* Loop/switch isn't completed */
            }
            if (not)
            {
                return false;
            }
            if (true) goto _L3; else goto _L5
_L5:
            i++;
            if (true) goto _L7; else goto _L6
_L6:
            return not;
        }

        public StringInSegement(String s, String as[], boolean flag)
        {
            propertyName = s;
            values = as;
            not = flag;
        }
    }

    static class StringOpSegement
        implements Filter
    {

        private final Operator op;
        private final String propertyName;
        private final String value;

        public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
        {
            boolean flag;
            flag = true;
            jsonpath = ((JSONPath) (jsonpath.getPropertyValue(obj2, propertyName, false)));
            if (op != Operator.EQ) goto _L2; else goto _L1
_L1:
            flag = value.equals(jsonpath);
_L4:
            return flag;
_L2:
            if (op != Operator.NE)
            {
                break; /* Loop/switch isn't completed */
            }
            if (value.equals(jsonpath))
            {
                return false;
            }
            if (true) goto _L4; else goto _L3
_L3:
            int i;
            if (jsonpath == null)
            {
                return false;
            }
            i = value.compareTo(jsonpath.toString());
            if (op != Operator.GE)
            {
                break; /* Loop/switch isn't completed */
            }
            if (i > 0)
            {
                return false;
            }
            if (true) goto _L4; else goto _L5
_L5:
            if (op != Operator.GT)
            {
                break; /* Loop/switch isn't completed */
            }
            if (i >= 0)
            {
                return false;
            }
            if (true) goto _L4; else goto _L6
_L6:
            if (op != Operator.LE)
            {
                break; /* Loop/switch isn't completed */
            }
            if (i < 0)
            {
                return false;
            }
            if (true) goto _L4; else goto _L7
_L7:
            if (op == Operator.LT)
            {
                if (i <= 0)
                {
                    return false;
                }
            } else
            {
                return false;
            }
            if (true) goto _L4; else goto _L8
_L8:
        }

        public StringOpSegement(String s, String s1, Operator operator)
        {
            propertyName = s;
            value = s1;
            op = operator;
        }
    }

    static class WildCardSegement
        implements Segement
    {

        public static WildCardSegement instance = new WildCardSegement();

        public Object eval(JSONPath jsonpath, Object obj, Object obj1)
        {
            return jsonpath.getPropertyValues(obj1);
        }


        WildCardSegement()
        {
        }
    }


    private static int CACHE_SIZE = 1024;
    private static ConcurrentMap pathCache = new ConcurrentHashMap(128, 0.75F, 1);
    private ParserConfig parserConfig;
    private final String path;
    private Segement segments[];
    private SerializeConfig serializeConfig;

    public JSONPath(String s)
    {
        this(s, SerializeConfig.getGlobalInstance(), ParserConfig.getGlobalInstance());
    }

    public JSONPath(String s, SerializeConfig serializeconfig, ParserConfig parserconfig)
    {
        if (s == null || s.isEmpty())
        {
            throw new IllegalArgumentException();
        } else
        {
            path = s;
            serializeConfig = serializeconfig;
            parserConfig = parserconfig;
            return;
        }
    }

    public static transient void arrayAdd(Object obj, String s, Object aobj[])
    {
        compile(s).arrayAdd(obj, aobj);
    }

    public static JSONPath compile(String s)
    {
        JSONPath jsonpath1 = (JSONPath)pathCache.get(s);
        JSONPath jsonpath = jsonpath1;
        if (jsonpath1 == null)
        {
            JSONPath jsonpath2 = new JSONPath(s);
            jsonpath = jsonpath2;
            if (pathCache.size() < CACHE_SIZE)
            {
                pathCache.putIfAbsent(s, jsonpath2);
                jsonpath = (JSONPath)pathCache.get(s);
            }
        }
        return jsonpath;
    }

    public static boolean contains(Object obj, String s)
    {
        if (obj == null)
        {
            return false;
        } else
        {
            return compile(s).contains(obj);
        }
    }

    public static boolean containsValue(Object obj, String s, Object obj1)
    {
        return compile(s).containsValue(obj, obj1);
    }

    static boolean eq(Object obj, Object obj1)
    {
        boolean flag1 = false;
        boolean flag;
        if (obj == obj1)
        {
            flag = true;
        } else
        {
            flag = flag1;
            if (obj != null)
            {
                flag = flag1;
                if (obj1 != null)
                {
                    if (obj.getClass() == obj1.getClass())
                    {
                        return obj.equals(obj1);
                    }
                    if (obj instanceof Number)
                    {
                        flag = flag1;
                        if (obj1 instanceof Number)
                        {
                            return eqNotNull((Number)obj, (Number)obj1);
                        }
                    } else
                    {
                        return obj.equals(obj1);
                    }
                }
            }
        }
        return flag;
    }

    static boolean eqNotNull(Number number, Number number1)
    {
        Class class1;
        Class class2;
        boolean flag;
        boolean flag1;
        class1 = number.getClass();
        flag = isInt(class1);
        class2 = number.getClass();
        flag1 = isInt(class2);
        if (!flag || !flag1) goto _L2; else goto _L1
_L1:
        if (number.longValue() != number1.longValue()) goto _L4; else goto _L3
_L3:
        return true;
_L4:
        return false;
_L2:
        boolean flag2 = isDouble(class1);
        boolean flag3 = isDouble(class2);
        if (flag2 && flag3 || flag2 && flag || flag3 && flag)
        {
            if (number.doubleValue() != number1.doubleValue())
            {
                return false;
            }
        } else
        {
            return false;
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

    public static Object eval(Object obj, String s)
    {
        return compile(s).eval(obj);
    }

    protected static boolean isDouble(Class class1)
    {
        return class1 == java/lang/Float || class1 == java/lang/Double;
    }

    protected static boolean isInt(Class class1)
    {
        return class1 == java/lang/Byte || class1 == java/lang/Short || class1 == java/lang/Integer || class1 == java/lang/Long;
    }

    public static void set(Object obj, String s, Object obj1)
    {
        compile(s).set(obj, obj1);
    }

    public static int size(Object obj, String s)
    {
        s = compile(s);
        return s.evalSize(s.eval(obj));
    }

    public transient void arrayAdd(Object obj, Object aobj[])
    {
        boolean flag;
        int l;
        l = 0;
        flag = false;
        break MISSING_BLOCK_LABEL_6;
        Object obj1;
        Object obj2;
        while (true) 
        {
            do
            {
                return;
            } while (aobj == null || aobj.length == 0 || obj == null);
            init();
            obj2 = null;
            int i = 0;
            obj1 = obj;
            for (; i < segments.length; i++)
            {
                if (i == segments.length - 1)
                {
                    obj2 = obj1;
                }
                obj1 = segments[i].eval(this, obj, obj1);
            }

            if (obj1 == null)
            {
                throw new JSONPathException((new StringBuilder()).append("value not found in path ").append(path).toString());
            }
            if (!(obj1 instanceof Collection))
            {
                break;
            }
            obj = (Collection)obj1;
            l = aobj.length;
            i = ((flag) ? 1 : 0);
            while (i < l) 
            {
                ((Collection) (obj)).add(aobj[i]);
                i++;
            }
        }
        obj = obj1.getClass();
        if (((Class) (obj)).isArray())
        {
            int k = Array.getLength(obj1);
            obj = Array.newInstance(((Class) (obj)).getComponentType(), aobj.length + k);
            System.arraycopy(obj1, 0, obj, 0, k);
            for (int j = l; j < aobj.length; j++)
            {
                Array.set(obj, k + j, aobj[j]);
            }

        } else
        {
            throw new UnsupportedOperationException();
        }
        aobj = segments[segments.length - 1];
        if (aobj instanceof PropertySegement)
        {
            ((PropertySegement)aobj).setValue(this, obj2, obj);
            return;
        }
        if (aobj instanceof ArrayAccessSegement)
        {
            ((ArrayAccessSegement)aobj).setValue(this, obj2, obj);
            return;
        } else
        {
            throw new UnsupportedOperationException();
        }
    }

    public boolean contains(Object obj)
    {
        if (obj != null) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        init();
        int i = 0;
        Object obj1 = obj;
label0:
        do
        {
label1:
            {
                if (i >= segments.length)
                {
                    break label1;
                }
                obj1 = segments[i].eval(this, obj, obj1);
                if (obj1 == null)
                {
                    break label0;
                }
                i++;
            }
        } while (true);
        if (true) goto _L1; else goto _L3
_L3:
        return true;
    }

    public boolean containsValue(Object obj, Object obj1)
    {
        obj = eval(obj);
        if (obj == obj1)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (obj instanceof Iterable)
        {
            for (obj = ((Iterable)obj).iterator(); ((Iterator) (obj)).hasNext();)
            {
                if (eq(((Iterator) (obj)).next(), obj1))
                {
                    return true;
                }
            }

            return false;
        } else
        {
            return eq(obj, obj1);
        }
    }

    public Object eval(Object obj)
    {
        if (obj != null) goto _L2; else goto _L1
_L1:
        Object obj2 = null;
_L4:
        return obj2;
_L2:
        init();
        int i = 0;
        Object obj1 = obj;
        do
        {
            obj2 = obj1;
            if (i >= segments.length)
            {
                continue;
            }
            obj1 = segments[i].eval(this, obj, obj1);
            i++;
        } while (true);
        if (true) goto _L4; else goto _L3
_L3:
    }

    int evalSize(Object obj)
    {
        int i;
        int j;
        i = 0;
        j = 0;
        if (obj != null) goto _L2; else goto _L1
_L1:
        j = -1;
_L4:
        return j;
_L2:
        if (obj instanceof Collection)
        {
            return ((Collection)obj).size();
        }
        if (obj instanceof Object[])
        {
            return ((Object[])(Object[])obj).length;
        }
        if (obj.getClass().isArray())
        {
            return Array.getLength(obj);
        }
        if (!(obj instanceof Map))
        {
            break MISSING_BLOCK_LABEL_116;
        }
        obj = ((Map)obj).values().iterator();
        i = j;
_L5:
        j = i;
        if (!((Iterator) (obj)).hasNext()) goto _L4; else goto _L3
_L3:
        if (((Iterator) (obj)).next() != null)
        {
            i++;
        }
          goto _L5
        Object obj1 = getJavaBeanSerializer(obj.getClass());
        if (obj1 == null)
        {
            return -1;
        }
        int k;
        try
        {
            obj = ((JavaBeanSerializer) (obj1)).getFieldValues(obj);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            throw new JSONException((new StringBuilder()).append("evalSize error : ").append(path).toString(), ((Throwable) (obj)));
        }
        k = 0;
        j = i;
        if (k >= ((List) (obj)).size()) goto _L4; else goto _L6
_L6:
        obj1 = ((List) (obj)).get(k);
        j = i;
        if (obj1 != null)
        {
            j = i + 1;
        }
        k++;
        i = j;
        break MISSING_BLOCK_LABEL_140;
    }

    protected Object getArrayItem(Object obj, int i)
    {
        if (obj != null) goto _L2; else goto _L1
_L1:
        return null;
_L2:
        if (!(obj instanceof List))
        {
            break MISSING_BLOCK_LABEL_68;
        }
        obj = (List)obj;
        if (i < 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (i >= ((List) (obj)).size()) goto _L1; else goto _L3
_L3:
        return ((List) (obj)).get(i);
        if (Math.abs(i) > ((List) (obj)).size()) goto _L1; else goto _L4
_L4:
        return ((List) (obj)).get(((List) (obj)).size() + i);
        int j;
        if (!obj.getClass().isArray())
        {
            break MISSING_BLOCK_LABEL_114;
        }
        j = Array.getLength(obj);
        if (i < 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (i >= j) goto _L1; else goto _L5
_L5:
        return Array.get(obj, i);
        if (Math.abs(i) > j) goto _L1; else goto _L6
_L6:
        return Array.get(obj, j + i);
        throw new UnsupportedOperationException();
    }

    protected JavaBeanSerializer getJavaBeanSerializer(Class class1)
    {
        class1 = serializeConfig.getObjectWriter(class1);
        if (class1 instanceof JavaBeanSerializer)
        {
            return (JavaBeanSerializer)class1;
        }
        if (class1 instanceof ASMJavaBeanSerializer)
        {
            return ((ASMJavaBeanSerializer)class1).getJavaBeanSerializer();
        } else
        {
            return null;
        }
    }

    public String getPath()
    {
        return path;
    }

    protected Object getPropertyValue(Object obj, String s, boolean flag)
    {
        if (obj == null)
        {
            return null;
        }
        if (obj instanceof Map)
        {
            return ((Map)obj).get(s);
        }
        JavaBeanSerializer javabeanserializer = getJavaBeanSerializer(obj.getClass());
        if (javabeanserializer != null)
        {
            try
            {
                obj = javabeanserializer.getFieldValue(obj, s);
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                throw new JSONPathException((new StringBuilder()).append("jsonpath error, path ").append(path).append(", segement ").append(s).toString(), ((Throwable) (obj)));
            }
            return obj;
        }
        if (obj instanceof List)
        {
            obj = (List)obj;
            ArrayList arraylist = new ArrayList(((List) (obj)).size());
            for (int i = 0; i < ((List) (obj)).size(); i++)
            {
                arraylist.add(getPropertyValue(((List) (obj)).get(i), s, flag));
            }

            return arraylist;
        } else
        {
            throw new JSONPathException((new StringBuilder()).append("jsonpath error, path ").append(path).append(", segement ").append(s).toString());
        }
    }

    protected Collection getPropertyValues(Object obj)
    {
        JavaBeanSerializer javabeanserializer = getJavaBeanSerializer(obj.getClass());
        if (javabeanserializer != null)
        {
            try
            {
                obj = javabeanserializer.getFieldValues(obj);
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                throw new JSONPathException((new StringBuilder()).append("jsonpath error, path ").append(path).toString(), ((Throwable) (obj)));
            }
            return ((Collection) (obj));
        }
        if (obj instanceof Map)
        {
            return ((Map)obj).values();
        } else
        {
            throw new UnsupportedOperationException();
        }
    }

    protected void init()
    {
        if (segments != null)
        {
            return;
        }
        if ("*".equals(path))
        {
            segments = (new Segement[] {
                WildCardSegement.instance
            });
            return;
        } else
        {
            segments = (new JSONPathParser(path)).explain();
            return;
        }
    }

    public boolean set(Object obj, Object obj1)
    {
        if (obj != null) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        Object obj2;
        int i;
        init();
        i = 0;
        obj2 = obj;
_L4:
        if (i >= segments.length)
        {
            break MISSING_BLOCK_LABEL_133;
        }
        if (i != segments.length - 1)
        {
label0:
            {
                obj2 = segments[i].eval(this, obj, obj2);
                if (obj2 != null)
                {
                    break label0;
                }
                obj2 = null;
            }
        }
_L5:
        if (obj2 == null) goto _L1; else goto _L3
_L3:
        obj = segments[segments.length - 1];
        if (obj instanceof PropertySegement)
        {
            ((PropertySegement)obj).setValue(this, obj2, obj1);
            return true;
        }
        break MISSING_BLOCK_LABEL_107;
        i++;
          goto _L4
        if (obj instanceof ArrayAccessSegement)
        {
            return ((ArrayAccessSegement)obj).setValue(this, obj2, obj1);
        } else
        {
            throw new UnsupportedOperationException();
        }
        obj2 = null;
          goto _L5
    }

    public boolean setArrayItem(JSONPath jsonpath, Object obj, int i, Object obj1)
    {
        if (!(obj instanceof List)) goto _L2; else goto _L1
_L1:
        jsonpath = (List)obj;
        if (i < 0) goto _L4; else goto _L3
_L3:
        jsonpath.set(i, obj1);
_L6:
        return true;
_L4:
        jsonpath.set(jsonpath.size() + i, obj1);
        return true;
_L2:
        int j;
        if (!obj.getClass().isArray())
        {
            break MISSING_BLOCK_LABEL_103;
        }
        j = Array.getLength(obj);
        if (i < 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (i >= j) goto _L6; else goto _L5
_L5:
        Array.set(obj, i, obj1);
        return true;
        if (Math.abs(i) > j) goto _L6; else goto _L7
_L7:
        Array.set(obj, j + i, obj1);
        return true;
        throw new UnsupportedOperationException();
    }

    protected boolean setPropertyValue(Object obj, String s, Object obj1)
    {
        if (obj instanceof Map)
        {
            ((Map)obj).put(s, obj1);
            return true;
        }
        if (obj instanceof List)
        {
            obj = ((List)obj).iterator();
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break;
                }
                Object obj2 = ((Iterator) (obj)).next();
                if (obj2 != null)
                {
                    setPropertyValue(obj2, s, obj1);
                }
            } while (true);
            return true;
        }
        Object obj3 = parserConfig.getDeserializer(obj.getClass());
        if (obj3 instanceof JavaBeanDeserializer)
        {
            obj3 = (JavaBeanDeserializer)obj3;
        } else
        if (obj3 instanceof ASMJavaBeanDeserializer)
        {
            obj3 = ((ASMJavaBeanDeserializer)obj3).getInnterSerializer();
        } else
        {
            obj3 = null;
        }
        if (obj3 != null)
        {
            s = ((JavaBeanDeserializer) (obj3)).getFieldDeserializer(s);
            if (s == null)
            {
                return false;
            } else
            {
                s.setValue(obj, obj1);
                return true;
            }
        } else
        {
            throw new UnsupportedOperationException();
        }
    }

    public int size(Object obj)
    {
        if (obj == null)
        {
            return -1;
        }
        init();
        int i = 0;
        Object obj1 = obj;
        for (; i < segments.length; i++)
        {
            obj1 = segments[i].eval(this, obj, obj1);
        }

        return evalSize(obj1);
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer.write(path);
    }

}
