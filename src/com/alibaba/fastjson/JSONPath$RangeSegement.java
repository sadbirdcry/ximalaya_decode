// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;

import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.alibaba.fastjson:
//            JSONPath

static class step
    implements step
{

    private final int end;
    private final int start;
    private final int step;

    public Object eval(JSONPath jsonpath, Object obj, Object obj1)
    {
        int k = nstance.val(jsonpath, obj, obj1).intValue();
        int i;
        int j;
        if (start >= 0)
        {
            i = start;
        } else
        {
            i = start + k;
        }
        if (end >= 0)
        {
            j = end;
        } else
        {
            j = end + k;
        }
        obj = new ArrayList((j - i) / step + 1);
        for (; i <= j && i < k; i += step)
        {
            ((List) (obj)).add(jsonpath.getArrayItem(obj1, i));
        }

        return obj;
    }

    public (int i, int j, int k)
    {
        start = i;
        end = j;
        step = k;
    }
}
