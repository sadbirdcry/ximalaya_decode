// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class TypeReference
{

    public static final Type LIST_STRING = (new _cls1()).getType();
    private final Type type = ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    protected TypeReference()
    {
    }

    public Type getType()
    {
        return type;
    }


    private class _cls1 extends TypeReference
    {

        _cls1()
        {
        }
    }

}
