// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.JSONToken;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer

public class StackTraceElementDeserializer
    implements ObjectDeserializer
{

    public static final StackTraceElementDeserializer instance = new StackTraceElementDeserializer();

    public StackTraceElementDeserializer()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        Type type1;
        Object obj1;
        String s1;
        JSONLexer jsonlexer;
        int j;
        jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() == 8)
        {
            jsonlexer.nextToken();
            return null;
        }
        if (jsonlexer.token() != 12 && jsonlexer.token() != 16)
        {
            throw new JSONException((new StringBuilder()).append("syntax error: ").append(JSONToken.name(jsonlexer.token())).toString());
        }
        s1 = null;
        obj1 = null;
        type1 = null;
        j = 0;
_L2:
        String s;
        int i;
        type = jsonlexer.scanSymbol(defaultjsonparser.getSymbolTable());
        if (type != null)
        {
            break; /* Loop/switch isn't completed */
        }
        if (jsonlexer.token() != 13)
        {
            continue; /* Loop/switch isn't completed */
        }
        jsonlexer.nextToken(16);
        s = s1;
        obj = obj1;
        type = type1;
        i = j;
_L4:
        return new StackTraceElement(s, ((String) (obj)), type, i);
        if (jsonlexer.token() == 16 && jsonlexer.isEnabled(Feature.AllowArbitraryCommas)) goto _L2; else goto _L1
_L1:
        jsonlexer.nextTokenWithColon(4);
        if ("className".equals(type))
        {
            if (jsonlexer.token() == 8)
            {
                s = null;
                obj = obj1;
                type = type1;
                i = j;
            } else
            if (jsonlexer.token() == 4)
            {
                s = jsonlexer.stringVal();
                i = j;
                type = type1;
                obj = obj1;
            } else
            {
                throw new JSONException("syntax error");
            }
        } else
        if ("methodName".equals(type))
        {
            if (jsonlexer.token() == 8)
            {
                obj = null;
                i = j;
                type = type1;
                s = s1;
            } else
            if (jsonlexer.token() == 4)
            {
                obj = jsonlexer.stringVal();
                i = j;
                type = type1;
                s = s1;
            } else
            {
                throw new JSONException("syntax error");
            }
        } else
        if ("fileName".equals(type))
        {
            if (jsonlexer.token() == 8)
            {
                type = null;
                i = j;
                obj = obj1;
                s = s1;
            } else
            if (jsonlexer.token() == 4)
            {
                type = jsonlexer.stringVal();
                i = j;
                obj = obj1;
                s = s1;
            } else
            {
                throw new JSONException("syntax error");
            }
        } else
        if ("lineNumber".equals(type))
        {
            if (jsonlexer.token() == 8)
            {
                i = 0;
                type = type1;
                obj = obj1;
                s = s1;
            } else
            if (jsonlexer.token() == 2)
            {
                i = jsonlexer.intValue();
                type = type1;
                obj = obj1;
                s = s1;
            } else
            {
                throw new JSONException("syntax error");
            }
        } else
        if ("nativeMethod".equals(type))
        {
            if (jsonlexer.token() == 8)
            {
                jsonlexer.nextToken(16);
                i = j;
                type = type1;
                obj = obj1;
                s = s1;
            } else
            if (jsonlexer.token() == 6)
            {
                jsonlexer.nextToken(16);
                i = j;
                type = type1;
                obj = obj1;
                s = s1;
            } else
            if (jsonlexer.token() == 7)
            {
                jsonlexer.nextToken(16);
                i = j;
                type = type1;
                obj = obj1;
                s = s1;
            } else
            {
                throw new JSONException("syntax error");
            }
        } else
        {
label0:
            {
                if (type != JSON.DEFAULT_TYPE_KEY)
                {
                    break label0;
                }
                if (jsonlexer.token() == 4)
                {
                    String s2 = jsonlexer.stringVal();
                    i = j;
                    type = type1;
                    obj = obj1;
                    s = s1;
                    if (!s2.equals("java.lang.StackTraceElement"))
                    {
                        throw new JSONException((new StringBuilder()).append("syntax error : ").append(s2).toString());
                    }
                } else
                {
                    i = j;
                    type = type1;
                    obj = obj1;
                    s = s1;
                    if (jsonlexer.token() != 8)
                    {
                        throw new JSONException("syntax error");
                    }
                }
            }
        }
        j = i;
        type1 = type;
        obj1 = obj;
        s1 = s;
        if (jsonlexer.token() != 13) goto _L2; else goto _L3
_L3:
        jsonlexer.nextToken(16);
          goto _L4
        throw new JSONException((new StringBuilder()).append("syntax error : ").append(type).toString());
          goto _L4
    }

    public int getFastMatchToken()
    {
        return 12;
    }

}
