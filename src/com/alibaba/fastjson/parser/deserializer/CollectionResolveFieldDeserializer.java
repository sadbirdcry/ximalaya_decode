// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            FieldDeserializer

public final class CollectionResolveFieldDeserializer extends FieldDeserializer
{

    private final Collection collection;

    public CollectionResolveFieldDeserializer(DefaultJSONParser defaultjsonparser, Collection collection1)
    {
        super(null, null);
        collection = collection1;
    }

    public void parseField(DefaultJSONParser defaultjsonparser, Object obj, Type type, Map map)
    {
    }

    public void setValue(Object obj, Object obj1)
    {
        collection.add(obj1);
    }
}
