// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.util.FieldInfo;
import com.alibaba.fastjson.util.TypeUtils;
import java.lang.reflect.Type;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            FieldDeserializer, ObjectDeserializer

public class LongFieldDeserializer extends FieldDeserializer
{

    private final ObjectDeserializer fieldValueDeserilizer;

    public LongFieldDeserializer(ParserConfig parserconfig, Class class1, FieldInfo fieldinfo)
    {
        super(class1, fieldinfo);
        fieldValueDeserilizer = parserconfig.getDeserializer(fieldinfo);
    }

    public int getFastMatchToken()
    {
        return fieldValueDeserilizer.getFastMatchToken();
    }

    public void parseField(DefaultJSONParser defaultjsonparser, Object obj, Type type, Map map)
    {
        type = defaultjsonparser.getLexer();
        if (type.token() != 2) goto _L2; else goto _L1
_L1:
        long l;
        l = type.longValue();
        type.nextToken(16);
        if (obj != null) goto _L4; else goto _L3
_L3:
        map.put(fieldInfo.getName(), Long.valueOf(l));
_L6:
        return;
_L4:
        setValue(obj, l);
        return;
_L2:
        if (type.token() == 8)
        {
            defaultjsonparser = null;
            type.nextToken(16);
        } else
        {
            defaultjsonparser = TypeUtils.castToLong(defaultjsonparser.parse());
        }
        if (defaultjsonparser != null || getFieldClass() != Long.TYPE)
        {
            if (obj == null)
            {
                map.put(fieldInfo.getName(), defaultjsonparser);
                return;
            } else
            {
                setValue(obj, defaultjsonparser);
                return;
            }
        }
        if (true) goto _L6; else goto _L5
_L5:
    }
}
