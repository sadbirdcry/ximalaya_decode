// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.JSONScanner;
import com.alibaba.fastjson.util.TypeUtils;
import java.lang.reflect.Type;
import java.util.Calendar;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer

public abstract class AbstractDateDeserializer
    implements ObjectDeserializer
{

    public AbstractDateDeserializer()
    {
    }

    protected abstract Object cast(DefaultJSONParser defaultjsonparser, Type type, Object obj, Object obj1);

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        JSONLexer jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() != 2) goto _L2; else goto _L1
_L1:
        Object obj1;
        Object obj2;
        obj1 = Long.valueOf(jsonlexer.longValue());
        jsonlexer.nextToken(16);
        obj2 = type;
_L4:
        return cast(defaultjsonparser, ((Type) (obj2)), obj, obj1);
_L2:
        if (jsonlexer.token() == 4)
        {
            String s = jsonlexer.stringVal();
            jsonlexer.nextToken(16);
            obj1 = s;
            obj2 = type;
            if (jsonlexer.isEnabled(Feature.AllowISO8601DateFormat))
            {
                obj2 = new JSONScanner(s);
                obj1 = s;
                if (((JSONScanner) (obj2)).scanISO8601DateIfMatch())
                {
                    obj1 = ((JSONScanner) (obj2)).getCalendar().getTime();
                }
                ((JSONScanner) (obj2)).close();
                obj2 = type;
            }
        } else
        if (jsonlexer.token() == 8)
        {
            jsonlexer.nextToken();
            obj1 = null;
            obj2 = type;
        } else
        if (jsonlexer.token() == 12)
        {
            jsonlexer.nextToken();
            if (jsonlexer.token() == 4)
            {
                obj1 = jsonlexer.stringVal();
                obj2 = type;
                if (JSON.DEFAULT_TYPE_KEY.equals(obj1))
                {
                    jsonlexer.nextToken();
                    defaultjsonparser.accept(17);
                    obj1 = TypeUtils.loadClass(jsonlexer.stringVal());
                    if (obj1 != null)
                    {
                        type = ((Type) (obj1));
                    }
                    defaultjsonparser.accept(4);
                    defaultjsonparser.accept(16);
                    obj2 = type;
                }
                jsonlexer.nextTokenWithColon(2);
                if (jsonlexer.token() == 2)
                {
                    long l = jsonlexer.longValue();
                    jsonlexer.nextToken();
                    obj1 = Long.valueOf(l);
                    defaultjsonparser.accept(13);
                } else
                {
                    throw new JSONException((new StringBuilder()).append("syntax error : ").append(jsonlexer.tokenName()).toString());
                }
            } else
            {
                throw new JSONException("syntax error");
            }
        } else
        if (defaultjsonparser.getResolveStatus() == 2)
        {
            defaultjsonparser.setResolveStatus(0);
            defaultjsonparser.accept(16);
            if (jsonlexer.token() == 4)
            {
                if (!"val".equals(jsonlexer.stringVal()))
                {
                    throw new JSONException("syntax error");
                }
                jsonlexer.nextToken();
                defaultjsonparser.accept(17);
                obj1 = defaultjsonparser.parse();
                defaultjsonparser.accept(13);
                obj2 = type;
            } else
            {
                throw new JSONException("syntax error");
            }
        } else
        {
            obj1 = defaultjsonparser.parse();
            obj2 = type;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }
}
