// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.util.TypeUtils;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer

public class JavaObjectDeserializer
    implements ObjectDeserializer
{

    public static final JavaObjectDeserializer instance = new JavaObjectDeserializer();

    public JavaObjectDeserializer()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        if (type instanceof GenericArrayType)
        {
            obj = ((GenericArrayType)type).getGenericComponentType();
            type = ((Type) (obj));
            if (obj instanceof TypeVariable)
            {
                type = ((TypeVariable)obj).getBounds()[0];
            }
            obj = new ArrayList();
            defaultjsonparser.parseArray(type, ((java.util.Collection) (obj)));
            if (type instanceof Class)
            {
                type = (Class)type;
                if (type == Boolean.TYPE)
                {
                    return TypeUtils.cast(obj, [Z, defaultjsonparser.getConfig());
                }
                if (type == Short.TYPE)
                {
                    return TypeUtils.cast(obj, [S, defaultjsonparser.getConfig());
                }
                if (type == Integer.TYPE)
                {
                    return TypeUtils.cast(obj, [I, defaultjsonparser.getConfig());
                }
                if (type == Long.TYPE)
                {
                    return TypeUtils.cast(obj, [J, defaultjsonparser.getConfig());
                }
                if (type == Float.TYPE)
                {
                    return TypeUtils.cast(obj, [F, defaultjsonparser.getConfig());
                }
                if (type == Double.TYPE)
                {
                    return TypeUtils.cast(obj, [D, defaultjsonparser.getConfig());
                } else
                {
                    defaultjsonparser = ((DefaultJSONParser) ((Object[])(Object[])Array.newInstance(type, ((List) (obj)).size())));
                    ((List) (obj)).toArray(defaultjsonparser);
                    return defaultjsonparser;
                }
            } else
            {
                return ((Object) (((List) (obj)).toArray()));
            }
        } else
        {
            return defaultjsonparser.parse(obj);
        }
    }

    public int getFastMatchToken()
    {
        return 12;
    }

}
