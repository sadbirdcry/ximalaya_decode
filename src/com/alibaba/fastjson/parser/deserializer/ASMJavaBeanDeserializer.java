// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.util.FieldInfo;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer, FieldDeserializer, JavaBeanDeserializer

public abstract class ASMJavaBeanDeserializer
    implements ObjectDeserializer
{
    public final class InnerJavaBeanDeserializer extends JavaBeanDeserializer
    {

        final ASMJavaBeanDeserializer this$0;

        public FieldDeserializer createFieldDeserializer(ParserConfig parserconfig, Class class1, FieldInfo fieldinfo)
        {
            return ASMJavaBeanDeserializer.this.createFieldDeserializer(parserconfig, class1, fieldinfo);
        }

        public boolean parseField(DefaultJSONParser defaultjsonparser, String s, Object obj, Type type, Map map)
        {
            return ASMJavaBeanDeserializer.this.parseField(defaultjsonparser, s, obj, type, map);
        }

        private InnerJavaBeanDeserializer(ParserConfig parserconfig, Class class1)
        {
            this$0 = ASMJavaBeanDeserializer.this;
            super(parserconfig, class1);
        }

        InnerJavaBeanDeserializer(ParserConfig parserconfig, Class class1, _cls1 _pcls1)
        {
            this(parserconfig, class1);
        }
    }


    protected InnerJavaBeanDeserializer serializer;

    public ASMJavaBeanDeserializer(ParserConfig parserconfig, Class class1)
    {
        serializer = new InnerJavaBeanDeserializer(parserconfig, class1, null);
        serializer.getFieldDeserializerMap();
    }

    public FieldDeserializer createFieldDeserializer(ParserConfig parserconfig, Class class1, FieldInfo fieldinfo)
    {
        return parserconfig.createFieldDeserializer(parserconfig, class1, fieldinfo);
    }

    public Object createInstance(DefaultJSONParser defaultjsonparser)
    {
        return serializer.createInstance(defaultjsonparser, serializer.getClazz());
    }

    public abstract Object createInstance(DefaultJSONParser defaultjsonparser, Type type);

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        return serializer.deserialze(defaultjsonparser, type, obj);
    }

    public int getFastMatchToken()
    {
        return serializer.getFastMatchToken();
    }

    public FieldDeserializer getFieldDeserializer(String s)
    {
        return (FieldDeserializer)serializer.getFieldDeserializerMap().get(s);
    }

    public Type getFieldType(String s)
    {
        return ((FieldDeserializer)serializer.getFieldDeserializerMap().get(s)).getFieldType();
    }

    public InnerJavaBeanDeserializer getInnterSerializer()
    {
        return serializer;
    }

    public boolean isSupportArrayToBean(JSONLexer jsonlexer)
    {
        return serializer.isSupportArrayToBean(jsonlexer);
    }

    public boolean parseField(DefaultJSONParser defaultjsonparser, String s, Object obj, Type type, Map map)
    {
        Object obj1;
        JSONLexer jsonlexer;
label0:
        {
            jsonlexer = defaultjsonparser.getLexer();
            Object obj2 = serializer.getFieldDeserializerMap();
            FieldDeserializer fielddeserializer = (FieldDeserializer)((Map) (obj2)).get(s);
            obj1 = fielddeserializer;
            if (fielddeserializer != null)
            {
                break label0;
            }
            obj2 = ((Map) (obj2)).entrySet().iterator();
            do
            {
                obj1 = fielddeserializer;
                if (!((Iterator) (obj2)).hasNext())
                {
                    break label0;
                }
                obj1 = (java.util.Map.Entry)((Iterator) (obj2)).next();
            } while (!((String)((java.util.Map.Entry) (obj1)).getKey()).equalsIgnoreCase(s));
            obj1 = (FieldDeserializer)((java.util.Map.Entry) (obj1)).getValue();
        }
        if (obj1 == null)
        {
            serializer.parseExtra(defaultjsonparser, obj, s);
            return false;
        } else
        {
            jsonlexer.nextTokenWithColon(((FieldDeserializer) (obj1)).getFastMatchToken());
            ((FieldDeserializer) (obj1)).parseField(defaultjsonparser, obj, type, map);
            return true;
        }
    }

    public Object parseRest(DefaultJSONParser defaultjsonparser, Type type, Object obj, Object obj1)
    {
        return serializer.deserialze(defaultjsonparser, type, obj, obj1);
    }
}
