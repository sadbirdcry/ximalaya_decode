// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONScanner;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            AbstractDateDeserializer, ObjectDeserializer

public class DateDeserializer extends AbstractDateDeserializer
    implements ObjectDeserializer
{

    public static final DateDeserializer instance = new DateDeserializer();

    public DateDeserializer()
    {
    }

    protected Object cast(DefaultJSONParser defaultjsonparser, Type type, Object obj, Object obj1)
    {
        if (obj1 != null) goto _L2; else goto _L1
_L1:
        obj = null;
_L4:
        return obj;
_L2:
        obj = obj1;
        if (obj1 instanceof Date) goto _L4; else goto _L3
_L3:
        if (obj1 instanceof Number)
        {
            return new Date(((Number)obj1).longValue());
        }
        if (!(obj1 instanceof String))
        {
            break MISSING_BLOCK_LABEL_156;
        }
        obj = (String)obj1;
        if (((String) (obj)).length() == 0)
        {
            return null;
        }
        obj1 = new JSONScanner(((String) (obj)));
        if (!((JSONScanner) (obj1)).scanISO8601DateIfMatch(false))
        {
            break MISSING_BLOCK_LABEL_117;
        }
        defaultjsonparser = ((JSONScanner) (obj1)).getCalendar();
        if (type == java/util/Calendar)
        {
            ((JSONScanner) (obj1)).close();
            return defaultjsonparser;
        }
        defaultjsonparser = defaultjsonparser.getTime();
        ((JSONScanner) (obj1)).close();
        return defaultjsonparser;
        ((JSONScanner) (obj1)).close();
        defaultjsonparser = defaultjsonparser.getDateFormat();
        try
        {
            defaultjsonparser = defaultjsonparser.parse(((String) (obj)));
        }
        // Misplaced declaration of an exception variable
        catch (DefaultJSONParser defaultjsonparser)
        {
            return new Date(Long.parseLong(((String) (obj))));
        }
        return defaultjsonparser;
        defaultjsonparser;
        ((JSONScanner) (obj1)).close();
        throw defaultjsonparser;
        throw new JSONException("parse error");
    }

    public int getFastMatchToken()
    {
        return 2;
    }

}
