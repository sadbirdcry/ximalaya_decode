// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.JSONToken;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.util.FieldInfo;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            FieldDeserializer, ObjectDeserializer

public class ArrayListTypeFieldDeserializer extends FieldDeserializer
{

    private ObjectDeserializer deserializer;
    private int itemFastMatchToken;
    private final Type itemType;

    public ArrayListTypeFieldDeserializer(ParserConfig parserconfig, Class class1, FieldInfo fieldinfo)
    {
        super(class1, fieldinfo);
        if (getFieldType() instanceof ParameterizedType)
        {
            itemType = ((ParameterizedType)getFieldType()).getActualTypeArguments()[0];
            return;
        } else
        {
            itemType = java/lang/Object;
            return;
        }
    }

    public int getFastMatchToken()
    {
        return 14;
    }

    public final void parseArray(DefaultJSONParser defaultjsonparser, Type type, Collection collection)
    {
        Object obj;
        Object obj1;
        Object obj2;
        obj1 = itemType;
        obj2 = deserializer;
        obj = obj1;
        if (!(obj1 instanceof TypeVariable)) goto _L2; else goto _L1
_L1:
        obj = obj1;
        if (!(type instanceof ParameterizedType)) goto _L2; else goto _L3
_L3:
        TypeVariable typevariable = (TypeVariable)obj1;
        ParameterizedType parameterizedtype = (ParameterizedType)type;
        int i;
        int j;
        if (parameterizedtype.getRawType() instanceof Class)
        {
            obj = (Class)parameterizedtype.getRawType();
        } else
        {
            obj = null;
        }
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_446;
        }
        j = ((Class) (obj)).getTypeParameters().length;
        i = 0;
        if (i >= j)
        {
            break MISSING_BLOCK_LABEL_446;
        }
        if (!((Class) (obj)).getTypeParameters()[i].getName().equals(typevariable.getName())) goto _L5; else goto _L4
_L4:
        obj = obj1;
        if (i != -1)
        {
label0:
            {
                obj1 = parameterizedtype.getActualTypeArguments()[i];
                obj = obj1;
                if (!obj1.equals(itemType))
                {
                    obj2 = defaultjsonparser.getConfig().getDeserializer(((Type) (obj1)));
                    obj = obj1;
                    obj1 = obj2;
                    break label0;
                }
            }
        }
          goto _L2
_L5:
        i++;
        break MISSING_BLOCK_LABEL_89;
_L7:
        obj2 = defaultjsonparser.getLexer();
        if (((JSONLexer) (obj2)).token() != 14)
        {
            collection = (new StringBuilder()).append("exepct '[', but ").append(JSONToken.name(((JSONLexer) (obj2)).token())).toString();
            defaultjsonparser = collection;
            if (type != null)
            {
                defaultjsonparser = (new StringBuilder()).append(collection).append(", type : ").append(type).toString();
            }
            throw new JSONException(defaultjsonparser);
        }
        type = ((Type) (obj1));
        if (obj1 == null)
        {
            type = defaultjsonparser.getConfig().getDeserializer(((Type) (obj)));
            deserializer = type;
            itemFastMatchToken = deserializer.getFastMatchToken();
        }
        ((JSONLexer) (obj2)).nextToken(itemFastMatchToken);
        i = 0;
        do
        {
            if (((JSONLexer) (obj2)).isEnabled(Feature.AllowArbitraryCommas))
            {
                for (; ((JSONLexer) (obj2)).token() == 16; ((JSONLexer) (obj2)).nextToken()) { }
            }
            if (((JSONLexer) (obj2)).token() == 15)
            {
                ((JSONLexer) (obj2)).nextToken(16);
                return;
            }
            collection.add(type.deserialze(defaultjsonparser, ((Type) (obj)), Integer.valueOf(i)));
            defaultjsonparser.checkListResolve(collection);
            if (((JSONLexer) (obj2)).token() == 16)
            {
                ((JSONLexer) (obj2)).nextToken(itemFastMatchToken);
            }
            i++;
        } while (true);
_L2:
        obj1 = obj2;
        if (true) goto _L7; else goto _L6
_L6:
        i = -1;
        if (true) goto _L4; else goto _L8
_L8:
    }

    public void parseField(DefaultJSONParser defaultjsonparser, Object obj, Type type, Map map)
    {
        if (defaultjsonparser.getLexer().token() == 8)
        {
            setValue(obj, null);
            return;
        }
        ArrayList arraylist = new ArrayList();
        com.alibaba.fastjson.parser.ParseContext parsecontext = defaultjsonparser.getContext();
        defaultjsonparser.setContext(parsecontext, obj, fieldInfo.getName());
        parseArray(defaultjsonparser, type, arraylist);
        defaultjsonparser.setContext(parsecontext);
        if (obj == null)
        {
            map.put(fieldInfo.getName(), arraylist);
            return;
        } else
        {
            setValue(obj, arraylist);
            return;
        }
    }
}
