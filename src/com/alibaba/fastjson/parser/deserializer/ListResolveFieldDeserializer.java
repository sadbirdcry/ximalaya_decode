// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.util.TypeUtils;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            FieldDeserializer

public final class ListResolveFieldDeserializer extends FieldDeserializer
{

    private final int index;
    private final List list;
    private final DefaultJSONParser parser;

    public ListResolveFieldDeserializer(DefaultJSONParser defaultjsonparser, List list1, int i)
    {
        super(null, null);
        parser = defaultjsonparser;
        index = i;
        list = list1;
    }

    public void parseField(DefaultJSONParser defaultjsonparser, Object obj, Type type, Map map)
    {
    }

    public void setValue(Object obj, Object obj1)
    {
        list.set(index, obj1);
        if (list instanceof JSONArray)
        {
            JSONArray jsonarray = (JSONArray)list;
            Object obj2 = jsonarray.getRelatedArray();
            if (obj2 != null && Array.getLength(obj2) > index)
            {
                obj = obj1;
                if (jsonarray.getComponentType() != null)
                {
                    obj = TypeUtils.cast(obj1, jsonarray.getComponentType(), parser.getConfig());
                }
                Array.set(obj2, index, obj);
            }
        }
    }
}
