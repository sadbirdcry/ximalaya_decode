// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.JSONScanner;
import java.lang.reflect.Type;
import java.sql.Time;
import java.util.Calendar;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer

public class TimeDeserializer
    implements ObjectDeserializer
{

    public static final TimeDeserializer instance = new TimeDeserializer();

    public TimeDeserializer()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        type = defaultjsonparser.getLexer();
        if (type.token() == 16)
        {
            type.nextToken(4);
            if (type.token() != 4)
            {
                throw new JSONException("syntax error");
            }
            type.nextTokenWithColon(2);
            if (type.token() != 2)
            {
                throw new JSONException("syntax error");
            }
            long l = type.longValue();
            type.nextToken(13);
            if (type.token() != 13)
            {
                throw new JSONException("syntax error");
            }
            type.nextToken(16);
            defaultjsonparser = new Time(l);
        } else
        {
            type = ((Type) (defaultjsonparser.parse()));
            if (type == null)
            {
                return null;
            }
            defaultjsonparser = type;
            if (!(type instanceof Time))
            {
                if (type instanceof Number)
                {
                    return new Time(((Number)type).longValue());
                }
                if (type instanceof String)
                {
                    defaultjsonparser = (String)type;
                    if (defaultjsonparser.length() == 0)
                    {
                        return null;
                    }
                    type = new JSONScanner(defaultjsonparser);
                    long l1;
                    if (type.scanISO8601DateIfMatch())
                    {
                        l1 = type.getCalendar().getTimeInMillis();
                    } else
                    {
                        l1 = Long.parseLong(defaultjsonparser);
                    }
                    type.close();
                    return new Time(l1);
                } else
                {
                    throw new JSONException("parse error");
                }
            }
        }
        return defaultjsonparser;
    }

    public int getFastMatchToken()
    {
        return 2;
    }

}
