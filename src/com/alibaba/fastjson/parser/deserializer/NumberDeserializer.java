// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.util.TypeUtils;
import java.lang.reflect.Type;
import java.math.BigDecimal;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer

public class NumberDeserializer
    implements ObjectDeserializer
{

    public static final NumberDeserializer instance = new NumberDeserializer();

    public NumberDeserializer()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        JSONLexer jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() != 2) goto _L2; else goto _L1
_L1:
        if (type != Double.TYPE && type != java/lang/Double) goto _L4; else goto _L3
_L3:
        defaultjsonparser = jsonlexer.numberString();
        jsonlexer.nextToken(16);
        defaultjsonparser = Double.valueOf(Double.parseDouble(defaultjsonparser));
_L6:
        return defaultjsonparser;
_L4:
        long l = jsonlexer.longValue();
        jsonlexer.nextToken(16);
        if (type == Short.TYPE || type == java/lang/Short)
        {
            return Short.valueOf((short)(int)l);
        }
        if (type == Byte.TYPE || type == java/lang/Byte)
        {
            return Byte.valueOf((byte)(int)l);
        }
        if (l >= 0xffffffff80000000L && l <= 0x7fffffffL)
        {
            return Integer.valueOf((int)l);
        } else
        {
            return Long.valueOf(l);
        }
_L2:
        if (jsonlexer.token() != 3)
        {
            break MISSING_BLOCK_LABEL_258;
        }
        if (type == Double.TYPE || type == java/lang/Double)
        {
            defaultjsonparser = jsonlexer.numberString();
            jsonlexer.nextToken(16);
            return Double.valueOf(Double.parseDouble(defaultjsonparser));
        }
        obj = jsonlexer.decimalValue();
        jsonlexer.nextToken(16);
        if (type == Short.TYPE || type == java/lang/Short)
        {
            return Short.valueOf(((BigDecimal) (obj)).shortValue());
        }
        if (type == Byte.TYPE)
        {
            break; /* Loop/switch isn't completed */
        }
        defaultjsonparser = ((DefaultJSONParser) (obj));
        if (type != java/lang/Byte) goto _L6; else goto _L5
_L5:
        return Byte.valueOf(((BigDecimal) (obj)).byteValue());
        defaultjsonparser = ((DefaultJSONParser) (defaultjsonparser.parse()));
        if (defaultjsonparser == null)
        {
            return null;
        }
        if (type == Double.TYPE || type == java/lang/Double)
        {
            return TypeUtils.castToDouble(defaultjsonparser);
        }
        if (type == Short.TYPE || type == java/lang/Short)
        {
            return TypeUtils.castToShort(defaultjsonparser);
        }
        if (type == Byte.TYPE || type == java/lang/Byte)
        {
            return TypeUtils.castToByte(defaultjsonparser);
        } else
        {
            return TypeUtils.castToBigDecimal(defaultjsonparser);
        }
    }

    public int getFastMatchToken()
    {
        return 2;
    }

}
