// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.util.FieldInfo;
import java.lang.reflect.Type;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            JavaBeanDeserializer, ASMJavaBeanDeserializer, FieldDeserializer

public final class <init> extends JavaBeanDeserializer
{

    final ASMJavaBeanDeserializer this$0;

    public FieldDeserializer createFieldDeserializer(ParserConfig parserconfig, Class class1, FieldInfo fieldinfo)
    {
        return ASMJavaBeanDeserializer.this.createFieldDeserializer(parserconfig, class1, fieldinfo);
    }

    public boolean parseField(DefaultJSONParser defaultjsonparser, String s, Object obj, Type type, Map map)
    {
        return ASMJavaBeanDeserializer.this.parseField(defaultjsonparser, s, obj, type, map);
    }

    private (ParserConfig parserconfig, Class class1)
    {
        this$0 = ASMJavaBeanDeserializer.this;
        super(parserconfig, class1);
    }

    this._cls0(ParserConfig parserconfig, Class class1, this._cls0 _pcls0)
    {
        this(parserconfig, class1);
    }
}
