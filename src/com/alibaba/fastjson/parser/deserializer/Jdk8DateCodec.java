// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.alibaba.fastjson.serializer.SerializeWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer

public class Jdk8DateCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final Jdk8DateCodec instance = new Jdk8DateCodec();

    public Jdk8DateCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        defaultjsonparser = defaultjsonparser.getLexer();
        if (defaultjsonparser.token() == 4)
        {
            obj = defaultjsonparser.stringVal();
            defaultjsonparser.nextToken();
            if (type == java/time/LocalDateTime)
            {
                return LocalDateTime.parse(((CharSequence) (obj)));
            }
            if (type == java/time/LocalDate)
            {
                return LocalDate.parse(((CharSequence) (obj)));
            }
            if (type == java/time/LocalTime)
            {
                return LocalTime.parse(((CharSequence) (obj)));
            }
            if (type == java/time/ZonedDateTime)
            {
                return ZonedDateTime.parse(((CharSequence) (obj)));
            }
            if (type == java/time/OffsetDateTime)
            {
                return OffsetDateTime.parse(((CharSequence) (obj)));
            }
            if (type == java/time/OffsetTime)
            {
                return OffsetTime.parse(((CharSequence) (obj)));
            }
            if (type == java/time/ZoneId)
            {
                return ZoneId.of(((String) (obj)));
            }
            if (type == java/time/Period)
            {
                return Period.parse(((CharSequence) (obj)));
            }
            if (type == java/time/Duration)
            {
                return Duration.parse(((CharSequence) (obj)));
            }
            if (type == java/time/Instant)
            {
                return Instant.parse(((CharSequence) (obj)));
            } else
            {
                return null;
            }
        } else
        {
            throw new UnsupportedOperationException();
        }
    }

    public int getFastMatchToken()
    {
        return 4;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer = jsonserializer.getWriter();
        if (obj == null)
        {
            jsonserializer.writeNull();
            return;
        } else
        {
            jsonserializer.writeString(obj.toString());
            return;
        }
    }

}
