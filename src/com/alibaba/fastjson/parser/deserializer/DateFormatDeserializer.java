// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            AbstractDateDeserializer, ObjectDeserializer

public class DateFormatDeserializer extends AbstractDateDeserializer
    implements ObjectDeserializer
{

    public static final DateFormatDeserializer instance = new DateFormatDeserializer();

    public DateFormatDeserializer()
    {
    }

    protected Object cast(DefaultJSONParser defaultjsonparser, Type type, Object obj, Object obj1)
    {
        if (obj1 != null)
        {
            if (obj1 instanceof String)
            {
                if ((defaultjsonparser = (String)obj1).length() != 0)
                {
                    return new SimpleDateFormat(defaultjsonparser);
                }
            } else
            {
                throw new JSONException("parse error");
            }
        }
        return null;
    }

    public int getFastMatchToken()
    {
        return 4;
    }

}
