// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.JSONToken;
import com.alibaba.fastjson.parser.ParseContext;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.serializer.FilterUtils;
import com.alibaba.fastjson.util.DeserializeBeanInfo;
import com.alibaba.fastjson.util.FieldInfo;
import com.alibaba.fastjson.util.TypeUtils;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer, FieldDeserializer

public class JavaBeanDeserializer
    implements ObjectDeserializer
{

    private DeserializeBeanInfo beanInfo;
    private final Class clazz;
    private final Map feildDeserializerMap;
    private final List fieldDeserializers;
    private final List sortedFieldDeserializers;

    public JavaBeanDeserializer(ParserConfig parserconfig, Class class1)
    {
        this(parserconfig, class1, ((Type) (class1)));
    }

    public JavaBeanDeserializer(ParserConfig parserconfig, Class class1, Type type)
    {
        feildDeserializerMap = new IdentityHashMap();
        fieldDeserializers = new ArrayList();
        sortedFieldDeserializers = new ArrayList();
        clazz = class1;
        beanInfo = DeserializeBeanInfo.computeSetters(class1, type);
        for (type = beanInfo.getFieldList().iterator(); type.hasNext(); addFieldDeserializer(parserconfig, class1, (FieldInfo)type.next())) { }
        for (parserconfig = beanInfo.getSortedFieldList().iterator(); parserconfig.hasNext(); sortedFieldDeserializers.add(class1))
        {
            class1 = (FieldInfo)parserconfig.next();
            class1 = (FieldDeserializer)feildDeserializerMap.get(class1.getName().intern());
        }

    }

    private void addFieldDeserializer(ParserConfig parserconfig, Class class1, FieldInfo fieldinfo)
    {
        String s = fieldinfo.getName().intern();
        parserconfig = createFieldDeserializer(parserconfig, class1, fieldinfo);
        feildDeserializerMap.put(s, parserconfig);
        fieldDeserializers.add(parserconfig);
    }

    public FieldDeserializer createFieldDeserializer(ParserConfig parserconfig, Class class1, FieldInfo fieldinfo)
    {
        return parserconfig.createFieldDeserializer(parserconfig, class1, fieldinfo);
    }

    public Object createInstance(DefaultJSONParser defaultjsonparser, Type type)
    {
        if (!(type instanceof Class) || !clazz.isInterface()) goto _L2; else goto _L1
_L1:
        Object obj;
        defaultjsonparser = (Class)type;
        type = Thread.currentThread().getContextClassLoader();
        obj = new JSONObject();
        obj = Proxy.newProxyInstance(type, new Class[] {
            defaultjsonparser
        }, ((java.lang.reflect.InvocationHandler) (obj)));
_L4:
        return obj;
_L2:
        if (beanInfo.getDefaultConstructor() == null)
        {
            return null;
        }
        type = beanInfo.getDefaultConstructor();
        if (type.getParameterTypes().length != 0)
        {
            break MISSING_BLOCK_LABEL_190;
        }
        type = ((Type) (type.newInstance(new Object[0])));
_L7:
        obj = type;
        if (!defaultjsonparser.isEnabled(Feature.InitStringFieldAsEmpty)) goto _L4; else goto _L3
_L3:
        defaultjsonparser = beanInfo.getFieldList().iterator();
_L6:
        obj = type;
        if (!defaultjsonparser.hasNext()) goto _L4; else goto _L5
_L5:
        FieldInfo fieldinfo = (FieldInfo)defaultjsonparser.next();
        if (fieldinfo.getFieldClass() == java/lang/String)
        {
            try
            {
                fieldinfo.set(type, "");
            }
            // Misplaced declaration of an exception variable
            catch (DefaultJSONParser defaultjsonparser)
            {
                throw new JSONException((new StringBuilder()).append("create instance error, class ").append(clazz.getName()).toString(), defaultjsonparser);
            }
        }
          goto _L6
          goto _L4
        try
        {
            type = ((Type) (type.newInstance(new Object[] {
                defaultjsonparser.getContext().getObject()
            })));
        }
        // Misplaced declaration of an exception variable
        catch (DefaultJSONParser defaultjsonparser)
        {
            throw new JSONException((new StringBuilder()).append("create instance error, class ").append(clazz.getName()).toString(), defaultjsonparser);
        }
          goto _L7
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        return deserialze(defaultjsonparser, type, obj, null);
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj, Object obj1)
    {
        Object obj2;
        Object obj3;
        Object obj4;
        ParseContext parsecontext;
        Object obj5;
        JSONLexer jsonlexer;
        if (type == com/alibaba/fastjson/JSON || type == com/alibaba/fastjson/JSONObject)
        {
            return defaultjsonparser.parse();
        }
        jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() == 8)
        {
            jsonlexer.nextToken(16);
            return null;
        }
        parsecontext = defaultjsonparser.getContext();
        if (obj1 != null && parsecontext != null)
        {
            parsecontext = parsecontext.getParentContext();
        }
        obj5 = null;
        obj4 = null;
        obj3 = obj5;
        obj2 = obj1;
        if (jsonlexer.token() != 13)
        {
            break MISSING_BLOCK_LABEL_155;
        }
        obj3 = obj5;
        obj2 = obj1;
        jsonlexer.nextToken(16);
        obj = obj1;
        if (obj1 != null)
        {
            break MISSING_BLOCK_LABEL_135;
        }
        obj3 = obj5;
        obj2 = obj1;
        obj = createInstance(defaultjsonparser, type);
        if (false)
        {
            throw new NullPointerException();
        } else
        {
            defaultjsonparser.setContext(parsecontext);
            return obj;
        }
        obj3 = obj5;
        obj2 = obj1;
        if (jsonlexer.token() != 14)
        {
            break MISSING_BLOCK_LABEL_230;
        }
        obj3 = obj5;
        obj2 = obj1;
        if (!isSupportArrayToBean(jsonlexer))
        {
            break MISSING_BLOCK_LABEL_230;
        }
        obj3 = obj5;
        obj2 = obj1;
        type = ((Type) (deserialzeArrayMapping(defaultjsonparser, type, obj, obj1)));
        if (false)
        {
            throw new NullPointerException();
        } else
        {
            defaultjsonparser.setContext(parsecontext);
            return type;
        }
        obj3 = obj5;
        obj2 = obj1;
        if (jsonlexer.token() == 12) goto _L2; else goto _L1
_L1:
        obj3 = obj5;
        obj2 = obj1;
        if (jsonlexer.token() == 16) goto _L2; else goto _L3
_L3:
        obj3 = obj5;
        obj2 = obj1;
        type = (new StringBuffer()).append("syntax error, expect {, actual ").append(jsonlexer.tokenName()).append(", pos ").append(jsonlexer.pos());
        obj3 = obj5;
        obj2 = obj1;
        if (!(obj instanceof String))
        {
            break MISSING_BLOCK_LABEL_352;
        }
        obj3 = obj5;
        obj2 = obj1;
        type.append(", fieldName ").append(obj);
        obj3 = obj5;
        obj2 = obj1;
        throw new JSONException(type.toString());
        type;
        obj1 = obj3;
_L28:
        if (obj1 != null)
        {
            ((ParseContext) (obj1)).setObject(obj2);
        }
        defaultjsonparser.setContext(parsecontext);
        throw type;
_L2:
        obj3 = obj5;
        obj2 = obj1;
        if (defaultjsonparser.getResolveStatus() != 2)
        {
            break MISSING_BLOCK_LABEL_426;
        }
        obj3 = obj5;
        obj2 = obj1;
        defaultjsonparser.setResolveStatus(0);
        obj2 = null;
_L11:
        obj3 = obj1;
        String s = jsonlexer.scanSymbol(defaultjsonparser.getSymbolTable());
        if (s != null)
        {
            break; /* Loop/switch isn't completed */
        }
        obj3 = obj1;
        if (jsonlexer.token() != 13) goto _L5; else goto _L4
_L4:
        obj3 = obj1;
        jsonlexer.nextToken(16);
        obj3 = obj1;
        obj1 = obj2;
        obj2 = obj3;
_L24:
        if (obj2 != null) goto _L7; else goto _L6
_L6:
        if (obj4 != null) goto _L9; else goto _L8
_L8:
        type = ((Type) (createInstance(defaultjsonparser, type)));
        if (obj1 != null)
        {
            break MISSING_BLOCK_LABEL_531;
        }
        obj3 = obj1;
        obj2 = type;
        obj1 = defaultjsonparser.setContext(parsecontext, type, obj);
        if (obj1 != null)
        {
            ((ParseContext) (obj1)).setObject(type);
        }
        defaultjsonparser.setContext(parsecontext);
        return type;
_L5:
        obj3 = obj1;
        if (jsonlexer.token() != 16)
        {
            break; /* Loop/switch isn't completed */
        }
        obj3 = obj1;
        if (defaultjsonparser.isEnabled(Feature.AllowArbitraryCommas)) goto _L11; else goto _L10
_L10:
        if ("$ref" != s) goto _L13; else goto _L12
_L12:
        obj3 = obj1;
        jsonlexer.nextTokenWithColon(4);
        obj3 = obj1;
        if (jsonlexer.token() != 4) goto _L15; else goto _L14
_L14:
        obj3 = obj1;
        obj4 = jsonlexer.stringVal();
        obj3 = obj1;
        if (!"@".equals(obj4)) goto _L17; else goto _L16
_L16:
        obj3 = obj1;
        obj1 = parsecontext.getObject();
_L22:
        obj3 = obj1;
        jsonlexer.nextToken(13);
        obj3 = obj1;
        if (jsonlexer.token() == 13) goto _L19; else goto _L18
_L18:
        obj3 = obj1;
        throw new JSONException("illegal ref");
_L17:
        obj3 = obj1;
        if (!"..".equals(obj4))
        {
            break MISSING_BLOCK_LABEL_779;
        }
        obj3 = obj1;
        type = parsecontext.getParentContext();
        obj3 = obj1;
        if (type.getObject() == null)
        {
            break MISSING_BLOCK_LABEL_746;
        }
        obj3 = obj1;
        type = ((Type) (type.getObject()));
        break MISSING_BLOCK_LABEL_1687;
        obj3 = obj1;
        defaultjsonparser.addResolveTask(new com.alibaba.fastjson.parser.DefaultJSONParser.ResolveTask(type, ((String) (obj4))));
        obj3 = obj1;
        defaultjsonparser.setResolveStatus(1);
        type = ((Type) (obj1));
        break MISSING_BLOCK_LABEL_1687;
        obj3 = obj1;
        if (!"$".equals(obj4))
        {
            break MISSING_BLOCK_LABEL_874;
        }
        type = parsecontext;
_L21:
        obj3 = obj1;
        if (type.getParentContext() == null)
        {
            break; /* Loop/switch isn't completed */
        }
        obj3 = obj1;
        type = type.getParentContext();
        if (true) goto _L21; else goto _L20
_L20:
        obj3 = obj1;
        if (type.getObject() == null)
        {
            break MISSING_BLOCK_LABEL_844;
        }
        obj3 = obj1;
        obj1 = type.getObject();
          goto _L22
        obj3 = obj1;
        defaultjsonparser.addResolveTask(new com.alibaba.fastjson.parser.DefaultJSONParser.ResolveTask(type, ((String) (obj4))));
        obj3 = obj1;
        defaultjsonparser.setResolveStatus(1);
          goto _L22
        obj3 = obj1;
        defaultjsonparser.addResolveTask(new com.alibaba.fastjson.parser.DefaultJSONParser.ResolveTask(parsecontext, ((String) (obj4))));
        obj3 = obj1;
        defaultjsonparser.setResolveStatus(1);
          goto _L22
_L15:
        obj3 = obj1;
        throw new JSONException((new StringBuilder()).append("illegal ref, ").append(JSONToken.name(jsonlexer.token())).toString());
_L19:
        obj3 = obj1;
        jsonlexer.nextToken(16);
        obj3 = obj1;
        defaultjsonparser.setContext(parsecontext, obj1, obj);
        if (obj2 != null)
        {
            ((ParseContext) (obj2)).setObject(obj1);
        }
        defaultjsonparser.setContext(parsecontext);
        return obj1;
_L13:
        obj3 = obj1;
        if (JSON.DEFAULT_TYPE_KEY != s)
        {
            break MISSING_BLOCK_LABEL_1197;
        }
        obj3 = obj1;
        jsonlexer.nextTokenWithColon(4);
        obj3 = obj1;
        if (jsonlexer.token() != 4)
        {
            break MISSING_BLOCK_LABEL_1182;
        }
        obj3 = obj1;
        obj5 = jsonlexer.stringVal();
        obj3 = obj1;
        jsonlexer.nextToken(16);
        obj3 = obj1;
        if (!(type instanceof Class))
        {
            break MISSING_BLOCK_LABEL_1131;
        }
        obj3 = obj1;
        if (!((String) (obj5)).equals(((Class)type).getName()))
        {
            break MISSING_BLOCK_LABEL_1131;
        }
        obj3 = obj1;
        if (jsonlexer.token() != 13) goto _L11; else goto _L23
_L23:
        obj3 = obj1;
        jsonlexer.nextToken();
        obj3 = obj1;
        obj1 = obj2;
        obj2 = obj3;
          goto _L24
        obj3 = obj1;
        type = TypeUtils.loadClass(((String) (obj5)));
        obj3 = obj1;
        type = ((Type) (defaultjsonparser.getConfig().getDeserializer(type).deserialze(defaultjsonparser, type, obj)));
        if (obj2 != null)
        {
            ((ParseContext) (obj2)).setObject(obj1);
        }
        defaultjsonparser.setContext(parsecontext);
        return type;
        obj3 = obj1;
        throw new JSONException("syntax error");
        if (obj1 != null || obj4 != null)
        {
            break MISSING_BLOCK_LABEL_1265;
        }
        obj3 = obj1;
        obj1 = createInstance(defaultjsonparser, type);
        if (obj1 != null)
        {
            break MISSING_BLOCK_LABEL_1246;
        }
        obj3 = obj1;
        obj4 = new HashMap(fieldDeserializers.size());
        obj3 = obj1;
        obj5 = defaultjsonparser.setContext(parsecontext, obj1, obj);
        obj2 = obj5;
        if (parseField(defaultjsonparser, s, obj1, type, ((Map) (obj4))))
        {
            continue; /* Loop/switch isn't completed */
        }
        if (jsonlexer.token() != 13) goto _L11; else goto _L25
_L25:
        jsonlexer.nextToken();
        obj3 = obj2;
        obj2 = obj1;
        obj1 = obj3;
          goto _L24
        if (jsonlexer.token() != 16) goto _L26; else goto _L11
_L26:
        if (jsonlexer.token() != 13)
        {
            continue; /* Loop/switch isn't completed */
        }
        jsonlexer.nextToken(16);
        obj3 = obj2;
        obj2 = obj1;
        obj1 = obj3;
          goto _L24
        if (jsonlexer.token() != 18 && jsonlexer.token() != 1) goto _L11; else goto _L27
_L27:
        throw new JSONException((new StringBuilder()).append("syntax error, unexpect token ").append(JSONToken.name(jsonlexer.token())).toString());
        type;
        obj = obj1;
        obj1 = obj2;
        obj2 = obj;
          goto _L28
_L9:
        int j;
        obj = beanInfo.getFieldList();
        j = ((List) (obj)).size();
        type = ((Type) (new Object[j]));
        int i = 0;
_L30:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        type[i] = ((Map) (obj4)).get(((FieldInfo)((List) (obj)).get(i)).getName());
        i++;
        if (true) goto _L30; else goto _L29
_L29:
        obj = beanInfo.getCreatorConstructor();
        if (obj == null) goto _L32; else goto _L31
_L31:
        type = ((Type) (beanInfo.getCreatorConstructor().newInstance(type)));
_L35:
        if (obj1 != null)
        {
            ((ParseContext) (obj1)).setObject(type);
        }
        defaultjsonparser.setContext(parsecontext);
        return type;
        type;
        throw new JSONException((new StringBuilder()).append("create instance error, ").append(beanInfo.getCreatorConstructor().toGenericString()).toString(), type);
_L32:
        obj = beanInfo.getFactoryMethod();
        if (obj == null) goto _L7; else goto _L33
_L33:
        try
        {
            type = ((Type) (beanInfo.getFactoryMethod().invoke(null, type)));
            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (Type type) { }
        finally { }
        throw new JSONException((new StringBuilder()).append("create factory method error, ").append(beanInfo.getFactoryMethod().toString()).toString(), type);
_L7:
        type = ((Type) (obj2));
        if (true) goto _L35; else goto _L34
_L34:
        type;
        obj1 = obj2;
        obj2 = obj3;
          goto _L28
        obj1 = type;
          goto _L22
    }

    public Object deserialzeArrayMapping(DefaultJSONParser defaultjsonparser, Type type, Object obj, Object obj1)
    {
        int i;
        int j;
        obj = defaultjsonparser.getLexer();
        if (((JSONLexer) (obj)).token() != 14)
        {
            throw new JSONException("error");
        }
        type = ((Type) (createInstance(defaultjsonparser, type)));
        j = sortedFieldDeserializers.size();
        i = 0;
_L1:
        char c;
        if (i >= j)
        {
            break MISSING_BLOCK_LABEL_301;
        }
        Class class1;
        if (i == j - 1)
        {
            c = ']';
        } else
        {
            c = ',';
        }
        obj1 = (FieldDeserializer)sortedFieldDeserializers.get(i);
        class1 = ((FieldDeserializer) (obj1)).getFieldClass();
        if (class1 == Integer.TYPE)
        {
            ((FieldDeserializer) (obj1)).setValue(type, ((JSONLexer) (obj)).scanInt(c));
        } else
        if (class1 == java/lang/String)
        {
            ((FieldDeserializer) (obj1)).setValue(type, ((JSONLexer) (obj)).scanString(c));
        } else
        if (class1 == Long.TYPE)
        {
            ((FieldDeserializer) (obj1)).setValue(type, ((JSONLexer) (obj)).scanLong(c));
        } else
        if (class1.isEnum())
        {
            ((FieldDeserializer) (obj1)).setValue(type, ((JSONLexer) (obj)).scanEnum(class1, defaultjsonparser.getSymbolTable(), c));
        } else
        {
            ((JSONLexer) (obj)).nextToken(14);
            ((FieldDeserializer) (obj1)).setValue(type, defaultjsonparser.parseObject(((FieldDeserializer) (obj1)).getFieldType()));
            if (c != ']')
            {
                continue; /* Loop/switch isn't completed */
            }
            if (((JSONLexer) (obj)).token() != 15)
            {
                throw new JSONException("syntax error");
            }
            ((JSONLexer) (obj)).nextToken(16);
        }
_L3:
        i++;
          goto _L1
        if (c != ',' || ((JSONLexer) (obj)).token() == 16) goto _L3; else goto _L2
_L2:
        throw new JSONException("syntax error");
        ((JSONLexer) (obj)).nextToken(16);
        return type;
    }

    public Class getClazz()
    {
        return clazz;
    }

    public int getFastMatchToken()
    {
        return 12;
    }

    public FieldDeserializer getFieldDeserializer(String s)
    {
        FieldDeserializer fielddeserializer = (FieldDeserializer)feildDeserializerMap.get(s);
        if (fielddeserializer != null)
        {
            return fielddeserializer;
        }
        for (Iterator iterator = feildDeserializerMap.entrySet().iterator(); iterator.hasNext();)
        {
            java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
            if (s.equals(entry.getKey()))
            {
                return (FieldDeserializer)entry.getValue();
            }
        }

        return null;
    }

    public Map getFieldDeserializerMap()
    {
        return feildDeserializerMap;
    }

    public List getSortedFieldDeserializers()
    {
        return sortedFieldDeserializers;
    }

    public final boolean isSupportArrayToBean(JSONLexer jsonlexer)
    {
        return Feature.isEnabled(beanInfo.getParserFeatures(), Feature.SupportArrayToBean) || jsonlexer.isEnabled(Feature.SupportArrayToBean);
    }

    void parseExtra(DefaultJSONParser defaultjsonparser, Object obj, String s)
    {
        Object obj1 = defaultjsonparser.getLexer();
        if (!((JSONLexer) (obj1)).isEnabled(Feature.IgnoreNotMatch))
        {
            throw new JSONException((new StringBuilder()).append("setter not found, class ").append(clazz.getName()).append(", property ").append(s).toString());
        }
        ((JSONLexer) (obj1)).nextTokenWithColon();
        obj1 = FilterUtils.getExtratype(defaultjsonparser, obj, s);
        if (obj1 == null)
        {
            obj1 = defaultjsonparser.parse();
        } else
        {
            obj1 = defaultjsonparser.parseObject(((Type) (obj1)));
        }
        FilterUtils.processExtra(defaultjsonparser, obj, s, obj1);
    }

    public boolean parseField(DefaultJSONParser defaultjsonparser, String s, Object obj, Type type, Map map)
    {
        Object obj1;
        JSONLexer jsonlexer;
label0:
        {
            jsonlexer = defaultjsonparser.getLexer();
            FieldDeserializer fielddeserializer = (FieldDeserializer)feildDeserializerMap.get(s);
            obj1 = fielddeserializer;
            if (fielddeserializer != null)
            {
                break label0;
            }
            Iterator iterator = feildDeserializerMap.entrySet().iterator();
            do
            {
                obj1 = fielddeserializer;
                if (!iterator.hasNext())
                {
                    break label0;
                }
                obj1 = (java.util.Map.Entry)iterator.next();
            } while (!((String)((java.util.Map.Entry) (obj1)).getKey()).equalsIgnoreCase(s));
            obj1 = (FieldDeserializer)((java.util.Map.Entry) (obj1)).getValue();
        }
        if (obj1 == null)
        {
            parseExtra(defaultjsonparser, obj, s);
            return false;
        } else
        {
            jsonlexer.nextTokenWithColon(((FieldDeserializer) (obj1)).getFastMatchToken());
            ((FieldDeserializer) (obj1)).parseField(defaultjsonparser, obj, type, map);
            return true;
        }
    }
}
