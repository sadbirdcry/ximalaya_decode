// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer

public class CharArrayDeserializer
    implements ObjectDeserializer
{

    public static final CharArrayDeserializer instance = new CharArrayDeserializer();

    public CharArrayDeserializer()
    {
    }

    public static Object deserialze(DefaultJSONParser defaultjsonparser)
    {
        JSONLexer jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() == 4)
        {
            defaultjsonparser = jsonlexer.stringVal();
            jsonlexer.nextToken(16);
            return defaultjsonparser.toCharArray();
        }
        if (jsonlexer.token() == 2)
        {
            defaultjsonparser = jsonlexer.integerValue();
            jsonlexer.nextToken(16);
            return defaultjsonparser.toString().toCharArray();
        }
        defaultjsonparser = ((DefaultJSONParser) (defaultjsonparser.parse()));
        if (defaultjsonparser == null)
        {
            return null;
        } else
        {
            return JSON.toJSONString(defaultjsonparser).toCharArray();
        }
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        return deserialze(defaultjsonparser);
    }

    public int getFastMatchToken()
    {
        return 4;
    }

}
