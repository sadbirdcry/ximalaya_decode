// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import java.lang.reflect.Type;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            FieldDeserializer

public final class MapResolveFieldDeserializer extends FieldDeserializer
{

    private final String key;
    private final Map map;

    public MapResolveFieldDeserializer(Map map1, String s)
    {
        super(null, null);
        key = s;
        map = map1;
    }

    public void parseField(DefaultJSONParser defaultjsonparser, Object obj, Type type, Map map1)
    {
    }

    public void setValue(Object obj, Object obj1)
    {
        map.put(key, obj1);
    }
}
