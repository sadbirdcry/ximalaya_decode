// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.ParseContext;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.util.FieldInfo;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            FieldDeserializer, ObjectDeserializer

public class DefaultFieldDeserializer extends FieldDeserializer
{

    private ObjectDeserializer fieldValueDeserilizer;

    public DefaultFieldDeserializer(ParserConfig parserconfig, Class class1, FieldInfo fieldinfo)
    {
        super(class1, fieldinfo);
    }

    public int getFastMatchToken()
    {
        if (fieldValueDeserilizer != null)
        {
            return fieldValueDeserilizer.getFastMatchToken();
        } else
        {
            return 2;
        }
    }

    public void parseField(DefaultJSONParser defaultjsonparser, Object obj, Type type, Map map)
    {
        if (fieldValueDeserilizer == null)
        {
            fieldValueDeserilizer = defaultjsonparser.getConfig().getDeserializer(fieldInfo);
        }
        if (type instanceof ParameterizedType)
        {
            defaultjsonparser.getContext().setType(type);
        }
        type = ((Type) (fieldValueDeserilizer.deserialze(defaultjsonparser, getFieldType(), fieldInfo.getName())));
        if (defaultjsonparser.getResolveStatus() == 1)
        {
            obj = defaultjsonparser.getLastResolveTask();
            ((com.alibaba.fastjson.parser.DefaultJSONParser.ResolveTask) (obj)).setFieldDeserializer(this);
            ((com.alibaba.fastjson.parser.DefaultJSONParser.ResolveTask) (obj)).setOwnerContext(defaultjsonparser.getContext());
            defaultjsonparser.setResolveStatus(0);
            return;
        }
        if (obj == null)
        {
            map.put(fieldInfo.getName(), type);
            return;
        } else
        {
            setValue(obj, type);
            return;
        }
    }
}
