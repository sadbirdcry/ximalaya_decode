// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            AbstractDateDeserializer, ObjectDeserializer

public class TimestampDeserializer extends AbstractDateDeserializer
    implements ObjectDeserializer
{

    public static final TimestampDeserializer instance = new TimestampDeserializer();

    public TimestampDeserializer()
    {
    }

    protected Object cast(DefaultJSONParser defaultjsonparser, Type type, Object obj, Object obj1)
    {
        if (obj1 != null)
        {
            if (obj1 instanceof Date)
            {
                return new Timestamp(((Date)obj1).getTime());
            }
            if (obj1 instanceof Number)
            {
                return new Timestamp(((Number)obj1).longValue());
            }
            if (obj1 instanceof String)
            {
                type = (String)obj1;
                if (type.length() != 0)
                {
                    defaultjsonparser = defaultjsonparser.getDateFormat();
                    try
                    {
                        defaultjsonparser = new Timestamp(defaultjsonparser.parse(type).getTime());
                    }
                    // Misplaced declaration of an exception variable
                    catch (DefaultJSONParser defaultjsonparser)
                    {
                        return new Timestamp(Long.parseLong(type));
                    }
                    return defaultjsonparser;
                }
            } else
            {
                throw new JSONException("parse error");
            }
        }
        return null;
    }

    public int getFastMatchToken()
    {
        return 2;
    }

}
