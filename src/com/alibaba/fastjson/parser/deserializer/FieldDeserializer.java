// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.util.FieldInfo;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public abstract class FieldDeserializer
{

    protected final Class clazz;
    protected final FieldInfo fieldInfo;

    public FieldDeserializer(Class class1, FieldInfo fieldinfo)
    {
        clazz = class1;
        fieldInfo = fieldinfo;
    }

    public int getFastMatchToken()
    {
        return 0;
    }

    public Field getField()
    {
        return fieldInfo.getField();
    }

    public Class getFieldClass()
    {
        return fieldInfo.getFieldClass();
    }

    public FieldInfo getFieldInfo()
    {
        return fieldInfo;
    }

    public Type getFieldType()
    {
        return fieldInfo.getFieldType();
    }

    public Method getMethod()
    {
        return fieldInfo.getMethod();
    }

    public abstract void parseField(DefaultJSONParser defaultjsonparser, Object obj, Type type, Map map);

    public void setValue(Object obj, int i)
    {
        setValue(obj, Integer.valueOf(i));
    }

    public void setValue(Object obj, long l)
    {
        setValue(obj, Long.valueOf(l));
    }

    public void setValue(Object obj, Object obj1)
    {
        Method method;
        method = fieldInfo.getMethod();
        if (method == null)
        {
            break MISSING_BLOCK_LABEL_281;
        }
        if (!fieldInfo.isGetOnly())
        {
            break MISSING_BLOCK_LABEL_249;
        }
        if (fieldInfo.getFieldClass() != java/util/concurrent/atomic/AtomicInteger)
        {
            break MISSING_BLOCK_LABEL_63;
        }
        obj = (AtomicInteger)method.invoke(obj, new Object[0]);
        if (obj != null)
        {
            try
            {
                ((AtomicInteger) (obj)).set(((AtomicInteger)obj1).get());
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                throw new JSONException((new StringBuilder()).append("set property error, ").append(fieldInfo.getName()).toString(), ((Throwable) (obj)));
            }
        }
        break MISSING_BLOCK_LABEL_335;
        if (fieldInfo.getFieldClass() != java/util/concurrent/atomic/AtomicLong)
        {
            break MISSING_BLOCK_LABEL_139;
        }
        obj = (AtomicLong)method.invoke(obj, new Object[0]);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_335;
        }
        ((AtomicLong) (obj)).set(((AtomicLong)obj1).get());
        return;
        if (fieldInfo.getFieldClass() != java/util/concurrent/atomic/AtomicBoolean)
        {
            break MISSING_BLOCK_LABEL_180;
        }
        obj = (AtomicBoolean)method.invoke(obj, new Object[0]);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_335;
        }
        ((AtomicBoolean) (obj)).set(((AtomicBoolean)obj1).get());
        return;
        if (!java/util/Map.isAssignableFrom(method.getReturnType()))
        {
            break MISSING_BLOCK_LABEL_220;
        }
        obj = (Map)method.invoke(obj, new Object[0]);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_335;
        }
        ((Map) (obj)).putAll((Map)obj1);
        return;
        obj = (Collection)method.invoke(obj, new Object[0]);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_335;
        }
        ((Collection) (obj)).addAll((Collection)obj1);
        return;
        if (obj1 != null)
        {
            break MISSING_BLOCK_LABEL_266;
        }
        if (fieldInfo.getFieldClass().isPrimitive())
        {
            break MISSING_BLOCK_LABEL_335;
        }
        method.invoke(obj, new Object[] {
            obj1
        });
        return;
        Field field = fieldInfo.getField();
        if (field != null)
        {
            try
            {
                field.set(obj, obj1);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                throw new JSONException((new StringBuilder()).append("set property error, ").append(fieldInfo.getName()).toString(), ((Throwable) (obj)));
            }
        }
    }

    public void setValue(Object obj, String s)
    {
        setValue(obj, s);
    }

    public void setValue(Object obj, boolean flag)
    {
        setValue(obj, Boolean.valueOf(flag));
    }
}
