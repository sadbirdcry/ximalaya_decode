// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.util.TypeUtils;
import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            JavaBeanDeserializer

public class ThrowableDeserializer extends JavaBeanDeserializer
{

    public ThrowableDeserializer(ParserConfig parserconfig, Class class1)
    {
        super(parserconfig, class1);
    }

    private Throwable createException(String s, Throwable throwable, Class class1)
        throws Exception
    {
        Object obj1 = null;
        Object obj = null;
        Constructor aconstructor[] = class1.getConstructors();
        int j = aconstructor.length;
        int i = 0;
        Object obj3 = null;
        class1 = obj1;
        while (i < j) 
        {
            Object obj2 = aconstructor[i];
            if (((Constructor) (obj2)).getParameterTypes().length == 0)
            {
                class1 = ((Class) (obj));
                obj = obj2;
            } else
            if (((Constructor) (obj2)).getParameterTypes().length == 1 && ((Constructor) (obj2)).getParameterTypes()[0] == java/lang/String)
            {
                obj = class1;
                class1 = ((Class) (obj2));
            } else
            if (((Constructor) (obj2)).getParameterTypes().length == 2 && ((Constructor) (obj2)).getParameterTypes()[0] == java/lang/String && ((Constructor) (obj2)).getParameterTypes()[1] == java/lang/Throwable)
            {
                Class class3 = class1;
                obj3 = obj2;
                class1 = ((Class) (obj));
                obj = class3;
            } else
            {
                Class class2 = class1;
                class1 = ((Class) (obj));
                obj = class2;
            }
            i++;
            obj2 = obj;
            obj = class1;
            class1 = ((Class) (obj2));
        }
        if (obj3 != null)
        {
            return (Throwable)((Constructor) (obj3)).newInstance(new Object[] {
                s, throwable
            });
        }
        if (obj != null)
        {
            return (Throwable)((Constructor) (obj)).newInstance(new Object[] {
                s
            });
        }
        if (class1 != null)
        {
            return (Throwable)class1.newInstance(new Object[0]);
        } else
        {
            return null;
        }
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        JSONLexer jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() != 8) goto _L2; else goto _L1
_L1:
        jsonlexer.nextToken();
        obj = null;
_L12:
        return obj;
_L2:
        if (defaultjsonparser.getResolveStatus() != 2) goto _L4; else goto _L3
_L3:
        defaultjsonparser.setResolveStatus(0);
_L14:
        obj = null;
        if (type == null || !(type instanceof Class))
        {
            break MISSING_BLOCK_LABEL_539;
        }
        type = (Class)type;
        if (!java/lang/Throwable.isAssignableFrom(type))
        {
            break MISSING_BLOCK_LABEL_539;
        }
_L22:
        Object obj1;
        Object obj2;
        HashMap hashmap;
        obj2 = null;
        Object obj3 = null;
        hashmap = new HashMap();
        obj1 = type;
        type = obj3;
_L15:
        Object obj4 = jsonlexer.scanSymbol(defaultjsonparser.getSymbolTable());
        if (obj4 != null) goto _L6; else goto _L5
_L5:
        if (jsonlexer.token() != 13) goto _L8; else goto _L7
_L7:
        jsonlexer.nextToken(16);
        obj4 = obj;
        obj = obj2;
_L18:
        if (obj1 != null) goto _L10; else goto _L9
_L9:
        defaultjsonparser = new Exception(((String) (obj)), ((Throwable) (obj4)));
_L21:
        obj = defaultjsonparser;
        if (type == null) goto _L12; else goto _L11
_L11:
        defaultjsonparser.setStackTrace(type);
        return defaultjsonparser;
_L4:
        if (jsonlexer.token() == 12) goto _L14; else goto _L13
_L13:
        throw new JSONException("syntax error");
_L8:
        if (jsonlexer.token() == 16 && jsonlexer.isEnabled(Feature.AllowArbitraryCommas)) goto _L15; else goto _L6
_L6:
        jsonlexer.nextTokenWithColon(4);
        if (!JSON.DEFAULT_TYPE_KEY.equals(obj4))
        {
            break MISSING_BLOCK_LABEL_310;
        }
        if (jsonlexer.token() != 4) goto _L17; else goto _L16
_L16:
        obj1 = TypeUtils.loadClass(jsonlexer.stringVal());
        jsonlexer.nextToken(16);
        obj4 = obj2;
        obj2 = obj;
        obj = obj4;
_L19:
        if (jsonlexer.token() != 13)
        {
            break MISSING_BLOCK_LABEL_481;
        }
        jsonlexer.nextToken(16);
        obj4 = obj2;
          goto _L18
_L17:
        throw new JSONException("syntax error");
        if ("message".equals(obj4))
        {
            if (jsonlexer.token() == 8)
            {
                obj2 = null;
            } else
            if (jsonlexer.token() == 4)
            {
                obj2 = jsonlexer.stringVal();
            } else
            {
                throw new JSONException("syntax error");
            }
            jsonlexer.nextToken();
            obj4 = obj;
            obj = obj2;
            obj2 = obj4;
        } else
        if ("cause".equals(obj4))
        {
            obj4 = (Throwable)deserialze(defaultjsonparser, null, "cause");
            obj = obj2;
            obj2 = obj4;
        } else
        if ("stackTrace".equals(obj4))
        {
            type = (StackTraceElement[])defaultjsonparser.parseObject([Ljava/lang/StackTraceElement;);
            obj4 = obj;
            obj = obj2;
            obj2 = obj4;
        } else
        {
            hashmap.put(obj4, defaultjsonparser.parse());
            obj4 = obj;
            obj = obj2;
            obj2 = obj4;
        }
          goto _L19
        obj4 = obj2;
        obj2 = obj;
        obj = obj4;
          goto _L15
_L10:
        try
        {
            obj1 = createException(((String) (obj)), ((Throwable) (obj4)), ((Class) (obj1)));
        }
        // Misplaced declaration of an exception variable
        catch (DefaultJSONParser defaultjsonparser)
        {
            throw new JSONException("create instance error", defaultjsonparser);
        }
        defaultjsonparser = ((DefaultJSONParser) (obj1));
        if (obj1 != null) goto _L21; else goto _L20
_L20:
        defaultjsonparser = new Exception(((String) (obj)), ((Throwable) (obj4)));
          goto _L21
        type = null;
          goto _L22
    }

    public int getFastMatchToken()
    {
        return 12;
    }
}
