// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONScanner;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            AbstractDateDeserializer, ObjectDeserializer

public class SqlDateDeserializer extends AbstractDateDeserializer
    implements ObjectDeserializer
{

    public static final SqlDateDeserializer instance = new SqlDateDeserializer();

    public SqlDateDeserializer()
    {
    }

    protected Object cast(DefaultJSONParser defaultjsonparser, Type type, Object obj, Object obj1)
    {
        if (obj1 != null) goto _L2; else goto _L1
_L1:
        return null;
_L2:
        if (obj1 instanceof Date)
        {
            return new java.sql.Date(((Date)obj1).getTime());
        }
        if (obj1 instanceof Number)
        {
            return new java.sql.Date(((Number)obj1).longValue());
        }
        if (!(obj1 instanceof String))
        {
            break MISSING_BLOCK_LABEL_159;
        }
        obj = (String)obj1;
        if (((String) (obj)).length() == 0) goto _L1; else goto _L3
_L3:
        type = new JSONScanner(((String) (obj)));
        if (!type.scanISO8601DateIfMatch()) goto _L5; else goto _L4
_L4:
        long l = type.getCalendar().getTimeInMillis();
_L7:
        type.close();
        return new java.sql.Date(l);
_L5:
        defaultjsonparser = defaultjsonparser.getDateFormat();
        defaultjsonparser = new java.sql.Date(defaultjsonparser.parse(((String) (obj))).getTime());
        type.close();
        return defaultjsonparser;
        defaultjsonparser;
        l = Long.parseLong(((String) (obj)));
        if (true) goto _L7; else goto _L6
_L6:
        defaultjsonparser;
        type.close();
        throw defaultjsonparser;
        throw new JSONException((new StringBuilder()).append("parse error : ").append(obj1).toString());
    }

    public int getFastMatchToken()
    {
        return 2;
    }

}
