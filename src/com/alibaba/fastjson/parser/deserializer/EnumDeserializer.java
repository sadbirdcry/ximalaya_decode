// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer

public class EnumDeserializer
    implements ObjectDeserializer
{

    private final Class enumClass;
    private final Map nameMap;
    private final Map ordinalMap;

    public EnumDeserializer(Class class1)
    {
        ordinalMap = new HashMap();
        nameMap = new HashMap();
        enumClass = class1;
        Object aobj[];
        Enum enum;
        int i;
        int j;
        try
        {
            aobj = (Object[])(Object[])class1.getMethod("values", new Class[0]).invoke(null, new Object[0]);
            j = aobj.length;
        }
        catch (Exception exception)
        {
            throw new JSONException((new StringBuilder()).append("init enum values error, ").append(class1.getName()).toString());
        }
        i = 0;
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        enum = (Enum)aobj[i];
        ordinalMap.put(Integer.valueOf(enum.ordinal()), enum);
        nameMap.put(enum.name(), enum);
        i++;
        if (true) goto _L2; else goto _L1
_L2:
        break MISSING_BLOCK_LABEL_63;
_L1:
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        type = defaultjsonparser.getLexer();
        if (type.token() != 2)
        {
            break MISSING_BLOCK_LABEL_93;
        }
        defaultjsonparser = Integer.valueOf(type.intValue());
        type.nextToken(16);
        type = ((Type) (ordinalMap.get(defaultjsonparser)));
        if (type == null)
        {
            try
            {
                throw new JSONException((new StringBuilder()).append("parse enum ").append(enumClass.getName()).append(" error, value : ").append(defaultjsonparser).toString());
            }
            // Misplaced declaration of an exception variable
            catch (DefaultJSONParser defaultjsonparser)
            {
                throw defaultjsonparser;
            }
            // Misplaced declaration of an exception variable
            catch (DefaultJSONParser defaultjsonparser)
            {
                throw new JSONException(defaultjsonparser.getMessage(), defaultjsonparser);
            }
        } else
        {
            return type;
        }
        if (type.token() != 4)
        {
            break MISSING_BLOCK_LABEL_147;
        }
        defaultjsonparser = type.stringVal();
        type.nextToken(16);
        if (defaultjsonparser.length() == 0)
        {
            return null;
        }
        nameMap.get(defaultjsonparser);
        return Enum.valueOf(enumClass, defaultjsonparser);
        if (type.token() != 8)
        {
            break MISSING_BLOCK_LABEL_182;
        }
        type.nextToken(16);
        return null;
        defaultjsonparser = ((DefaultJSONParser) (defaultjsonparser.parse()));
        throw new JSONException((new StringBuilder()).append("parse enum ").append(enumClass.getName()).append(" error, value : ").append(defaultjsonparser).toString());
    }

    public int getFastMatchToken()
    {
        return 2;
    }
}
