// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.util.FieldInfo;
import java.lang.reflect.Type;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            FieldDeserializer, ObjectDeserializer

public class StringFieldDeserializer extends FieldDeserializer
{

    private final ObjectDeserializer fieldValueDeserilizer;

    public StringFieldDeserializer(ParserConfig parserconfig, Class class1, FieldInfo fieldinfo)
    {
        super(class1, fieldinfo);
        fieldValueDeserilizer = parserconfig.getDeserializer(fieldinfo);
    }

    public int getFastMatchToken()
    {
        return fieldValueDeserilizer.getFastMatchToken();
    }

    public void parseField(DefaultJSONParser defaultjsonparser, Object obj, Type type, Map map)
    {
        type = defaultjsonparser.getLexer();
        if (type.token() == 4)
        {
            defaultjsonparser = type.stringVal();
            type.nextToken(16);
        } else
        {
            defaultjsonparser = ((DefaultJSONParser) (defaultjsonparser.parse()));
            if (defaultjsonparser == null)
            {
                defaultjsonparser = null;
            } else
            {
                defaultjsonparser = defaultjsonparser.toString();
            }
        }
        if (obj == null)
        {
            map.put(fieldInfo.getName(), defaultjsonparser);
            return;
        } else
        {
            setValue(obj, defaultjsonparser);
            return;
        }
    }
}
