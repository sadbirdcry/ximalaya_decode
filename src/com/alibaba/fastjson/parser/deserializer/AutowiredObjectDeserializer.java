// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import java.util.Set;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer

public interface AutowiredObjectDeserializer
    extends ObjectDeserializer
{

    public abstract Set getAutowiredFor();
}
