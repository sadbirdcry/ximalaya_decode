// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.util.DeserializeBeanInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ASMDeserializerFactory

static class fieldInfoList
{

    private final DeserializeBeanInfo beanInfo;
    private String className;
    private Class clazz;
    private List fieldInfoList;
    private int variantIndex;
    private Map variants;

    public DeserializeBeanInfo getBeanInfo()
    {
        return beanInfo;
    }

    public String getClassName()
    {
        return className;
    }

    public Class getClazz()
    {
        return clazz;
    }

    public List getFieldInfoList()
    {
        return fieldInfoList;
    }

    public int getVariantCount()
    {
        return variantIndex;
    }

    public int var(String s)
    {
        if ((Integer)variants.get(s) == null)
        {
            Map map = variants;
            int i = variantIndex;
            variantIndex = i + 1;
            map.put(s, Integer.valueOf(i));
        }
        return ((Integer)variants.get(s)).intValue();
    }

    public int var(String s, int i)
    {
        if ((Integer)variants.get(s) == null)
        {
            variants.put(s, Integer.valueOf(variantIndex));
            variantIndex = variantIndex + i;
        }
        return ((Integer)variants.get(s)).intValue();
    }

    public (String s, ParserConfig parserconfig, DeserializeBeanInfo deserializebeaninfo, int i)
    {
        variantIndex = 5;
        variants = new HashMap();
        className = s;
        clazz = deserializebeaninfo.getClazz();
        variantIndex = i;
        beanInfo = deserializebeaninfo;
        fieldInfoList = new ArrayList(deserializebeaninfo.getFieldList());
    }
}
