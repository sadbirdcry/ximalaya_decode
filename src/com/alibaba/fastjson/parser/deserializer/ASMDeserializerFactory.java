// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.asm.ClassWriter;
import com.alibaba.fastjson.asm.FieldVisitor;
import com.alibaba.fastjson.asm.Label;
import com.alibaba.fastjson.asm.MethodVisitor;
import com.alibaba.fastjson.asm.Opcodes;
import com.alibaba.fastjson.asm.Type;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.util.ASMClassLoader;
import com.alibaba.fastjson.util.ASMUtils;
import com.alibaba.fastjson.util.DeserializeBeanInfo;
import com.alibaba.fastjson.util.FieldInfo;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer, IntegerFieldDeserializer, FieldDeserializer, LongFieldDeserializer, 
//            StringFieldDeserializer

public class ASMDeserializerFactory
    implements Opcodes
{
    static class Context
    {

        private final DeserializeBeanInfo beanInfo;
        private String className;
        private Class clazz;
        private List fieldInfoList;
        private int variantIndex;
        private Map variants;

        public DeserializeBeanInfo getBeanInfo()
        {
            return beanInfo;
        }

        public String getClassName()
        {
            return className;
        }

        public Class getClazz()
        {
            return clazz;
        }

        public List getFieldInfoList()
        {
            return fieldInfoList;
        }

        public int getVariantCount()
        {
            return variantIndex;
        }

        public int var(String s)
        {
            if ((Integer)variants.get(s) == null)
            {
                Map map = variants;
                int i = variantIndex;
                variantIndex = i + 1;
                map.put(s, Integer.valueOf(i));
            }
            return ((Integer)variants.get(s)).intValue();
        }

        public int var(String s, int i)
        {
            if ((Integer)variants.get(s) == null)
            {
                variants.put(s, Integer.valueOf(variantIndex));
                variantIndex = variantIndex + i;
            }
            return ((Integer)variants.get(s)).intValue();
        }

        public Context(String s, ParserConfig parserconfig, DeserializeBeanInfo deserializebeaninfo, int i)
        {
            variantIndex = 5;
            variants = new HashMap();
            className = s;
            clazz = deserializebeaninfo.getClazz();
            variantIndex = i;
            beanInfo = deserializebeaninfo;
            fieldInfoList = new ArrayList(deserializebeaninfo.getFieldList());
        }
    }


    private static final ASMDeserializerFactory instance = new ASMDeserializerFactory();
    private final ASMClassLoader classLoader;
    private final AtomicLong seed;

    public ASMDeserializerFactory()
    {
        seed = new AtomicLong();
        classLoader = new ASMClassLoader();
    }

    public ASMDeserializerFactory(ClassLoader classloader)
    {
        seed = new AtomicLong();
        classLoader = new ASMClassLoader(classloader);
    }

    private void _batchSet(Context context, MethodVisitor methodvisitor)
    {
        _batchSet(context, methodvisitor, true);
    }

    private void _batchSet(Context context, MethodVisitor methodvisitor, boolean flag)
    {
        int j = context.getFieldInfoList().size();
        for (int i = 0; i < j; i++)
        {
            Label label = new Label();
            if (flag)
            {
                _isFlag(methodvisitor, context, i, label);
            }
            _loadAndSet(context, methodvisitor, (FieldInfo)context.getFieldInfoList().get(i));
            if (flag)
            {
                methodvisitor.visitLabel(label);
            }
        }

    }

    private void _createInstance(ClassWriter classwriter, Context context)
    {
        classwriter = classwriter.visitMethod(1, "createInstance", "(Lcom/alibaba/fastjson/parser/DefaultJSONParser;Ljava/lang/reflect/Type;)Ljava/lang/Object;", null, null);
        classwriter.visitTypeInsn(187, ASMUtils.getType(context.getClazz()));
        classwriter.visitInsn(89);
        classwriter.visitMethodInsn(183, ASMUtils.getType(context.getClazz()), "<init>", "()V");
        classwriter.visitInsn(176);
        classwriter.visitMaxs(3, 3);
        classwriter.visitEnd();
    }

    private void _createInstance(Context context, MethodVisitor methodvisitor)
    {
        if (Modifier.isPublic(context.getBeanInfo().getDefaultConstructor().getModifiers()))
        {
            methodvisitor.visitTypeInsn(187, ASMUtils.getType(context.getClazz()));
            methodvisitor.visitInsn(89);
            methodvisitor.visitMethodInsn(183, ASMUtils.getType(context.getClazz()), "<init>", "()V");
            methodvisitor.visitVarInsn(58, context.var("instance"));
            return;
        } else
        {
            methodvisitor.visitVarInsn(25, 0);
            methodvisitor.visitVarInsn(25, 1);
            methodvisitor.visitMethodInsn(183, "com/alibaba/fastjson/parser/deserializer/ASMJavaBeanDeserializer", "createInstance", "(Lcom/alibaba/fastjson/parser/DefaultJSONParser;)Ljava/lang/Object;");
            methodvisitor.visitTypeInsn(192, ASMUtils.getType(context.getClazz()));
            methodvisitor.visitVarInsn(58, context.var("instance"));
            return;
        }
    }

    private void _deserObject(Context context, MethodVisitor methodvisitor, FieldInfo fieldinfo, Class class1)
    {
        _getFieldDeser(context, methodvisitor, fieldinfo);
        methodvisitor.visitVarInsn(25, 1);
        if (fieldinfo.getFieldType() instanceof Class)
        {
            methodvisitor.visitLdcInsn(Type.getType(ASMUtils.getDesc(fieldinfo.getFieldClass())));
        } else
        {
            methodvisitor.visitVarInsn(25, 0);
            methodvisitor.visitLdcInsn(fieldinfo.getName());
            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/deserializer/ASMJavaBeanDeserializer", "getFieldType", "(Ljava/lang/String;)Ljava/lang/reflect/Type;");
        }
        methodvisitor.visitLdcInsn(fieldinfo.getName());
        methodvisitor.visitMethodInsn(185, "com/alibaba/fastjson/parser/deserializer/ObjectDeserializer", "deserialze", "(Lcom/alibaba/fastjson/parser/DefaultJSONParser;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;");
        methodvisitor.visitTypeInsn(192, ASMUtils.getType(class1));
        methodvisitor.visitVarInsn(58, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
    }

    private void _deserialize_endCheck(Context context, MethodVisitor methodvisitor, Label label)
    {
        methodvisitor.visitIntInsn(21, context.var("matchedCount"));
        methodvisitor.visitJumpInsn(158, label);
        methodvisitor.visitVarInsn(25, context.var("lexer"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "token", "()I");
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "RBRACE", "I");
        methodvisitor.visitJumpInsn(160, label);
        methodvisitor.visitVarInsn(25, context.var("lexer"));
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "COMMA", "I");
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "nextToken", "(I)V");
    }

    private void _deserialze_list_obj(Context context, MethodVisitor methodvisitor, Label label, FieldInfo fieldinfo, Class class1, Class class2, int i)
    {
        Label label1 = new Label();
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "matchField", "([C)Z");
        methodvisitor.visitJumpInsn(153, label1);
        _setFlag(methodvisitor, context, i);
        Label label2 = new Label();
        methodvisitor.visitVarInsn(25, context.var("lexer"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "token", "()I");
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "NULL", "I");
        methodvisitor.visitJumpInsn(160, label2);
        methodvisitor.visitVarInsn(25, context.var("lexer"));
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "COMMA", "I");
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "nextToken", "(I)V");
        methodvisitor.visitJumpInsn(167, label1);
        methodvisitor.visitLabel(label2);
        methodvisitor.visitVarInsn(25, context.var("lexer"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "token", "()I");
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "LBRACKET", "I");
        methodvisitor.visitJumpInsn(160, label);
        _getCollectionFieldItemDeser(context, methodvisitor, fieldinfo, class2);
        methodvisitor.visitMethodInsn(185, "com/alibaba/fastjson/parser/deserializer/ObjectDeserializer", "getFastMatchToken", "()I");
        methodvisitor.visitVarInsn(54, context.var("fastMatchToken"));
        methodvisitor.visitVarInsn(25, context.var("lexer"));
        methodvisitor.visitVarInsn(21, context.var("fastMatchToken"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "nextToken", "(I)V");
        _newCollection(methodvisitor, class1);
        methodvisitor.visitVarInsn(58, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "getContext", "()Lcom/alibaba/fastjson/parser/ParseContext;");
        methodvisitor.visitVarInsn(58, context.var("listContext"));
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitVarInsn(25, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
        methodvisitor.visitLdcInsn(fieldinfo.getName());
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "setContext", "(Ljava/lang/Object;Ljava/lang/Object;)Lcom/alibaba/fastjson/parser/ParseContext;");
        methodvisitor.visitInsn(87);
        label2 = new Label();
        Label label3 = new Label();
        methodvisitor.visitInsn(3);
        methodvisitor.visitVarInsn(54, context.var("i"));
        methodvisitor.visitLabel(label2);
        methodvisitor.visitVarInsn(25, context.var("lexer"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "token", "()I");
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "RBRACKET", "I");
        methodvisitor.visitJumpInsn(159, label3);
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(fieldinfo.getName()).append("_asm_list_item_deser__").toString(), "Lcom/alibaba/fastjson/parser/deserializer/ObjectDeserializer;");
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitLdcInsn(Type.getType(ASMUtils.getDesc(class2)));
        methodvisitor.visitVarInsn(21, context.var("i"));
        methodvisitor.visitMethodInsn(184, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
        methodvisitor.visitMethodInsn(185, "com/alibaba/fastjson/parser/deserializer/ObjectDeserializer", "deserialze", "(Lcom/alibaba/fastjson/parser/DefaultJSONParser;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;");
        methodvisitor.visitVarInsn(58, context.var("list_item_value"));
        methodvisitor.visitIincInsn(context.var("i"), 1);
        methodvisitor.visitVarInsn(25, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
        methodvisitor.visitVarInsn(25, context.var("list_item_value"));
        if (class1.isInterface())
        {
            methodvisitor.visitMethodInsn(185, ASMUtils.getType(class1), "add", "(Ljava/lang/Object;)Z");
        } else
        {
            methodvisitor.visitMethodInsn(182, ASMUtils.getType(class1), "add", "(Ljava/lang/Object;)Z");
        }
        methodvisitor.visitInsn(87);
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitVarInsn(25, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "checkListResolve", "(Ljava/util/Collection;)V");
        methodvisitor.visitVarInsn(25, context.var("lexer"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "token", "()I");
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "COMMA", "I");
        methodvisitor.visitJumpInsn(160, label2);
        methodvisitor.visitVarInsn(25, context.var("lexer"));
        methodvisitor.visitVarInsn(21, context.var("fastMatchToken"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "nextToken", "(I)V");
        methodvisitor.visitJumpInsn(167, label2);
        methodvisitor.visitLabel(label3);
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitVarInsn(25, context.var("listContext"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "setContext", "(Lcom/alibaba/fastjson/parser/ParseContext;)V");
        methodvisitor.visitVarInsn(25, context.var("lexer"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "token", "()I");
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "RBRACKET", "I");
        methodvisitor.visitJumpInsn(160, label);
        methodvisitor.visitVarInsn(25, context.var("lexer"));
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "COMMA", "I");
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "nextToken", "(I)V");
        methodvisitor.visitLabel(label1);
    }

    private void _deserialze_obj(Context context, MethodVisitor methodvisitor, Label label, FieldInfo fieldinfo, Class class1, int i)
    {
        label = new Label();
        Label label1 = new Label();
        methodvisitor.visitVarInsn(25, context.var("lexer"));
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(fieldinfo.getName()).append("_asm_prefix__").toString(), "[C");
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "matchField", "([C)Z");
        methodvisitor.visitJumpInsn(154, label);
        methodvisitor.visitInsn(1);
        methodvisitor.visitVarInsn(58, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
        methodvisitor.visitJumpInsn(167, label1);
        methodvisitor.visitLabel(label);
        _setFlag(methodvisitor, context, i);
        methodvisitor.visitVarInsn(21, context.var("matchedCount"));
        methodvisitor.visitInsn(4);
        methodvisitor.visitInsn(96);
        methodvisitor.visitVarInsn(54, context.var("matchedCount"));
        _deserObject(context, methodvisitor, fieldinfo, class1);
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "getResolveStatus", "()I");
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/parser/DefaultJSONParser", "NeedToResolve", "I");
        methodvisitor.visitJumpInsn(160, label1);
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "getLastResolveTask", "()Lcom/alibaba/fastjson/parser/DefaultJSONParser$ResolveTask;");
        methodvisitor.visitVarInsn(58, context.var("resolveTask"));
        methodvisitor.visitVarInsn(25, context.var("resolveTask"));
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "getContext", "()Lcom/alibaba/fastjson/parser/ParseContext;");
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser$ResolveTask", "setOwnerContext", "(Lcom/alibaba/fastjson/parser/ParseContext;)V");
        methodvisitor.visitVarInsn(25, context.var("resolveTask"));
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitLdcInsn(fieldinfo.getName());
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/deserializer/ASMJavaBeanDeserializer", "getFieldDeserializer", "(Ljava/lang/String;)Lcom/alibaba/fastjson/parser/deserializer/FieldDeserializer;");
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser$ResolveTask", "setFieldDeserializer", "(Lcom/alibaba/fastjson/parser/deserializer/FieldDeserializer;)V");
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/parser/DefaultJSONParser", "NONE", "I");
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "setResolveStatus", "(I)V");
        methodvisitor.visitLabel(label1);
    }

    private void _getCollectionFieldItemDeser(Context context, MethodVisitor methodvisitor, FieldInfo fieldinfo, Class class1)
    {
        Label label = new Label();
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(fieldinfo.getName()).append("_asm_list_item_deser__").toString(), "Lcom/alibaba/fastjson/parser/deserializer/ObjectDeserializer;");
        methodvisitor.visitJumpInsn(199, label);
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "getConfig", "()Lcom/alibaba/fastjson/parser/ParserConfig;");
        methodvisitor.visitLdcInsn(Type.getType(ASMUtils.getDesc(class1)));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/ParserConfig", "getDeserializer", "(Ljava/lang/reflect/Type;)Lcom/alibaba/fastjson/parser/deserializer/ObjectDeserializer;");
        methodvisitor.visitFieldInsn(181, context.getClassName(), (new StringBuilder()).append(fieldinfo.getName()).append("_asm_list_item_deser__").toString(), "Lcom/alibaba/fastjson/parser/deserializer/ObjectDeserializer;");
        methodvisitor.visitLabel(label);
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(fieldinfo.getName()).append("_asm_list_item_deser__").toString(), "Lcom/alibaba/fastjson/parser/deserializer/ObjectDeserializer;");
    }

    private void _getFieldDeser(Context context, MethodVisitor methodvisitor, FieldInfo fieldinfo)
    {
        Label label = new Label();
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(fieldinfo.getName()).append("_asm_deser__").toString(), "Lcom/alibaba/fastjson/parser/deserializer/ObjectDeserializer;");
        methodvisitor.visitJumpInsn(199, label);
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "getConfig", "()Lcom/alibaba/fastjson/parser/ParserConfig;");
        methodvisitor.visitLdcInsn(Type.getType(ASMUtils.getDesc(fieldinfo.getFieldClass())));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/ParserConfig", "getDeserializer", "(Ljava/lang/reflect/Type;)Lcom/alibaba/fastjson/parser/deserializer/ObjectDeserializer;");
        methodvisitor.visitFieldInsn(181, context.getClassName(), (new StringBuilder()).append(fieldinfo.getName()).append("_asm_deser__").toString(), "Lcom/alibaba/fastjson/parser/deserializer/ObjectDeserializer;");
        methodvisitor.visitLabel(label);
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(fieldinfo.getName()).append("_asm_deser__").toString(), "Lcom/alibaba/fastjson/parser/deserializer/ObjectDeserializer;");
    }

    private void _init(ClassWriter classwriter, Context context)
    {
        int l = context.getFieldInfoList().size();
        for (int i = 0; i < l; i++)
        {
            FieldInfo fieldinfo = (FieldInfo)context.getFieldInfoList().get(i);
            classwriter.visitField(1, (new StringBuilder()).append(fieldinfo.getName()).append("_asm_prefix__").toString(), "[C").visitEnd();
        }

        l = context.getFieldInfoList().size();
        int j = 0;
        do
        {
            if (j >= l)
            {
                break;
            }
            FieldInfo fieldinfo1 = (FieldInfo)context.getFieldInfoList().get(j);
            Class class1 = fieldinfo1.getFieldClass();
            if (!class1.isPrimitive() && !class1.isEnum())
            {
                if (java/util/Collection.isAssignableFrom(class1))
                {
                    classwriter.visitField(1, (new StringBuilder()).append(fieldinfo1.getName()).append("_asm_list_item_deser__").toString(), "Lcom/alibaba/fastjson/parser/deserializer/ObjectDeserializer;").visitEnd();
                } else
                {
                    classwriter.visitField(1, (new StringBuilder()).append(fieldinfo1.getName()).append("_asm_deser__").toString(), "Lcom/alibaba/fastjson/parser/deserializer/ObjectDeserializer;").visitEnd();
                }
            }
            j++;
        } while (true);
        classwriter = classwriter.visitMethod(1, "<init>", "(Lcom/alibaba/fastjson/parser/ParserConfig;Ljava/lang/Class;)V", null, null);
        classwriter.visitVarInsn(25, 0);
        classwriter.visitVarInsn(25, 1);
        classwriter.visitVarInsn(25, 2);
        classwriter.visitMethodInsn(183, "com/alibaba/fastjson/parser/deserializer/ASMJavaBeanDeserializer", "<init>", "(Lcom/alibaba/fastjson/parser/ParserConfig;Ljava/lang/Class;)V");
        classwriter.visitVarInsn(25, 0);
        classwriter.visitFieldInsn(180, "com/alibaba/fastjson/parser/deserializer/ASMJavaBeanDeserializer", "serializer", "Lcom/alibaba/fastjson/parser/deserializer/ASMJavaBeanDeserializer$InnerJavaBeanDeserializer;");
        classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/deserializer/JavaBeanDeserializer", "getFieldDeserializerMap", "()Ljava/util/Map;");
        classwriter.visitInsn(87);
        l = context.getFieldInfoList().size();
        for (int k = 0; k < l; k++)
        {
            FieldInfo fieldinfo2 = (FieldInfo)context.getFieldInfoList().get(k);
            classwriter.visitVarInsn(25, 0);
            classwriter.visitLdcInsn((new StringBuilder()).append("\"").append(fieldinfo2.getName()).append("\":").toString());
            classwriter.visitMethodInsn(182, "java/lang/String", "toCharArray", "()[C");
            classwriter.visitFieldInsn(181, context.getClassName(), (new StringBuilder()).append(fieldinfo2.getName()).append("_asm_prefix__").toString(), "[C");
        }

        classwriter.visitInsn(177);
        classwriter.visitMaxs(4, 4);
        classwriter.visitEnd();
    }

    private void _isEnable(Context context, MethodVisitor methodvisitor, Feature feature)
    {
        methodvisitor.visitVarInsn(25, context.var("lexer"));
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/parser/Feature", feature.name(), "Lcom/alibaba/fastjson/parser/Feature;");
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "isEnabled", "(Lcom/alibaba/fastjson/parser/Feature;)Z");
    }

    private void _loadAndSet(Context context, MethodVisitor methodvisitor, FieldInfo fieldinfo)
    {
        Class class1 = fieldinfo.getFieldClass();
        java.lang.reflect.Type type = fieldinfo.getFieldType();
        if (class1 == Boolean.TYPE)
        {
            methodvisitor.visitVarInsn(25, context.var("instance"));
            methodvisitor.visitVarInsn(21, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
            _set(context, methodvisitor, fieldinfo);
        } else
        {
            if (class1 == Byte.TYPE || class1 == Short.TYPE || class1 == Integer.TYPE || class1 == Character.TYPE)
            {
                methodvisitor.visitVarInsn(25, context.var("instance"));
                methodvisitor.visitVarInsn(21, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                _set(context, methodvisitor, fieldinfo);
                return;
            }
            if (class1 == Long.TYPE)
            {
                methodvisitor.visitVarInsn(25, context.var("instance"));
                methodvisitor.visitVarInsn(22, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString(), 2));
                if (fieldinfo.getMethod() != null)
                {
                    methodvisitor.visitMethodInsn(182, ASMUtils.getType(context.getClazz()), fieldinfo.getMethod().getName(), ASMUtils.getDesc(fieldinfo.getMethod()));
                    if (!fieldinfo.getMethod().getReturnType().equals(Void.TYPE))
                    {
                        methodvisitor.visitInsn(87);
                        return;
                    }
                } else
                {
                    methodvisitor.visitFieldInsn(181, ASMUtils.getType(fieldinfo.getDeclaringClass()), fieldinfo.getField().getName(), ASMUtils.getDesc(fieldinfo.getFieldClass()));
                    return;
                }
            } else
            {
                if (class1 == Float.TYPE)
                {
                    methodvisitor.visitVarInsn(25, context.var("instance"));
                    methodvisitor.visitVarInsn(23, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                    _set(context, methodvisitor, fieldinfo);
                    return;
                }
                if (class1 == Double.TYPE)
                {
                    methodvisitor.visitVarInsn(25, context.var("instance"));
                    methodvisitor.visitVarInsn(24, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString(), 2));
                    _set(context, methodvisitor, fieldinfo);
                    return;
                }
                if (class1 == java/lang/String)
                {
                    methodvisitor.visitVarInsn(25, context.var("instance"));
                    methodvisitor.visitVarInsn(25, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                    _set(context, methodvisitor, fieldinfo);
                    return;
                }
                if (class1.isEnum())
                {
                    methodvisitor.visitVarInsn(25, context.var("instance"));
                    methodvisitor.visitVarInsn(25, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                    _set(context, methodvisitor, fieldinfo);
                    return;
                }
                if (java/util/Collection.isAssignableFrom(class1))
                {
                    methodvisitor.visitVarInsn(25, context.var("instance"));
                    if (TypeUtils.getCollectionItemClass(type) == java/lang/String)
                    {
                        methodvisitor.visitVarInsn(25, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                        methodvisitor.visitTypeInsn(192, ASMUtils.getType(class1));
                    } else
                    {
                        methodvisitor.visitVarInsn(25, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                    }
                    _set(context, methodvisitor, fieldinfo);
                    return;
                } else
                {
                    methodvisitor.visitVarInsn(25, context.var("instance"));
                    methodvisitor.visitVarInsn(25, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                    _set(context, methodvisitor, fieldinfo);
                    return;
                }
            }
        }
    }

    private void _newCollection(MethodVisitor methodvisitor, Class class1)
    {
        if (class1.isAssignableFrom(java/util/ArrayList))
        {
            methodvisitor.visitTypeInsn(187, "java/util/ArrayList");
            methodvisitor.visitInsn(89);
            methodvisitor.visitMethodInsn(183, "java/util/ArrayList", "<init>", "()V");
        } else
        if (class1.isAssignableFrom(java/util/LinkedList))
        {
            methodvisitor.visitTypeInsn(187, ASMUtils.getType(java/util/LinkedList));
            methodvisitor.visitInsn(89);
            methodvisitor.visitMethodInsn(183, ASMUtils.getType(java/util/LinkedList), "<init>", "()V");
        } else
        if (class1.isAssignableFrom(java/util/HashSet))
        {
            methodvisitor.visitTypeInsn(187, ASMUtils.getType(java/util/HashSet));
            methodvisitor.visitInsn(89);
            methodvisitor.visitMethodInsn(183, ASMUtils.getType(java/util/HashSet), "<init>", "()V");
        } else
        if (class1.isAssignableFrom(java/util/TreeSet))
        {
            methodvisitor.visitTypeInsn(187, ASMUtils.getType(java/util/TreeSet));
            methodvisitor.visitInsn(89);
            methodvisitor.visitMethodInsn(183, ASMUtils.getType(java/util/TreeSet), "<init>", "()V");
        } else
        {
            methodvisitor.visitTypeInsn(187, ASMUtils.getType(class1));
            methodvisitor.visitInsn(89);
            methodvisitor.visitMethodInsn(183, ASMUtils.getType(class1), "<init>", "()V");
        }
        methodvisitor.visitTypeInsn(192, ASMUtils.getType(class1));
    }

    private void _set(Context context, MethodVisitor methodvisitor, FieldInfo fieldinfo)
    {
        if (fieldinfo.getMethod() != null)
        {
            methodvisitor.visitMethodInsn(182, ASMUtils.getType(fieldinfo.getDeclaringClass()), fieldinfo.getMethod().getName(), ASMUtils.getDesc(fieldinfo.getMethod()));
            if (!fieldinfo.getMethod().getReturnType().equals(Void.TYPE))
            {
                methodvisitor.visitInsn(87);
            }
            return;
        } else
        {
            methodvisitor.visitFieldInsn(181, ASMUtils.getType(fieldinfo.getDeclaringClass()), fieldinfo.getField().getName(), ASMUtils.getDesc(fieldinfo.getFieldClass()));
            return;
        }
    }

    private void _setContext(Context context, MethodVisitor methodvisitor)
    {
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitVarInsn(25, context.var("context"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "setContext", "(Lcom/alibaba/fastjson/parser/ParseContext;)V");
        Label label = new Label();
        methodvisitor.visitVarInsn(25, context.var("childContext"));
        methodvisitor.visitJumpInsn(198, label);
        methodvisitor.visitVarInsn(25, context.var("childContext"));
        methodvisitor.visitVarInsn(25, context.var("instance"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/ParseContext", "setObject", "(Ljava/lang/Object;)V");
        methodvisitor.visitLabel(label);
    }

    private void defineVarLexer(Context context, MethodVisitor methodvisitor)
    {
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "getLexer", "()Lcom/alibaba/fastjson/parser/JSONLexer;");
        methodvisitor.visitTypeInsn(192, "com/alibaba/fastjson/parser/JSONLexerBase");
        methodvisitor.visitVarInsn(58, context.var("lexer"));
    }

    public static final ASMDeserializerFactory getInstance()
    {
        return instance;
    }

    void _deserialze(ClassWriter classwriter, Context context)
    {
        if (context.getFieldInfoList().size() != 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj = context.getFieldInfoList().iterator();
_L5:
        if (!((Iterator) (obj)).hasNext()) goto _L4; else goto _L3
_L3:
        Object obj1;
        Object obj2;
        obj2 = (FieldInfo)((Iterator) (obj)).next();
        obj1 = ((FieldInfo) (obj2)).getFieldClass();
        obj2 = ((FieldInfo) (obj2)).getFieldType();
        if (obj1 == Character.TYPE || java/util/Collection.isAssignableFrom(((Class) (obj1))) && (!(obj2 instanceof ParameterizedType) || !(((ParameterizedType)obj2).getActualTypeArguments()[0] instanceof Class))) goto _L1; else goto _L5
_L4:
        Label label;
        int j;
        int k;
        Collections.sort(context.getFieldInfoList());
        classwriter = classwriter.visitMethod(1, "deserialze", "(Lcom/alibaba/fastjson/parser/DefaultJSONParser;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;", null, null);
        obj = new Label();
        obj1 = new Label();
        obj2 = new Label();
        label = new Label();
        defineVarLexer(context, classwriter);
        _isEnable(context, classwriter, Feature.SortFeidFastMatch);
        classwriter.visitJumpInsn(153, ((Label) (obj1)));
        Label label1 = new Label();
        classwriter.visitVarInsn(25, 0);
        classwriter.visitVarInsn(25, context.var("lexer"));
        classwriter.visitMethodInsn(183, "com/alibaba/fastjson/parser/deserializer/ASMJavaBeanDeserializer", "isSupportArrayToBean", "(Lcom/alibaba/fastjson/parser/JSONLexer;)Z");
        classwriter.visitJumpInsn(153, label1);
        classwriter.visitVarInsn(25, context.var("lexer"));
        classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "token", "()I");
        classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "LBRACKET", "I");
        classwriter.visitJumpInsn(160, label1);
        classwriter.visitVarInsn(25, 0);
        classwriter.visitVarInsn(25, 1);
        classwriter.visitVarInsn(25, 2);
        classwriter.visitVarInsn(25, 3);
        classwriter.visitMethodInsn(183, context.getClassName(), "deserialzeArrayMapping", "(Lcom/alibaba/fastjson/parser/DefaultJSONParser;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;");
        classwriter.visitInsn(176);
        classwriter.visitLabel(label1);
        classwriter.visitVarInsn(25, context.var("lexer"));
        classwriter.visitLdcInsn(context.getClazz().getName());
        classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanType", "(Ljava/lang/String;)I");
        classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONLexerBase", "NOT_MATCH", "I");
        classwriter.visitJumpInsn(159, ((Label) (obj1)));
        classwriter.visitVarInsn(25, 1);
        classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "getContext", "()Lcom/alibaba/fastjson/parser/ParseContext;");
        classwriter.visitVarInsn(58, context.var("mark_context"));
        classwriter.visitInsn(3);
        classwriter.visitVarInsn(54, context.var("matchedCount"));
        _createInstance(context, classwriter);
        classwriter.visitVarInsn(25, 1);
        classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "getContext", "()Lcom/alibaba/fastjson/parser/ParseContext;");
        classwriter.visitVarInsn(58, context.var("context"));
        classwriter.visitVarInsn(25, 1);
        classwriter.visitVarInsn(25, context.var("context"));
        classwriter.visitVarInsn(25, context.var("instance"));
        classwriter.visitVarInsn(25, 3);
        classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "setContext", "(Lcom/alibaba/fastjson/parser/ParseContext;Ljava/lang/Object;Ljava/lang/Object;)Lcom/alibaba/fastjson/parser/ParseContext;");
        classwriter.visitVarInsn(58, context.var("childContext"));
        classwriter.visitVarInsn(25, context.var("lexer"));
        classwriter.visitFieldInsn(180, "com/alibaba/fastjson/parser/JSONLexerBase", "matchStat", "I");
        classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONLexerBase", "END", "I");
        classwriter.visitJumpInsn(159, ((Label) (obj2)));
        classwriter.visitInsn(3);
        classwriter.visitIntInsn(54, context.var("matchStat"));
        k = context.getFieldInfoList().size();
        for (int i = 0; i < k; i += 32)
        {
            classwriter.visitInsn(3);
            classwriter.visitVarInsn(54, context.var((new StringBuilder()).append("_asm_flag_").append(i / 32).toString()));
        }

        j = 0;
_L7:
        if (j >= k)
        {
            break; /* Loop/switch isn't completed */
        }
        FieldInfo fieldinfo = (FieldInfo)context.getFieldInfoList().get(j);
        Class class1 = fieldinfo.getFieldClass();
        if (class1 == Boolean.TYPE || class1 == Byte.TYPE || class1 == Short.TYPE || class1 == Integer.TYPE)
        {
            classwriter.visitInsn(3);
            classwriter.visitVarInsn(54, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
        } else
        if (class1 == Long.TYPE)
        {
            classwriter.visitInsn(9);
            classwriter.visitVarInsn(55, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString(), 2));
        } else
        if (class1 == Float.TYPE)
        {
            classwriter.visitInsn(11);
            classwriter.visitVarInsn(56, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
        } else
        if (class1 == Double.TYPE)
        {
            classwriter.visitInsn(14);
            classwriter.visitVarInsn(57, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString(), 2));
        } else
        {
            if (class1 == java/lang/String)
            {
                Label label3 = new Label();
                _isEnable(context, classwriter, Feature.InitStringFieldAsEmpty);
                classwriter.visitJumpInsn(153, label3);
                _setFlag(classwriter, context, j);
                classwriter.visitLabel(label3);
                classwriter.visitVarInsn(25, context.var("lexer"));
                classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "stringDefaultValue", "()Ljava/lang/String;");
            } else
            {
                classwriter.visitInsn(1);
            }
            classwriter.visitTypeInsn(192, ASMUtils.getType(class1));
            classwriter.visitVarInsn(58, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
        }
_L10:
        j++;
        if (true) goto _L7; else goto _L6
_L6:
        j = 0;
_L8:
        Object obj3;
        Class class2;
        Object obj4;
        if (j >= k)
        {
            break MISSING_BLOCK_LABEL_3239;
        }
        obj3 = (FieldInfo)context.getFieldInfoList().get(j);
        class2 = ((FieldInfo) (obj3)).getFieldClass();
        obj4 = ((FieldInfo) (obj3)).getFieldType();
        Label label2 = new Label();
        if (class2 == Boolean.TYPE)
        {
            classwriter.visitVarInsn(25, context.var("lexer"));
            classwriter.visitVarInsn(25, 0);
            classwriter.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm_prefix__").toString(), "[C");
            classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanFieldBoolean", "([C)Z");
            classwriter.visitVarInsn(54, context.var((new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm").toString()));
        } else
        if (class2 == Byte.TYPE)
        {
            classwriter.visitVarInsn(25, context.var("lexer"));
            classwriter.visitVarInsn(25, 0);
            classwriter.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm_prefix__").toString(), "[C");
            classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanFieldInt", "([C)I");
            classwriter.visitVarInsn(54, context.var((new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm").toString()));
        } else
        if (class2 == Short.TYPE)
        {
            classwriter.visitVarInsn(25, context.var("lexer"));
            classwriter.visitVarInsn(25, 0);
            classwriter.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm_prefix__").toString(), "[C");
            classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanFieldInt", "([C)I");
            classwriter.visitVarInsn(54, context.var((new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm").toString()));
        } else
        if (class2 == Integer.TYPE)
        {
            classwriter.visitVarInsn(25, context.var("lexer"));
            classwriter.visitVarInsn(25, 0);
            classwriter.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm_prefix__").toString(), "[C");
            classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanFieldInt", "([C)I");
            classwriter.visitVarInsn(54, context.var((new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm").toString()));
        } else
        if (class2 == Long.TYPE)
        {
            classwriter.visitVarInsn(25, context.var("lexer"));
            classwriter.visitVarInsn(25, 0);
            classwriter.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm_prefix__").toString(), "[C");
            classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanFieldLong", "([C)J");
            classwriter.visitVarInsn(55, context.var((new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm").toString(), 2));
        } else
        if (class2 == Float.TYPE)
        {
            classwriter.visitVarInsn(25, context.var("lexer"));
            classwriter.visitVarInsn(25, 0);
            classwriter.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm_prefix__").toString(), "[C");
            classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanFieldFloat", "([C)F");
            classwriter.visitVarInsn(56, context.var((new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm").toString()));
        } else
        if (class2 == Double.TYPE)
        {
            classwriter.visitVarInsn(25, context.var("lexer"));
            classwriter.visitVarInsn(25, 0);
            classwriter.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm_prefix__").toString(), "[C");
            classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanFieldDouble", "([C)D");
            classwriter.visitVarInsn(57, context.var((new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm").toString(), 2));
        } else
        if (class2 == java/lang/String)
        {
            classwriter.visitVarInsn(25, context.var("lexer"));
            classwriter.visitVarInsn(25, 0);
            classwriter.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm_prefix__").toString(), "[C");
            classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanFieldString", "([C)Ljava/lang/String;");
            classwriter.visitVarInsn(58, context.var((new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm").toString()));
        } else
        if (class2.isEnum())
        {
            classwriter.visitVarInsn(25, context.var("lexer"));
            classwriter.visitVarInsn(25, 0);
            classwriter.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm_prefix__").toString(), "[C");
            obj4 = new Label();
            classwriter.visitInsn(1);
            classwriter.visitTypeInsn(192, ASMUtils.getType(class2));
            classwriter.visitVarInsn(58, context.var((new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm").toString()));
            classwriter.visitVarInsn(25, 1);
            classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "getSymbolTable", "()Lcom/alibaba/fastjson/parser/SymbolTable;");
            classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanFieldSymbol", "([CLcom/alibaba/fastjson/parser/SymbolTable;)Ljava/lang/String;");
            classwriter.visitInsn(89);
            classwriter.visitVarInsn(58, context.var((new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm_enumName").toString()));
            classwriter.visitJumpInsn(198, ((Label) (obj4)));
            classwriter.visitVarInsn(25, context.var((new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm_enumName").toString()));
            classwriter.visitMethodInsn(184, ASMUtils.getType(class2), "valueOf", (new StringBuilder()).append("(Ljava/lang/String;)").append(ASMUtils.getDesc(class2)).toString());
            classwriter.visitVarInsn(58, context.var((new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm").toString()));
            classwriter.visitLabel(((Label) (obj4)));
        } else
        {
label0:
            {
                if (!java/util/Collection.isAssignableFrom(class2))
                {
                    break MISSING_BLOCK_LABEL_3207;
                }
                classwriter.visitVarInsn(25, context.var("lexer"));
                classwriter.visitVarInsn(25, 0);
                classwriter.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm_prefix__").toString(), "[C");
                obj4 = TypeUtils.getCollectionItemClass(((java.lang.reflect.Type) (obj4)));
                if (obj4 != java/lang/String)
                {
                    break label0;
                }
                classwriter.visitLdcInsn(Type.getType(ASMUtils.getDesc(class2)));
                classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanFieldStringArray", (new StringBuilder()).append("([CLjava/lang/Class;)").append(ASMUtils.getDesc(java/util/Collection)).toString());
                classwriter.visitVarInsn(58, context.var((new StringBuilder()).append(((FieldInfo) (obj3)).getName()).append("_asm").toString()));
            }
        }
        classwriter.visitVarInsn(25, context.var("lexer"));
        classwriter.visitFieldInsn(180, "com/alibaba/fastjson/parser/JSONLexerBase", "matchStat", "I");
        obj3 = new Label();
        classwriter.visitJumpInsn(158, ((Label) (obj3)));
        _setFlag(classwriter, context, j);
        classwriter.visitLabel(((Label) (obj3)));
        classwriter.visitVarInsn(25, context.var("lexer"));
        classwriter.visitFieldInsn(180, "com/alibaba/fastjson/parser/JSONLexerBase", "matchStat", "I");
        classwriter.visitInsn(89);
        classwriter.visitVarInsn(54, context.var("matchStat"));
        classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONLexerBase", "NOT_MATCH", "I");
        classwriter.visitJumpInsn(159, ((Label) (obj)));
        classwriter.visitVarInsn(25, context.var("lexer"));
        classwriter.visitFieldInsn(180, "com/alibaba/fastjson/parser/JSONLexerBase", "matchStat", "I");
        classwriter.visitJumpInsn(158, label2);
        classwriter.visitVarInsn(21, context.var("matchedCount"));
        classwriter.visitInsn(4);
        classwriter.visitInsn(96);
        classwriter.visitVarInsn(54, context.var("matchedCount"));
        classwriter.visitVarInsn(25, context.var("lexer"));
        classwriter.visitFieldInsn(180, "com/alibaba/fastjson/parser/JSONLexerBase", "matchStat", "I");
        classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONLexerBase", "END", "I");
        classwriter.visitJumpInsn(159, label);
        classwriter.visitLabel(label2);
        if (j == k - 1)
        {
            classwriter.visitVarInsn(25, context.var("lexer"));
            classwriter.visitFieldInsn(180, "com/alibaba/fastjson/parser/JSONLexerBase", "matchStat", "I");
            classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONLexerBase", "END", "I");
            classwriter.visitJumpInsn(160, ((Label) (obj)));
        }
_L9:
        j++;
          goto _L8
        _deserialze_list_obj(context, classwriter, ((Label) (obj)), ((FieldInfo) (obj3)), class2, ((Class) (obj4)), j);
        if (j == k - 1)
        {
            _deserialize_endCheck(context, classwriter, ((Label) (obj)));
        }
          goto _L9
        _deserialze_obj(context, classwriter, ((Label) (obj)), ((FieldInfo) (obj3)), class2, j);
        if (j == k - 1)
        {
            _deserialize_endCheck(context, classwriter, ((Label) (obj)));
        }
          goto _L9
        classwriter.visitLabel(label);
        if (!context.getClazz().isInterface() && !Modifier.isAbstract(context.getClazz().getModifiers()))
        {
            _batchSet(context, classwriter);
        }
        classwriter.visitLabel(((Label) (obj2)));
        _setContext(context, classwriter);
        classwriter.visitVarInsn(25, context.var("instance"));
        classwriter.visitInsn(176);
        classwriter.visitLabel(((Label) (obj)));
        _batchSet(context, classwriter);
        classwriter.visitVarInsn(25, 0);
        classwriter.visitVarInsn(25, 1);
        classwriter.visitVarInsn(25, 2);
        classwriter.visitVarInsn(25, 3);
        classwriter.visitVarInsn(25, context.var("instance"));
        classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/deserializer/ASMJavaBeanDeserializer", "parseRest", "(Lcom/alibaba/fastjson/parser/DefaultJSONParser;Ljava/lang/reflect/Type;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
        classwriter.visitTypeInsn(192, ASMUtils.getType(context.getClazz()));
        classwriter.visitInsn(176);
        classwriter.visitLabel(((Label) (obj1)));
        classwriter.visitVarInsn(25, 0);
        classwriter.visitVarInsn(25, 1);
        classwriter.visitVarInsn(25, 2);
        classwriter.visitVarInsn(25, 3);
        classwriter.visitMethodInsn(183, "com/alibaba/fastjson/parser/deserializer/ASMJavaBeanDeserializer", "deserialze", "(Lcom/alibaba/fastjson/parser/DefaultJSONParser;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;");
        classwriter.visitInsn(176);
        classwriter.visitMaxs(5, context.getVariantCount());
        classwriter.visitEnd();
        return;
          goto _L10
    }

    void _deserialzeArrayMapping(ClassWriter classwriter, Context context)
    {
        classwriter = classwriter.visitMethod(1, "deserialzeArrayMapping", "(Lcom/alibaba/fastjson/parser/DefaultJSONParser;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;", null, null);
        defineVarLexer(context, classwriter);
        _createInstance(context, classwriter);
        List list = context.getBeanInfo().getSortedFieldList();
        int j = list.size();
        int i = 0;
        do
        {
            if (i < j)
            {
                FieldInfo fieldinfo;
                Class class1;
                Object obj;
                boolean flag;
                byte byte0;
                if (i == j - 1)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                if (flag)
                {
                    byte0 = 93;
                } else
                {
                    byte0 = 44;
                }
                fieldinfo = (FieldInfo)list.get(i);
                class1 = fieldinfo.getFieldClass();
                obj = fieldinfo.getFieldType();
                if (class1 == Byte.TYPE || class1 == Short.TYPE || class1 == Integer.TYPE)
                {
                    classwriter.visitVarInsn(25, context.var("lexer"));
                    classwriter.visitVarInsn(16, byte0);
                    classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanInt", "(C)I");
                    classwriter.visitVarInsn(54, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                } else
                if (class1 == Long.TYPE)
                {
                    classwriter.visitVarInsn(25, context.var("lexer"));
                    classwriter.visitVarInsn(16, byte0);
                    classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanLong", "(C)J");
                    classwriter.visitVarInsn(55, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString(), 2));
                } else
                if (class1 == Boolean.TYPE)
                {
                    classwriter.visitVarInsn(25, context.var("lexer"));
                    classwriter.visitVarInsn(16, byte0);
                    classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanBoolean", "(C)Z");
                    classwriter.visitVarInsn(54, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                } else
                if (class1 == Float.TYPE)
                {
                    classwriter.visitVarInsn(25, context.var("lexer"));
                    classwriter.visitVarInsn(16, byte0);
                    classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanFloat", "(C)F");
                    classwriter.visitVarInsn(56, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                } else
                if (class1 == Double.TYPE)
                {
                    classwriter.visitVarInsn(25, context.var("lexer"));
                    classwriter.visitVarInsn(16, byte0);
                    classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanDouble", "(C)D");
                    classwriter.visitVarInsn(57, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString(), 2));
                } else
                if (class1 == Character.TYPE)
                {
                    classwriter.visitVarInsn(25, context.var("lexer"));
                    classwriter.visitVarInsn(16, byte0);
                    classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanString", "(C)Ljava/lang/String;");
                    classwriter.visitInsn(3);
                    classwriter.visitMethodInsn(182, "java/lang/String", "charAt", "(I)C");
                    classwriter.visitVarInsn(54, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                } else
                if (class1 == java/lang/String)
                {
                    classwriter.visitVarInsn(25, context.var("lexer"));
                    classwriter.visitVarInsn(16, byte0);
                    classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanString", "(C)Ljava/lang/String;");
                    classwriter.visitVarInsn(58, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                } else
                if (class1.isEnum())
                {
                    classwriter.visitVarInsn(25, context.var("lexer"));
                    classwriter.visitLdcInsn(Type.getType(ASMUtils.getDesc(class1)));
                    classwriter.visitVarInsn(25, 1);
                    classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "getSymbolTable", "()Lcom/alibaba/fastjson/parser/SymbolTable;");
                    classwriter.visitVarInsn(16, byte0);
                    classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanEnum", "(Ljava/lang/Class;Lcom/alibaba/fastjson/parser/SymbolTable;C)Ljava/lang/Enum;");
                    classwriter.visitTypeInsn(192, ASMUtils.getType(class1));
                    classwriter.visitVarInsn(58, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                } else
                if (java/util/Collection.isAssignableFrom(class1))
                {
                    obj = TypeUtils.getCollectionItemClass(((java.lang.reflect.Type) (obj)));
                    if (obj == java/lang/String)
                    {
                        classwriter.visitVarInsn(25, context.var("lexer"));
                        classwriter.visitLdcInsn(Type.getType(ASMUtils.getDesc(class1)));
                        classwriter.visitVarInsn(16, byte0);
                        classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "scanStringArray", "(Ljava/lang/Class;C)Ljava/util/Collection;");
                        classwriter.visitVarInsn(58, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                    } else
                    {
                        classwriter.visitVarInsn(25, 1);
                        if (i == 0)
                        {
                            classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "LBRACKET", "I");
                        } else
                        {
                            classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "COMMA", "I");
                        }
                        classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "LBRACKET", "I");
                        classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "accept", "(II)V");
                        _newCollection(classwriter, class1);
                        classwriter.visitInsn(89);
                        classwriter.visitVarInsn(58, context.var((new StringBuilder()).append(fieldinfo.getName()).append("_asm").toString()));
                        _getCollectionFieldItemDeser(context, classwriter, fieldinfo, ((Class) (obj)));
                        classwriter.visitVarInsn(25, 1);
                        classwriter.visitLdcInsn(Type.getType(ASMUtils.getDesc(((Class) (obj)))));
                        classwriter.visitVarInsn(25, 3);
                        classwriter.visitMethodInsn(184, "com/alibaba/fastjson/util/ASMUtils", "parseArray", "(Ljava/util/Collection;Lcom/alibaba/fastjson/parser/deserializer/ObjectDeserializer;Lcom/alibaba/fastjson/parser/DefaultJSONParser;Ljava/lang/reflect/Type;Ljava/lang/Object;)V");
                    }
                } else
                {
                    classwriter.visitVarInsn(25, 1);
                    if (i == 0)
                    {
                        classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "LBRACKET", "I");
                    } else
                    {
                        classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "COMMA", "I");
                    }
                    classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "LBRACKET", "I");
                    classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "accept", "(II)V");
                    _deserObject(context, classwriter, fieldinfo, class1);
                    classwriter.visitVarInsn(25, 1);
                    if (!flag)
                    {
                        classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "COMMA", "I");
                        classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "LBRACKET", "I");
                    } else
                    {
                        classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "RBRACKET", "I");
                        classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "EOF", "I");
                    }
                    classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/DefaultJSONParser", "accept", "(II)V");
                }
            } else
            {
                _batchSet(context, classwriter, false);
                classwriter.visitVarInsn(25, context.var("lexer"));
                classwriter.visitFieldInsn(178, "com/alibaba/fastjson/parser/JSONToken", "COMMA", "I");
                classwriter.visitMethodInsn(182, "com/alibaba/fastjson/parser/JSONLexerBase", "nextToken", "(I)V");
                classwriter.visitVarInsn(25, context.var("instance"));
                classwriter.visitInsn(176);
                classwriter.visitMaxs(5, context.getVariantCount());
                classwriter.visitEnd();
                return;
            }
            i++;
        } while (true);
    }

    void _isFlag(MethodVisitor methodvisitor, Context context, int i, Label label)
    {
        methodvisitor.visitVarInsn(21, context.var((new StringBuilder()).append("_asm_flag_").append(i / 32).toString()));
        methodvisitor.visitLdcInsn(Integer.valueOf(1 << i));
        methodvisitor.visitInsn(126);
        methodvisitor.visitJumpInsn(153, label);
    }

    void _setFlag(MethodVisitor methodvisitor, Context context, int i)
    {
        String s = (new StringBuilder()).append("_asm_flag_").append(i / 32).toString();
        methodvisitor.visitVarInsn(21, context.var(s));
        methodvisitor.visitLdcInsn(Integer.valueOf(1 << i));
        methodvisitor.visitInsn(128);
        methodvisitor.visitVarInsn(54, context.var(s));
    }

    public FieldDeserializer createFieldDeserializer(ParserConfig parserconfig, Class class1, FieldInfo fieldinfo)
        throws Exception
    {
        Class class2 = fieldinfo.getFieldClass();
        if (class2 == Integer.TYPE || class2 == Long.TYPE || class2 == java/lang/String)
        {
            return createStringFieldDeserializer(parserconfig, class1, fieldinfo);
        } else
        {
            return parserconfig.createFieldDeserializerWithoutASM(parserconfig, class1, fieldinfo);
        }
    }

    public ObjectDeserializer createJavaBeanDeserializer(ParserConfig parserconfig, Class class1, java.lang.reflect.Type type)
        throws Exception
    {
        String s;
        byte abyte0[];
        if (class1.isPrimitive())
        {
            throw new IllegalArgumentException((new StringBuilder()).append("not support type :").append(class1.getName()).toString());
        }
        s = getGenClassName(class1);
        ClassWriter classwriter = new ClassWriter();
        classwriter.visit(49, 33, s, "com/alibaba/fastjson/parser/deserializer/ASMJavaBeanDeserializer", null);
        type = DeserializeBeanInfo.computeSetters(class1, type);
        _init(classwriter, new Context(s, parserconfig, type, 3));
        _createInstance(classwriter, new Context(s, parserconfig, type, 3));
        _deserialze(classwriter, new Context(s, parserconfig, type, 4));
        _deserialzeArrayMapping(classwriter, new Context(s, parserconfig, type, 4));
        abyte0 = classwriter.toByteArray();
        if (JSON.DUMP_CLASS == null)
        {
            break MISSING_BLOCK_LABEL_221;
        }
        FileOutputStream fileoutputstream = new FileOutputStream((new StringBuilder()).append(JSON.DUMP_CLASS).append(File.separator).append(s).append(".class").toString());
        type = fileoutputstream;
        fileoutputstream.write(abyte0);
        if (fileoutputstream != null)
        {
            fileoutputstream.close();
        }
_L1:
        return (ObjectDeserializer)classLoader.defineClassPublic(s, abyte0, 0, abyte0.length).getConstructor(new Class[] {
            com/alibaba/fastjson/parser/ParserConfig, java/lang/Class
        }).newInstance(new Object[] {
            parserconfig, class1
        });
        Exception exception;
        exception;
        fileoutputstream = null;
_L4:
        type = fileoutputstream;
        System.err.println((new StringBuilder()).append("FASTJSON dump class:").append(s).append("\u5931\u8D25:").append(exception.getMessage()).toString());
        if (fileoutputstream != null)
        {
            fileoutputstream.close();
        }
          goto _L1
        parserconfig;
        type = null;
_L3:
        if (type != null)
        {
            type.close();
        }
        throw parserconfig;
        parserconfig;
        if (true) goto _L3; else goto _L2
_L2:
        exception;
          goto _L4
    }

    public FieldDeserializer createStringFieldDeserializer(ParserConfig parserconfig, Class class1, FieldInfo fieldinfo)
        throws Exception
    {
        Class class2 = fieldinfo.getFieldClass();
        Method method = fieldinfo.getMethod();
        String s = getGenFieldDeserializer(class1, fieldinfo);
        ClassWriter classwriter = new ClassWriter();
        Object obj;
        MethodVisitor methodvisitor2;
        char c;
        if (class2 == Integer.TYPE)
        {
            obj = com/alibaba/fastjson/parser/deserializer/IntegerFieldDeserializer;
        } else
        if (class2 == Long.TYPE)
        {
            obj = com/alibaba/fastjson/parser/deserializer/LongFieldDeserializer;
        } else
        {
            obj = com/alibaba/fastjson/parser/deserializer/StringFieldDeserializer;
        }
        if (class1.isInterface())
        {
            c = '\271';
        } else
        {
            c = '\266';
        }
        classwriter.visit(49, 33, s, ASMUtils.getType(((Class) (obj))), null);
        methodvisitor2 = classwriter.visitMethod(1, "<init>", "(Lcom/alibaba/fastjson/parser/ParserConfig;Ljava/lang/Class;Lcom/alibaba/fastjson/util/FieldInfo;)V", null, null);
        methodvisitor2.visitVarInsn(25, 0);
        methodvisitor2.visitVarInsn(25, 1);
        methodvisitor2.visitVarInsn(25, 2);
        methodvisitor2.visitVarInsn(25, 3);
        methodvisitor2.visitMethodInsn(183, ASMUtils.getType(((Class) (obj))), "<init>", "(Lcom/alibaba/fastjson/parser/ParserConfig;Ljava/lang/Class;Lcom/alibaba/fastjson/util/FieldInfo;)V");
        methodvisitor2.visitInsn(177);
        methodvisitor2.visitMaxs(4, 6);
        methodvisitor2.visitEnd();
        if (method != null)
        {
            if (class2 == Integer.TYPE)
            {
                obj = classwriter.visitMethod(1, "setValue", "(Ljava/lang/Object;I)V", null, null);
                ((MethodVisitor) (obj)).visitVarInsn(25, 1);
                ((MethodVisitor) (obj)).visitTypeInsn(192, ASMUtils.getType(method.getDeclaringClass()));
                ((MethodVisitor) (obj)).visitVarInsn(21, 2);
                ((MethodVisitor) (obj)).visitMethodInsn(c, ASMUtils.getType(method.getDeclaringClass()), method.getName(), ASMUtils.getDesc(method));
                ((MethodVisitor) (obj)).visitInsn(177);
                ((MethodVisitor) (obj)).visitMaxs(3, 3);
                ((MethodVisitor) (obj)).visitEnd();
            } else
            if (class2 == Long.TYPE)
            {
                MethodVisitor methodvisitor = classwriter.visitMethod(1, "setValue", "(Ljava/lang/Object;J)V", null, null);
                methodvisitor.visitVarInsn(25, 1);
                methodvisitor.visitTypeInsn(192, ASMUtils.getType(method.getDeclaringClass()));
                methodvisitor.visitVarInsn(22, 2);
                methodvisitor.visitMethodInsn(c, ASMUtils.getType(method.getDeclaringClass()), method.getName(), ASMUtils.getDesc(method));
                methodvisitor.visitInsn(177);
                methodvisitor.visitMaxs(3, 4);
                methodvisitor.visitEnd();
            } else
            {
                MethodVisitor methodvisitor1 = classwriter.visitMethod(1, "setValue", "(Ljava/lang/Object;Ljava/lang/Object;)V", null, null);
                methodvisitor1.visitVarInsn(25, 1);
                methodvisitor1.visitTypeInsn(192, ASMUtils.getType(method.getDeclaringClass()));
                methodvisitor1.visitVarInsn(25, 2);
                methodvisitor1.visitTypeInsn(192, ASMUtils.getType(class2));
                methodvisitor1.visitMethodInsn(c, ASMUtils.getType(method.getDeclaringClass()), method.getName(), ASMUtils.getDesc(method));
                methodvisitor1.visitInsn(177);
                methodvisitor1.visitMaxs(3, 3);
                methodvisitor1.visitEnd();
            }
        }
        obj = classwriter.toByteArray();
        return (FieldDeserializer)classLoader.defineClassPublic(s, ((byte []) (obj)), 0, obj.length).getConstructor(new Class[] {
            com/alibaba/fastjson/parser/ParserConfig, java/lang/Class, com/alibaba/fastjson/util/FieldInfo
        }).newInstance(new Object[] {
            parserconfig, class1, fieldinfo
        });
    }

    public String getGenClassName(Class class1)
    {
        return (new StringBuilder()).append("Fastjson_ASM_").append(class1.getSimpleName()).append("_").append(seed.incrementAndGet()).toString();
    }

    public String getGenFieldDeserializer(Class class1, FieldInfo fieldinfo)
    {
        class1 = (new StringBuilder()).append("Fastjson_ASM__Field_").append(class1.getSimpleName()).toString();
        return (new StringBuilder()).append(class1).append("_").append(fieldinfo.getName()).append("_").append(seed.incrementAndGet()).toString();
    }

    public boolean isExternalClass(Class class1)
    {
        return classLoader.isExternalClass(class1);
    }

}
