// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.util.TypeUtils;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer

public class ClassDerializer
    implements ObjectDeserializer
{

    public static final ClassDerializer instance = new ClassDerializer();

    public ClassDerializer()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        defaultjsonparser = defaultjsonparser.getLexer();
        if (defaultjsonparser.token() == 8)
        {
            defaultjsonparser.nextToken();
            return null;
        }
        if (defaultjsonparser.token() != 4)
        {
            throw new JSONException("expect className");
        } else
        {
            type = defaultjsonparser.stringVal();
            defaultjsonparser.nextToken(16);
            return TypeUtils.loadClass(type);
        }
    }

    public int getFastMatchToken()
    {
        return 4;
    }

}
