// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.JSONToken;
import com.alibaba.fastjson.parser.ParseContext;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.util.TypeUtils;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer

public class MapDeserializer
    implements ObjectDeserializer
{

    public static final MapDeserializer instance = new MapDeserializer();

    public MapDeserializer()
    {
    }

    public static Object parseMap(DefaultJSONParser defaultjsonparser, Map map, Type type, Type type1, Object obj)
    {
        Object obj1;
        JSONLexer jsonlexer;
        ObjectDeserializer objectdeserializer;
        ObjectDeserializer objectdeserializer1;
        obj1 = null;
        jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() != 12 && jsonlexer.token() != 16)
        {
            throw new JSONException((new StringBuilder()).append("syntax error, expect {, actual ").append(jsonlexer.tokenName()).toString());
        }
        objectdeserializer = defaultjsonparser.getConfig().getDeserializer(type);
        objectdeserializer1 = defaultjsonparser.getConfig().getDeserializer(type1);
        jsonlexer.nextToken(objectdeserializer.getFastMatchToken());
        obj = defaultjsonparser.getContext();
_L4:
        if (jsonlexer.token() != 13)
        {
            break MISSING_BLOCK_LABEL_135;
        }
        jsonlexer.nextToken(16);
        defaultjsonparser.setContext(((ParseContext) (obj)));
        return map;
        if (jsonlexer.token() != 4 || !jsonlexer.isRef() || defaultjsonparser.isEnabled(Feature.DisableSpecialKeyDetect))
        {
            break MISSING_BLOCK_LABEL_364;
        }
        jsonlexer.nextTokenWithColon(4);
        if (jsonlexer.token() != 4)
        {
            break MISSING_BLOCK_LABEL_311;
        }
        map = jsonlexer.stringVal();
        if (!"..".equals(map)) goto _L2; else goto _L1
_L1:
        map = ((Map) (((ParseContext) (obj)).getParentContext().getObject()));
_L3:
        jsonlexer.nextToken(13);
        if (jsonlexer.token() != 13)
        {
            throw new JSONException("illegal ref");
        }
        break MISSING_BLOCK_LABEL_347;
        map;
        defaultjsonparser.setContext(((ParseContext) (obj)));
        throw map;
_L2:
        if (!"$".equals(map))
        {
            break MISSING_BLOCK_LABEL_286;
        }
        map = ((Map) (obj));
        while (map.getParentContext() != null) 
        {
            map = map.getParentContext();
        }
        map = ((Map) (map.getObject()));
          goto _L3
        defaultjsonparser.addResolveTask(new com.alibaba.fastjson.parser.DefaultJSONParser.ResolveTask(((ParseContext) (obj)), map));
        defaultjsonparser.setResolveStatus(1);
        map = obj1;
          goto _L3
        throw new JSONException((new StringBuilder()).append("illegal ref, ").append(JSONToken.name(jsonlexer.token())).toString());
        jsonlexer.nextToken(16);
        defaultjsonparser.setContext(((ParseContext) (obj)));
        return map;
        if (map.size() != 0 || jsonlexer.token() != 4 || !JSON.DEFAULT_TYPE_KEY.equals(jsonlexer.stringVal()) || defaultjsonparser.isEnabled(Feature.DisableSpecialKeyDetect))
        {
            break MISSING_BLOCK_LABEL_468;
        }
        jsonlexer.nextTokenWithColon(4);
        jsonlexer.nextToken(16);
        if (jsonlexer.token() != 13)
        {
            break MISSING_BLOCK_LABEL_454;
        }
        jsonlexer.nextToken();
        defaultjsonparser.setContext(((ParseContext) (obj)));
        return map;
        jsonlexer.nextToken(objectdeserializer.getFastMatchToken());
        Object obj2 = objectdeserializer.deserialze(defaultjsonparser, type, null);
        if (jsonlexer.token() != 17)
        {
            throw new JSONException((new StringBuilder()).append("syntax error, expect :, actual ").append(jsonlexer.token()).toString());
        }
        jsonlexer.nextToken(objectdeserializer1.getFastMatchToken());
        map.put(obj2, objectdeserializer1.deserialze(defaultjsonparser, type1, obj2));
        if (jsonlexer.token() == 16)
        {
            jsonlexer.nextToken(objectdeserializer.getFastMatchToken());
        }
          goto _L4
    }

    public static Map parseMap(DefaultJSONParser defaultjsonparser, Map map, Type type, Object obj)
    {
        ParseContext parsecontext;
        JSONLexer jsonlexer;
        jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() != 12)
        {
            throw new JSONException((new StringBuilder()).append("syntax error, expect {, actual ").append(jsonlexer.token()).toString());
        }
        parsecontext = defaultjsonparser.getContext();
_L4:
        int i;
        jsonlexer.skipWhitespace();
        i = jsonlexer.getCurrent();
        char c1 = i;
        if (!defaultjsonparser.isEnabled(Feature.AllowArbitraryCommas)) goto _L2; else goto _L1
_L1:
        c1 = i;
        if (i != ',')
        {
            break; /* Loop/switch isn't completed */
        }
        jsonlexer.next();
        jsonlexer.skipWhitespace();
        i = jsonlexer.getCurrent();
        if (true) goto _L1; else goto _L2
_L2:
        if (c1 != '"')
        {
            break MISSING_BLOCK_LABEL_208;
        }
        Object obj1;
        obj1 = jsonlexer.scanSymbol(defaultjsonparser.getSymbolTable(), '"');
        jsonlexer.skipWhitespace();
        if (jsonlexer.getCurrent() != ':')
        {
            throw new JSONException((new StringBuilder()).append("expect ':' at ").append(jsonlexer.pos()).toString());
        }
        break MISSING_BLOCK_LABEL_440;
        map;
        defaultjsonparser.setContext(parsecontext);
        throw map;
        if (c1 != '}')
        {
            break MISSING_BLOCK_LABEL_247;
        }
        jsonlexer.next();
        jsonlexer.resetStringPosition();
        jsonlexer.nextToken(16);
        defaultjsonparser.setContext(parsecontext);
        return map;
        if (c1 != '\'')
        {
            break MISSING_BLOCK_LABEL_341;
        }
        if (!defaultjsonparser.isEnabled(Feature.AllowSingleQuotes))
        {
            throw new JSONException("syntax error");
        }
        obj1 = jsonlexer.scanSymbol(defaultjsonparser.getSymbolTable(), '\'');
        jsonlexer.skipWhitespace();
        if (jsonlexer.getCurrent() != ':')
        {
            throw new JSONException((new StringBuilder()).append("expect ':' at ").append(jsonlexer.pos()).toString());
        }
        break MISSING_BLOCK_LABEL_440;
        char c;
        if (!defaultjsonparser.isEnabled(Feature.AllowUnQuotedFieldNames))
        {
            throw new JSONException("syntax error");
        }
        obj1 = jsonlexer.scanSymbolUnQuoted(defaultjsonparser.getSymbolTable());
        jsonlexer.skipWhitespace();
        c = jsonlexer.getCurrent();
        if (c == ':')
        {
            break MISSING_BLOCK_LABEL_440;
        }
        throw new JSONException((new StringBuilder()).append("expect ':' at ").append(jsonlexer.pos()).append(", actual ").append(c).toString());
        jsonlexer.next();
        jsonlexer.skipWhitespace();
        jsonlexer.getCurrent();
        jsonlexer.resetStringPosition();
        if (obj1 != JSON.DEFAULT_TYPE_KEY || defaultjsonparser.isEnabled(Feature.DisableSpecialKeyDetect))
        {
            break MISSING_BLOCK_LABEL_616;
        }
        obj1 = TypeUtils.loadClass(jsonlexer.scanSymbol(defaultjsonparser.getSymbolTable(), '"'));
        if (!java/util/Map.isAssignableFrom(((Class) (obj1))))
        {
            break MISSING_BLOCK_LABEL_554;
        }
        jsonlexer.nextToken(16);
        if (jsonlexer.token() != 13) goto _L4; else goto _L3
_L3:
        jsonlexer.nextToken(16);
        defaultjsonparser.setContext(parsecontext);
        return map;
        map = defaultjsonparser.getConfig().getDeserializer(((Type) (obj1)));
        jsonlexer.nextToken(16);
        defaultjsonparser.setResolveStatus(2);
        if (parsecontext == null)
        {
            break MISSING_BLOCK_LABEL_594;
        }
        if (!(obj instanceof Integer))
        {
            defaultjsonparser.popContext();
        }
        map = (Map)map.deserialze(defaultjsonparser, ((Type) (obj1)), obj);
        defaultjsonparser.setContext(parsecontext);
        return map;
        jsonlexer.nextToken();
        if (jsonlexer.token() != 8)
        {
            break MISSING_BLOCK_LABEL_705;
        }
        Object obj2 = null;
        jsonlexer.nextToken();
_L5:
        map.put(obj1, obj2);
        defaultjsonparser.checkMapResolve(map, ((String) (obj1)));
        defaultjsonparser.setContext(parsecontext, obj2, obj1);
        i = jsonlexer.token();
        if (i == 20 || i == 15)
        {
            defaultjsonparser.setContext(parsecontext);
            return map;
        }
        continue; /* Loop/switch isn't completed */
        obj2 = defaultjsonparser.parseObject(type);
          goto _L5
        if (i != 13) goto _L4; else goto _L6
_L6:
        jsonlexer.nextToken();
        defaultjsonparser.setContext(parsecontext);
        return map;
    }

    protected Map createMap(Type type)
    {
        if (type == java/util/Properties)
        {
            return new Properties();
        }
        if (type == java/util/Hashtable)
        {
            return new Hashtable();
        }
        if (type == java/util/IdentityHashMap)
        {
            return new IdentityHashMap();
        }
        if (type == java/util/SortedMap || type == java/util/TreeMap)
        {
            return new TreeMap();
        }
        if (type == java/util/concurrent/ConcurrentMap || type == java/util/concurrent/ConcurrentHashMap)
        {
            return new ConcurrentHashMap();
        }
        if (type == java/util/Map || type == java/util/HashMap)
        {
            return new HashMap();
        }
        if (type == java/util/LinkedHashMap)
        {
            return new LinkedHashMap();
        }
        if (type instanceof ParameterizedType)
        {
            return createMap(((ParameterizedType)type).getRawType());
        }
        Object obj = (Class)type;
        if (((Class) (obj)).isInterface())
        {
            throw new JSONException((new StringBuilder()).append("unsupport type ").append(type).toString());
        }
        try
        {
            obj = (Map)((Class) (obj)).newInstance();
        }
        catch (Exception exception)
        {
            throw new JSONException((new StringBuilder()).append("unsupport type ").append(type).toString(), exception);
        }
        return ((Map) (obj));
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        Object obj1;
        Map map;
        obj1 = defaultjsonparser.getLexer();
        if (((JSONLexer) (obj1)).token() == 8)
        {
            ((JSONLexer) (obj1)).nextToken(16);
            return null;
        }
        map = createMap(type);
        obj1 = defaultjsonparser.getContext();
        defaultjsonparser.setContext(((ParseContext) (obj1)), map, obj);
        type = ((Type) (deserialze(defaultjsonparser, type, obj, map)));
        defaultjsonparser.setContext(((ParseContext) (obj1)));
        return type;
        type;
        defaultjsonparser.setContext(((ParseContext) (obj1)));
        throw type;
    }

    protected Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj, Map map)
    {
        if (type instanceof ParameterizedType)
        {
            Object obj1 = (ParameterizedType)type;
            type = ((ParameterizedType) (obj1)).getActualTypeArguments()[0];
            obj1 = ((ParameterizedType) (obj1)).getActualTypeArguments()[1];
            if (java/lang/String == type)
            {
                return parseMap(defaultjsonparser, map, ((Type) (obj1)), obj);
            } else
            {
                return parseMap(defaultjsonparser, map, type, ((Type) (obj1)), obj);
            }
        } else
        {
            return defaultjsonparser.parseObject(map, obj);
        }
    }

    public int getFastMatchToken()
    {
        return 12;
    }

}
