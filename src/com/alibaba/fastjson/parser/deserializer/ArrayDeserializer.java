// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.ParseContext;
import com.alibaba.fastjson.util.TypeUtils;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer

public class ArrayDeserializer
    implements ObjectDeserializer
{

    public static final ArrayDeserializer instance = new ArrayDeserializer();

    public ArrayDeserializer()
    {
    }

    private Object toObjectArray(DefaultJSONParser defaultjsonparser, Class class1, JSONArray jsonarray)
    {
        Object obj2;
        int i;
        int k;
        if (jsonarray == null)
        {
            return null;
        }
        k = jsonarray.size();
        obj2 = Array.newInstance(class1, k);
        i = 0;
_L2:
        Object obj1;
        if (i >= k)
        {
            break; /* Loop/switch isn't completed */
        }
        obj1 = jsonarray.get(i);
        if (obj1 == jsonarray)
        {
            Array.set(obj2, i, obj2);
        } else
        {
label0:
            {
                if (!class1.isArray())
                {
                    break label0;
                }
                Object obj;
                if (class1.isInstance(obj1))
                {
                    obj = obj1;
                } else
                {
                    obj = toObjectArray(defaultjsonparser, class1, (JSONArray)obj1);
                }
                Array.set(obj2, i, obj);
            }
        }
_L3:
        i++;
        if (true) goto _L2; else goto _L1
        Object aobj[];
        if (!(obj1 instanceof JSONArray))
        {
            break MISSING_BLOCK_LABEL_236;
        }
        JSONArray jsonarray1 = (JSONArray)obj1;
        int l = jsonarray1.size();
        int j = 0;
        boolean flag = false;
        for (; j < l; j++)
        {
            if (jsonarray1.get(j) == jsonarray)
            {
                jsonarray1.set(i, obj2);
                flag = true;
            }
        }

        if (!flag)
        {
            break MISSING_BLOCK_LABEL_236;
        }
        aobj = jsonarray1.toArray();
_L4:
        if (aobj == null)
        {
            aobj = ((Object []) (TypeUtils.cast(obj1, class1, defaultjsonparser.getConfig())));
        }
        Array.set(obj2, i, ((Object) (aobj)));
          goto _L3
_L1:
        jsonarray.setRelatedArray(obj2);
        jsonarray.setComponentType(class1);
        return obj2;
        aobj = null;
          goto _L4
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        JSONLexer jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() == 8)
        {
            jsonlexer.nextToken(16);
            return null;
        }
        if (jsonlexer.token() == 4)
        {
            defaultjsonparser = jsonlexer.bytesValue();
            jsonlexer.nextToken(16);
            return defaultjsonparser;
        }
        if (type instanceof GenericArrayType)
        {
            type = ((GenericArrayType)type).getGenericComponentType();
            if (type instanceof TypeVariable)
            {
                Object obj1 = (TypeVariable)type;
                type = defaultjsonparser.getContext().getType();
                if (type instanceof ParameterizedType)
                {
                    ParameterizedType parameterizedtype = (ParameterizedType)type;
                    type = parameterizedtype.getRawType();
                    if (type instanceof Class)
                    {
                        TypeVariable atypevariable[] = ((Class)type).getTypeParameters();
                        type = null;
                        for (int i = 0; i < atypevariable.length; i++)
                        {
                            if (atypevariable[i].getName().equals(((TypeVariable) (obj1)).getName()))
                            {
                                type = parameterizedtype.getActualTypeArguments()[i];
                            }
                        }

                    } else
                    {
                        type = null;
                    }
                    if (type instanceof Class)
                    {
                        type = (Class)type;
                    } else
                    {
                        type = java/lang/Object;
                    }
                } else
                {
                    type = java/lang/Object;
                }
            } else
            {
                type = (Class)type;
            }
        } else
        {
            type = ((Class)type).getComponentType();
        }
        obj1 = new JSONArray();
        defaultjsonparser.parseArray(type, ((java.util.Collection) (obj1)), obj);
        return toObjectArray(defaultjsonparser, type, ((JSONArray) (obj1)));
    }

    public int getFastMatchToken()
    {
        return 14;
    }

}
