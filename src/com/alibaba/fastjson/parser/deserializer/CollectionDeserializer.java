// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

// Referenced classes of package com.alibaba.fastjson.parser.deserializer:
//            ObjectDeserializer

public class CollectionDeserializer
    implements ObjectDeserializer
{

    public static final CollectionDeserializer instance = new CollectionDeserializer();

    public CollectionDeserializer()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        if (defaultjsonparser.getLexer().token() == 8)
        {
            defaultjsonparser.getLexer().nextToken(16);
            return null;
        }
        Class class1 = getRawClass(type);
        Object obj1;
        if (class1 == java/util/AbstractCollection)
        {
            obj1 = new ArrayList();
        } else
        if (class1.isAssignableFrom(java/util/HashSet))
        {
            obj1 = new HashSet();
        } else
        if (class1.isAssignableFrom(java/util/LinkedHashSet))
        {
            obj1 = new LinkedHashSet();
        } else
        if (class1.isAssignableFrom(java/util/TreeSet))
        {
            obj1 = new TreeSet();
        } else
        if (class1.isAssignableFrom(java/util/ArrayList))
        {
            obj1 = new ArrayList();
        } else
        if (class1.isAssignableFrom(java/util/EnumSet))
        {
            if (type instanceof ParameterizedType)
            {
                obj1 = ((ParameterizedType)type).getActualTypeArguments()[0];
            } else
            {
                obj1 = java/lang/Object;
            }
            obj1 = EnumSet.noneOf((Class)obj1);
        } else
        {
            try
            {
                obj1 = (Collection)class1.newInstance();
            }
            // Misplaced declaration of an exception variable
            catch (DefaultJSONParser defaultjsonparser)
            {
                throw new JSONException((new StringBuilder()).append("create instane error, class ").append(class1.getName()).toString());
            }
        }
        if (type instanceof ParameterizedType)
        {
            type = ((ParameterizedType)type).getActualTypeArguments()[0];
        } else
        {
            type = java/lang/Object;
        }
        defaultjsonparser.parseArray(type, ((Collection) (obj1)), obj);
        return obj1;
    }

    public int getFastMatchToken()
    {
        return 14;
    }

    public Class getRawClass(Type type)
    {
        if (type instanceof Class)
        {
            return (Class)type;
        }
        if (type instanceof ParameterizedType)
        {
            return getRawClass(((ParameterizedType)type).getRawType());
        } else
        {
            throw new JSONException("TODO");
        }
    }

}
