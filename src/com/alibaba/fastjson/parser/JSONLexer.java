// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser;

import java.math.BigDecimal;
import java.util.Collection;

// Referenced classes of package com.alibaba.fastjson.parser:
//            Feature, SymbolTable

public interface JSONLexer
{

    public static final int ARRAY = 2;
    public static final int END = 4;
    public static final byte EOI = 26;
    public static final int NOT_MATCH = -1;
    public static final int NOT_MATCH_NAME = -2;
    public static final int OBJECT = 1;
    public static final int UNKOWN = 0;
    public static final int VALUE = 3;

    public abstract byte[] bytesValue();

    public abstract void close();

    public abstract void config(Feature feature, boolean flag);

    public abstract Number decimalValue(boolean flag);

    public abstract BigDecimal decimalValue();

    public abstract float floatValue();

    public abstract int getBufferPosition();

    public abstract char getCurrent();

    public abstract int intValue();

    public abstract Number integerValue();

    public abstract boolean isBlankInput();

    public abstract boolean isEnabled(Feature feature);

    public abstract boolean isRef();

    public abstract long longValue();

    public abstract char next();

    public abstract void nextToken();

    public abstract void nextToken(int i);

    public abstract void nextTokenWithColon();

    public abstract void nextTokenWithColon(int i);

    public abstract String numberString();

    public abstract int pos();

    public abstract void resetStringPosition();

    public abstract Enum scanEnum(Class class1, SymbolTable symboltable, char c);

    public abstract int scanInt(char c);

    public abstract long scanLong(char c);

    public abstract void scanNumber();

    public abstract String scanString(char c);

    public abstract void scanString();

    public abstract Collection scanStringArray(Class class1, char c);

    public abstract String scanSymbol(SymbolTable symboltable);

    public abstract String scanSymbol(SymbolTable symboltable, char c);

    public abstract String scanSymbolUnQuoted(SymbolTable symboltable);

    public abstract String scanSymbolWithSeperator(SymbolTable symboltable, char c);

    public abstract void skipWhitespace();

    public abstract String stringVal();

    public abstract int token();

    public abstract String tokenName();
}
