// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser;

import com.alibaba.fastjson.JSON;

public class SymbolTable
{
    protected static final class Entry
    {

        public final byte bytes[];
        public final char characters[];
        public final int hashCode;
        public Entry next;
        public final String symbol;

        public Entry(String s, int i, int j, int k, Entry entry)
        {
            symbol = SymbolTable.subString(s, i, j).intern();
            characters = symbol.toCharArray();
            next = entry;
            hashCode = k;
            bytes = null;
        }

        public Entry(char ac[], int i, int j, int k, Entry entry)
        {
            characters = new char[j];
            System.arraycopy(ac, i, characters, 0, j);
            symbol = (new String(characters)).intern();
            next = entry;
            hashCode = k;
            bytes = null;
        }
    }


    public static final int DEFAULT_TABLE_SIZE = 512;
    public static final int MAX_BUCKET_LENTH = 8;
    public static final int MAX_SIZE = 4096;
    private final Entry buckets[];
    private final int indexMask;
    private int size;
    private final String symbols[];
    private final char symbols_char[][];

    public SymbolTable()
    {
        this(512);
        addSymbol("$ref", 0, 4, "$ref".hashCode());
        addSymbol(JSON.DEFAULT_TYPE_KEY, 0, 5, JSON.DEFAULT_TYPE_KEY.hashCode());
    }

    public SymbolTable(int i)
    {
        size = 0;
        indexMask = i - 1;
        buckets = new Entry[i];
        symbols = new String[i];
        symbols_char = new char[i][];
    }

    public static int hash(char ac[], int i, int j)
    {
        int l = 0;
        char c = '\0';
        int k = i;
        for (i = c; l < j; i = i * 31 + c)
        {
            c = ac[k];
            l++;
            k++;
        }

        return i;
    }

    private static String subString(String s, int i, int j)
    {
        char ac[] = new char[j];
        for (int k = i; k < i + j; k++)
        {
            ac[k - i] = s.charAt(k);
        }

        return new String(ac);
    }

    public String addSymbol(String s, int i, int j, int k)
    {
        Object obj;
        int k1;
        k1 = k & indexMask;
        obj = symbols[k1];
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_322;
        }
        if (((String) (obj)).length() != j) goto _L2; else goto _L1
_L1:
        char ac[];
        int l;
        ac = symbols_char[k1];
        l = 0;
_L8:
        if (l >= j) goto _L4; else goto _L3
_L3:
        if (s.charAt(i + l) == ac[l]) goto _L6; else goto _L5
_L5:
        l = 0;
_L23:
        if (l != 0)
        {
            return ((String) (obj));
        }
          goto _L7
_L6:
        l++;
          goto _L8
_L7:
        int i1;
        obj = buckets[k1];
        i1 = 0;
_L20:
        if (obj == null) goto _L10; else goto _L9
_L9:
        int j1;
        ac = ((Entry) (obj)).characters;
        j1 = i1;
        if (j != ac.length) goto _L12; else goto _L11
_L11:
        j1 = i1;
        if (k != ((Entry) (obj)).hashCode) goto _L12; else goto _L13
_L13:
        j1 = 0;
_L21:
        if (j1 >= j) goto _L15; else goto _L14
_L14:
        if (s.charAt(i + j1) == ac[j1]) goto _L17; else goto _L16
_L16:
        j1 = 0;
_L22:
        if (j1 != 0) goto _L19; else goto _L18
_L18:
        j1 = i1 + 1;
_L12:
        obj = ((Entry) (obj)).next;
        i1 = j1;
          goto _L20
_L2:
        l = 0;
          goto _L7
_L17:
        j1++;
          goto _L21
_L19:
        return ((Entry) (obj)).symbol;
_L10:
        if (i1 >= 8)
        {
            return subString(s, i, j);
        }
        if (size >= 4096)
        {
            return subString(s, i, j);
        }
        s = new Entry(s, i, j, k, buckets[k1]);
        buckets[k1] = s;
        if (l != 0)
        {
            symbols[k1] = ((Entry) (s)).symbol;
            symbols_char[k1] = ((Entry) (s)).characters;
        }
        size = size + 1;
        return ((Entry) (s)).symbol;
_L15:
        j1 = 1;
          goto _L22
_L4:
        l = 1;
          goto _L23
        l = 1;
          goto _L7
    }

    public String addSymbol(char ac[], int i, int j)
    {
        return addSymbol(ac, i, j, hash(ac, i, j));
    }

    public String addSymbol(char ac[], int i, int j, int k)
    {
        Object obj;
        int k1;
        k1 = k & indexMask;
        obj = symbols[k1];
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_326;
        }
        if (((String) (obj)).length() != j) goto _L2; else goto _L1
_L1:
        char ac1[];
        int l;
        ac1 = symbols_char[k1];
        l = 0;
_L8:
        if (l >= j) goto _L4; else goto _L3
_L3:
        if (ac[i + l] == ac1[l]) goto _L6; else goto _L5
_L5:
        l = 0;
_L23:
        if (l != 0)
        {
            return ((String) (obj));
        }
          goto _L7
_L6:
        l++;
          goto _L8
_L7:
        int i1;
        obj = buckets[k1];
        i1 = 0;
_L20:
        if (obj == null) goto _L10; else goto _L9
_L9:
        int j1;
        ac1 = ((Entry) (obj)).characters;
        j1 = i1;
        if (j != ac1.length) goto _L12; else goto _L11
_L11:
        j1 = i1;
        if (k != ((Entry) (obj)).hashCode) goto _L12; else goto _L13
_L13:
        j1 = 0;
_L21:
        if (j1 >= j) goto _L15; else goto _L14
_L14:
        if (ac[i + j1] == ac1[j1]) goto _L17; else goto _L16
_L16:
        j1 = 0;
_L22:
        if (j1 != 0) goto _L19; else goto _L18
_L18:
        j1 = i1 + 1;
_L12:
        obj = ((Entry) (obj)).next;
        i1 = j1;
          goto _L20
_L2:
        l = 0;
          goto _L7
_L17:
        j1++;
          goto _L21
_L19:
        return ((Entry) (obj)).symbol;
_L10:
        if (i1 >= 8)
        {
            return new String(ac, i, j);
        }
        if (size >= 4096)
        {
            return new String(ac, i, j);
        }
        ac = new Entry(ac, i, j, k, buckets[k1]);
        buckets[k1] = ac;
        if (l != 0)
        {
            symbols[k1] = ((Entry) (ac)).symbol;
            symbols_char[k1] = ((Entry) (ac)).characters;
        }
        size = size + 1;
        return ((Entry) (ac)).symbol;
_L15:
        j1 = 1;
          goto _L22
_L4:
        l = 1;
          goto _L23
        l = 1;
          goto _L7
    }

    public int size()
    {
        return size;
    }

}
