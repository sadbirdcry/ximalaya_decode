// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.deserializer.ASMJavaBeanDeserializer;
import com.alibaba.fastjson.parser.deserializer.CollectionResolveFieldDeserializer;
import com.alibaba.fastjson.parser.deserializer.FieldDeserializer;
import com.alibaba.fastjson.parser.deserializer.JavaBeanDeserializer;
import com.alibaba.fastjson.parser.deserializer.ListResolveFieldDeserializer;
import com.alibaba.fastjson.parser.deserializer.MapResolveFieldDeserializer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.serializer.IntegerCodec;
import com.alibaba.fastjson.serializer.LongCodec;
import com.alibaba.fastjson.serializer.StringCodec;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.Closeable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

// Referenced classes of package com.alibaba.fastjson.parser:
//            AbstractJSONParser, ParserConfig, ParseContext, JSONLexer, 
//            JSONScanner, JSONToken, Feature, SymbolTable

public class DefaultJSONParser extends AbstractJSONParser
    implements Closeable
{
    public static class ResolveTask
    {

        private final ParseContext context;
        private FieldDeserializer fieldDeserializer;
        private ParseContext ownerContext;
        private final String referenceValue;

        public ParseContext getContext()
        {
            return context;
        }

        public FieldDeserializer getFieldDeserializer()
        {
            return fieldDeserializer;
        }

        public ParseContext getOwnerContext()
        {
            return ownerContext;
        }

        public String getReferenceValue()
        {
            return referenceValue;
        }

        public void setFieldDeserializer(FieldDeserializer fielddeserializer)
        {
            fieldDeserializer = fielddeserializer;
        }

        public void setOwnerContext(ParseContext parsecontext)
        {
            ownerContext = parsecontext;
        }

        public ResolveTask(ParseContext parsecontext, String s)
        {
            context = parsecontext;
            referenceValue = s;
        }
    }


    public static final int NONE = 0;
    public static final int NeedToResolve = 1;
    public static final int TypeNameRedirect = 2;
    private static final Set primitiveClasses;
    protected ParserConfig config;
    protected ParseContext context;
    private ParseContext contextArray[];
    private int contextArrayIndex;
    private DateFormat dateFormat;
    private String dateFormatPattern;
    private List extraProcessors;
    private List extraTypeProviders;
    protected final Object input;
    protected final JSONLexer lexer;
    private int resolveStatus;
    private List resolveTaskList;
    protected final SymbolTable symbolTable;

    public DefaultJSONParser(JSONLexer jsonlexer)
    {
        this(jsonlexer, ParserConfig.getGlobalInstance());
    }

    public DefaultJSONParser(JSONLexer jsonlexer, ParserConfig parserconfig)
    {
        this(null, jsonlexer, parserconfig);
    }

    public DefaultJSONParser(Object obj, JSONLexer jsonlexer, ParserConfig parserconfig)
    {
        dateFormatPattern = JSON.DEFFAULT_DATE_FORMAT;
        contextArray = new ParseContext[8];
        contextArrayIndex = 0;
        resolveStatus = 0;
        extraTypeProviders = null;
        extraProcessors = null;
        lexer = jsonlexer;
        input = obj;
        config = parserconfig;
        symbolTable = parserconfig.getSymbolTable();
        jsonlexer.nextToken(12);
    }

    public DefaultJSONParser(String s)
    {
        this(s, ParserConfig.getGlobalInstance(), JSON.DEFAULT_PARSER_FEATURE);
    }

    public DefaultJSONParser(String s, ParserConfig parserconfig)
    {
        this(s, ((JSONLexer) (new JSONScanner(s, JSON.DEFAULT_PARSER_FEATURE))), parserconfig);
    }

    public DefaultJSONParser(String s, ParserConfig parserconfig, int i)
    {
        this(s, ((JSONLexer) (new JSONScanner(s, i))), parserconfig);
    }

    public DefaultJSONParser(char ac[], int i, ParserConfig parserconfig, int j)
    {
        this(ac, ((JSONLexer) (new JSONScanner(ac, i, j))), parserconfig);
    }

    private void addContext(ParseContext parsecontext)
    {
        int i = contextArrayIndex;
        contextArrayIndex = i + 1;
        if (i >= contextArray.length)
        {
            ParseContext aparsecontext[] = new ParseContext[(contextArray.length * 3) / 2];
            System.arraycopy(contextArray, 0, aparsecontext, 0, contextArray.length);
            contextArray = aparsecontext;
        }
        contextArray[i] = parsecontext;
    }

    public final void accept(int i)
    {
        JSONLexer jsonlexer = getLexer();
        if (jsonlexer.token() == i)
        {
            jsonlexer.nextToken();
            return;
        } else
        {
            throw new JSONException((new StringBuilder()).append("syntax error, expect ").append(JSONToken.name(i)).append(", actual ").append(JSONToken.name(jsonlexer.token())).toString());
        }
    }

    public final void accept(int i, int j)
    {
        JSONLexer jsonlexer = getLexer();
        if (jsonlexer.token() == i)
        {
            jsonlexer.nextToken(j);
            return;
        } else
        {
            throw new JSONException((new StringBuilder()).append("syntax error, expect ").append(JSONToken.name(i)).append(", actual ").append(JSONToken.name(jsonlexer.token())).toString());
        }
    }

    public void acceptType(String s)
    {
        JSONLexer jsonlexer = lexer;
        jsonlexer.nextTokenWithColon();
        if (jsonlexer.token() != 4)
        {
            throw new JSONException("type not match error");
        }
        if (s.equals(jsonlexer.stringVal()))
        {
            jsonlexer.nextToken();
            if (jsonlexer.token() == 16)
            {
                jsonlexer.nextToken();
            }
            return;
        } else
        {
            throw new JSONException("type not match error");
        }
    }

    public void addResolveTask(ResolveTask resolvetask)
    {
        if (resolveTaskList == null)
        {
            resolveTaskList = new ArrayList(2);
        }
        resolveTaskList.add(resolvetask);
    }

    public void checkListResolve(Collection collection)
    {
label0:
        {
            if (resolveStatus == 1)
            {
                if (!(collection instanceof List))
                {
                    break label0;
                }
                int i = collection.size();
                collection = (List)collection;
                ResolveTask resolvetask = getLastResolveTask();
                resolvetask.setFieldDeserializer(new ListResolveFieldDeserializer(this, collection, i - 1));
                resolvetask.setOwnerContext(context);
                setResolveStatus(0);
            }
            return;
        }
        ResolveTask resolvetask1 = getLastResolveTask();
        resolvetask1.setFieldDeserializer(new CollectionResolveFieldDeserializer(this, collection));
        resolvetask1.setOwnerContext(context);
        setResolveStatus(0);
    }

    public void checkMapResolve(Map map, String s)
    {
        if (resolveStatus == 1)
        {
            map = new MapResolveFieldDeserializer(map, s);
            s = getLastResolveTask();
            s.setFieldDeserializer(map);
            s.setOwnerContext(context);
            setResolveStatus(0);
        }
    }

    public void close()
    {
        JSONLexer jsonlexer = getLexer();
        if (isEnabled(Feature.AutoCloseSource) && jsonlexer.token() != 20)
        {
            throw new JSONException((new StringBuilder()).append("not close json text, token : ").append(JSONToken.name(jsonlexer.token())).toString());
        }
        break MISSING_BLOCK_LABEL_71;
        Exception exception;
        exception;
        jsonlexer.close();
        throw exception;
        jsonlexer.close();
        return;
    }

    public void config(Feature feature, boolean flag)
    {
        getLexer().config(feature, flag);
    }

    public ParserConfig getConfig()
    {
        return config;
    }

    public ParseContext getContext()
    {
        return context;
    }

    public String getDateFomartPattern()
    {
        return dateFormatPattern;
    }

    public DateFormat getDateFormat()
    {
        if (dateFormat == null)
        {
            dateFormat = new SimpleDateFormat(dateFormatPattern);
        }
        return dateFormat;
    }

    public List getExtraProcessors()
    {
        if (extraProcessors == null)
        {
            extraProcessors = new ArrayList(2);
        }
        return extraProcessors;
    }

    public List getExtraProcessorsDirect()
    {
        return extraProcessors;
    }

    public List getExtraTypeProviders()
    {
        if (extraTypeProviders == null)
        {
            extraTypeProviders = new ArrayList(2);
        }
        return extraTypeProviders;
    }

    public List getExtraTypeProvidersDirect()
    {
        return extraTypeProviders;
    }

    public String getInput()
    {
        if (input instanceof char[])
        {
            return new String((char[])(char[])input);
        } else
        {
            return input.toString();
        }
    }

    public ResolveTask getLastResolveTask()
    {
        return (ResolveTask)resolveTaskList.get(resolveTaskList.size() - 1);
    }

    public JSONLexer getLexer()
    {
        return lexer;
    }

    public Object getObject(String s)
    {
        for (int i = 0; i < contextArrayIndex; i++)
        {
            if (s.equals(contextArray[i].getPath()))
            {
                return contextArray[i].getObject();
            }
        }

        return null;
    }

    public int getResolveStatus()
    {
        return resolveStatus;
    }

    public List getResolveTaskList()
    {
        if (resolveTaskList == null)
        {
            resolveTaskList = new ArrayList(2);
        }
        return resolveTaskList;
    }

    public List getResolveTaskListDirect()
    {
        return resolveTaskList;
    }

    public SymbolTable getSymbolTable()
    {
        return symbolTable;
    }

    public void handleResovleTask(Object obj)
    {
        if (resolveTaskList != null)
        {
            int j = resolveTaskList.size();
            int i = 0;
            while (i < j) 
            {
                Object obj1 = (ResolveTask)resolveTaskList.get(i);
                FieldDeserializer fielddeserializer = ((ResolveTask) (obj1)).getFieldDeserializer();
                if (fielddeserializer != null)
                {
                    obj = null;
                    if (((ResolveTask) (obj1)).getOwnerContext() != null)
                    {
                        obj = ((ResolveTask) (obj1)).getOwnerContext().getObject();
                    }
                    String s = ((ResolveTask) (obj1)).getReferenceValue();
                    if (s.startsWith("$"))
                    {
                        obj1 = getObject(s);
                    } else
                    {
                        obj1 = ((ResolveTask) (obj1)).getContext().getObject();
                    }
                    fielddeserializer.setValue(obj, obj1);
                }
                i++;
            }
        }
    }

    public boolean isEnabled(Feature feature)
    {
        return getLexer().isEnabled(feature);
    }

    public Object parse()
    {
        return parse(null);
    }

    public Object parse(Object obj)
    {
        Object obj1;
        JSONLexer jsonlexer;
        obj1 = null;
        jsonlexer = getLexer();
        jsonlexer.token();
        JVM INSTR tableswitch 2 23: default 116
    //                   2 231
    //                   3 246
    //                   4 268
    //                   5 116
    //                   6 356
    //                   7 366
    //                   8 340
    //                   9 376
    //                   10 116
    //                   11 116
    //                   12 211
    //                   13 116
    //                   14 195
    //                   15 116
    //                   16 116
    //                   17 116
    //                   18 116
    //                   19 116
    //                   20 452
    //                   21 149
    //                   22 173
    //                   23 348;
           goto _L1 _L2 _L3 _L4 _L1 _L5 _L6 _L7 _L8 _L1 _L1 _L9 _L1 _L10 _L1 _L1 _L1 _L1 _L1 _L11 _L12 _L13 _L14
_L1:
        throw new JSONException((new StringBuilder()).append("syntax error, pos ").append(jsonlexer.getBufferPosition()).toString());
_L12:
        jsonlexer.nextToken();
        obj1 = new HashSet();
        parseArray(((Collection) (obj1)), obj);
        obj = obj1;
_L16:
        return obj;
_L13:
        jsonlexer.nextToken();
        obj1 = new TreeSet();
        parseArray(((Collection) (obj1)), obj);
        return obj1;
_L10:
        obj1 = new JSONArray();
        parseArray(((Collection) (obj1)), obj);
        return obj1;
_L9:
        return parseObject(new JSONObject(isEnabled(Feature.OrderedField)), obj);
_L2:
        obj = jsonlexer.integerValue();
        jsonlexer.nextToken();
        return obj;
_L3:
        obj = jsonlexer.decimalValue(isEnabled(Feature.UseBigDecimal));
        jsonlexer.nextToken();
        return obj;
_L4:
        obj1 = jsonlexer.stringVal();
        jsonlexer.nextToken(16);
        obj = obj1;
        if (!jsonlexer.isEnabled(Feature.AllowISO8601DateFormat))
        {
            continue; /* Loop/switch isn't completed */
        }
        obj = new JSONScanner(((String) (obj1)));
        if (!((JSONScanner) (obj)).scanISO8601DateIfMatch())
        {
            break MISSING_BLOCK_LABEL_327;
        }
        obj1 = ((JSONScanner) (obj)).getCalendar().getTime();
        ((JSONScanner) (obj)).close();
        return obj1;
        ((JSONScanner) (obj)).close();
        return obj1;
        obj1;
        ((JSONScanner) (obj)).close();
        throw obj1;
_L7:
        jsonlexer.nextToken();
        return null;
_L14:
        jsonlexer.nextToken();
        return null;
_L5:
        jsonlexer.nextToken();
        return Boolean.TRUE;
_L6:
        jsonlexer.nextToken();
        return Boolean.FALSE;
_L8:
        jsonlexer.nextToken(18);
        if (jsonlexer.token() != 18)
        {
            throw new JSONException("syntax error");
        } else
        {
            jsonlexer.nextToken(10);
            accept(10);
            long l = jsonlexer.integerValue().longValue();
            accept(2);
            accept(11);
            return new Date(l);
        }
_L11:
        obj = obj1;
        if (!jsonlexer.isBlankInput())
        {
            throw new JSONException((new StringBuilder()).append("unterminated json string, pos ").append(jsonlexer.getBufferPosition()).toString());
        }
        if (true) goto _L16; else goto _L15
_L15:
    }

    public List parseArray(Class class1)
    {
        ArrayList arraylist = new ArrayList();
        parseArray(class1, ((Collection) (arraylist)));
        return arraylist;
    }

    public void parseArray(Class class1, Collection collection)
    {
        parseArray(((Type) (class1)), collection);
    }

    public void parseArray(Type type, Collection collection)
    {
        parseArray(type, collection, null);
    }

    public void parseArray(Type type, Collection collection, Object obj)
    {
        Object obj1;
        ParseContext parsecontext;
        int i;
        if (lexer.token() == 21 || lexer.token() == 22)
        {
            lexer.nextToken();
        }
        if (lexer.token() != 14)
        {
            throw new JSONException((new StringBuilder()).append("exepct '[', but ").append(JSONToken.name(lexer.token())).toString());
        }
        if (Integer.TYPE == type)
        {
            obj1 = IntegerCodec.instance;
            lexer.nextToken(2);
        } else
        if (java/lang/String == type)
        {
            obj1 = StringCodec.instance;
            lexer.nextToken(4);
        } else
        {
            obj1 = config.getDeserializer(type);
            lexer.nextToken(((ObjectDeserializer) (obj1)).getFastMatchToken());
        }
        parsecontext = getContext();
        setContext(collection, obj);
        i = 0;
_L9:
        if (isEnabled(Feature.AllowArbitraryCommas))
        {
            for (; lexer.token() == 16; lexer.nextToken()) { }
        }
        break MISSING_BLOCK_LABEL_226;
        type;
        setContext(parsecontext);
        throw type;
        int j = lexer.token();
        if (j == 15)
        {
            setContext(parsecontext);
            lexer.nextToken(16);
            return;
        }
        if (Integer.TYPE != type) goto _L2; else goto _L1
_L1:
        collection.add(IntegerCodec.instance.deserialze(this, null, null));
_L5:
        if (lexer.token() == 16)
        {
            lexer.nextToken(((ObjectDeserializer) (obj1)).getFastMatchToken());
        }
        break MISSING_BLOCK_LABEL_450;
_L2:
        if (java/lang/String != type)
        {
            break MISSING_BLOCK_LABEL_391;
        }
        if (lexer.token() != 4) goto _L4; else goto _L3
_L3:
        obj = lexer.stringVal();
        lexer.nextToken(16);
_L6:
        collection.add(obj);
          goto _L5
_L4:
        obj = parse();
label0:
        {
            if (obj != null)
            {
                break label0;
            }
            obj = null;
        }
          goto _L6
        obj = obj.toString();
          goto _L6
        if (lexer.token() != 8)
        {
            break MISSING_BLOCK_LABEL_432;
        }
        lexer.nextToken();
        obj = null;
_L7:
        collection.add(obj);
        checkListResolve(collection);
          goto _L5
        obj = ((ObjectDeserializer) (obj1)).deserialze(this, type, Integer.valueOf(i));
          goto _L7
        i++;
        if (true) goto _L9; else goto _L8
_L8:
    }

    public final void parseArray(Collection collection)
    {
        parseArray(collection, null);
    }

    public final void parseArray(Collection collection, Object obj)
    {
        ParseContext parsecontext;
        JSONLexer jsonlexer;
        int i;
        jsonlexer = getLexer();
        if (jsonlexer.token() == 21 || jsonlexer.token() == 22)
        {
            jsonlexer.nextToken();
        }
        if (jsonlexer.token() != 14)
        {
            throw new JSONException((new StringBuilder()).append("syntax error, expect [, actual ").append(JSONToken.name(jsonlexer.token())).append(", pos ").append(jsonlexer.pos()).toString());
        }
        jsonlexer.nextToken(4);
        parsecontext = getContext();
        setContext(collection, obj);
        i = 0;
_L16:
        if (isEnabled(Feature.AllowArbitraryCommas))
        {
            for (; jsonlexer.token() == 16; jsonlexer.nextToken()) { }
        }
        break MISSING_BLOCK_LABEL_167;
        collection;
        setContext(parsecontext);
        throw collection;
        jsonlexer.token();
        JVM INSTR tableswitch 2 23: default 585
    //                   2 317
    //                   3 337
    //                   4 383
    //                   5 276
    //                   6 452
    //                   7 468
    //                   8 532
    //                   9 276
    //                   10 276
    //                   11 276
    //                   12 484
    //                   13 276
    //                   14 511
    //                   15 558
    //                   16 276
    //                   17 276
    //                   18 276
    //                   19 276
    //                   20 574
    //                   21 276
    //                   22 276
    //                   23 545;
           goto _L1 _L2 _L3 _L4 _L1 _L5 _L6 _L7 _L1 _L1 _L1 _L8 _L1 _L9 _L10 _L1 _L1 _L1 _L1 _L11 _L1 _L1 _L12
_L11:
        break MISSING_BLOCK_LABEL_574;
_L1:
        obj = parse();
_L14:
        collection.add(obj);
        checkListResolve(collection);
        if (jsonlexer.token() == 16)
        {
            jsonlexer.nextToken(4);
        }
        break MISSING_BLOCK_LABEL_588;
_L2:
        obj = jsonlexer.integerValue();
        jsonlexer.nextToken(16);
        continue; /* Loop/switch isn't completed */
_L3:
        if (!jsonlexer.isEnabled(Feature.UseBigDecimal))
        {
            break MISSING_BLOCK_LABEL_371;
        }
        obj = jsonlexer.decimalValue(true);
_L13:
        jsonlexer.nextToken(16);
        continue; /* Loop/switch isn't completed */
        obj = jsonlexer.decimalValue(false);
          goto _L13
_L4:
        String s;
        s = jsonlexer.stringVal();
        jsonlexer.nextToken(16);
        obj = s;
        JSONScanner jsonscanner;
        if (!jsonlexer.isEnabled(Feature.AllowISO8601DateFormat))
        {
            continue; /* Loop/switch isn't completed */
        }
        jsonscanner = new JSONScanner(s);
        obj = s;
        if (jsonscanner.scanISO8601DateIfMatch())
        {
            obj = jsonscanner.getCalendar().getTime();
        }
        jsonscanner.close();
        continue; /* Loop/switch isn't completed */
_L5:
        obj = Boolean.TRUE;
        jsonlexer.nextToken(16);
        continue; /* Loop/switch isn't completed */
_L6:
        obj = Boolean.FALSE;
        jsonlexer.nextToken(16);
        continue; /* Loop/switch isn't completed */
_L8:
        obj = parseObject(new JSONObject(isEnabled(Feature.OrderedField)), Integer.valueOf(i));
        continue; /* Loop/switch isn't completed */
_L9:
        obj = new JSONArray();
        parseArray(((Collection) (obj)), Integer.valueOf(i));
        continue; /* Loop/switch isn't completed */
_L7:
        jsonlexer.nextToken(4);
        obj = null;
        continue; /* Loop/switch isn't completed */
_L12:
        jsonlexer.nextToken(4);
        obj = null;
        if (true) goto _L14; else goto _L10
_L10:
        jsonlexer.nextToken(16);
        setContext(parsecontext);
        return;
        throw new JSONException("unclosed jsonArray");
        i++;
        if (true) goto _L16; else goto _L15
_L15:
    }

    public Object[] parseArray(Type atype[])
    {
        Object aobj[];
        int i;
        if (lexer.token() == 8)
        {
            lexer.nextToken(16);
            return null;
        }
        if (lexer.token() != 14)
        {
            throw new JSONException((new StringBuilder()).append("syntax error : ").append(lexer.tokenName()).toString());
        }
        aobj = new Object[atype.length];
        if (atype.length == 0)
        {
            lexer.nextToken(15);
            if (lexer.token() != 15)
            {
                throw new JSONException("syntax error");
            } else
            {
                lexer.nextToken(16);
                return new Object[0];
            }
        }
        lexer.nextToken(2);
        i = 0;
_L8:
        if (i >= atype.length) goto _L2; else goto _L1
_L1:
        if (lexer.token() != 8) goto _L4; else goto _L3
_L3:
        Object obj;
        lexer.nextToken(16);
        obj = null;
_L6:
        aobj[i] = obj;
        if (lexer.token() != 15)
        {
            break; /* Loop/switch isn't completed */
        }
_L2:
        if (lexer.token() != 15)
        {
            throw new JSONException("syntax error");
        } else
        {
            lexer.nextToken(16);
            return aobj;
        }
_L4:
label0:
        {
label1:
            {
                Type type = atype[i];
                if (type == Integer.TYPE || type == java/lang/Integer)
                {
                    if (lexer.token() == 2)
                    {
                        obj = Integer.valueOf(lexer.intValue());
                        lexer.nextToken(16);
                    } else
                    {
                        obj = TypeUtils.cast(parse(), type, config);
                    }
                    continue; /* Loop/switch isn't completed */
                }
                if (type == java/lang/String)
                {
                    if (lexer.token() == 4)
                    {
                        obj = lexer.stringVal();
                        lexer.nextToken(16);
                    } else
                    {
                        obj = TypeUtils.cast(parse(), type, config);
                    }
                    continue; /* Loop/switch isn't completed */
                }
                ArrayList arraylist;
                int j;
                boolean flag;
                if (i == atype.length - 1 && (type instanceof Class))
                {
                    obj = (Class)type;
                    flag = ((Class) (obj)).isArray();
                    obj = ((Class) (obj)).getComponentType();
                } else
                {
                    obj = null;
                    flag = false;
                }
                if (!flag || lexer.token() == 14)
                {
                    break label0;
                }
                arraylist = new ArrayList();
                obj = config.getDeserializer(((Type) (obj)));
                j = ((ObjectDeserializer) (obj)).getFastMatchToken();
                if (lexer.token() != 15)
                {
                    do
                    {
                        arraylist.add(((ObjectDeserializer) (obj)).deserialze(this, type, null));
                        if (lexer.token() != 16)
                        {
                            break;
                        }
                        lexer.nextToken(j);
                    } while (true);
                    if (lexer.token() != 15)
                    {
                        break label1;
                    }
                }
                obj = TypeUtils.cast(arraylist, type, config);
                continue; /* Loop/switch isn't completed */
            }
            throw new JSONException((new StringBuilder()).append("syntax error :").append(JSONToken.name(lexer.token())).toString());
        }
        obj = config.getDeserializer(type).deserialze(this, type, null);
        if (true) goto _L6; else goto _L5
_L5:
        if (lexer.token() != 16)
        {
            throw new JSONException((new StringBuilder()).append("syntax error :").append(JSONToken.name(lexer.token())).toString());
        }
        if (i == atype.length - 1)
        {
            lexer.nextToken(15);
        } else
        {
            lexer.nextToken(2);
        }
        i++;
        if (true) goto _L8; else goto _L7
_L7:
    }

    public Object parseArrayWithType(Type type)
    {
        if (lexer.token() == 8)
        {
            lexer.nextToken();
            return null;
        }
        Type atype[] = ((ParameterizedType)type).getActualTypeArguments();
        if (atype.length != 1)
        {
            throw new JSONException((new StringBuilder()).append("not support type ").append(type).toString());
        }
        Object obj = atype[0];
        if (obj instanceof Class)
        {
            type = new ArrayList();
            parseArray((Class)obj, type);
            return type;
        }
        if (obj instanceof WildcardType)
        {
            WildcardType wildcardtype = (WildcardType)obj;
            obj = wildcardtype.getUpperBounds()[0];
            if (java/lang/Object.equals(obj))
            {
                if (wildcardtype.getLowerBounds().length == 0)
                {
                    return parse();
                } else
                {
                    throw new JSONException((new StringBuilder()).append("not support type : ").append(type).toString());
                }
            } else
            {
                type = new ArrayList();
                parseArray((Class)obj, type);
                return type;
            }
        }
        if (obj instanceof TypeVariable)
        {
            Object obj1 = (TypeVariable)obj;
            Type atype1[] = ((TypeVariable) (obj1)).getBounds();
            if (atype1.length != 1)
            {
                throw new JSONException((new StringBuilder()).append("not support : ").append(obj1).toString());
            }
            obj1 = atype1[0];
            if (obj1 instanceof Class)
            {
                type = new ArrayList();
                parseArray((Class)obj1, type);
                return type;
            }
        }
        if (obj instanceof ParameterizedType)
        {
            type = (ParameterizedType)obj;
            obj = new ArrayList();
            parseArray(type, ((Collection) (obj)));
            return obj;
        } else
        {
            throw new JSONException((new StringBuilder()).append("TODO : ").append(type).toString());
        }
    }

    public Object parseKey()
    {
        if (lexer.token() == 18)
        {
            String s = lexer.stringVal();
            lexer.nextToken(16);
            return s;
        } else
        {
            return parse(null);
        }
    }

    public JSONObject parseObject()
    {
        JSONObject jsonobject = new JSONObject(isEnabled(Feature.OrderedField));
        parseObject(((Map) (jsonobject)));
        return jsonobject;
    }

    public Object parseObject(Class class1)
    {
        return parseObject(((Type) (class1)));
    }

    public Object parseObject(Type type)
    {
        if (lexer.token() == 8)
        {
            lexer.nextToken();
            return null;
        }
        Type type1 = type;
        if (lexer.token() == 4)
        {
            type = TypeUtils.unwrap(type);
            if (type == [B)
            {
                type = lexer.bytesValue();
                lexer.nextToken();
                return type;
            }
            type1 = type;
            if (type == [C)
            {
                type = lexer.stringVal();
                lexer.nextToken();
                return type.toCharArray();
            }
        }
        type = config.getDeserializer(type1);
        try
        {
            type = ((Type) (type.deserialze(this, type1, null)));
        }
        // Misplaced declaration of an exception variable
        catch (Type type)
        {
            throw type;
        }
        // Misplaced declaration of an exception variable
        catch (Type type)
        {
            throw new JSONException(type.getMessage(), type);
        }
        return type;
    }

    public Object parseObject(Map map)
    {
        return parseObject(map, null);
    }

    public final Object parseObject(Map map, Object obj)
    {
        ParseContext parsecontext;
        JSONLexer jsonlexer;
        boolean flag;
        jsonlexer = lexer;
        if (jsonlexer.token() == 8)
        {
            jsonlexer.next();
            return null;
        }
        if (jsonlexer.token() != 12 && jsonlexer.token() != 16)
        {
            throw new JSONException((new StringBuilder()).append("syntax error, expect {, actual ").append(jsonlexer.tokenName()).toString());
        }
        parsecontext = getContext();
        flag = false;
_L17:
        char c1;
        jsonlexer.skipWhitespace();
        c1 = jsonlexer.getCurrent();
        char c2 = c1;
        if (!isEnabled(Feature.AllowArbitraryCommas)) goto _L2; else goto _L1
_L1:
        c2 = c1;
        if (c1 != ',')
        {
            break; /* Loop/switch isn't completed */
        }
        jsonlexer.next();
        jsonlexer.skipWhitespace();
        c1 = jsonlexer.getCurrent();
        if (true) goto _L1; else goto _L2
_L2:
        boolean flag1 = false;
        if (c2 != '"') goto _L4; else goto _L3
_L3:
        Object obj2;
        obj2 = jsonlexer.scanSymbol(symbolTable, '"');
        jsonlexer.skipWhitespace();
        Object obj1 = obj2;
        if (jsonlexer.getCurrent() != ':')
        {
            throw new JSONException((new StringBuilder()).append("expect ':' at ").append(jsonlexer.pos()).append(", name ").append(obj2).toString());
        }
          goto _L5
        map;
        setContext(parsecontext);
        throw map;
_L4:
        if (c2 != '}')
        {
            break MISSING_BLOCK_LABEL_302;
        }
        jsonlexer.next();
        jsonlexer.resetStringPosition();
        jsonlexer.nextToken();
        setContext(parsecontext);
        return map;
        if (c2 != '\'') goto _L7; else goto _L6
_L6:
        if (!isEnabled(Feature.AllowSingleQuotes))
        {
            throw new JSONException("syntax error");
        }
        obj1 = jsonlexer.scanSymbol(symbolTable, '\'');
        jsonlexer.skipWhitespace();
        if (jsonlexer.getCurrent() != ':')
        {
            throw new JSONException((new StringBuilder()).append("expect ':' at ").append(jsonlexer.pos()).toString());
        }
          goto _L5
_L7:
        if (c2 != '\032')
        {
            break MISSING_BLOCK_LABEL_416;
        }
        throw new JSONException("syntax error");
        if (c2 != ',') goto _L9; else goto _L8
_L8:
        throw new JSONException("syntax error");
_L61:
        jsonlexer.resetStringPosition();
        jsonlexer.scanNumber();
        if (jsonlexer.token() != 2) goto _L11; else goto _L10
_L10:
        obj2 = jsonlexer.integerValue();
_L12:
        obj1 = obj2;
        if (jsonlexer.getCurrent() != ':')
        {
            throw new JSONException((new StringBuilder()).append("expect ':' at ").append(jsonlexer.pos()).append(", name ").append(obj2).toString());
        }
        break; /* Loop/switch isn't completed */
_L11:
        obj2 = jsonlexer.decimalValue(true);
        if (true) goto _L12; else goto _L5
_L63:
        jsonlexer.nextToken();
        obj1 = parse();
        flag1 = true;
_L5:
        if (flag1)
        {
            break MISSING_BLOCK_LABEL_578;
        }
        jsonlexer.next();
        jsonlexer.skipWhitespace();
        flag1 = jsonlexer.getCurrent();
        jsonlexer.resetStringPosition();
        if (obj1 != JSON.DEFAULT_TYPE_KEY || isEnabled(Feature.DisableSpecialKeyDetect)) goto _L14; else goto _L13
_L13:
        obj2 = jsonlexer.scanSymbol(symbolTable, '"');
        obj1 = TypeUtils.loadClass(((String) (obj2)));
        if (obj1 != null) goto _L16; else goto _L15
_L15:
        map.put(JSON.DEFAULT_TYPE_KEY, obj2);
          goto _L17
_L64:
        char c;
        if (!isEnabled(Feature.AllowUnQuotedFieldNames))
        {
            throw new JSONException("syntax error");
        }
        obj1 = jsonlexer.scanSymbolUnQuoted(symbolTable);
        jsonlexer.skipWhitespace();
        c = jsonlexer.getCurrent();
        if (c == ':') goto _L5; else goto _L18
_L18:
        throw new JSONException((new StringBuilder()).append("expect ':' at ").append(jsonlexer.pos()).append(", actual ").append(c).toString());
_L16:
        jsonlexer.nextToken(16);
        if (jsonlexer.token() != 13) goto _L20; else goto _L19
_L19:
        jsonlexer.nextToken(16);
        map = config.getDeserializer(((Type) (obj1)));
        if (!(map instanceof ASMJavaBeanDeserializer)) goto _L22; else goto _L21
_L21:
        map = ((Map) (((ASMJavaBeanDeserializer)map).createInstance(this, ((Type) (obj1)))));
_L29:
        obj = map;
        if (map != null) goto _L24; else goto _L23
_L23:
        if (obj1 != java/lang/Cloneable) goto _L26; else goto _L25
_L25:
        obj = new HashMap();
_L24:
        setContext(parsecontext);
        return obj;
_L22:
        if (!(map instanceof JavaBeanDeserializer)) goto _L28; else goto _L27
_L27:
        map = ((Map) (((JavaBeanDeserializer)map).createInstance(this, ((Type) (obj1)))));
          goto _L29
_L26:
        obj = ((Class) (obj1)).newInstance();
          goto _L24
        map;
        throw new JSONException("create instance error", map);
_L20:
        setResolveStatus(2);
        if (context != null && !(obj instanceof Integer))
        {
            popContext();
        }
        map = ((Map) (config.getDeserializer(((Type) (obj1))).deserialze(this, ((Type) (obj1)), obj)));
        setContext(parsecontext);
        return map;
_L14:
        if (obj1 != "$ref") goto _L31; else goto _L30
_L30:
        if (isEnabled(Feature.DisableSpecialKeyDetect)) goto _L31; else goto _L32
_L32:
        jsonlexer.nextToken(4);
        if (jsonlexer.token() != 4) goto _L34; else goto _L33
_L33:
        obj2 = jsonlexer.stringVal();
        jsonlexer.nextToken(13);
        obj1 = null;
        obj = null;
        map = null;
        if (!"@".equals(obj2)) goto _L36; else goto _L35
_L35:
        if (getContext() == null) goto _L38; else goto _L37
_L37:
        obj1 = getContext();
        obj = ((ParseContext) (obj1)).getObject();
        if (!(obj instanceof Object[]) && !(obj instanceof Collection)) goto _L40; else goto _L39
_L42:
        if (jsonlexer.token() != 13)
        {
            throw new JSONException("syntax error");
        }
          goto _L41
_L40:
        if (((ParseContext) (obj1)).getParentContext() != null)
        {
            map = ((Map) (((ParseContext) (obj1)).getParentContext().getObject()));
        }
          goto _L42
_L36:
label0:
        {
            if (!"..".equals(obj2))
            {
                break MISSING_BLOCK_LABEL_1142;
            }
            map = parsecontext.getParentContext();
            if (map.getObject() == null)
            {
                break label0;
            }
            map = ((Map) (map.getObject()));
        }
          goto _L42
        addResolveTask(new ResolveTask(map, ((String) (obj2))));
        setResolveStatus(1);
        map = ((Map) (obj1));
          goto _L42
        if (!"$".equals(obj2))
        {
            break MISSING_BLOCK_LABEL_1210;
        }
        map = parsecontext;
        while (map.getParentContext() != null) 
        {
            map = map.getParentContext();
        }
label1:
        {
            if (map.getObject() == null)
            {
                break label1;
            }
            map = ((Map) (map.getObject()));
        }
          goto _L42
        addResolveTask(new ResolveTask(map, ((String) (obj2))));
        setResolveStatus(1);
        map = ((Map) (obj));
          goto _L42
        addResolveTask(new ResolveTask(parsecontext, ((String) (obj2))));
        setResolveStatus(1);
          goto _L38
_L41:
        jsonlexer.nextToken(16);
        setContext(parsecontext);
        return map;
_L34:
        throw new JSONException((new StringBuilder()).append("illegal ref, ").append(JSONToken.name(jsonlexer.token())).toString());
_L31:
        if (flag)
        {
            break MISSING_BLOCK_LABEL_1302;
        }
        setContext(map, obj);
        flag = true;
        obj2 = obj1;
        if (map.getClass() != com/alibaba/fastjson/JSONObject) goto _L44; else goto _L43
_L43:
        if (obj1 != null) goto _L46; else goto _L45
_L45:
        obj2 = "null";
_L44:
        if (flag1 != '"') goto _L48; else goto _L47
_L47:
        jsonlexer.scanString();
        obj1 = jsonlexer.stringVal();
        Object obj3 = obj1;
        if (!jsonlexer.isEnabled(Feature.AllowISO8601DateFormat))
        {
            break MISSING_BLOCK_LABEL_1404;
        }
        obj3 = new JSONScanner(((String) (obj1)));
        if (((JSONScanner) (obj3)).scanISO8601DateIfMatch())
        {
            obj1 = ((JSONScanner) (obj3)).getCalendar().getTime();
        }
        ((JSONScanner) (obj3)).close();
        obj3 = obj1;
        map.put(obj2, obj3);
_L51:
        jsonlexer.skipWhitespace();
        flag1 = jsonlexer.getCurrent();
        if (flag1 != ',') goto _L50; else goto _L49
_L49:
        jsonlexer.next();
          goto _L17
_L46:
        obj2 = obj1.toString();
          goto _L44
_L65:
        jsonlexer.scanNumber();
        if (jsonlexer.token() != 2)
        {
            break MISSING_BLOCK_LABEL_1500;
        }
        obj1 = jsonlexer.integerValue();
_L52:
        map.put(obj2, obj1);
          goto _L51
        obj1 = jsonlexer.decimalValue(isEnabled(Feature.UseBigDecimal));
          goto _L52
_L66:
        if (flag1 != '[')
        {
            break MISSING_BLOCK_LABEL_1614;
        }
        jsonlexer.nextToken();
        obj1 = new JSONArray();
        parseArray(((Collection) (obj1)), obj2);
        map.put(obj2, obj1);
        if (jsonlexer.token() != 13)
        {
            continue; /* Loop/switch isn't completed */
        }
        jsonlexer.nextToken();
        setContext(parsecontext);
        return map;
        if (jsonlexer.token() != 16) goto _L53; else goto _L17
_L53:
        throw new JSONException("syntax error");
        if (flag1 != '{')
        {
            break MISSING_BLOCK_LABEL_1860;
        }
        jsonlexer.nextToken();
        if (obj == null) goto _L55; else goto _L54
_L54:
        if (obj.getClass() != java/lang/Integer) goto _L55; else goto _L56
_L56:
        flag1 = true;
_L57:
        obj3 = new JSONObject(isEnabled(Feature.OrderedField));
        obj1 = null;
        if (flag1 != 0)
        {
            break MISSING_BLOCK_LABEL_1680;
        }
        obj1 = setContext(parsecontext, obj3, obj2);
        Object obj4 = parseObject(((Map) (obj3)), obj2);
        if (obj1 == null || obj3 == obj4)
        {
            break MISSING_BLOCK_LABEL_1708;
        }
        ((ParseContext) (obj1)).setObject(map);
        checkMapResolve(map, obj2.toString());
        if (map.getClass() != com/alibaba/fastjson/JSONObject)
        {
            break MISSING_BLOCK_LABEL_1797;
        }
        map.put(obj2.toString(), obj4);
_L58:
        if (flag1 == 0)
        {
            break MISSING_BLOCK_LABEL_1758;
        }
        setContext(parsecontext, obj4, obj2);
        if (jsonlexer.token() != 13)
        {
            continue; /* Loop/switch isn't completed */
        }
        jsonlexer.nextToken();
        setContext(parsecontext);
        setContext(parsecontext);
        return map;
_L55:
        flag1 = false;
          goto _L57
        map.put(obj2, obj4);
          goto _L58
        if (jsonlexer.token() != 16) goto _L59; else goto _L17
_L59:
        throw new JSONException((new StringBuilder()).append("syntax error, ").append(jsonlexer.tokenName()).toString());
        jsonlexer.nextToken();
        obj3 = parse();
        obj1 = obj2;
        if (map.getClass() == com/alibaba/fastjson/JSONObject)
        {
            obj1 = obj2.toString();
        }
        map.put(obj1, obj3);
        if (jsonlexer.token() != 13)
        {
            continue; /* Loop/switch isn't completed */
        }
        jsonlexer.nextToken();
        setContext(parsecontext);
        return map;
        if (jsonlexer.token() != 16) goto _L60; else goto _L17
_L60:
        throw new JSONException((new StringBuilder()).append("syntax error, position at ").append(jsonlexer.pos()).append(", name ").append(obj1).toString());
_L50:
        if (flag1 != '}')
        {
            break MISSING_BLOCK_LABEL_2036;
        }
        jsonlexer.next();
        jsonlexer.resetStringPosition();
        jsonlexer.nextToken();
        setContext(map, obj);
        setContext(parsecontext);
        return map;
        throw new JSONException((new StringBuilder()).append("syntax error, position at ").append(jsonlexer.pos()).append(", name ").append(obj2).toString());
_L28:
        map = null;
          goto _L29
_L9:
        if ((c2 < '0' || c2 > '9') && c2 != '-') goto _L62; else goto _L61
_L62:
        if (c2 != '{' && c2 != '[') goto _L64; else goto _L63
_L39:
        map = ((Map) (obj));
          goto _L42
_L38:
        map = null;
          goto _L42
_L48:
        if ((flag1 < '0' || flag1 > '9') && flag1 != '-') goto _L66; else goto _L65
    }

    public void parseObject(Object obj)
    {
        Class class1;
        Map map;
        class1 = obj.getClass();
        map = config.getFieldDeserializers(class1);
        if (lexer.token() != 12 && lexer.token() != 16)
        {
            throw new JSONException((new StringBuilder()).append("syntax error, expect {, actual ").append(lexer.tokenName()).toString());
        }
          goto _L1
_L6:
        if (lexer.token() != 16 || !isEnabled(Feature.AllowArbitraryCommas)) goto _L2; else goto _L1
_L1:
        String s;
        s = lexer.scanSymbol(symbolTable);
        if (s == null)
        {
            if (lexer.token() == 13)
            {
                lexer.nextToken(16);
                return;
            }
            continue; /* Loop/switch isn't completed */
        }
_L2:
        FieldDeserializer fielddeserializer;
label0:
        {
            fielddeserializer = (FieldDeserializer)map.get(s);
            if (fielddeserializer != null || s == null)
            {
                break label0;
            }
            Iterator iterator = map.entrySet().iterator();
            java.util.Map.Entry entry;
            do
            {
                if (!iterator.hasNext())
                {
                    break label0;
                }
                entry = (java.util.Map.Entry)iterator.next();
            } while (!s.equalsIgnoreCase((String)entry.getKey()));
            fielddeserializer = (FieldDeserializer)entry.getValue();
        }
        if (fielddeserializer != null)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!isEnabled(Feature.IgnoreNotMatch))
        {
            throw new JSONException((new StringBuilder()).append("setter not found, class ").append(class1.getName()).append(", property ").append(s).toString());
        }
        lexer.nextTokenWithColon();
        parse();
        if (lexer.token() == 13)
        {
            lexer.nextToken();
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
        Object obj2 = fielddeserializer.getFieldClass();
        Object obj1 = fielddeserializer.getFieldType();
        if (obj2 == Integer.TYPE)
        {
            lexer.nextTokenWithColon(2);
            obj1 = IntegerCodec.instance.deserialze(this, ((Type) (obj1)), null);
        } else
        if (obj2 == java/lang/String)
        {
            lexer.nextTokenWithColon(4);
            obj1 = StringCodec.deserialze(this);
        } else
        if (obj2 == Long.TYPE)
        {
            lexer.nextTokenWithColon(2);
            obj1 = LongCodec.instance.deserialze(this, ((Type) (obj1)), null);
        } else
        {
            obj2 = config.getDeserializer(((Class) (obj2)), ((Type) (obj1)));
            lexer.nextTokenWithColon(((ObjectDeserializer) (obj2)).getFastMatchToken());
            obj1 = ((ObjectDeserializer) (obj2)).deserialze(this, ((Type) (obj1)), null);
        }
        fielddeserializer.setValue(obj, obj1);
        if (lexer.token() == 16 || lexer.token() != 13) goto _L1; else goto _L4
_L4:
        lexer.nextToken(16);
        return;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public void popContext()
    {
        if (isEnabled(Feature.DisableCircularReferenceDetect))
        {
            return;
        } else
        {
            context = context.getParentContext();
            contextArray[contextArrayIndex - 1] = null;
            contextArrayIndex = contextArrayIndex - 1;
            return;
        }
    }

    public void setConfig(ParserConfig parserconfig)
    {
        config = parserconfig;
    }

    public ParseContext setContext(ParseContext parsecontext, Object obj, Object obj1)
    {
        if (isEnabled(Feature.DisableCircularReferenceDetect))
        {
            return null;
        } else
        {
            context = new ParseContext(parsecontext, obj, obj1);
            addContext(context);
            return context;
        }
    }

    public ParseContext setContext(Object obj, Object obj1)
    {
        if (isEnabled(Feature.DisableCircularReferenceDetect))
        {
            return null;
        } else
        {
            return setContext(context, obj, obj1);
        }
    }

    public void setContext(ParseContext parsecontext)
    {
        if (isEnabled(Feature.DisableCircularReferenceDetect))
        {
            return;
        } else
        {
            context = parsecontext;
            return;
        }
    }

    public void setDateFomrat(DateFormat dateformat)
    {
        dateFormat = dateformat;
    }

    public void setDateFormat(String s)
    {
        dateFormatPattern = s;
        dateFormat = null;
    }

    public void setResolveStatus(int i)
    {
        resolveStatus = i;
    }

    static 
    {
        primitiveClasses = new HashSet();
        primitiveClasses.add(Boolean.TYPE);
        primitiveClasses.add(Byte.TYPE);
        primitiveClasses.add(Short.TYPE);
        primitiveClasses.add(Integer.TYPE);
        primitiveClasses.add(Long.TYPE);
        primitiveClasses.add(Float.TYPE);
        primitiveClasses.add(Double.TYPE);
        primitiveClasses.add(java/lang/Boolean);
        primitiveClasses.add(java/lang/Byte);
        primitiveClasses.add(java/lang/Short);
        primitiveClasses.add(java/lang/Integer);
        primitiveClasses.add(java/lang/Long);
        primitiveClasses.add(java/lang/Float);
        primitiveClasses.add(java/lang/Double);
        primitiveClasses.add(java/math/BigInteger);
        primitiveClasses.add(java/math/BigDecimal);
        primitiveClasses.add(java/lang/String);
    }
}
