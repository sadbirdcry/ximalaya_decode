// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser;


// Referenced classes of package com.alibaba.fastjson.parser:
//            DefaultJSONParser, ParserConfig

public class DefaultExtJSONParser extends DefaultJSONParser
{

    public DefaultExtJSONParser(String s)
    {
        this(s, ParserConfig.getGlobalInstance());
    }

    public DefaultExtJSONParser(String s, ParserConfig parserconfig)
    {
        super(s, parserconfig);
    }

    public DefaultExtJSONParser(String s, ParserConfig parserconfig, int i)
    {
        super(s, parserconfig, i);
    }

    public DefaultExtJSONParser(char ac[], int i, ParserConfig parserconfig, int j)
    {
        super(ac, i, parserconfig, j);
    }
}
