// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser;


public final class Feature extends Enum
{

    private static final Feature $VALUES[];
    public static final Feature AllowArbitraryCommas;
    public static final Feature AllowComment;
    public static final Feature AllowISO8601DateFormat;
    public static final Feature AllowSingleQuotes;
    public static final Feature AllowUnQuotedFieldNames;
    public static final Feature AutoCloseSource;
    public static final Feature DisableASM;
    public static final Feature DisableCircularReferenceDetect;
    public static final Feature DisableSpecialKeyDetect;
    public static final Feature IgnoreNotMatch;
    public static final Feature InitStringFieldAsEmpty;
    public static final Feature InternFieldNames;
    public static final Feature OrderedField;
    public static final Feature SortFeidFastMatch;
    public static final Feature SupportArrayToBean;
    public static final Feature UseBigDecimal;
    private final int mask = 1 << ordinal();

    private Feature(String s, int i)
    {
        super(s, i);
    }

    public static int config(int i, Feature feature, boolean flag)
    {
        if (flag)
        {
            return feature.getMask() | i;
        } else
        {
            return ~feature.getMask() & i;
        }
    }

    public static boolean isEnabled(int i, Feature feature)
    {
        return (feature.getMask() & i) != 0;
    }

    public static int of(Feature afeature[])
    {
        int i;
        int k;
        i = 0;
        k = 0;
        if (afeature != null) goto _L2; else goto _L1
_L1:
        return k;
_L2:
        int l = afeature.length;
        int j = 0;
        do
        {
            k = i;
            if (j >= l)
            {
                continue;
            }
            k = afeature[j].getMask();
            j++;
            i = k | i;
        } while (true);
        if (true) goto _L1; else goto _L3
_L3:
    }

    public static Feature valueOf(String s)
    {
        return (Feature)Enum.valueOf(com/alibaba/fastjson/parser/Feature, s);
    }

    public static Feature[] values()
    {
        return (Feature[])$VALUES.clone();
    }

    public final int getMask()
    {
        return mask;
    }

    static 
    {
        AutoCloseSource = new Feature("AutoCloseSource", 0);
        AllowComment = new Feature("AllowComment", 1);
        AllowUnQuotedFieldNames = new Feature("AllowUnQuotedFieldNames", 2);
        AllowSingleQuotes = new Feature("AllowSingleQuotes", 3);
        InternFieldNames = new Feature("InternFieldNames", 4);
        AllowISO8601DateFormat = new Feature("AllowISO8601DateFormat", 5);
        AllowArbitraryCommas = new Feature("AllowArbitraryCommas", 6);
        UseBigDecimal = new Feature("UseBigDecimal", 7);
        IgnoreNotMatch = new Feature("IgnoreNotMatch", 8);
        SortFeidFastMatch = new Feature("SortFeidFastMatch", 9);
        DisableASM = new Feature("DisableASM", 10);
        DisableCircularReferenceDetect = new Feature("DisableCircularReferenceDetect", 11);
        InitStringFieldAsEmpty = new Feature("InitStringFieldAsEmpty", 12);
        SupportArrayToBean = new Feature("SupportArrayToBean", 13);
        OrderedField = new Feature("OrderedField", 14);
        DisableSpecialKeyDetect = new Feature("DisableSpecialKeyDetect", 15);
        $VALUES = (new Feature[] {
            AutoCloseSource, AllowComment, AllowUnQuotedFieldNames, AllowSingleQuotes, InternFieldNames, AllowISO8601DateFormat, AllowArbitraryCommas, UseBigDecimal, IgnoreNotMatch, SortFeidFastMatch, 
            DisableASM, DisableCircularReferenceDetect, InitStringFieldAsEmpty, SupportArrayToBean, OrderedField, DisableSpecialKeyDetect
        });
    }
}
