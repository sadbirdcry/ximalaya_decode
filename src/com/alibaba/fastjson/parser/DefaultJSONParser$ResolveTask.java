// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser;

import com.alibaba.fastjson.parser.deserializer.FieldDeserializer;

// Referenced classes of package com.alibaba.fastjson.parser:
//            DefaultJSONParser, ParseContext

public static class referenceValue
{

    private final ParseContext context;
    private FieldDeserializer fieldDeserializer;
    private ParseContext ownerContext;
    private final String referenceValue;

    public ParseContext getContext()
    {
        return context;
    }

    public FieldDeserializer getFieldDeserializer()
    {
        return fieldDeserializer;
    }

    public ParseContext getOwnerContext()
    {
        return ownerContext;
    }

    public String getReferenceValue()
    {
        return referenceValue;
    }

    public void setFieldDeserializer(FieldDeserializer fielddeserializer)
    {
        fieldDeserializer = fielddeserializer;
    }

    public void setOwnerContext(ParseContext parsecontext)
    {
        ownerContext = parsecontext;
    }

    public (ParseContext parsecontext, String s)
    {
        context = parsecontext;
        referenceValue = s;
    }
}
