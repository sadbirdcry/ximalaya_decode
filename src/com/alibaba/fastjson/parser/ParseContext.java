// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser;

import java.lang.reflect.Type;

public class ParseContext
{

    private final Object fieldName;
    private Object object;
    private final ParseContext parent;
    private Type type;

    public ParseContext(ParseContext parsecontext, Object obj, Object obj1)
    {
        parent = parsecontext;
        object = obj;
        fieldName = obj1;
    }

    public Object getObject()
    {
        return object;
    }

    public ParseContext getParentContext()
    {
        return parent;
    }

    public String getPath()
    {
        if (parent == null)
        {
            return "$";
        }
        if (fieldName instanceof Integer)
        {
            return (new StringBuilder()).append(parent.getPath()).append("[").append(fieldName).append("]").toString();
        } else
        {
            return (new StringBuilder()).append(parent.getPath()).append(".").append(fieldName).toString();
        }
    }

    public Type getType()
    {
        return type;
    }

    public void setObject(Object obj)
    {
        object = obj;
    }

    public void setType(Type type1)
    {
        type = type1;
    }

    public String toString()
    {
        return getPath();
    }
}
