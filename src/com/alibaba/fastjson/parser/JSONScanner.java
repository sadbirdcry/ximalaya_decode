// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.util.ASMUtils;
import com.alibaba.fastjson.util.Base64;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;
import java.util.TimeZone;

// Referenced classes of package com.alibaba.fastjson.parser:
//            JSONLexerBase, SymbolTable

public final class JSONScanner extends JSONLexerBase
{

    protected static final char typeFieldName[];
    public final int ISO8601_LEN_0;
    public final int ISO8601_LEN_1;
    public final int ISO8601_LEN_2;
    private boolean isAndroid;
    private final String text;

    public JSONScanner(String s)
    {
        this(s, JSON.DEFAULT_PARSER_FEATURE);
    }

    public JSONScanner(String s, int i)
    {
        isAndroid = ASMUtils.isAndroid();
        ISO8601_LEN_0 = "0000-00-00".length();
        ISO8601_LEN_1 = "0000-00-00T00:00:00".length();
        ISO8601_LEN_2 = "0000-00-00T00:00:00.000".length();
        features = i;
        text = s;
        bp = -1;
        next();
        if (ch == '\uFEFF')
        {
            next();
        }
    }

    public JSONScanner(char ac[], int i)
    {
        this(ac, i, JSON.DEFAULT_PARSER_FEATURE);
    }

    public JSONScanner(char ac[], int i, int j)
    {
        this(new String(ac, 0, i), j);
    }

    static boolean charArrayCompare(String s, int i, char ac[])
    {
        int k = ac.length;
        if (k + i <= s.length()) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        int j = 0;
label0:
        do
        {
label1:
            {
                if (j >= k)
                {
                    break label1;
                }
                if (ac[j] != s.charAt(i + j))
                {
                    break label0;
                }
                j++;
            }
        } while (true);
        if (true) goto _L1; else goto _L3
_L3:
        return true;
    }

    static boolean checkDate(char c, char c1, char c2, char c3, char c4, char c5, int i, int j)
    {
_L2:
        return false;
        if (c != '1' && c != '2' || c1 < '0' || c1 > '9' || c2 < '0' || c2 > '9' || c3 < '0' || c3 > '9') goto _L2; else goto _L1
_L1:
        if (c4 != '0')
        {
            continue; /* Loop/switch isn't completed */
        }
        if (c5 < '1' || c5 > '9') goto _L2; else goto _L3
_L3:
        if (i != 48) goto _L5; else goto _L4
_L4:
        if (j < 49 || j > 57) goto _L2; else goto _L6
_L6:
        return true;
        if (c4 != '1') goto _L2; else goto _L7
_L7:
        if (c5 != '0' && c5 != '1' && c5 != '2')
        {
            return false;
        }
          goto _L3
_L5:
        if (i != 49 && i != 50)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (j < 48) goto _L2; else goto _L8
_L8:
        if (j <= 57) goto _L6; else goto _L9
_L9:
        return false;
        if (i != 51) goto _L2; else goto _L10
_L10:
        if (j != 48 && j != 49)
        {
            return false;
        }
          goto _L6
    }

    private boolean checkTime(char c, char c1, char c2, char c3, char c4, char c5)
    {
        if (c != '0') goto _L2; else goto _L1
_L1:
        if (c1 >= '0' && c1 <= '9') goto _L4; else goto _L3
_L3:
        return false;
_L2:
        if (c != '1')
        {
            continue; /* Loop/switch isn't completed */
        }
        if (c1 < '0' || c1 > '9') goto _L3; else goto _L4
_L4:
        if (c2 < '0' || c2 > '5')
        {
            continue; /* Loop/switch isn't completed */
        }
        if (c3 < '0' || c3 > '9') goto _L3; else goto _L5
_L5:
        if (c4 < '0' || c4 > '5')
        {
            continue; /* Loop/switch isn't completed */
        }
        if (c5 < '0' || c5 > '9') goto _L3; else goto _L6
_L6:
        return true;
        if (c != '2' || c1 < '0') goto _L3; else goto _L7
_L7:
        if (c1 <= '4') goto _L4; else goto _L8
_L8:
        return false;
        if (c2 != '6') goto _L3; else goto _L9
_L9:
        if (c3 == '0') goto _L5; else goto _L10
_L10:
        return false;
        if (c4 != '6') goto _L3; else goto _L11
_L11:
        if (c5 != '0')
        {
            return false;
        }
          goto _L6
    }

    private void setCalendar(char c, char c1, char c2, char c3, char c4, char c5, char c6, 
            char c7)
    {
        Locale locale = Locale.getDefault();
        calendar = Calendar.getInstance(TimeZone.getDefault(), locale);
        c = digits[c];
        c1 = digits[c1];
        c2 = digits[c2];
        c3 = digits[c3];
        c4 = digits[c4];
        c5 = digits[c5];
        c6 = digits[c6];
        c7 = digits[c7];
        calendar.set(1, c * 1000 + c1 * 100 + c2 * 10 + c3);
        calendar.set(2, (c4 * 10 + c5) - 1);
        calendar.set(5, c6 * 10 + c7);
    }

    public final String addSymbol(int i, int j, int k, SymbolTable symboltable)
    {
        return symboltable.addSymbol(text, i, j, k);
    }

    protected final void arrayCopy(int i, char ac[], int j, int k)
    {
        text.getChars(i, i + k, ac, j);
    }

    public byte[] bytesValue()
    {
        return Base64.decodeFast(text, np + 1, sp);
    }

    public final boolean charArrayCompare(char ac[])
    {
        return charArrayCompare(text, bp, ac);
    }

    public final char charAt(int i)
    {
        if (i >= text.length())
        {
            return '\032';
        } else
        {
            return text.charAt(i);
        }
    }

    protected final void copyTo(int i, int j, char ac[])
    {
        text.getChars(i, i + j, ac, 0);
    }

    public final int indexOf(char c, int i)
    {
        return text.indexOf(c, i);
    }

    public boolean isEOF()
    {
        return bp == text.length() || ch == '\032' && bp + 1 == text.length();
    }

    public final char next()
    {
        int i = bp + 1;
        bp = i;
        char c = charAt(i);
        ch = c;
        return c;
    }

    public final String numberString()
    {
        int i;
label0:
        {
            char c = charAt((np + sp) - 1);
            int j = sp;
            if (c != 'L' && c != 'S' && c != 'B' && c != 'F')
            {
                i = j;
                if (c != 'D')
                {
                    break label0;
                }
            }
            i = j - 1;
        }
        return subString(np, i);
    }

    public boolean scanFieldBoolean(char ac[])
    {
        matchStat = 0;
        if (!charArrayCompare(text, bp, ac))
        {
            matchStat = -2;
            return false;
        }
        int k = bp + ac.length;
        int i = k + 1;
        k = charAt(k);
        boolean flag;
        if (k == 116)
        {
            k = i + 1;
            if (charAt(i) != 'r')
            {
                matchStat = -1;
                return false;
            }
            i = k + 1;
            if (charAt(k) != 'u')
            {
                matchStat = -1;
                return false;
            }
            if (charAt(i) != 'e')
            {
                matchStat = -1;
                return false;
            }
            bp = i + 1;
            i = charAt(bp);
            flag = true;
        } else
        if (k == 102)
        {
            int l = i + 1;
            if (charAt(i) != 'a')
            {
                matchStat = -1;
                return false;
            }
            i = l + 1;
            if (charAt(l) != 'l')
            {
                matchStat = -1;
                return false;
            }
            l = i + 1;
            if (charAt(i) != 's')
            {
                matchStat = -1;
                return false;
            }
            if (charAt(l) != 'e')
            {
                matchStat = -1;
                return false;
            }
            bp = l + 1;
            i = charAt(bp);
            flag = false;
        } else
        {
            matchStat = -1;
            return false;
        }
        if (i == 44)
        {
            i = bp + 1;
            bp = i;
            ch = charAt(i);
            matchStat = 3;
            token = 16;
        } else
        if (i == 125)
        {
            int j = bp + 1;
            bp = j;
            j = charAt(j);
            if (j == 44)
            {
                token = 16;
                j = bp + 1;
                bp = j;
                ch = charAt(j);
            } else
            if (j == 93)
            {
                token = 15;
                j = bp + 1;
                bp = j;
                ch = charAt(j);
            } else
            if (j == 125)
            {
                token = 13;
                j = bp + 1;
                bp = j;
                ch = charAt(j);
            } else
            if (j == 26)
            {
                token = 20;
            } else
            {
                matchStat = -1;
                return false;
            }
            matchStat = 4;
        } else
        {
            matchStat = -1;
            return false;
        }
        return flag;
    }

    public int scanFieldInt(char ac[])
    {
        matchStat = 0;
        int i1 = bp;
        char c = ch;
        int j;
        if (!charArrayCompare(text, bp, ac))
        {
            matchStat = -2;
            j = 0;
        } else
        {
            int i = bp + ac.length;
            j = i + 1;
            i = charAt(i);
            char c1;
            if (i >= '0' && i <= '9')
            {
                i = digits[i];
                int l;
                do
                {
                    l = j + 1;
                    c1 = charAt(j);
                    if (c1 < '0' || c1 > '9')
                    {
                        break;
                    }
                    i = i * 10 + digits[c1];
                    j = l;
                } while (true);
                if (c1 == '.')
                {
                    matchStat = -1;
                    return 0;
                }
                bp = l - 1;
                if (i < 0)
                {
                    matchStat = -1;
                    return 0;
                }
            } else
            {
                matchStat = -1;
                return 0;
            }
            if (c1 == ',')
            {
                j = bp + 1;
                bp = j;
                ch = charAt(j);
                matchStat = 3;
                token = 16;
                return i;
            }
            j = i;
            if (c1 == '}')
            {
                int k = bp + 1;
                bp = k;
                k = charAt(k);
                if (k == 44)
                {
                    token = 16;
                    k = bp + 1;
                    bp = k;
                    ch = charAt(k);
                } else
                if (k == 93)
                {
                    token = 15;
                    k = bp + 1;
                    bp = k;
                    ch = charAt(k);
                } else
                if (k == 125)
                {
                    token = 13;
                    k = bp + 1;
                    bp = k;
                    ch = charAt(k);
                } else
                if (k == 26)
                {
                    token = 20;
                } else
                {
                    bp = i1;
                    ch = c;
                    matchStat = -1;
                    return 0;
                }
                matchStat = 4;
                return i;
            }
        }
        return j;
    }

    public long scanFieldLong(char ac[])
    {
        matchStat = 0;
        int l = bp;
        char c = ch;
        if (!charArrayCompare(text, bp, ac))
        {
            matchStat = -2;
            return 0L;
        }
        int k = bp + ac.length;
        int i = k + 1;
        k = charAt(k);
        long l1;
        if (k >= '0' && k <= '9')
        {
            l1 = digits[k];
            do
            {
                k = i + 1;
                i = charAt(i);
                if (i < '0' || i > '9')
                {
                    break;
                }
                l1 = l1 * 10L + (long)digits[i];
                i = k;
            } while (true);
            if (i == 46)
            {
                matchStat = -1;
                return 0L;
            }
            bp = k - 1;
            if (l1 < 0L)
            {
                bp = l;
                ch = c;
                matchStat = -1;
                return 0L;
            }
        } else
        {
            bp = l;
            ch = c;
            matchStat = -1;
            return 0L;
        }
        if (i == 44)
        {
            i = bp + 1;
            bp = i;
            ch = charAt(i);
            matchStat = 3;
            token = 16;
            return l1;
        }
        if (i == 125)
        {
            int j = bp + 1;
            bp = j;
            j = charAt(j);
            if (j == 44)
            {
                token = 16;
                j = bp + 1;
                bp = j;
                ch = charAt(j);
            } else
            if (j == 93)
            {
                token = 15;
                j = bp + 1;
                bp = j;
                ch = charAt(j);
            } else
            if (j == 125)
            {
                token = 13;
                j = bp + 1;
                bp = j;
                ch = charAt(j);
            } else
            if (j == 26)
            {
                token = 20;
            } else
            {
                bp = l;
                ch = c;
                matchStat = -1;
                return 0L;
            }
            matchStat = 4;
            return l1;
        } else
        {
            matchStat = -1;
            return 0L;
        }
    }

    public String scanFieldString(char ac[])
    {
        boolean flag1 = false;
        matchStat = 0;
        int i1 = bp;
        char c = ch;
        if (!charArrayCompare(text, bp, ac))
        {
            matchStat = -2;
            return stringDefaultValue();
        }
        int l = bp + ac.length;
        int i = l + 1;
        if (charAt(l) != '"')
        {
            matchStat = -1;
            return stringDefaultValue();
        }
        int j1 = text.indexOf('"', i);
        if (j1 == -1)
        {
            throw new JSONException("unclosed str");
        }
        ac = subString(i, j1 - i);
        i = 0;
label0:
        do
        {
label1:
            {
                boolean flag = flag1;
                if (i < ac.length())
                {
                    if (ac.charAt(i) != '\\')
                    {
                        break label1;
                    }
                    flag = true;
                }
                if (flag)
                {
                    matchStat = -1;
                    return stringDefaultValue();
                }
                break label0;
            }
            i++;
        } while (true);
        bp = j1 + 1;
        char c1 = charAt(bp);
        ch = c1;
        if (c1 == ',')
        {
            int j = bp + 1;
            bp = j;
            ch = charAt(j);
            matchStat = 3;
            return ac;
        }
        if (c1 == '}')
        {
            int k = bp + 1;
            bp = k;
            k = charAt(k);
            if (k == 44)
            {
                token = 16;
                k = bp + 1;
                bp = k;
                ch = charAt(k);
            } else
            if (k == 93)
            {
                token = 15;
                k = bp + 1;
                bp = k;
                ch = charAt(k);
            } else
            if (k == 125)
            {
                token = 13;
                k = bp + 1;
                bp = k;
                ch = charAt(k);
            } else
            if (k == 26)
            {
                token = 20;
            } else
            {
                bp = i1;
                ch = c;
                matchStat = -1;
                return stringDefaultValue();
            }
            matchStat = 4;
            return ac;
        } else
        {
            matchStat = -1;
            return stringDefaultValue();
        }
    }

    public Collection scanFieldStringArray(char ac[], Class class1)
    {
        int i;
        int i1;
        matchStat = 0;
        if (!charArrayCompare(text, bp, ac))
        {
            matchStat = -2;
            return null;
        }
        if (class1.isAssignableFrom(java/util/HashSet))
        {
            class1 = new HashSet();
        } else
        if (class1.isAssignableFrom(java/util/ArrayList))
        {
            class1 = new ArrayList();
        } else
        {
            try
            {
                class1 = (Collection)class1.newInstance();
            }
            // Misplaced declaration of an exception variable
            catch (char ac[])
            {
                throw new JSONException(ac.getMessage(), ac);
            }
        }
        i = bp + ac.length;
        i1 = i + 1;
        if (charAt(i) != '[')
        {
            matchStat = -1;
            return null;
        }
        i = i1 + 1;
        i1 = charAt(i1);
_L6:
        if (i1 != '"')
        {
            matchStat = -1;
            return null;
        }
        i1 = i;
_L4:
        int j1;
label0:
        {
            j1 = i1 + 1;
            i1 = charAt(i1);
            if (i1 != 34)
            {
                break label0;
            }
            class1.add(subString(i, j1 - i - 1));
            i1 = j1 + 1;
            i = charAt(j1);
            if (i == ',')
            {
                i = i1 + 1;
                i1 = charAt(i1);
            } else
            {
                if (i == 93)
                {
                    i = charAt(i1);
                    bp = i1 + 1;
                    if (i == ',')
                    {
                        ch = charAt(bp);
                        matchStat = 3;
                        return class1;
                    }
                } else
                {
                    matchStat = -1;
                    return null;
                }
                if (i == '}')
                {
                    char c = charAt(bp);
                    if (c == ',')
                    {
                        token = 16;
                        int j = bp + 1;
                        bp = j;
                        ch = charAt(j);
                    } else
                    if (c == ']')
                    {
                        token = 15;
                        int k = bp + 1;
                        bp = k;
                        ch = charAt(k);
                    } else
                    if (c == '}')
                    {
                        token = 13;
                        int l = bp + 1;
                        bp = l;
                        ch = charAt(l);
                    } else
                    if (c == '\032')
                    {
                        token = 20;
                        ch = c;
                    } else
                    {
                        matchStat = -1;
                        return null;
                    }
                    matchStat = 4;
                    return class1;
                } else
                {
                    matchStat = -1;
                    return null;
                }
            }
        }
        if (true) goto _L2; else goto _L1
_L2:
        continue; /* Loop/switch isn't completed */
_L1:
        if (i1 == 92)
        {
            matchStat = -1;
            return null;
        }
        i1 = j1;
        if (true) goto _L4; else goto _L3
_L3:
        if (true) goto _L6; else goto _L5
_L5:
    }

    public String scanFieldSymbol(char ac[], SymbolTable symboltable)
    {
        int i = 0;
        matchStat = 0;
        if (!charArrayCompare(text, bp, ac))
        {
            matchStat = -2;
            return null;
        }
        int k = bp + ac.length;
        int l = k + 1;
        if (charAt(k) != '"')
        {
            matchStat = -1;
            return null;
        }
        k = l;
        do
        {
            int i1;
label0:
            {
                i1 = k + 1;
                k = charAt(k);
                char c;
                if (k == '"')
                {
                    bp = i1;
                    c = charAt(bp);
                    ch = c;
                    ac = symboltable.addSymbol(text, l, i1 - l - 1, i);
                    if (c == ',')
                    {
                        i = bp + 1;
                        bp = i;
                        ch = charAt(i);
                        matchStat = 3;
                        return ac;
                    }
                } else
                {
                    i = i * 31 + k;
                    if (k == '\\')
                    {
                        matchStat = -1;
                        return null;
                    }
                    break label0;
                }
                if (c == '}')
                {
                    int j = bp + 1;
                    bp = j;
                    j = charAt(j);
                    if (j == 44)
                    {
                        token = 16;
                        j = bp + 1;
                        bp = j;
                        ch = charAt(j);
                    } else
                    if (j == 93)
                    {
                        token = 15;
                        j = bp + 1;
                        bp = j;
                        ch = charAt(j);
                    } else
                    if (j == 125)
                    {
                        token = 13;
                        j = bp + 1;
                        bp = j;
                        ch = charAt(j);
                    } else
                    if (j == 26)
                    {
                        token = 20;
                    } else
                    {
                        matchStat = -1;
                        return null;
                    }
                    matchStat = 4;
                    return ac;
                } else
                {
                    matchStat = -1;
                    return null;
                }
            }
            k = i1;
        } while (true);
    }

    public boolean scanISO8601DateIfMatch()
    {
        return scanISO8601DateIfMatch(true);
    }

    public boolean scanISO8601DateIfMatch(boolean flag)
    {
        int i;
        char c23;
        int j2;
        j2 = text.length() - bp;
        if (flag || j2 <= 13)
        {
            break MISSING_BLOCK_LABEL_316;
        }
        i = charAt(bp);
        c23 = charAt(bp + 1);
        char c25 = charAt(bp + 2);
        char c27 = charAt(bp + 3);
        char c29 = charAt(bp + 4);
        char c30 = charAt(bp + 5);
        char c31 = charAt((bp + j2) - 1);
        char c32 = charAt((bp + j2) - 2);
        if (i != '/' || c23 != 'D' || c25 != 'a' || c27 != 't' || c29 != 'e' || c30 != '(' || c31 != '/' || c32 != ')')
        {
            break MISSING_BLOCK_LABEL_316;
        }
        c23 = '\uFFFF';
        i = 6;
_L5:
        if (i >= j2) goto _L2; else goto _L1
_L1:
        char c28 = charAt(bp + i);
        if (c28 != '+') goto _L4; else goto _L3
_L3:
        byte byte0 = i;
_L6:
        i++;
        c23 = byte0;
          goto _L5
_L4:
        if (c28 < '0')
        {
            break; /* Loop/switch isn't completed */
        }
        byte0 = c23;
        if (c28 <= '9') goto _L6; else goto _L2
_L2:
        if (c23 == -1)
        {
            return false;
        } else
        {
            int j = bp + 6;
            long l3 = Long.parseLong(subString(j, c23 - j));
            Locale locale = Locale.getDefault();
            calendar = Calendar.getInstance(TimeZone.getDefault(), locale);
            calendar.setTimeInMillis(l3);
            token = 5;
            return true;
        }
        if (j2 == 8 || j2 == 14 || j2 == 17)
        {
            if (flag)
            {
                return false;
            }
            char c = charAt(bp);
            char c3 = charAt(bp + 1);
            char c6 = charAt(bp + 2);
            char c9 = charAt(bp + 3);
            char c12 = charAt(bp + 4);
            char c15 = charAt(bp + 5);
            char c18 = charAt(bp + 6);
            char c20 = charAt(bp + 7);
            if (!checkDate(c, c3, c6, c9, c12, c15, c18, c20))
            {
                return false;
            }
            setCalendar(c, c3, c6, c9, c12, c15, c18, c20);
            int k;
            int j1;
            int l1;
            if (j2 != 8)
            {
                char c1 = charAt(bp + 8);
                char c4 = charAt(bp + 9);
                char c7 = charAt(bp + 10);
                char c10 = charAt(bp + 11);
                char c13 = charAt(bp + 12);
                char c16 = charAt(bp + 13);
                if (!checkTime(c1, c4, c7, c10, c13, c16))
                {
                    return false;
                }
                int l2;
                if (j2 == 17)
                {
                    k = charAt(bp + 14);
                    char c24 = charAt(bp + 15);
                    char c26 = charAt(bp + 16);
                    if (k < '0' || k > '9')
                    {
                        return false;
                    }
                    if (c24 < '0' || c24 > '9')
                    {
                        return false;
                    }
                    if (c26 < '0' || c26 > '9')
                    {
                        return false;
                    }
                    k = digits[k] * 100 + digits[c24] * 10 + digits[c26];
                } else
                {
                    k = 0;
                }
                j2 = digits[c1];
                l2 = digits[c4];
                j1 = digits[c7];
                l1 = digits[c10] + j1 * 10;
                j1 = digits[c13] * 10 + digits[c16];
                j2 = j2 * 10 + l2;
            } else
            {
                j2 = 0;
                l1 = 0;
                j1 = 0;
                k = 0;
            }
            calendar.set(11, j2);
            calendar.set(12, l1);
            calendar.set(13, j1);
            calendar.set(14, k);
            token = 5;
            return true;
        }
        if (j2 < ISO8601_LEN_0)
        {
            return false;
        }
        if (charAt(bp + 4) != '-')
        {
            return false;
        }
        if (charAt(bp + 7) != '-')
        {
            return false;
        }
        char c2 = charAt(bp);
        char c5 = charAt(bp + 1);
        char c8 = charAt(bp + 2);
        char c11 = charAt(bp + 3);
        char c14 = charAt(bp + 5);
        char c17 = charAt(bp + 6);
        char c19 = charAt(bp + 8);
        char c21 = charAt(bp + 9);
        if (!checkDate(c2, c5, c8, c11, c14, c17, c19, c21))
        {
            return false;
        }
        setCalendar(c2, c5, c8, c11, c14, c17, c19, c21);
        int l = charAt(bp + 10);
        if (l == 'T' || l == ' ' && !flag)
        {
            if (j2 < ISO8601_LEN_1)
            {
                return false;
            }
        } else
        if (l == '"' || l == '\032')
        {
            calendar.set(11, 0);
            calendar.set(12, 0);
            calendar.set(13, 0);
            calendar.set(14, 0);
            l = bp + 10;
            bp = l;
            ch = charAt(l);
            token = 5;
            return true;
        } else
        {
            return false;
        }
        if (charAt(bp + 13) != ':')
        {
            return false;
        }
        if (charAt(bp + 16) != ':')
        {
            return false;
        }
        c2 = charAt(bp + 11);
        c5 = charAt(bp + 12);
        c8 = charAt(bp + 14);
        c11 = charAt(bp + 15);
        c14 = charAt(bp + 17);
        c17 = charAt(bp + 18);
        if (!checkTime(c2, c5, c8, c11, c14, c17))
        {
            return false;
        }
        l = digits[c2];
        int k1 = digits[c5];
        int i2 = digits[c8];
        int i3 = digits[c11];
        int j3 = digits[c14];
        int k3 = digits[c17];
        calendar.set(11, l * 10 + k1);
        calendar.set(12, i2 * 10 + i3);
        calendar.set(13, j3 * 10 + k3);
        l = charAt(bp + 19);
        if (l == '.')
        {
            if (j2 < ISO8601_LEN_2)
            {
                return false;
            }
        } else
        {
            calendar.set(14, 0);
            k1 = bp + 19;
            bp = k1;
            ch = charAt(k1);
            token = 5;
            if (l == 'Z' && calendar.getTimeZone().getRawOffset() != 0)
            {
                String as[] = TimeZone.getAvailableIDs(0);
                if (as.length > 0)
                {
                    TimeZone timezone = TimeZone.getTimeZone(as[0]);
                    calendar.setTimeZone(timezone);
                }
            }
            return true;
        }
        l = charAt(bp + 20);
        if (l < '0' || l > '9')
        {
            return false;
        }
        i2 = digits[l];
        j2 = 1;
        i3 = charAt(bp + 21);
        k1 = j2;
        l = i2;
        if (i3 >= '0')
        {
            k1 = j2;
            l = i2;
            if (i3 <= '9')
            {
                l = digits[i3] + i2 * 10;
                k1 = 2;
            }
        }
        i2 = k1;
        j2 = l;
        if (k1 == 2)
        {
            i3 = charAt(bp + 22);
            i2 = k1;
            j2 = l;
            if (i3 >= '0')
            {
                i2 = k1;
                j2 = l;
                if (i3 <= '9')
                {
                    j2 = digits[i3] + l * 10;
                    i2 = 3;
                }
            }
        }
        calendar.set(14, j2);
        k1 = 0;
        i3 = charAt(bp + 20 + i2);
        if (i3 == '+' || i3 == '-')
        {
            k1 = charAt(bp + 20 + i2 + 1);
            if (k1 < 48 || k1 > 49)
            {
                return false;
            }
            int k2 = charAt(bp + 20 + i2 + 2);
            if (k2 < '0' || k2 > '9')
            {
                return false;
            }
            char c22 = charAt(bp + 20 + i2 + 3);
            if (c22 == ':')
            {
                if (charAt(bp + 20 + i2 + 4) != '0')
                {
                    return false;
                }
                if (charAt(bp + 20 + i2 + 5) != '0')
                {
                    return false;
                }
                c22 = '\006';
            } else
            if (c22 == '0')
            {
                if (charAt(bp + 20 + i2 + 4) != '0')
                {
                    return false;
                }
                c22 = '\005';
            } else
            {
                c22 = '\003';
            }
            k1 = (digits[k1] * 10 + digits[k2]) * 3600 * 1000;
            k2 = k1;
            if (i3 == '-')
            {
                k2 = -k1;
            }
            k1 = c22;
            if (calendar.getTimeZone().getRawOffset() != k2)
            {
                String as1[] = TimeZone.getAvailableIDs(k2);
                k1 = c22;
                if (as1.length > 0)
                {
                    TimeZone timezone1 = TimeZone.getTimeZone(as1[0]);
                    calendar.setTimeZone(timezone1);
                    k1 = c22;
                }
            }
        } else
        if (i3 == 'Z')
        {
            boolean flag1 = true;
            k1 = ((flag1) ? 1 : 0);
            if (calendar.getTimeZone().getRawOffset() != 0)
            {
                String as2[] = TimeZone.getAvailableIDs(0);
                k1 = ((flag1) ? 1 : 0);
                if (as2.length > 0)
                {
                    TimeZone timezone2 = TimeZone.getTimeZone(as2[0]);
                    calendar.setTimeZone(timezone2);
                    k1 = ((flag1) ? 1 : 0);
                }
            }
        }
        c22 = charAt(bp + (i2 + 20 + k1));
        if (c22 != '\032' && c22 != '"')
        {
            return false;
        } else
        {
            int i1 = i2 + 20 + k1 + bp;
            bp = i1;
            ch = charAt(i1);
            token = 5;
            return true;
        }
    }

    public final int scanType(String s)
    {
        int i = 0;
        matchStat = 0;
        if (!charArrayCompare(text, bp, typeFieldName))
        {
            return -2;
        }
        int j = bp + typeFieldName.length;
        int k;
        for (k = s.length(); i < k; i++)
        {
            if (s.charAt(i) != charAt(j + i))
            {
                return -1;
            }
        }

        i = j + k;
        if (charAt(i) != '"')
        {
            return -1;
        }
        j = i + 1;
        ch = charAt(j);
        if (ch == ',')
        {
            i = j + 1;
            ch = charAt(i);
            bp = i;
            token = 16;
            return 3;
        }
        i = j;
        if (ch == '}')
        {
            i = j + 1;
            ch = charAt(i);
            if (ch == ',')
            {
                token = 16;
                i++;
                ch = charAt(i);
            } else
            if (ch == ']')
            {
                token = 15;
                i++;
                ch = charAt(i);
            } else
            if (ch == '}')
            {
                token = 13;
                i++;
                ch = charAt(i);
            } else
            if (ch == '\032')
            {
                token = 20;
            } else
            {
                return -1;
            }
            matchStat = 4;
        }
        bp = i;
        return matchStat;
    }

    public final String stringVal()
    {
        if (!hasSpecial)
        {
            return subString(np + 1, sp);
        } else
        {
            return new String(sbuf, 0, sp);
        }
    }

    public final String subString(int i, int j)
    {
        if (isAndroid)
        {
            char ac[] = new char[j];
            for (int k = i; k < i + j; k++)
            {
                ac[k - i] = text.charAt(k);
            }

            return new String(ac);
        } else
        {
            return text.substring(i, i + j);
        }
    }

    static 
    {
        typeFieldName = (new StringBuilder()).append("\"").append(JSON.DEFAULT_TYPE_KEY).append("\":\"").toString().toCharArray();
    }
}
