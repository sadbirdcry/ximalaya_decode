// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.util.IOUtils;
import java.io.Closeable;
import java.lang.ref.SoftReference;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.parser:
//            JSONLexer, Feature, SymbolTable, JSONToken

public abstract class JSONLexerBase
    implements JSONLexer, Closeable
{

    private static final Map DEFAULT_KEYWORDS;
    protected static final int INT_MULTMIN_RADIX_TEN = 0xf3333334;
    protected static final int INT_N_MULTMAX_RADIX_TEN = 0xf3333334;
    protected static final long MULTMIN_RADIX_TEN = 0xf333333333333334L;
    protected static final long N_MULTMAX_RADIX_TEN = 0xf333333333333334L;
    private static final ThreadLocal SBUF_REF_LOCAL = new ThreadLocal();
    protected static final int digits[];
    protected static final char typeFieldName[];
    protected int bp;
    protected Calendar calendar;
    protected char ch;
    protected int eofPos;
    protected int features;
    protected boolean hasSpecial;
    protected Map keywods;
    public int matchStat;
    protected int np;
    protected int pos;
    protected char sbuf[];
    protected int sp;
    protected int token;

    public JSONLexerBase()
    {
        features = JSON.DEFAULT_PARSER_FEATURE;
        calendar = null;
        matchStat = 0;
        keywods = DEFAULT_KEYWORDS;
        SoftReference softreference = (SoftReference)SBUF_REF_LOCAL.get();
        if (softreference != null)
        {
            sbuf = (char[])softreference.get();
            SBUF_REF_LOCAL.set(null);
        }
        if (sbuf == null)
        {
            sbuf = new char[64];
        }
    }

    public static boolean isWhitespace(char c)
    {
        return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == '\f' || c == '\b';
    }

    private void scanStringSingleQuote()
    {
        np = bp;
        hasSpecial = false;
        do
        {
            char c = next();
            if (c == '\'')
            {
                token = 4;
                next();
                return;
            }
            if (c == '\032')
            {
                throw new JSONException("unclosed single-quote string");
            }
            if (c == '\\')
            {
                if (!hasSpecial)
                {
                    hasSpecial = true;
                    if (sp > sbuf.length)
                    {
                        char ac[] = new char[sp * 2];
                        System.arraycopy(sbuf, 0, ac, 0, sbuf.length);
                        sbuf = ac;
                    }
                    copyTo(np + 1, sp, sbuf);
                }
                c = next();
                switch (c)
                {
                default:
                    ch = c;
                    throw new JSONException("unclosed single-quote string");

                case 48: // '0'
                    putChar('\0');
                    break;

                case 49: // '1'
                    putChar('\001');
                    break;

                case 50: // '2'
                    putChar('\002');
                    break;

                case 51: // '3'
                    putChar('\003');
                    break;

                case 52: // '4'
                    putChar('\004');
                    break;

                case 53: // '5'
                    putChar('\005');
                    break;

                case 54: // '6'
                    putChar('\006');
                    break;

                case 55: // '7'
                    putChar('\007');
                    break;

                case 98: // 'b'
                    putChar('\b');
                    break;

                case 116: // 't'
                    putChar('\t');
                    break;

                case 110: // 'n'
                    putChar('\n');
                    break;

                case 118: // 'v'
                    putChar('\013');
                    break;

                case 70: // 'F'
                case 102: // 'f'
                    putChar('\f');
                    break;

                case 114: // 'r'
                    putChar('\r');
                    break;

                case 34: // '"'
                    putChar('"');
                    break;

                case 39: // '\''
                    putChar('\'');
                    break;

                case 47: // '/'
                    putChar('/');
                    break;

                case 92: // '\\'
                    putChar('\\');
                    break;

                case 120: // 'x'
                    char c1 = next();
                    char c2 = next();
                    putChar((char)(digits[c1] * 16 + digits[c2]));
                    break;

                case 117: // 'u'
                    putChar((char)Integer.parseInt(new String(new char[] {
                        next(), next(), next(), next()
                    }), 16));
                    break;
                }
            } else
            if (!hasSpecial)
            {
                sp = sp + 1;
            } else
            if (sp == sbuf.length)
            {
                putChar(c);
            } else
            {
                char ac1[] = sbuf;
                int i = sp;
                sp = i + 1;
                ac1[i] = c;
            }
        } while (true);
    }

    public abstract String addSymbol(int i, int j, int k, SymbolTable symboltable);

    protected abstract void arrayCopy(int i, char ac[], int j, int k);

    public abstract byte[] bytesValue();

    protected abstract boolean charArrayCompare(char ac[]);

    public abstract char charAt(int i);

    public void close()
    {
        if (sbuf.length <= 8192)
        {
            SBUF_REF_LOCAL.set(new SoftReference(sbuf));
        }
        sbuf = null;
    }

    public void config(Feature feature, boolean flag)
    {
        features = Feature.config(features, feature, flag);
    }

    protected abstract void copyTo(int i, int j, char ac[]);

    public final Number decimalValue(boolean flag)
    {
        char c = charAt((np + sp) - 1);
        if (c == 'F')
        {
            return Float.valueOf(Float.parseFloat(numberString()));
        }
        if (c == 'D')
        {
            return Double.valueOf(Double.parseDouble(numberString()));
        }
        if (flag)
        {
            return decimalValue();
        } else
        {
            return Double.valueOf(doubleValue());
        }
    }

    public final BigDecimal decimalValue()
    {
        return new BigDecimal(numberString());
    }

    public double doubleValue()
    {
        return Double.parseDouble(numberString());
    }

    public float floatValue()
    {
        return Float.parseFloat(numberString());
    }

    public final int getBufferPosition()
    {
        return bp;
    }

    public Calendar getCalendar()
    {
        return calendar;
    }

    public final char getCurrent()
    {
        return ch;
    }

    public abstract int indexOf(char c, int i);

    public final int intValue()
    {
        int j = 0;
        if (np == -1)
        {
            np = 0;
        }
        int i = np;
        int j1 = np + sp;
        boolean flag;
        int k;
        if (charAt(np) == '-')
        {
            flag = true;
            k = 0x80000000;
            i++;
        } else
        {
            k = 0x80000001;
            flag = false;
        }
        if (!flag);
        if (i < j1)
        {
            int ai[] = digits;
            j = i + 1;
            int l = -ai[charAt(i)];
            i = j;
            j = l;
        }
        do
        {
label0:
            {
                int i1;
                char c;
                if (i < j1)
                {
                    i1 = i + 1;
                    c = charAt(i);
                    i = i1;
                    if (c != 'L')
                    {
                        i = i1;
                        if (c != 'S')
                        {
                            if (c != 'B')
                            {
                                break label0;
                            }
                            i = i1;
                        }
                    }
                }
                if (flag)
                {
                    if (i > np + 1)
                    {
                        return j;
                    } else
                    {
                        throw new NumberFormatException(numberString());
                    }
                } else
                {
                    return -j;
                }
            }
            i = digits[c];
            if (j < 0xf3333334)
            {
                throw new NumberFormatException(numberString());
            }
            j *= 10;
            if (j < k + i)
            {
                throw new NumberFormatException(numberString());
            }
            j -= i;
            i = i1;
        } while (true);
    }

    public final Number integerValue()
        throws NumberFormatException
    {
        int i;
        int j;
        int k;
        long l1;
        l1 = 0L;
        if (np == -1)
        {
            np = 0;
        }
        k = np;
        i = np;
        j = sp + i;
        i = 32;
        charAt(j - 1);
        JVM INSTR lookupswitch 3: default 80
    //                   66: 203
    //                   76: 183
    //                   83: 193;
           goto _L1 _L2 _L3 _L4
_L1:
        boolean flag;
        int i1;
        long l2;
        if (charAt(np) == '-')
        {
            l2 = 0x8000000000000000L;
            flag = true;
            k++;
        } else
        {
            l2 = 0x8000000000000001L;
            flag = false;
        }
        if (!flag);
        if (k < j)
        {
            int ai[] = digits;
            int l = k + 1;
            l1 = -ai[charAt(k)];
            k = l;
        }
_L5:
        if (k >= j)
        {
            break MISSING_BLOCK_LABEL_276;
        }
        i1 = digits[charAt(k)];
        if (l1 < 0xf333333333333334L)
        {
            return new BigInteger(numberString());
        }
        break MISSING_BLOCK_LABEL_227;
_L3:
        j--;
        i = 76;
          goto _L1
_L4:
        j--;
        i = 83;
          goto _L1
_L2:
        j--;
        i = 66;
          goto _L1
        l1 *= 10L;
        if (l1 < (long)i1 + l2)
        {
            return new BigInteger(numberString());
        }
        l1 -= i1;
        k++;
          goto _L5
        if (flag)
        {
            if (k > np + 1)
            {
                if (l1 >= 0xffffffff80000000L && i != 76)
                {
                    if (i == 83)
                    {
                        return Short.valueOf((short)(int)l1);
                    }
                    if (i == 66)
                    {
                        return Byte.valueOf((byte)(int)l1);
                    } else
                    {
                        return Integer.valueOf((int)l1);
                    }
                } else
                {
                    return Long.valueOf(l1);
                }
            } else
            {
                throw new NumberFormatException(numberString());
            }
        }
        l1 = -l1;
        if (l1 <= 0x7fffffffL && i != 76)
        {
            if (i == 83)
            {
                return Short.valueOf((short)(int)l1);
            }
            if (i == 66)
            {
                return Byte.valueOf((byte)(int)l1);
            } else
            {
                return Integer.valueOf((int)l1);
            }
        } else
        {
            return Long.valueOf(l1);
        }
    }

    public final boolean isBlankInput()
    {
        int i;
        boolean flag1;
        flag1 = false;
        i = 0;
_L6:
        char c = charAt(i);
        if (c != '\032') goto _L2; else goto _L1
_L1:
        boolean flag = true;
_L4:
        return flag;
_L2:
        flag = flag1;
        if (!isWhitespace(c)) goto _L4; else goto _L3
_L3:
        i++;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public abstract boolean isEOF();

    public final boolean isEnabled(Feature feature)
    {
        return Feature.isEnabled(features, feature);
    }

    public final boolean isRef()
    {
        while (sp != 4 || charAt(np + 1) != '$' || charAt(np + 2) != 'r' || charAt(np + 3) != 'e' || charAt(np + 4) != 'f') 
        {
            return false;
        }
        return true;
    }

    protected transient void lexError(String s, Object aobj[])
    {
        token = 1;
    }

    public final long longValue()
        throws NumberFormatException
    {
        long l1 = 0L;
        if (np == -1)
        {
            np = 0;
        }
        int i = np;
        int l = np + sp;
        boolean flag;
        long l2;
        if (charAt(np) == '-')
        {
            i++;
            flag = true;
            l2 = 0x8000000000000000L;
        } else
        {
            flag = false;
            l2 = 0x8000000000000001L;
        }
        if (!flag);
        if (i < l)
        {
            int ai[] = digits;
            int j = i + 1;
            l1 = -ai[charAt(i)];
            i = j;
        }
        do
        {
label0:
            {
                int k;
                char c;
                if (i < l)
                {
                    k = i + 1;
                    c = charAt(i);
                    i = k;
                    if (c != 'L')
                    {
                        i = k;
                        if (c != 'S')
                        {
                            if (c != 'B')
                            {
                                break label0;
                            }
                            i = k;
                        }
                    }
                }
                if (flag)
                {
                    if (i > np + 1)
                    {
                        return l1;
                    } else
                    {
                        throw new NumberFormatException(numberString());
                    }
                } else
                {
                    return -l1;
                }
            }
            i = digits[c];
            if (l1 < 0xf333333333333334L)
            {
                throw new NumberFormatException(numberString());
            }
            l1 *= 10L;
            if (l1 < (long)i + l2)
            {
                throw new NumberFormatException(numberString());
            }
            l1 -= i;
            i = k;
        } while (true);
    }

    public final boolean matchField(char ac[])
    {
        if (!charArrayCompare(ac))
        {
            return false;
        }
        bp = bp + ac.length;
        ch = charAt(bp);
        if (ch == '{')
        {
            next();
            token = 12;
        } else
        if (ch == '[')
        {
            next();
            token = 14;
        } else
        {
            nextToken();
        }
        return true;
    }

    public final int matchStat()
    {
        return matchStat;
    }

    public abstract char next();

    public final void nextIdent()
    {
        for (; isWhitespace(ch); next()) { }
        if (ch == '_' || Character.isLetter(ch))
        {
            scanIdent();
            return;
        } else
        {
            nextToken();
            return;
        }
    }

    public final void nextToken()
    {
        sp = 0;
        do
        {
            pos = bp;
            if (ch == '"')
            {
                scanString();
                return;
            }
            if (ch == ',')
            {
                next();
                token = 16;
                return;
            }
            if (ch >= '0' && ch <= '9')
            {
                scanNumber();
                return;
            }
            if (ch == '-')
            {
                scanNumber();
                return;
            }
            switch (ch)
            {
            default:
                if (isEOF())
                {
                    if (token == 20)
                    {
                        throw new JSONException("EOF error");
                    } else
                    {
                        token = 20;
                        int i = eofPos;
                        bp = i;
                        pos = i;
                        return;
                    }
                } else
                {
                    lexError("illegal.char", new Object[] {
                        String.valueOf(ch)
                    });
                    next();
                    return;
                }

            case 39: // '\''
                if (!isEnabled(Feature.AllowSingleQuotes))
                {
                    throw new JSONException("Feature.AllowSingleQuotes is false");
                } else
                {
                    scanStringSingleQuote();
                    return;
                }

            case 8: // '\b'
            case 9: // '\t'
            case 10: // '\n'
            case 12: // '\f'
            case 13: // '\r'
            case 32: // ' '
                next();
                break;

            case 116: // 't'
                scanTrue();
                return;

            case 84: // 'T'
                scanTreeSet();
                return;

            case 83: // 'S'
                scanSet();
                return;

            case 102: // 'f'
                scanFalse();
                return;

            case 110: // 'n'
                scanNullOrNew();
                return;

            case 78: // 'N'
                scanNULL();
                return;

            case 117: // 'u'
                scanUndefined();
                return;

            case 40: // '('
                next();
                token = 10;
                return;

            case 41: // ')'
                next();
                token = 11;
                return;

            case 91: // '['
                next();
                token = 14;
                return;

            case 93: // ']'
                next();
                token = 15;
                return;

            case 123: // '{'
                next();
                token = 12;
                return;

            case 125: // '}'
                next();
                token = 13;
                return;

            case 58: // ':'
                next();
                token = 17;
                return;
            }
        } while (true);
    }

    public final void nextToken(int i)
    {
        sp = 0;
_L10:
        i;
        JVM INSTR tableswitch 2 20: default 96
    //                   2 279
    //                   3 96
    //                   4 374
    //                   5 96
    //                   6 96
    //                   7 96
    //                   8 96
    //                   9 96
    //                   10 96
    //                   11 96
    //                   12 158
    //                   13 96
    //                   14 469
    //                   15 511
    //                   16 200
    //                   17 96
    //                   18 548
    //                   19 96
    //                   20 532;
           goto _L1 _L2 _L1 _L3 _L1 _L1 _L1 _L1 _L1 _L1 _L1 _L4 _L1 _L5 _L6 _L7 _L1 _L8 _L1 _L9
_L1:
        if (ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\f' || ch == '\b')
        {
            next();
        } else
        {
            nextToken();
            return;
        }
        if (true) goto _L10; else goto _L4
_L4:
        if (ch == '{')
        {
            token = 12;
            next();
            return;
        }
        if (ch == '[')
        {
            token = 14;
            next();
            return;
        }
          goto _L1
_L7:
        if (ch == ',')
        {
            token = 16;
            next();
            return;
        }
        if (ch == '}')
        {
            token = 13;
            next();
            return;
        }
        if (ch == ']')
        {
            token = 15;
            next();
            return;
        }
        if (ch == '\032')
        {
            token = 20;
            return;
        }
          goto _L1
_L2:
        if (ch >= '0' && ch <= '9')
        {
            pos = bp;
            scanNumber();
            return;
        }
        if (ch == '"')
        {
            pos = bp;
            scanString();
            return;
        }
        if (ch == '[')
        {
            token = 14;
            next();
            return;
        }
        if (ch == '{')
        {
            token = 12;
            next();
            return;
        }
          goto _L1
_L3:
        if (ch == '"')
        {
            pos = bp;
            scanString();
            return;
        }
        if (ch >= '0' && ch <= '9')
        {
            pos = bp;
            scanNumber();
            return;
        }
        if (ch == '[')
        {
            token = 14;
            next();
            return;
        }
        if (ch == '{')
        {
            token = 12;
            next();
            return;
        }
          goto _L1
_L5:
        if (ch == '[')
        {
            token = 14;
            next();
            return;
        }
        if (ch == '{')
        {
            token = 12;
            next();
            return;
        }
          goto _L1
_L6:
        if (ch == ']')
        {
            token = 15;
            next();
            return;
        }
_L9:
        if (ch != '\032') goto _L1; else goto _L11
_L11:
        token = 20;
        return;
_L8:
        nextIdent();
        return;
    }

    public final void nextTokenWithChar(char c)
    {
        sp = 0;
        do
        {
            if (ch == c)
            {
                next();
                nextToken();
                return;
            }
            if (ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\f' || ch == '\b')
            {
                next();
            } else
            {
                throw new JSONException((new StringBuilder()).append("not match ").append(c).append(" - ").append(ch).toString());
            }
        } while (true);
    }

    public final void nextTokenWithChar(char c, int i)
    {
        sp = 0;
_L2:
        if (ch == c)
        {
            next();
            break MISSING_BLOCK_LABEL_18;
        } else
        {
            if (isWhitespace(ch))
            {
                next();
            } else
            {
                throw new JSONException((new StringBuilder()).append("not match ").append(i).append(" - ").append(ch).toString());
            }
            continue; /* Loop/switch isn't completed */
        }
        do
        {
            if (i == 2)
            {
                if (ch >= '0' && ch <= '9')
                {
                    pos = bp;
                    scanNumber();
                    return;
                }
                if (ch == '"')
                {
                    pos = bp;
                    scanString();
                    return;
                }
            } else
            if (i == 4)
            {
                if (ch == '"')
                {
                    pos = bp;
                    scanString();
                    return;
                }
                if (ch >= '0' && ch <= '9')
                {
                    pos = bp;
                    scanNumber();
                    return;
                }
            } else
            if (i == 12)
            {
                if (ch == '{')
                {
                    token = 12;
                    next();
                    return;
                }
                if (ch == '[')
                {
                    token = 14;
                    next();
                    return;
                }
            } else
            if (i == 14)
            {
                if (ch == '[')
                {
                    token = 14;
                    next();
                    return;
                }
                if (ch == '{')
                {
                    token = 12;
                    next();
                    return;
                }
            }
            if (!isWhitespace(ch))
            {
                break;
            }
            next();
        } while (true);
        nextToken();
        return;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public final void nextTokenWithColon()
    {
        nextTokenWithChar(':');
    }

    public final void nextTokenWithColon(int i)
    {
        nextTokenWithChar(':');
    }

    public final void nextTokenWithComma()
    {
        nextTokenWithChar(':');
    }

    public final void nextTokenWithComma(int i)
    {
        nextTokenWithChar(',');
    }

    public abstract String numberString();

    public final int pos()
    {
        return pos;
    }

    protected final void putChar(char c)
    {
        if (sp == sbuf.length)
        {
            char ac[] = new char[sbuf.length * 2];
            System.arraycopy(sbuf, 0, ac, 0, sbuf.length);
            sbuf = ac;
        }
        char ac1[] = sbuf;
        int i = sp;
        sp = i + 1;
        ac1[i] = c;
    }

    public final void resetStringPosition()
    {
        sp = 0;
    }

    public boolean scanBoolean(char c)
    {
        int i;
        boolean flag;
        flag = false;
        matchStat = 0;
        i = charAt(bp + 0);
        if (i != 116) goto _L2; else goto _L1
_L1:
        if (charAt(bp + 1) != 'r' || charAt(bp + 1 + 1) != 'u' || charAt(bp + 1 + 2) != 'e') goto _L4; else goto _L3
_L3:
        byte byte0;
        i = bp;
        byte0 = 5;
        i = charAt(i + 4);
        flag = true;
_L6:
        if (i == c)
        {
            bp = bp + (byte0 - 1);
            next();
            matchStat = 3;
            return flag;
        } else
        {
            matchStat = -1;
            return flag;
        }
_L4:
        matchStat = -1;
        return false;
_L2:
        if (i == 102)
        {
            if (charAt(bp + 1) == 'a' && charAt(bp + 1 + 1) == 'l' && charAt(bp + 1 + 2) == 's' && charAt(bp + 1 + 3) == 'e')
            {
                i = bp;
                byte0 = 6;
                i = charAt(i + 5);
            } else
            {
                matchStat = -1;
                return false;
            }
        } else
        {
            byte0 = 1;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    public Enum scanEnum(Class class1, SymbolTable symboltable, char c)
    {
        symboltable = scanSymbolWithSeperator(symboltable, c);
        if (symboltable == null)
        {
            return null;
        } else
        {
            return Enum.valueOf(class1, symboltable);
        }
    }

    public final void scanFalse()
    {
        if (ch != 'f')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch != 'a')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch != 'l')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch != 's')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch != 'e')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch == ' ' || ch == ',' || ch == '}' || ch == ']' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\032' || ch == '\f' || ch == '\b' || ch == ':')
        {
            token = 7;
            return;
        } else
        {
            throw new JSONException("scan false error");
        }
    }

    public boolean scanFieldBoolean(char ac[])
    {
        matchStat = 0;
        if (!charArrayCompare(ac))
        {
            matchStat = -2;
            return false;
        }
        int k = ac.length;
        int l = bp;
        int i = k + 1;
        k = charAt(k + l);
        boolean flag;
        if (k == 116)
        {
            l = bp;
            k = i + 1;
            if (charAt(l + i) != 'r')
            {
                matchStat = -1;
                return false;
            }
            i = bp;
            l = k + 1;
            if (charAt(i + k) != 'u')
            {
                matchStat = -1;
                return false;
            }
            k = bp;
            i = l + 1;
            if (charAt(k + l) != 'e')
            {
                matchStat = -1;
                return false;
            }
            flag = true;
        } else
        if (k == 102)
        {
            int i1 = bp;
            k = i + 1;
            if (charAt(i1 + i) != 'a')
            {
                matchStat = -1;
                return false;
            }
            i1 = bp;
            i = k + 1;
            if (charAt(i1 + k) != 'l')
            {
                matchStat = -1;
                return false;
            }
            k = bp;
            i1 = i + 1;
            if (charAt(k + i) != 's')
            {
                matchStat = -1;
                return false;
            }
            if (charAt(i1 + bp) != 'e')
            {
                matchStat = -1;
                return false;
            }
            i = i1 + 1;
            flag = false;
        } else
        {
            matchStat = -1;
            return false;
        }
        l = bp;
        k = i + 1;
        i = charAt(i + l);
        if (i == 44)
        {
            bp = bp + (k - 1);
            next();
            matchStat = 3;
            token = 16;
            return flag;
        }
        if (i == 125)
        {
            int j1 = bp;
            int j = k + 1;
            k = charAt(j1 + k);
            if (k == ',')
            {
                token = 16;
                bp = bp + (j - 1);
                next();
            } else
            if (k == ']')
            {
                token = 15;
                bp = bp + (j - 1);
                next();
            } else
            if (k == '}')
            {
                token = 13;
                bp = bp + (j - 1);
                next();
            } else
            if (k == '\032')
            {
                token = 20;
                bp = bp + (j - 1);
                ch = '\032';
            } else
            {
                matchStat = -1;
                return false;
            }
            matchStat = 4;
            return flag;
        } else
        {
            matchStat = -1;
            return false;
        }
    }

    public final double scanFieldDouble(char c)
    {
label0:
        {
            int l;
            int i1;
label1:
            {
                matchStat = 0;
                int k = bp;
                int i = 1;
                k = charAt(k + 0);
                if (k < '0' || k > '9')
                {
                    break label0;
                }
                do
                {
                    k = bp;
                    l = i + 1;
                    i1 = charAt(i + k);
                    if (i1 < '0' || i1 > '9')
                    {
                        break;
                    }
                    i = l;
                } while (true);
                k = i1;
                i = l;
                if (i1 == '.')
                {
                    k = bp;
                    i = l + 1;
                    k = charAt(l + k);
                    if (k >= 48 && k <= 57)
                    {
                        do
                        {
                            k = bp;
                            l = i + 1;
                            i1 = charAt(i + k);
                            k = i1;
                            i = l;
                            if (i1 < '0')
                            {
                                break;
                            }
                            k = i1;
                            i = l;
                            if (i1 > '9')
                            {
                                break;
                            }
                            i = l;
                        } while (true);
                    } else
                    {
                        matchStat = -1;
                        return 0.0D;
                    }
                }
                if (k != 101)
                {
                    l = k;
                    i1 = i;
                    if (k != 69)
                    {
                        break label1;
                    }
                }
                l = bp;
                k = i + 1;
                i = charAt(l + i);
                if (i == 43 || i == 45)
                {
                    l = bp;
                    i = k + 1;
                    l = charAt(l + k);
                    k = i;
                    i = l;
                }
                do
                {
                    l = i;
                    i1 = k;
                    if (i < 48)
                    {
                        break;
                    }
                    l = i;
                    i1 = k;
                    if (i > 57)
                    {
                        break;
                    }
                    i = charAt(bp + k);
                    k++;
                } while (true);
            }
            int j = bp;
            double d = Double.parseDouble(subString(j, (bp + i1) - j - 1));
            if (l == c)
            {
                bp = bp + (i1 - 1);
                next();
                matchStat = 3;
                token = 16;
                return d;
            } else
            {
                matchStat = -1;
                return d;
            }
        }
        matchStat = -1;
        return 0.0D;
    }

    public final double scanFieldDouble(char ac[])
    {
        double d;
        int j1;
        int k1;
label0:
        {
label1:
            {
label2:
                {
                    matchStat = 0;
                    if (!charArrayCompare(ac))
                    {
                        matchStat = -2;
                        return 0.0D;
                    }
                    int l = ac.length;
                    j1 = bp;
                    int i = l + 1;
                    l = charAt(l + j1);
                    if (l < '0' || l > '9')
                    {
                        break label1;
                    }
                    do
                    {
                        l = bp;
                        j1 = i + 1;
                        k1 = charAt(i + l);
                        if (k1 < '0' || k1 > '9')
                        {
                            break;
                        }
                        i = j1;
                    } while (true);
                    l = k1;
                    i = j1;
                    if (k1 == '.')
                    {
                        l = bp;
                        i = j1 + 1;
                        l = charAt(j1 + l);
                        if (l >= 48 && l <= 57)
                        {
                            do
                            {
                                l = bp;
                                j1 = i + 1;
                                k1 = charAt(i + l);
                                l = k1;
                                i = j1;
                                if (k1 < '0')
                                {
                                    break;
                                }
                                l = k1;
                                i = j1;
                                if (k1 > '9')
                                {
                                    break;
                                }
                                i = j1;
                            } while (true);
                        } else
                        {
                            matchStat = -1;
                            return 0.0D;
                        }
                    }
                    if (l != 101)
                    {
                        k1 = l;
                        j1 = i;
                        if (l != 69)
                        {
                            break label2;
                        }
                    }
                    j1 = bp;
                    l = i + 1;
                    i = charAt(j1 + i);
                    if (i == 43 || i == 45)
                    {
                        j1 = bp;
                        i = l + 1;
                        j1 = charAt(j1 + l);
                        l = i;
                        i = j1;
                    }
                    do
                    {
                        k1 = i;
                        j1 = l;
                        if (i < 48)
                        {
                            break;
                        }
                        k1 = i;
                        j1 = l;
                        if (i > 57)
                        {
                            break;
                        }
                        i = charAt(bp + l);
                        l++;
                    } while (true);
                }
                int j = bp + ac.length;
                d = Double.parseDouble(subString(j, (bp + j1) - j - 1));
                if (k1 == 44)
                {
                    bp = bp + (j1 - 1);
                    next();
                    matchStat = 3;
                    token = 16;
                    return d;
                }
                break label0;
            }
            matchStat = -1;
            return 0.0D;
        }
        if (k1 == 125)
        {
            int i1 = bp;
            int k = j1 + 1;
            i1 = charAt(i1 + j1);
            if (i1 == ',')
            {
                token = 16;
                bp = bp + (k - 1);
                next();
            } else
            if (i1 == ']')
            {
                token = 15;
                bp = bp + (k - 1);
                next();
            } else
            if (i1 == '}')
            {
                token = 13;
                bp = bp + (k - 1);
                next();
            } else
            if (i1 == '\032')
            {
                token = 20;
                bp = bp + (k - 1);
                ch = '\032';
            } else
            {
                matchStat = -1;
                return 0.0D;
            }
            matchStat = 4;
            return d;
        } else
        {
            matchStat = -1;
            return 0.0D;
        }
    }

    public final float scanFieldFloat(char ac[])
    {
        matchStat = 0;
        if (!charArrayCompare(ac))
        {
            matchStat = -2;
            return 0.0F;
        }
        int j = ac.length;
        int i1 = bp;
        int i = j + 1;
        j = charAt(j + i1);
        float f;
        if (j >= '0' && j <= '9')
        {
            int k;
            char c;
            do
            {
                i1 = bp;
                k = i + 1;
                c = charAt(i + i1);
                if (c < '0' || c > '9')
                {
                    break;
                }
                i = k;
            } while (true);
            i1 = c;
            i = k;
            if (c == '.')
            {
                i1 = bp;
                i = k + 1;
                k = charAt(k + i1);
                if (k >= '0' && k <= '9')
                {
                    do
                    {
                        i1 = bp;
                        k = i + 1;
                        char c1 = charAt(i + i1);
                        i1 = c1;
                        i = k;
                        if (c1 < '0')
                        {
                            break;
                        }
                        i1 = c1;
                        i = k;
                        if (c1 > '9')
                        {
                            break;
                        }
                        i = k;
                    } while (true);
                } else
                {
                    matchStat = -1;
                    return 0.0F;
                }
            }
            k = bp + ac.length;
            f = Float.parseFloat(subString(k, (bp + i) - k - 1));
            if (i1 == 44)
            {
                bp = bp + (i - 1);
                next();
                matchStat = 3;
                token = 16;
                return f;
            }
        } else
        {
            matchStat = -1;
            return 0.0F;
        }
        if (i1 == 125)
        {
            int j1 = bp;
            int l = i + 1;
            i = charAt(j1 + i);
            if (i == ',')
            {
                token = 16;
                bp = bp + (l - 1);
                next();
            } else
            if (i == ']')
            {
                token = 15;
                bp = bp + (l - 1);
                next();
            } else
            if (i == '}')
            {
                token = 13;
                bp = bp + (l - 1);
                next();
            } else
            if (i == '\032')
            {
                bp = bp + (l - 1);
                token = 20;
                ch = '\032';
            } else
            {
                matchStat = -1;
                return 0.0F;
            }
            matchStat = 4;
            return f;
        } else
        {
            matchStat = -1;
            return 0.0F;
        }
    }

    public int scanFieldInt(char ac[])
    {
        matchStat = 0;
        if (!charArrayCompare(ac))
        {
            matchStat = -2;
            return 0;
        }
        int i = ac.length;
        int l = bp;
        int j = i + 1;
        i = charAt(i + l);
        if (i >= '0' && i <= '9')
        {
            i = digits[i];
            do
            {
                int i1 = bp;
                l = j + 1;
                j = charAt(j + i1);
                if (j < '0' || j > '9')
                {
                    break;
                }
                i = i * 10 + digits[j];
                j = l;
            } while (true);
            if (j == 46)
            {
                matchStat = -1;
                return 0;
            }
            if (i < 0)
            {
                matchStat = -1;
                return 0;
            }
        } else
        {
            matchStat = -1;
            return 0;
        }
        if (j == 44)
        {
            bp = bp + (l - 1);
            next();
            matchStat = 3;
            token = 16;
            return i;
        }
        if (j == 125)
        {
            int j1 = bp;
            int k = l + 1;
            l = charAt(j1 + l);
            if (l == ',')
            {
                token = 16;
                bp = bp + (k - 1);
                next();
            } else
            if (l == ']')
            {
                token = 15;
                bp = bp + (k - 1);
                next();
            } else
            if (l == '}')
            {
                token = 13;
                bp = bp + (k - 1);
                next();
            } else
            if (l == '\032')
            {
                token = 20;
                bp = bp + (k - 1);
                ch = '\032';
            } else
            {
                matchStat = -1;
                return 0;
            }
            matchStat = 4;
            return i;
        } else
        {
            matchStat = -1;
            return 0;
        }
    }

    public long scanFieldLong(char ac[])
    {
        matchStat = 0;
        if (!charArrayCompare(ac))
        {
            matchStat = -2;
            return 0L;
        }
        int k = ac.length;
        int l = bp;
        int i = k + 1;
        k = charAt(k + l);
        long l1;
        if (k >= '0' && k <= '9')
        {
            l1 = digits[k];
            do
            {
                int i1 = bp;
                k = i + 1;
                i = charAt(i + i1);
                if (i < '0' || i > '9')
                {
                    break;
                }
                l1 = l1 * 10L + (long)digits[i];
                i = k;
            } while (true);
            if (i == 46)
            {
                matchStat = -1;
                return 0L;
            }
            if (l1 < 0L)
            {
                matchStat = -1;
                return 0L;
            }
        } else
        {
            matchStat = -1;
            return 0L;
        }
        if (i == 44)
        {
            bp = bp + (k - 1);
            next();
            matchStat = 3;
            token = 16;
            return l1;
        }
        if (i == 125)
        {
            int j1 = bp;
            int j = k + 1;
            k = charAt(j1 + k);
            if (k == ',')
            {
                token = 16;
                bp = bp + (j - 1);
                next();
            } else
            if (k == ']')
            {
                token = 15;
                bp = bp + (j - 1);
                next();
            } else
            if (k == '}')
            {
                token = 13;
                bp = bp + (j - 1);
                next();
            } else
            if (k == '\032')
            {
                token = 20;
                bp = bp + (j - 1);
                ch = '\032';
            } else
            {
                matchStat = -1;
                return 0L;
            }
            matchStat = 4;
            return l1;
        } else
        {
            matchStat = -1;
            return 0L;
        }
    }

    public String scanFieldString(char ac[])
    {
        int l = 0;
        matchStat = 0;
        if (!charArrayCompare(ac))
        {
            matchStat = -2;
            return stringDefaultValue();
        }
        int j1 = ac.length;
        if (charAt(j1 + bp) != '"')
        {
            matchStat = -1;
            return stringDefaultValue();
        }
        int k1 = indexOf('"', bp + ac.length + 1);
        if (k1 == -1)
        {
            throw new JSONException("unclosed str");
        }
        int i = bp + ac.length + 1;
        String s = subString(i, k1 - i);
        i = bp + ac.length + 1;
label0:
        do
        {
label1:
            {
                boolean flag = l;
                if (i < k1)
                {
                    if (charAt(i) != '\\')
                    {
                        break label1;
                    }
                    flag = true;
                }
                if (flag)
                {
                    matchStat = -1;
                    return stringDefaultValue();
                }
                break label0;
            }
            i++;
        } while (true);
        int j = (k1 - (bp + ac.length + 1)) + 1 + (j1 + 1);
        l = bp;
        i = j + 1;
        j = charAt(j + l);
        if (j == ',')
        {
            bp = bp + (i - 1);
            next();
            matchStat = 3;
            return s;
        }
        if (j == '}')
        {
            int i1 = bp;
            int k = i + 1;
            i = charAt(i1 + i);
            if (i == ',')
            {
                token = 16;
                bp = bp + (k - 1);
                next();
            } else
            if (i == ']')
            {
                token = 15;
                bp = bp + (k - 1);
                next();
            } else
            if (i == '}')
            {
                token = 13;
                bp = bp + (k - 1);
                next();
            } else
            if (i == '\032')
            {
                token = 20;
                bp = bp + (k - 1);
                ch = '\032';
            } else
            {
                matchStat = -1;
                return stringDefaultValue();
            }
            matchStat = 4;
            return s;
        } else
        {
            matchStat = -1;
            return stringDefaultValue();
        }
    }

    public Collection scanFieldStringArray(char ac[], Class class1)
    {
        int i;
        int j;
        matchStat = 0;
        if (!charArrayCompare(ac))
        {
            matchStat = -2;
            return null;
        }
        int k;
        if (class1.isAssignableFrom(java/util/HashSet))
        {
            class1 = new HashSet();
        } else
        if (class1.isAssignableFrom(java/util/ArrayList))
        {
            class1 = new ArrayList();
        } else
        {
            try
            {
                class1 = (Collection)class1.newInstance();
            }
            // Misplaced declaration of an exception variable
            catch (char ac[])
            {
                throw new JSONException(ac.getMessage(), ac);
            }
        }
        i = ac.length;
        k = bp;
        j = i + 1;
        if (charAt(i + k) != '[')
        {
            matchStat = -1;
            return null;
        }
        k = bp;
        i = j + 1;
        j = charAt(k + j);
_L6:
        if (j != '"')
        {
            matchStat = -1;
            return null;
        }
        j = i;
_L4:
        int l;
label0:
        {
            int i1 = bp;
            l = j + 1;
            j = charAt(j + i1);
            if (j != 34)
            {
                break label0;
            }
            i = bp + i;
            class1.add(subString(i, (bp + l) - i - 1));
            i = bp;
            j = l + 1;
            i = charAt(i + l);
            if (i == ',')
            {
                l = bp;
                i = j + 1;
                j = charAt(l + j);
            } else
            {
                if (i == 93)
                {
                    l = bp;
                    i = j + 1;
                    j = charAt(l + j);
                    if (j == ',')
                    {
                        bp = bp + (i - 1);
                        next();
                        matchStat = 3;
                        return class1;
                    }
                } else
                {
                    matchStat = -1;
                    return null;
                }
                if (j == '}')
                {
                    l = bp;
                    j = i + 1;
                    i = charAt(l + i);
                    if (i == ',')
                    {
                        token = 16;
                        bp = bp + (j - 1);
                        next();
                    } else
                    if (i == ']')
                    {
                        token = 15;
                        bp = bp + (j - 1);
                        next();
                    } else
                    if (i == '}')
                    {
                        token = 13;
                        bp = bp + (j - 1);
                        next();
                    } else
                    if (i == '\032')
                    {
                        bp = bp + (j - 1);
                        token = 20;
                        ch = '\032';
                    } else
                    {
                        matchStat = -1;
                        return null;
                    }
                    matchStat = 4;
                    return class1;
                } else
                {
                    matchStat = -1;
                    return null;
                }
            }
        }
        if (true) goto _L2; else goto _L1
_L2:
        continue; /* Loop/switch isn't completed */
_L1:
        if (j == 92)
        {
            matchStat = -1;
            return null;
        }
        j = l;
        if (true) goto _L4; else goto _L3
_L3:
        if (true) goto _L6; else goto _L5
_L5:
    }

    public String scanFieldSymbol(char ac[], SymbolTable symboltable)
    {
        int i;
        int j;
        int i1;
        i = 0;
        matchStat = 0;
        if (!charArrayCompare(ac))
        {
            matchStat = -2;
            return null;
        }
        int l = ac.length;
        int k1 = bp;
        j = l + 1;
        if (charAt(l + k1) != '"')
        {
            matchStat = -1;
            return null;
        }
_L4:
        int l1 = bp;
        i1 = j + 1;
        j = charAt(j + l1);
        if (j == 34)
        {
            j = bp + ac.length + 1;
            ac = addSymbol(j, (bp + i1) - j - 1, i, symboltable);
            j = bp;
            i = i1 + 1;
            j = charAt(j + i1);
            if (j == 44)
            {
                bp = bp + (i - 1);
                next();
                matchStat = 3;
                return ac;
            }
            break; /* Loop/switch isn't completed */
        }
        i = i * 31 + j;
        if (j == 92)
        {
            matchStat = -1;
            return null;
        }
        if (true) goto _L2; else goto _L1
_L1:
        break; /* Loop/switch isn't completed */
_L2:
        j = i1;
        if (true) goto _L4; else goto _L3
_L3:
        if (j == 125)
        {
            int j1 = bp;
            int k = i + 1;
            i = charAt(j1 + i);
            if (i == ',')
            {
                token = 16;
                bp = bp + (k - 1);
                next();
            } else
            if (i == ']')
            {
                token = 15;
                bp = bp + (k - 1);
                next();
            } else
            if (i == '}')
            {
                token = 13;
                bp = bp + (k - 1);
                next();
            } else
            if (i == '\032')
            {
                token = 20;
                bp = bp + (k - 1);
                ch = '\032';
            } else
            {
                matchStat = -1;
                return null;
            }
            matchStat = 4;
            return ac;
        } else
        {
            matchStat = -1;
            return null;
        }
    }

    public final float scanFloat(char c)
    {
        matchStat = 0;
        int i = bp;
        int k = 1;
        i = charAt(i + 0);
        if (i >= '0' && i <= '9')
        {
            int j;
            char c1;
            do
            {
                int l = bp;
                j = k + 1;
                c1 = charAt(k + l);
                if (c1 < '0' || c1 > '9')
                {
                    break;
                }
                k = j;
            } while (true);
            k = c1;
            int i1 = j;
            if (c1 == '.')
            {
                i1 = bp;
                k = j + 1;
                j = charAt(j + i1);
                if (j >= '0' && j <= '9')
                {
                    do
                    {
                        i1 = bp;
                        j = k + 1;
                        char c2 = charAt(k + i1);
                        k = c2;
                        i1 = j;
                        if (c2 < '0')
                        {
                            break;
                        }
                        k = c2;
                        i1 = j;
                        if (c2 > '9')
                        {
                            break;
                        }
                        k = j;
                    } while (true);
                } else
                {
                    matchStat = -1;
                    return 0.0F;
                }
            }
            j = bp;
            float f = Float.parseFloat(subString(j, (bp + i1) - j - 1));
            if (k == c)
            {
                bp = bp + (i1 - 1);
                next();
                matchStat = 3;
                token = 16;
                return f;
            } else
            {
                matchStat = -1;
                return f;
            }
        } else
        {
            matchStat = -1;
            return 0.0F;
        }
    }

    public final void scanIdent()
    {
        np = bp - 1;
        hasSpecial = false;
        do
        {
            sp = sp + 1;
            next();
        } while (Character.isLetterOrDigit(ch));
        Object obj = stringVal();
        obj = (Integer)keywods.get(obj);
        if (obj != null)
        {
            token = ((Integer) (obj)).intValue();
            return;
        } else
        {
            token = 18;
            return;
        }
    }

    public int scanInt(char c)
    {
        matchStat = 0;
        int i = bp;
        int j = 1;
        i = charAt(i + 0);
        int k;
        if (i >= '0' && i <= '9')
        {
            i = digits[i];
            do
            {
                int l = bp;
                k = j + 1;
                j = charAt(j + l);
                if (j < '0' || j > '9')
                {
                    break;
                }
                i = i * 10 + digits[j];
                j = k;
            } while (true);
            if (j == 46)
            {
                matchStat = -1;
                return 0;
            }
            if (i < 0)
            {
                matchStat = -1;
                return 0;
            }
        } else
        {
            matchStat = -1;
            return 0;
        }
        if (j == c)
        {
            bp = bp + (k - 1);
            next();
            matchStat = 3;
            token = 16;
            return i;
        } else
        {
            matchStat = -1;
            return i;
        }
    }

    public long scanLong(char c)
    {
        matchStat = 0;
        int j = bp;
        int i = 1;
        j = charAt(j + 0);
        long l;
        if (j >= '0' && j <= '9')
        {
            l = digits[j];
            do
            {
                int k = bp;
                j = i + 1;
                i = charAt(i + k);
                if (i < '0' || i > '9')
                {
                    break;
                }
                l = l * 10L + (long)digits[i];
                i = j;
            } while (true);
            if (i == 46)
            {
                matchStat = -1;
                return 0L;
            }
            if (l < 0L)
            {
                matchStat = -1;
                return 0L;
            }
        } else
        {
            matchStat = -1;
            return 0L;
        }
        if (i == c)
        {
            bp = bp + (j - 1);
            next();
            matchStat = 3;
            token = 16;
            return l;
        } else
        {
            matchStat = -1;
            return l;
        }
    }

    public final void scanNULL()
    {
label0:
        {
            if (ch != 'N')
            {
                throw new JSONException("error parse NULL");
            }
            next();
            if (ch == 'U')
            {
                next();
                if (ch != 'L')
                {
                    throw new JSONException("error parse U");
                }
                next();
                if (ch != 'L')
                {
                    throw new JSONException("error parse NULL");
                }
                next();
                if (ch != ' ' && ch != ',' && ch != '}' && ch != ']' && ch != '\n' && ch != '\r' && ch != '\t' && ch != '\032' && ch != '\f' && ch != '\b')
                {
                    break label0;
                }
                token = 8;
            }
            return;
        }
        throw new JSONException("scan NULL error");
    }

    public final void scanNullOrNew()
    {
        if (ch != 'n')
        {
            throw new JSONException("error parse null or new");
        }
        next();
        if (ch == 'u')
        {
            next();
            if (ch != 'l')
            {
                throw new JSONException("error parse l");
            }
            next();
            if (ch != 'l')
            {
                throw new JSONException("error parse l");
            }
            next();
            if (ch == ' ' || ch == ',' || ch == '}' || ch == ']' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\032' || ch == '\f' || ch == '\b')
            {
                token = 8;
                return;
            } else
            {
                throw new JSONException("scan true error");
            }
        }
        if (ch != 'e')
        {
            throw new JSONException("error parse e");
        }
        next();
        if (ch != 'w')
        {
            throw new JSONException("error parse w");
        }
        next();
        if (ch == ' ' || ch == ',' || ch == '}' || ch == ']' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\032' || ch == '\f' || ch == '\b')
        {
            token = 9;
            return;
        } else
        {
            throw new JSONException("scan true error");
        }
    }

    public final void scanNumber()
    {
        np = bp;
        if (ch == '-')
        {
            sp = sp + 1;
            next();
        }
        while (ch >= '0' && ch <= '9') 
        {
            sp = sp + 1;
            next();
        }
        boolean flag = false;
        if (ch == '.')
        {
            sp = sp + 1;
            next();
            while (ch >= '0' && ch <= '9') 
            {
                sp = sp + 1;
                next();
            }
            flag = true;
        }
        if (ch == 'L')
        {
            sp = sp + 1;
            next();
        } else
        if (ch == 'S')
        {
            sp = sp + 1;
            next();
        } else
        if (ch == 'B')
        {
            sp = sp + 1;
            next();
        } else
        if (ch == 'F')
        {
            sp = sp + 1;
            next();
            flag = true;
        } else
        if (ch == 'D')
        {
            sp = sp + 1;
            next();
            flag = true;
        } else
        if (ch == 'e' || ch == 'E')
        {
            sp = sp + 1;
            next();
            if (ch == '+' || ch == '-')
            {
                sp = sp + 1;
                next();
            }
            while (ch >= '0' && ch <= '9') 
            {
                sp = sp + 1;
                next();
            }
            if (ch == 'D' || ch == 'F')
            {
                sp = sp + 1;
                next();
            }
            flag = true;
        }
        if (flag)
        {
            token = 3;
            return;
        } else
        {
            token = 2;
            return;
        }
    }

    public final void scanSet()
    {
        if (ch != 'S')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch != 'e')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch != 't')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\f' || ch == '\b' || ch == '[' || ch == '(')
        {
            token = 21;
            return;
        } else
        {
            throw new JSONException("scan set error");
        }
    }

    public String scanString(char c)
    {
        boolean flag1 = false;
        matchStat = 0;
        int i = charAt(bp + 0);
        if (i == 'n')
        {
            if (charAt(bp + 1) == 'u' && charAt(bp + 1 + 1) == 'l' && charAt(bp + 1 + 2) == 'l')
            {
                if (charAt(bp + 4) == c)
                {
                    bp = bp + 4;
                    next();
                    matchStat = 3;
                    return null;
                } else
                {
                    matchStat = -1;
                    return null;
                }
            } else
            {
                matchStat = -1;
                return null;
            }
        }
        if (i != '"')
        {
            matchStat = -1;
            return stringDefaultValue();
        }
        i = bp + 1;
        int j = indexOf('"', i);
        if (j == -1)
        {
            throw new JSONException("unclosed str");
        }
        String s = subString(bp + 1, j - i);
        i = bp + 1;
label0:
        do
        {
label1:
            {
                boolean flag = flag1;
                if (i < j)
                {
                    if (charAt(i) != '\\')
                    {
                        break label1;
                    }
                    flag = true;
                }
                if (flag)
                {
                    matchStat = -1;
                    return stringDefaultValue();
                }
                break label0;
            }
            i++;
        } while (true);
        i = (j - (bp + 1)) + 1 + 1;
        if (charAt(i + bp) == c)
        {
            bp = bp + ((i + 1) - 1);
            next();
            matchStat = 3;
            return s;
        } else
        {
            matchStat = -1;
            return s;
        }
    }

    public final void scanString()
    {
        np = bp;
        hasSpecial = false;
        do
        {
            char c = next();
            if (c == '"')
            {
                token = 4;
                ch = next();
                return;
            }
            if (c == '\032')
            {
                throw new JSONException((new StringBuilder()).append("unclosed string : ").append(c).toString());
            }
            if (c == '\\')
            {
                if (!hasSpecial)
                {
                    hasSpecial = true;
                    if (sp >= sbuf.length)
                    {
                        int k = sbuf.length * 2;
                        int i = k;
                        if (sp > k)
                        {
                            i = sp;
                        }
                        char ac[] = new char[i];
                        System.arraycopy(sbuf, 0, ac, 0, sbuf.length);
                        sbuf = ac;
                    }
                    copyTo(np + 1, sp, sbuf);
                }
                c = next();
                switch (c)
                {
                default:
                    ch = c;
                    throw new JSONException((new StringBuilder()).append("unclosed string : ").append(c).toString());

                case 48: // '0'
                    putChar('\0');
                    break;

                case 49: // '1'
                    putChar('\001');
                    break;

                case 50: // '2'
                    putChar('\002');
                    break;

                case 51: // '3'
                    putChar('\003');
                    break;

                case 52: // '4'
                    putChar('\004');
                    break;

                case 53: // '5'
                    putChar('\005');
                    break;

                case 54: // '6'
                    putChar('\006');
                    break;

                case 55: // '7'
                    putChar('\007');
                    break;

                case 98: // 'b'
                    putChar('\b');
                    break;

                case 116: // 't'
                    putChar('\t');
                    break;

                case 110: // 'n'
                    putChar('\n');
                    break;

                case 118: // 'v'
                    putChar('\013');
                    break;

                case 70: // 'F'
                case 102: // 'f'
                    putChar('\f');
                    break;

                case 114: // 'r'
                    putChar('\r');
                    break;

                case 34: // '"'
                    putChar('"');
                    break;

                case 39: // '\''
                    putChar('\'');
                    break;

                case 47: // '/'
                    putChar('/');
                    break;

                case 92: // '\\'
                    putChar('\\');
                    break;

                case 120: // 'x'
                    char c1 = next();
                    char c2 = next();
                    putChar((char)(digits[c1] * 16 + digits[c2]));
                    break;

                case 117: // 'u'
                    putChar((char)Integer.parseInt(new String(new char[] {
                        next(), next(), next(), next()
                    }), 16));
                    break;
                }
            } else
            if (!hasSpecial)
            {
                sp = sp + 1;
            } else
            if (sp == sbuf.length)
            {
                putChar(c);
            } else
            {
                char ac1[] = sbuf;
                int j = sp;
                sp = j + 1;
                ac1[j] = c;
            }
        } while (true);
    }

    public Collection scanStringArray(Class class1, char c)
    {
        int i;
        int j;
        matchStat = 0;
        if (class1.isAssignableFrom(java/util/HashSet))
        {
            class1 = new HashSet();
        } else
        if (class1.isAssignableFrom(java/util/ArrayList))
        {
            class1 = new ArrayList();
        } else
        {
            try
            {
                class1 = (Collection)class1.newInstance();
            }
            // Misplaced declaration of an exception variable
            catch (Class class1)
            {
                throw new JSONException(class1.getMessage(), class1);
            }
        }
        i = charAt(bp + 0);
        if (i == 'n')
        {
            if (charAt(bp + 1) == 'u' && charAt(bp + 1 + 1) == 'l' && charAt(bp + 1 + 2) == 'l')
            {
                if (charAt(bp + 4) == c)
                {
                    bp = bp + 4;
                    next();
                    matchStat = 3;
                    return null;
                } else
                {
                    matchStat = -1;
                    return null;
                }
            } else
            {
                matchStat = -1;
                return null;
            }
        }
        if (i != '[')
        {
            matchStat = -1;
            return null;
        }
        j = bp;
        i = 2;
        j = charAt(j + 1);
_L3:
        if (j != 110 || charAt(bp + i) != 'u' || charAt(bp + i + 1) != 'l' || charAt(bp + i + 2) != 'l') goto _L2; else goto _L1
_L1:
        j = i + 3;
        int k = bp;
        i = j + 1;
        j = charAt(j + k);
_L4:
        int l;
        if (j == 44)
        {
            j = charAt(bp + i);
            i++;
        } else
        if (j == 93)
        {
            if (charAt(bp + i) == c)
            {
                bp = bp + ((i + 1) - 1);
                next();
                matchStat = 3;
                return class1;
            } else
            {
                matchStat = -1;
                return class1;
            }
        } else
        {
            matchStat = -1;
            return null;
        }
        if (true) goto _L3; else goto _L2
_L2:
        if (j != 34)
        {
            matchStat = -1;
            return null;
        }
        j = i;
_L5:
label0:
        {
            int i1 = bp;
            l = j + 1;
            j = charAt(j + i1);
            if (j != 34)
            {
                break label0;
            }
            i = bp + i;
            class1.add(subString(i, (bp + l) - i - 1));
            j = bp;
            i = l + 1;
            j = charAt(j + l);
        }
          goto _L4
        if (j == 92)
        {
            matchStat = -1;
            return null;
        }
        j = l;
          goto _L5
    }

    public final String scanSymbol(SymbolTable symboltable)
    {
        skipWhitespace();
        if (ch == '"')
        {
            return scanSymbol(symboltable, '"');
        }
        if (ch == '\'')
        {
            if (!isEnabled(Feature.AllowSingleQuotes))
            {
                throw new JSONException("syntax error");
            } else
            {
                return scanSymbol(symboltable, '\'');
            }
        }
        if (ch == '}')
        {
            next();
            token = 13;
            return null;
        }
        if (ch == ',')
        {
            next();
            token = 16;
            return null;
        }
        if (ch == '\032')
        {
            token = 20;
            return null;
        }
        if (!isEnabled(Feature.AllowUnQuotedFieldNames))
        {
            throw new JSONException("syntax error");
        } else
        {
            return scanSymbolUnQuoted(symboltable);
        }
    }

    public final String scanSymbol(SymbolTable symboltable, char c)
    {
        np = bp;
        sp = 0;
        int j = 0;
        int i = 0;
        do
        {
            char c1 = next();
            if (c1 == c)
            {
                token = 4;
                if (j == 0)
                {
                    char c2;
                    char ac[];
                    int k;
                    if (np == -1)
                    {
                        c = '\0';
                    } else
                    {
                        c = np + 1;
                    }
                    symboltable = addSymbol(c, sp, i, symboltable);
                } else
                {
                    symboltable = symboltable.addSymbol(sbuf, 0, sp, i);
                }
                sp = 0;
                next();
                return symboltable;
            }
            if (c1 == '\032')
            {
                throw new JSONException("unclosed.str");
            }
            if (c1 == '\\')
            {
                k = j;
                if (j == 0)
                {
                    if (sp >= sbuf.length)
                    {
                        k = sbuf.length * 2;
                        j = k;
                        if (sp > k)
                        {
                            j = sp;
                        }
                        ac = new char[j];
                        System.arraycopy(sbuf, 0, ac, 0, sbuf.length);
                        sbuf = ac;
                    }
                    arrayCopy(np + 1, sbuf, 0, sp);
                    k = 1;
                }
                c1 = next();
                switch (c1)
                {
                default:
                    ch = c1;
                    throw new JSONException("unclosed.str.lit");

                case 48: // '0'
                    i = i * 31 + c1;
                    putChar('\0');
                    j = k;
                    break;

                case 49: // '1'
                    i = i * 31 + c1;
                    putChar('\001');
                    j = k;
                    break;

                case 50: // '2'
                    i = i * 31 + c1;
                    putChar('\002');
                    j = k;
                    break;

                case 51: // '3'
                    i = i * 31 + c1;
                    putChar('\003');
                    j = k;
                    break;

                case 52: // '4'
                    i = i * 31 + c1;
                    putChar('\004');
                    j = k;
                    break;

                case 53: // '5'
                    i = i * 31 + c1;
                    putChar('\005');
                    j = k;
                    break;

                case 54: // '6'
                    i = i * 31 + c1;
                    putChar('\006');
                    j = k;
                    break;

                case 55: // '7'
                    i = i * 31 + c1;
                    putChar('\007');
                    j = k;
                    break;

                case 98: // 'b'
                    i = i * 31 + 8;
                    putChar('\b');
                    j = k;
                    break;

                case 116: // 't'
                    i = i * 31 + 9;
                    putChar('\t');
                    j = k;
                    break;

                case 110: // 'n'
                    i = i * 31 + 10;
                    putChar('\n');
                    j = k;
                    break;

                case 118: // 'v'
                    i = i * 31 + 11;
                    putChar('\013');
                    j = k;
                    break;

                case 70: // 'F'
                case 102: // 'f'
                    i = i * 31 + 12;
                    putChar('\f');
                    j = k;
                    break;

                case 114: // 'r'
                    i = i * 31 + 13;
                    putChar('\r');
                    j = k;
                    break;

                case 34: // '"'
                    i = i * 31 + 34;
                    putChar('"');
                    j = k;
                    break;

                case 39: // '\''
                    i = i * 31 + 39;
                    putChar('\'');
                    j = k;
                    break;

                case 47: // '/'
                    i = i * 31 + 47;
                    putChar('/');
                    j = k;
                    break;

                case 92: // '\\'
                    i = i * 31 + 92;
                    putChar('\\');
                    j = k;
                    break;

                case 120: // 'x'
                    c1 = next();
                    ch = c1;
                    c2 = next();
                    ch = c2;
                    c1 = (char)(digits[c1] * 16 + digits[c2]);
                    i = i * 31 + c1;
                    putChar(c1);
                    j = k;
                    break;

                case 117: // 'u'
                    j = Integer.parseInt(new String(new char[] {
                        next(), next(), next(), next()
                    }), 16);
                    i = i * 31 + j;
                    putChar((char)j);
                    j = k;
                    break;
                }
            } else
            {
                i = i * 31 + c1;
                if (j == 0)
                {
                    sp = sp + 1;
                } else
                if (sp == sbuf.length)
                {
                    putChar(c1);
                } else
                {
                    ac = sbuf;
                    k = sp;
                    sp = k + 1;
                    ac[k] = c1;
                }
            }
        } while (true);
    }

    public final String scanSymbolUnQuoted(SymbolTable symboltable)
    {
        boolean aflag[] = IOUtils.firstIdentifierFlags;
        char c = ch;
        boolean flag;
        if (ch >= aflag.length || aflag[c])
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (!flag)
        {
            throw new JSONException((new StringBuilder()).append("illegal identifier : ").append(ch).toString());
        }
        aflag = IOUtils.identifierFlags;
        np = bp;
        sp = 1;
        int i = c;
        do
        {
            char c1 = next();
            if (c1 < aflag.length && !aflag[c1])
            {
                ch = charAt(bp);
                token = 18;
                if (sp == 4 && i == 0x33c587 && charAt(np) == 'n' && charAt(np + 1) == 'u' && charAt(np + 2) == 'l' && charAt(np + 3) == 'l')
                {
                    return null;
                } else
                {
                    return addSymbol(np, sp, i, symboltable);
                }
            }
            i = i * 31 + c1;
            sp = sp + 1;
        } while (true);
    }

    public String scanSymbolWithSeperator(SymbolTable symboltable, char c)
    {
        int i = 0;
        matchStat = 0;
        int k = bp;
        int j = 1;
        k = charAt(k + 0);
        if (k == 'n')
        {
            if (charAt(bp + 1) == 'u' && charAt(bp + 1 + 1) == 'l' && charAt(bp + 1 + 2) == 'l')
            {
                if (charAt(bp + 4) == c)
                {
                    bp = bp + 4;
                    next();
                    matchStat = 3;
                    return null;
                } else
                {
                    matchStat = -1;
                    return null;
                }
            } else
            {
                matchStat = -1;
                return null;
            }
        }
        if (k != '"')
        {
            matchStat = -1;
            return null;
        }
        do
        {
            int i1 = bp;
            l = j + 1;
            j = charAt(j + i1);
            if (j == 34)
            {
                j = bp + 0 + 1;
                symboltable = addSymbol(j, (bp + l) - j - 1, i, symboltable);
                if (charAt(bp + l) == c)
                {
                    bp = bp + ((l + 1) - 1);
                    next();
                    matchStat = 3;
                    return symboltable;
                } else
                {
                    matchStat = -1;
                    return symboltable;
                }
            }
            i = i * 31 + j;
            if (j != 92)
            {
                j = l;
            } else
            {
                matchStat = -1;
                return null;
            }
        } while (true);
    }

    public final void scanTreeSet()
    {
        if (ch != 'T')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch != 'r')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch != 'e')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch != 'e')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch != 'S')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch != 'e')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch != 't')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\f' || ch == '\b' || ch == '[' || ch == '(')
        {
            token = 22;
            return;
        } else
        {
            throw new JSONException("scan set error");
        }
    }

    public final void scanTrue()
    {
        if (ch != 't')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch != 'r')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch != 'u')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch != 'e')
        {
            throw new JSONException("error parse true");
        }
        next();
        if (ch == ' ' || ch == ',' || ch == '}' || ch == ']' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\032' || ch == '\f' || ch == '\b' || ch == ':')
        {
            token = 6;
            return;
        } else
        {
            throw new JSONException("scan true error");
        }
    }

    public int scanType(String s)
    {
        int i = 0;
        matchStat = 0;
        if (!charArrayCompare(typeFieldName))
        {
            return -2;
        }
        int j = bp + typeFieldName.length;
        int k;
        for (k = s.length(); i < k; i++)
        {
            if (s.charAt(i) != charAt(j + i))
            {
                return -1;
            }
        }

        i = j + k;
        if (charAt(i) != '"')
        {
            return -1;
        }
        j = i + 1;
        ch = charAt(j);
        if (ch == ',')
        {
            i = j + 1;
            ch = charAt(i);
            bp = i;
            token = 16;
            return 3;
        }
        i = j;
        if (ch == '}')
        {
            i = j + 1;
            ch = charAt(i);
            if (ch == ',')
            {
                token = 16;
                i++;
                ch = charAt(i);
            } else
            if (ch == ']')
            {
                token = 15;
                i++;
                ch = charAt(i);
            } else
            if (ch == '}')
            {
                token = 13;
                i++;
                ch = charAt(i);
            } else
            if (ch == '\032')
            {
                token = 20;
            } else
            {
                return -1;
            }
            matchStat = 4;
        }
        bp = i;
        return matchStat;
    }

    public final void scanUndefined()
    {
        if (ch != 'u')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch != 'n')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch != 'd')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch != 'e')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch != 'f')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch != 'i')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch != 'n')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch != 'e')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch != 'd')
        {
            throw new JSONException("error parse false");
        }
        next();
        if (ch == ' ' || ch == ',' || ch == '}' || ch == ']' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\032' || ch == '\f' || ch == '\b')
        {
            token = 23;
            return;
        } else
        {
            throw new JSONException("scan false error");
        }
    }

    public final void skipWhitespace()
    {
        while (ch < IOUtils.whitespaceFlags.length && IOUtils.whitespaceFlags[ch]) 
        {
            next();
        }
    }

    public final String stringDefaultValue()
    {
        if (isEnabled(Feature.InitStringFieldAsEmpty))
        {
            return "";
        } else
        {
            return null;
        }
    }

    public abstract String stringVal();

    public abstract String subString(int i, int j);

    public final int token()
    {
        return token;
    }

    public final String tokenName()
    {
        return JSONToken.name(token);
    }

    static 
    {
        HashMap hashmap = new HashMap();
        hashmap.put("null", Integer.valueOf(8));
        hashmap.put("new", Integer.valueOf(9));
        hashmap.put("true", Integer.valueOf(6));
        hashmap.put("false", Integer.valueOf(7));
        hashmap.put("undefined", Integer.valueOf(23));
        DEFAULT_KEYWORDS = hashmap;
        typeFieldName = (new StringBuilder()).append("\"").append(JSON.DEFAULT_TYPE_KEY).append("\":\"").toString().toCharArray();
        digits = new int[103];
        for (int i = 48; i <= 57; i++)
        {
            digits[i] = i - 48;
        }

        for (int j = 97; j <= 102; j++)
        {
            digits[j] = (j - 97) + 10;
        }

        for (int k = 65; k <= 70; k++)
        {
            digits[k] = (k - 65) + 10;
        }

    }
}
