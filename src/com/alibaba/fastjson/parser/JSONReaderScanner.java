// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.util.Base64;
import com.alibaba.fastjson.util.IOUtils;
import java.io.CharArrayReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.ref.SoftReference;

// Referenced classes of package com.alibaba.fastjson.parser:
//            JSONLexerBase, SymbolTable

public final class JSONReaderScanner extends JSONLexerBase
{

    public static int BUF_INIT_LEN = 8192;
    private static final ThreadLocal BUF_REF_LOCAL = new ThreadLocal();
    private char buf[];
    private int bufLength;
    private Reader reader;

    public JSONReaderScanner(Reader reader1)
    {
        this(reader1, JSON.DEFAULT_PARSER_FEATURE);
    }

    public JSONReaderScanner(Reader reader1, int i)
    {
        reader = reader1;
        features = i;
        SoftReference softreference = (SoftReference)BUF_REF_LOCAL.get();
        if (softreference != null)
        {
            buf = (char[])softreference.get();
            BUF_REF_LOCAL.set(null);
        }
        if (buf == null)
        {
            buf = new char[BUF_INIT_LEN];
        }
        try
        {
            bufLength = reader1.read(buf);
        }
        // Misplaced declaration of an exception variable
        catch (Reader reader1)
        {
            throw new JSONException(reader1.getMessage(), reader1);
        }
        bp = -1;
        next();
        if (ch == '\uFEFF')
        {
            next();
        }
    }

    public JSONReaderScanner(String s)
    {
        this(s, JSON.DEFAULT_PARSER_FEATURE);
    }

    public JSONReaderScanner(String s, int i)
    {
        this(((Reader) (new StringReader(s))), i);
    }

    public JSONReaderScanner(char ac[], int i)
    {
        this(ac, i, JSON.DEFAULT_PARSER_FEATURE);
    }

    public JSONReaderScanner(char ac[], int i, int j)
    {
        this(((Reader) (new CharArrayReader(ac, 0, i))), j);
    }

    public final String addSymbol(int i, int j, int k, SymbolTable symboltable)
    {
        return symboltable.addSymbol(buf, i, j, k);
    }

    protected final void arrayCopy(int i, char ac[], int j, int k)
    {
        System.arraycopy(buf, i, ac, j, k);
    }

    public byte[] bytesValue()
    {
        return Base64.decodeFast(buf, np + 1, sp);
    }

    public final boolean charArrayCompare(char ac[])
    {
        for (int i = 0; i < ac.length; i++)
        {
            if (charAt(bp + i) != ac[i])
            {
                return false;
            }
        }

        return true;
    }

    public final char charAt(int i)
    {
        char c;
        int j;
        c = '\032';
        j = i;
        if (i < bufLength)
        {
            break MISSING_BLOCK_LABEL_174;
        }
        if (bufLength != -1) goto _L2; else goto _L1
_L1:
        if (i < sp)
        {
            c = buf[i];
        }
_L4:
        return c;
_L2:
        j = bufLength - bp;
        if (j > 0)
        {
            System.arraycopy(buf, bp, buf, 0, j);
        }
        try
        {
            bufLength = reader.read(buf, j, buf.length - j);
        }
        catch (IOException ioexception)
        {
            throw new JSONException(ioexception.getMessage(), ioexception);
        }
        if (bufLength == 0)
        {
            throw new JSONException("illegal stat, textLength is zero");
        }
        if (bufLength == -1) goto _L4; else goto _L3
_L3:
        bufLength = bufLength + j;
        j = i - bp;
        np = np - bp;
        bp = 0;
        return buf[j];
    }

    public void close()
    {
        super.close();
        BUF_REF_LOCAL.set(new SoftReference(buf));
        buf = null;
        IOUtils.close(reader);
    }

    protected final void copyTo(int i, int j, char ac[])
    {
        System.arraycopy(buf, i, ac, 0, j);
    }

    public final int indexOf(char c, int i)
    {
        i -= bp;
        do
        {
            if (c == charAt(bp + i))
            {
                return i + bp;
            }
            if (c == '\032')
            {
                return -1;
            }
            i++;
        } while (true);
    }

    public boolean isEOF()
    {
        return bufLength == -1 || bp == buf.length || ch == '\032' && bp + 1 == buf.length;
    }

    public final char next()
    {
        int i;
        int j;
        j = bp + 1;
        bp = j;
        i = j;
        if (j < bufLength)
        {
            break MISSING_BLOCK_LABEL_253;
        }
        if (bufLength == -1)
        {
            return '\032';
        }
        if (sp > 0)
        {
            j = bufLength - sp;
            i = j;
            if (ch == '"')
            {
                i = j - 1;
            }
            System.arraycopy(buf, i, buf, 0, sp);
        }
        np = -1;
        j = sp;
        bp = j;
        char ac[];
        int k;
        int l;
        try
        {
            l = bp;
            k = buf.length - l;
        }
        catch (IOException ioexception)
        {
            throw new JSONException(ioexception.getMessage(), ioexception);
        }
        i = k;
        if (k != 0)
        {
            break MISSING_BLOCK_LABEL_169;
        }
        ac = new char[buf.length * 2];
        System.arraycopy(buf, 0, ac, 0, buf.length);
        buf = ac;
        i = buf.length - l;
        bufLength = reader.read(buf, bp, i);
        if (bufLength == 0)
        {
            throw new JSONException("illegal stat, textLength is zero");
        }
        if (bufLength == -1)
        {
            ch = '\032';
            return '\032';
        }
        bufLength = bufLength + bp;
        i = j;
        char c = buf[i];
        ch = c;
        return c;
    }

    public final String numberString()
    {
        int i;
        int j;
label0:
        {
            j = np;
            i = j;
            if (j == -1)
            {
                i = 0;
            }
            char c = charAt((sp + i) - 1);
            int k = sp;
            if (c != 'L' && c != 'S' && c != 'B' && c != 'F')
            {
                j = k;
                if (c != 'D')
                {
                    break label0;
                }
            }
            j = k - 1;
        }
        return new String(buf, i, j);
    }

    public final String stringVal()
    {
        if (!hasSpecial)
        {
            int i = np + 1;
            if (i < 0)
            {
                throw new IllegalStateException();
            }
            if (i > buf.length - sp)
            {
                throw new IllegalStateException();
            } else
            {
                return new String(buf, i, sp);
            }
        } else
        {
            return new String(sbuf, 0, sp);
        }
    }

    public final String subString(int i, int j)
    {
        if (j < 0)
        {
            throw new StringIndexOutOfBoundsException(j);
        } else
        {
            return new String(buf, i, j);
        }
    }

}
