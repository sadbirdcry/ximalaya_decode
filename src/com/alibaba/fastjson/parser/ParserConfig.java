// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.parser;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.alibaba.fastjson.asm.ASMException;
import com.alibaba.fastjson.parser.deserializer.ASMDeserializerFactory;
import com.alibaba.fastjson.parser.deserializer.ASMJavaBeanDeserializer;
import com.alibaba.fastjson.parser.deserializer.ArrayDeserializer;
import com.alibaba.fastjson.parser.deserializer.ArrayListTypeFieldDeserializer;
import com.alibaba.fastjson.parser.deserializer.AutowiredObjectDeserializer;
import com.alibaba.fastjson.parser.deserializer.BooleanFieldDeserializer;
import com.alibaba.fastjson.parser.deserializer.CharArrayDeserializer;
import com.alibaba.fastjson.parser.deserializer.ClassDerializer;
import com.alibaba.fastjson.parser.deserializer.CollectionDeserializer;
import com.alibaba.fastjson.parser.deserializer.DateDeserializer;
import com.alibaba.fastjson.parser.deserializer.DateFormatDeserializer;
import com.alibaba.fastjson.parser.deserializer.DefaultFieldDeserializer;
import com.alibaba.fastjson.parser.deserializer.EnumDeserializer;
import com.alibaba.fastjson.parser.deserializer.FieldDeserializer;
import com.alibaba.fastjson.parser.deserializer.IntegerFieldDeserializer;
import com.alibaba.fastjson.parser.deserializer.JSONArrayDeserializer;
import com.alibaba.fastjson.parser.deserializer.JSONObjectDeserializer;
import com.alibaba.fastjson.parser.deserializer.JavaBeanDeserializer;
import com.alibaba.fastjson.parser.deserializer.JavaObjectDeserializer;
import com.alibaba.fastjson.parser.deserializer.Jdk8DateCodec;
import com.alibaba.fastjson.parser.deserializer.LongFieldDeserializer;
import com.alibaba.fastjson.parser.deserializer.MapDeserializer;
import com.alibaba.fastjson.parser.deserializer.NumberDeserializer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.parser.deserializer.SqlDateDeserializer;
import com.alibaba.fastjson.parser.deserializer.StackTraceElementDeserializer;
import com.alibaba.fastjson.parser.deserializer.StringFieldDeserializer;
import com.alibaba.fastjson.parser.deserializer.ThrowableDeserializer;
import com.alibaba.fastjson.parser.deserializer.TimeDeserializer;
import com.alibaba.fastjson.parser.deserializer.TimestampDeserializer;
import com.alibaba.fastjson.serializer.AtomicIntegerArrayCodec;
import com.alibaba.fastjson.serializer.AtomicLongArrayCodec;
import com.alibaba.fastjson.serializer.BigDecimalCodec;
import com.alibaba.fastjson.serializer.BigIntegerCodec;
import com.alibaba.fastjson.serializer.BooleanCodec;
import com.alibaba.fastjson.serializer.CalendarCodec;
import com.alibaba.fastjson.serializer.CharacterCodec;
import com.alibaba.fastjson.serializer.CharsetCodec;
import com.alibaba.fastjson.serializer.ColorCodec;
import com.alibaba.fastjson.serializer.CurrencyCodec;
import com.alibaba.fastjson.serializer.FileCodec;
import com.alibaba.fastjson.serializer.FloatCodec;
import com.alibaba.fastjson.serializer.FontCodec;
import com.alibaba.fastjson.serializer.InetAddressCodec;
import com.alibaba.fastjson.serializer.InetSocketAddressCodec;
import com.alibaba.fastjson.serializer.IntegerCodec;
import com.alibaba.fastjson.serializer.LocaleCodec;
import com.alibaba.fastjson.serializer.LongCodec;
import com.alibaba.fastjson.serializer.PatternCodec;
import com.alibaba.fastjson.serializer.PointCodec;
import com.alibaba.fastjson.serializer.RectangleCodec;
import com.alibaba.fastjson.serializer.ReferenceCodec;
import com.alibaba.fastjson.serializer.StringCodec;
import com.alibaba.fastjson.serializer.TimeZoneCodec;
import com.alibaba.fastjson.serializer.URICodec;
import com.alibaba.fastjson.serializer.URLCodec;
import com.alibaba.fastjson.serializer.UUIDCodec;
import com.alibaba.fastjson.util.ASMUtils;
import com.alibaba.fastjson.util.DeserializeBeanInfo;
import com.alibaba.fastjson.util.FieldInfo;
import com.alibaba.fastjson.util.IdentityHashMap;
import com.alibaba.fastjson.util.ServiceLoader;
import java.io.Closeable;
import java.io.File;
import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.AccessControlException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicLongArray;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

// Referenced classes of package com.alibaba.fastjson.parser:
//            SymbolTable

public class ParserConfig
{

    private static ParserConfig global = new ParserConfig();
    private boolean asmEnable;
    protected ASMDeserializerFactory asmFactory;
    private final IdentityHashMap derializers;
    private final Set primitiveClasses;
    protected final SymbolTable symbolTable;

    public ParserConfig()
    {
        this(null, null);
    }

    public ParserConfig(ASMDeserializerFactory asmdeserializerfactory)
    {
        this(asmdeserializerfactory, null);
    }

    private ParserConfig(ASMDeserializerFactory asmdeserializerfactory, ClassLoader classloader)
    {
        ASMDeserializerFactory asmdeserializerfactory1;
        primitiveClasses = new HashSet();
        derializers = new IdentityHashMap();
        boolean flag;
        if (!ASMUtils.isAndroid())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        asmEnable = flag;
        symbolTable = new SymbolTable();
        asmdeserializerfactory1 = asmdeserializerfactory;
        if (asmdeserializerfactory != null) goto _L2; else goto _L1
_L1:
        if (classloader != null) goto _L4; else goto _L3
_L3:
        classloader = ASMDeserializerFactory.getInstance();
        asmdeserializerfactory = classloader;
_L5:
        asmdeserializerfactory1 = asmdeserializerfactory;
_L2:
        asmFactory = asmdeserializerfactory1;
        if (asmdeserializerfactory1 == null)
        {
            asmEnable = false;
        }
        primitiveClasses.add(Boolean.TYPE);
        primitiveClasses.add(java/lang/Boolean);
        primitiveClasses.add(Character.TYPE);
        primitiveClasses.add(java/lang/Character);
        primitiveClasses.add(Byte.TYPE);
        primitiveClasses.add(java/lang/Byte);
        primitiveClasses.add(Short.TYPE);
        primitiveClasses.add(java/lang/Short);
        primitiveClasses.add(Integer.TYPE);
        primitiveClasses.add(java/lang/Integer);
        primitiveClasses.add(Long.TYPE);
        primitiveClasses.add(java/lang/Long);
        primitiveClasses.add(Float.TYPE);
        primitiveClasses.add(java/lang/Float);
        primitiveClasses.add(Double.TYPE);
        primitiveClasses.add(java/lang/Double);
        primitiveClasses.add(java/math/BigInteger);
        primitiveClasses.add(java/math/BigDecimal);
        primitiveClasses.add(java/lang/String);
        primitiveClasses.add(java/util/Date);
        primitiveClasses.add(java/sql/Date);
        primitiveClasses.add(java/sql/Time);
        primitiveClasses.add(java/sql/Timestamp);
        derializers.put(java/text/SimpleDateFormat, DateFormatDeserializer.instance);
        derializers.put(java/sql/Timestamp, TimestampDeserializer.instance);
        derializers.put(java/sql/Date, SqlDateDeserializer.instance);
        derializers.put(java/sql/Time, TimeDeserializer.instance);
        derializers.put(java/util/Date, DateDeserializer.instance);
        derializers.put(java/util/Calendar, CalendarCodec.instance);
        derializers.put(com/alibaba/fastjson/JSONObject, JSONObjectDeserializer.instance);
        derializers.put(com/alibaba/fastjson/JSONArray, JSONArrayDeserializer.instance);
        derializers.put(java/util/Map, MapDeserializer.instance);
        derializers.put(java/util/HashMap, MapDeserializer.instance);
        derializers.put(java/util/LinkedHashMap, MapDeserializer.instance);
        derializers.put(java/util/TreeMap, MapDeserializer.instance);
        derializers.put(java/util/concurrent/ConcurrentMap, MapDeserializer.instance);
        derializers.put(java/util/concurrent/ConcurrentHashMap, MapDeserializer.instance);
        derializers.put(java/util/Collection, CollectionDeserializer.instance);
        derializers.put(java/util/List, CollectionDeserializer.instance);
        derializers.put(java/util/ArrayList, CollectionDeserializer.instance);
        derializers.put(java/lang/Object, JavaObjectDeserializer.instance);
        derializers.put(java/lang/String, StringCodec.instance);
        derializers.put(java/lang/StringBuffer, StringCodec.instance);
        derializers.put(java/lang/StringBuilder, StringCodec.instance);
        derializers.put(Character.TYPE, CharacterCodec.instance);
        derializers.put(java/lang/Character, CharacterCodec.instance);
        derializers.put(Byte.TYPE, NumberDeserializer.instance);
        derializers.put(java/lang/Byte, NumberDeserializer.instance);
        derializers.put(Short.TYPE, NumberDeserializer.instance);
        derializers.put(java/lang/Short, NumberDeserializer.instance);
        derializers.put(Integer.TYPE, IntegerCodec.instance);
        derializers.put(java/lang/Integer, IntegerCodec.instance);
        derializers.put(Long.TYPE, LongCodec.instance);
        derializers.put(java/lang/Long, LongCodec.instance);
        derializers.put(java/math/BigInteger, BigIntegerCodec.instance);
        derializers.put(java/math/BigDecimal, BigDecimalCodec.instance);
        derializers.put(Float.TYPE, FloatCodec.instance);
        derializers.put(java/lang/Float, FloatCodec.instance);
        derializers.put(Double.TYPE, NumberDeserializer.instance);
        derializers.put(java/lang/Double, NumberDeserializer.instance);
        derializers.put(Boolean.TYPE, BooleanCodec.instance);
        derializers.put(java/lang/Boolean, BooleanCodec.instance);
        derializers.put(java/lang/Class, ClassDerializer.instance);
        derializers.put([C, CharArrayDeserializer.instance);
        derializers.put(java/util/concurrent/atomic/AtomicBoolean, BooleanCodec.instance);
        derializers.put(java/util/concurrent/atomic/AtomicInteger, IntegerCodec.instance);
        derializers.put(java/util/concurrent/atomic/AtomicLong, LongCodec.instance);
        derializers.put(java/util/concurrent/atomic/AtomicReference, ReferenceCodec.instance);
        derializers.put(java/lang/ref/WeakReference, ReferenceCodec.instance);
        derializers.put(java/lang/ref/SoftReference, ReferenceCodec.instance);
        derializers.put(java/util/UUID, UUIDCodec.instance);
        derializers.put(java/util/TimeZone, TimeZoneCodec.instance);
        derializers.put(java/util/Locale, LocaleCodec.instance);
        derializers.put(java/util/Currency, CurrencyCodec.instance);
        derializers.put(java/net/InetAddress, InetAddressCodec.instance);
        derializers.put(java/net/Inet4Address, InetAddressCodec.instance);
        derializers.put(java/net/Inet6Address, InetAddressCodec.instance);
        derializers.put(java/net/InetSocketAddress, InetSocketAddressCodec.instance);
        derializers.put(java/io/File, FileCodec.instance);
        derializers.put(java/net/URI, URICodec.instance);
        derializers.put(java/net/URL, URLCodec.instance);
        derializers.put(java/util/regex/Pattern, PatternCodec.instance);
        derializers.put(java/nio/charset/Charset, CharsetCodec.instance);
        derializers.put(java/lang/Number, NumberDeserializer.instance);
        derializers.put(java/util/concurrent/atomic/AtomicIntegerArray, AtomicIntegerArrayCodec.instance);
        derializers.put(java/util/concurrent/atomic/AtomicLongArray, AtomicLongArrayCodec.instance);
        derializers.put(java/lang/StackTraceElement, StackTraceElementDeserializer.instance);
        derializers.put(java/io/Serializable, JavaObjectDeserializer.instance);
        derializers.put(java/lang/Cloneable, JavaObjectDeserializer.instance);
        derializers.put(java/lang/Comparable, JavaObjectDeserializer.instance);
        derializers.put(java/io/Closeable, JavaObjectDeserializer.instance);
        try
        {
            derializers.put(Class.forName("java.awt.Point"), PointCodec.instance);
            derializers.put(Class.forName("java.awt.Font"), FontCodec.instance);
            derializers.put(Class.forName("java.awt.Rectangle"), RectangleCodec.instance);
            derializers.put(Class.forName("java.awt.Color"), ColorCodec.instance);
        }
        // Misplaced declaration of an exception variable
        catch (ASMDeserializerFactory asmdeserializerfactory) { }
        try
        {
            derializers.put(Class.forName("java.time.LocalDateTime"), Jdk8DateCodec.instance);
            derializers.put(Class.forName("java.time.LocalDate"), Jdk8DateCodec.instance);
            derializers.put(Class.forName("java.time.LocalTime"), Jdk8DateCodec.instance);
            derializers.put(Class.forName("java.time.ZonedDateTime"), Jdk8DateCodec.instance);
            derializers.put(Class.forName("java.time.OffsetDateTime"), Jdk8DateCodec.instance);
            derializers.put(Class.forName("java.time.OffsetTime"), Jdk8DateCodec.instance);
            derializers.put(Class.forName("java.time.ZoneOffset"), Jdk8DateCodec.instance);
            derializers.put(Class.forName("java.time.ZoneRegion"), Jdk8DateCodec.instance);
            derializers.put(Class.forName("java.time.ZoneId"), Jdk8DateCodec.instance);
            derializers.put(Class.forName("java.time.Period"), Jdk8DateCodec.instance);
            derializers.put(Class.forName("java.time.Duration"), Jdk8DateCodec.instance);
            derializers.put(Class.forName("java.time.Instant"), Jdk8DateCodec.instance);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (ASMDeserializerFactory asmdeserializerfactory)
        {
            return;
        }
_L4:
        classloader = new ASMDeserializerFactory(classloader);
        asmdeserializerfactory = classloader;
          goto _L5
        classloader;
        asmdeserializerfactory1 = asmdeserializerfactory;
          goto _L2
        classloader;
        asmdeserializerfactory1 = asmdeserializerfactory;
          goto _L2
        classloader;
        asmdeserializerfactory1 = asmdeserializerfactory;
          goto _L2
    }

    public ParserConfig(ClassLoader classloader)
    {
        this(null, classloader);
    }

    public static Field getField(Class class1, String s)
    {
        Field field1 = getField0(class1, s);
        Field field = field1;
        if (field1 == null)
        {
            field = getField0(class1, (new StringBuilder()).append("_").append(s).toString());
        }
        field1 = field;
        if (field == null)
        {
            field1 = getField0(class1, (new StringBuilder()).append("m_").append(s).toString());
        }
        return field1;
    }

    private static Field getField0(Class class1, String s)
    {
        Field afield[] = class1.getDeclaredFields();
        int j = afield.length;
        for (int i = 0; i < j; i++)
        {
            Field field = afield[i];
            if (s.equals(field.getName()))
            {
                return field;
            }
        }

        if (class1.getSuperclass() != null && class1.getSuperclass() != java/lang/Object)
        {
            return getField(class1.getSuperclass(), s);
        } else
        {
            return null;
        }
    }

    public static ParserConfig getGlobalInstance()
    {
        return global;
    }

    public FieldDeserializer createFieldDeserializer(ParserConfig parserconfig, Class class1, FieldInfo fieldinfo)
    {
        boolean flag;
        boolean flag1;
        flag1 = false;
        flag = asmEnable;
        if (!flag) goto _L2; else goto _L1
_L1:
        Object obj = class1;
_L6:
        if (Modifier.isPublic(((Class) (obj)).getModifiers())) goto _L4; else goto _L3
_L3:
        flag = false;
          goto _L2
_L4:
        class2 = ((Class) (obj)).getSuperclass();
        if (class2 == java/lang/Object) goto _L2; else goto _L5
_L5:
        obj = class2;
        if (class2 != null) goto _L6; else goto _L2
_L2:
        if (fieldinfo.getFieldClass() == java/lang/Class)
        {
            flag = false;
        }
        if (flag && asmFactory != null && asmFactory.isExternalClass(class1))
        {
            flag = flag1;
        }
        Class class2;
        if (!flag)
        {
            return createFieldDeserializerWithoutASM(parserconfig, class1, fieldinfo);
        }
        try
        {
            obj = asmFactory.createFieldDeserializer(parserconfig, class1, fieldinfo);
        }
        catch (Throwable throwable)
        {
            return createFieldDeserializerWithoutASM(parserconfig, class1, fieldinfo);
        }
        return ((FieldDeserializer) (obj));
    }

    public FieldDeserializer createFieldDeserializerWithoutASM(ParserConfig parserconfig, Class class1, FieldInfo fieldinfo)
    {
        Class class2 = fieldinfo.getFieldClass();
        if (class2 == Boolean.TYPE || class2 == java/lang/Boolean)
        {
            return new BooleanFieldDeserializer(parserconfig, class1, fieldinfo);
        }
        if (class2 == Integer.TYPE || class2 == java/lang/Integer)
        {
            return new IntegerFieldDeserializer(parserconfig, class1, fieldinfo);
        }
        if (class2 == Long.TYPE || class2 == java/lang/Long)
        {
            return new LongFieldDeserializer(parserconfig, class1, fieldinfo);
        }
        if (class2 == java/lang/String)
        {
            return new StringFieldDeserializer(parserconfig, class1, fieldinfo);
        }
        if (class2 == java/util/List || class2 == java/util/ArrayList)
        {
            return new ArrayListTypeFieldDeserializer(parserconfig, class1, fieldinfo);
        } else
        {
            return new DefaultFieldDeserializer(parserconfig, class1, fieldinfo);
        }
    }

    public ObjectDeserializer createJavaBeanDeserializer(Class class1, Type type)
    {
        boolean flag;
        boolean flag3;
        flag3 = false;
        flag = asmEnable;
        if (!flag) goto _L2; else goto _L1
_L1:
        Object obj = class1;
_L8:
        if (Modifier.isPublic(((Class) (obj)).getModifiers())) goto _L4; else goto _L3
_L3:
        flag = false;
_L2:
        boolean flag1;
        flag1 = flag;
        if (class1.getTypeParameters().length != 0)
        {
            flag1 = false;
        }
        flag = flag1;
        if (flag1)
        {
            flag = flag1;
            if (asmFactory != null)
            {
                flag = flag1;
                if (asmFactory.isExternalClass(class1))
                {
                    flag = false;
                }
            }
        }
        flag1 = flag;
        if (flag)
        {
            flag1 = ASMUtils.checkName(class1.getName());
        }
        flag = flag1;
        if (!flag1) goto _L6; else goto _L5
_L5:
        flag = flag1;
        if (class1.isInterface())
        {
            flag = false;
        }
        obj = DeserializeBeanInfo.computeSetters(class1, type);
        if (((DeserializeBeanInfo) (obj)).getFieldList().size() > 200)
        {
            flag = false;
        }
        flag1 = flag;
        if (((DeserializeBeanInfo) (obj)).getDefaultConstructor() == null)
        {
            flag1 = flag;
            if (!class1.isInterface())
            {
                flag1 = false;
            }
        }
        obj = ((DeserializeBeanInfo) (obj)).getFieldList().iterator();
        flag = flag1;
_L9:
        Object obj2;
        Class class2;
        if (((Iterator) (obj)).hasNext())
        {
            obj2 = (FieldInfo)((Iterator) (obj)).next();
            if (((FieldInfo) (obj2)).isGetOnly())
            {
                flag = false;
            } else
            {
label0:
                {
                    class2 = ((FieldInfo) (obj2)).getFieldClass();
                    if (Modifier.isPublic(class2.getModifiers()))
                    {
                        break label0;
                    }
                    flag = false;
                }
            }
        }
_L6:
        if (flag && class1.isMemberClass() && !Modifier.isStatic(class1.getModifiers()))
        {
            flag = flag3;
        }
        if (!flag)
        {
            return new JavaBeanDeserializer(this, class1, type);
        }
        break MISSING_BLOCK_LABEL_393;
_L4:
        obj2 = ((Class) (obj)).getSuperclass();
        if (obj2 == java/lang/Object) goto _L2; else goto _L7
_L7:
        obj = obj2;
        if (obj2 != null) goto _L8; else goto _L2
        boolean flag2 = flag;
        if (class2.isMemberClass())
        {
            flag2 = flag;
            if (!Modifier.isStatic(class2.getModifiers()))
            {
                flag2 = false;
            }
        }
        if (!ASMUtils.checkName(((FieldInfo) (obj2)).getMember().getName()))
        {
            flag2 = false;
        }
        obj2 = (JSONField)((FieldInfo) (obj2)).getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        ObjectDeserializer objectdeserializer;
        Object obj1;
        if (obj2 != null && !ASMUtils.checkName(((JSONField) (obj2)).name()))
        {
            flag = false;
        } else
        {
            flag = flag2;
        }
          goto _L9
        try
        {
            objectdeserializer = asmFactory.createJavaBeanDeserializer(this, class1, type);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            return new JavaBeanDeserializer(this, class1, type);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            return new JavaBeanDeserializer(this, class1, type);
        }
        // Misplaced declaration of an exception variable
        catch (Type type)
        {
            throw new JSONException((new StringBuilder()).append("create asm deserializer error, ").append(class1.getName()).toString(), type);
        }
        return objectdeserializer;
    }

    public IdentityHashMap getDerializers()
    {
        return derializers;
    }

    public ObjectDeserializer getDeserializer(FieldInfo fieldinfo)
    {
        return getDeserializer(fieldinfo.getFieldClass(), fieldinfo.getFieldType());
    }

    public ObjectDeserializer getDeserializer(Class class1, Type type)
    {
        ObjectDeserializer objectdeserializer = (ObjectDeserializer)derializers.get(type);
        if (objectdeserializer != null)
        {
            type = objectdeserializer;
        } else
        {
            Object obj = type;
            if (type == null)
            {
                obj = class1;
            }
            ObjectDeserializer objectdeserializer1 = (ObjectDeserializer)derializers.get(obj);
            type = objectdeserializer1;
            if (objectdeserializer1 == null)
            {
                type = (JSONType)class1.getAnnotation(com/alibaba/fastjson/annotation/JSONType);
                if (type != null)
                {
                    type = type.mappingTo();
                    if (type != java/lang/Void)
                    {
                        return getDeserializer(((Class) (type)), type);
                    }
                }
                if ((obj instanceof WildcardType) || (obj instanceof TypeVariable) || (obj instanceof ParameterizedType))
                {
                    objectdeserializer1 = (ObjectDeserializer)derializers.get(class1);
                }
                type = objectdeserializer1;
                if (objectdeserializer1 == null)
                {
                    type = Thread.currentThread().getContextClassLoader();
                    try
                    {
                        for (type = ServiceLoader.load(com/alibaba/fastjson/parser/deserializer/AutowiredObjectDeserializer, type).iterator(); type.hasNext();)
                        {
                            AutowiredObjectDeserializer autowiredobjectdeserializer = (AutowiredObjectDeserializer)type.next();
                            Iterator iterator = autowiredobjectdeserializer.getAutowiredFor().iterator();
                            while (iterator.hasNext()) 
                            {
                                Type type1 = (Type)iterator.next();
                                derializers.put(type1, autowiredobjectdeserializer);
                            }
                        }

                    }
                    // Misplaced declaration of an exception variable
                    catch (Type type) { }
                    ObjectDeserializer objectdeserializer2 = (ObjectDeserializer)derializers.get(obj);
                    type = objectdeserializer2;
                    if (objectdeserializer2 == null)
                    {
                        if (class1.isEnum())
                        {
                            class1 = new EnumDeserializer(class1);
                        } else
                        if (class1.isArray())
                        {
                            class1 = ArrayDeserializer.instance;
                        } else
                        if (class1 == java/util/Set || class1 == java/util/HashSet || class1 == java/util/Collection || class1 == java/util/List || class1 == java/util/ArrayList)
                        {
                            class1 = CollectionDeserializer.instance;
                        } else
                        if (java/util/Collection.isAssignableFrom(class1))
                        {
                            class1 = CollectionDeserializer.instance;
                        } else
                        if (java/util/Map.isAssignableFrom(class1))
                        {
                            class1 = MapDeserializer.instance;
                        } else
                        if (java/lang/Throwable.isAssignableFrom(class1))
                        {
                            class1 = new ThrowableDeserializer(this, class1);
                        } else
                        {
                            class1 = createJavaBeanDeserializer(class1, ((Type) (obj)));
                        }
                        putDeserializer(((Type) (obj)), class1);
                        return class1;
                    }
                }
            }
        }
        return type;
    }

    public ObjectDeserializer getDeserializer(Type type)
    {
        ObjectDeserializer objectdeserializer = (ObjectDeserializer)derializers.get(type);
        if (objectdeserializer != null)
        {
            return objectdeserializer;
        }
        if (type instanceof Class)
        {
            return getDeserializer((Class)type, type);
        }
        if (type instanceof ParameterizedType)
        {
            Type type1 = ((ParameterizedType)type).getRawType();
            if (type1 instanceof Class)
            {
                return getDeserializer((Class)type1, type);
            } else
            {
                return getDeserializer(type1);
            }
        } else
        {
            return JavaObjectDeserializer.instance;
        }
    }

    public Map getFieldDeserializers(Class class1)
    {
        class1 = getDeserializer(class1);
        if (class1 instanceof JavaBeanDeserializer)
        {
            return ((JavaBeanDeserializer)class1).getFieldDeserializerMap();
        }
        if (class1 instanceof ASMJavaBeanDeserializer)
        {
            return ((ASMJavaBeanDeserializer)class1).getInnterSerializer().getFieldDeserializerMap();
        } else
        {
            return Collections.emptyMap();
        }
    }

    public SymbolTable getSymbolTable()
    {
        return symbolTable;
    }

    public boolean isAsmEnable()
    {
        return asmEnable;
    }

    public boolean isPrimitive(Class class1)
    {
        return primitiveClasses.contains(class1);
    }

    public void putDeserializer(Type type, ObjectDeserializer objectdeserializer)
    {
        derializers.put(type, objectdeserializer);
    }

    public void setAsmEnable(boolean flag)
    {
        asmEnable = flag;
    }

}
