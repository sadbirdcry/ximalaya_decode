// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.parser.deserializer.ExtraProcessor;
import com.alibaba.fastjson.parser.deserializer.ExtraTypeProvider;
import com.alibaba.fastjson.parser.deserializer.ParseProcess;
import com.alibaba.fastjson.serializer.AfterFilter;
import com.alibaba.fastjson.serializer.BeforeFilter;
import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.LabelFilter;
import com.alibaba.fastjson.serializer.NameFilter;
import com.alibaba.fastjson.serializer.PropertyFilter;
import com.alibaba.fastjson.serializer.PropertyPreFilter;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.alibaba.fastjson.serializer.SerializeWriter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ValueFilter;
import com.alibaba.fastjson.util.FieldInfo;
import com.alibaba.fastjson.util.IOUtils;
import com.alibaba.fastjson.util.ThreadLocalCache;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.alibaba.fastjson:
//            JSONAware, JSONStreamAware, JSONArray, JSONObject, 
//            TypeReference, JSONException

public abstract class JSON
    implements JSONAware, JSONStreamAware
{

    public static int DEFAULT_GENERATE_FEATURE = 0;
    public static int DEFAULT_PARSER_FEATURE = 0;
    public static String DEFAULT_TYPE_KEY = "@type";
    public static String DEFFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static String DUMP_CLASS = null;
    public static final String VERSION = "1.2.7";

    public JSON()
    {
    }

    public static Object parse(String s)
    {
        return parse(s, DEFAULT_PARSER_FEATURE);
    }

    public static Object parse(String s, int i)
    {
        if (s == null)
        {
            return null;
        } else
        {
            s = new DefaultJSONParser(s, ParserConfig.getGlobalInstance(), i);
            Object obj = s.parse();
            s.handleResovleTask(obj);
            s.close();
            return obj;
        }
    }

    public static transient Object parse(String s, Feature afeature[])
    {
        int j = DEFAULT_PARSER_FEATURE;
        int k = afeature.length;
        for (int i = 0; i < k; i++)
        {
            j = Feature.config(j, afeature[i], true);
        }

        return parse(s, j);
    }

    public static Object parse(byte abyte0[], int i, int j, CharsetDecoder charsetdecoder, int k)
    {
        charsetdecoder.reset();
        char ac[] = ThreadLocalCache.getChars((int)((double)j * (double)charsetdecoder.maxCharsPerByte()));
        abyte0 = ByteBuffer.wrap(abyte0, i, j);
        CharBuffer charbuffer = CharBuffer.wrap(ac);
        IOUtils.decode(charsetdecoder, abyte0, charbuffer);
        abyte0 = new DefaultJSONParser(ac, charbuffer.position(), ParserConfig.getGlobalInstance(), k);
        charsetdecoder = ((CharsetDecoder) (abyte0.parse()));
        abyte0.handleResovleTask(charsetdecoder);
        abyte0.close();
        return charsetdecoder;
    }

    public static transient Object parse(byte abyte0[], int i, int j, CharsetDecoder charsetdecoder, Feature afeature[])
    {
        if (abyte0 == null || abyte0.length == 0)
        {
            return null;
        }
        int l = DEFAULT_PARSER_FEATURE;
        int i1 = afeature.length;
        for (int k = 0; k < i1; k++)
        {
            l = Feature.config(l, afeature[k], true);
        }

        return parse(abyte0, i, j, charsetdecoder, l);
    }

    public static transient Object parse(byte abyte0[], Feature afeature[])
    {
        return parse(abyte0, 0, abyte0.length, ThreadLocalCache.getUTF8Decoder(), afeature);
    }

    public static JSONArray parseArray(String s)
    {
        Object obj;
        DefaultJSONParser defaultjsonparser;
        JSONLexer jsonlexer;
        obj = null;
        if (s == null)
        {
            return null;
        }
        defaultjsonparser = new DefaultJSONParser(s, ParserConfig.getGlobalInstance());
        jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() != 8) goto _L2; else goto _L1
_L1:
        jsonlexer.nextToken();
        s = obj;
_L4:
        defaultjsonparser.close();
        return s;
_L2:
        s = obj;
        if (jsonlexer.token() != 20)
        {
            s = new JSONArray();
            defaultjsonparser.parseArray(s);
            defaultjsonparser.handleResovleTask(s);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static List parseArray(String s, Class class1)
    {
        Object obj = null;
        if (s == null)
        {
            return null;
        }
        DefaultJSONParser defaultjsonparser = new DefaultJSONParser(s, ParserConfig.getGlobalInstance());
        s = defaultjsonparser.getLexer();
        if (s.token() == 8)
        {
            s.nextToken();
            s = obj;
        } else
        {
            s = new ArrayList();
            defaultjsonparser.parseArray(class1, s);
            defaultjsonparser.handleResovleTask(s);
        }
        defaultjsonparser.close();
        return s;
    }

    public static List parseArray(String s, Type atype[])
    {
        Object obj = null;
        if (s == null)
        {
            return null;
        }
        DefaultJSONParser defaultjsonparser = new DefaultJSONParser(s, ParserConfig.getGlobalInstance());
        s = ((String) (defaultjsonparser.parseArray(atype)));
        if (s == null)
        {
            s = obj;
        } else
        {
            s = Arrays.asList(s);
        }
        defaultjsonparser.handleResovleTask(s);
        defaultjsonparser.close();
        return s;
    }

    public static JSONObject parseObject(String s)
    {
        s = ((String) (parse(s)));
        if (s instanceof JSONObject)
        {
            return (JSONObject)s;
        } else
        {
            return (JSONObject)toJSON(s);
        }
    }

    public static transient JSONObject parseObject(String s, Feature afeature[])
    {
        return (JSONObject)parse(s, afeature);
    }

    public static transient Object parseObject(String s, TypeReference typereference, Feature afeature[])
    {
        return parseObject(s, typereference.getType(), ParserConfig.getGlobalInstance(), DEFAULT_PARSER_FEATURE, afeature);
    }

    public static Object parseObject(String s, Class class1)
    {
        return parseObject(s, class1, new Feature[0]);
    }

    public static transient Object parseObject(String s, Class class1, ParseProcess parseprocess, Feature afeature[])
    {
        return parseObject(s, ((Type) (class1)), ParserConfig.getGlobalInstance(), parseprocess, DEFAULT_PARSER_FEATURE, afeature);
    }

    public static transient Object parseObject(String s, Class class1, Feature afeature[])
    {
        return parseObject(s, ((Type) (class1)), ParserConfig.getGlobalInstance(), DEFAULT_PARSER_FEATURE, afeature);
    }

    public static transient Object parseObject(String s, Type type, int i, Feature afeature[])
    {
        if (s == null)
        {
            return null;
        }
        int k = afeature.length;
        boolean flag = false;
        int j = i;
        for (i = ((flag) ? 1 : 0); i < k; i++)
        {
            j = Feature.config(j, afeature[i], true);
        }

        s = new DefaultJSONParser(s, ParserConfig.getGlobalInstance(), j);
        type = ((Type) (s.parseObject(type)));
        s.handleResovleTask(type);
        s.close();
        return type;
    }

    public static transient Object parseObject(String s, Type type, ParserConfig parserconfig, int i, Feature afeature[])
    {
        return parseObject(s, type, parserconfig, ((ParseProcess) (null)), i, afeature);
    }

    public static transient Object parseObject(String s, Type type, ParserConfig parserconfig, ParseProcess parseprocess, int i, Feature afeature[])
    {
        if (s == null)
        {
            return null;
        }
        int k = afeature.length;
        boolean flag = false;
        int j = i;
        for (i = ((flag) ? 1 : 0); i < k; i++)
        {
            j = Feature.config(j, afeature[i], true);
        }

        s = new DefaultJSONParser(s, parserconfig, j);
        if (parseprocess instanceof ExtraTypeProvider)
        {
            s.getExtraTypeProviders().add((ExtraTypeProvider)parseprocess);
        }
        if (parseprocess instanceof ExtraProcessor)
        {
            s.getExtraProcessors().add((ExtraProcessor)parseprocess);
        }
        type = ((Type) (s.parseObject(type)));
        s.handleResovleTask(type);
        s.close();
        return type;
    }

    public static transient Object parseObject(String s, Type type, ParseProcess parseprocess, Feature afeature[])
    {
        return parseObject(s, type, ParserConfig.getGlobalInstance(), DEFAULT_PARSER_FEATURE, afeature);
    }

    public static transient Object parseObject(String s, Type type, Feature afeature[])
    {
        return parseObject(s, type, ParserConfig.getGlobalInstance(), DEFAULT_PARSER_FEATURE, afeature);
    }

    public static transient Object parseObject(byte abyte0[], int i, int j, CharsetDecoder charsetdecoder, Type type, Feature afeature[])
    {
        charsetdecoder.reset();
        char ac[] = ThreadLocalCache.getChars((int)((double)j * (double)charsetdecoder.maxCharsPerByte()));
        abyte0 = ByteBuffer.wrap(abyte0, i, j);
        CharBuffer charbuffer = CharBuffer.wrap(ac);
        IOUtils.decode(charsetdecoder, abyte0, charbuffer);
        return parseObject(ac, charbuffer.position(), type, afeature);
    }

    public static transient Object parseObject(byte abyte0[], Type type, Feature afeature[])
    {
        return parseObject(abyte0, 0, abyte0.length, ThreadLocalCache.getUTF8Decoder(), type, afeature);
    }

    public static transient Object parseObject(char ac[], int i, Type type, Feature afeature[])
    {
        if (ac == null || ac.length == 0)
        {
            return null;
        }
        int k = DEFAULT_PARSER_FEATURE;
        int l = afeature.length;
        for (int j = 0; j < l; j++)
        {
            k = Feature.config(k, afeature[j], true);
        }

        ac = new DefaultJSONParser(ac, i, ParserConfig.getGlobalInstance(), k);
        type = ((Type) (ac.parseObject(type)));
        ac.handleResovleTask(type);
        ac.close();
        return type;
    }

    private static void setFilter(JSONSerializer jsonserializer, SerializeFilter serializefilter)
    {
        if (serializefilter != null)
        {
            if (serializefilter instanceof PropertyPreFilter)
            {
                jsonserializer.getPropertyPreFilters().add((PropertyPreFilter)serializefilter);
            }
            if (serializefilter instanceof NameFilter)
            {
                jsonserializer.getNameFilters().add((NameFilter)serializefilter);
            }
            if (serializefilter instanceof ValueFilter)
            {
                jsonserializer.getValueFilters().add((ValueFilter)serializefilter);
            }
            if (serializefilter instanceof PropertyFilter)
            {
                jsonserializer.getPropertyFilters().add((PropertyFilter)serializefilter);
            }
            if (serializefilter instanceof BeforeFilter)
            {
                jsonserializer.getBeforeFilters().add((BeforeFilter)serializefilter);
            }
            if (serializefilter instanceof AfterFilter)
            {
                jsonserializer.getAfterFilters().add((AfterFilter)serializefilter);
            }
            if (serializefilter instanceof LabelFilter)
            {
                jsonserializer.getLabelFilters().add((LabelFilter)serializefilter);
                return;
            }
        }
    }

    private static transient void setFilter(JSONSerializer jsonserializer, SerializeFilter aserializefilter[])
    {
        int j = aserializefilter.length;
        for (int i = 0; i < j; i++)
        {
            setFilter(jsonserializer, aserializefilter[i]);
        }

    }

    public static Object toJSON(Object obj)
    {
        return toJSON(obj, ParserConfig.getGlobalInstance());
    }

    public static Object toJSON(Object obj, ParserConfig parserconfig)
    {
        Object obj1;
        if (obj == null)
        {
            obj1 = null;
        } else
        {
            obj1 = obj;
            if (!(obj instanceof JSON))
            {
                if (obj instanceof Map)
                {
                    parserconfig = (Map)obj;
                    obj = new JSONObject(parserconfig.size());
                    for (parserconfig = parserconfig.entrySet().iterator(); parserconfig.hasNext(); ((JSONObject) (obj)).put(TypeUtils.castToString(((java.util.Map.Entry) (obj1)).getKey()), toJSON(((java.util.Map.Entry) (obj1)).getValue())))
                    {
                        obj1 = (java.util.Map.Entry)parserconfig.next();
                    }

                    return obj;
                }
                if (obj instanceof Collection)
                {
                    parserconfig = (Collection)obj;
                    obj = new JSONArray(parserconfig.size());
                    for (parserconfig = parserconfig.iterator(); parserconfig.hasNext(); ((JSONArray) (obj)).add(toJSON(parserconfig.next()))) { }
                    return obj;
                }
                Class class1 = obj.getClass();
                if (class1.isEnum())
                {
                    return ((Enum)obj).name();
                }
                if (class1.isArray())
                {
                    int j = Array.getLength(obj);
                    parserconfig = new JSONArray(j);
                    for (int i = 0; i < j; i++)
                    {
                        parserconfig.add(toJSON(Array.get(obj, i)));
                    }

                    return parserconfig;
                }
                obj1 = obj;
                if (!parserconfig.isPrimitive(class1))
                {
                    try
                    {
                        Object obj2 = TypeUtils.computeGetters(class1, null);
                        parserconfig = new JSONObject(((List) (obj2)).size());
                        FieldInfo fieldinfo;
                        Object obj3;
                        for (obj2 = ((List) (obj2)).iterator(); ((Iterator) (obj2)).hasNext(); parserconfig.put(fieldinfo.getName(), obj3))
                        {
                            fieldinfo = (FieldInfo)((Iterator) (obj2)).next();
                            obj3 = toJSON(fieldinfo.get(obj));
                        }

                    }
                    // Misplaced declaration of an exception variable
                    catch (Object obj)
                    {
                        throw new JSONException("toJSON error", ((Throwable) (obj)));
                    }
                    // Misplaced declaration of an exception variable
                    catch (Object obj)
                    {
                        throw new JSONException("toJSON error", ((Throwable) (obj)));
                    }
                    return parserconfig;
                }
            }
        }
        return obj1;
    }

    public static transient byte[] toJSONBytes(Object obj, SerializeConfig serializeconfig, SerializerFeature aserializerfeature[])
    {
        SerializeWriter serializewriter = new SerializeWriter();
        int j;
        serializeconfig = new JSONSerializer(serializewriter, serializeconfig);
        j = aserializerfeature.length;
        int i = 0;
_L2:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        serializeconfig.config(aserializerfeature[i], true);
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        serializeconfig.write(obj);
        obj = serializewriter.toBytes("UTF-8");
        serializewriter.close();
        return ((byte []) (obj));
        obj;
        serializewriter.close();
        throw obj;
    }

    public static transient byte[] toJSONBytes(Object obj, SerializerFeature aserializerfeature[])
    {
        SerializeWriter serializewriter = new SerializeWriter();
        JSONSerializer jsonserializer;
        int j;
        jsonserializer = new JSONSerializer(serializewriter);
        j = aserializerfeature.length;
        int i = 0;
_L2:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        jsonserializer.config(aserializerfeature[i], true);
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        jsonserializer.write(obj);
        obj = serializewriter.toBytes("UTF-8");
        serializewriter.close();
        return ((byte []) (obj));
        obj;
        serializewriter.close();
        throw obj;
    }

    public static String toJSONString(Object obj)
    {
        return toJSONString(obj, new SerializerFeature[0]);
    }

    public static transient String toJSONString(Object obj, SerializeConfig serializeconfig, SerializeFilter serializefilter, SerializerFeature aserializerfeature[])
    {
        SerializeWriter serializewriter = new SerializeWriter();
        int j;
        serializeconfig = new JSONSerializer(serializewriter, serializeconfig);
        j = aserializerfeature.length;
        int i = 0;
_L2:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        serializeconfig.config(aserializerfeature[i], true);
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        setFilter(serializeconfig, serializefilter);
        serializeconfig.write(obj);
        obj = serializewriter.toString();
        serializewriter.close();
        return ((String) (obj));
        obj;
        serializewriter.close();
        throw obj;
    }

    public static transient String toJSONString(Object obj, SerializeConfig serializeconfig, SerializeFilter aserializefilter[], SerializerFeature aserializerfeature[])
    {
        SerializeWriter serializewriter = new SerializeWriter();
        int j;
        serializeconfig = new JSONSerializer(serializewriter, serializeconfig);
        j = aserializerfeature.length;
        int i = 0;
_L2:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        serializeconfig.config(aserializerfeature[i], true);
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        setFilter(serializeconfig, aserializefilter);
        serializeconfig.write(obj);
        obj = serializewriter.toString();
        serializewriter.close();
        return ((String) (obj));
        obj;
        serializewriter.close();
        throw obj;
    }

    public static transient String toJSONString(Object obj, SerializeConfig serializeconfig, SerializerFeature aserializerfeature[])
    {
        return toJSONString(obj, serializeconfig, (SerializeFilter)null, aserializerfeature);
    }

    public static transient String toJSONString(Object obj, SerializeFilter serializefilter, SerializerFeature aserializerfeature[])
    {
        SerializeWriter serializewriter = new SerializeWriter();
        JSONSerializer jsonserializer;
        int j;
        jsonserializer = new JSONSerializer(serializewriter);
        j = aserializerfeature.length;
        int i = 0;
_L2:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        jsonserializer.config(aserializerfeature[i], true);
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        jsonserializer.config(SerializerFeature.WriteDateUseDateFormat, true);
        setFilter(jsonserializer, serializefilter);
        jsonserializer.write(obj);
        obj = serializewriter.toString();
        serializewriter.close();
        return ((String) (obj));
        obj;
        serializewriter.close();
        throw obj;
    }

    public static String toJSONString(Object obj, boolean flag)
    {
        if (!flag)
        {
            return toJSONString(obj);
        } else
        {
            return toJSONString(obj, new SerializerFeature[] {
                SerializerFeature.PrettyFormat
            });
        }
    }

    public static transient String toJSONString(Object obj, SerializeFilter aserializefilter[], SerializerFeature aserializerfeature[])
    {
        SerializeWriter serializewriter = new SerializeWriter();
        JSONSerializer jsonserializer;
        int j;
        jsonserializer = new JSONSerializer(serializewriter);
        j = aserializerfeature.length;
        int i = 0;
_L2:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        jsonserializer.config(aserializerfeature[i], true);
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        jsonserializer.config(SerializerFeature.WriteDateUseDateFormat, true);
        setFilter(jsonserializer, aserializefilter);
        jsonserializer.write(obj);
        obj = serializewriter.toString();
        serializewriter.close();
        return ((String) (obj));
        obj;
        serializewriter.close();
        throw obj;
    }

    public static transient String toJSONString(Object obj, SerializerFeature aserializerfeature[])
    {
        SerializeWriter serializewriter = new SerializeWriter();
        JSONSerializer jsonserializer;
        int j;
        jsonserializer = new JSONSerializer(serializewriter);
        j = aserializerfeature.length;
        int i = 0;
_L2:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        jsonserializer.config(aserializerfeature[i], true);
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        jsonserializer.write(obj);
        obj = serializewriter.toString();
        serializewriter.close();
        return ((String) (obj));
        obj;
        serializewriter.close();
        throw obj;
    }

    public static transient String toJSONStringWithDateFormat(Object obj, String s, SerializerFeature aserializerfeature[])
    {
        SerializeWriter serializewriter = new SerializeWriter();
        JSONSerializer jsonserializer;
        int j;
        jsonserializer = new JSONSerializer(serializewriter);
        j = aserializerfeature.length;
        int i = 0;
_L2:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        jsonserializer.config(aserializerfeature[i], true);
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        jsonserializer.config(SerializerFeature.WriteDateUseDateFormat, true);
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_70;
        }
        jsonserializer.setDateFormat(s);
        jsonserializer.write(obj);
        obj = serializewriter.toString();
        serializewriter.close();
        return ((String) (obj));
        obj;
        serializewriter.close();
        throw obj;
    }

    public static transient String toJSONStringZ(Object obj, SerializeConfig serializeconfig, SerializerFeature aserializerfeature[])
    {
        aserializerfeature = new SerializeWriter(aserializerfeature);
        (new JSONSerializer(aserializerfeature, serializeconfig)).write(obj);
        obj = aserializerfeature.toString();
        aserializerfeature.close();
        return ((String) (obj));
        obj;
        aserializerfeature.close();
        throw obj;
    }

    public static Object toJavaObject(JSON json, Class class1)
    {
        return TypeUtils.cast(json, class1, ParserConfig.getGlobalInstance());
    }

    public static transient void writeJSONStringTo(Object obj, Writer writer, SerializerFeature aserializerfeature[])
    {
        writer = new SerializeWriter(writer);
        JSONSerializer jsonserializer;
        int j;
        jsonserializer = new JSONSerializer(writer);
        j = aserializerfeature.length;
        int i = 0;
_L2:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        jsonserializer.config(aserializerfeature[i], true);
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        jsonserializer.write(obj);
        writer.close();
        return;
        obj;
        writer.close();
        throw obj;
    }

    public String toJSONString()
    {
        SerializeWriter serializewriter = new SerializeWriter();
        String s;
        (new JSONSerializer(serializewriter)).write(this);
        s = serializewriter.toString();
        serializewriter.close();
        return s;
        Exception exception;
        exception;
        serializewriter.close();
        throw exception;
    }

    public String toString()
    {
        return toJSONString();
    }

    public void writeJSONString(Appendable appendable)
    {
        SerializeWriter serializewriter = new SerializeWriter();
        (new JSONSerializer(serializewriter)).write(this);
        appendable.append(serializewriter.toString());
        serializewriter.close();
        return;
        appendable;
        throw new JSONException(appendable.getMessage(), appendable);
        appendable;
        serializewriter.close();
        throw appendable;
    }

    static 
    {
        DEFAULT_PARSER_FEATURE = Feature.AutoCloseSource.getMask() | 0 | Feature.InternFieldNames.getMask() | Feature.UseBigDecimal.getMask() | Feature.AllowUnQuotedFieldNames.getMask() | Feature.AllowSingleQuotes.getMask() | Feature.AllowArbitraryCommas.getMask() | Feature.SortFeidFastMatch.getMask() | Feature.IgnoreNotMatch.getMask();
        DEFAULT_GENERATE_FEATURE = SerializerFeature.QuoteFieldNames.getMask() | 0 | SerializerFeature.SkipTransientField.getMask() | SerializerFeature.WriteEnumUsingName.getMask() | SerializerFeature.SortField.getMask();
    }
}
