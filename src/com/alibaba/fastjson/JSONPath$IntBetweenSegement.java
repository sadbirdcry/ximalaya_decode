// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;


// Referenced classes of package com.alibaba.fastjson:
//            JSONPath

static class not
    implements not
{

    private final long endValue;
    private final boolean not;
    private final String propertyName;
    private final long startValue;

    public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
    {
        jsonpath = ((JSONPath) (jsonpath.getPropertyValue(obj2, propertyName, false)));
        if (jsonpath == null)
        {
            return false;
        }
        if (jsonpath instanceof Number)
        {
            long l = ((Number)jsonpath).longValue();
            if (l >= startValue && l <= endValue)
            {
                boolean flag;
                if (!not)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                return flag;
            }
        }
        return not;
    }

    public (String s, long l, long l1, boolean flag)
    {
        propertyName = s;
        startValue = l;
        endValue = l1;
        not = flag;
    }
}
