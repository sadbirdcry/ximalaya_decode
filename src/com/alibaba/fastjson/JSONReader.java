// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.JSONReaderScanner;
import com.alibaba.fastjson.util.IOUtils;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.Closeable;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson:
//            JSONStreamContext, JSONException, TypeReference

public class JSONReader
    implements Closeable
{

    private JSONStreamContext context;
    private final DefaultJSONParser parser;

    public JSONReader(DefaultJSONParser defaultjsonparser)
    {
        parser = defaultjsonparser;
    }

    public JSONReader(JSONLexer jsonlexer)
    {
        this(new DefaultJSONParser(jsonlexer));
    }

    public JSONReader(Reader reader)
    {
        this(((JSONLexer) (new JSONReaderScanner(reader))));
    }

    private void endStructure()
    {
        context = context.getParent();
        if (context != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        context.getState();
        JVM INSTR tableswitch 1001 1004: default 56
    //                   1001 86
    //                   1002 72
    //                   1003 86
    //                   1004 79;
           goto _L3 _L4 _L5 _L4 _L6
_L4:
        break MISSING_BLOCK_LABEL_86;
_L5:
        break; /* Loop/switch isn't completed */
_L3:
        char c = '\uFFFF';
_L8:
        if (c != -1)
        {
            context.setState(c);
            return;
        }
        if (true) goto _L1; else goto _L7
_L7:
        c = '\u03EB';
          goto _L8
_L6:
        c = '\u03ED';
          goto _L8
        c = '\u03EA';
          goto _L8
    }

    private void readAfter()
    {
        char c;
        int i;
        c = '\u03EA';
        i = context.getState();
        i;
        JVM INSTR tableswitch 1001 1005: default 48
    //                   1001 79
    //                   1002 75
    //                   1003 79
    //                   1004 98
    //                   1005 93;
           goto _L1 _L2 _L3 _L2 _L4 _L5
_L2:
        break; /* Loop/switch isn't completed */
_L1:
        throw new JSONException((new StringBuilder()).append("illegal state : ").append(i).toString());
_L3:
        c = '\u03EB';
_L7:
        if (c != '\uFFFF')
        {
            context.setState(c);
        }
        return;
_L5:
        c = '\uFFFF';
        continue; /* Loop/switch isn't completed */
_L4:
        c = '\u03ED';
        if (true) goto _L7; else goto _L6
_L6:
    }

    private void readBefore()
    {
        int i = context.getState();
        switch (i)
        {
        default:
            throw new JSONException((new StringBuilder()).append("illegal state : ").append(i).toString());

        case 1002: 
            parser.accept(17);
            // fall through

        case 1001: 
        case 1004: 
            return;

        case 1003: 
            parser.accept(16, 18);
            return;

        case 1005: 
            parser.accept(16);
            break;
        }
    }

    private void startStructure()
    {
        switch (context.getState())
        {
        default:
            throw new JSONException((new StringBuilder()).append("illegal state : ").append(context.getState()).toString());

        case 1002: 
            parser.accept(17);
            // fall through

        case 1001: 
        case 1004: 
            return;

        case 1003: 
        case 1005: 
            parser.accept(16);
            break;
        }
    }

    public void close()
    {
        IOUtils.close(parser);
    }

    public void config(Feature feature, boolean flag)
    {
        parser.config(feature, flag);
    }

    public void endArray()
    {
        parser.accept(15);
        endStructure();
    }

    public void endObject()
    {
        parser.accept(13);
        endStructure();
    }

    public boolean hasNext()
    {
        int i;
        int j;
        if (context == null)
        {
            throw new JSONException("context is null");
        }
        i = parser.getLexer().token();
        j = context.getState();
        j;
        JVM INSTR tableswitch 1001 1005: default 72
    //                   1001 109
    //                   1002 72
    //                   1003 109
    //                   1004 99
    //                   1005 99;
           goto _L1 _L2 _L1 _L2 _L3 _L3
_L1:
        throw new JSONException((new StringBuilder()).append("illegal state : ").append(j).toString());
_L3:
        if (i == 15) goto _L5; else goto _L4
_L4:
        return true;
_L5:
        return false;
_L2:
        if (i == 13)
        {
            return false;
        }
        if (true) goto _L4; else goto _L6
_L6:
    }

    public Integer readInteger()
    {
        Object obj;
        if (context == null)
        {
            obj = parser.parse();
        } else
        {
            readBefore();
            obj = parser.parse();
            readAfter();
        }
        return TypeUtils.castToInt(obj);
    }

    public Long readLong()
    {
        Object obj;
        if (context == null)
        {
            obj = parser.parse();
        } else
        {
            readBefore();
            obj = parser.parse();
            readAfter();
        }
        return TypeUtils.castToLong(obj);
    }

    public Object readObject()
    {
        if (context == null)
        {
            return parser.parse();
        }
        readBefore();
        context.getState();
        JVM INSTR tableswitch 1001 1003: default 52
    //                   1001 66
    //                   1002 52
    //                   1003 66;
           goto _L1 _L2 _L1 _L2
_L1:
        Object obj = parser.parse();
_L4:
        readAfter();
        return obj;
_L2:
        obj = parser.parseKey();
        if (true) goto _L4; else goto _L3
_L3:
    }

    public Object readObject(TypeReference typereference)
    {
        return readObject(typereference.getType());
    }

    public Object readObject(Class class1)
    {
        if (context == null)
        {
            return parser.parseObject(class1);
        } else
        {
            readBefore();
            class1 = ((Class) (parser.parseObject(class1)));
            readAfter();
            return class1;
        }
    }

    public Object readObject(Type type)
    {
        if (context == null)
        {
            return parser.parseObject(type);
        } else
        {
            readBefore();
            type = ((Type) (parser.parseObject(type)));
            readAfter();
            return type;
        }
    }

    public Object readObject(Map map)
    {
        if (context == null)
        {
            return parser.parseObject(map);
        } else
        {
            readBefore();
            map = ((Map) (parser.parseObject(map)));
            readAfter();
            return map;
        }
    }

    public void readObject(Object obj)
    {
        if (context == null)
        {
            parser.parseObject(obj);
            return;
        } else
        {
            readBefore();
            parser.parseObject(obj);
            readAfter();
            return;
        }
    }

    public String readString()
    {
        Object obj;
        if (context == null)
        {
            obj = parser.parse();
        } else
        {
            readBefore();
            obj = parser.parse();
            readAfter();
        }
        return TypeUtils.castToString(obj);
    }

    public void startArray()
    {
        if (context == null)
        {
            context = new JSONStreamContext(null, 1004);
        } else
        {
            startStructure();
            context = new JSONStreamContext(context, 1004);
        }
        parser.accept(14);
    }

    public void startObject()
    {
        if (context == null)
        {
            context = new JSONStreamContext(null, 1001);
        } else
        {
            startStructure();
            context = new JSONStreamContext(context, 1001);
        }
        parser.accept(12, 18);
    }
}
