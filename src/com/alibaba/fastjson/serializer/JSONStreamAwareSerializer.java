// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSONStreamAware;
import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer

public class JSONStreamAwareSerializer
    implements ObjectSerializer
{

    public static JSONStreamAwareSerializer instance = new JSONStreamAwareSerializer();

    public JSONStreamAwareSerializer()
    {
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer = jsonserializer.getWriter();
        ((JSONStreamAware)obj).writeJSONString(jsonserializer);
    }

}
