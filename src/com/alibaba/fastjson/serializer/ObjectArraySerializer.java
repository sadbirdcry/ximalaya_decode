// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class ObjectArraySerializer
    implements ObjectSerializer
{

    public static final ObjectArraySerializer instance = new ObjectArraySerializer();

    public ObjectArraySerializer()
    {
    }

    public final void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        SerialContext serialcontext;
        SerializeWriter serializewriter;
        Object aobj[];
        int j;
        int k;
        serializewriter = jsonserializer.getWriter();
        aobj = (Object[])(Object[])obj;
        if (obj == null)
        {
            if (serializewriter.isEnabled(SerializerFeature.WriteNullListAsEmpty))
            {
                serializewriter.write("[]");
                return;
            } else
            {
                serializewriter.writeNull();
                return;
            }
        }
        j = aobj.length;
        k = j - 1;
        if (k == -1)
        {
            serializewriter.append("[]");
            return;
        }
        serialcontext = jsonserializer.getContext();
        jsonserializer.setContext(serialcontext, obj, obj1, 0);
        obj = null;
        serializewriter.append('[');
        if (!serializewriter.isEnabled(SerializerFeature.PrettyFormat))
        {
            break MISSING_BLOCK_LABEL_180;
        }
        jsonserializer.incrementIndent();
        jsonserializer.println();
        i = 0;
_L2:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        if (i == 0)
        {
            break MISSING_BLOCK_LABEL_140;
        }
        serializewriter.write(',');
        jsonserializer.println();
        jsonserializer.write(aobj[i]);
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        jsonserializer.decrementIdent();
        jsonserializer.println();
        serializewriter.write(']');
        jsonserializer.setContext(serialcontext);
        return;
        i = 0;
        obj1 = null;
_L13:
        if (i >= k) goto _L4; else goto _L3
_L3:
        Object obj2;
        obj2 = aobj[i];
        if (obj2 != null)
        {
            break MISSING_BLOCK_LABEL_215;
        }
        serializewriter.append("null,");
        break MISSING_BLOCK_LABEL_365;
        if (!jsonserializer.containsReference(obj2)) goto _L6; else goto _L5
_L5:
        jsonserializer.writeReference(obj2);
_L7:
        serializewriter.append(',');
        break MISSING_BLOCK_LABEL_365;
        obj;
        jsonserializer.setContext(serialcontext);
        throw obj;
_L6:
        type = obj2.getClass();
        if (type != obj)
        {
            break MISSING_BLOCK_LABEL_278;
        }
        ((ObjectSerializer) (obj1)).write(jsonserializer, obj2, null, null, 0);
        continue; /* Loop/switch isn't completed */
        obj1 = jsonserializer.getObjectWriter(type);
        ((ObjectSerializer) (obj1)).write(jsonserializer, obj2, null, null, 0);
        obj = type;
        if (true) goto _L7; else goto _L4
_L4:
        obj = aobj[k];
        if (obj != null) goto _L9; else goto _L8
_L8:
        serializewriter.append("null]");
_L10:
        jsonserializer.setContext(serialcontext);
        return;
_L9:
        if (!jsonserializer.containsReference(obj))
        {
            break MISSING_BLOCK_LABEL_352;
        }
        jsonserializer.writeReference(obj);
_L11:
        serializewriter.append(']');
          goto _L10
        jsonserializer.writeWithFieldName(obj, Integer.valueOf(k));
          goto _L11
        i++;
        if (true) goto _L13; else goto _L12
_L12:
    }

}
