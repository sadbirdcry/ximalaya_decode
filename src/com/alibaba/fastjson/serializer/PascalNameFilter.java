// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;


// Referenced classes of package com.alibaba.fastjson.serializer:
//            NameFilter

public class PascalNameFilter
    implements NameFilter
{

    public PascalNameFilter()
    {
    }

    public String process(Object obj, String s, Object obj1)
    {
        if (s == null || s.length() == 0)
        {
            return s;
        } else
        {
            obj = s.toCharArray();
            obj[0] = Character.toUpperCase(obj[0]);
            return new String(((char []) (obj)));
        }
    }
}
