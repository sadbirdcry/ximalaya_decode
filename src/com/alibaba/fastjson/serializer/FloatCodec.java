// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class FloatCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static FloatCodec instance = new FloatCodec();

    public FloatCodec()
    {
    }

    public static Object deserialze(DefaultJSONParser defaultjsonparser)
    {
        JSONLexer jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() == 2)
        {
            defaultjsonparser = jsonlexer.numberString();
            jsonlexer.nextToken(16);
            return Float.valueOf(Float.parseFloat(defaultjsonparser));
        }
        if (jsonlexer.token() == 3)
        {
            float f = jsonlexer.floatValue();
            jsonlexer.nextToken(16);
            return Float.valueOf(f);
        }
        defaultjsonparser = ((DefaultJSONParser) (defaultjsonparser.parse()));
        if (defaultjsonparser == null)
        {
            return null;
        } else
        {
            return TypeUtils.castToFloat(defaultjsonparser);
        }
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        return deserialze(defaultjsonparser);
    }

    public int getFastMatchToken()
    {
        return 2;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        type = jsonserializer.getWriter();
        if (obj != null) goto _L2; else goto _L1
_L1:
        if (!jsonserializer.isEnabled(SerializerFeature.WriteNullNumberAsZero)) goto _L4; else goto _L3
_L3:
        type.write('0');
_L6:
        return;
_L4:
        type.writeNull();
        return;
_L2:
        float f = ((Float)obj).floatValue();
        if (Float.isNaN(f))
        {
            type.writeNull();
            return;
        }
        if (Float.isInfinite(f))
        {
            type.writeNull();
            return;
        }
        obj1 = Float.toString(f);
        obj = obj1;
        if (((String) (obj1)).endsWith(".0"))
        {
            obj = ((String) (obj1)).substring(0, ((String) (obj1)).length() - 2);
        }
        type.write(((String) (obj)));
        if (jsonserializer.isEnabled(SerializerFeature.WriteClassName))
        {
            type.write('F');
            return;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

}
