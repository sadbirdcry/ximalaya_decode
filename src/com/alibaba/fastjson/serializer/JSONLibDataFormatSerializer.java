// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSONObject;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter

public class JSONLibDataFormatSerializer
    implements ObjectSerializer
{

    public JSONLibDataFormatSerializer()
    {
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        if (obj == null)
        {
            jsonserializer.getWriter().writeNull();
            return;
        } else
        {
            obj = (Date)obj;
            obj1 = new JSONObject();
            ((JSONObject) (obj1)).put("date", Integer.valueOf(((Date) (obj)).getDate()));
            ((JSONObject) (obj1)).put("day", Integer.valueOf(((Date) (obj)).getDay()));
            ((JSONObject) (obj1)).put("hours", Integer.valueOf(((Date) (obj)).getHours()));
            ((JSONObject) (obj1)).put("minutes", Integer.valueOf(((Date) (obj)).getMinutes()));
            ((JSONObject) (obj1)).put("month", Integer.valueOf(((Date) (obj)).getMonth()));
            ((JSONObject) (obj1)).put("seconds", Integer.valueOf(((Date) (obj)).getSeconds()));
            ((JSONObject) (obj1)).put("time", Long.valueOf(((Date) (obj)).getTime()));
            ((JSONObject) (obj1)).put("timezoneOffset", Integer.valueOf(((Date) (obj)).getTimezoneOffset()));
            ((JSONObject) (obj1)).put("year", Integer.valueOf(((Date) (obj)).getYear()));
            jsonserializer.write(obj1);
            return;
        }
    }
}
