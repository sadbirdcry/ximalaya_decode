// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class BigDecimalCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final BigDecimalCodec instance = new BigDecimalCodec();

    public BigDecimalCodec()
    {
    }

    public static Object deserialze(DefaultJSONParser defaultjsonparser)
    {
        JSONLexer jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() == 2)
        {
            long l = jsonlexer.longValue();
            jsonlexer.nextToken(16);
            return new BigDecimal(l);
        }
        if (jsonlexer.token() == 3)
        {
            defaultjsonparser = jsonlexer.decimalValue();
            jsonlexer.nextToken(16);
            return defaultjsonparser;
        }
        defaultjsonparser = ((DefaultJSONParser) (defaultjsonparser.parse()));
        if (defaultjsonparser == null)
        {
            return null;
        } else
        {
            return TypeUtils.castToBigDecimal(defaultjsonparser);
        }
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        return deserialze(defaultjsonparser);
    }

    public int getFastMatchToken()
    {
        return 2;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer = jsonserializer.getWriter();
        if (obj != null) goto _L2; else goto _L1
_L1:
        if (!jsonserializer.isEnabled(SerializerFeature.WriteNullNumberAsZero)) goto _L4; else goto _L3
_L3:
        jsonserializer.write('0');
_L6:
        return;
_L4:
        jsonserializer.writeNull();
        return;
_L2:
        obj = (BigDecimal)obj;
        jsonserializer.write(((BigDecimal) (obj)).toString());
        if (jsonserializer.isEnabled(SerializerFeature.WriteClassName) && type != java/math/BigDecimal && ((BigDecimal) (obj)).scale() == 0)
        {
            jsonserializer.write('.');
            return;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

}
