// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.UnknownHostException;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer

public class InetAddressCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static InetAddressCodec instance = new InetAddressCodec();

    public InetAddressCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        defaultjsonparser = (String)defaultjsonparser.parse();
        if (defaultjsonparser == null)
        {
            return null;
        }
        if (defaultjsonparser.length() == 0)
        {
            return null;
        }
        try
        {
            defaultjsonparser = InetAddress.getByName(defaultjsonparser);
        }
        // Misplaced declaration of an exception variable
        catch (DefaultJSONParser defaultjsonparser)
        {
            throw new JSONException("deserialize error", defaultjsonparser);
        }
        return defaultjsonparser;
    }

    public int getFastMatchToken()
    {
        return 4;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        if (obj == null)
        {
            jsonserializer.writeNull();
            return;
        } else
        {
            jsonserializer.write(((InetAddress)obj).getHostAddress());
            return;
        }
    }

}
