// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter

public class SimpleDateFormatSerializer
    implements ObjectSerializer
{

    private final String pattern;

    public SimpleDateFormatSerializer(String s)
    {
        pattern = s;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        if (obj == null)
        {
            jsonserializer.getWriter().writeNull();
            return;
        } else
        {
            obj = (Date)obj;
            jsonserializer.write((new SimpleDateFormat(pattern)).format(((Date) (obj))));
            return;
        }
    }
}
