// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicInteger;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class IntegerCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static IntegerCodec instance = new IntegerCodec();

    public IntegerCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        obj = defaultjsonparser.getLexer();
        if (((JSONLexer) (obj)).token() == 8)
        {
            ((JSONLexer) (obj)).nextToken(16);
            obj = null;
        } else
        {
            if (((JSONLexer) (obj)).token() == 2)
            {
                int i = ((JSONLexer) (obj)).intValue();
                ((JSONLexer) (obj)).nextToken(16);
                defaultjsonparser = Integer.valueOf(i);
            } else
            if (((JSONLexer) (obj)).token() == 3)
            {
                defaultjsonparser = ((JSONLexer) (obj)).decimalValue();
                ((JSONLexer) (obj)).nextToken(16);
                defaultjsonparser = Integer.valueOf(defaultjsonparser.intValue());
            } else
            {
                defaultjsonparser = TypeUtils.castToInt(defaultjsonparser.parse());
            }
            obj = defaultjsonparser;
            if (type == java/util/concurrent/atomic/AtomicInteger)
            {
                return new AtomicInteger(defaultjsonparser.intValue());
            }
        }
        return obj;
    }

    public int getFastMatchToken()
    {
        return 2;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        obj1 = jsonserializer.getWriter();
        obj = (Number)obj;
        if (obj != null) goto _L2; else goto _L1
_L1:
        if (!((SerializeWriter) (obj1)).isEnabled(SerializerFeature.WriteNullNumberAsZero)) goto _L4; else goto _L3
_L3:
        ((SerializeWriter) (obj1)).write('0');
_L6:
        return;
_L4:
        ((SerializeWriter) (obj1)).writeNull();
        return;
_L2:
        ((SerializeWriter) (obj1)).writeInt(((Number) (obj)).intValue());
        if (jsonserializer.isEnabled(SerializerFeature.WriteClassName))
        {
            jsonserializer = obj.getClass();
            if (jsonserializer == java/lang/Byte)
            {
                ((SerializeWriter) (obj1)).write('B');
                return;
            }
            if (jsonserializer == java/lang/Short)
            {
                ((SerializeWriter) (obj1)).write('S');
                return;
            }
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

}
