// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicLongArray;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class AtomicLongArrayCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final AtomicLongArrayCodec instance = new AtomicLongArrayCodec();

    public AtomicLongArrayCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        if (defaultjsonparser.getLexer().token() == 8)
        {
            defaultjsonparser.getLexer().nextToken(16);
            return null;
        }
        type = new JSONArray();
        defaultjsonparser.parseArray(type);
        defaultjsonparser = new AtomicLongArray(type.size());
        for (int i = 0; i < type.size(); i++)
        {
            defaultjsonparser.set(i, type.getLong(i).longValue());
        }

        return defaultjsonparser;
    }

    public int getFastMatchToken()
    {
        return 14;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer = jsonserializer.getWriter();
        if (obj == null)
        {
            if (jsonserializer.isEnabled(SerializerFeature.WriteNullListAsEmpty))
            {
                jsonserializer.write("[]");
                return;
            } else
            {
                jsonserializer.writeNull();
                return;
            }
        }
        obj = (AtomicLongArray)obj;
        int j = ((AtomicLongArray) (obj)).length();
        jsonserializer.append('[');
        for (i = 0; i < j; i++)
        {
            long l = ((AtomicLongArray) (obj)).get(i);
            if (i != 0)
            {
                jsonserializer.write(',');
            }
            jsonserializer.writeLong(l);
        }

        jsonserializer.append(']');
    }

}
