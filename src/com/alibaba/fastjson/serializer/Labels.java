// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.util.Arrays;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            LabelFilter

public class Labels
{
    private static class DefaultLabelFilter
        implements LabelFilter
    {

        private String excludes[];
        private String includes[];

        public boolean apply(String s)
        {
            if (excludes == null || Arrays.binarySearch(excludes, s) < 0)
            {
                if (includes != null)
                {
                    if (Arrays.binarySearch(includes, s) >= 0)
                    {
                        return true;
                    }
                } else
                {
                    return true;
                }
            }
            return false;
        }

        public DefaultLabelFilter(String as[], String as1[])
        {
            if (as != null)
            {
                includes = new String[as.length];
                System.arraycopy(as, 0, includes, 0, as.length);
                Arrays.sort(includes);
            }
            if (as1 != null)
            {
                excludes = new String[as1.length];
                System.arraycopy(as1, 0, excludes, 0, as1.length);
                Arrays.sort(excludes);
            }
        }
    }


    public Labels()
    {
    }

    public static transient LabelFilter excludes(String as[])
    {
        return new DefaultLabelFilter(null, as);
    }

    public static transient LabelFilter includes(String as[])
    {
        return new DefaultLabelFilter(as, null);
    }
}
