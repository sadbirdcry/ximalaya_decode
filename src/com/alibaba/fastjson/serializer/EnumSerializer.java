// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter, SerializerFeature

public class EnumSerializer
    implements ObjectSerializer
{

    public static final EnumSerializer instance = new EnumSerializer();

    public EnumSerializer()
    {
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        obj1 = jsonserializer.getWriter();
        if (obj == null)
        {
            jsonserializer.getWriter().writeNull();
            return;
        }
        obj = (Enum)obj;
        if (jsonserializer.isEnabled(SerializerFeature.WriteEnumUsingName))
        {
            jsonserializer.write(((Enum) (obj)).name());
            return;
        }
        if (jsonserializer.isEnabled(SerializerFeature.WriteEnumUsingToString))
        {
            jsonserializer.write(((Enum) (obj)).toString());
            return;
        } else
        {
            ((SerializeWriter) (obj1)).writeInt(((Enum) (obj)).ordinal());
            return;
        }
    }

}
