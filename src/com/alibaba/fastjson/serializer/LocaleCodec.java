// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Locale;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer

public class LocaleCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final LocaleCodec instance = new LocaleCodec();

    public LocaleCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        defaultjsonparser = (String)defaultjsonparser.parse();
        if (defaultjsonparser == null)
        {
            return null;
        }
        defaultjsonparser = defaultjsonparser.split("_");
        if (defaultjsonparser.length == 1)
        {
            return new Locale(defaultjsonparser[0]);
        }
        if (defaultjsonparser.length == 2)
        {
            return new Locale(defaultjsonparser[0], defaultjsonparser[1]);
        } else
        {
            return new Locale(defaultjsonparser[0], defaultjsonparser[1], defaultjsonparser[2]);
        }
    }

    public int getFastMatchToken()
    {
        return 4;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        if (obj == null)
        {
            jsonserializer.writeNull();
            return;
        } else
        {
            jsonserializer.write(((Locale)obj).toString());
            return;
        }
    }

}
