// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, SerializerFeature, JSONSerializer, SerializeWriter, 
//            SerialContext

public final class ListSerializer
    implements ObjectSerializer
{

    public static final ListSerializer instance = new ListSerializer();

    public ListSerializer()
    {
    }

    public final void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        Type type1;
        Object obj2;
        SerializeWriter serializewriter;
        boolean flag;
        flag = jsonserializer.isEnabled(SerializerFeature.WriteClassName);
        serializewriter = jsonserializer.getWriter();
        obj2 = null;
        type1 = ((Type) (obj2));
        if (flag)
        {
            type1 = ((Type) (obj2));
            if (type instanceof ParameterizedType)
            {
                type1 = ((ParameterizedType)type).getActualTypeArguments()[0];
            }
        }
        if (obj == null)
        {
            if (serializewriter.isEnabled(SerializerFeature.WriteNullListAsEmpty))
            {
                serializewriter.write("[]");
                return;
            } else
            {
                serializewriter.writeNull();
                return;
            }
        }
        obj2 = (List)obj;
        if (((List) (obj2)).size() == 0)
        {
            serializewriter.append("[]");
            return;
        }
        type = jsonserializer.getContext();
        jsonserializer.setContext(type, obj, obj1, 0);
        if (!serializewriter.isEnabled(SerializerFeature.PrettyFormat)) goto _L2; else goto _L1
_L1:
        serializewriter.append('[');
        jsonserializer.incrementIndent();
        obj2 = ((List) (obj2)).iterator();
        i = 0;
_L10:
        if (!((Iterator) (obj2)).hasNext()) goto _L4; else goto _L3
_L3:
        Object obj3 = ((Iterator) (obj2)).next();
        if (i == 0)
        {
            break MISSING_BLOCK_LABEL_189;
        }
        serializewriter.append(',');
        jsonserializer.println();
        if (obj3 == null) goto _L6; else goto _L5
_L5:
        if (!jsonserializer.containsReference(obj3)) goto _L8; else goto _L7
_L7:
        jsonserializer.writeReference(obj3);
          goto _L9
_L8:
        ObjectSerializer objectserializer = jsonserializer.getObjectWriter(obj3.getClass());
        jsonserializer.setContext(new SerialContext(type, obj, obj1, 0, 0));
        objectserializer.write(jsonserializer, obj3, Integer.valueOf(i), type1, 0);
          goto _L9
        obj;
        jsonserializer.setContext(type);
        throw obj;
_L6:
        jsonserializer.getWriter().writeNull();
          goto _L9
_L4:
        jsonserializer.decrementIdent();
        jsonserializer.println();
        serializewriter.append(']');
        jsonserializer.setContext(type);
        return;
_L2:
        serializewriter.append('[');
        obj2 = ((List) (obj2)).iterator();
        i = 0;
_L11:
        if (!((Iterator) (obj2)).hasNext())
        {
            break MISSING_BLOCK_LABEL_512;
        }
        obj3 = ((Iterator) (obj2)).next();
        if (i == 0)
        {
            break MISSING_BLOCK_LABEL_359;
        }
        serializewriter.append(',');
        if (obj3 != null)
        {
            break MISSING_BLOCK_LABEL_375;
        }
        serializewriter.append("null");
        break MISSING_BLOCK_LABEL_536;
        Class class1 = obj3.getClass();
        if (class1 != java/lang/Integer)
        {
            break MISSING_BLOCK_LABEL_405;
        }
        serializewriter.writeInt(((Integer)obj3).intValue());
        break MISSING_BLOCK_LABEL_536;
        if (class1 != java/lang/Long)
        {
            break MISSING_BLOCK_LABEL_449;
        }
        long l = ((Long)obj3).longValue();
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_439;
        }
        serializewriter.writeLongAndChar(l, 'L');
        break MISSING_BLOCK_LABEL_536;
        serializewriter.writeLong(l);
        break MISSING_BLOCK_LABEL_536;
        jsonserializer.setContext(new SerialContext(type, obj, obj1, 0, 0));
        if (jsonserializer.containsReference(obj3))
        {
            jsonserializer.writeReference(obj3);
            break MISSING_BLOCK_LABEL_536;
        }
        jsonserializer.getObjectWriter(obj3.getClass()).write(jsonserializer, obj3, Integer.valueOf(i), type1, 0);
        break MISSING_BLOCK_LABEL_536;
        serializewriter.append(']');
        jsonserializer.setContext(type);
        return;
_L9:
        i++;
          goto _L10
        i++;
          goto _L11
    }

}
