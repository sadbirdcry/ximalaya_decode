// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSON;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter, SerializerFeature, 
//            FilterUtils

public class MapSerializer
    implements ObjectSerializer
{

    public static MapSerializer instance = new MapSerializer();

    public MapSerializer()
    {
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        SerialContext serialcontext;
        SerializeWriter serializewriter;
        Map map;
        serializewriter = jsonserializer.getWriter();
        if (obj == null)
        {
            serializewriter.writeNull();
            return;
        }
        map = (Map)obj;
        if (jsonserializer.containsReference(obj))
        {
            jsonserializer.writeReference(obj);
            return;
        }
        serialcontext = jsonserializer.getContext();
        jsonserializer.setContext(serialcontext, obj, obj1, 0);
        serializewriter.write('{');
        jsonserializer.incrementIndent();
        obj1 = null;
        i = 1;
        if (!serializewriter.isEnabled(SerializerFeature.WriteClassName))
        {
            break MISSING_BLOCK_LABEL_101;
        }
        serializewriter.writeFieldName(JSON.DEFAULT_TYPE_KEY);
        serializewriter.writeString(obj.getClass().getName());
        i = 0;
        Iterator iterator = map.entrySet().iterator();
        ObjectSerializer objectserializer = null;
_L34:
        Object obj2;
        Object obj3;
        if (!iterator.hasNext())
        {
            break MISSING_BLOCK_LABEL_715;
        }
        type = (java.util.Map.Entry)iterator.next();
        obj2 = type.getValue();
        type = ((Type) (type.getKey()));
        obj3 = jsonserializer.getPropertyPreFiltersDirect();
        if (obj3 == null) goto _L2; else goto _L1
_L1:
        if (((List) (obj3)).size() <= 0) goto _L2; else goto _L3
_L3:
        if (type == null) goto _L5; else goto _L4
_L4:
        if (!(type instanceof String)) goto _L6; else goto _L5
_L5:
        if (!FilterUtils.applyName(jsonserializer, obj, (String)type))
        {
            continue; /* Loop/switch isn't completed */
        }
_L2:
        obj3 = jsonserializer.getPropertyFiltersDirect();
        if (obj3 == null) goto _L8; else goto _L7
_L7:
        if (((List) (obj3)).size() <= 0) goto _L8; else goto _L9
_L9:
        if (type == null) goto _L11; else goto _L10
_L10:
        if (!(type instanceof String)) goto _L12; else goto _L11
_L11:
        if (!FilterUtils.apply(jsonserializer, obj, (String)type, obj2))
        {
            break; /* Loop/switch isn't completed */
        }
_L8:
        obj3 = jsonserializer.getNameFiltersDirect();
        if (obj3 == null) goto _L14; else goto _L13
_L13:
        if (((List) (obj3)).size() <= 0) goto _L14; else goto _L15
_L15:
        if (type == null) goto _L17; else goto _L16
_L16:
        if (!(type instanceof String)) goto _L18; else goto _L17
_L17:
        type = FilterUtils.processKey(jsonserializer, obj, (String)type, obj2);
_L14:
        obj3 = jsonserializer.getValueFiltersDirect();
        if (obj3 == null) goto _L20; else goto _L19
_L19:
        if (((List) (obj3)).size() <= 0) goto _L20; else goto _L21
_L21:
        if (type == null) goto _L23; else goto _L22
_L22:
        if (!(type instanceof String)) goto _L24; else goto _L23
_L23:
        obj2 = FilterUtils.processValue(jsonserializer, obj, (String)type, obj2);
_L20:
        if (obj2 != null)
        {
            break MISSING_BLOCK_LABEL_365;
        }
        if (!jsonserializer.isEnabled(SerializerFeature.WriteMapNullValue))
        {
            break; /* Loop/switch isn't completed */
        }
        if (!(type instanceof String)) goto _L26; else goto _L25
_L25:
        obj3 = (String)type;
        if (i != 0)
        {
            break MISSING_BLOCK_LABEL_392;
        }
        serializewriter.write(',');
        if (serializewriter.isEnabled(SerializerFeature.PrettyFormat))
        {
            jsonserializer.println();
        }
        serializewriter.writeFieldName(((String) (obj3)), true);
_L31:
        if (obj2 != null)
        {
            break MISSING_BLOCK_LABEL_657;
        }
        serializewriter.writeNull();
        i = 0;
        break; /* Loop/switch isn't completed */
_L6:
        if (!type.getClass().isPrimitive() && !(type instanceof Number) || FilterUtils.applyName(jsonserializer, obj, JSON.toJSONString(type))) goto _L2; else goto _L27
_L27:
        break; /* Loop/switch isn't completed */
_L12:
        if (!type.getClass().isPrimitive() && !(type instanceof Number) || FilterUtils.apply(jsonserializer, obj, JSON.toJSONString(type), obj2)) goto _L8; else goto _L28
_L28:
        continue; /* Loop/switch isn't completed */
_L18:
        if (!type.getClass().isPrimitive() && !(type instanceof Number)) goto _L14; else goto _L29
_L29:
        type = FilterUtils.processKey(jsonserializer, obj, JSON.toJSONString(type), obj2);
          goto _L14
_L24:
        if (!type.getClass().isPrimitive() && !(type instanceof Number)) goto _L20; else goto _L30
_L30:
        obj2 = FilterUtils.processValue(jsonserializer, obj, JSON.toJSONString(type), obj2);
          goto _L20
_L26:
        if (i != 0)
        {
            break MISSING_BLOCK_LABEL_587;
        }
        serializewriter.write(',');
        if (!serializewriter.isEnabled(SerializerFeature.BrowserCompatible) && !serializewriter.isEnabled(SerializerFeature.WriteNonStringKeyAsString) && !serializewriter.isEnabled(SerializerFeature.BrowserSecure))
        {
            break MISSING_BLOCK_LABEL_648;
        }
        jsonserializer.write(JSON.toJSONString(type));
_L32:
        serializewriter.write(':');
          goto _L31
        obj;
        jsonserializer.setContext(serialcontext);
        throw obj;
        jsonserializer.write(type);
          goto _L32
        obj3 = obj2.getClass();
        if (obj3 != obj1)
        {
            break MISSING_BLOCK_LABEL_687;
        }
        objectserializer.write(jsonserializer, obj2, type, null, 0);
        break MISSING_BLOCK_LABEL_764;
        objectserializer = jsonserializer.getObjectWriter(((Class) (obj3)));
        objectserializer.write(jsonserializer, obj2, type, null, 0);
        obj1 = obj3;
        break MISSING_BLOCK_LABEL_764;
        jsonserializer.setContext(serialcontext);
        jsonserializer.decrementIdent();
        if (serializewriter.isEnabled(SerializerFeature.PrettyFormat) && map.size() > 0)
        {
            jsonserializer.println();
        }
        serializewriter.write('}');
        return;
        i = 0;
        if (true) goto _L34; else goto _L33
_L33:
    }

}
