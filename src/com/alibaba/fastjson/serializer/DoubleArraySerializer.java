// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class DoubleArraySerializer
    implements ObjectSerializer
{

    public static final DoubleArraySerializer instance = new DoubleArraySerializer();

    public DoubleArraySerializer()
    {
    }

    public final void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer = jsonserializer.getWriter();
        if (obj == null)
        {
            if (jsonserializer.isEnabled(SerializerFeature.WriteNullListAsEmpty))
            {
                jsonserializer.write("[]");
                return;
            } else
            {
                jsonserializer.writeNull();
                return;
            }
        }
        obj = (double[])(double[])obj;
        int j = obj.length - 1;
        if (j == -1)
        {
            jsonserializer.append("[]");
            return;
        }
        jsonserializer.append('[');
        i = 0;
        while (i < j) 
        {
            double d = obj[i];
            if (Double.isNaN(d))
            {
                jsonserializer.writeNull();
            } else
            {
                jsonserializer.append(Double.toString(d));
            }
            jsonserializer.append(',');
            i++;
        }
        double d1 = obj[j];
        if (Double.isNaN(d1))
        {
            jsonserializer.writeNull();
        } else
        {
            jsonserializer.append(Double.toString(d1));
        }
        jsonserializer.append(']');
    }

}
