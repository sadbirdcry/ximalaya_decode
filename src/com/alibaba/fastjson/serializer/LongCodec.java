// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicLong;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class LongCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static LongCodec instance = new LongCodec();

    public LongCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        obj = defaultjsonparser.getLexer();
        if (((JSONLexer) (obj)).token() == 2)
        {
            long l = ((JSONLexer) (obj)).longValue();
            ((JSONLexer) (obj)).nextToken(16);
            defaultjsonparser = Long.valueOf(l);
        } else
        {
            defaultjsonparser = ((DefaultJSONParser) (defaultjsonparser.parse()));
            if (defaultjsonparser == null)
            {
                return null;
            }
            defaultjsonparser = TypeUtils.castToLong(defaultjsonparser);
        }
        obj = defaultjsonparser;
        if (type == java/util/concurrent/atomic/AtomicLong)
        {
            obj = new AtomicLong(defaultjsonparser.longValue());
        }
        return obj;
    }

    public int getFastMatchToken()
    {
        return 2;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        obj1 = jsonserializer.getWriter();
        if (obj != null) goto _L2; else goto _L1
_L1:
        if (!((SerializeWriter) (obj1)).isEnabled(SerializerFeature.WriteNullNumberAsZero)) goto _L4; else goto _L3
_L3:
        ((SerializeWriter) (obj1)).write('0');
_L6:
        return;
_L4:
        ((SerializeWriter) (obj1)).writeNull();
        return;
_L2:
        long l = ((Long)obj).longValue();
        ((SerializeWriter) (obj1)).writeLong(l);
        if (jsonserializer.isEnabled(SerializerFeature.WriteClassName) && l <= 0x7fffffffL && l >= 0xffffffff80000000L && type != java/lang/Long)
        {
            ((SerializeWriter) (obj1)).write('L');
            return;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

}
