// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.util.Base64;
import com.alibaba.fastjson.util.IOUtils;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.lang.ref.SoftReference;
import java.math.BigDecimal;
import java.nio.charset.Charset;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            SerializerFeature, SerialWriterStringEncoder

public final class SerializeWriter extends Writer
{

    private static final ThreadLocal bufLocal = new ThreadLocal();
    protected char buf[];
    protected int count;
    private int features;
    private final Writer writer;

    public SerializeWriter()
    {
        this((Writer)null);
    }

    public SerializeWriter(int i)
    {
        this(null, i);
    }

    public SerializeWriter(Writer writer1)
    {
        writer = writer1;
        features = JSON.DEFAULT_GENERATE_FEATURE;
        writer1 = (SoftReference)bufLocal.get();
        if (writer1 != null)
        {
            buf = (char[])writer1.get();
            bufLocal.set(null);
        }
        if (buf == null)
        {
            buf = new char[1024];
        }
    }

    public SerializeWriter(Writer writer1, int i)
    {
        writer = writer1;
        if (i <= 0)
        {
            throw new IllegalArgumentException((new StringBuilder()).append("Negative initial size: ").append(i).toString());
        } else
        {
            buf = new char[i];
            return;
        }
    }

    public transient SerializeWriter(Writer writer1, SerializerFeature aserializerfeature[])
    {
        int j = 0;
        super();
        writer = writer1;
        writer1 = (SoftReference)bufLocal.get();
        if (writer1 != null)
        {
            buf = (char[])writer1.get();
            bufLocal.set(null);
        }
        if (buf == null)
        {
            buf = new char[1024];
        }
        int k = aserializerfeature.length;
        for (int i = 0; i < k; i++)
        {
            j |= aserializerfeature[i].getMask();
        }

        features = j;
    }

    public transient SerializeWriter(SerializerFeature aserializerfeature[])
    {
        this(null, aserializerfeature);
    }

    static boolean isSpecial(char c, int i)
    {
        if (c != ' ')
        {
            if (c == '/' && SerializerFeature.isEnabled(i, SerializerFeature.WriteSlashAsSpecial))
            {
                return true;
            }
            if ((c <= '#' || c == '\\') && (c <= '\037' || c == '\\' || c == '"'))
            {
                return true;
            }
        }
        return false;
    }

    private void writeEnumFieldValue(char c, String s, String s1)
    {
        if (isEnabled(SerializerFeature.UseSingleQuotes))
        {
            writeFieldValue(c, s, s1);
            return;
        } else
        {
            writeFieldValueStringWithDoubleQuote(c, s, s1, false);
            return;
        }
    }

    private void writeEnumValue(String s, char c)
    {
        if (isEnabled(SerializerFeature.UseSingleQuotes))
        {
            write('\'');
            write(s);
            write('\'');
            write(c);
            return;
        } else
        {
            write('"');
            write(s);
            write('"');
            write(c);
            return;
        }
    }

    private void writeFieldValueStringWithDoubleQuote(char c, String s, String s1, boolean flag)
    {
        int i;
        int k3;
        int l3;
        int l = s.length();
        i = count;
        int j;
        if (s1 == null)
        {
            j = 4;
            i += l + 8;
        } else
        {
            j = s1.length();
            i += l + j + 6;
        }
        if (i > buf.length)
        {
            if (writer != null)
            {
                write(c);
                writeStringWithDoubleQuote(s, ':', flag);
                writeStringWithDoubleQuote(s1, '\0', flag);
                return;
            }
            expandCapacity(i);
        }
        buf[count] = c;
        int j1 = count + 2;
        int l1 = j1 + l;
        buf[count + 1] = '"';
        s.getChars(0, l, buf, j1);
        count = i;
        buf[l1] = '"';
        j1 = l1 + 1;
        s = buf;
        l = j1 + 1;
        s[j1] = ':';
        if (s1 == null)
        {
            s = buf;
            i = l + 1;
            s[l] = 'n';
            s = buf;
            j = i + 1;
            s[i] = 'u';
            s = buf;
            i = j + 1;
            s[j] = 'l';
            buf[i] = 'l';
            return;
        }
        s = buf;
        k3 = l + 1;
        s[l] = '"';
        l3 = k3 + j;
        s1.getChars(0, j, buf, k3);
        if (!flag || isEnabled(SerializerFeature.DisableCheckSpecialChar)) goto _L2; else goto _L1
_L1:
        int k;
        int i1;
        int k1;
        int i2;
        int j2;
        i2 = 0;
        j2 = -1;
        k1 = 0;
        k = k3;
        byte byte0 = -1;
        i1 = i;
        i = k;
        k = byte0;
_L9:
        if (i >= l3) goto _L4; else goto _L3
_L3:
        c = buf[i];
        if (c != '\u2028') goto _L6; else goto _L5
_L5:
        j2 = i2 + 1;
        i1 += 4;
        k1 = j2;
        i2 = i1;
        if (k != -1) goto _L8; else goto _L7
_L7:
        char c1 = c;
        k = i1;
        i2 = j2;
        i1 = i;
        k1 = i;
        j2 = k;
        k = c1;
_L10:
        i++;
        int k2 = j2;
        j2 = k1;
        k1 = k;
        k = i1;
        i1 = k2;
          goto _L9
_L6:
label0:
        {
            if (c < ']')
            {
                break label0;
            }
            if (c < '\177' || c > '\240')
            {
                break MISSING_BLOCK_LABEL_1455;
            }
            k1 = k;
            if (k == -1)
            {
                k1 = i;
            }
            j2 = i1 + 4;
            int l2 = i2 + 1;
            i2 = i;
            k = c;
            i1 = k1;
            k1 = i2;
            i2 = l2;
        }
          goto _L10
        if (!isSpecial(c, features))
        {
            break MISSING_BLOCK_LABEL_1455;
        }
        j2 = i2 + 1;
        if (c < IOUtils.specicalFlags_doubleQuotes.length && IOUtils.specicalFlags_doubleQuotes[c] == 4)
        {
            i1 += 4;
        }
        k1 = j2;
        i2 = i1;
        if (k != -1) goto _L8; else goto _L11
_L11:
        k = c;
        int i3 = i1;
        i2 = j2;
        i1 = i;
        k1 = i;
        j2 = i3;
          goto _L10
_L4:
        if (i2 <= 0) goto _L2; else goto _L12
_L12:
        i = i1 + i2;
        if (i > buf.length)
        {
            expandCapacity(i);
        }
        count = i;
        if (i2 != 1) goto _L14; else goto _L13
_L13:
        if (k1 == 8232)
        {
            System.arraycopy(buf, j2 + 1, buf, j2 + 6, l3 - j2 - 1);
            buf[j2] = '\\';
            s = buf;
            i = j2 + 1;
            s[i] = 'u';
            s = buf;
            i++;
            s[i] = '2';
            s = buf;
            i++;
            s[i] = '0';
            s = buf;
            i++;
            s[i] = '2';
            buf[i + 1] = '8';
        } else
        if (k1 < IOUtils.specicalFlags_doubleQuotes.length && IOUtils.specicalFlags_doubleQuotes[k1] == 4)
        {
            System.arraycopy(buf, j2 + 1, buf, j2 + 6, l3 - j2 - 1);
            s = buf;
            i = j2 + 1;
            s[j2] = '\\';
            s = buf;
            k = i + 1;
            s[i] = 'u';
            s = buf;
            i = k + 1;
            s[k] = IOUtils.DIGITS[k1 >>> 12 & 0xf];
            s = buf;
            k = i + 1;
            s[i] = IOUtils.DIGITS[k1 >>> 8 & 0xf];
            s = buf;
            i = k + 1;
            s[k] = IOUtils.DIGITS[k1 >>> 4 & 0xf];
            buf[i] = IOUtils.DIGITS[k1 & 0xf];
        } else
        {
            System.arraycopy(buf, j2 + 1, buf, j2 + 2, l3 - j2 - 1);
            buf[j2] = '\\';
            buf[j2 + 1] = IOUtils.replaceChars[k1];
        }
_L2:
        buf[count - 1] = '"';
        return;
_L14:
        if (i2 > 1)
        {
            i = l3;
            k1 = k - k3;
            i1 = k;
            k = i;
            i = i1;
            i1 = k1;
            while (i1 < s1.length()) 
            {
                c = s1.charAt(i1);
                if (c < IOUtils.specicalFlags_doubleQuotes.length && IOUtils.specicalFlags_doubleQuotes[c] != 0 || c == '/' && isEnabled(SerializerFeature.WriteSlashAsSpecial))
                {
                    s = buf;
                    k1 = i + 1;
                    s[i] = '\\';
                    if (IOUtils.specicalFlags_doubleQuotes[c] == 4)
                    {
                        s = buf;
                        i = k1 + 1;
                        s[k1] = 'u';
                        s = buf;
                        k1 = i + 1;
                        s[i] = IOUtils.DIGITS[c >>> 12 & 0xf];
                        s = buf;
                        i = k1 + 1;
                        s[k1] = IOUtils.DIGITS[c >>> 8 & 0xf];
                        s = buf;
                        k1 = i + 1;
                        s[i] = IOUtils.DIGITS[c >>> 4 & 0xf];
                        s = buf;
                        i = k1 + 1;
                        s[k1] = IOUtils.DIGITS[c & 0xf];
                        k += 5;
                    } else
                    {
                        s = buf;
                        i = k1 + 1;
                        s[k1] = IOUtils.replaceChars[c];
                        k++;
                    }
                } else
                if (c == '\u2028')
                {
                    s = buf;
                    k1 = i + 1;
                    s[i] = '\\';
                    s = buf;
                    i = k1 + 1;
                    s[k1] = 'u';
                    s = buf;
                    k1 = i + 1;
                    s[i] = IOUtils.DIGITS[c >>> 12 & 0xf];
                    s = buf;
                    i = k1 + 1;
                    s[k1] = IOUtils.DIGITS[c >>> 8 & 0xf];
                    s = buf;
                    k1 = i + 1;
                    s[i] = IOUtils.DIGITS[c >>> 4 & 0xf];
                    s = buf;
                    i = k1 + 1;
                    s[k1] = IOUtils.DIGITS[c & 0xf];
                    k += 5;
                } else
                {
                    buf[i] = c;
                    i++;
                }
                i1++;
            }
        }
        if (true) goto _L2; else goto _L8
_L8:
        j2 = i2;
        i2 = k1;
        k1 = i;
        char c2 = c;
        i1 = k;
        k = c2;
          goto _L10
        int j3 = k;
        k = k1;
        k1 = j2;
        j2 = i1;
        i1 = j3;
          goto _L10
    }

    private void writeKeyWithDoubleQuoteIfHasSpecial(String s)
    {
_L1:
        if (i != 0)
        {
            write('"');
        }
        int j = 0;
        while (j < l) 
        {
            char c = s.charAt(j);
            if (c < abyte0.length && abyte0[c] != 0)
            {
                write('\\');
                write(IOUtils.replaceChars[c]);
            } else
            {
                write(c);
            }
            j++;
        }
        if (i != 0)
        {
            write('"');
        }
        write(':');
        return;
        int i;
label0:
        {
            byte abyte0[] = IOUtils.specicalFlags_doubleQuotes;
            int l = s.length();
            int k1 = count + l + 1;
            if (k1 > buf.length)
            {
                if (writer != null)
                {
                    if (l == 0)
                    {
                        write('"');
                        write('"');
                        write(':');
                        return;
                    }
                    i = 0;
                    do
                    {
                        if (i >= l)
                        {
                            break label0;
                        }
                        char c1 = s.charAt(i);
                        if (c1 < abyte0.length && abyte0[c1] != 0)
                        {
                            i = 1;
                            break MISSING_BLOCK_LABEL_97;
                        }
                        i++;
                    } while (true);
                }
                expandCapacity(k1);
            }
            if (l == 0)
            {
                if (count + 3 > buf.length)
                {
                    expandCapacity(count + 3);
                }
                s = buf;
                i = count;
                count = i + 1;
                s[i] = '"';
                s = buf;
                i = count;
                count = i + 1;
                s[i] = '"';
                s = buf;
                i = count;
                count = i + 1;
                s[i] = ':';
                return;
            }
            int l1 = count;
            int j1 = l1 + l;
            s.getChars(0, l, buf, l1);
            count = k1;
            boolean flag = false;
            l = l1;
            while (l < j1) 
            {
                char c2 = buf[l];
                int i1 = l;
                boolean flag1 = flag;
                i = j1;
                int k = k1;
                if (c2 < abyte0.length)
                {
                    i1 = l;
                    flag1 = flag;
                    i = j1;
                    k = k1;
                    if (abyte0[c2] != 0)
                    {
                        if (!flag)
                        {
                            k = k1 + 3;
                            if (k > buf.length)
                            {
                                expandCapacity(k);
                            }
                            count = k;
                            System.arraycopy(buf, l + 1, buf, l + 3, j1 - l - 1);
                            System.arraycopy(buf, 0, buf, 1, l);
                            buf[l1] = '"';
                            s = buf;
                            i = l + 1;
                            s[i] = '\\';
                            s = buf;
                            i1 = i + 1;
                            s[i1] = IOUtils.replaceChars[c2];
                            i = j1 + 2;
                            buf[count - 2] = '"';
                            flag1 = true;
                        } else
                        {
                            k = k1 + 1;
                            if (k > buf.length)
                            {
                                expandCapacity(k);
                            }
                            count = k;
                            System.arraycopy(buf, l + 1, buf, l + 2, j1 - l);
                            buf[l] = '\\';
                            s = buf;
                            i1 = l + 1;
                            s[i1] = IOUtils.replaceChars[c2];
                            i = j1 + 1;
                            flag1 = flag;
                        }
                    }
                }
                l = i1 + 1;
                flag = flag1;
                j1 = i;
                k1 = k;
            }
            buf[count - 1] = ':';
            return;
        }
        i = 0;
          goto _L1
    }

    private void writeKeyWithSingleQuoteIfHasSpecial(String s)
    {
_L1:
        if (i != 0)
        {
            write('\'');
        }
        int j = 0;
        while (j < l) 
        {
            char c = s.charAt(j);
            if (c < abyte0.length && abyte0[c] != 0)
            {
                write('\\');
                write(IOUtils.replaceChars[c]);
            } else
            {
                write(c);
            }
            j++;
        }
        if (i != 0)
        {
            write('\'');
        }
        write(':');
        return;
        int i;
label0:
        {
            byte abyte0[] = IOUtils.specicalFlags_singleQuotes;
            int l = s.length();
            int k1 = count + l + 1;
            if (k1 > buf.length)
            {
                if (writer != null)
                {
                    if (l == 0)
                    {
                        write('\'');
                        write('\'');
                        write(':');
                        return;
                    }
                    i = 0;
                    do
                    {
                        if (i >= l)
                        {
                            break label0;
                        }
                        char c1 = s.charAt(i);
                        if (c1 < abyte0.length && abyte0[c1] != 0)
                        {
                            i = 1;
                            break MISSING_BLOCK_LABEL_97;
                        }
                        i++;
                    } while (true);
                }
                expandCapacity(k1);
            }
            if (l == 0)
            {
                if (count + 3 > buf.length)
                {
                    expandCapacity(count + 3);
                }
                s = buf;
                i = count;
                count = i + 1;
                s[i] = '\'';
                s = buf;
                i = count;
                count = i + 1;
                s[i] = '\'';
                s = buf;
                i = count;
                count = i + 1;
                s[i] = ':';
                return;
            }
            int l1 = count;
            int j1 = l1 + l;
            s.getChars(0, l, buf, l1);
            count = k1;
            boolean flag = false;
            l = l1;
            while (l < j1) 
            {
                char c2 = buf[l];
                int i1 = l;
                boolean flag1 = flag;
                i = j1;
                int k = k1;
                if (c2 < abyte0.length)
                {
                    i1 = l;
                    flag1 = flag;
                    i = j1;
                    k = k1;
                    if (abyte0[c2] != 0)
                    {
                        if (!flag)
                        {
                            k = k1 + 3;
                            if (k > buf.length)
                            {
                                expandCapacity(k);
                            }
                            count = k;
                            System.arraycopy(buf, l + 1, buf, l + 3, j1 - l - 1);
                            System.arraycopy(buf, 0, buf, 1, l);
                            buf[l1] = '\'';
                            s = buf;
                            i = l + 1;
                            s[i] = '\\';
                            s = buf;
                            i1 = i + 1;
                            s[i1] = IOUtils.replaceChars[c2];
                            i = j1 + 2;
                            buf[count - 2] = '\'';
                            flag1 = true;
                        } else
                        {
                            k = k1 + 1;
                            if (k > buf.length)
                            {
                                expandCapacity(k);
                            }
                            count = k;
                            System.arraycopy(buf, l + 1, buf, l + 2, j1 - l);
                            buf[l] = '\\';
                            s = buf;
                            i1 = l + 1;
                            s[i1] = IOUtils.replaceChars[c2];
                            i = j1 + 1;
                            flag1 = flag;
                        }
                    }
                }
                l = i1 + 1;
                flag = flag1;
                j1 = i;
                k1 = k;
            }
            buf[k1 - 1] = ':';
            return;
        }
        i = 0;
          goto _L1
    }

    private void writeStringWithDoubleQuote(String s, char c)
    {
        writeStringWithDoubleQuote(s, c, true);
    }

    private void writeStringWithDoubleQuote(String s, char c, boolean flag)
    {
        if (s != null) goto _L2; else goto _L1
_L1:
        writeNull();
        if (c != 0)
        {
            write(c);
        }
_L7:
        return;
_L2:
        int i;
        int k1;
        k1 = s.length();
        int j = count + k1 + 2;
        i = j;
        if (c != 0)
        {
            i = j + 1;
        }
        if (i <= buf.length)
        {
            break MISSING_BLOCK_LABEL_637;
        }
        if (writer == null)
        {
            break; /* Loop/switch isn't completed */
        }
        write('"');
        i = 0;
_L4:
        char c1;
        if (i >= s.length())
        {
            break MISSING_BLOCK_LABEL_615;
        }
        c1 = s.charAt(i);
        if (!isEnabled(SerializerFeature.BrowserSecure))
        {
            break; /* Loop/switch isn't completed */
        }
        if (c1 >= '0' && c1 <= '9' || c1 >= 'a' && c1 <= 'z' || c1 >= 'A' && c1 <= 'Z' || c1 == ',' || c1 == '.' || c1 == '_')
        {
            break MISSING_BLOCK_LABEL_606;
        }
        write('\\');
        write('u');
        write(IOUtils.DIGITS[c1 >>> 12 & 0xf]);
        write(IOUtils.DIGITS[c1 >>> 8 & 0xf]);
        write(IOUtils.DIGITS[c1 >>> 4 & 0xf]);
        write(IOUtils.DIGITS[c1 & 0xf]);
_L5:
        i++;
        if (true) goto _L4; else goto _L3
_L3:
        if (isEnabled(SerializerFeature.BrowserCompatible))
        {
            if (c1 == '\b' || c1 == '\f' || c1 == '\n' || c1 == '\r' || c1 == '\t' || c1 == '"' || c1 == '/' || c1 == '\\')
            {
                write('\\');
                write(IOUtils.replaceChars[c1]);
            } else
            if (c1 < ' ')
            {
                write('\\');
                write('u');
                write('0');
                write('0');
                write(IOUtils.ASCII_CHARS[c1 * 2]);
                write(IOUtils.ASCII_CHARS[c1 * 2 + 1]);
            } else
            {
                if (c1 < '\177')
                {
                    break MISSING_BLOCK_LABEL_606;
                }
                write('\\');
                write('u');
                write(IOUtils.DIGITS[c1 >>> 12 & 0xf]);
                write(IOUtils.DIGITS[c1 >>> 8 & 0xf]);
                write(IOUtils.DIGITS[c1 >>> 4 & 0xf]);
                write(IOUtils.DIGITS[c1 & 0xf]);
            }
        } else
        {
            if ((c1 >= IOUtils.specicalFlags_doubleQuotes.length || IOUtils.specicalFlags_doubleQuotes[c1] == 0) && (c1 != '/' || !isEnabled(SerializerFeature.WriteSlashAsSpecial)))
            {
                break MISSING_BLOCK_LABEL_606;
            }
            write('\\');
            if (IOUtils.specicalFlags_doubleQuotes[c1] == 4)
            {
                write('u');
                write(IOUtils.DIGITS[c1 >>> 12 & 0xf]);
                write(IOUtils.DIGITS[c1 >>> 8 & 0xf]);
                write(IOUtils.DIGITS[c1 >>> 4 & 0xf]);
                write(IOUtils.DIGITS[c1 & 0xf]);
            } else
            {
                write(IOUtils.replaceChars[c1]);
            }
        }
          goto _L5
        write(c1);
          goto _L5
        write('"');
        if (c != 0)
        {
            write(c);
            return;
        }
        if (true) goto _L7; else goto _L6
_L6:
        expandCapacity(i);
        int j2;
        int l2;
        int i3;
        int j3;
        int k3;
label0:
        {
            j3 = count + 1;
            k3 = j3 + k1;
            buf[count] = '"';
            s.getChars(0, k1, buf, j3);
            count = i;
            if (!isEnabled(SerializerFeature.BrowserSecure))
            {
                break label0;
            }
            k1 = i;
            int k = -1;
            for (i = j3; i < k3;)
            {
                int l1;
                int k2;
label1:
                {
                    char c4 = buf[i];
                    if (c4 >= '0')
                    {
                        k2 = k;
                        l1 = k1;
                        if (c4 <= '9')
                        {
                            break label1;
                        }
                    }
                    if (c4 >= 'a')
                    {
                        k2 = k;
                        l1 = k1;
                        if (c4 <= 'z')
                        {
                            break label1;
                        }
                    }
                    if (c4 >= 'A')
                    {
                        k2 = k;
                        l1 = k1;
                        if (c4 <= 'Z')
                        {
                            break label1;
                        }
                    }
                    k2 = k;
                    l1 = k1;
                    if (c4 != ',')
                    {
                        k2 = k;
                        l1 = k1;
                        if (c4 != '.')
                        {
                            k2 = k;
                            l1 = k1;
                            if (c4 != '_')
                            {
                                l1 = k1 + 5;
                                k2 = i;
                            }
                        }
                    }
                }
                i++;
                k = k2;
                k1 = l1;
            }

            if (k1 > buf.length)
            {
                expandCapacity(k1);
            }
            count = k1;
            i = k;
            for (int l = k3; i >= j3; l = k1)
            {
label2:
                {
                    char c2 = buf[i];
                    if (c2 >= '0')
                    {
                        k1 = l;
                        if (c2 <= '9')
                        {
                            break label2;
                        }
                    }
                    if (c2 >= 'a')
                    {
                        k1 = l;
                        if (c2 <= 'z')
                        {
                            break label2;
                        }
                    }
                    if (c2 >= 'A')
                    {
                        k1 = l;
                        if (c2 <= 'Z')
                        {
                            break label2;
                        }
                    }
                    k1 = l;
                    if (c2 != ',')
                    {
                        k1 = l;
                        if (c2 != '.')
                        {
                            k1 = l;
                            if (c2 != '_')
                            {
                                System.arraycopy(buf, i + 1, buf, i + 6, l - i - 1);
                                buf[i] = '\\';
                                buf[i + 1] = 'u';
                                buf[i + 2] = IOUtils.DIGITS[c2 >>> 12 & 0xf];
                                buf[i + 3] = IOUtils.DIGITS[c2 >>> 8 & 0xf];
                                buf[i + 4] = IOUtils.DIGITS[c2 >>> 4 & 0xf];
                                buf[i + 5] = IOUtils.DIGITS[c2 & 0xf];
                                k1 = l + 5;
                            }
                        }
                    }
                }
                i--;
            }

            if (c != 0)
            {
                buf[count - 2] = '"';
                buf[count - 1] = c;
                return;
            } else
            {
                buf[count - 1] = '"';
                return;
            }
        }
        if (isEnabled(SerializerFeature.BrowserCompatible))
        {
            int i2 = i;
            int i1 = -1;
            i = j3;
            while (i < k3) 
            {
                char c3 = buf[i];
                if (c3 == '"' || c3 == '/' || c3 == '\\')
                {
                    k1 = i2 + 1;
                    i1 = i;
                } else
                if (c3 == '\b' || c3 == '\f' || c3 == '\n' || c3 == '\r' || c3 == '\t')
                {
                    k1 = i2 + 1;
                    i1 = i;
                } else
                if (c3 < ' ')
                {
                    k1 = i2 + 5;
                    i1 = i;
                } else
                {
                    k1 = i2;
                    if (c3 >= '\177')
                    {
                        k1 = i2 + 5;
                        i1 = i;
                    }
                }
                i++;
                i2 = k1;
            }
            if (i2 > buf.length)
            {
                expandCapacity(i2);
            }
            count = i2;
            k1 = k3;
            while (i1 >= j3) 
            {
                c1 = buf[i1];
                if (c1 == '\b' || c1 == '\f' || c1 == '\n' || c1 == '\r' || c1 == '\t')
                {
                    System.arraycopy(buf, i1 + 1, buf, i1 + 2, k1 - i1 - 1);
                    buf[i1] = '\\';
                    buf[i1 + 1] = IOUtils.replaceChars[c1];
                    i = k1 + 1;
                } else
                if (c1 == '"' || c1 == '/' || c1 == '\\')
                {
                    System.arraycopy(buf, i1 + 1, buf, i1 + 2, k1 - i1 - 1);
                    buf[i1] = '\\';
                    buf[i1 + 1] = c1;
                    i = k1 + 1;
                } else
                if (c1 < ' ')
                {
                    System.arraycopy(buf, i1 + 1, buf, i1 + 6, k1 - i1 - 1);
                    buf[i1] = '\\';
                    buf[i1 + 1] = 'u';
                    buf[i1 + 2] = '0';
                    buf[i1 + 3] = '0';
                    buf[i1 + 4] = IOUtils.ASCII_CHARS[c1 * 2];
                    buf[i1 + 5] = IOUtils.ASCII_CHARS[c1 * 2 + 1];
                    i = k1 + 5;
                } else
                {
                    i = k1;
                    if (c1 >= '\177')
                    {
                        System.arraycopy(buf, i1 + 1, buf, i1 + 6, k1 - i1 - 1);
                        buf[i1] = '\\';
                        buf[i1 + 1] = 'u';
                        buf[i1 + 2] = IOUtils.DIGITS[c1 >>> 12 & 0xf];
                        buf[i1 + 3] = IOUtils.DIGITS[c1 >>> 8 & 0xf];
                        buf[i1 + 4] = IOUtils.DIGITS[c1 >>> 4 & 0xf];
                        buf[i1 + 5] = IOUtils.DIGITS[c1 & 0xf];
                        i = k1 + 5;
                    }
                }
                i1--;
                k1 = i;
            }
            if (c != 0)
            {
                buf[count - 2] = '"';
                buf[count - 1] = c;
                return;
            } else
            {
                buf[count - 1] = '"';
                return;
            }
        }
        l2 = 0;
        i3 = -1;
        j2 = 0;
        if (!flag) goto _L9; else goto _L8
_L8:
        int j1;
        j1 = j3;
        byte byte0 = -1;
        k1 = i;
        i = j1;
        j1 = byte0;
_L16:
        if (i >= k3) goto _L11; else goto _L10
_L10:
        c1 = buf[i];
        if (c1 != '\u2028') goto _L13; else goto _L12
_L12:
        i3 = l2 + 1;
        k1 += 4;
        j2 = i3;
        l2 = k1;
        if (j1 != -1) goto _L15; else goto _L14
_L14:
        char c5 = c1;
        j1 = k1;
        l2 = i3;
        k1 = i;
        j2 = i;
        i3 = j1;
        j1 = c5;
_L17:
        i++;
        int l3 = i3;
        i3 = j2;
        j2 = j1;
        j1 = k1;
        k1 = l3;
          goto _L16
_L13:
label3:
        {
            if (c1 < ']')
            {
                break label3;
            }
            if (c1 < '\177' || c1 > '\240')
            {
                break MISSING_BLOCK_LABEL_3115;
            }
            j2 = j1;
            if (j1 == -1)
            {
                j2 = i;
            }
            i3 = k1 + 4;
            int i4 = l2 + 1;
            l2 = i;
            j1 = c1;
            k1 = j2;
            j2 = l2;
            l2 = i4;
        }
          goto _L17
        if (!isSpecial(c1, features))
        {
            break MISSING_BLOCK_LABEL_3115;
        }
        i3 = l2 + 1;
        if (c1 < IOUtils.specicalFlags_doubleQuotes.length && IOUtils.specicalFlags_doubleQuotes[c1] == 4)
        {
            k1 += 4;
        }
        j2 = i3;
        l2 = k1;
        if (j1 != -1) goto _L15; else goto _L18
_L18:
        j1 = c1;
        int j4 = k1;
        l2 = i3;
        k1 = i;
        j2 = i;
        i3 = j4;
          goto _L17
_L11:
        if (l2 > 0)
        {
            i = k1 + l2;
            if (i > buf.length)
            {
                expandCapacity(i);
            }
            count = i;
            if (l2 == 1)
            {
                if (j2 == 8232)
                {
                    System.arraycopy(buf, i3 + 1, buf, i3 + 6, k3 - i3 - 1);
                    buf[i3] = '\\';
                    s = buf;
                    i = i3 + 1;
                    s[i] = 'u';
                    s = buf;
                    i++;
                    s[i] = '2';
                    s = buf;
                    i++;
                    s[i] = '0';
                    s = buf;
                    i++;
                    s[i] = '2';
                    buf[i + 1] = '8';
                } else
                if (j2 < IOUtils.specicalFlags_doubleQuotes.length && IOUtils.specicalFlags_doubleQuotes[j2] == 4)
                {
                    System.arraycopy(buf, i3 + 1, buf, i3 + 6, k3 - i3 - 1);
                    s = buf;
                    i = i3 + 1;
                    s[i3] = '\\';
                    s = buf;
                    j1 = i + 1;
                    s[i] = 'u';
                    s = buf;
                    i = j1 + 1;
                    s[j1] = IOUtils.DIGITS[j2 >>> 12 & 0xf];
                    s = buf;
                    j1 = i + 1;
                    s[i] = IOUtils.DIGITS[j2 >>> 8 & 0xf];
                    s = buf;
                    i = j1 + 1;
                    s[j1] = IOUtils.DIGITS[j2 >>> 4 & 0xf];
                    buf[i] = IOUtils.DIGITS[j2 & 0xf];
                } else
                {
                    System.arraycopy(buf, i3 + 1, buf, i3 + 2, k3 - i3 - 1);
                    buf[i3] = '\\';
                    buf[i3 + 1] = IOUtils.replaceChars[j2];
                }
            } else
            if (l2 > 1)
            {
                k1 = j1 - j3;
                i = j1;
                j1 = k3;
                while (k1 < s.length()) 
                {
                    c1 = s.charAt(k1);
                    if (c1 < IOUtils.specicalFlags_doubleQuotes.length && IOUtils.specicalFlags_doubleQuotes[c1] != 0 || c1 == '/' && isEnabled(SerializerFeature.WriteSlashAsSpecial))
                    {
                        char ac[] = buf;
                        j2 = i + 1;
                        ac[i] = '\\';
                        if (IOUtils.specicalFlags_doubleQuotes[c1] == 4)
                        {
                            char ac1[] = buf;
                            i = j2 + 1;
                            ac1[j2] = 'u';
                            ac1 = buf;
                            j2 = i + 1;
                            ac1[i] = IOUtils.DIGITS[c1 >>> 12 & 0xf];
                            ac1 = buf;
                            i = j2 + 1;
                            ac1[j2] = IOUtils.DIGITS[c1 >>> 8 & 0xf];
                            ac1 = buf;
                            j2 = i + 1;
                            ac1[i] = IOUtils.DIGITS[c1 >>> 4 & 0xf];
                            ac1 = buf;
                            i = j2 + 1;
                            ac1[j2] = IOUtils.DIGITS[c1 & 0xf];
                            j1 += 5;
                        } else
                        {
                            char ac2[] = buf;
                            i = j2 + 1;
                            ac2[j2] = IOUtils.replaceChars[c1];
                            j1++;
                        }
                    } else
                    if (c1 == '\u2028')
                    {
                        char ac3[] = buf;
                        j2 = i + 1;
                        ac3[i] = '\\';
                        ac3 = buf;
                        i = j2 + 1;
                        ac3[j2] = 'u';
                        ac3 = buf;
                        j2 = i + 1;
                        ac3[i] = IOUtils.DIGITS[c1 >>> 12 & 0xf];
                        ac3 = buf;
                        i = j2 + 1;
                        ac3[j2] = IOUtils.DIGITS[c1 >>> 8 & 0xf];
                        ac3 = buf;
                        j2 = i + 1;
                        ac3[i] = IOUtils.DIGITS[c1 >>> 4 & 0xf];
                        ac3 = buf;
                        i = j2 + 1;
                        ac3[j2] = IOUtils.DIGITS[c1 & 0xf];
                        j1 += 5;
                    } else
                    {
                        buf[i] = c1;
                        i++;
                    }
                    k1++;
                }
            }
        }
_L9:
        if (c != 0)
        {
            buf[count - 2] = '"';
            buf[count - 1] = c;
            return;
        } else
        {
            buf[count - 1] = '"';
            return;
        }
_L15:
        i3 = l2;
        l2 = j2;
        j2 = i;
        char c6 = c1;
        k1 = j1;
        j1 = c6;
          goto _L17
        int k4 = j1;
        j1 = j2;
        j2 = i3;
        i3 = k1;
        k1 = k4;
          goto _L17
    }

    private void writeStringWithSingleQuote(String s)
    {
        char c1 = '\0';
        int i = 0;
        if (s == null)
        {
            i = count + 4;
            if (i > buf.length)
            {
                expandCapacity(i);
            }
            "null".getChars(0, 4, buf, count);
            count = i;
            return;
        }
        int j = s.length();
        int l1 = count + j + 2;
        if (l1 > buf.length)
        {
            if (writer != null)
            {
                write('\'');
                while (i < s.length()) 
                {
                    char c = s.charAt(i);
                    if (c <= '\r' || c == '\\' || c == '\'' || c == '/' && isEnabled(SerializerFeature.WriteSlashAsSpecial))
                    {
                        write('\\');
                        write(IOUtils.replaceChars[c]);
                    } else
                    {
                        write(c);
                    }
                    i++;
                }
                write('\'');
                return;
            }
            expandCapacity(l1);
        }
        int j1 = count + 1;
        int k1 = j1 + j;
        buf[count] = '\'';
        s.getChars(0, j, buf, j1);
        count = l1;
        int k = -1;
        i = j1;
        j = 0;
        while (i < k1) 
        {
            int l = buf[i];
            if (l <= '\r' || l == '\\' || l == '\'' || l == '/' && isEnabled(SerializerFeature.WriteSlashAsSpecial))
            {
                k = j + 1;
                c1 = l;
                j = i;
            } else
            {
                int i1 = j;
                j = k;
                k = i1;
            }
            i++;
            l = k;
            k = j;
            j = l;
        }
        i = l1 + j;
        if (i > buf.length)
        {
            expandCapacity(i);
        }
        count = i;
        if (j == 1)
        {
            System.arraycopy(buf, k + 1, buf, k + 2, k1 - k - 1);
            buf[k] = '\\';
            buf[k + 1] = IOUtils.replaceChars[c1];
        } else
        if (j > 1)
        {
            System.arraycopy(buf, k + 1, buf, k + 2, k1 - k - 1);
            buf[k] = '\\';
            s = buf;
            i = k + 1;
            s[i] = IOUtils.replaceChars[c1];
            j = k1 + 1;
            i -= 2;
            while (i >= j1) 
            {
label0:
                {
                    c1 = buf[i];
                    if (c1 > '\r' && c1 != '\\' && c1 != '\'')
                    {
                        k = j;
                        if (c1 != '/')
                        {
                            break label0;
                        }
                        k = j;
                        if (!isEnabled(SerializerFeature.WriteSlashAsSpecial))
                        {
                            break label0;
                        }
                    }
                    System.arraycopy(buf, i + 1, buf, i + 2, j - i - 1);
                    buf[i] = '\\';
                    buf[i + 1] = IOUtils.replaceChars[c1];
                    k = j + 1;
                }
                i--;
                j = k;
            }
        }
        buf[count - 1] = '\'';
        return;
        if (true) goto _L2; else goto _L1
_L2:
        break MISSING_BLOCK_LABEL_401;
_L1:
    }

    public SerializeWriter append(char c)
    {
        write(c);
        return this;
    }

    public SerializeWriter append(CharSequence charsequence)
    {
        if (charsequence == null)
        {
            charsequence = "null";
        } else
        {
            charsequence = charsequence.toString();
        }
        write(charsequence, 0, charsequence.length());
        return this;
    }

    public SerializeWriter append(CharSequence charsequence, int i, int j)
    {
        Object obj = charsequence;
        if (charsequence == null)
        {
            obj = "null";
        }
        charsequence = ((CharSequence) (obj)).subSequence(i, j).toString();
        write(charsequence, 0, charsequence.length());
        return this;
    }

    public volatile Writer append(char c)
        throws IOException
    {
        return append(c);
    }

    public volatile Writer append(CharSequence charsequence)
        throws IOException
    {
        return append(charsequence);
    }

    public volatile Writer append(CharSequence charsequence, int i, int j)
        throws IOException
    {
        return append(charsequence, i, j);
    }

    public volatile Appendable append(char c)
        throws IOException
    {
        return append(c);
    }

    public volatile Appendable append(CharSequence charsequence)
        throws IOException
    {
        return append(charsequence);
    }

    public volatile Appendable append(CharSequence charsequence, int i, int j)
        throws IOException
    {
        return append(charsequence, i, j);
    }

    public void close()
    {
        if (writer != null && count > 0)
        {
            flush();
        }
        if (buf.length <= 8192)
        {
            bufLocal.set(new SoftReference(buf));
        }
        buf = null;
    }

    public void config(SerializerFeature serializerfeature, boolean flag)
    {
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_67;
        }
        features = features | serializerfeature.getMask();
        if (serializerfeature != SerializerFeature.WriteEnumUsingToString) goto _L2; else goto _L1
_L1:
        features = features & ~SerializerFeature.WriteEnumUsingName.getMask();
_L4:
        return;
_L2:
        if (serializerfeature != SerializerFeature.WriteEnumUsingName) goto _L4; else goto _L3
_L3:
        features = features & ~SerializerFeature.WriteEnumUsingToString.getMask();
        return;
        features = features & ~serializerfeature.getMask();
        return;
    }

    public void expandCapacity(int i)
    {
        int j = (buf.length * 3) / 2 + 1;
        char ac[];
        if (j >= i)
        {
            i = j;
        }
        ac = new char[i];
        System.arraycopy(buf, 0, ac, 0, count);
        buf = ac;
    }

    public void flush()
    {
        if (writer == null)
        {
            return;
        }
        try
        {
            writer.write(buf, 0, count);
            writer.flush();
        }
        catch (IOException ioexception)
        {
            throw new JSONException(ioexception.getMessage(), ioexception);
        }
        count = 0;
    }

    public int getBufferLength()
    {
        return buf.length;
    }

    public boolean isEnabled(SerializerFeature serializerfeature)
    {
        return SerializerFeature.isEnabled(features, serializerfeature);
    }

    public void reset()
    {
        count = 0;
    }

    public int size()
    {
        return count;
    }

    public byte[] toBytes(String s)
    {
        if (writer != null)
        {
            throw new UnsupportedOperationException("writer not null");
        }
        String s1 = s;
        if (s == null)
        {
            s1 = "UTF-8";
        }
        return (new SerialWriterStringEncoder(Charset.forName(s1))).encode(buf, 0, count);
    }

    public char[] toCharArray()
    {
        if (writer != null)
        {
            throw new UnsupportedOperationException("writer not null");
        } else
        {
            char ac[] = new char[count];
            System.arraycopy(buf, 0, ac, 0, count);
            return ac;
        }
    }

    public String toString()
    {
        return new String(buf, 0, count);
    }

    public void write(char c)
    {
        int j = count + 1;
        int i = j;
        if (j > buf.length)
        {
            if (writer == null)
            {
                expandCapacity(j);
                i = j;
            } else
            {
                flush();
                i = 1;
            }
        }
        buf[count] = c;
        count = i;
    }

    public void write(int i)
    {
        int k = count + 1;
        int j = k;
        if (k > buf.length)
        {
            if (writer == null)
            {
                expandCapacity(k);
                j = k;
            } else
            {
                flush();
                j = 1;
            }
        }
        buf[count] = (char)i;
        count = j;
    }

    public void write(String s)
    {
        if (s == null)
        {
            writeNull();
            return;
        } else
        {
            write(s, 0, s.length());
            return;
        }
    }

    public void write(String s, int i, int j)
    {
        int i1 = count + j;
        int k;
        int l;
        if (i1 > buf.length)
        {
            k = i;
            l = j;
            if (writer == null)
            {
                expandCapacity(i1);
                k = i1;
                l = i;
            } else
            {
                do
                {
                    j = buf.length - count;
                    s.getChars(k, k + j, buf, count);
                    count = buf.length;
                    flush();
                    i = l - j;
                    i1 = k + j;
                    k = i1;
                    l = i;
                } while (i > buf.length);
                j = i;
                l = i1;
                k = i;
            }
        } else
        {
            k = i1;
            l = i;
        }
        s.getChars(l, j + l, buf, count);
        count = k;
    }

    public void write(boolean flag)
    {
        if (flag)
        {
            write("true");
            return;
        } else
        {
            write("false");
            return;
        }
    }

    public void write(char ac[], int i, int j)
    {
        if (i < 0 || i > ac.length || j < 0 || i + j > ac.length || i + j < 0)
        {
            throw new IndexOutOfBoundsException();
        }
        if (j == 0)
        {
            return;
        }
        int i1 = count + j;
        int k;
        int l;
        if (i1 > buf.length)
        {
            k = i;
            l = j;
            if (writer == null)
            {
                expandCapacity(i1);
                k = i1;
                l = i;
            } else
            {
                do
                {
                    j = buf.length - count;
                    System.arraycopy(ac, k, buf, count, j);
                    count = buf.length;
                    flush();
                    i = l - j;
                    i1 = k + j;
                    k = i1;
                    l = i;
                } while (i > buf.length);
                j = i;
                l = i1;
                k = i;
            }
        } else
        {
            k = i1;
            l = i;
        }
        System.arraycopy(ac, l, buf, count, j);
        count = k;
    }

    public void writeBooleanAndChar(boolean flag, char c)
    {
        if (flag)
        {
            if (c == ',')
            {
                write("true,");
                return;
            }
            if (c == ']')
            {
                write("true]");
                return;
            } else
            {
                write("true");
                write(c);
                return;
            }
        }
        if (c == ',')
        {
            write("false,");
            return;
        }
        if (c == ']')
        {
            write("false]");
            return;
        } else
        {
            write("false");
            write(c);
            return;
        }
    }

    public void writeByteArray(byte abyte0[])
    {
        int l = 0;
        int k = 0;
        int i1 = abyte0.length;
        boolean flag = isEnabled(SerializerFeature.UseSingleQuotes);
        char c;
        if (flag)
        {
            c = '\'';
        } else
        {
            c = '"';
        }
        if (i1 == 0)
        {
            if (flag)
            {
                abyte0 = "''";
            } else
            {
                abyte0 = "\"\"";
            }
            write(abyte0);
            return;
        }
        char ac[] = Base64.CA;
        int j1 = (i1 / 3) * 3;
        int i = (i1 - 1) / 3;
        int l1 = count;
        int k1 = (i + 1 << 2) + count + 2;
        if (k1 > buf.length)
        {
            if (writer != null)
            {
                write(c);
                for (i = 0; i < j1;)
                {
                    l1 = i + 1;
                    l = abyte0[i];
                    k1 = l1 + 1;
                    l1 = abyte0[l1];
                    i = k1 + 1;
                    l = (l1 & 0xff) << 8 | (l & 0xff) << 16 | abyte0[k1] & 0xff;
                    write(ac[l >>> 18 & 0x3f]);
                    write(ac[l >>> 12 & 0x3f]);
                    write(ac[l >>> 6 & 0x3f]);
                    write(ac[l & 0x3f]);
                }

                l = i1 - j1;
                if (l > 0)
                {
                    j1 = abyte0[j1];
                    i = k;
                    if (l == 2)
                    {
                        i = (abyte0[i1 - 1] & 0xff) << 2;
                    }
                    i |= (j1 & 0xff) << 10;
                    write(ac[i >> 12]);
                    write(ac[i >>> 6 & 0x3f]);
                    char c1;
                    if (l == 2)
                    {
                        c1 = ac[i & 0x3f];
                    } else
                    {
                        c1 = '=';
                    }
                    write(c1);
                    write('=');
                }
                write(c);
                return;
            }
            expandCapacity(k1);
        }
        count = k1;
        char ac1[] = buf;
        i = l1 + 1;
        ac1[l1] = c;
        for (k = 0; k < j1;)
        {
            int k2 = k + 1;
            int i2 = abyte0[k];
            int j2 = k2 + 1;
            k2 = abyte0[k2];
            k = j2 + 1;
            i2 = (k2 & 0xff) << 8 | (i2 & 0xff) << 16 | abyte0[j2] & 0xff;
            char ac2[] = buf;
            j2 = i + 1;
            ac2[i] = ac[i2 >>> 18 & 0x3f];
            ac2 = buf;
            i = j2 + 1;
            ac2[j2] = ac[i2 >>> 12 & 0x3f];
            ac2 = buf;
            j2 = i + 1;
            ac2[i] = ac[i2 >>> 6 & 0x3f];
            ac2 = buf;
            i = j2 + 1;
            ac2[j2] = ac[i2 & 0x3f];
        }

        k = i1 - j1;
        if (k > 0)
        {
            j1 = abyte0[j1];
            int j = l;
            if (k == 2)
            {
                j = (abyte0[i1 - 1] & 0xff) << 2;
            }
            j |= (j1 & 0xff) << 10;
            buf[k1 - 5] = ac[j >> 12];
            buf[k1 - 4] = ac[j >>> 6 & 0x3f];
            abyte0 = buf;
            byte byte0;
            if (k == 2)
            {
                byte0 = ac[j & 0x3f];
            } else
            {
                byte0 = 61;
            }
            abyte0[k1 - 3] = byte0;
            buf[k1 - 2] = '=';
        }
        buf[k1 - 1] = c;
    }

    public void writeCharacterAndChar(char c, char c1)
    {
        writeString(Character.toString(c));
        write(c1);
    }

    public void writeDoubleAndChar(double d, char c)
    {
        String s1 = Double.toString(d);
        String s = s1;
        if (s1.endsWith(".0"))
        {
            s = s1.substring(0, s1.length() - 2);
        }
        write(s);
        write(c);
    }

    public void writeEnum(Enum enum, char c)
    {
        if (enum == null)
        {
            writeNull();
            write(',');
            return;
        }
        if (isEnabled(SerializerFeature.WriteEnumUsingName))
        {
            writeEnumValue(enum.name(), c);
            return;
        }
        if (isEnabled(SerializerFeature.WriteEnumUsingToString))
        {
            writeEnumValue(enum.toString(), c);
            return;
        } else
        {
            writeIntAndChar(enum.ordinal(), c);
            return;
        }
    }

    public void writeFieldEmptyList(char c, String s)
    {
        write(c);
        writeFieldName(s);
        write("[]");
    }

    public void writeFieldName(String s)
    {
        writeFieldName(s, false);
    }

    public void writeFieldName(String s, boolean flag)
    {
        if (s == null)
        {
            write("null:");
            return;
        }
        if (isEnabled(SerializerFeature.UseSingleQuotes))
        {
            if (isEnabled(SerializerFeature.QuoteFieldNames))
            {
                writeStringWithSingleQuote(s);
                write(':');
                return;
            } else
            {
                writeKeyWithSingleQuoteIfHasSpecial(s);
                return;
            }
        }
        if (isEnabled(SerializerFeature.QuoteFieldNames))
        {
            writeStringWithDoubleQuote(s, ':', flag);
            return;
        } else
        {
            writeKeyWithDoubleQuoteIfHasSpecial(s);
            return;
        }
    }

    public void writeFieldNull(char c, String s)
    {
        write(c);
        writeFieldName(s);
        writeNull();
    }

    public void writeFieldNullBoolean(char c, String s)
    {
        write(c);
        writeFieldName(s);
        if (isEnabled(SerializerFeature.WriteNullBooleanAsFalse))
        {
            write("false");
            return;
        } else
        {
            writeNull();
            return;
        }
    }

    public void writeFieldNullList(char c, String s)
    {
        write(c);
        writeFieldName(s);
        if (isEnabled(SerializerFeature.WriteNullListAsEmpty))
        {
            write("[]");
            return;
        } else
        {
            writeNull();
            return;
        }
    }

    public void writeFieldNullNumber(char c, String s)
    {
        write(c);
        writeFieldName(s);
        if (isEnabled(SerializerFeature.WriteNullNumberAsZero))
        {
            write('0');
            return;
        } else
        {
            writeNull();
            return;
        }
    }

    public void writeFieldNullString(char c, String s)
    {
        write(c);
        writeFieldName(s);
        if (isEnabled(SerializerFeature.WriteNullStringAsEmpty))
        {
            writeString("");
            return;
        } else
        {
            writeNull();
            return;
        }
    }

    public void writeFieldValue(char c, String s, char c1)
    {
        write(c);
        writeFieldName(s);
        if (c1 == 0)
        {
            writeString("\0");
            return;
        } else
        {
            writeString(Character.toString(c1));
            return;
        }
    }

    public void writeFieldValue(char c, String s, double d)
    {
        write(c);
        writeFieldName(s);
        if (d == 0.0D)
        {
            write('0');
            return;
        }
        if (Double.isNaN(d))
        {
            writeNull();
            return;
        }
        if (Double.isInfinite(d))
        {
            writeNull();
            return;
        }
        String s1 = Double.toString(d);
        s = s1;
        if (s1.endsWith(".0"))
        {
            s = s1.substring(0, s1.length() - 2);
        }
        write(s);
    }

    public void writeFieldValue(char c, String s, float f)
    {
        write(c);
        writeFieldName(s);
        if (f == 0.0F)
        {
            write('0');
            return;
        }
        if (Float.isNaN(f))
        {
            writeNull();
            return;
        }
        if (Float.isInfinite(f))
        {
            writeNull();
            return;
        }
        String s1 = Float.toString(f);
        s = s1;
        if (s1.endsWith(".0"))
        {
            s = s1.substring(0, s1.length() - 2);
        }
        write(s);
    }

    public void writeFieldValue(char c, String s, int i)
    {
        if (i == 0x80000000 || !isEnabled(SerializerFeature.QuoteFieldNames))
        {
            writeFieldValue1(c, s, i);
            return;
        }
        char c1;
        int j;
        int k;
        int l;
        if (isEnabled(SerializerFeature.UseSingleQuotes))
        {
            c1 = '\'';
        } else
        {
            c1 = '"';
        }
        if (i < 0)
        {
            j = IOUtils.stringSize(-i) + 1;
        } else
        {
            j = IOUtils.stringSize(i);
        }
        k = s.length();
        l = j + (count + k + 4);
        if (l > buf.length)
        {
            if (writer != null)
            {
                writeFieldValue1(c, s, i);
                return;
            }
            expandCapacity(l);
        }
        j = count;
        count = l;
        buf[j] = c;
        l = j + k + 1;
        buf[j + 1] = c1;
        s.getChars(0, k, buf, j + 2);
        buf[l + 1] = c1;
        buf[l + 2] = ':';
        IOUtils.getChars(i, count, buf);
    }

    public void writeFieldValue(char c, String s, long l)
    {
        if (l == 0x8000000000000000L || !isEnabled(SerializerFeature.QuoteFieldNames))
        {
            writeFieldValue1(c, s, l);
            return;
        }
        char c1;
        int i;
        int j;
        int k;
        if (isEnabled(SerializerFeature.UseSingleQuotes))
        {
            c1 = '\'';
        } else
        {
            c1 = '"';
        }
        if (l < 0L)
        {
            i = IOUtils.stringSize(-l) + 1;
        } else
        {
            i = IOUtils.stringSize(l);
        }
        j = s.length();
        k = i + (count + j + 4);
        if (k > buf.length)
        {
            if (writer != null)
            {
                write(c);
                writeFieldName(s);
                writeLong(l);
                return;
            }
            expandCapacity(k);
        }
        i = count;
        count = k;
        buf[i] = c;
        k = i + j + 1;
        buf[i + 1] = c1;
        s.getChars(0, j, buf, i + 2);
        buf[k + 1] = c1;
        buf[k + 2] = ':';
        IOUtils.getChars(l, count, buf);
    }

    public void writeFieldValue(char c, String s, Enum enum)
    {
        if (enum == null)
        {
            write(c);
            writeFieldName(s);
            writeNull();
            return;
        }
        if (isEnabled(SerializerFeature.WriteEnumUsingName))
        {
            writeEnumFieldValue(c, s, enum.name());
            return;
        }
        if (isEnabled(SerializerFeature.WriteEnumUsingToString))
        {
            writeEnumFieldValue(c, s, enum.toString());
            return;
        } else
        {
            writeFieldValue(c, s, enum.ordinal());
            return;
        }
    }

    public void writeFieldValue(char c, String s, String s1)
    {
        if (isEnabled(SerializerFeature.QuoteFieldNames))
        {
            if (isEnabled(SerializerFeature.UseSingleQuotes))
            {
                write(c);
                writeFieldName(s);
                if (s1 == null)
                {
                    writeNull();
                    return;
                } else
                {
                    writeString(s1);
                    return;
                }
            }
            if (isEnabled(SerializerFeature.BrowserSecure))
            {
                write(c);
                writeStringWithDoubleQuote(s, ':');
                writeStringWithDoubleQuote(s1, '\0');
                return;
            }
            if (isEnabled(SerializerFeature.BrowserCompatible))
            {
                write(c);
                writeStringWithDoubleQuote(s, ':');
                writeStringWithDoubleQuote(s1, '\0');
                return;
            } else
            {
                writeFieldValueStringWithDoubleQuote(c, s, s1, true);
                return;
            }
        }
        write(c);
        writeFieldName(s);
        if (s1 == null)
        {
            writeNull();
            return;
        } else
        {
            writeString(s1);
            return;
        }
    }

    public void writeFieldValue(char c, String s, BigDecimal bigdecimal)
    {
        write(c);
        writeFieldName(s);
        if (bigdecimal == null)
        {
            writeNull();
            return;
        } else
        {
            write(bigdecimal.toString());
            return;
        }
    }

    public void writeFieldValue(char c, String s, boolean flag)
    {
        char c1;
        int i;
        int j;
        int k;
        if (isEnabled(SerializerFeature.UseSingleQuotes))
        {
            c1 = '\'';
        } else
        {
            c1 = '"';
        }
        if (flag)
        {
            i = 4;
        } else
        {
            i = 5;
        }
        j = s.length();
        k = i + (count + j + 4);
        if (k > buf.length)
        {
            if (writer != null)
            {
                write(c);
                writeString(s);
                write(':');
                write(flag);
                return;
            }
            expandCapacity(k);
        }
        i = count;
        count = k;
        buf[i] = c;
        k = i + j + 1;
        buf[i + 1] = c1;
        s.getChars(0, j, buf, i + 2);
        buf[k + 1] = c1;
        if (flag)
        {
            System.arraycopy(":true".toCharArray(), 0, buf, k + 2, 5);
            return;
        } else
        {
            System.arraycopy(":false".toCharArray(), 0, buf, k + 2, 6);
            return;
        }
    }

    public void writeFieldValue1(char c, String s, int i)
    {
        write(c);
        writeFieldName(s);
        writeInt(i);
    }

    public void writeFieldValue1(char c, String s, long l)
    {
        write(c);
        writeFieldName(s);
        writeLong(l);
    }

    public void writeFloatAndChar(float f, char c)
    {
        String s1 = Float.toString(f);
        String s = s1;
        if (s1.endsWith(".0"))
        {
            s = s1.substring(0, s1.length() - 2);
        }
        write(s);
        write(c);
    }

    public void writeInt(int i)
    {
        int j;
label0:
        {
            if (i == 0x80000000)
            {
                write("-2147483648");
                return;
            }
            int k;
            if (i < 0)
            {
                j = IOUtils.stringSize(-i) + 1;
            } else
            {
                j = IOUtils.stringSize(i);
            }
            k = count + j;
            if (k > buf.length)
            {
                if (writer != null)
                {
                    break label0;
                }
                expandCapacity(k);
            }
            IOUtils.getChars(i, k, buf);
            count = k;
            return;
        }
        char ac[] = new char[j];
        IOUtils.getChars(i, j, ac);
        write(ac, 0, ac.length);
    }

    public void writeIntAndChar(int i, char c)
    {
        if (i == 0x80000000)
        {
            write("-2147483648");
            write(c);
            return;
        }
        int j;
        int k;
        if (i < 0)
        {
            j = IOUtils.stringSize(-i) + 1;
        } else
        {
            j = IOUtils.stringSize(i);
        }
        j += count;
        k = j + 1;
        if (k > buf.length)
        {
            if (writer != null)
            {
                writeInt(i);
                write(c);
                return;
            }
            expandCapacity(k);
        }
        IOUtils.getChars(i, j, buf);
        buf[j] = c;
        count = k;
    }

    public void writeLong(long l)
    {
        int i;
label0:
        {
            if (l == 0x8000000000000000L)
            {
                write("-9223372036854775808");
                return;
            }
            int j;
            if (l < 0L)
            {
                i = IOUtils.stringSize(-l) + 1;
            } else
            {
                i = IOUtils.stringSize(l);
            }
            j = count + i;
            if (j > buf.length)
            {
                if (writer != null)
                {
                    break label0;
                }
                expandCapacity(j);
            }
            IOUtils.getChars(l, j, buf);
            count = j;
            return;
        }
        char ac[] = new char[i];
        IOUtils.getChars(l, i, ac);
        write(ac, 0, ac.length);
    }

    public void writeLongAndChar(long l, char c)
        throws IOException
    {
        if (l == 0x8000000000000000L)
        {
            write("-9223372036854775808");
            write(c);
            return;
        }
        int i;
        int j;
        if (l < 0L)
        {
            i = IOUtils.stringSize(-l) + 1;
        } else
        {
            i = IOUtils.stringSize(l);
        }
        i += count;
        j = i + 1;
        if (j > buf.length)
        {
            if (writer != null)
            {
                writeLong(l);
                write(c);
                return;
            }
            expandCapacity(j);
        }
        IOUtils.getChars(l, i, buf);
        buf[i] = c;
        count = j;
    }

    public void writeNull()
    {
        write("null");
    }

    public void writeString(String s)
    {
        if (isEnabled(SerializerFeature.UseSingleQuotes))
        {
            writeStringWithSingleQuote(s);
            return;
        } else
        {
            writeStringWithDoubleQuote(s, '\0');
            return;
        }
    }

    public void writeString(String s, char c)
    {
        if (isEnabled(SerializerFeature.UseSingleQuotes))
        {
            writeStringWithSingleQuote(s);
            write(c);
            return;
        } else
        {
            writeStringWithDoubleQuote(s, c);
            return;
        }
    }

    public void writeTo(OutputStream outputstream, String s)
        throws IOException
    {
        writeTo(outputstream, Charset.forName(s));
    }

    public void writeTo(OutputStream outputstream, Charset charset)
        throws IOException
    {
        if (writer != null)
        {
            throw new UnsupportedOperationException("writer not null");
        } else
        {
            outputstream.write((new String(buf, 0, count)).getBytes(charset));
            return;
        }
    }

    public void writeTo(Writer writer1)
        throws IOException
    {
        if (writer != null)
        {
            throw new UnsupportedOperationException("writer not null");
        } else
        {
            writer1.write(buf, 0, count);
            return;
        }
    }

}
