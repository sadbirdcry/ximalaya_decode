// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class FloatArraySerializer
    implements ObjectSerializer
{

    public static final FloatArraySerializer instance = new FloatArraySerializer();

    public FloatArraySerializer()
    {
    }

    public final void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer = jsonserializer.getWriter();
        if (obj == null)
        {
            if (jsonserializer.isEnabled(SerializerFeature.WriteNullListAsEmpty))
            {
                jsonserializer.write("[]");
                return;
            } else
            {
                jsonserializer.writeNull();
                return;
            }
        }
        obj = (float[])(float[])obj;
        int j = obj.length - 1;
        if (j == -1)
        {
            jsonserializer.append("[]");
            return;
        }
        jsonserializer.append('[');
        i = 0;
        while (i < j) 
        {
            float f = obj[i];
            if (Float.isNaN(f))
            {
                jsonserializer.writeNull();
            } else
            {
                jsonserializer.append(Float.toString(f));
            }
            jsonserializer.append(',');
            i++;
        }
        float f1 = obj[j];
        if (Float.isNaN(f1))
        {
            jsonserializer.writeNull();
        } else
        {
            jsonserializer.append(Float.toString(f1));
        }
        jsonserializer.append(']');
    }

}
