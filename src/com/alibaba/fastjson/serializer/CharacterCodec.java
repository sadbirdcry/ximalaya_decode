// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter

public class CharacterCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final CharacterCodec instance = new CharacterCodec();

    public CharacterCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        defaultjsonparser = ((DefaultJSONParser) (defaultjsonparser.parse()));
        if (defaultjsonparser == null)
        {
            return null;
        } else
        {
            return TypeUtils.castToChar(defaultjsonparser);
        }
    }

    public int getFastMatchToken()
    {
        return 4;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer = jsonserializer.getWriter();
        obj = (Character)obj;
        if (obj == null)
        {
            jsonserializer.writeString("");
            return;
        }
        if (((Character) (obj)).charValue() == 0)
        {
            jsonserializer.writeString("\0");
            return;
        } else
        {
            jsonserializer.writeString(((Character) (obj)).toString());
            return;
        }
    }

}
