// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import java.awt.Point;
import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter, SerializerFeature

public class PointCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final PointCodec instance = new PointCodec();

    public PointCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        int i;
        int j;
        j = 0;
        type = defaultjsonparser.getLexer();
        if (type.token() == 8)
        {
            type.nextToken(16);
            return null;
        }
        if (type.token() != 12 && type.token() != 16)
        {
            throw new JSONException("syntax error");
        }
        type.nextToken();
        i = 0;
_L10:
        if (type.token() == 13)
        {
            type.nextToken();
            return new Point(i, j);
        }
        if (type.token() != 4) goto _L2; else goto _L1
_L1:
        obj = type.stringVal();
        if (JSON.DEFAULT_TYPE_KEY.equals(obj))
        {
            defaultjsonparser.acceptType("java.awt.Point");
            continue; /* Loop/switch isn't completed */
        }
        type.nextTokenWithColon(2);
        if (type.token() != 2) goto _L4; else goto _L3
_L3:
        int k;
        k = type.intValue();
        type.nextToken();
        if (!((String) (obj)).equalsIgnoreCase("x")) goto _L6; else goto _L5
_L5:
        int l;
        l = k;
        k = j;
_L8:
        j = k;
        i = l;
        if (type.token() == 16)
        {
            type.nextToken(4);
            j = k;
            i = l;
        }
        continue; /* Loop/switch isn't completed */
_L2:
        throw new JSONException("syntax error");
_L4:
        throw new JSONException((new StringBuilder()).append("syntax error : ").append(type.tokenName()).toString());
_L6:
        if (!((String) (obj)).equalsIgnoreCase("y"))
        {
            break; /* Loop/switch isn't completed */
        }
        l = i;
        if (true) goto _L8; else goto _L7
_L7:
        throw new JSONException((new StringBuilder()).append("syntax error, ").append(((String) (obj))).toString());
        if (true) goto _L10; else goto _L9
_L9:
    }

    public int getFastMatchToken()
    {
        return 12;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        char c = '{';
        jsonserializer = jsonserializer.getWriter();
        obj = (Point)obj;
        if (obj == null)
        {
            jsonserializer.writeNull();
            return;
        }
        if (jsonserializer.isEnabled(SerializerFeature.WriteClassName))
        {
            jsonserializer.write('{');
            jsonserializer.writeFieldName(JSON.DEFAULT_TYPE_KEY);
            jsonserializer.writeString(java/awt/Point.getName());
            c = ',';
        }
        jsonserializer.writeFieldValue(c, "x", ((Point) (obj)).getX());
        jsonserializer.writeFieldValue(',', "y", ((Point) (obj)).getY());
        jsonserializer.write('}');
    }

}
