// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONAware;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONStreamAware;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.alibaba.fastjson.parser.deserializer.Jdk8DateCodec;
import com.alibaba.fastjson.util.ASMUtils;
import com.alibaba.fastjson.util.IdentityHashMap;
import com.alibaba.fastjson.util.ServiceLoader;
import java.io.File;
import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.Clob;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Currency;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicLongArray;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ASMSerializerFactory, BooleanCodec, CharacterCodec, IntegerCodec, 
//            LongCodec, FloatCodec, DoubleSerializer, BigDecimalCodec, 
//            BigIntegerCodec, StringCodec, ByteArraySerializer, ShortArraySerializer, 
//            IntArraySerializer, LongArraySerializer, FloatArraySerializer, DoubleArraySerializer, 
//            BooleanArraySerializer, CharArraySerializer, ObjectArraySerializer, ClassSerializer, 
//            DateFormatSerializer, LocaleCodec, CurrencyCodec, TimeZoneCodec, 
//            UUIDCodec, InetAddressCodec, InetSocketAddressCodec, FileCodec, 
//            URICodec, URLCodec, AppendableSerializer, PatternCodec, 
//            CharsetCodec, AtomicBooleanSerializer, AtomicIntegerSerializer, AtomicLongSerializer, 
//            ReferenceCodec, AtomicIntegerArrayCodec, AtomicLongArrayCodec, ColorCodec, 
//            FontCodec, PointCodec, RectangleCodec, JavaBeanSerializer, 
//            ObjectSerializer, AutowiredObjectSerializer, MapSerializer, ListSerializer, 
//            CollectionSerializer, DateSerializer, JSONAwareSerializer, JSONSerializable, 
//            JSONSerializableSerializer, JSONStreamAwareSerializer, EnumSerializer, ArraySerializer, 
//            ExceptionSerializer, EnumerationSeriliazer, CalendarCodec, ClobSeriliazer

public class SerializeConfig extends IdentityHashMap
{

    private static final SerializeConfig globalInstance = new SerializeConfig();
    private boolean asm;
    private ASMSerializerFactory asmFactory;
    private String typeKey;

    public SerializeConfig()
    {
        this(1024);
    }

    public SerializeConfig(int i)
    {
        super(i);
        boolean flag;
        if (!ASMUtils.isAndroid())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        asm = flag;
        typeKey = JSON.DEFAULT_TYPE_KEY;
        try
        {
            asmFactory = new ASMSerializerFactory();
        }
        catch (NoClassDefFoundError noclassdeffounderror)
        {
            asm = false;
        }
        catch (ExceptionInInitializerError exceptionininitializererror)
        {
            asm = false;
        }
        put(java/lang/Boolean, BooleanCodec.instance);
        put(java/lang/Character, CharacterCodec.instance);
        put(java/lang/Byte, IntegerCodec.instance);
        put(java/lang/Short, IntegerCodec.instance);
        put(java/lang/Integer, IntegerCodec.instance);
        put(java/lang/Long, LongCodec.instance);
        put(java/lang/Float, FloatCodec.instance);
        put(java/lang/Double, DoubleSerializer.instance);
        put(java/math/BigDecimal, BigDecimalCodec.instance);
        put(java/math/BigInteger, BigIntegerCodec.instance);
        put(java/lang/String, StringCodec.instance);
        put([B, ByteArraySerializer.instance);
        put([S, ShortArraySerializer.instance);
        put([I, IntArraySerializer.instance);
        put([J, LongArraySerializer.instance);
        put([F, FloatArraySerializer.instance);
        put([D, DoubleArraySerializer.instance);
        put([Z, BooleanArraySerializer.instance);
        put([C, CharArraySerializer.instance);
        put([Ljava/lang/Object;, ObjectArraySerializer.instance);
        put(java/lang/Class, ClassSerializer.instance);
        put(java/text/SimpleDateFormat, DateFormatSerializer.instance);
        put(java/util/Locale, LocaleCodec.instance);
        put(java/util/Currency, CurrencyCodec.instance);
        put(java/util/TimeZone, TimeZoneCodec.instance);
        put(java/util/UUID, UUIDCodec.instance);
        put(java/net/InetAddress, InetAddressCodec.instance);
        put(java/net/Inet4Address, InetAddressCodec.instance);
        put(java/net/Inet6Address, InetAddressCodec.instance);
        put(java/net/InetSocketAddress, InetSocketAddressCodec.instance);
        put(java/io/File, FileCodec.instance);
        put(java/net/URI, URICodec.instance);
        put(java/net/URL, URLCodec.instance);
        put(java/lang/Appendable, AppendableSerializer.instance);
        put(java/lang/StringBuffer, AppendableSerializer.instance);
        put(java/lang/StringBuilder, AppendableSerializer.instance);
        put(java/util/regex/Pattern, PatternCodec.instance);
        put(java/nio/charset/Charset, CharsetCodec.instance);
        put(java/util/concurrent/atomic/AtomicBoolean, AtomicBooleanSerializer.instance);
        put(java/util/concurrent/atomic/AtomicInteger, AtomicIntegerSerializer.instance);
        put(java/util/concurrent/atomic/AtomicLong, AtomicLongSerializer.instance);
        put(java/util/concurrent/atomic/AtomicReference, ReferenceCodec.instance);
        put(java/util/concurrent/atomic/AtomicIntegerArray, AtomicIntegerArrayCodec.instance);
        put(java/util/concurrent/atomic/AtomicLongArray, AtomicLongArrayCodec.instance);
        put(java/lang/ref/WeakReference, ReferenceCodec.instance);
        put(java/lang/ref/SoftReference, ReferenceCodec.instance);
        try
        {
            put(Class.forName("java.awt.Color"), ColorCodec.instance);
            put(Class.forName("java.awt.Font"), FontCodec.instance);
            put(Class.forName("java.awt.Point"), PointCodec.instance);
            put(Class.forName("java.awt.Rectangle"), RectangleCodec.instance);
        }
        catch (Throwable throwable1) { }
        try
        {
            put(Class.forName("java.time.LocalDateTime"), Jdk8DateCodec.instance);
            put(Class.forName("java.time.LocalDate"), Jdk8DateCodec.instance);
            put(Class.forName("java.time.LocalTime"), Jdk8DateCodec.instance);
            put(Class.forName("java.time.ZonedDateTime"), Jdk8DateCodec.instance);
            put(Class.forName("java.time.OffsetDateTime"), Jdk8DateCodec.instance);
            put(Class.forName("java.time.OffsetTime"), Jdk8DateCodec.instance);
            put(Class.forName("java.time.ZoneOffset"), Jdk8DateCodec.instance);
            put(Class.forName("java.time.ZoneRegion"), Jdk8DateCodec.instance);
            put(Class.forName("java.time.Period"), Jdk8DateCodec.instance);
            put(Class.forName("java.time.Duration"), Jdk8DateCodec.instance);
            put(Class.forName("java.time.Instant"), Jdk8DateCodec.instance);
            return;
        }
        catch (Throwable throwable)
        {
            return;
        }
    }

    public static SerializeConfig getGlobalInstance()
    {
        return globalInstance;
    }

    public final ObjectSerializer createASMSerializer(Class class1)
        throws Exception
    {
        return asmFactory.createJavaBeanSerializer(class1, null);
    }

    public ObjectSerializer createJavaBeanSerializer(Class class1)
    {
        boolean flag3 = false;
        if (Modifier.isPublic(class1.getModifiers())) goto _L2; else goto _L1
_L1:
        Object obj = new JavaBeanSerializer(class1);
_L6:
        return ((ObjectSerializer) (obj));
_L2:
        int i;
        int j;
        boolean flag2;
        flag2 = asm;
        if (flag2 && asmFactory.isExternalClass(class1) || class1 == java/io/Serializable || class1 == java/lang/Object)
        {
            flag2 = false;
        }
        obj = (JSONType)class1.getAnnotation(com/alibaba/fastjson/annotation/JSONType);
        boolean flag = flag2;
        if (obj != null)
        {
            flag = flag2;
            if (!((JSONType) (obj)).asm())
            {
                flag = false;
            }
        }
        flag2 = flag;
        if (flag)
        {
            flag2 = flag;
            if (!ASMUtils.checkName(class1.getName()))
            {
                flag2 = false;
            }
        }
        if (!flag2)
        {
            break MISSING_BLOCK_LABEL_251;
        }
        obj = class1.getDeclaredFields();
        j = obj.length;
        i = 0;
_L7:
        Object obj2;
        if (i >= j)
        {
            break MISSING_BLOCK_LABEL_251;
        }
        obj2 = (JSONField)obj[i].getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        if (obj2 == null || ASMUtils.checkName(((JSONField) (obj2)).name())) goto _L4; else goto _L3
_L3:
        boolean flag1 = flag3;
_L8:
        if (!flag1)
        {
            break; /* Loop/switch isn't completed */
        }
        obj2 = createASMSerializer(class1);
        obj = obj2;
        if (obj2 != null) goto _L6; else goto _L5
_L5:
        return new JavaBeanSerializer(class1);
_L4:
        i++;
          goto _L7
        Object obj1;
        obj1;
        throw new JSONException((new StringBuilder()).append("create asm serializer error, class ").append(class1).toString(), ((Throwable) (obj1)));
        obj1;
          goto _L5
        flag1 = flag2;
          goto _L8
    }

    public ObjectSerializer getObjectWriter(Class class1)
    {
        Object obj;
        Object obj1;
        boolean flag;
        flag = false;
        obj1 = (ObjectSerializer)get(class1);
        obj = obj1;
        if (obj1 == null)
        {
            try
            {
                obj = ServiceLoader.load(com/alibaba/fastjson/serializer/AutowiredObjectSerializer, Thread.currentThread().getContextClassLoader()).iterator();
                do
                {
                    if (!((Iterator) (obj)).hasNext())
                    {
                        break;
                    }
                    obj1 = ((Iterator) (obj)).next();
                    if (obj1 instanceof AutowiredObjectSerializer)
                    {
                        obj1 = (AutowiredObjectSerializer)obj1;
                        Iterator iterator = ((AutowiredObjectSerializer) (obj1)).getAutowiredFor().iterator();
                        while (iterator.hasNext()) 
                        {
                            put((Type)iterator.next(), obj1);
                        }
                    }
                } while (true);
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
            obj = (ObjectSerializer)get(class1);
        }
        obj1 = obj;
        if (obj == null)
        {
            ClassLoader classloader = com/alibaba/fastjson/JSON.getClassLoader();
            obj1 = obj;
            if (classloader != Thread.currentThread().getContextClassLoader())
            {
                try
                {
                    obj = ServiceLoader.load(com/alibaba/fastjson/serializer/AutowiredObjectSerializer, classloader).iterator();
                    do
                    {
                        if (!((Iterator) (obj)).hasNext())
                        {
                            break;
                        }
                        obj1 = ((Iterator) (obj)).next();
                        if (obj1 instanceof AutowiredObjectSerializer)
                        {
                            obj1 = (AutowiredObjectSerializer)obj1;
                            Iterator iterator1 = ((AutowiredObjectSerializer) (obj1)).getAutowiredFor().iterator();
                            while (iterator1.hasNext()) 
                            {
                                put((Type)iterator1.next(), obj1);
                            }
                        }
                    } while (true);
                }
                catch (ClassCastException classcastexception) { }
                obj1 = (ObjectSerializer)get(class1);
            }
        }
        classcastexception = ((ClassCastException) (obj1));
        if (obj1 != null) goto _L2; else goto _L1
_L1:
        if (java/util/Map.isAssignableFrom(class1))
        {
            put(class1, MapSerializer.instance);
        } else
        if (java/util/List.isAssignableFrom(class1))
        {
            put(class1, ListSerializer.instance);
        } else
        if (java/util/Collection.isAssignableFrom(class1))
        {
            put(class1, CollectionSerializer.instance);
        } else
        if (java/util/Date.isAssignableFrom(class1))
        {
            put(class1, DateSerializer.instance);
        } else
        if (com/alibaba/fastjson/JSONAware.isAssignableFrom(class1))
        {
            put(class1, JSONAwareSerializer.instance);
        } else
        if (com/alibaba/fastjson/serializer/JSONSerializable.isAssignableFrom(class1))
        {
            put(class1, JSONSerializableSerializer.instance);
        } else
        if (com/alibaba/fastjson/JSONStreamAware.isAssignableFrom(class1))
        {
            put(class1, JSONStreamAwareSerializer.instance);
        } else
        if (class1.isEnum() || class1.getSuperclass() != null && class1.getSuperclass().isEnum())
        {
            put(class1, EnumSerializer.instance);
        } else
        if (class1.isArray())
        {
            Class class2 = class1.getComponentType();
            put(class1, new ArraySerializer(class2, getObjectWriter(class2)));
        } else
        if (java/lang/Throwable.isAssignableFrom(class1))
        {
            put(class1, new ExceptionSerializer(class1));
        } else
        if (java/util/TimeZone.isAssignableFrom(class1))
        {
            put(class1, TimeZoneCodec.instance);
        } else
        if (java/lang/Appendable.isAssignableFrom(class1))
        {
            put(class1, AppendableSerializer.instance);
        } else
        if (java/nio/charset/Charset.isAssignableFrom(class1))
        {
            put(class1, CharsetCodec.instance);
        } else
        if (java/util/Enumeration.isAssignableFrom(class1))
        {
            put(class1, EnumerationSeriliazer.instance);
        } else
        if (java/util/Calendar.isAssignableFrom(class1))
        {
            put(class1, CalendarCodec.instance);
        } else
        {
label0:
            {
                if (!java/sql/Clob.isAssignableFrom(class1))
                {
                    break label0;
                }
                put(class1, ClobSeriliazer.instance);
            }
        }
_L4:
        classcastexception = (ObjectSerializer)get(class1);
_L2:
        return classcastexception;
        Class aclass[];
        int i;
        int j;
        aclass = class1.getInterfaces();
        j = aclass.length;
        i = 0;
_L3:
        if (i >= j)
        {
            break MISSING_BLOCK_LABEL_778;
        }
        Class class3 = aclass[i];
        ObjectSerializer objectserializer;
        if (class3.getName().equals("net.sf.cglib.proxy.Factory") || class3.getName().equals("org.springframework.cglib.proxy.Factory"))
        {
            i = 0;
            flag = true;
        } else
        {
label1:
            {
                if (!class3.getName().equals("javassist.util.proxy.ProxyObject"))
                {
                    break label1;
                }
                i = 1;
            }
        }
_L5:
        if (flag || i != 0)
        {
            objectserializer = getObjectWriter(class1.getSuperclass());
            put(class1, objectserializer);
            return objectserializer;
        }
        break MISSING_BLOCK_LABEL_743;
        i++;
          goto _L3
        if (Proxy.isProxyClass(class1))
        {
            put(class1, createJavaBeanSerializer(class1));
        } else
        {
            put(class1, createJavaBeanSerializer(class1));
        }
          goto _L4
        i = 0;
          goto _L5
    }

    public String getTypeKey()
    {
        return typeKey;
    }

    public boolean isAsmEnable()
    {
        return asm;
    }

    public void setAsmEnable(boolean flag)
    {
        asm = flag;
    }

    public void setTypeKey(String s)
    {
        typeKey = s;
    }

}
