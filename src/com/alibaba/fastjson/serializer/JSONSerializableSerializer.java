// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializable, JSONSerializer

public class JSONSerializableSerializer
    implements ObjectSerializer
{

    public static JSONSerializableSerializer instance = new JSONSerializableSerializer();

    public JSONSerializableSerializer()
    {
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        ((JSONSerializable)obj).write(jsonserializer, obj1, type, 0);
    }

}
