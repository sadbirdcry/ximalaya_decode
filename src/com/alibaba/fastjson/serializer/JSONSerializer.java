// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSONException;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.IdentityHashMap;
import java.util.List;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            SerializeWriter, SerializeConfig, SerialContext, SerializerFeature, 
//            ObjectSerializer, StringCodec, JSONSerializerMap

public class JSONSerializer
{

    private List afterFilters;
    private List beforeFilters;
    private final SerializeConfig config;
    private SerialContext context;
    private DateFormat dateFormat;
    private String dateFormatPattern;
    private String indent;
    private int indentCount;
    private List labelFilters;
    private List nameFilters;
    private final SerializeWriter out;
    private List propertyFilters;
    private List propertyPreFilters;
    private IdentityHashMap references;
    private List valueFilters;

    public JSONSerializer()
    {
        this(new SerializeWriter(), SerializeConfig.getGlobalInstance());
    }

    public JSONSerializer(JSONSerializerMap jsonserializermap)
    {
        this(new SerializeWriter(), ((SerializeConfig) (jsonserializermap)));
    }

    public JSONSerializer(SerializeConfig serializeconfig)
    {
        this(new SerializeWriter(), serializeconfig);
    }

    public JSONSerializer(SerializeWriter serializewriter)
    {
        this(serializewriter, SerializeConfig.getGlobalInstance());
    }

    public JSONSerializer(SerializeWriter serializewriter, SerializeConfig serializeconfig)
    {
        beforeFilters = null;
        afterFilters = null;
        propertyFilters = null;
        valueFilters = null;
        nameFilters = null;
        propertyPreFilters = null;
        labelFilters = null;
        indentCount = 0;
        indent = "\t";
        references = null;
        out = serializewriter;
        config = serializeconfig;
    }

    public static void write(SerializeWriter serializewriter, Object obj)
    {
        (new JSONSerializer(serializewriter)).write(obj);
    }

    public static void write(Writer writer, Object obj)
    {
        SerializeWriter serializewriter = new SerializeWriter();
        (new JSONSerializer(serializewriter)).write(obj);
        serializewriter.writeTo(writer);
        serializewriter.close();
        return;
        writer;
        throw new JSONException(writer.getMessage(), writer);
        writer;
        serializewriter.close();
        throw writer;
    }

    public void close()
    {
        out.close();
    }

    public void config(SerializerFeature serializerfeature, boolean flag)
    {
        out.config(serializerfeature, flag);
    }

    public boolean containsReference(Object obj)
    {
        if (references == null)
        {
            return false;
        } else
        {
            return references.containsKey(obj);
        }
    }

    public void decrementIdent()
    {
        indentCount = indentCount - 1;
    }

    public List getAfterFilters()
    {
        if (afterFilters == null)
        {
            afterFilters = new ArrayList();
        }
        return afterFilters;
    }

    public List getAfterFiltersDirect()
    {
        return afterFilters;
    }

    public List getBeforeFilters()
    {
        if (beforeFilters == null)
        {
            beforeFilters = new ArrayList();
        }
        return beforeFilters;
    }

    public List getBeforeFiltersDirect()
    {
        return beforeFilters;
    }

    public SerialContext getContext()
    {
        return context;
    }

    public DateFormat getDateFormat()
    {
        if (dateFormat == null && dateFormatPattern != null)
        {
            dateFormat = new SimpleDateFormat(dateFormatPattern);
        }
        return dateFormat;
    }

    public String getDateFormatPattern()
    {
        if (dateFormat instanceof SimpleDateFormat)
        {
            return ((SimpleDateFormat)dateFormat).toPattern();
        } else
        {
            return dateFormatPattern;
        }
    }

    public int getIndentCount()
    {
        return indentCount;
    }

    public List getLabelFilters()
    {
        if (labelFilters == null)
        {
            labelFilters = new ArrayList();
        }
        return labelFilters;
    }

    public List getLabelFiltersDirect()
    {
        return labelFilters;
    }

    public SerializeConfig getMapping()
    {
        return config;
    }

    public List getNameFilters()
    {
        if (nameFilters == null)
        {
            nameFilters = new ArrayList();
        }
        return nameFilters;
    }

    public List getNameFiltersDirect()
    {
        return nameFilters;
    }

    public ObjectSerializer getObjectWriter(Class class1)
    {
        return config.getObjectWriter(class1);
    }

    public List getPropertyFilters()
    {
        if (propertyFilters == null)
        {
            propertyFilters = new ArrayList();
        }
        return propertyFilters;
    }

    public List getPropertyFiltersDirect()
    {
        return propertyFilters;
    }

    public List getPropertyPreFilters()
    {
        if (propertyPreFilters == null)
        {
            propertyPreFilters = new ArrayList();
        }
        return propertyPreFilters;
    }

    public List getPropertyPreFiltersDirect()
    {
        return propertyPreFilters;
    }

    public SerialContext getSerialContext(Object obj)
    {
        if (references == null)
        {
            return null;
        } else
        {
            return (SerialContext)references.get(obj);
        }
    }

    public List getValueFilters()
    {
        if (valueFilters == null)
        {
            valueFilters = new ArrayList();
        }
        return valueFilters;
    }

    public List getValueFiltersDirect()
    {
        return valueFilters;
    }

    public SerializeWriter getWriter()
    {
        return out;
    }

    public void incrementIndent()
    {
        indentCount = indentCount + 1;
    }

    public boolean isEnabled(SerializerFeature serializerfeature)
    {
        return out.isEnabled(serializerfeature);
    }

    public final boolean isWriteClassName(Type type, Object obj)
    {
        if (out.isEnabled(SerializerFeature.WriteClassName)) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        if (type != null || !isEnabled(SerializerFeature.NotWriteRootClassName))
        {
            break; /* Loop/switch isn't completed */
        }
        boolean flag;
        if (context.getParent() == null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (flag) goto _L1; else goto _L3
_L3:
        return true;
    }

    public void popContext()
    {
        if (context != null)
        {
            context = context.getParent();
        }
    }

    public void println()
    {
        out.write('\n');
        for (int i = 0; i < indentCount; i++)
        {
            out.write(indent);
        }

    }

    public void setContext(SerialContext serialcontext)
    {
        context = serialcontext;
    }

    public void setContext(SerialContext serialcontext, Object obj, Object obj1, int i)
    {
        setContext(serialcontext, obj, obj1, i, 0);
    }

    public void setContext(SerialContext serialcontext, Object obj, Object obj1, int i, int j)
    {
        if (isEnabled(SerializerFeature.DisableCircularReferenceDetect))
        {
            return;
        }
        context = new SerialContext(serialcontext, obj, obj1, i, j);
        if (references == null)
        {
            references = new IdentityHashMap();
        }
        references.put(obj, context);
    }

    public void setContext(Object obj, Object obj1)
    {
        setContext(context, obj, obj1, 0);
    }

    public void setDateFormat(String s)
    {
        dateFormatPattern = s;
        if (dateFormat != null)
        {
            dateFormat = null;
        }
    }

    public void setDateFormat(DateFormat dateformat)
    {
        dateFormat = dateformat;
        if (dateFormatPattern != null)
        {
            dateFormatPattern = null;
        }
    }

    public String toString()
    {
        return out.toString();
    }

    public final void write(Object obj)
    {
        if (obj == null)
        {
            out.writeNull();
            return;
        }
        ObjectSerializer objectserializer = getObjectWriter(obj.getClass());
        try
        {
            objectserializer.write(this, obj, null, null, 0);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            throw new JSONException(((IOException) (obj)).getMessage(), ((Throwable) (obj)));
        }
    }

    public final void write(String s)
    {
        StringCodec.instance.write(this, s);
    }

    protected final void writeKeyValue(char c, String s, Object obj)
    {
        if (c != 0)
        {
            out.write(c);
        }
        out.writeFieldName(s);
        write(obj);
    }

    public void writeNull()
    {
        out.writeNull();
    }

    public void writeReference(Object obj)
    {
        SerialContext serialcontext1 = getContext();
        if (obj == serialcontext1.getObject())
        {
            out.write("{\"$ref\":\"@\"}");
            return;
        }
        SerialContext serialcontext2 = serialcontext1.getParent();
        SerialContext serialcontext = serialcontext1;
        if (serialcontext2 != null)
        {
            serialcontext = serialcontext1;
            if (obj == serialcontext2.getObject())
            {
                out.write("{\"$ref\":\"..\"}");
                return;
            }
        }
        for (; serialcontext.getParent() != null; serialcontext = serialcontext.getParent()) { }
        if (obj == serialcontext.getObject())
        {
            out.write("{\"$ref\":\"$\"}");
            return;
        } else
        {
            obj = getSerialContext(obj).getPath();
            out.write("{\"$ref\":\"");
            out.write(((String) (obj)));
            out.write("\"}");
            return;
        }
    }

    public final void writeWithFieldName(Object obj, Object obj1)
    {
        writeWithFieldName(obj, obj1, null, 0);
    }

    public final void writeWithFieldName(Object obj, Object obj1, Type type, int i)
    {
        if (obj == null)
        {
            try
            {
                out.writeNull();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                throw new JSONException(((IOException) (obj)).getMessage(), ((Throwable) (obj)));
            }
        }
        getObjectWriter(obj.getClass()).write(this, obj, obj1, type, i);
        return;
    }

    public final void writeWithFormat(Object obj, String s)
    {
        if (obj instanceof Date)
        {
            DateFormat dateformat = getDateFormat();
            Object obj1 = dateformat;
            if (dateformat == null)
            {
                obj1 = new SimpleDateFormat(s);
            }
            obj = ((DateFormat) (obj1)).format((Date)obj);
            out.writeString(((String) (obj)));
            return;
        } else
        {
            write(obj);
            return;
        }
    }
}
