// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Currency;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter

public class CurrencyCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final CurrencyCodec instance = new CurrencyCodec();

    public CurrencyCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        defaultjsonparser = (String)defaultjsonparser.parse();
        if (defaultjsonparser == null || defaultjsonparser.length() == 0)
        {
            return null;
        } else
        {
            return Currency.getInstance(defaultjsonparser);
        }
    }

    public int getFastMatchToken()
    {
        return 4;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer = jsonserializer.getWriter();
        if (obj == null)
        {
            jsonserializer.writeNull();
            return;
        } else
        {
            jsonserializer.writeString(((Currency)obj).getCurrencyCode());
            return;
        }
    }

}
