// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.util.FieldInfo;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            SerializerFeature, JSONSerializer, SerializeWriter

public abstract class FieldSerializer
{

    private final String double_quoted_fieldPrefix;
    protected final FieldInfo fieldInfo;
    private final String single_quoted_fieldPrefix;
    private final String un_quoted_fieldPrefix;
    private boolean writeNull;

    public FieldSerializer(FieldInfo fieldinfo)
    {
        writeNull = false;
        fieldInfo = fieldinfo;
        fieldinfo.setAccessible(true);
        double_quoted_fieldPrefix = (new StringBuilder()).append('"').append(fieldinfo.getName()).append("\":").toString();
        single_quoted_fieldPrefix = (new StringBuilder()).append('\'').append(fieldinfo.getName()).append("':").toString();
        un_quoted_fieldPrefix = (new StringBuilder()).append(fieldinfo.getName()).append(":").toString();
        fieldinfo = (JSONField)fieldinfo.getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        if (fieldinfo != null)
        {
            fieldinfo = fieldinfo.serialzeFeatures();
            int j = fieldinfo.length;
            for (int i = 0; i < j; i++)
            {
                if (fieldinfo[i] == SerializerFeature.WriteMapNullValue)
                {
                    writeNull = true;
                }
            }

        }
    }

    public Field getField()
    {
        return fieldInfo.getField();
    }

    public String getLabel()
    {
        return fieldInfo.getLabel();
    }

    public Method getMethod()
    {
        return fieldInfo.getMethod();
    }

    public String getName()
    {
        return fieldInfo.getName();
    }

    public Object getPropertyValue(Object obj)
        throws Exception
    {
        try
        {
            obj = fieldInfo.get(obj);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            throw new JSONException((new StringBuilder()).append("get property error\u3002 ").append(fieldInfo.gerQualifiedName()).toString(), ((Throwable) (obj)));
        }
        return obj;
    }

    public boolean isWriteNull()
    {
        return writeNull;
    }

    public void writePrefix(JSONSerializer jsonserializer)
        throws IOException
    {
        SerializeWriter serializewriter = jsonserializer.getWriter();
        if (jsonserializer.isEnabled(SerializerFeature.QuoteFieldNames))
        {
            if (jsonserializer.isEnabled(SerializerFeature.UseSingleQuotes))
            {
                serializewriter.write(single_quoted_fieldPrefix);
                return;
            } else
            {
                serializewriter.write(double_quoted_fieldPrefix);
                return;
            }
        } else
        {
            serializewriter.write(un_quoted_fieldPrefix);
            return;
        }
    }

    public abstract void writeProperty(JSONSerializer jsonserializer, Object obj)
        throws Exception;

    public abstract void writeValue(JSONSerializer jsonserializer, Object obj)
        throws Exception;
}
