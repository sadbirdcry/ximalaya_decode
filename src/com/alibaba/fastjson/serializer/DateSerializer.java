// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.util.IOUtils;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter, SerializerFeature

public class DateSerializer
    implements ObjectSerializer
{

    public static final DateSerializer instance = new DateSerializer();

    public DateSerializer()
    {
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        obj1 = jsonserializer.getWriter();
        if (obj == null)
        {
            ((SerializeWriter) (obj1)).writeNull();
            return;
        }
        Date date = (Date)obj;
        if (((SerializeWriter) (obj1)).isEnabled(SerializerFeature.WriteDateUseDateFormat))
        {
            obj = jsonserializer.getDateFormat();
            jsonserializer = ((JSONSerializer) (obj));
            if (obj == null)
            {
                jsonserializer = new SimpleDateFormat(JSON.DEFFAULT_DATE_FORMAT);
            }
            ((SerializeWriter) (obj1)).writeString(jsonserializer.format(date));
            return;
        }
        if (((SerializeWriter) (obj1)).isEnabled(SerializerFeature.WriteClassName) && obj.getClass() != type)
        {
            if (obj.getClass() == java/util/Date)
            {
                ((SerializeWriter) (obj1)).write("new Date(");
                ((SerializeWriter) (obj1)).writeLongAndChar(((Date)obj).getTime(), ')');
                return;
            } else
            {
                ((SerializeWriter) (obj1)).write('{');
                ((SerializeWriter) (obj1)).writeFieldName(JSON.DEFAULT_TYPE_KEY);
                jsonserializer.write(obj.getClass().getName());
                ((SerializeWriter) (obj1)).writeFieldValue(',', "val", ((Date)obj).getTime());
                ((SerializeWriter) (obj1)).write('}');
                return;
            }
        }
        long l1 = date.getTime();
        if (jsonserializer.isEnabled(SerializerFeature.UseISO8601DateFormat))
        {
            int j;
            int k;
            int l;
            int i1;
            int j1;
            int k1;
            if (jsonserializer.isEnabled(SerializerFeature.UseSingleQuotes))
            {
                ((SerializeWriter) (obj1)).append('\'');
            } else
            {
                ((SerializeWriter) (obj1)).append('"');
            }
            type = Calendar.getInstance();
            type.setTimeInMillis(l1);
            i = type.get(1);
            j = type.get(2) + 1;
            k = type.get(5);
            l = type.get(11);
            i1 = type.get(12);
            j1 = type.get(13);
            k1 = type.get(14);
            if (k1 != 0)
            {
                obj = "0000-00-00T00:00:00.000".toCharArray();
                IOUtils.getChars(k1, 23, ((char []) (obj)));
                IOUtils.getChars(j1, 19, ((char []) (obj)));
                IOUtils.getChars(i1, 16, ((char []) (obj)));
                IOUtils.getChars(l, 13, ((char []) (obj)));
                IOUtils.getChars(k, 10, ((char []) (obj)));
                IOUtils.getChars(j, 7, ((char []) (obj)));
                IOUtils.getChars(i, 4, ((char []) (obj)));
            } else
            if (j1 == 0 && i1 == 0 && l == 0)
            {
                obj = "0000-00-00".toCharArray();
                IOUtils.getChars(k, 10, ((char []) (obj)));
                IOUtils.getChars(j, 7, ((char []) (obj)));
                IOUtils.getChars(i, 4, ((char []) (obj)));
            } else
            {
                obj = "0000-00-00T00:00:00".toCharArray();
                IOUtils.getChars(j1, 19, ((char []) (obj)));
                IOUtils.getChars(i1, 16, ((char []) (obj)));
                IOUtils.getChars(l, 13, ((char []) (obj)));
                IOUtils.getChars(k, 10, ((char []) (obj)));
                IOUtils.getChars(j, 7, ((char []) (obj)));
                IOUtils.getChars(i, 4, ((char []) (obj)));
            }
            ((SerializeWriter) (obj1)).write(((char []) (obj)));
            i = type.getTimeZone().getRawOffset() / 0x36ee80;
            if (i == 0)
            {
                ((SerializeWriter) (obj1)).append("Z");
            } else
            if (i > 0)
            {
                ((SerializeWriter) (obj1)).append("+").append(String.format("%02d", new Object[] {
                    Integer.valueOf(i)
                })).append(":00");
            } else
            {
                ((SerializeWriter) (obj1)).append("-").append(String.format("%02d", new Object[] {
                    Integer.valueOf(-i)
                })).append(":00");
            }
            if (jsonserializer.isEnabled(SerializerFeature.UseSingleQuotes))
            {
                ((SerializeWriter) (obj1)).append('\'');
                return;
            } else
            {
                ((SerializeWriter) (obj1)).append('"');
                return;
            }
        } else
        {
            ((SerializeWriter) (obj1)).writeLong(l1);
            return;
        }
    }

}
