// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class StringCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static StringCodec instance = new StringCodec();

    public StringCodec()
    {
    }

    public static Object deserialze(DefaultJSONParser defaultjsonparser)
    {
        JSONLexer jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() == 4)
        {
            defaultjsonparser = jsonlexer.stringVal();
            jsonlexer.nextToken(16);
            return defaultjsonparser;
        }
        if (jsonlexer.token() == 2)
        {
            defaultjsonparser = jsonlexer.numberString();
            jsonlexer.nextToken(16);
            return defaultjsonparser;
        }
        defaultjsonparser = ((DefaultJSONParser) (defaultjsonparser.parse()));
        if (defaultjsonparser == null)
        {
            return null;
        } else
        {
            return defaultjsonparser.toString();
        }
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        obj = null;
        if (type != java/lang/StringBuffer) goto _L2; else goto _L1
_L1:
        type = defaultjsonparser.getLexer();
        if (type.token() != 4) goto _L4; else goto _L3
_L3:
        defaultjsonparser = type.stringVal();
        type.nextToken(16);
        defaultjsonparser = new StringBuffer(defaultjsonparser);
_L6:
        return defaultjsonparser;
_L4:
        type = ((Type) (defaultjsonparser.parse()));
        defaultjsonparser = ((DefaultJSONParser) (obj));
        if (type != null)
        {
            return new StringBuffer(type.toString());
        }
        continue; /* Loop/switch isn't completed */
_L2:
        if (type == java/lang/StringBuilder)
        {
            type = defaultjsonparser.getLexer();
            if (type.token() == 4)
            {
                defaultjsonparser = type.stringVal();
                type.nextToken(16);
                return new StringBuilder(defaultjsonparser);
            }
            type = ((Type) (defaultjsonparser.parse()));
            defaultjsonparser = ((DefaultJSONParser) (obj));
            if (type != null)
            {
                return new StringBuilder(type.toString());
            }
        } else
        {
            return deserialze(defaultjsonparser);
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    public int getFastMatchToken()
    {
        return 4;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        write(jsonserializer, (String)obj);
    }

    public void write(JSONSerializer jsonserializer, String s)
    {
        jsonserializer = jsonserializer.getWriter();
        if (s == null)
        {
            if (jsonserializer.isEnabled(SerializerFeature.WriteNullStringAsEmpty))
            {
                jsonserializer.writeString("");
                return;
            } else
            {
                jsonserializer.writeNull();
                return;
            }
        } else
        {
            jsonserializer.writeString(s);
            return;
        }
    }

}
