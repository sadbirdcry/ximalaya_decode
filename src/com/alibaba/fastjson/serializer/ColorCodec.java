// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import java.awt.Color;
import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter, SerializerFeature

public class ColorCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final ColorCodec instance = new ColorCodec();

    public ColorCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        int j;
        int k;
        int l;
        int i1;
        i1 = 0;
        defaultjsonparser = defaultjsonparser.getLexer();
        if (defaultjsonparser.token() != 12 && defaultjsonparser.token() != 16)
        {
            throw new JSONException("syntax error");
        }
        defaultjsonparser.nextToken();
        l = 0;
        k = 0;
        j = 0;
_L10:
        if (defaultjsonparser.token() == 13)
        {
            defaultjsonparser.nextToken();
            return new Color(j, k, l, i1);
        }
        if (defaultjsonparser.token() != 4) goto _L2; else goto _L1
_L1:
        type = defaultjsonparser.stringVal();
        defaultjsonparser.nextTokenWithColon(2);
        if (defaultjsonparser.token() != 2) goto _L4; else goto _L3
_L3:
        int i;
        i = defaultjsonparser.intValue();
        defaultjsonparser.nextToken();
        if (!type.equalsIgnoreCase("r")) goto _L6; else goto _L5
_L5:
        int j1;
        int k1;
        int l1;
        l1 = i;
        k1 = k;
        j1 = l;
        i = i1;
_L8:
        i1 = i;
        l = j1;
        k = k1;
        j = l1;
        if (defaultjsonparser.token() == 16)
        {
            defaultjsonparser.nextToken(4);
            i1 = i;
            l = j1;
            k = k1;
            j = l1;
        }
        continue; /* Loop/switch isn't completed */
_L2:
        throw new JSONException("syntax error");
_L4:
        throw new JSONException("syntax error");
_L6:
        if (type.equalsIgnoreCase("g"))
        {
            k1 = i;
            i = i1;
            j1 = l;
            l1 = j;
            continue; /* Loop/switch isn't completed */
        }
        if (type.equalsIgnoreCase("b"))
        {
            j1 = i;
            i = i1;
            k1 = k;
            l1 = j;
            continue; /* Loop/switch isn't completed */
        }
        if (!type.equalsIgnoreCase("alpha"))
        {
            break; /* Loop/switch isn't completed */
        }
        j1 = l;
        k1 = k;
        l1 = j;
        if (true) goto _L8; else goto _L7
_L7:
        throw new JSONException((new StringBuilder()).append("syntax error, ").append(type).toString());
        if (true) goto _L10; else goto _L9
_L9:
    }

    public int getFastMatchToken()
    {
        return 12;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        char c = '{';
        jsonserializer = jsonserializer.getWriter();
        obj = (Color)obj;
        if (obj == null)
        {
            jsonserializer.writeNull();
            return;
        }
        if (jsonserializer.isEnabled(SerializerFeature.WriteClassName))
        {
            jsonserializer.write('{');
            jsonserializer.writeFieldName(JSON.DEFAULT_TYPE_KEY);
            jsonserializer.writeString(java/awt/Color.getName());
            c = ',';
        }
        jsonserializer.writeFieldValue(c, "r", ((Color) (obj)).getRed());
        jsonserializer.writeFieldValue(',', "g", ((Color) (obj)).getGreen());
        jsonserializer.writeFieldValue(',', "b", ((Color) (obj)).getBlue());
        if (((Color) (obj)).getAlpha() > 0)
        {
            jsonserializer.writeFieldValue(',', "alpha", ((Color) (obj)).getAlpha());
        }
        jsonserializer.write('}');
    }

}
