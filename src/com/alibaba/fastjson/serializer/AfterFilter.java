// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;


// Referenced classes of package com.alibaba.fastjson.serializer:
//            SerializeFilter, JSONSerializer

public abstract class AfterFilter
    implements SerializeFilter
{

    private static final Character COMMA = Character.valueOf(',');
    private static final ThreadLocal seperatorLocal = new ThreadLocal();
    private static final ThreadLocal serializerLocal = new ThreadLocal();

    public AfterFilter()
    {
    }

    final char writeAfter(JSONSerializer jsonserializer, Object obj, char c)
    {
        serializerLocal.set(jsonserializer);
        seperatorLocal.set(Character.valueOf(c));
        writeAfter(obj);
        serializerLocal.set(null);
        return ((Character)seperatorLocal.get()).charValue();
    }

    public abstract void writeAfter(Object obj);

    protected final void writeKeyValue(String s, Object obj)
    {
        JSONSerializer jsonserializer = (JSONSerializer)serializerLocal.get();
        char c = ((Character)seperatorLocal.get()).charValue();
        jsonserializer.writeKeyValue(c, s, obj);
        if (c != ',')
        {
            seperatorLocal.set(COMMA);
        }
    }

}
