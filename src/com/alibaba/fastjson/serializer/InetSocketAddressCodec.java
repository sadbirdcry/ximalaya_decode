// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.InetSocketAddress;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter

public class InetSocketAddressCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static InetSocketAddressCodec instance = new InetSocketAddressCodec();

    public InetSocketAddressCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        int i;
        type = null;
        obj = defaultjsonparser.getLexer();
        if (((JSONLexer) (obj)).token() == 8)
        {
            ((JSONLexer) (obj)).nextToken();
            return null;
        }
        defaultjsonparser.accept(12);
        i = 0;
_L3:
        String s;
        s = ((JSONLexer) (obj)).stringVal();
        ((JSONLexer) (obj)).nextToken(17);
        if (!s.equals("address")) goto _L2; else goto _L1
_L1:
        defaultjsonparser.accept(17);
        type = (InetAddress)defaultjsonparser.parseObject(java/net/InetAddress);
_L4:
        if (((JSONLexer) (obj)).token() == 16)
        {
            ((JSONLexer) (obj)).nextToken();
        } else
        {
            defaultjsonparser.accept(13);
            return new InetSocketAddress(type, i);
        }
        if (true) goto _L3; else goto _L2
_L2:
        if (s.equals("port"))
        {
            defaultjsonparser.accept(17);
            if (((JSONLexer) (obj)).token() != 2)
            {
                throw new JSONException("port is not int");
            }
            i = ((JSONLexer) (obj)).intValue();
            ((JSONLexer) (obj)).nextToken();
        } else
        {
            defaultjsonparser.accept(17);
            defaultjsonparser.parse();
        }
          goto _L4
    }

    public int getFastMatchToken()
    {
        return 12;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        if (obj == null)
        {
            jsonserializer.writeNull();
            return;
        }
        obj1 = jsonserializer.getWriter();
        obj = (InetSocketAddress)obj;
        type = ((InetSocketAddress) (obj)).getAddress();
        ((SerializeWriter) (obj1)).write('{');
        if (type != null)
        {
            ((SerializeWriter) (obj1)).writeFieldName("address");
            jsonserializer.write(type);
            ((SerializeWriter) (obj1)).write(',');
        }
        ((SerializeWriter) (obj1)).writeFieldName("port");
        ((SerializeWriter) (obj1)).writeInt(((InetSocketAddress) (obj)).getPort());
        ((SerializeWriter) (obj1)).write('}');
    }

}
