// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.util.FieldInfo;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            FieldSerializer, JSONSerializer, SerializerFeature, SerializeWriter

final class NumberFieldSerializer extends FieldSerializer
{

    public NumberFieldSerializer(FieldInfo fieldinfo)
    {
        super(fieldinfo);
    }

    public void writeProperty(JSONSerializer jsonserializer, Object obj)
        throws Exception
    {
        writePrefix(jsonserializer);
        writeValue(jsonserializer, obj);
    }

    public void writeValue(JSONSerializer jsonserializer, Object obj)
        throws Exception
    {
        jsonserializer = jsonserializer.getWriter();
        if (obj == null)
        {
            if (jsonserializer.isEnabled(SerializerFeature.WriteNullNumberAsZero))
            {
                jsonserializer.write('0');
                return;
            } else
            {
                jsonserializer.writeNull();
                return;
            }
        } else
        {
            jsonserializer.append(obj.toString());
            return;
        }
    }
}
