// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;


public final class JSONSerializerContext
{
    protected static final class Entry
    {

        public final int hashCode;
        public Entry next;
        public final Object object;

        public Entry(Object obj, int i, Entry entry)
        {
            object = obj;
            next = entry;
            hashCode = i;
        }
    }


    public static final int DEFAULT_TABLE_SIZE = 128;
    private final Entry buckets[];
    private final int indexMask;

    public JSONSerializerContext()
    {
        this(128);
    }

    public JSONSerializerContext(int i)
    {
        indexMask = i - 1;
        buckets = new Entry[i];
    }

    public final boolean put(Object obj)
    {
        int i = System.identityHashCode(obj);
        int j = i & indexMask;
        for (Entry entry = buckets[j]; entry != null; entry = entry.next)
        {
            if (obj == entry.object)
            {
                return true;
            }
        }

        obj = new Entry(obj, i, buckets[j]);
        buckets[j] = ((Entry) (obj));
        return false;
    }
}
