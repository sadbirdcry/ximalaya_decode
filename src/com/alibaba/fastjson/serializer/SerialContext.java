// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;


// Referenced classes of package com.alibaba.fastjson.serializer:
//            SerializerFeature

public class SerialContext
{

    private int features;
    private int fieldFeatures;
    private final Object fieldName;
    private final Object object;
    private final SerialContext parent;

    public SerialContext(SerialContext serialcontext, Object obj, Object obj1, int i, int j)
    {
        parent = serialcontext;
        object = obj;
        fieldName = obj1;
        features = i;
        fieldFeatures = j;
    }

    public int getFeatures()
    {
        return features;
    }

    public Object getFieldName()
    {
        return fieldName;
    }

    public Object getObject()
    {
        return object;
    }

    public SerialContext getParent()
    {
        return parent;
    }

    public String getPath()
    {
        if (parent == null)
        {
            return "$";
        }
        if (fieldName instanceof Integer)
        {
            return (new StringBuilder()).append(parent.getPath()).append("[").append(fieldName).append("]").toString();
        } else
        {
            return (new StringBuilder()).append(parent.getPath()).append(".").append(fieldName).toString();
        }
    }

    public boolean isEnabled(SerializerFeature serializerfeature)
    {
        return SerializerFeature.isEnabled(features, fieldFeatures, serializerfeature);
    }

    public String toString()
    {
        return getPath();
    }
}
