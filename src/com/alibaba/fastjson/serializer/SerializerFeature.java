// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;


public final class SerializerFeature extends Enum
{

    private static final SerializerFeature $VALUES[];
    public static final SerializerFeature BeanToArray;
    public static final SerializerFeature BrowserCompatible;
    public static final SerializerFeature BrowserSecure;
    public static final SerializerFeature DisableCheckSpecialChar;
    public static final SerializerFeature DisableCircularReferenceDetect;
    public static final SerializerFeature IgnoreNonFieldGetter;
    public static final SerializerFeature NotWriteDefaultValue;
    public static final SerializerFeature NotWriteRootClassName;
    public static final SerializerFeature PrettyFormat;
    public static final SerializerFeature QuoteFieldNames;
    public static final SerializerFeature SkipTransientField;
    public static final SerializerFeature SortField;
    public static final SerializerFeature UseISO8601DateFormat;
    public static final SerializerFeature UseSingleQuotes;
    public static final SerializerFeature WriteClassName;
    public static final SerializerFeature WriteDateUseDateFormat;
    public static final SerializerFeature WriteEnumUsingName;
    public static final SerializerFeature WriteEnumUsingToString;
    public static final SerializerFeature WriteMapNullValue;
    public static final SerializerFeature WriteNonStringKeyAsString;
    public static final SerializerFeature WriteNullBooleanAsFalse;
    public static final SerializerFeature WriteNullListAsEmpty;
    public static final SerializerFeature WriteNullNumberAsZero;
    public static final SerializerFeature WriteNullStringAsEmpty;
    public static final SerializerFeature WriteSlashAsSpecial;
    public static final SerializerFeature WriteTabAsSpecial;
    private final int mask = 1 << ordinal();

    private SerializerFeature(String s, int i)
    {
        super(s, i);
    }

    public static int config(int i, SerializerFeature serializerfeature, boolean flag)
    {
        if (flag)
        {
            return serializerfeature.getMask() | i;
        } else
        {
            return ~serializerfeature.getMask() & i;
        }
    }

    public static boolean isEnabled(int i, int j, SerializerFeature serializerfeature)
    {
        int k = serializerfeature.getMask();
        return (i & k) != 0 || (k & j) != 0;
    }

    public static boolean isEnabled(int i, SerializerFeature serializerfeature)
    {
        return (serializerfeature.getMask() & i) != 0;
    }

    public static int of(SerializerFeature aserializerfeature[])
    {
        int i;
        int k;
        i = 0;
        k = 0;
        if (aserializerfeature != null) goto _L2; else goto _L1
_L1:
        return k;
_L2:
        int l = aserializerfeature.length;
        int j = 0;
        do
        {
            k = i;
            if (j >= l)
            {
                continue;
            }
            k = aserializerfeature[j].getMask();
            j++;
            i = k | i;
        } while (true);
        if (true) goto _L1; else goto _L3
_L3:
    }

    public static SerializerFeature valueOf(String s)
    {
        return (SerializerFeature)Enum.valueOf(com/alibaba/fastjson/serializer/SerializerFeature, s);
    }

    public static SerializerFeature[] values()
    {
        return (SerializerFeature[])$VALUES.clone();
    }

    public final int getMask()
    {
        return mask;
    }

    static 
    {
        QuoteFieldNames = new SerializerFeature("QuoteFieldNames", 0);
        UseSingleQuotes = new SerializerFeature("UseSingleQuotes", 1);
        WriteMapNullValue = new SerializerFeature("WriteMapNullValue", 2);
        WriteEnumUsingToString = new SerializerFeature("WriteEnumUsingToString", 3);
        WriteEnumUsingName = new SerializerFeature("WriteEnumUsingName", 4);
        UseISO8601DateFormat = new SerializerFeature("UseISO8601DateFormat", 5);
        WriteNullListAsEmpty = new SerializerFeature("WriteNullListAsEmpty", 6);
        WriteNullStringAsEmpty = new SerializerFeature("WriteNullStringAsEmpty", 7);
        WriteNullNumberAsZero = new SerializerFeature("WriteNullNumberAsZero", 8);
        WriteNullBooleanAsFalse = new SerializerFeature("WriteNullBooleanAsFalse", 9);
        SkipTransientField = new SerializerFeature("SkipTransientField", 10);
        SortField = new SerializerFeature("SortField", 11);
        WriteTabAsSpecial = new SerializerFeature("WriteTabAsSpecial", 12);
        PrettyFormat = new SerializerFeature("PrettyFormat", 13);
        WriteClassName = new SerializerFeature("WriteClassName", 14);
        DisableCircularReferenceDetect = new SerializerFeature("DisableCircularReferenceDetect", 15);
        WriteSlashAsSpecial = new SerializerFeature("WriteSlashAsSpecial", 16);
        BrowserCompatible = new SerializerFeature("BrowserCompatible", 17);
        WriteDateUseDateFormat = new SerializerFeature("WriteDateUseDateFormat", 18);
        NotWriteRootClassName = new SerializerFeature("NotWriteRootClassName", 19);
        DisableCheckSpecialChar = new SerializerFeature("DisableCheckSpecialChar", 20);
        BeanToArray = new SerializerFeature("BeanToArray", 21);
        WriteNonStringKeyAsString = new SerializerFeature("WriteNonStringKeyAsString", 22);
        NotWriteDefaultValue = new SerializerFeature("NotWriteDefaultValue", 23);
        BrowserSecure = new SerializerFeature("BrowserSecure", 24);
        IgnoreNonFieldGetter = new SerializerFeature("IgnoreNonFieldGetter", 25);
        $VALUES = (new SerializerFeature[] {
            QuoteFieldNames, UseSingleQuotes, WriteMapNullValue, WriteEnumUsingToString, WriteEnumUsingName, UseISO8601DateFormat, WriteNullListAsEmpty, WriteNullStringAsEmpty, WriteNullNumberAsZero, WriteNullBooleanAsFalse, 
            SkipTransientField, SortField, WriteTabAsSpecial, PrettyFormat, WriteClassName, DisableCircularReferenceDetect, WriteSlashAsSpecial, BrowserCompatible, WriteDateUseDateFormat, NotWriteRootClassName, 
            DisableCheckSpecialChar, BeanToArray, WriteNonStringKeyAsString, NotWriteDefaultValue, BrowserSecure, IgnoreNonFieldGetter
        });
    }
}
