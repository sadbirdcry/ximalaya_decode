// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class BooleanArraySerializer
    implements ObjectSerializer
{

    public static BooleanArraySerializer instance = new BooleanArraySerializer();

    public BooleanArraySerializer()
    {
    }

    public final void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer = jsonserializer.getWriter();
        if (obj == null)
        {
            if (jsonserializer.isEnabled(SerializerFeature.WriteNullListAsEmpty))
            {
                jsonserializer.write("[]");
                return;
            } else
            {
                jsonserializer.writeNull();
                return;
            }
        }
        obj = (boolean[])(boolean[])obj;
        jsonserializer.write('[');
        for (i = 0; i < obj.length; i++)
        {
            if (i != 0)
            {
                jsonserializer.write(',');
            }
            jsonserializer.write(obj[i]);
        }

        jsonserializer.write(']');
    }

}
