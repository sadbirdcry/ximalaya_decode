// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import java.awt.Font;
import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter, SerializerFeature

public class FontCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final FontCodec instance = new FontCodec();

    public FontCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        int i;
        int j;
        type = null;
        j = 0;
        obj = defaultjsonparser.getLexer();
        if (((JSONLexer) (obj)).token() == 8)
        {
            ((JSONLexer) (obj)).nextToken(16);
            return null;
        }
        if (((JSONLexer) (obj)).token() != 12 && ((JSONLexer) (obj)).token() != 16)
        {
            throw new JSONException("syntax error");
        }
        ((JSONLexer) (obj)).nextToken();
        i = 0;
        defaultjsonparser = type;
_L10:
        if (((JSONLexer) (obj)).token() == 13)
        {
            ((JSONLexer) (obj)).nextToken();
            return new Font(defaultjsonparser, j, i);
        }
        if (((JSONLexer) (obj)).token() != 4) goto _L2; else goto _L1
_L1:
        type = ((JSONLexer) (obj)).stringVal();
        ((JSONLexer) (obj)).nextTokenWithColon(2);
        if (!type.equalsIgnoreCase("name")) goto _L4; else goto _L3
_L3:
        if (((JSONLexer) (obj)).token() != 4) goto _L6; else goto _L5
_L5:
        int k;
        int l;
        type = ((JSONLexer) (obj)).stringVal();
        ((JSONLexer) (obj)).nextToken();
        l = i;
        k = j;
_L8:
        defaultjsonparser = type;
        j = k;
        i = l;
        if (((JSONLexer) (obj)).token() == 16)
        {
            ((JSONLexer) (obj)).nextToken(4);
            defaultjsonparser = type;
            j = k;
            i = l;
        }
        continue; /* Loop/switch isn't completed */
_L2:
        throw new JSONException("syntax error");
_L6:
        throw new JSONException("syntax error");
_L4:
        if (type.equalsIgnoreCase("style"))
        {
            if (((JSONLexer) (obj)).token() == 2)
            {
                k = ((JSONLexer) (obj)).intValue();
                ((JSONLexer) (obj)).nextToken();
                type = defaultjsonparser;
                l = i;
            } else
            {
                throw new JSONException("syntax error");
            }
            continue; /* Loop/switch isn't completed */
        }
        if (!type.equalsIgnoreCase("size"))
        {
            break; /* Loop/switch isn't completed */
        }
        if (((JSONLexer) (obj)).token() == 2)
        {
            l = ((JSONLexer) (obj)).intValue();
            ((JSONLexer) (obj)).nextToken();
            type = defaultjsonparser;
            k = j;
        } else
        {
            throw new JSONException("syntax error");
        }
        if (true) goto _L8; else goto _L7
_L7:
        throw new JSONException((new StringBuilder()).append("syntax error, ").append(type).toString());
        if (true) goto _L10; else goto _L9
_L9:
    }

    public int getFastMatchToken()
    {
        return 12;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        char c = '{';
        jsonserializer = jsonserializer.getWriter();
        obj = (Font)obj;
        if (obj == null)
        {
            jsonserializer.writeNull();
            return;
        }
        if (jsonserializer.isEnabled(SerializerFeature.WriteClassName))
        {
            jsonserializer.write('{');
            jsonserializer.writeFieldName(JSON.DEFAULT_TYPE_KEY);
            jsonserializer.writeString(java/awt/Font.getName());
            c = ',';
        }
        jsonserializer.writeFieldValue(c, "name", ((Font) (obj)).getName());
        jsonserializer.writeFieldValue(',', "style", ((Font) (obj)).getStyle());
        jsonserializer.writeFieldValue(',', "size", ((Font) (obj)).getSize());
        jsonserializer.write('}');
    }

}
