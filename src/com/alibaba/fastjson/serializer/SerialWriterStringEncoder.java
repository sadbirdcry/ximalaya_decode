// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.util.ThreadLocalCache;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;

public class SerialWriterStringEncoder
{

    private final CharsetEncoder encoder;

    public SerialWriterStringEncoder(Charset charset)
    {
        this(charset.newEncoder().onMalformedInput(CodingErrorAction.REPLACE).onUnmappableCharacter(CodingErrorAction.REPLACE));
    }

    public SerialWriterStringEncoder(CharsetEncoder charsetencoder)
    {
        encoder = charsetencoder;
    }

    private static int scale(int i, float f)
    {
        return (int)((double)i * (double)f);
    }

    public byte[] encode(char ac[], int i, int j)
    {
        if (j == 0)
        {
            return new byte[0];
        } else
        {
            encoder.reset();
            return encode(ac, i, j, ThreadLocalCache.getBytes(scale(j, encoder.maxBytesPerChar())));
        }
    }

    public byte[] encode(char ac[], int i, int j, byte abyte0[])
    {
        ByteBuffer bytebuffer = ByteBuffer.wrap(abyte0);
        ac = CharBuffer.wrap(ac, i, j);
        try
        {
            ac = encoder.encode(ac, bytebuffer, true);
            if (!ac.isUnderflow())
            {
                ac.throwException();
            }
            ac = encoder.flush(bytebuffer);
            if (!ac.isUnderflow())
            {
                ac.throwException();
            }
        }
        // Misplaced declaration of an exception variable
        catch (char ac[])
        {
            throw new JSONException(ac.getMessage(), ac);
        }
        i = bytebuffer.position();
        ac = new byte[i];
        System.arraycopy(abyte0, 0, ac, 0, i);
        return ac;
    }

    public CharsetEncoder getEncoder()
    {
        return encoder;
    }
}
