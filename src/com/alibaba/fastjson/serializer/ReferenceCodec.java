// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicReference;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer

public class ReferenceCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final ReferenceCodec instance = new ReferenceCodec();

    public ReferenceCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        type = (ParameterizedType)type;
        defaultjsonparser = ((DefaultJSONParser) (defaultjsonparser.parseObject(type.getActualTypeArguments()[0])));
        type = type.getRawType();
        if (type == java/util/concurrent/atomic/AtomicReference)
        {
            return new AtomicReference(defaultjsonparser);
        }
        if (type == java/lang/ref/WeakReference)
        {
            return new WeakReference(defaultjsonparser);
        }
        if (type == java/lang/ref/SoftReference)
        {
            return new SoftReference(defaultjsonparser);
        } else
        {
            throw new UnsupportedOperationException(type.toString());
        }
    }

    public int getFastMatchToken()
    {
        return 12;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        if (obj instanceof AtomicReference)
        {
            obj = ((AtomicReference)obj).get();
        } else
        {
            obj = ((Reference)obj).get();
        }
        jsonserializer.write(obj);
    }

}
