// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;


// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JavaBeanSerializer

public abstract class ASMJavaBeanSerializer
    implements ObjectSerializer
{

    protected JavaBeanSerializer nature;

    public ASMJavaBeanSerializer(Class class1)
    {
        nature = new JavaBeanSerializer(class1);
    }

    public JavaBeanSerializer getJavaBeanSerializer()
    {
        return nature;
    }
}
