// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.deserializer.ExtraProcessor;
import com.alibaba.fastjson.parser.deserializer.ExtraTypeProvider;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            JSONSerializer, PropertyFilter, LabelFilter, PropertyPreFilter, 
//            NameFilter, ValueFilter, AfterFilter, BeforeFilter

public class FilterUtils
{

    public FilterUtils()
    {
    }

    public static boolean apply(JSONSerializer jsonserializer, Object obj, String s, byte byte0)
    {
        jsonserializer = jsonserializer.getPropertyFiltersDirect();
        if (jsonserializer != null)
        {
            for (jsonserializer = jsonserializer.iterator(); jsonserializer.hasNext();)
            {
                if (!((PropertyFilter)jsonserializer.next()).apply(obj, s, Byte.valueOf(byte0)))
                {
                    return false;
                }
            }

            return true;
        } else
        {
            return true;
        }
    }

    public static boolean apply(JSONSerializer jsonserializer, Object obj, String s, char c)
    {
        jsonserializer = jsonserializer.getPropertyFiltersDirect();
        if (jsonserializer != null)
        {
            for (jsonserializer = jsonserializer.iterator(); jsonserializer.hasNext();)
            {
                if (!((PropertyFilter)jsonserializer.next()).apply(obj, s, Character.valueOf(c)))
                {
                    return false;
                }
            }

            return true;
        } else
        {
            return true;
        }
    }

    public static boolean apply(JSONSerializer jsonserializer, Object obj, String s, double d)
    {
        jsonserializer = jsonserializer.getPropertyFiltersDirect();
        if (jsonserializer != null)
        {
            for (jsonserializer = jsonserializer.iterator(); jsonserializer.hasNext();)
            {
                if (!((PropertyFilter)jsonserializer.next()).apply(obj, s, Double.valueOf(d)))
                {
                    return false;
                }
            }

            return true;
        } else
        {
            return true;
        }
    }

    public static boolean apply(JSONSerializer jsonserializer, Object obj, String s, float f)
    {
        jsonserializer = jsonserializer.getPropertyFiltersDirect();
        if (jsonserializer != null)
        {
            for (jsonserializer = jsonserializer.iterator(); jsonserializer.hasNext();)
            {
                if (!((PropertyFilter)jsonserializer.next()).apply(obj, s, Float.valueOf(f)))
                {
                    return false;
                }
            }

            return true;
        } else
        {
            return true;
        }
    }

    public static boolean apply(JSONSerializer jsonserializer, Object obj, String s, int i)
    {
        jsonserializer = jsonserializer.getPropertyFiltersDirect();
        if (jsonserializer != null)
        {
            for (jsonserializer = jsonserializer.iterator(); jsonserializer.hasNext();)
            {
                if (!((PropertyFilter)jsonserializer.next()).apply(obj, s, Integer.valueOf(i)))
                {
                    return false;
                }
            }

            return true;
        } else
        {
            return true;
        }
    }

    public static boolean apply(JSONSerializer jsonserializer, Object obj, String s, long l)
    {
        jsonserializer = jsonserializer.getPropertyFiltersDirect();
        if (jsonserializer != null)
        {
            for (jsonserializer = jsonserializer.iterator(); jsonserializer.hasNext();)
            {
                if (!((PropertyFilter)jsonserializer.next()).apply(obj, s, Long.valueOf(l)))
                {
                    return false;
                }
            }

            return true;
        } else
        {
            return true;
        }
    }

    public static boolean apply(JSONSerializer jsonserializer, Object obj, String s, Object obj1)
    {
        jsonserializer = jsonserializer.getPropertyFiltersDirect();
        if (jsonserializer == null)
        {
            return true;
        }
        for (jsonserializer = jsonserializer.iterator(); jsonserializer.hasNext();)
        {
            if (!((PropertyFilter)jsonserializer.next()).apply(obj, s, obj1))
            {
                return false;
            }
        }

        return true;
    }

    public static boolean apply(JSONSerializer jsonserializer, Object obj, String s, short word0)
    {
        jsonserializer = jsonserializer.getPropertyFiltersDirect();
        if (jsonserializer != null)
        {
            for (jsonserializer = jsonserializer.iterator(); jsonserializer.hasNext();)
            {
                if (!((PropertyFilter)jsonserializer.next()).apply(obj, s, Short.valueOf(word0)))
                {
                    return false;
                }
            }

            return true;
        } else
        {
            return true;
        }
    }

    public static boolean applyLabel(JSONSerializer jsonserializer, String s)
    {
        jsonserializer = jsonserializer.getLabelFiltersDirect();
        if (jsonserializer != null)
        {
            for (jsonserializer = jsonserializer.iterator(); jsonserializer.hasNext();)
            {
                if (!((LabelFilter)jsonserializer.next()).apply(s))
                {
                    return false;
                }
            }

            return true;
        } else
        {
            return true;
        }
    }

    public static boolean applyName(JSONSerializer jsonserializer, Object obj, String s)
    {
        Object obj1 = jsonserializer.getPropertyPreFiltersDirect();
        if (obj1 == null)
        {
            return true;
        }
        for (obj1 = ((List) (obj1)).iterator(); ((Iterator) (obj1)).hasNext();)
        {
            if (!((PropertyPreFilter)((Iterator) (obj1)).next()).apply(jsonserializer, obj, s))
            {
                return false;
            }
        }

        return true;
    }

    public static Type getExtratype(DefaultJSONParser defaultjsonparser, Object obj, String s)
    {
        Object obj1 = null;
        Object obj2 = null;
        defaultjsonparser = defaultjsonparser.getExtraTypeProvidersDirect();
        if (defaultjsonparser == null)
        {
            defaultjsonparser = obj2;
        } else
        {
            Iterator iterator = defaultjsonparser.iterator();
            defaultjsonparser = obj1;
            while (iterator.hasNext()) 
            {
                defaultjsonparser = ((ExtraTypeProvider)iterator.next()).getExtraType(obj, s);
            }
        }
        return defaultjsonparser;
    }

    public static void processExtra(DefaultJSONParser defaultjsonparser, Object obj, String s, Object obj1)
    {
        defaultjsonparser = defaultjsonparser.getExtraProcessorsDirect();
        if (defaultjsonparser != null)
        {
            defaultjsonparser = defaultjsonparser.iterator();
            while (defaultjsonparser.hasNext()) 
            {
                ((ExtraProcessor)defaultjsonparser.next()).processExtra(obj, s, obj1);
            }
        }
    }

    public static String processKey(JSONSerializer jsonserializer, Object obj, String s, byte byte0)
    {
        Object obj1 = jsonserializer.getNameFiltersDirect();
        jsonserializer = s;
        if (obj1 != null)
        {
            obj1 = ((List) (obj1)).iterator();
            do
            {
                jsonserializer = s;
                if (!((Iterator) (obj1)).hasNext())
                {
                    break;
                }
                s = ((NameFilter)((Iterator) (obj1)).next()).process(obj, s, Byte.valueOf(byte0));
            } while (true);
        }
        return jsonserializer;
    }

    public static String processKey(JSONSerializer jsonserializer, Object obj, String s, char c)
    {
        Object obj1 = jsonserializer.getNameFiltersDirect();
        jsonserializer = s;
        if (obj1 != null)
        {
            obj1 = ((List) (obj1)).iterator();
            do
            {
                jsonserializer = s;
                if (!((Iterator) (obj1)).hasNext())
                {
                    break;
                }
                s = ((NameFilter)((Iterator) (obj1)).next()).process(obj, s, Character.valueOf(c));
            } while (true);
        }
        return jsonserializer;
    }

    public static String processKey(JSONSerializer jsonserializer, Object obj, String s, double d)
    {
        Object obj1 = jsonserializer.getNameFiltersDirect();
        jsonserializer = s;
        if (obj1 != null)
        {
            obj1 = ((List) (obj1)).iterator();
            do
            {
                jsonserializer = s;
                if (!((Iterator) (obj1)).hasNext())
                {
                    break;
                }
                s = ((NameFilter)((Iterator) (obj1)).next()).process(obj, s, Double.valueOf(d));
            } while (true);
        }
        return jsonserializer;
    }

    public static String processKey(JSONSerializer jsonserializer, Object obj, String s, float f)
    {
        Object obj1 = jsonserializer.getNameFiltersDirect();
        jsonserializer = s;
        if (obj1 != null)
        {
            obj1 = ((List) (obj1)).iterator();
            do
            {
                jsonserializer = s;
                if (!((Iterator) (obj1)).hasNext())
                {
                    break;
                }
                s = ((NameFilter)((Iterator) (obj1)).next()).process(obj, s, Float.valueOf(f));
            } while (true);
        }
        return jsonserializer;
    }

    public static String processKey(JSONSerializer jsonserializer, Object obj, String s, int i)
    {
        Object obj1 = jsonserializer.getNameFiltersDirect();
        jsonserializer = s;
        if (obj1 != null)
        {
            obj1 = ((List) (obj1)).iterator();
            do
            {
                jsonserializer = s;
                if (!((Iterator) (obj1)).hasNext())
                {
                    break;
                }
                s = ((NameFilter)((Iterator) (obj1)).next()).process(obj, s, Integer.valueOf(i));
            } while (true);
        }
        return jsonserializer;
    }

    public static String processKey(JSONSerializer jsonserializer, Object obj, String s, long l)
    {
        Object obj1 = jsonserializer.getNameFiltersDirect();
        jsonserializer = s;
        if (obj1 != null)
        {
            obj1 = ((List) (obj1)).iterator();
            do
            {
                jsonserializer = s;
                if (!((Iterator) (obj1)).hasNext())
                {
                    break;
                }
                s = ((NameFilter)((Iterator) (obj1)).next()).process(obj, s, Long.valueOf(l));
            } while (true);
        }
        return jsonserializer;
    }

    public static String processKey(JSONSerializer jsonserializer, Object obj, String s, Object obj1)
    {
        Object obj2 = jsonserializer.getNameFiltersDirect();
        jsonserializer = s;
        if (obj2 != null)
        {
            obj2 = ((List) (obj2)).iterator();
            do
            {
                jsonserializer = s;
                if (!((Iterator) (obj2)).hasNext())
                {
                    break;
                }
                s = ((NameFilter)((Iterator) (obj2)).next()).process(obj, s, obj1);
            } while (true);
        }
        return jsonserializer;
    }

    public static String processKey(JSONSerializer jsonserializer, Object obj, String s, short word0)
    {
        Object obj1 = jsonserializer.getNameFiltersDirect();
        jsonserializer = s;
        if (obj1 != null)
        {
            obj1 = ((List) (obj1)).iterator();
            do
            {
                jsonserializer = s;
                if (!((Iterator) (obj1)).hasNext())
                {
                    break;
                }
                s = ((NameFilter)((Iterator) (obj1)).next()).process(obj, s, Short.valueOf(word0));
            } while (true);
        }
        return jsonserializer;
    }

    public static String processKey(JSONSerializer jsonserializer, Object obj, String s, boolean flag)
    {
        Object obj1 = jsonserializer.getNameFiltersDirect();
        jsonserializer = s;
        if (obj1 != null)
        {
            obj1 = ((List) (obj1)).iterator();
            do
            {
                jsonserializer = s;
                if (!((Iterator) (obj1)).hasNext())
                {
                    break;
                }
                s = ((NameFilter)((Iterator) (obj1)).next()).process(obj, s, Boolean.valueOf(flag));
            } while (true);
        }
        return jsonserializer;
    }

    public static Object processValue(JSONSerializer jsonserializer, Object obj, String s, Object obj1)
    {
        Object obj2 = jsonserializer.getValueFiltersDirect();
        jsonserializer = ((JSONSerializer) (obj1));
        if (obj2 != null)
        {
            obj2 = ((List) (obj2)).iterator();
            do
            {
                jsonserializer = ((JSONSerializer) (obj1));
                if (!((Iterator) (obj2)).hasNext())
                {
                    break;
                }
                obj1 = ((ValueFilter)((Iterator) (obj2)).next()).process(obj, s, obj1);
            } while (true);
        }
        return jsonserializer;
    }

    public static char writeAfter(JSONSerializer jsonserializer, Object obj, char c)
    {
        Object obj1 = jsonserializer.getAfterFiltersDirect();
        char c1 = c;
        if (obj1 != null)
        {
            obj1 = ((List) (obj1)).iterator();
            do
            {
                c1 = c;
                if (!((Iterator) (obj1)).hasNext())
                {
                    break;
                }
                c = ((AfterFilter)((Iterator) (obj1)).next()).writeAfter(jsonserializer, obj, c);
            } while (true);
        }
        return c1;
    }

    public static char writeBefore(JSONSerializer jsonserializer, Object obj, char c)
    {
        Object obj1 = jsonserializer.getBeforeFiltersDirect();
        char c1 = c;
        if (obj1 != null)
        {
            obj1 = ((List) (obj1)).iterator();
            do
            {
                c1 = c;
                if (!((Iterator) (obj1)).hasNext())
                {
                    break;
                }
                c = ((BeforeFilter)((Iterator) (obj1)).next()).writeBefore(jsonserializer, obj, c);
            } while (true);
        }
        return c1;
    }
}
