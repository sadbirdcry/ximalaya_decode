// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.util.FieldInfo;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, FieldSerializer, NumberFieldSerializer, ObjectFieldSerializer, 
//            SerializerFeature, JSONSerializer, SerializeWriter, FilterUtils, 
//            SerialContext

public class JavaBeanSerializer
    implements ObjectSerializer
{

    private int features;
    private transient Map getterMap;
    private final FieldSerializer getters[];
    private final FieldSerializer sortedGetters[];

    public JavaBeanSerializer(Class class1)
    {
        this(class1, (Map)null);
    }

    public JavaBeanSerializer(Class class1, Map map)
    {
        features = 0;
        features = TypeUtils.getSerializeFeatures(class1);
        ArrayList arraylist = new ArrayList();
        for (Iterator iterator = TypeUtils.computeGetters(class1, map, false).iterator(); iterator.hasNext(); arraylist.add(createFieldSerializer((FieldInfo)iterator.next()))) { }
        getters = (FieldSerializer[])arraylist.toArray(new FieldSerializer[arraylist.size()]);
        arraylist = new ArrayList();
        for (class1 = TypeUtils.computeGetters(class1, map, true).iterator(); class1.hasNext(); arraylist.add(createFieldSerializer((FieldInfo)class1.next()))) { }
        sortedGetters = (FieldSerializer[])arraylist.toArray(new FieldSerializer[arraylist.size()]);
    }

    public transient JavaBeanSerializer(Class class1, String as[])
    {
        this(class1, createAliasMap(as));
    }

    static transient Map createAliasMap(String as[])
    {
        HashMap hashmap = new HashMap();
        int j = as.length;
        for (int i = 0; i < j; i++)
        {
            String s = as[i];
            hashmap.put(s, s);
        }

        return hashmap;
    }

    public FieldSerializer createFieldSerializer(FieldInfo fieldinfo)
    {
        if (fieldinfo.getFieldClass() == java/lang/Number)
        {
            return new NumberFieldSerializer(fieldinfo);
        } else
        {
            return new ObjectFieldSerializer(fieldinfo);
        }
    }

    public Object getFieldValue(Object obj, String s)
        throws Exception
    {
        s = (FieldSerializer)getGetterMap().get(s);
        if (s == null)
        {
            return null;
        } else
        {
            return s.getPropertyValue(obj);
        }
    }

    public List getFieldValues(Object obj)
        throws Exception
    {
        ArrayList arraylist = new ArrayList(sortedGetters.length);
        FieldSerializer afieldserializer[] = sortedGetters;
        int j = afieldserializer.length;
        for (int i = 0; i < j; i++)
        {
            arraylist.add(afieldserializer[i].getPropertyValue(obj));
        }

        return arraylist;
    }

    public Map getGetterMap()
    {
        if (getterMap == null)
        {
            HashMap hashmap = new HashMap(getters.length);
            FieldSerializer afieldserializer[] = sortedGetters;
            int j = afieldserializer.length;
            for (int i = 0; i < j; i++)
            {
                FieldSerializer fieldserializer = afieldserializer[i];
                hashmap.put(fieldserializer.getName(), fieldserializer);
            }

            getterMap = hashmap;
        }
        return getterMap;
    }

    public FieldSerializer[] getGetters()
    {
        return getters;
    }

    public boolean isWriteAsArray(JSONSerializer jsonserializer)
    {
        while (SerializerFeature.isEnabled(features, SerializerFeature.BeanToArray) || jsonserializer.isEnabled(SerializerFeature.BeanToArray)) 
        {
            return true;
        }
        return false;
    }

    protected boolean isWriteClassName(JSONSerializer jsonserializer, Object obj, Type type, Object obj1)
    {
        return jsonserializer.isWriteClassName(type, obj);
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        SerializeWriter serializewriter = jsonserializer.getWriter();
        if (obj != null) goto _L2; else goto _L1
_L1:
        serializewriter.writeNull();
_L4:
        return;
_L2:
        if (writeReference(jsonserializer, obj, i)) goto _L4; else goto _L3
_L3:
        char c;
        char c1;
        FieldSerializer afieldserializer[];
        SerialContext serialcontext;
        boolean flag1;
        boolean flag;
        boolean flag2;
        if (serializewriter.isEnabled(SerializerFeature.SortField))
        {
            afieldserializer = sortedGetters;
        } else
        {
            afieldserializer = getters;
        }
        serialcontext = jsonserializer.getContext();
        jsonserializer.setContext(serialcontext, obj, obj1, features, i);
        flag1 = isWriteAsArray(jsonserializer);
        if (flag1)
        {
            c1 = '[';
        } else
        {
            c1 = '{';
        }
        if (flag1)
        {
            c = ']';
        } else
        {
            c = '}';
        }
        serializewriter.append(c1);
        if (afieldserializer.length > 0 && serializewriter.isEnabled(SerializerFeature.PrettyFormat))
        {
            jsonserializer.incrementIndent();
            jsonserializer.println();
        }
        flag = false;
        i = ((flag) ? 1 : 0);
        if (!isWriteClassName(jsonserializer, obj, type, obj1)) goto _L6; else goto _L5
_L5:
        i = ((flag) ? 1 : 0);
        if (obj.getClass() == type) goto _L6; else goto _L7
_L7:
        serializewriter.writeFieldName(JSON.DEFAULT_TYPE_KEY);
        jsonserializer.write(obj.getClass());
        i = 1;
          goto _L6
_L13:
        if (j >= afieldserializer.length) goto _L9; else goto _L8
_L8:
        obj1 = afieldserializer[j];
        type = ((FieldSerializer) (obj1)).getField();
        if (!jsonserializer.isEnabled(SerializerFeature.SkipTransientField) || type == null) goto _L11; else goto _L10
_L10:
        flag2 = Modifier.isTransient(type.getModifiers());
        if (!flag2) goto _L11; else goto _L12
_L12:
        j++;
          goto _L13
_L39:
        c1 = '\0';
          goto _L14
_L11:
        if (!jsonserializer.isEnabled(SerializerFeature.IgnoreNonFieldGetter) || type != null) goto _L15; else goto _L12
_L15:
        if (FilterUtils.applyName(jsonserializer, obj, ((FieldSerializer) (obj1)).getName())) goto _L16; else goto _L12
_L16:
        if (FilterUtils.applyLabel(jsonserializer, ((FieldSerializer) (obj1)).getLabel())) goto _L17; else goto _L12
_L17:
        type = ((Type) (((FieldSerializer) (obj1)).getPropertyValue(obj)));
        if (FilterUtils.apply(jsonserializer, obj, ((FieldSerializer) (obj1)).getName(), type)) goto _L18; else goto _L12
_L18:
        s = FilterUtils.processKey(jsonserializer, obj, ((FieldSerializer) (obj1)).getName(), type);
        obj2 = FilterUtils.processValue(jsonserializer, obj, ((FieldSerializer) (obj1)).getName(), type);
        if (obj2 != null || flag1) goto _L20; else goto _L19
_L19:
        if (((FieldSerializer) (obj1)).isWriteNull() || jsonserializer.isEnabled(SerializerFeature.WriteMapNullValue)) goto _L20; else goto _L12
_L20:
        if (obj2 == null) goto _L22; else goto _L21
_L21:
        if (!jsonserializer.isEnabled(SerializerFeature.NotWriteDefaultValue)) goto _L22; else goto _L23
_L23:
        class1 = ((FieldSerializer) (obj1)).fieldInfo.getFieldClass();
        if (class1 != Byte.TYPE || !(obj2 instanceof Byte) || ((Byte)obj2).byteValue() != 0) goto _L24; else goto _L12
_L24:
        if (class1 != Short.TYPE || !(obj2 instanceof Short) || ((Short)obj2).shortValue() != 0) goto _L25; else goto _L12
_L25:
        if (class1 != Integer.TYPE || !(obj2 instanceof Integer) || ((Integer)obj2).intValue() != 0) goto _L26; else goto _L12
_L26:
        if (class1 != Long.TYPE || !(obj2 instanceof Long) || ((Long)obj2).longValue() != 0L) goto _L27; else goto _L12
_L27:
        if (class1 != Float.TYPE || !(obj2 instanceof Float) || ((Float)obj2).floatValue() != 0.0F) goto _L28; else goto _L12
_L28:
        if (class1 != Double.TYPE || !(obj2 instanceof Double) || ((Double)obj2).doubleValue() != 0.0D) goto _L29; else goto _L12
_L29:
        if (class1 != Boolean.TYPE || !(obj2 instanceof Boolean) || ((Boolean)obj2).booleanValue()) goto _L22; else goto _L12
_L22:
        if (i == 0)
        {
            break MISSING_BLOCK_LABEL_683;
        }
        serializewriter.append(',');
        if (serializewriter.isEnabled(SerializerFeature.PrettyFormat))
        {
            jsonserializer.println();
        }
        if (s == ((FieldSerializer) (obj1)).getName()) goto _L31; else goto _L30
_L30:
        if (flag1)
        {
            break MISSING_BLOCK_LABEL_704;
        }
        serializewriter.writeFieldName(s);
        jsonserializer.write(obj2);
          goto _L32
_L31:
        if (type == obj2) goto _L34; else goto _L33
_L33:
        if (flag1)
        {
            break MISSING_BLOCK_LABEL_730;
        }
        ((FieldSerializer) (obj1)).writePrefix(jsonserializer);
        jsonserializer.write(obj2);
          goto _L32
        obj;
        throw new JSONException("write javaBean error", ((Throwable) (obj)));
        obj;
        jsonserializer.setContext(serialcontext);
        throw obj;
_L34:
        if (flag1) goto _L36; else goto _L35
_L35:
        ((FieldSerializer) (obj1)).writeProperty(jsonserializer, obj2);
          goto _L32
_L36:
        ((FieldSerializer) (obj1)).writeValue(jsonserializer, obj2);
          goto _L32
_L37:
        FilterUtils.writeAfter(jsonserializer, obj, c1);
        if (afieldserializer.length > 0 && serializewriter.isEnabled(SerializerFeature.PrettyFormat))
        {
            jsonserializer.decrementIdent();
            jsonserializer.println();
        }
        serializewriter.append(c);
        jsonserializer.setContext(serialcontext);
        return;
_L41:
        c1 = '\0';
          goto _L37
_L6:
        if (i == 0) goto _L39; else goto _L38
_L38:
        c1 = ',';
_L14:
        String s;
        Object obj2;
        Class class1;
        int j;
        if (FilterUtils.writeBefore(jsonserializer, obj, c1) == ',')
        {
            i = 1;
        } else
        {
            i = 0;
        }
        j = 0;
          goto _L13
_L32:
        i = 1;
          goto _L12
_L9:
        if (i == 0) goto _L41; else goto _L40
_L40:
        c1 = ',';
          goto _L37
    }

    public boolean writeReference(JSONSerializer jsonserializer, Object obj, int i)
    {
        for (SerialContext serialcontext = jsonserializer.getContext(); serialcontext != null && SerializerFeature.isEnabled(serialcontext.getFeatures(), i, SerializerFeature.DisableCircularReferenceDetect) || !jsonserializer.containsReference(obj);)
        {
            return false;
        }

        jsonserializer.writeReference(obj);
        return true;
    }
}
