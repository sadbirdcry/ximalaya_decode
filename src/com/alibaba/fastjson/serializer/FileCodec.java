// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter

public class FileCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static FileCodec instance = new FileCodec();

    public FileCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        defaultjsonparser = ((DefaultJSONParser) (defaultjsonparser.parse()));
        if (defaultjsonparser == null)
        {
            return null;
        } else
        {
            return new File((String)defaultjsonparser);
        }
    }

    public int getFastMatchToken()
    {
        return 4;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        obj1 = jsonserializer.getWriter();
        if (obj == null)
        {
            ((SerializeWriter) (obj1)).writeNull();
            return;
        } else
        {
            jsonserializer.write(((File)obj).getPath());
            return;
        }
    }

}
