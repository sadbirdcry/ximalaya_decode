// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.util.HashMap;
import java.util.Map;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ASMSerializerFactory

static class beanSerializeFeatures
{

    private final int beanSerializeFeatures;
    private final String className;
    private int variantIndex;
    private Map variants;

    public int features()
    {
        return 5;
    }

    public int fieldName()
    {
        return 6;
    }

    public String getClassName()
    {
        return className;
    }

    public int getVariantCount()
    {
        return variantIndex;
    }

    public int obj()
    {
        return 2;
    }

    public int original()
    {
        return 7;
    }

    public int paramFieldName()
    {
        return 3;
    }

    public int paramFieldType()
    {
        return 4;
    }

    public int processValue()
    {
        return 8;
    }

    public int serializer()
    {
        return 1;
    }

    public int var(String s)
    {
        if ((Integer)variants.get(s) == null)
        {
            Map map = variants;
            int i = variantIndex;
            variantIndex = i + 1;
            map.put(s, Integer.valueOf(i));
        }
        return ((Integer)variants.get(s)).intValue();
    }

    public int var(String s, int i)
    {
        if ((Integer)variants.get(s) == null)
        {
            variants.put(s, Integer.valueOf(variantIndex));
            variantIndex = variantIndex + i;
        }
        return ((Integer)variants.get(s)).intValue();
    }


    public (String s, int i)
    {
        variantIndex = 9;
        variants = new HashMap();
        className = s;
        beanSerializeFeatures = i;
    }
}
