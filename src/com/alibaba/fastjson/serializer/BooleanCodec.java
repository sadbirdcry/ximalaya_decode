// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicBoolean;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class BooleanCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final BooleanCodec instance = new BooleanCodec();

    public BooleanCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        obj = defaultjsonparser.getLexer();
        if (((JSONLexer) (obj)).token() == 6)
        {
            ((JSONLexer) (obj)).nextToken(16);
            defaultjsonparser = Boolean.TRUE;
        } else
        if (((JSONLexer) (obj)).token() == 7)
        {
            ((JSONLexer) (obj)).nextToken(16);
            defaultjsonparser = Boolean.FALSE;
        } else
        if (((JSONLexer) (obj)).token() == 2)
        {
            int i = ((JSONLexer) (obj)).intValue();
            ((JSONLexer) (obj)).nextToken(16);
            if (i == 1)
            {
                defaultjsonparser = Boolean.TRUE;
            } else
            {
                defaultjsonparser = Boolean.FALSE;
            }
        } else
        {
            defaultjsonparser = ((DefaultJSONParser) (defaultjsonparser.parse()));
            if (defaultjsonparser == null)
            {
                return null;
            }
            defaultjsonparser = TypeUtils.castToBoolean(defaultjsonparser);
        }
        obj = defaultjsonparser;
        if (type == java/util/concurrent/atomic/AtomicBoolean)
        {
            obj = new AtomicBoolean(defaultjsonparser.booleanValue());
        }
        return obj;
    }

    public int getFastMatchToken()
    {
        return 6;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer = jsonserializer.getWriter();
        obj = (Boolean)obj;
        if (obj == null)
        {
            if (jsonserializer.isEnabled(SerializerFeature.WriteNullBooleanAsFalse))
            {
                jsonserializer.write("false");
                return;
            } else
            {
                jsonserializer.writeNull();
                return;
            }
        }
        if (((Boolean) (obj)).booleanValue())
        {
            jsonserializer.write("true");
            return;
        } else
        {
            jsonserializer.write("false");
            return;
        }
    }

}
