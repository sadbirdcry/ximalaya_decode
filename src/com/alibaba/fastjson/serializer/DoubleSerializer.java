// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DecimalFormat;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class DoubleSerializer
    implements ObjectSerializer
{

    public static final DoubleSerializer instance = new DoubleSerializer();
    private DecimalFormat decimalFormat;

    public DoubleSerializer()
    {
        decimalFormat = null;
    }

    public DoubleSerializer(String s)
    {
        this(new DecimalFormat(s));
    }

    public DoubleSerializer(DecimalFormat decimalformat)
    {
        decimalFormat = null;
        decimalFormat = decimalformat;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        type = jsonserializer.getWriter();
        if (obj != null) goto _L2; else goto _L1
_L1:
        if (!jsonserializer.isEnabled(SerializerFeature.WriteNullNumberAsZero)) goto _L4; else goto _L3
_L3:
        type.write('0');
_L6:
        return;
_L4:
        type.writeNull();
        return;
_L2:
        double d = ((Double)obj).doubleValue();
        if (Double.isNaN(d))
        {
            type.writeNull();
            return;
        }
        if (Double.isInfinite(d))
        {
            type.writeNull();
            return;
        }
        if (decimalFormat == null)
        {
            obj1 = Double.toString(d);
            obj = obj1;
            if (((String) (obj1)).endsWith(".0"))
            {
                obj = ((String) (obj1)).substring(0, ((String) (obj1)).length() - 2);
            }
        } else
        {
            obj = decimalFormat.format(d);
        }
        type.append(((CharSequence) (obj)));
        if (jsonserializer.isEnabled(SerializerFeature.WriteClassName))
        {
            type.write('D');
            return;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

}
