// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.lang.reflect.Type;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class ArraySerializer
    implements ObjectSerializer
{

    private final ObjectSerializer compObjectSerializer;
    private final Class componentType;

    public ArraySerializer(Class class1, ObjectSerializer objectserializer)
    {
        componentType = class1;
        compObjectSerializer = objectserializer;
    }

    public final void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        SerializeWriter serializewriter;
        Object aobj[];
        int j;
        serializewriter = jsonserializer.getWriter();
        if (obj == null)
        {
            if (serializewriter.isEnabled(SerializerFeature.WriteNullListAsEmpty))
            {
                serializewriter.write("[]");
                return;
            } else
            {
                serializewriter.writeNull();
                return;
            }
        }
        aobj = (Object[])(Object[])obj;
        j = aobj.length;
        type = jsonserializer.getContext();
        jsonserializer.setContext(type, obj, obj1, 0);
        serializewriter.append('[');
        i = 0;
_L2:
        if (i >= j)
        {
            break MISSING_BLOCK_LABEL_182;
        }
        if (i == 0)
        {
            break MISSING_BLOCK_LABEL_95;
        }
        serializewriter.append(',');
        obj = aobj[i];
        if (obj != null)
        {
            break MISSING_BLOCK_LABEL_116;
        }
        serializewriter.append("null");
        break MISSING_BLOCK_LABEL_197;
        if (obj.getClass() == componentType)
        {
            compObjectSerializer.write(jsonserializer, obj, Integer.valueOf(i), null, 0);
            break MISSING_BLOCK_LABEL_197;
        }
        break MISSING_BLOCK_LABEL_157;
        obj;
        jsonserializer.setContext(type);
        throw obj;
        jsonserializer.getObjectWriter(obj.getClass()).write(jsonserializer, obj, Integer.valueOf(i), null, 0);
        break MISSING_BLOCK_LABEL_197;
        serializewriter.append(']');
        jsonserializer.setContext(type);
        return;
        i++;
        if (true) goto _L2; else goto _L1
_L1:
    }
}
