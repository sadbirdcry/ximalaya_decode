// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigInteger;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class BigIntegerCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final BigIntegerCodec instance = new BigIntegerCodec();

    public BigIntegerCodec()
    {
    }

    public static Object deserialze(DefaultJSONParser defaultjsonparser)
    {
        JSONLexer jsonlexer = defaultjsonparser.getLexer();
        if (jsonlexer.token() == 2)
        {
            defaultjsonparser = jsonlexer.numberString();
            jsonlexer.nextToken(16);
            return new BigInteger(defaultjsonparser);
        }
        defaultjsonparser = ((DefaultJSONParser) (defaultjsonparser.parse()));
        if (defaultjsonparser == null)
        {
            return null;
        } else
        {
            return TypeUtils.castToBigInteger(defaultjsonparser);
        }
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        return deserialze(defaultjsonparser);
    }

    public int getFastMatchToken()
    {
        return 2;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer = jsonserializer.getWriter();
        if (obj == null)
        {
            if (jsonserializer.isEnabled(SerializerFeature.WriteNullNumberAsZero))
            {
                jsonserializer.write('0');
                return;
            } else
            {
                jsonserializer.writeNull();
                return;
            }
        } else
        {
            jsonserializer.write(((BigInteger)obj).toString());
            return;
        }
    }

}
