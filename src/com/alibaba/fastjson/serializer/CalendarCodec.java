// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.deserializer.DateDeserializer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.util.IOUtils;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter, SerializerFeature

public class CalendarCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final CalendarCodec instance = new CalendarCodec();

    public CalendarCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        defaultjsonparser = ((DefaultJSONParser) (DateDeserializer.instance.deserialze(defaultjsonparser, type, obj)));
        if (defaultjsonparser instanceof Calendar)
        {
            return defaultjsonparser;
        }
        defaultjsonparser = (Date)defaultjsonparser;
        if (defaultjsonparser == null)
        {
            return null;
        } else
        {
            type = Calendar.getInstance();
            type.setTime(defaultjsonparser);
            return type;
        }
    }

    public int getFastMatchToken()
    {
        return 2;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        obj1 = jsonserializer.getWriter();
        if (obj == null)
        {
            ((SerializeWriter) (obj1)).writeNull();
            return;
        }
        type = (Calendar)obj;
        if (jsonserializer.isEnabled(SerializerFeature.UseISO8601DateFormat))
        {
            int j;
            int k;
            int l;
            int i1;
            int j1;
            int k1;
            if (jsonserializer.isEnabled(SerializerFeature.UseSingleQuotes))
            {
                ((SerializeWriter) (obj1)).append('\'');
            } else
            {
                ((SerializeWriter) (obj1)).append('"');
            }
            i = type.get(1);
            j = type.get(2) + 1;
            k = type.get(5);
            l = type.get(11);
            i1 = type.get(12);
            j1 = type.get(13);
            k1 = type.get(14);
            if (k1 != 0)
            {
                obj = "0000-00-00T00:00:00.000".toCharArray();
                IOUtils.getChars(k1, 23, ((char []) (obj)));
                IOUtils.getChars(j1, 19, ((char []) (obj)));
                IOUtils.getChars(i1, 16, ((char []) (obj)));
                IOUtils.getChars(l, 13, ((char []) (obj)));
                IOUtils.getChars(k, 10, ((char []) (obj)));
                IOUtils.getChars(j, 7, ((char []) (obj)));
                IOUtils.getChars(i, 4, ((char []) (obj)));
            } else
            if (j1 == 0 && i1 == 0 && l == 0)
            {
                obj = "0000-00-00".toCharArray();
                IOUtils.getChars(k, 10, ((char []) (obj)));
                IOUtils.getChars(j, 7, ((char []) (obj)));
                IOUtils.getChars(i, 4, ((char []) (obj)));
            } else
            {
                obj = "0000-00-00T00:00:00".toCharArray();
                IOUtils.getChars(j1, 19, ((char []) (obj)));
                IOUtils.getChars(i1, 16, ((char []) (obj)));
                IOUtils.getChars(l, 13, ((char []) (obj)));
                IOUtils.getChars(k, 10, ((char []) (obj)));
                IOUtils.getChars(j, 7, ((char []) (obj)));
                IOUtils.getChars(i, 4, ((char []) (obj)));
            }
            ((SerializeWriter) (obj1)).write(((char []) (obj)));
            i = type.getTimeZone().getRawOffset() / 0x36ee80;
            if (i == 0)
            {
                ((SerializeWriter) (obj1)).append("Z");
            } else
            if (i > 0)
            {
                ((SerializeWriter) (obj1)).append("+").append(String.format("%02d", new Object[] {
                    Integer.valueOf(i)
                })).append(":00");
            } else
            {
                ((SerializeWriter) (obj1)).append("-").append(String.format("%02d", new Object[] {
                    Integer.valueOf(-i)
                })).append(":00");
            }
            if (jsonserializer.isEnabled(SerializerFeature.UseSingleQuotes))
            {
                ((SerializeWriter) (obj1)).append('\'');
                return;
            } else
            {
                ((SerializeWriter) (obj1)).append('"');
                return;
            }
        } else
        {
            jsonserializer.write(type.getTime());
            return;
        }
    }

}
