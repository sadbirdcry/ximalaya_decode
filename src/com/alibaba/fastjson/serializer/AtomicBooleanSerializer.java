// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicBoolean;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter

public class AtomicBooleanSerializer
    implements ObjectSerializer
{

    public static final AtomicBooleanSerializer instance = new AtomicBooleanSerializer();

    public AtomicBooleanSerializer()
    {
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer = jsonserializer.getWriter();
        if (((AtomicBoolean)obj).get())
        {
            jsonserializer.append("true");
            return;
        } else
        {
            jsonserializer.append("false");
            return;
        }
    }

}
