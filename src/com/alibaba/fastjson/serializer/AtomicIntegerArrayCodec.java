// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicIntegerArray;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class AtomicIntegerArrayCodec
    implements ObjectDeserializer, ObjectSerializer
{

    public static final AtomicIntegerArrayCodec instance = new AtomicIntegerArrayCodec();

    public AtomicIntegerArrayCodec()
    {
    }

    public Object deserialze(DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        if (defaultjsonparser.getLexer().token() == 8)
        {
            defaultjsonparser.getLexer().nextToken(16);
            return null;
        }
        type = new JSONArray();
        defaultjsonparser.parseArray(type);
        defaultjsonparser = new AtomicIntegerArray(type.size());
        for (int i = 0; i < type.size(); i++)
        {
            defaultjsonparser.set(i, type.getInteger(i).intValue());
        }

        return defaultjsonparser;
    }

    public int getFastMatchToken()
    {
        return 14;
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        jsonserializer = jsonserializer.getWriter();
        if (obj == null)
        {
            if (jsonserializer.isEnabled(SerializerFeature.WriteNullListAsEmpty))
            {
                jsonserializer.write("[]");
                return;
            } else
            {
                jsonserializer.writeNull();
                return;
            }
        }
        obj = (AtomicIntegerArray)obj;
        int j = ((AtomicIntegerArray) (obj)).length();
        jsonserializer.append('[');
        for (i = 0; i < j; i++)
        {
            int k = ((AtomicIntegerArray) (obj)).get(i);
            if (i != 0)
            {
                jsonserializer.write(',');
            }
            jsonserializer.writeInt(k);
        }

        jsonserializer.append(']');
    }

}
