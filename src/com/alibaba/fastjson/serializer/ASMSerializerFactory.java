// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.alibaba.fastjson.asm.ClassWriter;
import com.alibaba.fastjson.asm.FieldVisitor;
import com.alibaba.fastjson.asm.Label;
import com.alibaba.fastjson.asm.MethodVisitor;
import com.alibaba.fastjson.asm.Opcodes;
import com.alibaba.fastjson.asm.Type;
import com.alibaba.fastjson.util.ASMClassLoader;
import com.alibaba.fastjson.util.ASMUtils;
import com.alibaba.fastjson.util.FieldInfo;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            SerializerFeature, ObjectSerializer

public class ASMSerializerFactory
    implements Opcodes
{
    static class Context
    {

        private final int beanSerializeFeatures;
        private final String className;
        private int variantIndex;
        private Map variants;

        public int features()
        {
            return 5;
        }

        public int fieldName()
        {
            return 6;
        }

        public String getClassName()
        {
            return className;
        }

        public int getVariantCount()
        {
            return variantIndex;
        }

        public int obj()
        {
            return 2;
        }

        public int original()
        {
            return 7;
        }

        public int paramFieldName()
        {
            return 3;
        }

        public int paramFieldType()
        {
            return 4;
        }

        public int processValue()
        {
            return 8;
        }

        public int serializer()
        {
            return 1;
        }

        public int var(String s)
        {
            if ((Integer)variants.get(s) == null)
            {
                Map map = variants;
                int i = variantIndex;
                variantIndex = i + 1;
                map.put(s, Integer.valueOf(i));
            }
            return ((Integer)variants.get(s)).intValue();
        }

        public int var(String s, int i)
        {
            if ((Integer)variants.get(s) == null)
            {
                variants.put(s, Integer.valueOf(variantIndex));
                variantIndex = variantIndex + i;
            }
            return ((Integer)variants.get(s)).intValue();
        }


        public Context(String s, int i)
        {
            variantIndex = 9;
            variants = new HashMap();
            className = s;
            beanSerializeFeatures = i;
        }
    }


    private ASMClassLoader classLoader;
    private final AtomicLong seed = new AtomicLong();

    public ASMSerializerFactory()
    {
        classLoader = new ASMClassLoader();
    }

    private void _after(MethodVisitor methodvisitor, Context context)
    {
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitVarInsn(25, context.obj());
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "writeAfter", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;C)C");
        methodvisitor.visitVarInsn(54, context.var("seperator"));
    }

    private void _apply(MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        fieldinfo = fieldinfo.getFieldClass();
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitVarInsn(25, context.obj());
        methodvisitor.visitVarInsn(25, context.fieldName());
        if (fieldinfo == Byte.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("byte"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "apply", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;B)Z");
            return;
        }
        if (fieldinfo == Short.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("short"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "apply", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;S)Z");
            return;
        }
        if (fieldinfo == Integer.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("int"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "apply", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;I)Z");
            return;
        }
        if (fieldinfo == Character.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("char"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "apply", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;C)Z");
            return;
        }
        if (fieldinfo == Long.TYPE)
        {
            methodvisitor.visitVarInsn(22, context.var("long", 2));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "apply", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;J)Z");
            return;
        }
        if (fieldinfo == Float.TYPE)
        {
            methodvisitor.visitVarInsn(23, context.var("float"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "apply", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;F)Z");
            return;
        }
        if (fieldinfo == Double.TYPE)
        {
            methodvisitor.visitVarInsn(24, context.var("double", 2));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "apply", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;D)Z");
            return;
        }
        if (fieldinfo == Boolean.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("boolean"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "apply", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;B)Z");
            return;
        }
        if (fieldinfo == java/math/BigDecimal)
        {
            methodvisitor.visitVarInsn(25, context.var("decimal"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "apply", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Z");
            return;
        }
        if (fieldinfo == java/lang/String)
        {
            methodvisitor.visitVarInsn(25, context.var("string"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "apply", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Z");
            return;
        }
        if (fieldinfo.isEnum())
        {
            methodvisitor.visitVarInsn(25, context.var("enum"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "apply", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Z");
            return;
        }
        if (java/util/List.isAssignableFrom(fieldinfo))
        {
            methodvisitor.visitVarInsn(25, context.var("list"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "apply", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Z");
            return;
        } else
        {
            methodvisitor.visitVarInsn(25, context.var("object"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "apply", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Z");
            return;
        }
    }

    private void _before(MethodVisitor methodvisitor, Context context)
    {
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitVarInsn(25, context.obj());
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "writeBefore", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;C)C");
        methodvisitor.visitVarInsn(54, context.var("seperator"));
    }

    private void _boolean(Class class1, MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        class1 = new Label();
        _nameApply(methodvisitor, fieldinfo, context, class1);
        _get(methodvisitor, context, fieldinfo);
        methodvisitor.visitVarInsn(54, context.var("boolean"));
        _filters(methodvisitor, fieldinfo, context, class1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitVarInsn(21, context.var("boolean"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;Z)V");
        _seperator(methodvisitor, context);
        methodvisitor.visitLabel(class1);
    }

    private void _byte(Class class1, MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        class1 = new Label();
        _nameApply(methodvisitor, fieldinfo, context, class1);
        _get(methodvisitor, context, fieldinfo);
        methodvisitor.visitVarInsn(54, context.var("byte"));
        _filters(methodvisitor, fieldinfo, context, class1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitVarInsn(21, context.var("byte"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;I)V");
        _seperator(methodvisitor, context);
        methodvisitor.visitLabel(class1);
    }

    private void _char(Class class1, MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        class1 = new Label();
        _nameApply(methodvisitor, fieldinfo, context, class1);
        _get(methodvisitor, context, fieldinfo);
        methodvisitor.visitVarInsn(54, context.var("char"));
        _filters(methodvisitor, fieldinfo, context, class1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitVarInsn(21, context.var("char"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;C)V");
        _seperator(methodvisitor, context);
        methodvisitor.visitLabel(class1);
    }

    private void _decimal(Class class1, MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        class1 = new Label();
        _nameApply(methodvisitor, fieldinfo, context, class1);
        _get(methodvisitor, context, fieldinfo);
        methodvisitor.visitVarInsn(58, context.var("decimal"));
        _filters(methodvisitor, fieldinfo, context, class1);
        Label label = new Label();
        Label label1 = new Label();
        Label label2 = new Label();
        methodvisitor.visitLabel(label);
        methodvisitor.visitVarInsn(25, context.var("decimal"));
        methodvisitor.visitJumpInsn(199, label1);
        _if_write_null(methodvisitor, fieldinfo, context);
        methodvisitor.visitJumpInsn(167, label2);
        methodvisitor.visitLabel(label1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitVarInsn(25, context.var("decimal"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;Ljava/math/BigDecimal;)V");
        _seperator(methodvisitor, context);
        methodvisitor.visitJumpInsn(167, label2);
        methodvisitor.visitLabel(label2);
        methodvisitor.visitLabel(class1);
    }

    private void _double(Class class1, MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        class1 = new Label();
        _nameApply(methodvisitor, fieldinfo, context, class1);
        _get(methodvisitor, context, fieldinfo);
        methodvisitor.visitVarInsn(57, context.var("double", 2));
        _filters(methodvisitor, fieldinfo, context, class1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitVarInsn(24, context.var("double", 2));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;D)V");
        _seperator(methodvisitor, context);
        methodvisitor.visitLabel(class1);
    }

    private void _enum(Class class1, MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        int i = 0;
        class1 = (JSONField)fieldinfo.getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        boolean flag1;
        if (class1 != null)
        {
            class1 = class1.serialzeFeatures();
            int j = class1.length;
            boolean flag = false;
            do
            {
                flag1 = flag;
                if (i >= j)
                {
                    break;
                }
                if (class1[i] == SerializerFeature.WriteEnumUsingToString)
                {
                    flag = true;
                }
                i++;
            } while (true);
        } else
        {
            flag1 = false;
        }
        class1 = new Label();
        Label label = new Label();
        Label label1 = new Label();
        _nameApply(methodvisitor, fieldinfo, context, label1);
        _get(methodvisitor, context, fieldinfo);
        methodvisitor.visitTypeInsn(192, "java/lang/Enum");
        methodvisitor.visitVarInsn(58, context.var("enum"));
        _filters(methodvisitor, fieldinfo, context, label1);
        methodvisitor.visitVarInsn(25, context.var("enum"));
        methodvisitor.visitJumpInsn(199, class1);
        _if_write_null(methodvisitor, fieldinfo, context);
        methodvisitor.visitJumpInsn(167, label);
        methodvisitor.visitLabel(class1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitVarInsn(25, context.var("enum"));
        if (flag1)
        {
            methodvisitor.visitMethodInsn(182, "java/lang/Object", "toString", "()Ljava/lang/String;");
            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;Ljava/lang/String;)V");
        } else
        {
            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;Ljava/lang/Enum;)V");
        }
        _seperator(methodvisitor, context);
        methodvisitor.visitLabel(label);
        methodvisitor.visitLabel(label1);
    }

    private void _filters(MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context, Label label)
    {
        if (fieldinfo.getField() != null && Modifier.isTransient(fieldinfo.getField().getModifiers()))
        {
            methodvisitor.visitVarInsn(25, context.var("out"));
            methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/serializer/SerializerFeature", "SkipTransientField", "Lcom/alibaba/fastjson/serializer/SerializerFeature;");
            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "isEnabled", "(Lcom/alibaba/fastjson/serializer/SerializerFeature;)Z");
            methodvisitor.visitJumpInsn(154, label);
        }
        _notWriteDefault(methodvisitor, fieldinfo, context, label);
        _apply(methodvisitor, fieldinfo, context);
        methodvisitor.visitJumpInsn(153, label);
        _processKey(methodvisitor, fieldinfo, context);
        Label label1 = new Label();
        _processValue(methodvisitor, fieldinfo, context);
        methodvisitor.visitVarInsn(25, context.original());
        methodvisitor.visitVarInsn(25, context.processValue());
        methodvisitor.visitJumpInsn(165, label1);
        _writeObject(methodvisitor, fieldinfo, context, label);
        methodvisitor.visitJumpInsn(167, label);
        methodvisitor.visitLabel(label1);
    }

    private void _float(Class class1, MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        class1 = new Label();
        _nameApply(methodvisitor, fieldinfo, context, class1);
        _get(methodvisitor, context, fieldinfo);
        methodvisitor.visitVarInsn(56, context.var("float"));
        _filters(methodvisitor, fieldinfo, context, class1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitVarInsn(23, context.var("float"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;F)V");
        _seperator(methodvisitor, context);
        methodvisitor.visitLabel(class1);
    }

    private void _get(MethodVisitor methodvisitor, Context context, FieldInfo fieldinfo)
    {
        Method method = fieldinfo.getMethod();
        if (method != null)
        {
            methodvisitor.visitVarInsn(25, context.var("entity"));
            methodvisitor.visitMethodInsn(182, ASMUtils.getType(method.getDeclaringClass()), method.getName(), ASMUtils.getDesc(method));
            return;
        } else
        {
            methodvisitor.visitVarInsn(25, context.var("entity"));
            methodvisitor.visitFieldInsn(180, ASMUtils.getType(fieldinfo.getDeclaringClass()), fieldinfo.getField().getName(), ASMUtils.getDesc(fieldinfo.getFieldClass()));
            return;
        }
    }

    private void _if_write_null(MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        Class class1 = fieldinfo.getFieldClass();
        Label label3 = new Label();
        Label label = new Label();
        Label label1 = new Label();
        Label label2 = new Label();
        methodvisitor.visitLabel(label3);
        fieldinfo = (JSONField)fieldinfo.getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        boolean flag5;
        boolean flag6;
        boolean flag7;
        boolean flag8;
        boolean flag9;
        if (fieldinfo != null)
        {
            fieldinfo = fieldinfo.serialzeFeatures();
            int j = fieldinfo.length;
            boolean flag4 = false;
            boolean flag3 = false;
            boolean flag2 = false;
            boolean flag1 = false;
            boolean flag = false;
            int i = 0;
            do
            {
                flag9 = flag4;
                flag8 = flag3;
                flag7 = flag2;
                flag6 = flag1;
                flag5 = flag;
                if (i >= j)
                {
                    break;
                }
                SerializerFeature serializerfeature = fieldinfo[i];
                if (serializerfeature == SerializerFeature.WriteMapNullValue)
                {
                    flag8 = true;
                    flag7 = flag1;
                    flag6 = flag2;
                    flag5 = flag3;
                } else
                if (serializerfeature == SerializerFeature.WriteNullNumberAsZero)
                {
                    flag7 = true;
                    flag5 = flag3;
                    flag6 = flag2;
                    flag8 = flag;
                } else
                if (serializerfeature == SerializerFeature.WriteNullStringAsEmpty)
                {
                    flag6 = true;
                    flag5 = flag3;
                    flag7 = flag1;
                    flag8 = flag;
                } else
                if (serializerfeature == SerializerFeature.WriteNullBooleanAsFalse)
                {
                    flag5 = true;
                    flag6 = flag2;
                    flag7 = flag1;
                    flag8 = flag;
                } else
                {
                    flag5 = flag3;
                    flag6 = flag2;
                    flag7 = flag1;
                    flag8 = flag;
                    if (serializerfeature == SerializerFeature.WriteNullListAsEmpty)
                    {
                        flag4 = true;
                        flag5 = flag3;
                        flag6 = flag2;
                        flag7 = flag1;
                        flag8 = flag;
                    }
                }
                i++;
                flag3 = flag5;
                flag2 = flag6;
                flag1 = flag7;
                flag = flag8;
            } while (true);
        } else
        {
            flag9 = false;
            flag8 = false;
            flag7 = false;
            flag6 = false;
            flag5 = false;
        }
        if (!flag5)
        {
            methodvisitor.visitVarInsn(25, context.var("out"));
            methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/serializer/SerializerFeature", "WriteMapNullValue", "Lcom/alibaba/fastjson/serializer/SerializerFeature;");
            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "isEnabled", "(Lcom/alibaba/fastjson/serializer/SerializerFeature;)Z");
            methodvisitor.visitJumpInsn(153, label);
        }
        methodvisitor.visitLabel(label1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        if (class1 == java/lang/String || class1 == java/lang/Character)
        {
            if (flag7)
            {
                methodvisitor.visitLdcInsn("");
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;Ljava/lang/String;)V");
            } else
            {
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldNullString", "(CLjava/lang/String;)V");
            }
        } else
        if (java/lang/Number.isAssignableFrom(class1))
        {
            if (flag6)
            {
                methodvisitor.visitInsn(3);
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;I)V");
            } else
            {
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldNullNumber", "(CLjava/lang/String;)V");
            }
        } else
        if (class1 == java/lang/Boolean)
        {
            if (flag8)
            {
                methodvisitor.visitInsn(3);
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;Z)V");
            } else
            {
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldNullBoolean", "(CLjava/lang/String;)V");
            }
        } else
        if (java/util/Collection.isAssignableFrom(class1) || class1.isArray())
        {
            if (flag9)
            {
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldEmptyList", "(CLjava/lang/String;)V");
            } else
            {
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldNullList", "(CLjava/lang/String;)V");
            }
        } else
        {
            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldNull", "(CLjava/lang/String;)V");
        }
        _seperator(methodvisitor, context);
        methodvisitor.visitJumpInsn(167, label2);
        methodvisitor.visitLabel(label);
        methodvisitor.visitLabel(label2);
    }

    private void _int(Class class1, MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        class1 = new Label();
        _nameApply(methodvisitor, fieldinfo, context, class1);
        _get(methodvisitor, context, fieldinfo);
        methodvisitor.visitVarInsn(54, context.var("int"));
        _filters(methodvisitor, fieldinfo, context, class1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitVarInsn(21, context.var("int"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;I)V");
        _seperator(methodvisitor, context);
        methodvisitor.visitLabel(class1);
    }

    private void _labelApply(MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context, Label label)
    {
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitLdcInsn(fieldinfo.getLabel());
        methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "applyLabel", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/String;)Z");
        methodvisitor.visitJumpInsn(153, label);
    }

    private void _list(Class class1, MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        class1 = fieldinfo.getFieldType();
        Class class2;
        Label label;
        Label label1;
        Label label2;
        Label label3;
        Label label4;
        if (class1 instanceof Class)
        {
            class1 = java/lang/Object;
        } else
        {
            class1 = ((ParameterizedType)class1).getActualTypeArguments()[0];
        }
        if (class1 instanceof Class)
        {
            class2 = (Class)class1;
        } else
        {
            class2 = null;
        }
        label = new Label();
        label2 = new Label();
        label3 = new Label();
        label1 = new Label();
        methodvisitor.visitLabel(label2);
        _nameApply(methodvisitor, fieldinfo, context, label);
        _get(methodvisitor, context, fieldinfo);
        methodvisitor.visitTypeInsn(192, "java/util/List");
        methodvisitor.visitVarInsn(58, context.var("list"));
        _filters(methodvisitor, fieldinfo, context, label);
        methodvisitor.visitVarInsn(25, context.var("list"));
        methodvisitor.visitJumpInsn(199, label3);
        _if_write_null(methodvisitor, fieldinfo, context);
        methodvisitor.visitJumpInsn(167, label1);
        methodvisitor.visitLabel(label3);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "write", "(C)V");
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldName", "(Ljava/lang/String;)V");
        methodvisitor.visitVarInsn(25, context.var("list"));
        methodvisitor.visitMethodInsn(185, "java/util/List", "size", "()I");
        methodvisitor.visitVarInsn(54, context.var("int"));
        label3 = new Label();
        label4 = new Label();
        label2 = new Label();
        methodvisitor.visitLabel(label3);
        methodvisitor.visitVarInsn(21, context.var("int"));
        methodvisitor.visitInsn(3);
        methodvisitor.visitJumpInsn(160, label4);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitLdcInsn("[]");
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "write", "(Ljava/lang/String;)V");
        methodvisitor.visitJumpInsn(167, label2);
        methodvisitor.visitLabel(label4);
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitVarInsn(25, context.var("list"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "setContext", "(Ljava/lang/Object;Ljava/lang/Object;)V");
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(16, 91);
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "write", "(C)V");
        methodvisitor.visitInsn(1);
        methodvisitor.visitTypeInsn(192, "com/alibaba/fastjson/serializer/ObjectSerializer");
        methodvisitor.visitVarInsn(58, context.var("list_ser"));
        label3 = new Label();
        label4 = new Label();
        methodvisitor.visitInsn(3);
        methodvisitor.visitVarInsn(54, context.var("i"));
        methodvisitor.visitLabel(label3);
        methodvisitor.visitVarInsn(21, context.var("i"));
        methodvisitor.visitVarInsn(21, context.var("int"));
        methodvisitor.visitInsn(4);
        methodvisitor.visitInsn(100);
        methodvisitor.visitJumpInsn(162, label4);
        if (class1 == java/lang/String)
        {
            methodvisitor.visitVarInsn(25, context.var("out"));
            methodvisitor.visitVarInsn(25, context.var("list"));
            methodvisitor.visitVarInsn(21, context.var("i"));
            methodvisitor.visitMethodInsn(185, "java/util/List", "get", "(I)Ljava/lang/Object;");
            methodvisitor.visitTypeInsn(192, "java/lang/String");
            methodvisitor.visitVarInsn(16, 44);
            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeString", "(Ljava/lang/String;C)V");
        } else
        {
            methodvisitor.visitVarInsn(25, context.serializer());
            methodvisitor.visitVarInsn(25, context.var("list"));
            methodvisitor.visitVarInsn(21, context.var("i"));
            methodvisitor.visitMethodInsn(185, "java/util/List", "get", "(I)Ljava/lang/Object;");
            methodvisitor.visitVarInsn(21, context.var("i"));
            methodvisitor.visitMethodInsn(184, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
            if (class2 != null && Modifier.isPublic(class2.getModifiers()))
            {
                methodvisitor.visitLdcInsn(Type.getType(ASMUtils.getDesc((Class)class1)));
                methodvisitor.visitLdcInsn(Integer.valueOf(fieldinfo.getSerialzeFeatures()));
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "writeWithFieldName", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
            } else
            {
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "writeWithFieldName", "(Ljava/lang/Object;Ljava/lang/Object;)V");
            }
            methodvisitor.visitVarInsn(25, context.var("out"));
            methodvisitor.visitVarInsn(16, 44);
            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "write", "(C)V");
        }
        methodvisitor.visitIincInsn(context.var("i"), 1);
        methodvisitor.visitJumpInsn(167, label3);
        methodvisitor.visitLabel(label4);
        if (class1 == java/lang/String)
        {
            methodvisitor.visitVarInsn(25, context.var("out"));
            methodvisitor.visitVarInsn(25, context.var("list"));
            methodvisitor.visitVarInsn(21, context.var("int"));
            methodvisitor.visitInsn(4);
            methodvisitor.visitInsn(100);
            methodvisitor.visitMethodInsn(185, "java/util/List", "get", "(I)Ljava/lang/Object;");
            methodvisitor.visitTypeInsn(192, "java/lang/String");
            methodvisitor.visitVarInsn(16, 93);
            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeString", "(Ljava/lang/String;C)V");
        } else
        {
            methodvisitor.visitVarInsn(25, context.serializer());
            methodvisitor.visitVarInsn(25, context.var("list"));
            methodvisitor.visitVarInsn(21, context.var("i"));
            methodvisitor.visitMethodInsn(185, "java/util/List", "get", "(I)Ljava/lang/Object;");
            methodvisitor.visitVarInsn(21, context.var("i"));
            methodvisitor.visitMethodInsn(184, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
            if (class2 != null && Modifier.isPublic(class2.getModifiers()))
            {
                methodvisitor.visitLdcInsn(Type.getType(ASMUtils.getDesc((Class)class1)));
                methodvisitor.visitLdcInsn(Integer.valueOf(fieldinfo.getSerialzeFeatures()));
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "writeWithFieldName", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
            } else
            {
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "writeWithFieldName", "(Ljava/lang/Object;Ljava/lang/Object;)V");
            }
            methodvisitor.visitVarInsn(25, context.var("out"));
            methodvisitor.visitVarInsn(16, 93);
            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "write", "(C)V");
        }
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "popContext", "()V");
        methodvisitor.visitLabel(label2);
        _seperator(methodvisitor, context);
        methodvisitor.visitLabel(label1);
        methodvisitor.visitLabel(label);
    }

    private void _long(Class class1, MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        class1 = new Label();
        _nameApply(methodvisitor, fieldinfo, context, class1);
        _get(methodvisitor, context, fieldinfo);
        methodvisitor.visitVarInsn(55, context.var("long", 2));
        _filters(methodvisitor, fieldinfo, context, class1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitVarInsn(22, context.var("long", 2));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;J)V");
        _seperator(methodvisitor, context);
        methodvisitor.visitLabel(class1);
    }

    private void _nameApply(MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context, Label label)
    {
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitVarInsn(25, context.obj());
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "applyName", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;)Z");
        methodvisitor.visitJumpInsn(153, label);
        _labelApply(methodvisitor, fieldinfo, context, label);
        if (fieldinfo.getField() == null)
        {
            methodvisitor.visitVarInsn(25, context.var("out"));
            methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/serializer/SerializerFeature", "IgnoreNonFieldGetter", "Lcom/alibaba/fastjson/serializer/SerializerFeature;");
            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "isEnabled", "(Lcom/alibaba/fastjson/serializer/SerializerFeature;)Z");
            methodvisitor.visitJumpInsn(154, label);
        }
    }

    private void _notWriteDefault(MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context, Label label)
    {
        Label label1;
        label1 = new Label();
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/serializer/SerializerFeature", "NotWriteDefaultValue", "Lcom/alibaba/fastjson/serializer/SerializerFeature;");
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "isEnabled", "(Lcom/alibaba/fastjson/serializer/SerializerFeature;)Z");
        methodvisitor.visitJumpInsn(153, label1);
        fieldinfo = fieldinfo.getFieldClass();
        if (fieldinfo != Boolean.TYPE) goto _L2; else goto _L1
_L1:
        methodvisitor.visitVarInsn(21, context.var("boolean"));
        methodvisitor.visitJumpInsn(153, label);
_L4:
        methodvisitor.visitLabel(label1);
        return;
_L2:
        if (fieldinfo == Byte.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("byte"));
            methodvisitor.visitJumpInsn(153, label);
        } else
        if (fieldinfo == Short.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("short"));
            methodvisitor.visitJumpInsn(153, label);
        } else
        if (fieldinfo == Integer.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("int"));
            methodvisitor.visitJumpInsn(153, label);
        } else
        if (fieldinfo == Long.TYPE)
        {
            methodvisitor.visitVarInsn(22, context.var("long"));
            methodvisitor.visitInsn(9);
            methodvisitor.visitInsn(148);
            methodvisitor.visitJumpInsn(153, label);
        } else
        if (fieldinfo == Float.TYPE)
        {
            methodvisitor.visitVarInsn(23, context.var("float"));
            methodvisitor.visitInsn(11);
            methodvisitor.visitInsn(149);
            methodvisitor.visitJumpInsn(153, label);
        } else
        if (fieldinfo == Double.TYPE)
        {
            methodvisitor.visitVarInsn(24, context.var("double"));
            methodvisitor.visitInsn(14);
            methodvisitor.visitInsn(151);
            methodvisitor.visitJumpInsn(153, label);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private void _object(Class class1, MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        class1 = new Label();
        _nameApply(methodvisitor, fieldinfo, context, class1);
        _get(methodvisitor, context, fieldinfo);
        methodvisitor.visitVarInsn(58, context.var("object"));
        _filters(methodvisitor, fieldinfo, context, class1);
        _writeObject(methodvisitor, fieldinfo, context, class1);
        methodvisitor.visitLabel(class1);
    }

    private void _processKey(MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        fieldinfo = fieldinfo.getFieldClass();
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitVarInsn(25, context.obj());
        methodvisitor.visitVarInsn(25, context.fieldName());
        if (fieldinfo == Byte.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("byte"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processKey", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;B)Ljava/lang/String;");
        } else
        if (fieldinfo == Short.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("short"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processKey", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;S)Ljava/lang/String;");
        } else
        if (fieldinfo == Integer.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("int"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processKey", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;I)Ljava/lang/String;");
        } else
        if (fieldinfo == Character.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("char"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processKey", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;C)Ljava/lang/String;");
        } else
        if (fieldinfo == Long.TYPE)
        {
            methodvisitor.visitVarInsn(22, context.var("long", 2));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processKey", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;J)Ljava/lang/String;");
        } else
        if (fieldinfo == Float.TYPE)
        {
            methodvisitor.visitVarInsn(23, context.var("float"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processKey", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;F)Ljava/lang/String;");
        } else
        if (fieldinfo == Double.TYPE)
        {
            methodvisitor.visitVarInsn(24, context.var("double", 2));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processKey", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;D)Ljava/lang/String;");
        } else
        if (fieldinfo == Boolean.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("boolean"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processKey", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;Z)Ljava/lang/String;");
        } else
        if (fieldinfo == java/math/BigDecimal)
        {
            methodvisitor.visitVarInsn(25, context.var("decimal"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processKey", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;");
        } else
        if (fieldinfo == java/lang/String)
        {
            methodvisitor.visitVarInsn(25, context.var("string"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processKey", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;");
        } else
        if (fieldinfo.isEnum())
        {
            methodvisitor.visitVarInsn(25, context.var("enum"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processKey", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;");
        } else
        if (java/util/List.isAssignableFrom(fieldinfo))
        {
            methodvisitor.visitVarInsn(25, context.var("list"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processKey", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;");
        } else
        {
            methodvisitor.visitVarInsn(25, context.var("object"));
            methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processKey", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;");
        }
        methodvisitor.visitVarInsn(58, context.fieldName());
    }

    private void _processValue(MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        fieldinfo = fieldinfo.getFieldClass();
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitVarInsn(25, context.obj());
        methodvisitor.visitVarInsn(25, context.fieldName());
        if (fieldinfo == Byte.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("byte"));
            methodvisitor.visitMethodInsn(184, "java/lang/Byte", "valueOf", "(B)Ljava/lang/Byte;");
        } else
        if (fieldinfo == Short.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("short"));
            methodvisitor.visitMethodInsn(184, "java/lang/Short", "valueOf", "(S)Ljava/lang/Short;");
        } else
        if (fieldinfo == Integer.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("int"));
            methodvisitor.visitMethodInsn(184, "java/lang/Integer", "valueOf", "(I)Ljava/lang/Integer;");
        } else
        if (fieldinfo == Character.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("char"));
            methodvisitor.visitMethodInsn(184, "java/lang/Character", "valueOf", "(C)Ljava/lang/Character;");
        } else
        if (fieldinfo == Long.TYPE)
        {
            methodvisitor.visitVarInsn(22, context.var("long", 2));
            methodvisitor.visitMethodInsn(184, "java/lang/Long", "valueOf", "(J)Ljava/lang/Long;");
        } else
        if (fieldinfo == Float.TYPE)
        {
            methodvisitor.visitVarInsn(23, context.var("float"));
            methodvisitor.visitMethodInsn(184, "java/lang/Float", "valueOf", "(F)Ljava/lang/Float;");
        } else
        if (fieldinfo == Double.TYPE)
        {
            methodvisitor.visitVarInsn(24, context.var("double", 2));
            methodvisitor.visitMethodInsn(184, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;");
        } else
        if (fieldinfo == Boolean.TYPE)
        {
            methodvisitor.visitVarInsn(21, context.var("boolean"));
            methodvisitor.visitMethodInsn(184, "java/lang/Boolean", "valueOf", "(Z)Ljava/lang/Boolean;");
        } else
        if (fieldinfo == java/math/BigDecimal)
        {
            methodvisitor.visitVarInsn(25, context.var("decimal"));
        } else
        if (fieldinfo == java/lang/String)
        {
            methodvisitor.visitVarInsn(25, context.var("string"));
        } else
        if (fieldinfo.isEnum())
        {
            methodvisitor.visitVarInsn(25, context.var("enum"));
        } else
        if (java/util/List.isAssignableFrom(fieldinfo))
        {
            methodvisitor.visitVarInsn(25, context.var("list"));
        } else
        {
            methodvisitor.visitVarInsn(25, context.var("object"));
        }
        methodvisitor.visitVarInsn(58, context.original());
        methodvisitor.visitVarInsn(25, context.original());
        methodvisitor.visitMethodInsn(184, "com/alibaba/fastjson/serializer/FilterUtils", "processValue", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;");
        methodvisitor.visitVarInsn(58, context.processValue());
    }

    private void _seperator(MethodVisitor methodvisitor, Context context)
    {
        methodvisitor.visitVarInsn(16, 44);
        methodvisitor.visitVarInsn(54, context.var("seperator"));
    }

    private void _short(Class class1, MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        class1 = new Label();
        _nameApply(methodvisitor, fieldinfo, context, class1);
        _get(methodvisitor, context, fieldinfo);
        methodvisitor.visitVarInsn(54, context.var("short"));
        _filters(methodvisitor, fieldinfo, context, class1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitVarInsn(21, context.var("short"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;I)V");
        _seperator(methodvisitor, context);
        methodvisitor.visitLabel(class1);
    }

    private void _string(Class class1, MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context)
    {
        class1 = new Label();
        _nameApply(methodvisitor, fieldinfo, context, class1);
        _get(methodvisitor, context, fieldinfo);
        methodvisitor.visitVarInsn(58, context.var("string"));
        _filters(methodvisitor, fieldinfo, context, class1);
        Label label = new Label();
        Label label1 = new Label();
        methodvisitor.visitVarInsn(25, context.var("string"));
        methodvisitor.visitJumpInsn(199, label);
        _if_write_null(methodvisitor, fieldinfo, context);
        methodvisitor.visitJumpInsn(167, label1);
        methodvisitor.visitLabel(label);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitVarInsn(25, context.var("string"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldValue", "(CLjava/lang/String;Ljava/lang/String;)V");
        _seperator(methodvisitor, context);
        methodvisitor.visitLabel(label1);
        methodvisitor.visitLabel(class1);
    }

    private void _writeObject(MethodVisitor methodvisitor, FieldInfo fieldinfo, Context context, Label label)
    {
        String s = fieldinfo.getFormat();
        Label label1 = new Label();
        methodvisitor.visitVarInsn(25, context.processValue());
        methodvisitor.visitJumpInsn(199, label1);
        _if_write_null(methodvisitor, fieldinfo, context);
        methodvisitor.visitJumpInsn(167, label);
        methodvisitor.visitLabel(label1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "write", "(C)V");
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(25, context.fieldName());
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFieldName", "(Ljava/lang/String;)V");
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitVarInsn(25, context.processValue());
        if (s != null)
        {
            methodvisitor.visitLdcInsn(s);
            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "writeWithFormat", "(Ljava/lang/Object;Ljava/lang/String;)V");
        } else
        {
            methodvisitor.visitVarInsn(25, context.fieldName());
            if ((fieldinfo.getFieldType() instanceof Class) && ((Class)fieldinfo.getFieldType()).isPrimitive())
            {
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "writeWithFieldName", "(Ljava/lang/Object;Ljava/lang/Object;)V");
            } else
            {
                methodvisitor.visitVarInsn(25, 0);
                methodvisitor.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(fieldinfo.getName()).append("_asm_fieldType").toString(), "Ljava/lang/reflect/Type;");
                methodvisitor.visitLdcInsn(Integer.valueOf(fieldinfo.getSerialzeFeatures()));
                methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "writeWithFieldName", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
            }
        }
        _seperator(methodvisitor, context);
    }

    private void generateWriteAsArray(Class class1, MethodVisitor methodvisitor, List list, Context context)
        throws Exception
    {
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(16, 91);
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "write", "(C)V");
        int j = list.size();
        if (j == 0)
        {
            methodvisitor.visitVarInsn(25, context.var("out"));
            methodvisitor.visitVarInsn(16, 93);
            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "write", "(C)V");
        } else
        {
            int i = 0;
            while (i < j) 
            {
                Class class2;
                byte byte0;
                if (i == j - 1)
                {
                    byte0 = 93;
                } else
                {
                    byte0 = 44;
                }
                class1 = (FieldInfo)list.get(i);
                class2 = class1.getFieldClass();
                methodvisitor.visitLdcInsn(class1.getName());
                methodvisitor.visitVarInsn(58, context.fieldName());
                if (class2 == Byte.TYPE || class2 == Short.TYPE || class2 == Integer.TYPE)
                {
                    methodvisitor.visitVarInsn(25, context.var("out"));
                    _get(methodvisitor, context, class1);
                    methodvisitor.visitVarInsn(16, byte0);
                    methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeIntAndChar", "(IC)V");
                } else
                if (class2 == Long.TYPE)
                {
                    methodvisitor.visitVarInsn(25, context.var("out"));
                    _get(methodvisitor, context, class1);
                    methodvisitor.visitVarInsn(16, byte0);
                    methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeLongAndChar", "(JC)V");
                } else
                if (class2 == Float.TYPE)
                {
                    methodvisitor.visitVarInsn(25, context.var("out"));
                    _get(methodvisitor, context, class1);
                    methodvisitor.visitVarInsn(16, byte0);
                    methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeFloatAndChar", "(FC)V");
                } else
                if (class2 == Double.TYPE)
                {
                    methodvisitor.visitVarInsn(25, context.var("out"));
                    _get(methodvisitor, context, class1);
                    methodvisitor.visitVarInsn(16, byte0);
                    methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeDoubleAndChar", "(DC)V");
                } else
                if (class2 == Boolean.TYPE)
                {
                    methodvisitor.visitVarInsn(25, context.var("out"));
                    _get(methodvisitor, context, class1);
                    methodvisitor.visitVarInsn(16, byte0);
                    methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeBooleanAndChar", "(ZC)V");
                } else
                if (class2 == Character.TYPE)
                {
                    methodvisitor.visitVarInsn(25, context.var("out"));
                    _get(methodvisitor, context, class1);
                    methodvisitor.visitVarInsn(16, byte0);
                    methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeCharacterAndChar", "(CC)V");
                } else
                if (class2 == java/lang/String)
                {
                    methodvisitor.visitVarInsn(25, context.var("out"));
                    _get(methodvisitor, context, class1);
                    methodvisitor.visitVarInsn(16, byte0);
                    methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeString", "(Ljava/lang/String;C)V");
                } else
                if (class2.isEnum())
                {
                    methodvisitor.visitVarInsn(25, context.var("out"));
                    _get(methodvisitor, context, class1);
                    methodvisitor.visitVarInsn(16, byte0);
                    methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "writeEnum", "(Ljava/lang/Enum;C)V");
                } else
                {
                    String s = class1.getFormat();
                    methodvisitor.visitVarInsn(25, context.serializer());
                    _get(methodvisitor, context, class1);
                    if (s != null)
                    {
                        methodvisitor.visitLdcInsn(s);
                        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "writeWithFormat", "(Ljava/lang/Object;Ljava/lang/String;)V");
                    } else
                    {
                        methodvisitor.visitVarInsn(25, context.fieldName());
                        if ((class1.getFieldType() instanceof Class) && ((Class)class1.getFieldType()).isPrimitive())
                        {
                            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "writeWithFieldName", "(Ljava/lang/Object;Ljava/lang/Object;)V");
                        } else
                        {
                            methodvisitor.visitVarInsn(25, 0);
                            methodvisitor.visitFieldInsn(180, context.getClassName(), (new StringBuilder()).append(class1.getName()).append("_asm_fieldType").toString(), "Ljava/lang/reflect/Type;");
                            methodvisitor.visitLdcInsn(Integer.valueOf(class1.getSerialzeFeatures()));
                            methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "writeWithFieldName", "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
                        }
                    }
                    methodvisitor.visitVarInsn(25, context.var("out"));
                    methodvisitor.visitVarInsn(16, byte0);
                    methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "write", "(C)V");
                }
                i++;
            }
        }
    }

    private void generateWriteMethod(Class class1, MethodVisitor methodvisitor, List list, Context context)
        throws Exception
    {
        Label label = new Label();
        int j = list.size();
        Label label1 = new Label();
        Label label2 = new Label();
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitFieldInsn(178, "com/alibaba/fastjson/serializer/SerializerFeature", "PrettyFormat", "Lcom/alibaba/fastjson/serializer/SerializerFeature;");
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "isEnabled", "(Lcom/alibaba/fastjson/serializer/SerializerFeature;)Z");
        methodvisitor.visitJumpInsn(153, label1);
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitFieldInsn(180, context.getClassName(), "nature", "Lcom/alibaba/fastjson/serializer/JavaBeanSerializer;");
        methodvisitor.visitJumpInsn(199, label2);
        methodvisitor.visitLabel(label2);
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitFieldInsn(180, context.getClassName(), "nature", "Lcom/alibaba/fastjson/serializer/JavaBeanSerializer;");
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitVarInsn(25, 2);
        methodvisitor.visitVarInsn(25, 3);
        methodvisitor.visitVarInsn(25, 4);
        methodvisitor.visitVarInsn(21, 5);
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JavaBeanSerializer", "write", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
        methodvisitor.visitInsn(177);
        methodvisitor.visitLabel(label1);
        label1 = new Label();
        label2 = new Label();
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitFieldInsn(180, context.getClassName(), "nature", "Lcom/alibaba/fastjson/serializer/JavaBeanSerializer;");
        methodvisitor.visitJumpInsn(199, label2);
        methodvisitor.visitLabel(label2);
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitFieldInsn(180, context.getClassName(), "nature", "Lcom/alibaba/fastjson/serializer/JavaBeanSerializer;");
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitVarInsn(25, 2);
        methodvisitor.visitVarInsn(21, 5);
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JavaBeanSerializer", "writeReference", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;I)Z");
        methodvisitor.visitJumpInsn(153, label1);
        methodvisitor.visitInsn(177);
        methodvisitor.visitLabel(label1);
        label1 = new Label();
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitFieldInsn(180, context.getClassName(), "nature", "Lcom/alibaba/fastjson/serializer/JavaBeanSerializer;");
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JavaBeanSerializer", "isWriteAsArray", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;)Z");
        methodvisitor.visitJumpInsn(153, label1);
        methodvisitor.visitVarInsn(25, 0);
        methodvisitor.visitVarInsn(25, 1);
        methodvisitor.visitVarInsn(25, 2);
        methodvisitor.visitVarInsn(25, 3);
        methodvisitor.visitVarInsn(25, 4);
        methodvisitor.visitMethodInsn(182, context.getClassName(), "writeAsArray", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;)V");
        methodvisitor.visitInsn(177);
        methodvisitor.visitLabel(label1);
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "getContext", "()Lcom/alibaba/fastjson/serializer/SerialContext;");
        methodvisitor.visitVarInsn(58, context.var("parent"));
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitVarInsn(25, context.var("parent"));
        methodvisitor.visitVarInsn(25, context.obj());
        methodvisitor.visitVarInsn(25, context.paramFieldName());
        methodvisitor.visitLdcInsn(Integer.valueOf(context.beanSerializeFeatures));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "setContext", "(Lcom/alibaba/fastjson/serializer/SerialContext;Ljava/lang/Object;Ljava/lang/Object;I)V");
        label1 = new Label();
        label2 = new Label();
        Label label3 = new Label();
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitVarInsn(25, context.paramFieldType());
        methodvisitor.visitVarInsn(25, context.obj());
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "isWriteClassName", "(Ljava/lang/reflect/Type;Ljava/lang/Object;)Z");
        methodvisitor.visitJumpInsn(153, label2);
        methodvisitor.visitVarInsn(25, context.paramFieldType());
        methodvisitor.visitVarInsn(25, context.obj());
        methodvisitor.visitMethodInsn(182, "java/lang/Object", "getClass", "()Ljava/lang/Class;");
        methodvisitor.visitJumpInsn(165, label2);
        methodvisitor.visitLabel(label3);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitLdcInsn((new StringBuilder()).append("{\"").append(JSON.DEFAULT_TYPE_KEY).append("\":\"").append(class1.getName()).append("\"").toString());
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "write", "(Ljava/lang/String;)V");
        methodvisitor.visitVarInsn(16, 44);
        methodvisitor.visitJumpInsn(167, label1);
        methodvisitor.visitLabel(label2);
        methodvisitor.visitVarInsn(16, 123);
        methodvisitor.visitLabel(label1);
        methodvisitor.visitVarInsn(54, context.var("seperator"));
        _before(methodvisitor, context);
        int i = 0;
        while (i < j) 
        {
            FieldInfo fieldinfo = (FieldInfo)list.get(i);
            Class class2 = fieldinfo.getFieldClass();
            methodvisitor.visitLdcInsn(fieldinfo.getName());
            methodvisitor.visitVarInsn(58, context.fieldName());
            if (class2 == Byte.TYPE)
            {
                _byte(class1, methodvisitor, fieldinfo, context);
            } else
            if (class2 == Short.TYPE)
            {
                _short(class1, methodvisitor, fieldinfo, context);
            } else
            if (class2 == Integer.TYPE)
            {
                _int(class1, methodvisitor, fieldinfo, context);
            } else
            if (class2 == Long.TYPE)
            {
                _long(class1, methodvisitor, fieldinfo, context);
            } else
            if (class2 == Float.TYPE)
            {
                _float(class1, methodvisitor, fieldinfo, context);
            } else
            if (class2 == Double.TYPE)
            {
                _double(class1, methodvisitor, fieldinfo, context);
            } else
            if (class2 == Boolean.TYPE)
            {
                _boolean(class1, methodvisitor, fieldinfo, context);
            } else
            if (class2 == Character.TYPE)
            {
                _char(class1, methodvisitor, fieldinfo, context);
            } else
            if (class2 == java/lang/String)
            {
                _string(class1, methodvisitor, fieldinfo, context);
            } else
            if (class2 == java/math/BigDecimal)
            {
                _decimal(class1, methodvisitor, fieldinfo, context);
            } else
            if (java/util/List.isAssignableFrom(class2))
            {
                _list(class1, methodvisitor, fieldinfo, context);
            } else
            if (class2.isEnum())
            {
                _enum(class1, methodvisitor, fieldinfo, context);
            } else
            {
                _object(class1, methodvisitor, fieldinfo, context);
            }
            i++;
        }
        _after(methodvisitor, context);
        class1 = new Label();
        list = new Label();
        methodvisitor.visitVarInsn(21, context.var("seperator"));
        methodvisitor.visitIntInsn(16, 123);
        methodvisitor.visitJumpInsn(160, class1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(16, 123);
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "write", "(C)V");
        methodvisitor.visitLabel(class1);
        methodvisitor.visitVarInsn(25, context.var("out"));
        methodvisitor.visitVarInsn(16, 125);
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "write", "(C)V");
        methodvisitor.visitLabel(list);
        methodvisitor.visitLabel(label);
        methodvisitor.visitVarInsn(25, context.serializer());
        methodvisitor.visitVarInsn(25, context.var("parent"));
        methodvisitor.visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "setContext", "(Lcom/alibaba/fastjson/serializer/SerialContext;)V");
    }

    public ObjectSerializer createJavaBeanSerializer(Class class1, Map map)
        throws Exception
    {
        String s;
        byte abyte0[];
        if (class1.isPrimitive())
        {
            throw new JSONException((new StringBuilder()).append("unsupportd class ").append(class1.getName()).toString());
        }
        Object obj = TypeUtils.computeGetters(class1, map, false);
        if (((List) (obj)).size() > 256)
        {
            return null;
        }
        for (Iterator iterator = ((List) (obj)).iterator(); iterator.hasNext();)
        {
            if (!ASMUtils.checkName(((FieldInfo)iterator.next()).getMember().getName()))
            {
                return null;
            }
        }

        s = getGenClassName(class1);
        int i = TypeUtils.getSerializeFeatures(class1);
        ClassWriter classwriter = new ClassWriter();
        classwriter.visit(49, 33, s, "com/alibaba/fastjson/serializer/ASMJavaBeanSerializer", new String[] {
            "com/alibaba/fastjson/serializer/ObjectSerializer"
        });
        FieldInfo fieldinfo;
        for (Iterator iterator1 = ((List) (obj)).iterator(); iterator1.hasNext(); classwriter.visitField(1, (new StringBuilder()).append(fieldinfo.getName()).append("_asm_fieldType").toString(), "Ljava/lang/reflect/Type;").visitEnd())
        {
            fieldinfo = (FieldInfo)iterator1.next();
            classwriter.visitField(1, (new StringBuilder()).append(fieldinfo.getName()).append("_asm_fieldPrefix").toString(), "Ljava/lang/reflect/Type;").visitEnd();
        }

        Object obj1 = classwriter.visitMethod(1, "<init>", "()V", null, null);
        ((MethodVisitor) (obj1)).visitVarInsn(25, 0);
        ((MethodVisitor) (obj1)).visitLdcInsn(Type.getType(ASMUtils.getDesc(class1)));
        ((MethodVisitor) (obj1)).visitMethodInsn(183, "com/alibaba/fastjson/serializer/ASMJavaBeanSerializer", "<init>", "(Ljava/lang/Class;)V");
        Object obj2 = ((List) (obj)).iterator();
        while (((Iterator) (obj2)).hasNext()) 
        {
            FieldInfo fieldinfo1 = (FieldInfo)((Iterator) (obj2)).next();
            ((MethodVisitor) (obj1)).visitVarInsn(25, 0);
            ((MethodVisitor) (obj1)).visitLdcInsn(Type.getType(ASMUtils.getDesc(fieldinfo1.getDeclaringClass())));
            if (fieldinfo1.getMethod() != null)
            {
                ((MethodVisitor) (obj1)).visitLdcInsn(fieldinfo1.getMethod().getName());
                ((MethodVisitor) (obj1)).visitMethodInsn(184, "com/alibaba/fastjson/util/ASMUtils", "getMethodType", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Type;");
            } else
            {
                ((MethodVisitor) (obj1)).visitLdcInsn(fieldinfo1.getField().getName());
                ((MethodVisitor) (obj1)).visitMethodInsn(184, "com/alibaba/fastjson/util/ASMUtils", "getFieldType", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Type;");
            }
            ((MethodVisitor) (obj1)).visitFieldInsn(181, s, (new StringBuilder()).append(fieldinfo1.getName()).append("_asm_fieldType").toString(), "Ljava/lang/reflect/Type;");
        }
        ((MethodVisitor) (obj1)).visitInsn(177);
        ((MethodVisitor) (obj1)).visitMaxs(4, 4);
        ((MethodVisitor) (obj1)).visitEnd();
        obj1 = new Context(s, i);
        obj2 = classwriter.visitMethod(1, "write", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V", null, new String[] {
            "java/io/IOException"
        });
        ((MethodVisitor) (obj2)).visitVarInsn(25, ((Context) (obj1)).serializer());
        ((MethodVisitor) (obj2)).visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "getWriter", "()Lcom/alibaba/fastjson/serializer/SerializeWriter;");
        ((MethodVisitor) (obj2)).visitVarInsn(58, ((Context) (obj1)).var("out"));
        JSONType jsontype = (JSONType)class1.getAnnotation(com/alibaba/fastjson/annotation/JSONType);
        if (jsontype == null || jsontype.alphabetic())
        {
            Label label = new Label();
            ((MethodVisitor) (obj2)).visitVarInsn(25, ((Context) (obj1)).var("out"));
            ((MethodVisitor) (obj2)).visitFieldInsn(178, "com/alibaba/fastjson/serializer/SerializerFeature", "SortField", "Lcom/alibaba/fastjson/serializer/SerializerFeature;");
            ((MethodVisitor) (obj2)).visitMethodInsn(182, "com/alibaba/fastjson/serializer/SerializeWriter", "isEnabled", "(Lcom/alibaba/fastjson/serializer/SerializerFeature;)Z");
            ((MethodVisitor) (obj2)).visitJumpInsn(153, label);
            ((MethodVisitor) (obj2)).visitVarInsn(25, 0);
            ((MethodVisitor) (obj2)).visitVarInsn(25, 1);
            ((MethodVisitor) (obj2)).visitVarInsn(25, 2);
            ((MethodVisitor) (obj2)).visitVarInsn(25, 3);
            ((MethodVisitor) (obj2)).visitVarInsn(25, 4);
            ((MethodVisitor) (obj2)).visitVarInsn(21, 5);
            ((MethodVisitor) (obj2)).visitMethodInsn(182, s, "write1", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V");
            ((MethodVisitor) (obj2)).visitInsn(177);
            ((MethodVisitor) (obj2)).visitLabel(label);
        }
        ((MethodVisitor) (obj2)).visitVarInsn(25, ((Context) (obj1)).obj());
        ((MethodVisitor) (obj2)).visitTypeInsn(192, ASMUtils.getType(class1));
        ((MethodVisitor) (obj2)).visitVarInsn(58, ((Context) (obj1)).var("entity"));
        generateWriteMethod(class1, ((MethodVisitor) (obj2)), ((List) (obj)), ((Context) (obj1)));
        ((MethodVisitor) (obj2)).visitInsn(177);
        ((MethodVisitor) (obj2)).visitMaxs(6, ((Context) (obj1)).getVariantCount() + 1);
        ((MethodVisitor) (obj2)).visitEnd();
        map = TypeUtils.computeGetters(class1, map, true);
        obj = new Context(s, i);
        obj1 = classwriter.visitMethod(1, "write1", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;I)V", null, new String[] {
            "java/io/IOException"
        });
        ((MethodVisitor) (obj1)).visitVarInsn(25, ((Context) (obj)).serializer());
        ((MethodVisitor) (obj1)).visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "getWriter", "()Lcom/alibaba/fastjson/serializer/SerializeWriter;");
        ((MethodVisitor) (obj1)).visitVarInsn(58, ((Context) (obj)).var("out"));
        ((MethodVisitor) (obj1)).visitVarInsn(25, ((Context) (obj)).obj());
        ((MethodVisitor) (obj1)).visitTypeInsn(192, ASMUtils.getType(class1));
        ((MethodVisitor) (obj1)).visitVarInsn(58, ((Context) (obj)).var("entity"));
        generateWriteMethod(class1, ((MethodVisitor) (obj1)), map, ((Context) (obj)));
        ((MethodVisitor) (obj1)).visitInsn(177);
        ((MethodVisitor) (obj1)).visitMaxs(6, ((Context) (obj)).getVariantCount() + 1);
        ((MethodVisitor) (obj1)).visitEnd();
        obj = new Context(s, i);
        obj1 = classwriter.visitMethod(1, "writeAsArray", "(Lcom/alibaba/fastjson/serializer/JSONSerializer;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Type;)V", null, new String[] {
            "java/io/IOException"
        });
        ((MethodVisitor) (obj1)).visitVarInsn(25, ((Context) (obj)).serializer());
        ((MethodVisitor) (obj1)).visitMethodInsn(182, "com/alibaba/fastjson/serializer/JSONSerializer", "getWriter", "()Lcom/alibaba/fastjson/serializer/SerializeWriter;");
        ((MethodVisitor) (obj1)).visitVarInsn(58, ((Context) (obj)).var("out"));
        ((MethodVisitor) (obj1)).visitVarInsn(25, ((Context) (obj)).obj());
        ((MethodVisitor) (obj1)).visitTypeInsn(192, ASMUtils.getType(class1));
        ((MethodVisitor) (obj1)).visitVarInsn(58, ((Context) (obj)).var("entity"));
        generateWriteAsArray(class1, ((MethodVisitor) (obj1)), map, ((Context) (obj)));
        ((MethodVisitor) (obj1)).visitInsn(177);
        ((MethodVisitor) (obj1)).visitMaxs(6, ((Context) (obj)).getVariantCount() + 1);
        ((MethodVisitor) (obj1)).visitEnd();
        abyte0 = classwriter.toByteArray();
        if (JSON.DUMP_CLASS == null)
        {
            break MISSING_BLOCK_LABEL_1316;
        }
        map = new FileOutputStream((new StringBuilder()).append(JSON.DUMP_CLASS).append(File.separator).append(s).append(".class").toString());
        class1 = map;
        map.write(abyte0);
        if (map != null)
        {
            map.close();
        }
_L1:
        return (ObjectSerializer)classLoader.defineClassPublic(s, abyte0, 0, abyte0.length).newInstance();
        Exception exception;
        exception;
        map = null;
_L4:
        class1 = map;
        System.err.println((new StringBuilder()).append("FASTJSON dump class:").append(s).append("\u5931\u8D25:").append(exception.getMessage()).toString());
        if (map != null)
        {
            map.close();
        }
          goto _L1
        map;
        class1 = null;
_L3:
        if (class1 != null)
        {
            class1.close();
        }
        throw map;
        map;
        if (true) goto _L3; else goto _L2
_L2:
        exception;
          goto _L4
    }

    public String getGenClassName(Class class1)
    {
        return (new StringBuilder()).append("Serializer_").append(seed.incrementAndGet()).toString();
    }

    public boolean isExternalClass(Class class1)
    {
        return classLoader.isExternalClass(class1);
    }
}
