// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Enumeration;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class EnumerationSeriliazer
    implements ObjectSerializer
{

    public static EnumerationSeriliazer instance = new EnumerationSeriliazer();

    public EnumerationSeriliazer()
    {
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        Type type1;
        Enumeration enumeration;
        SerializeWriter serializewriter;
        i = 0;
        serializewriter = jsonserializer.getWriter();
        if (obj == null)
        {
            if (serializewriter.isEnabled(SerializerFeature.WriteNullListAsEmpty))
            {
                serializewriter.write("[]");
                return;
            } else
            {
                serializewriter.writeNull();
                return;
            }
        }
        enumeration = null;
        type1 = enumeration;
        if (jsonserializer.isEnabled(SerializerFeature.WriteClassName))
        {
            type1 = enumeration;
            if (type instanceof ParameterizedType)
            {
                type1 = ((ParameterizedType)type).getActualTypeArguments()[0];
            }
        }
        enumeration = (Enumeration)obj;
        type = jsonserializer.getContext();
        jsonserializer.setContext(type, obj, obj1, 0);
        serializewriter.append('[');
_L1:
        if (!enumeration.hasMoreElements())
        {
            break MISSING_BLOCK_LABEL_195;
        }
        obj = enumeration.nextElement();
        int j;
        j = i + 1;
        if (i == 0)
        {
            break MISSING_BLOCK_LABEL_147;
        }
        serializewriter.append(',');
        if (obj != null)
        {
            break MISSING_BLOCK_LABEL_163;
        }
        serializewriter.writeNull();
        i = j;
          goto _L1
        jsonserializer.getObjectWriter(obj.getClass()).write(jsonserializer, obj, Integer.valueOf(j - 1), type1, 0);
        i = j;
          goto _L1
        serializewriter.append(']');
        jsonserializer.setContext(type);
        return;
        obj;
        jsonserializer.setContext(type);
        throw obj;
    }

}
