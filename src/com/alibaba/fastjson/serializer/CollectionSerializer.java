// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializerFeature, SerializeWriter

public class CollectionSerializer
    implements ObjectSerializer
{

    public static final CollectionSerializer instance = new CollectionSerializer();

    public CollectionSerializer()
    {
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        Type type1;
        SerializeWriter serializewriter;
        int j;
        serializewriter = jsonserializer.getWriter();
        if (obj == null)
        {
            if (serializewriter.isEnabled(SerializerFeature.WriteNullListAsEmpty))
            {
                serializewriter.write("[]");
                return;
            } else
            {
                serializewriter.writeNull();
                return;
            }
        }
        Collection collection = null;
        type1 = collection;
        if (jsonserializer.isEnabled(SerializerFeature.WriteClassName))
        {
            type1 = collection;
            if (type instanceof ParameterizedType)
            {
                type1 = ((ParameterizedType)type).getActualTypeArguments()[0];
            }
        }
        collection = (Collection)obj;
        type = jsonserializer.getContext();
        jsonserializer.setContext(type, obj, obj1, 0);
        if (jsonserializer.isEnabled(SerializerFeature.WriteClassName))
        {
            if (java/util/HashSet == collection.getClass())
            {
                serializewriter.append("Set");
            } else
            if (java/util/TreeSet == collection.getClass())
            {
                serializewriter.append("TreeSet");
            }
        }
        serializewriter.append('[');
        obj = collection.iterator();
        i = 0;
_L2:
        if (!((Iterator) (obj)).hasNext())
        {
            break MISSING_BLOCK_LABEL_324;
        }
        obj1 = ((Iterator) (obj)).next();
        j = i + 1;
        if (i == 0)
        {
            break MISSING_BLOCK_LABEL_181;
        }
        serializewriter.append(',');
        if (obj1 != null)
        {
            break MISSING_BLOCK_LABEL_218;
        }
        serializewriter.writeNull();
        i = j;
        continue; /* Loop/switch isn't completed */
        Class class1 = obj1.getClass();
        if (class1 != java/lang/Integer)
        {
            break MISSING_BLOCK_LABEL_250;
        }
        serializewriter.writeInt(((Integer)obj1).intValue());
        i = j;
        continue; /* Loop/switch isn't completed */
        if (class1 != java/lang/Long)
        {
            break MISSING_BLOCK_LABEL_294;
        }
        serializewriter.writeLong(((Long)obj1).longValue());
        if (!serializewriter.isEnabled(SerializerFeature.WriteClassName))
        {
            break MISSING_BLOCK_LABEL_348;
        }
        serializewriter.write('L');
        i = j;
        continue; /* Loop/switch isn't completed */
        jsonserializer.getObjectWriter(class1).write(jsonserializer, obj1, Integer.valueOf(j - 1), type1, 0);
        i = j;
        continue; /* Loop/switch isn't completed */
        serializewriter.append(']');
        jsonserializer.setContext(type);
        return;
        obj;
        jsonserializer.setContext(type);
        throw obj;
        i = j;
        if (true) goto _L2; else goto _L1
_L1:
    }

}
