// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.sql.Clob;
import java.sql.SQLException;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer

public class ClobSeriliazer
    implements ObjectSerializer
{

    public static final ClobSeriliazer instance = new ClobSeriliazer();

    public ClobSeriliazer()
    {
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        if (obj == null)
        {
            try
            {
                jsonserializer.writeNull();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (JSONSerializer jsonserializer)
            {
                throw new IOException("write clob error", jsonserializer);
            }
        }
        obj = ((Clob)obj).getCharacterStream();
        obj1 = new StringWriter();
        type = new char[1024];
_L1:
        i = ((Reader) (obj)).read(type);
        if (i == -1)
        {
            break MISSING_BLOCK_LABEL_72;
        }
        ((StringWriter) (obj1)).write(type, 0, i);
          goto _L1
        ((Reader) (obj)).close();
        jsonserializer.write(((StringWriter) (obj1)).toString());
        return;
    }

}
