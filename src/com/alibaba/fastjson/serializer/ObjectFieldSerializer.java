// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.util.FieldInfo;
import java.util.Collection;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            FieldSerializer, SerializerFeature, JSONSerializer, SerializeWriter, 
//            ObjectSerializer

public class ObjectFieldSerializer extends FieldSerializer
{
    static class RuntimeSerializerInfo
    {

        ObjectSerializer fieldSerializer;
        Class runtimeFieldClass;

        public RuntimeSerializerInfo(ObjectSerializer objectserializer, Class class1)
        {
            fieldSerializer = objectserializer;
            runtimeFieldClass = class1;
        }
    }


    private String format;
    private RuntimeSerializerInfo runtimeInfo;
    boolean writeEnumUsingName;
    boolean writeEnumUsingToString;
    boolean writeNullBooleanAsFalse;
    boolean writeNullListAsEmpty;
    boolean writeNullStringAsEmpty;
    boolean writeNumberAsZero;

    public ObjectFieldSerializer(FieldInfo fieldinfo)
    {
        super(fieldinfo);
        writeNumberAsZero = false;
        writeNullStringAsEmpty = false;
        writeNullBooleanAsFalse = false;
        writeNullListAsEmpty = false;
        writeEnumUsingToString = false;
        writeEnumUsingName = false;
        fieldinfo = (JSONField)fieldinfo.getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        if (fieldinfo != null)
        {
            format = fieldinfo.format();
            if (format.trim().length() == 0)
            {
                format = null;
            }
            fieldinfo = fieldinfo.serialzeFeatures();
            int j = fieldinfo.length;
            int i = 0;
            while (i < j) 
            {
                SerializerFeature serializerfeature = fieldinfo[i];
                if (serializerfeature == SerializerFeature.WriteNullNumberAsZero)
                {
                    writeNumberAsZero = true;
                } else
                if (serializerfeature == SerializerFeature.WriteNullStringAsEmpty)
                {
                    writeNullStringAsEmpty = true;
                } else
                if (serializerfeature == SerializerFeature.WriteNullBooleanAsFalse)
                {
                    writeNullBooleanAsFalse = true;
                } else
                if (serializerfeature == SerializerFeature.WriteNullListAsEmpty)
                {
                    writeNullListAsEmpty = true;
                } else
                if (serializerfeature == SerializerFeature.WriteEnumUsingToString)
                {
                    writeEnumUsingToString = true;
                } else
                if (serializerfeature == SerializerFeature.WriteEnumUsingName)
                {
                    writeEnumUsingName = true;
                }
                i++;
            }
        }
    }

    public void writeProperty(JSONSerializer jsonserializer, Object obj)
        throws Exception
    {
        writePrefix(jsonserializer);
        writeValue(jsonserializer, obj);
    }

    public void writeValue(JSONSerializer jsonserializer, Object obj)
        throws Exception
    {
        if (format != null)
        {
            jsonserializer.writeWithFormat(obj, format);
            return;
        }
        Object obj1;
        int i;
        if (runtimeInfo == null)
        {
            if (obj == null)
            {
                obj1 = fieldInfo.getFieldClass();
            } else
            {
                obj1 = obj.getClass();
            }
            runtimeInfo = new RuntimeSerializerInfo(jsonserializer.getObjectWriter(((Class) (obj1))), ((Class) (obj1)));
        }
        obj1 = runtimeInfo;
        i = fieldInfo.getSerialzeFeatures();
        if (obj == null)
        {
            if (writeNumberAsZero && java/lang/Number.isAssignableFrom(((RuntimeSerializerInfo) (obj1)).runtimeFieldClass))
            {
                jsonserializer.getWriter().write('0');
                return;
            }
            if (writeNullStringAsEmpty && java/lang/String == ((RuntimeSerializerInfo) (obj1)).runtimeFieldClass)
            {
                jsonserializer.getWriter().write("\"\"");
                return;
            }
            if (writeNullBooleanAsFalse && java/lang/Boolean == ((RuntimeSerializerInfo) (obj1)).runtimeFieldClass)
            {
                jsonserializer.getWriter().write("false");
                return;
            }
            if (writeNullListAsEmpty && java/util/Collection.isAssignableFrom(((RuntimeSerializerInfo) (obj1)).runtimeFieldClass))
            {
                jsonserializer.getWriter().write("[]");
                return;
            } else
            {
                ((RuntimeSerializerInfo) (obj1)).fieldSerializer.write(jsonserializer, null, fieldInfo.getName(), null, i);
                return;
            }
        }
        if (((RuntimeSerializerInfo) (obj1)).runtimeFieldClass.isEnum())
        {
            if (writeEnumUsingName)
            {
                jsonserializer.getWriter().writeString(((Enum)obj).name());
                return;
            }
            if (writeEnumUsingToString)
            {
                jsonserializer.getWriter().writeString(((Enum)obj).toString());
                return;
            }
        }
        Class class1 = obj.getClass();
        if (class1 == ((RuntimeSerializerInfo) (obj1)).runtimeFieldClass)
        {
            ((RuntimeSerializerInfo) (obj1)).fieldSerializer.write(jsonserializer, obj, fieldInfo.getName(), fieldInfo.getFieldType(), i);
            return;
        } else
        {
            jsonserializer.getObjectWriter(class1).write(jsonserializer, obj, fieldInfo.getName(), fieldInfo.getFieldType(), i);
            return;
        }
    }
}
