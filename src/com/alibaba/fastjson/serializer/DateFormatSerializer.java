// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSON;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            ObjectSerializer, JSONSerializer, SerializeWriter, SerializerFeature

public class DateFormatSerializer
    implements ObjectSerializer
{

    public static final DateFormatSerializer instance = new DateFormatSerializer();

    public DateFormatSerializer()
    {
    }

    public void write(JSONSerializer jsonserializer, Object obj, Object obj1, Type type, int i)
        throws IOException
    {
        obj1 = jsonserializer.getWriter();
        if (obj == null)
        {
            ((SerializeWriter) (obj1)).writeNull();
            return;
        }
        String s = ((SimpleDateFormat)obj).toPattern();
        if (((SerializeWriter) (obj1)).isEnabled(SerializerFeature.WriteClassName) && obj.getClass() != type)
        {
            ((SerializeWriter) (obj1)).write('{');
            ((SerializeWriter) (obj1)).writeFieldName(JSON.DEFAULT_TYPE_KEY);
            jsonserializer.write(obj.getClass().getName());
            ((SerializeWriter) (obj1)).writeFieldValue(',', "val", s);
            ((SerializeWriter) (obj1)).write('}');
            return;
        } else
        {
            ((SerializeWriter) (obj1)).writeString(s);
            return;
        }
    }

}
