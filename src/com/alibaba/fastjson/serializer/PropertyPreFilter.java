// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;


// Referenced classes of package com.alibaba.fastjson.serializer:
//            SerializeFilter, JSONSerializer

public interface PropertyPreFilter
    extends SerializeFilter
{

    public abstract boolean apply(JSONSerializer jsonserializer, Object obj, String s);
}
