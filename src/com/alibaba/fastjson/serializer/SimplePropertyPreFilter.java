// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.serializer;

import java.util.HashSet;
import java.util.Set;

// Referenced classes of package com.alibaba.fastjson.serializer:
//            PropertyPreFilter, JSONSerializer

public class SimplePropertyPreFilter
    implements PropertyPreFilter
{

    private final Class clazz;
    private final Set excludes;
    private final Set includes;

    public transient SimplePropertyPreFilter(Class class1, String as[])
    {
        includes = new HashSet();
        excludes = new HashSet();
        clazz = class1;
        int j = as.length;
        for (int i = 0; i < j; i++)
        {
            class1 = as[i];
            if (class1 != null)
            {
                includes.add(class1);
            }
        }

    }

    public transient SimplePropertyPreFilter(String as[])
    {
        this(null, as);
    }

    public boolean apply(JSONSerializer jsonserializer, Object obj, String s)
    {
        if (obj != null && (clazz == null || clazz.isInstance(obj)))
        {
            if (excludes.contains(s))
            {
                return false;
            }
            if (includes.size() != 0 && !includes.contains(s))
            {
                return false;
            }
        }
        return true;
    }

    public Class getClazz()
    {
        return clazz;
    }

    public Set getExcludes()
    {
        return excludes;
    }

    public Set getIncludes()
    {
        return includes;
    }
}
