// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.annotation;

import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import java.lang.annotation.Annotation;

public interface JSONField
    extends Annotation
{

    public abstract boolean deserialize();

    public abstract String format();

    public abstract String label();

    public abstract String name();

    public abstract int ordinal();

    public abstract Feature[] parseFeatures();

    public abstract boolean serialize();

    public abstract SerializerFeature[] serialzeFeatures();
}
