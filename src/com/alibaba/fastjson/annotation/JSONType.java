// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.annotation;

import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import java.lang.annotation.Annotation;

public interface JSONType
    extends Annotation
{

    public abstract boolean alphabetic();

    public abstract boolean asm();

    public abstract String[] ignores();

    public abstract String[] includes();

    public abstract Class mappingTo();

    public abstract String[] orders();

    public abstract Feature[] parseFeatures();

    public abstract SerializerFeature[] serialzeFeatures();
}
