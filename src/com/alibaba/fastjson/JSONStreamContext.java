// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;


class JSONStreamContext
{

    static final int ArrayValue = 1005;
    static final int PropertyKey = 1002;
    static final int PropertyValue = 1003;
    static final int StartArray = 1004;
    static final int StartObject = 1001;
    private final JSONStreamContext parent;
    private int state;

    public JSONStreamContext(JSONStreamContext jsonstreamcontext, int i)
    {
        parent = jsonstreamcontext;
        state = i;
    }

    public JSONStreamContext getParent()
    {
        return parent;
    }

    public int getState()
    {
        return state;
    }

    public void setState(int i)
    {
        state = i;
    }
}
