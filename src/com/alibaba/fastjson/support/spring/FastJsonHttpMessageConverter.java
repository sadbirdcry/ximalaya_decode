// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.support.spring;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

public class FastJsonHttpMessageConverter extends AbstractHttpMessageConverter
{

    public static final Charset UTF8 = Charset.forName("UTF-8");
    private Charset charset;
    private SerializerFeature features[];

    public FastJsonHttpMessageConverter()
    {
        super(new MediaType[] {
            new MediaType("application", "json", UTF8), new MediaType("application", "*+json", UTF8)
        });
        charset = UTF8;
        features = new SerializerFeature[0];
    }

    public Charset getCharset()
    {
        return charset;
    }

    public SerializerFeature[] getFeatures()
    {
        return features;
    }

    protected Object readInternal(Class class1, HttpInputMessage httpinputmessage)
        throws IOException, HttpMessageNotReadableException
    {
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
        httpinputmessage = httpinputmessage.getBody();
        byte abyte0[] = new byte[1024];
        do
        {
            int i;
            do
            {
                i = httpinputmessage.read(abyte0);
                if (i == -1)
                {
                    httpinputmessage = bytearrayoutputstream.toByteArray();
                    return JSON.parseObject(httpinputmessage, 0, httpinputmessage.length, charset.newDecoder(), class1, new Feature[0]);
                }
            } while (i <= 0);
            bytearrayoutputstream.write(abyte0, 0, i);
        } while (true);
    }

    public void setCharset(Charset charset1)
    {
        charset = charset1;
    }

    public transient void setFeatures(SerializerFeature aserializerfeature[])
    {
        features = aserializerfeature;
    }

    protected boolean supports(Class class1)
    {
        return true;
    }

    protected void writeInternal(Object obj, HttpOutputMessage httpoutputmessage)
        throws IOException, HttpMessageNotWritableException
    {
        httpoutputmessage.getBody().write(JSON.toJSONString(obj, features).getBytes(charset));
    }

}
