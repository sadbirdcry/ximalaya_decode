// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.support.spring;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.view.AbstractView;

public class FastJsonJsonView extends AbstractView
{

    public static final String DEFAULT_CONTENT_TYPE = "application/json";
    public static final Charset UTF8 = Charset.forName("UTF-8");
    private Charset charset;
    private boolean disableCaching;
    private boolean extractValueFromSingleKeyModel;
    private Set renderedAttributes;
    private SerializerFeature serializerFeatures[];
    private boolean updateContentLength;

    public FastJsonJsonView()
    {
        charset = UTF8;
        serializerFeatures = new SerializerFeature[0];
        disableCaching = true;
        updateContentLength = false;
        extractValueFromSingleKeyModel = false;
        setContentType("application/json");
        setExposePathVariables(false);
    }

    protected Object filterModel(Map map)
    {
        HashMap hashmap = new HashMap(map.size());
        Set set;
        if (!CollectionUtils.isEmpty(renderedAttributes))
        {
            set = renderedAttributes;
        } else
        {
            set = map.keySet();
        }
        map = map.entrySet().iterator();
        do
        {
            if (!map.hasNext())
            {
                break;
            }
            java.util.Map.Entry entry = (java.util.Map.Entry)map.next();
            if (!(entry.getValue() instanceof BindingResult) && set.contains(entry.getKey()))
            {
                hashmap.put(entry.getKey(), entry.getValue());
            }
        } while (true);
        if (extractValueFromSingleKeyModel && hashmap.size() == 1)
        {
            map = hashmap.entrySet().iterator();
            if (map.hasNext())
            {
                return ((java.util.Map.Entry)map.next()).getValue();
            }
        }
        return hashmap;
    }

    public Charset getCharset()
    {
        return charset;
    }

    public SerializerFeature[] getFeatures()
    {
        return serializerFeatures;
    }

    public boolean isExtractValueFromSingleKeyModel()
    {
        return extractValueFromSingleKeyModel;
    }

    protected void prepareResponse(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse)
    {
        setResponseContentType(httpservletrequest, httpservletresponse);
        httpservletresponse.setCharacterEncoding(UTF8.name());
        if (disableCaching)
        {
            httpservletresponse.addHeader("Pragma", "no-cache");
            httpservletresponse.addHeader("Cache-Control", "no-cache, no-store, max-age=0");
            httpservletresponse.addDateHeader("Expires", 1L);
        }
    }

    protected void renderMergedOutputModel(Map map, HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse)
        throws Exception
    {
        httpservletrequest = JSON.toJSONString(filterModel(map), serializerFeatures).getBytes(charset);
        if (updateContentLength)
        {
            map = createTemporaryOutputStream();
        } else
        {
            map = httpservletresponse.getOutputStream();
        }
        map.write(httpservletrequest);
        if (updateContentLength)
        {
            writeToResponse(httpservletresponse, (ByteArrayOutputStream)map);
        }
    }

    public void setCharset(Charset charset1)
    {
        charset = charset1;
    }

    public void setDisableCaching(boolean flag)
    {
        disableCaching = flag;
    }

    public void setExtractValueFromSingleKeyModel(boolean flag)
    {
        extractValueFromSingleKeyModel = flag;
    }

    public transient void setFeatures(SerializerFeature aserializerfeature[])
    {
        serializerFeatures = aserializerfeature;
    }

    public void setRenderedAttributes(Set set)
    {
        renderedAttributes = set;
    }

    public transient void setSerializerFeature(SerializerFeature aserializerfeature[])
    {
        setFeatures(aserializerfeature);
    }

    public void setUpdateContentLength(boolean flag)
    {
        updateContentLength = flag;
    }

}
