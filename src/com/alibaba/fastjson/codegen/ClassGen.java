// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.codegen;

import java.io.IOException;
import java.lang.reflect.Type;

public abstract class ClassGen
{

    protected Class clazz;
    private String indent;
    private int indentCount;
    protected Appendable out;
    protected Type type;

    public ClassGen(Class class1, Appendable appendable)
    {
        this(class1, null, appendable);
    }

    public ClassGen(Class class1, Type type1, Appendable appendable)
    {
        indent = "\t";
        indentCount = 0;
        clazz = class1;
        type = type1;
        out = appendable;
    }

    protected void beginClass(String s)
        throws IOException
    {
        print("public class ");
        print(s);
        print(" implements ObjectDeserializer {");
        incrementIndent();
        println();
    }

    protected void beginInit(String s)
        throws IOException
    {
        print("public ");
        print(s);
        println(" () {");
        incrementIndent();
    }

    public void decrementIndent()
    {
        indentCount = indentCount - 1;
    }

    protected void endClass()
        throws IOException
    {
        decrementIndent();
        println();
        print("}");
        println();
    }

    protected void endInit()
        throws IOException
    {
        decrementIndent();
        print("}");
        println();
    }

    public abstract void gen()
        throws IOException;

    protected void genField(String s, Class class1)
        throws IOException
    {
        if (class1 == [C)
        {
            print("char[]");
        }
        print(" ");
        print(s);
        println(";");
    }

    public void incrementIndent()
    {
        indentCount = indentCount + 1;
    }

    protected void print(String s)
        throws IOException
    {
        out.append(s);
    }

    protected void printClassName(Class class1)
        throws IOException
    {
        print(class1.getName().replace('$', '.'));
    }

    public void printIndent()
        throws IOException
    {
        for (int i = 0; i < indentCount; i++)
        {
            print(indent);
        }

    }

    protected void printPackage()
        throws IOException
    {
        print("package ");
        print(clazz.getPackage().getName());
        println(";");
    }

    protected void println()
        throws IOException
    {
        out.append("\n");
        printIndent();
    }

    protected void println(String s)
        throws IOException
    {
        out.append(s);
        out.append("\n");
        printIndent();
    }
}
