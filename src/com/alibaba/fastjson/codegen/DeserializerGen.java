// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.codegen;

import com.alibaba.fastjson.util.DeserializeBeanInfo;
import com.alibaba.fastjson.util.FieldInfo;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

// Referenced classes of package com.alibaba.fastjson.codegen:
//            ClassGen

public class DeserializerGen extends ClassGen
{

    private DeserializeBeanInfo beanInfo;
    private String genClassName;

    public DeserializerGen(Class class1, Appendable appendable)
    {
        super(class1, appendable);
    }

    private void genBatchSet(List list, boolean flag)
        throws IOException
    {
        int j = list.size();
        int i = 0;
        while (i < j) 
        {
            FieldInfo fieldinfo = (FieldInfo)list.get(i);
            String s = (new StringBuilder()).append("_asm_flag_").append(i / 32).toString();
            if (flag)
            {
                print("if ((");
                print(s);
                print(" & ");
                print(Integer.toString(1 << i));
                print(") != 0) {");
                println();
                incrementIndent();
            }
            if (fieldinfo.getMethod() != null)
            {
                print("\tinstance.");
                print(fieldinfo.getMethod().getName());
                print("(");
                printFieldVarName(fieldinfo);
                println(");");
            } else
            {
                print("\tinstance.");
                print(fieldinfo.getField().getName());
                print(" = ");
                printFieldVarName(fieldinfo);
                println(";");
            }
            if (flag)
            {
                decrementIndent();
                println();
                println("}");
            }
            i++;
        }
    }

    private void genEndCheck()
        throws IOException
    {
        println("if (matchedCount <= 0 || lexer.token() != JSONToken.RBRACE) {");
        println("\trestFlag = true;");
        println("} else if (lexer.token() == JSONToken.COMMA) {");
        println("\tlexer.nextToken();");
        println("}");
    }

    private void genSetFlag(int i)
        throws IOException
    {
        print((new StringBuilder()).append("_asm_flag_").append(i / 32).toString());
        print(" |= ");
        print(Integer.toString(1 << i));
        print(";");
        println();
    }

    private void printFieldDeser(FieldInfo fieldinfo)
        throws IOException
    {
        print(fieldinfo.getName());
        print("_gen_deser__");
    }

    private void printFieldPrefix(FieldInfo fieldinfo)
        throws IOException
    {
        print(fieldinfo.getName());
        print("_gen_prefix__");
    }

    private void printFieldVarEnumName(FieldInfo fieldinfo)
        throws IOException
    {
        print(fieldinfo.getName());
        print("_gen_enum_name");
    }

    private void printFieldVarName(FieldInfo fieldinfo)
        throws IOException
    {
        print(fieldinfo.getName());
        print("_gen");
    }

    private void printListFieldItemDeser(FieldInfo fieldinfo)
        throws IOException
    {
        print(fieldinfo.getName());
        print("_gen_list_item_deser__");
    }

    private void printListFieldItemType(FieldInfo fieldinfo)
        throws IOException
    {
        print(fieldinfo.getName());
        print("_gen_list_item_type__");
    }

    public void gen()
        throws IOException
    {
        beanInfo = DeserializeBeanInfo.computeSetters(clazz, type);
        genClassName = (new StringBuilder()).append(clazz.getSimpleName()).append("GenDecoder").toString();
        print("package ");
        print(clazz.getPackage().getName());
        println(";");
        println();
        println("import java.lang.reflect.Type;");
        println();
        println("import com.alibaba.fastjson.parser.DefaultJSONParser;");
        println("import com.alibaba.fastjson.parser.DefaultJSONParser.ResolveTask;");
        println("import com.alibaba.fastjson.parser.ParserConfig;");
        println("import com.alibaba.fastjson.parser.Feature;");
        println("import com.alibaba.fastjson.parser.JSONLexerBase;");
        println("import com.alibaba.fastjson.parser.JSONToken;");
        println("import com.alibaba.fastjson.parser.ParseContext;");
        println("import com.alibaba.fastjson.parser.deserializer.ASMJavaBeanDeserializer;");
        println("import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;");
        println();
        print("public class ");
        print(genClassName);
        print(" extends ASMJavaBeanDeserializer implements ObjectDeserializer {");
        incrementIndent();
        println();
        genConstructor();
        genCreateInstance();
        genDeserialze();
        endClass();
    }

    protected void genConstructor()
        throws IOException
    {
        boolean flag1 = false;
        int j = beanInfo.getFieldList().size();
        for (int i = 0; i < j; i++)
        {
            FieldInfo fieldinfo = (FieldInfo)beanInfo.getFieldList().get(i);
            print("private char[] ");
            printFieldPrefix(fieldinfo);
            print(" = \"\\\"");
            print(fieldinfo.getName());
            print("\\\":\".toCharArray();");
            println();
        }

        println();
        int k = beanInfo.getFieldList().size();
        j = 0;
        boolean flag = flag1;
        do
        {
            if (j >= k)
            {
                break;
            }
            FieldInfo fieldinfo1 = (FieldInfo)beanInfo.getFieldList().get(j);
            Class class1 = fieldinfo1.getFieldClass();
            if (!class1.isPrimitive() && !class1.isEnum())
            {
                print("private ObjectDeserializer ");
                if (java/util/Collection.isAssignableFrom(class1))
                {
                    printListFieldItemDeser(fieldinfo1);
                } else
                {
                    printFieldDeser(fieldinfo1);
                }
                println(";");
                if (java/util/Collection.isAssignableFrom(class1))
                {
                    print("private Type ");
                    printListFieldItemType(fieldinfo1);
                    print(" = ");
                    printClassName(TypeUtils.getCollectionItemClass(fieldinfo1.getFieldType()));
                    println(".class;");
                }
                flag = true;
            }
            j++;
        } while (true);
        if (flag)
        {
            println();
        }
        print("public ");
        print(genClassName);
        print(" (ParserConfig config, Class clazz) {");
        incrementIndent();
        println();
        println("super(config, clazz);");
        decrementIndent();
        println();
        print("}");
        println();
    }

    protected void genCreateInstance()
        throws IOException
    {
        println();
        print("public Object createInstance(DefaultJSONParser parser, Type type) {");
        incrementIndent();
        println();
        print("return new ");
        print(clazz.getSimpleName());
        print("();");
        println();
        decrementIndent();
        println();
        print("}");
    }

    protected void genDeserialze()
        throws IOException
    {
        boolean flag = false;
        if (beanInfo.getFieldList().size() != 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Iterator iterator = beanInfo.getFieldList().iterator();
_L5:
        if (!iterator.hasNext()) goto _L4; else goto _L3
_L3:
        Class class1;
        Object obj;
        obj = (FieldInfo)iterator.next();
        class1 = ((FieldInfo) (obj)).getFieldClass();
        obj = ((FieldInfo) (obj)).getFieldType();
        if (class1 == Character.TYPE || java/util/Collection.isAssignableFrom(class1) && (!(obj instanceof ParameterizedType) || !(((ParameterizedType)obj).getActualTypeArguments()[0] instanceof Class))) goto _L1; else goto _L5
_L4:
        ArrayList arraylist = new ArrayList(beanInfo.getFieldList());
        Collections.sort(arraylist);
        println();
        print("public Object deserialze(DefaultJSONParser parser, Type type, Object fieldName) {");
        incrementIndent();
        println();
        println("JSONLexerBase lexer = (JSONLexerBase) parser.getLexer();");
        println();
        println("if (!lexer.isEnabled(Feature.SortFeidFastMatch)) {");
        println("\treturn super.deserialze(parser, type, fieldName);");
        println("}");
        println();
        println("if (isSupportArrayToBean(lexer)) {");
        println("\t// deserialzeArrayMapping");
        println("}");
        println();
        println("if (lexer.scanType(\"Department\") == JSONLexerBase.NOT_MATCH) {");
        println("\treturn super.deserialze(parser, type, fieldName);");
        println("}");
        println();
        println("ParseContext mark_context = parser.getContext();");
        println("int matchedCount = 0;");
        print(clazz.getSimpleName());
        print(" instance = ");
        int k;
        if (Modifier.isPublic(beanInfo.getDefaultConstructor().getModifiers()))
        {
            print("new ");
            print(clazz.getSimpleName());
            println("();");
        } else
        {
            print("(");
            print(clazz.getSimpleName());
            print(") createInstance(parser);");
        }
        println();
        println("ParseContext context = parser.getContext();");
        println("ParseContext childContext = parser.setContext(context, instance, fieldName);");
        println();
        println("if (lexer.matchStat == JSONLexerBase.END) {");
        println("\treturn instance;");
        println("}");
        println();
        println("int matchStat = 0;");
        k = arraylist.size();
        for (int i = 0; i < k; i += 32)
        {
            print("int _asm_flag_");
            print(Integer.toString(i / 32));
            println(" = 0;");
        }

        int j = 0;
        while (j < k) 
        {
            FieldInfo fieldinfo = (FieldInfo)arraylist.get(j);
            Class class2 = fieldinfo.getFieldClass();
            if (class2 == Boolean.TYPE)
            {
                print("boolean ");
                printFieldVarName(fieldinfo);
                println(" = false;");
            } else
            if (class2 == Byte.TYPE || class2 == Short.TYPE || class2 == Integer.TYPE || class2 == Long.TYPE || class2 == Float.TYPE || class2 == Double.TYPE)
            {
                print(class2.getSimpleName());
                print(" ");
                printFieldVarName(fieldinfo);
                println(" = 0;");
            } else
            if (class2 == java/lang/String)
            {
                print("String ");
                printFieldVarName(fieldinfo);
                println(";");
                println("if (lexer.isEnabled(Feature.InitStringFieldAsEmpty)) {");
                print("\t");
                printFieldVarName(fieldinfo);
                println(" = lexer.stringDefaultValue();");
                print("\t");
                genSetFlag(j);
                println("} else {");
                print("\t");
                printFieldVarName(fieldinfo);
                println(" = null;");
                println("}");
            } else
            {
                printClassName(class2);
                print(" ");
                printFieldVarName(fieldinfo);
                print(" = null;");
                println();
            }
            j++;
        }
        println("boolean endFlag = false, restFlag = false;");
        println();
        j = ((flag) ? 1 : 0);
        while (j < k) 
        {
            print("if ((!endFlag) && (!restFlag)) {");
            incrementIndent();
            println();
            FieldInfo fieldinfo1 = (FieldInfo)arraylist.get(j);
            Class class3 = fieldinfo1.getFieldClass();
            Object obj1 = fieldinfo1.getFieldType();
            if (class3 == Boolean.TYPE)
            {
                printFieldVarName(fieldinfo1);
                print(" = lexer.scanFieldBoolean(");
                printFieldPrefix(fieldinfo1);
                println(");");
            } else
            if (class3 == Byte.TYPE || class3 == Short.TYPE || class3 == Integer.TYPE)
            {
                printFieldVarName(fieldinfo1);
                print(" = lexer.scanFieldInt(");
                printFieldPrefix(fieldinfo1);
                println(");");
            } else
            if (class3 == Long.TYPE)
            {
                printFieldVarName(fieldinfo1);
                print(" = lexer.scanFieldLong(");
                printFieldPrefix(fieldinfo1);
                println(");");
            } else
            if (class3 == Float.TYPE)
            {
                printFieldVarName(fieldinfo1);
                print(" = lexer.scanFieldFloat(");
                printFieldPrefix(fieldinfo1);
                println(");");
            } else
            if (class3 == Double.TYPE)
            {
                printFieldVarName(fieldinfo1);
                print(" = lexer.scanFieldDouble(");
                printFieldPrefix(fieldinfo1);
                println(");");
            } else
            if (class3 == java/lang/String)
            {
                printFieldVarName(fieldinfo1);
                print(" = lexer.scanFieldString(");
                printFieldPrefix(fieldinfo1);
                println(");");
            } else
            if (class3.isEnum())
            {
                print("String ");
                printFieldVarEnumName(fieldinfo1);
                print(" = lexer.scanFieldSymbol(");
                printFieldPrefix(fieldinfo1);
                println(", parser.getSymbolTable());");
                print("if (");
                printFieldVarEnumName(fieldinfo1);
                println(" == null) {");
                print("\t");
                printFieldVarName(fieldinfo1);
                print(" = ");
                printClassName(class3);
                print(".valueOf(");
                printFieldVarEnumName(fieldinfo1);
                println(");");
                println("}");
            } else
            if (java/util/Collection.isAssignableFrom(class3))
            {
                obj1 = TypeUtils.getCollectionItemClass(((java.lang.reflect.Type) (obj1)));
                if (obj1 == java/lang/String)
                {
                    printFieldVarName(fieldinfo1);
                    print(" = (");
                    printClassName(class3);
                    print(") lexer.scanFieldStringArray(");
                    printFieldPrefix(fieldinfo1);
                    print(", ");
                    printClassName(class3);
                    print(".class);");
                    println();
                } else
                {
                    genDeserialzeList(fieldinfo1, j, ((Class) (obj1)));
                    if (j == k - 1)
                    {
                        genEndCheck();
                    }
                }
            } else
            {
                genDeserialzeObject(fieldinfo1, j);
                if (j == k - 1)
                {
                    genEndCheck();
                }
            }
            println("if(lexer.matchStat > 0) {");
            print("\t");
            genSetFlag(j);
            println("\tmatchedCount++;");
            println("}");
            println("if(lexer.matchStat == JSONLexerBase.NOT_MATCH) {");
            println("\trestFlag = true;");
            println("}");
            println("if(lexer.matchStat != JSONLexerBase.END) {");
            println("\tendFlag = true;");
            println("}");
            decrementIndent();
            println();
            println("}");
            j++;
        }
        genBatchSet(arraylist, true);
        println();
        println("if (restFlag) {");
        println("\treturn super.parseRest(parser, type, fieldName, instance);");
        println("}");
        println();
        print("return instance;");
        println();
        decrementIndent();
        println();
        print("}");
        return;
    }

    protected void genDeserialzeList(FieldInfo fieldinfo, int i, Class class1)
        throws IOException
    {
        print("if (lexer.matchField(");
        printFieldPrefix(fieldinfo);
        print(")) {");
        println();
        print("\t");
        genSetFlag(i);
        println("\tif (lexer.token() == JSONToken.NULL) {");
        println("\t\tlexer.nextToken(JSONToken.COMMA);");
        println("\t} else {");
        println("\t\tif (lexer.token() == JSONToken.LBRACKET) {");
        print("\t\t\tif(");
        printListFieldItemDeser(fieldinfo);
        print(" == null) {");
        println();
        print("\t\t\t\t");
        printListFieldItemDeser(fieldinfo);
        print(" = parser.getConfig().getDeserializer(");
        printClassName(class1);
        print(".class);");
        println();
        print("\t\t\t}");
        println();
        print("\t\t\tfinal int fastMatchToken = ");
        printListFieldItemDeser(fieldinfo);
        print(".getFastMatchToken();");
        println();
        println("\t\t\tlexer.nextToken(fastMatchToken);");
        print("\t\t\t");
        printFieldVarName(fieldinfo);
        print(" = ");
        Class class2 = fieldinfo.getFieldClass();
        if (class2.isAssignableFrom(java/util/ArrayList))
        {
            print("new java.util.ArrayList();");
        } else
        if (class2.isAssignableFrom(java/util/LinkedList))
        {
            print("new java.util.LinkedList();");
        } else
        if (class2.isAssignableFrom(java/util/HashSet))
        {
            print("new java.util.HashSet();");
        } else
        if (class2.isAssignableFrom(java/util/TreeSet))
        {
            print("new java.util.TreeSet();");
        } else
        {
            print("new ");
            printClassName(class2);
            print("();");
        }
        println();
        println("\t\t\tParseContext listContext = parser.getContext();");
        print("\t\t\tparser.setContext(");
        printFieldVarName(fieldinfo);
        print(", \"");
        print(fieldinfo.getName());
        print("\");");
        println();
        println();
        println("\t\t\tfor(int i = 0; ;++i) {");
        println("\t\t\t\tif (lexer.token() == JSONToken.RBRACKET) {");
        println("\t\t\t\t\tbreak;");
        println("\t\t\t\t}");
        print("\t\t\t\t");
        printClassName(class1);
        print(" itemValue = ");
        printListFieldItemDeser(fieldinfo);
        print(".deserialze(parser, ");
        printListFieldItemType(fieldinfo);
        println(", i);");
        print("\t\t\t\t");
        printFieldVarName(fieldinfo);
        println(".add(itemValue);");
        print("\t\t\t\tparser.checkListResolve(");
        printFieldVarName(fieldinfo);
        println(");");
        println("\t\t\t\tif (lexer.token() == JSONToken.COMMA) {");
        println("\t\t\t\t\tlexer.nextToken(fastMatchToken);");
        println("\t\t\t\t}");
        println("\t\t\t}");
        println("\t\t\tparser.setContext(listContext);");
        println("\t\t\tif (lexer.token() != JSONToken.RBRACKET) {");
        println("\t\t\t\trestFlag = true;");
        println("\t\t\t}");
        println("\t\t\tlexer.nextToken(JSONToken.COMMA);");
        println();
        println("\t\t} else {");
        println("\t\t\trestFlag = true;");
        println("\t\t}");
        println("\t}");
        println("}");
    }

    protected void genDeserialzeObject(FieldInfo fieldinfo, int i)
        throws IOException
    {
        print("if (lexer.matchField(");
        printFieldPrefix(fieldinfo);
        print(")) {");
        println();
        print("\t");
        genSetFlag(i);
        println("\tmatchedCount++;");
        print("if (");
        printFieldDeser(fieldinfo);
        print(" == null) {");
        println();
        print("\t");
        printFieldDeser(fieldinfo);
        print(" = parser.getConfig().getDeserializer(");
        printClassName(fieldinfo.getFieldClass());
        println(".class);");
        println("}");
        print("\t");
        printFieldDeser(fieldinfo);
        print(".deserialze(parser, ");
        if (fieldinfo.getFieldType() instanceof Class)
        {
            printClassName(fieldinfo.getFieldClass());
            print(".class");
        } else
        {
            print("getFieldType(\"");
            println(fieldinfo.getName());
            print("\")");
        }
        print(",\"");
        print(fieldinfo.getName());
        println("\");");
        println("\tif(parser.getResolveStatus() == DefaultJSONParser.NeedToResolve) {");
        println("\t\tResolveTask resolveTask = parser.getLastResolveTask();");
        println("\t\tresolveTask.setOwnerContext(parser.getContext());");
        print("\t\tresolveTask.setFieldDeserializer(this.getFieldDeserializer(\"");
        print(fieldinfo.getName());
        println("\"));");
        println("\t\tparser.setResolveStatus(DefaultJSONParser.NONE);");
        println("\t}");
        println("}");
    }
}
