// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;


// Referenced classes of package com.alibaba.fastjson:
//            JSONPath

static class not
    implements not
{

    private final boolean not;
    private final String propertyName;
    private final Long values[];

    public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
    {
        boolean flag;
        flag = true;
        jsonpath = ((JSONPath) (jsonpath.getPropertyValue(obj2, propertyName, false)));
        if (jsonpath == null)
        {
            jsonpath = values;
            int k = jsonpath.length;
            for (int i = 0; i < k; i++)
            {
                if (jsonpath[i] == null)
                {
                    return !not;
                }
            }

            return not;
        }
        if (!(jsonpath instanceof Number)) goto _L2; else goto _L1
_L1:
        int j;
        int l;
        long l1;
        l1 = ((Number)jsonpath).longValue();
        jsonpath = values;
        l = jsonpath.length;
        j = 0;
_L4:
        if (j >= l)
        {
            break; /* Loop/switch isn't completed */
        }
        obj = jsonpath[j];
          goto _L3
_L6:
        j++;
        if (true) goto _L4; else goto _L2
_L3:
        if (obj == null || ((Long) (obj)).longValue() != l1) goto _L6; else goto _L5
_L5:
        if (not)
        {
            flag = false;
        }
        return flag;
_L2:
        return not;
    }

    public (String s, Long along[], boolean flag)
    {
        propertyName = s;
        values = along;
        not = flag;
    }
}
