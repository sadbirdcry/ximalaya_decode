// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Referenced classes of package com.alibaba.fastjson:
//            JSONPath

static class not
    implements not
{

    private final boolean not;
    private final Pattern pattern;
    private final String propertyName;

    public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
    {
        jsonpath = ((JSONPath) (jsonpath.getPropertyValue(obj2, propertyName, false)));
        if (jsonpath != null)
        {
            jsonpath = jsonpath.toString();
            boolean flag = pattern.matcher(jsonpath).matches();
            if (not)
            {
                if (!flag)
                {
                    return true;
                }
            } else
            {
                return flag;
            }
        }
        return false;
    }

    public (String s, String s1, boolean flag)
    {
        propertyName = s;
        pattern = Pattern.compile(s1);
        not = flag;
    }
}
