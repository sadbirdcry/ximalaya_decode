// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;


// Referenced classes of package com.alibaba.fastjson:
//            JSONPath

static final class  extends Enum
{

    private static final NOT_BETWEEN $VALUES[];
    public static final NOT_BETWEEN BETWEEN;
    public static final NOT_BETWEEN EQ;
    public static final NOT_BETWEEN GE;
    public static final NOT_BETWEEN GT;
    public static final NOT_BETWEEN IN;
    public static final NOT_BETWEEN LE;
    public static final NOT_BETWEEN LIKE;
    public static final NOT_BETWEEN LT;
    public static final NOT_BETWEEN NE;
    public static final NOT_BETWEEN NOT_BETWEEN;
    public static final NOT_BETWEEN NOT_IN;
    public static final NOT_BETWEEN NOT_LIKE;
    public static final NOT_BETWEEN NOT_RLIKE;
    public static final NOT_BETWEEN RLIKE;

    public static  valueOf(String s)
    {
        return ()Enum.valueOf(com/alibaba/fastjson/JSONPath$Operator, s);
    }

    public static [] values()
    {
        return ([])$VALUES.clone();
    }

    static 
    {
        EQ = new <init>("EQ", 0);
        NE = new <init>("NE", 1);
        GT = new <init>("GT", 2);
        GE = new <init>("GE", 3);
        LT = new <init>("LT", 4);
        LE = new <init>("LE", 5);
        LIKE = new <init>("LIKE", 6);
        NOT_LIKE = new <init>("NOT_LIKE", 7);
        RLIKE = new <init>("RLIKE", 8);
        NOT_RLIKE = new <init>("NOT_RLIKE", 9);
        IN = new <init>("IN", 10);
        NOT_IN = new <init>("NOT_IN", 11);
        BETWEEN = new <init>("BETWEEN", 12);
        NOT_BETWEEN = new <init>("NOT_BETWEEN", 13);
        $VALUES = (new .VALUES[] {
            EQ, NE, GT, GE, LT, LE, LIKE, NOT_LIKE, RLIKE, NOT_RLIKE, 
            IN, NOT_IN, BETWEEN, NOT_BETWEEN
        });
    }

    private (String s, int i)
    {
        super(s, i);
    }
}
