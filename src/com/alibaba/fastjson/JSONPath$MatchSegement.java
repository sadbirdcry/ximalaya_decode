// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;


// Referenced classes of package com.alibaba.fastjson:
//            JSONPath

static class minLength
    implements minLength
{

    private final String containsValues[];
    private final String endsWithValue;
    private final int minLength;
    private final boolean not;
    private final String propertyName;
    private final String startsWithValue;

    public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
    {
        jsonpath = ((JSONPath) (jsonpath.getPropertyValue(obj2, propertyName, false)));
        if (jsonpath != null)
        {
            jsonpath = jsonpath.toString();
            if (jsonpath.length() < minLength)
            {
                return not;
            }
            int i;
            if (startsWithValue != null)
            {
                if (!jsonpath.startsWith(startsWithValue))
                {
                    return not;
                }
                i = startsWithValue.length() + 0;
            } else
            {
                i = 0;
            }
            if (containsValues != null)
            {
                obj = containsValues;
                int k = obj.length;
                boolean flag = false;
                int j = i;
                for (i = ((flag) ? 1 : 0); i < k; i++)
                {
                    obj1 = obj[i];
                    j = jsonpath.indexOf(((String) (obj1)), j);
                    if (j == -1)
                    {
                        return not;
                    }
                    j += ((String) (obj1)).length();
                }

            }
            if (endsWithValue != null && !jsonpath.endsWith(endsWithValue))
            {
                return not;
            }
            if (!not)
            {
                return true;
            }
        }
        return false;
    }

    public (String s, String s1, String s2, String as[], boolean flag)
    {
        boolean flag1 = false;
        super();
        propertyName = s;
        startsWithValue = s1;
        endsWithValue = s2;
        containsValues = as;
        not = flag;
        int i;
        int j;
        int k;
        if (s1 != null)
        {
            j = s1.length() + 0;
        } else
        {
            j = 0;
        }
        i = j;
        if (s2 != null)
        {
            i = j + s2.length();
        }
        k = i;
        if (as != null)
        {
            int l = as.length;
            j = ((flag1) ? 1 : 0);
            do
            {
                k = i;
                if (j >= l)
                {
                    break;
                }
                k = as[j].length();
                j++;
                i = k + i;
            } while (true);
        }
        minLength = k;
    }
}
