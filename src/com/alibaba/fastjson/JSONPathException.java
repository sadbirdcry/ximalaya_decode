// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;


// Referenced classes of package com.alibaba.fastjson:
//            JSONException

public class JSONPathException extends JSONException
{

    private static final long serialVersionUID = 1L;

    public JSONPathException(String s)
    {
        super(s);
    }

    public JSONPathException(String s, Throwable throwable)
    {
        super(s, throwable);
    }
}
