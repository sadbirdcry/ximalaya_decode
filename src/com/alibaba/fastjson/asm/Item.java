// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.asm;


final class Item
{

    int hashCode;
    int index;
    int intVal;
    long longVal;
    Item next;
    String strVal1;
    String strVal2;
    String strVal3;
    int type;

    Item()
    {
    }

    Item(int i, Item item)
    {
        index = i;
        type = item.type;
        intVal = item.intVal;
        longVal = item.longVal;
        strVal1 = item.strVal1;
        strVal2 = item.strVal2;
        strVal3 = item.strVal3;
        hashCode = item.hashCode;
    }

    boolean isEqualTo(Item item)
    {
        type;
        JVM INSTR tableswitch 1 15: default 80
    //                   1 124
    //                   2 80
    //                   3 150
    //                   4 150
    //                   5 136
    //                   6 136
    //                   7 124
    //                   8 124
    //                   9 80
    //                   10 80
    //                   11 80
    //                   12 163
    //                   13 124
    //                   14 80
    //                   15 136;
           goto _L1 _L2 _L1 _L3 _L3 _L4 _L4 _L2 _L2 _L1 _L1 _L1 _L5 _L2 _L1 _L4
_L1:
        if (!item.strVal1.equals(strVal1) || !item.strVal2.equals(strVal2) || !item.strVal3.equals(strVal3)) goto _L7; else goto _L6
_L6:
        return true;
_L2:
        return item.strVal1.equals(strVal1);
_L4:
        if (item.longVal != longVal)
        {
            return false;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (item.intVal != intVal)
        {
            return false;
        }
        continue; /* Loop/switch isn't completed */
_L5:
        if (!item.strVal1.equals(strVal1) || !item.strVal2.equals(strVal2))
        {
            return false;
        }
        if (true) goto _L6; else goto _L7
_L7:
        return false;
    }

    void set(int i)
    {
        type = 3;
        intVal = i;
        hashCode = 0x7fffffff & type + i;
    }

    void set(int i, String s, String s1, String s2)
    {
        type = i;
        strVal1 = s;
        strVal2 = s1;
        strVal3 = s2;
        switch (i)
        {
        default:
            hashCode = s.hashCode() * s1.hashCode() * s2.hashCode() + i & 0x7fffffff;
            return;

        case 1: // '\001'
        case 7: // '\007'
        case 8: // '\b'
        case 13: // '\r'
            hashCode = s.hashCode() + i & 0x7fffffff;
            return;

        case 12: // '\f'
            hashCode = s.hashCode() * s1.hashCode() + i & 0x7fffffff;
            break;
        }
    }
}
