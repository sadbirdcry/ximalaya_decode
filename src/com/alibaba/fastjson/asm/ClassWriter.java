// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.asm;


// Referenced classes of package com.alibaba.fastjson.asm:
//            ByteVector, Item, Type, FieldWriter, 
//            MethodWriter, FieldVisitor, MethodVisitor

public class ClassWriter
{

    static final int ACC_SYNTHETIC_ATTRIBUTE = 0x40000;
    static final int CLASS = 7;
    public static final int COMPUTE_FRAMES = 2;
    public static final int COMPUTE_MAXS = 1;
    static final int DOUBLE = 6;
    static final int FIELD = 9;
    static final int FIELDORMETH_INSN = 6;
    static final int FLOAT = 4;
    static final int IINC_INSN = 12;
    static final int IMETH = 11;
    static final int IMPLVAR_INSN = 4;
    static final int INT = 3;
    static final int ITFDYNMETH_INSN = 7;
    static final int LABELW_INSN = 9;
    static final int LABEL_INSN = 8;
    static final int LDCW_INSN = 11;
    static final int LDC_INSN = 10;
    static final int LONG = 5;
    static final int LOOK_INSN = 14;
    static final int MANA_INSN = 15;
    static final int METH = 10;
    static final int NAME_TYPE = 12;
    static final int NOARG_INSN = 0;
    static final int SBYTE_INSN = 1;
    static final int SHORT_INSN = 2;
    static final int STR = 8;
    static final int TABL_INSN = 13;
    static final byte TYPE[];
    static final int TYPE_INSN = 5;
    static final int TYPE_MERGED = 15;
    static final int TYPE_NORMAL = 13;
    static final int TYPE_UNINIT = 14;
    static final int UTF8 = 1;
    static final int VAR_INSN = 3;
    static final int WIDE_INSN = 16;
    private int access;
    FieldWriter firstField;
    MethodWriter firstMethod;
    int index;
    private int interfaceCount;
    private int interfaces[];
    Item items[];
    final Item key;
    final Item key2;
    final Item key3;
    FieldWriter lastField;
    MethodWriter lastMethod;
    private int name;
    final ByteVector pool;
    private int superName;
    String thisName;
    int threshold;
    Item typeTable[];
    int version;

    public ClassWriter()
    {
        this(0);
    }

    private ClassWriter(int i)
    {
        index = 1;
        pool = new ByteVector();
        items = new Item[256];
        threshold = (int)(0.75D * (double)items.length);
        key = new Item();
        key2 = new Item();
        key3 = new Item();
    }

    private Item get(Item item)
    {
        Item item1;
        for (item1 = items[item.hashCode % items.length]; item1 != null && (item1.type != item.type || !item.isEqualTo(item1)); item1 = item1.next) { }
        return item1;
    }

    private Item newString(String s)
    {
        key2.set(8, s, null, null);
        Item item1 = get(key2);
        Item item = item1;
        if (item1 == null)
        {
            pool.put12(8, newUTF8(s));
            int i = index;
            index = i + 1;
            item = new Item(i, key2);
            put(item);
        }
        return item;
    }

    private void put(Item item)
    {
        if (index > threshold)
        {
            int i = items.length;
            int k = i * 2 + 1;
            Item aitem[] = new Item[k];
            for (i--; i >= 0; i--)
            {
                Item item2;
                for (Item item1 = items[i]; item1 != null; item1 = item2)
                {
                    int l = item1.hashCode % aitem.length;
                    item2 = item1.next;
                    item1.next = aitem[l];
                    aitem[l] = item1;
                }

            }

            items = aitem;
            threshold = (int)((double)k * 0.75D);
        }
        int j = item.hashCode % items.length;
        item.next = items[j];
        items[j] = item;
    }

    private void put122(int i, int j, int k)
    {
        pool.put12(i, j).putShort(k);
    }

    public int newClass(String s)
    {
        return newClassItem(s).index;
    }

    Item newClassItem(String s)
    {
        key2.set(7, s, null, null);
        Item item1 = get(key2);
        Item item = item1;
        if (item1 == null)
        {
            pool.put12(7, newUTF8(s));
            int i = index;
            index = i + 1;
            item = new Item(i, key2);
            put(item);
        }
        return item;
    }

    Item newConstItem(Object obj)
    {
        if (obj instanceof Integer)
        {
            return newInteger(((Integer)obj).intValue());
        }
        if (obj instanceof String)
        {
            return newString((String)obj);
        }
        if (obj instanceof Type)
        {
            obj = (Type)obj;
            if (((Type) (obj)).getSort() == 10)
            {
                obj = ((Type) (obj)).getInternalName();
            } else
            {
                obj = ((Type) (obj)).getDescriptor();
            }
            return newClassItem(((String) (obj)));
        } else
        {
            throw new IllegalArgumentException((new StringBuilder()).append("value ").append(obj).toString());
        }
    }

    Item newFieldItem(String s, String s1, String s2)
    {
        key3.set(9, s, s1, s2);
        Item item1 = get(key3);
        Item item = item1;
        if (item1 == null)
        {
            put122(9, newClass(s), newNameType(s1, s2));
            int i = index;
            index = i + 1;
            item = new Item(i, key3);
            put(item);
        }
        return item;
    }

    Item newInteger(int i)
    {
        key.set(i);
        Item item1 = get(key);
        Item item = item1;
        if (item1 == null)
        {
            pool.putByte(3).putInt(i);
            i = index;
            index = i + 1;
            item = new Item(i, key);
            put(item);
        }
        return item;
    }

    Item newMethodItem(String s, String s1, String s2, boolean flag)
    {
        Item item;
        int i;
        if (flag)
        {
            i = 11;
        } else
        {
            i = 10;
        }
        key3.set(i, s, s1, s2);
        item = get(key3);
        if (item == null)
        {
            put122(i, newClass(s), newNameType(s1, s2));
            i = index;
            index = i + 1;
            s = new Item(i, key3);
            put(s);
            return s;
        } else
        {
            return item;
        }
    }

    public int newNameType(String s, String s1)
    {
        return newNameTypeItem(s, s1).index;
    }

    Item newNameTypeItem(String s, String s1)
    {
        key2.set(12, s, s1, null);
        Item item1 = get(key2);
        Item item = item1;
        if (item1 == null)
        {
            put122(12, newUTF8(s), newUTF8(s1));
            int i = index;
            index = i + 1;
            item = new Item(i, key2);
            put(item);
        }
        return item;
    }

    public int newUTF8(String s)
    {
        key.set(1, s, null, null);
        Item item1 = get(key);
        Item item = item1;
        if (item1 == null)
        {
            pool.putByte(1).putUTF8(s);
            int i = index;
            index = i + 1;
            item = new Item(i, key);
            put(item);
        }
        return item.index;
    }

    public byte[] toByteArray()
    {
        int i = interfaceCount * 2 + 24;
        Object obj = firstField;
        int j;
        for (j = 0; obj != null; j++)
        {
            i += ((FieldWriter) (obj)).getSize();
            obj = ((FieldWriter) (obj)).next;
        }

        obj = firstMethod;
        boolean flag = false;
        int k = i;
        i = ((flag) ? 1 : 0);
        for (; obj != null; obj = ((MethodWriter) (obj)).next)
        {
            i++;
            k += ((MethodWriter) (obj)).getSize();
        }

        ByteVector bytevector = new ByteVector(pool.length + k);
        bytevector.putInt(0xcafebabe).putInt(version);
        bytevector.putShort(index).putByteArray(pool.data, 0, pool.length);
        bytevector.putShort(~(0x60000 | (access & 0x40000) / 64) & access).putShort(name).putShort(superName);
        bytevector.putShort(interfaceCount);
        for (int l = 0; l < interfaceCount; l++)
        {
            bytevector.putShort(interfaces[l]);
        }

        bytevector.putShort(j);
        for (FieldWriter fieldwriter = firstField; fieldwriter != null; fieldwriter = fieldwriter.next)
        {
            fieldwriter.put(bytevector);
        }

        bytevector.putShort(i);
        for (MethodWriter methodwriter = firstMethod; methodwriter != null; methodwriter = methodwriter.next)
        {
            methodwriter.put(bytevector);
        }

        bytevector.putShort(0);
        return bytevector.data;
    }

    public void visit(int i, int j, String s, String s1, String as[])
    {
        boolean flag = false;
        version = i;
        access = j;
        name = newClass(s);
        thisName = s;
        if (s1 == null)
        {
            i = 0;
        } else
        {
            i = newClass(s1);
        }
        superName = i;
        if (as != null && as.length > 0)
        {
            interfaceCount = as.length;
            interfaces = new int[interfaceCount];
            for (i = ((flag) ? 1 : 0); i < interfaceCount; i++)
            {
                interfaces[i] = newClass(as[i]);
            }

        }
    }

    public FieldVisitor visitField(int i, String s, String s1)
    {
        return new FieldWriter(this, i, s, s1);
    }

    public MethodVisitor visitMethod(int i, String s, String s1, String s2, String as[])
    {
        return new MethodWriter(this, i, s, s1, s2, as);
    }

    static 
    {
        byte abyte0[] = new byte[220];
        for (int i = 0; i < abyte0.length; i++)
        {
            abyte0[i] = (byte)("AAAAAAAAAAAAAAAABCKLLDDDDDEEEEEEEEEEEEEEEEEEEEAAAAAAAADDDDDEEEEEEEEEEEEEEEEEEEEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAAAAAAAAAAAAAAAAAAIIIIIIIIIIIIIIIIDNOAAAAAAGGGGGGGHHFBFAAFFAAQPIIJJIIIIIIIIIIIIIIIIII".charAt(i) - 65);
        }

        TYPE = abyte0;
    }
}
