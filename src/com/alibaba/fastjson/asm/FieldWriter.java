// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.asm;


// Referenced classes of package com.alibaba.fastjson.asm:
//            FieldVisitor, ClassWriter, ByteVector

final class FieldWriter
    implements FieldVisitor
{

    private final int access;
    private final int desc;
    private final int name;
    FieldWriter next;

    FieldWriter(ClassWriter classwriter, int i, String s, String s1)
    {
        if (classwriter.firstField == null)
        {
            classwriter.firstField = this;
        } else
        {
            classwriter.lastField.next = this;
        }
        classwriter.lastField = this;
        access = i;
        name = classwriter.newUTF8(s);
        desc = classwriter.newUTF8(s1);
    }

    int getSize()
    {
        return 8;
    }

    void put(ByteVector bytevector)
    {
        bytevector.putShort(~(0x60000 | (access & 0x40000) / 64) & access).putShort(name).putShort(desc);
        bytevector.putShort(0);
    }

    public void visitEnd()
    {
    }
}
