// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.asm;


// Referenced classes of package com.alibaba.fastjson.asm:
//            Label

public interface MethodVisitor
{

    public abstract void visitEnd();

    public abstract void visitFieldInsn(int i, String s, String s1, String s2);

    public abstract void visitIincInsn(int i, int j);

    public abstract void visitInsn(int i);

    public abstract void visitIntInsn(int i, int j);

    public abstract void visitJumpInsn(int i, Label label);

    public abstract void visitLabel(Label label);

    public abstract void visitLdcInsn(Object obj);

    public abstract void visitMaxs(int i, int j);

    public abstract void visitMethodInsn(int i, String s, String s1, String s2);

    public abstract void visitTypeInsn(int i, String s);

    public abstract void visitVarInsn(int i, int j);
}
