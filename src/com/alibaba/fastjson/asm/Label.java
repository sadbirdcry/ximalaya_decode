// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.asm;


// Referenced classes of package com.alibaba.fastjson.asm:
//            ByteVector, MethodWriter

public class Label
{

    static final int RESOLVED = 2;
    public Object info;
    int inputStackTop;
    int line;
    Label next;
    int outputStackMax;
    int position;
    private int referenceCount;
    private int srcAndRefPositions[];
    int status;
    Label successor;

    public Label()
    {
    }

    private void addReference(int i, int j)
    {
        if (srcAndRefPositions == null)
        {
            srcAndRefPositions = new int[6];
        }
        if (referenceCount >= srcAndRefPositions.length)
        {
            int ai[] = new int[srcAndRefPositions.length + 6];
            System.arraycopy(srcAndRefPositions, 0, ai, 0, srcAndRefPositions.length);
            srcAndRefPositions = ai;
        }
        int ai1[] = srcAndRefPositions;
        int k = referenceCount;
        referenceCount = k + 1;
        ai1[k] = i;
        ai1 = srcAndRefPositions;
        i = referenceCount;
        referenceCount = i + 1;
        ai1[i] = j;
    }

    void put(MethodWriter methodwriter, ByteVector bytevector, int i)
    {
        if ((status & 2) == 0)
        {
            addReference(i, bytevector.length);
            bytevector.putShort(-1);
            return;
        } else
        {
            bytevector.putShort(position - i);
            return;
        }
    }

    void resolve(MethodWriter methodwriter, int i, byte abyte0[])
    {
        status = status | 2;
        position = i;
        for (int j = 0; j < referenceCount;)
        {
            methodwriter = srcAndRefPositions;
            int k = j + 1;
            int l = methodwriter[j];
            methodwriter = srcAndRefPositions;
            j = k + 1;
            k = methodwriter[k];
            l = i - l;
            abyte0[k] = (byte)(l >>> 8);
            abyte0[k + 1] = (byte)l;
        }

    }
}
