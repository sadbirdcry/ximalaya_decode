// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.asm;


// Referenced classes of package com.alibaba.fastjson.asm:
//            MethodVisitor, ByteVector, ClassWriter, Item, 
//            Label, Type

class MethodWriter
    implements MethodVisitor
{

    static final int ACC_CONSTRUCTOR = 0x40000;
    static final int APPEND_FRAME = 252;
    static final int CHOP_FRAME = 248;
    static final int FULL_FRAME = 255;
    static final int RESERVED = 128;
    static final int SAME_FRAME = 0;
    static final int SAME_FRAME_EXTENDED = 251;
    static final int SAME_LOCALS_1_STACK_ITEM_FRAME = 64;
    static final int SAME_LOCALS_1_STACK_ITEM_FRAME_EXTENDED = 247;
    private int access;
    int classReaderLength;
    private ByteVector code;
    final ClassWriter cw;
    private final int desc;
    int exceptionCount;
    int exceptions[];
    private int maxLocals;
    private int maxStack;
    private final int name;
    MethodWriter next;

    MethodWriter(ClassWriter classwriter, int i, String s, String s1, String s2, String as[])
    {
        code = new ByteVector();
        if (classwriter.firstMethod == null)
        {
            classwriter.firstMethod = this;
        } else
        {
            classwriter.lastMethod.next = this;
        }
        classwriter.lastMethod = this;
        cw = classwriter;
        access = i;
        name = classwriter.newUTF8(s);
        desc = classwriter.newUTF8(s1);
        if (as != null && as.length > 0)
        {
            exceptionCount = as.length;
            exceptions = new int[exceptionCount];
            for (i = 0; i < exceptionCount; i++)
            {
                exceptions[i] = classwriter.newClass(as[i]);
            }

        }
    }

    final int getSize()
    {
        int i = 8;
        if (code.length > 0)
        {
            cw.newUTF8("Code");
            i = 8 + (code.length + 18 + 0);
        }
        int j = i;
        if (exceptionCount > 0)
        {
            cw.newUTF8("Exceptions");
            j = i + (exceptionCount * 2 + 8);
        }
        return j;
    }

    final void put(ByteVector bytevector)
    {
        boolean flag = false;
        bytevector.putShort(~(0x60000 | (access & 0x40000) / 64) & access).putShort(name).putShort(desc);
        int i;
        int j;
        if (code.length > 0)
        {
            i = 1;
        } else
        {
            i = 0;
        }
        j = i;
        if (exceptionCount > 0)
        {
            j = i + 1;
        }
        bytevector.putShort(j);
        if (code.length > 0)
        {
            i = code.length;
            bytevector.putShort(cw.newUTF8("Code")).putInt(i + 12 + 0);
            bytevector.putShort(maxStack).putShort(maxLocals);
            bytevector.putInt(code.length).putByteArray(code.data, 0, code.length);
            bytevector.putShort(0);
            bytevector.putShort(0);
        }
        if (exceptionCount > 0)
        {
            bytevector.putShort(cw.newUTF8("Exceptions")).putInt(exceptionCount * 2 + 2);
            bytevector.putShort(exceptionCount);
            for (i = ((flag) ? 1 : 0); i < exceptionCount; i++)
            {
                bytevector.putShort(exceptions[i]);
            }

        }
    }

    public void visitEnd()
    {
    }

    public void visitFieldInsn(int i, String s, String s1, String s2)
    {
        s = cw.newFieldItem(s, s1, s2);
        code.put12(i, ((Item) (s)).index);
    }

    public void visitIincInsn(int i, int j)
    {
        code.putByte(132).put11(i, j);
    }

    public void visitInsn(int i)
    {
        code.putByte(i);
    }

    public void visitIntInsn(int i, int j)
    {
        code.put11(i, j);
    }

    public void visitJumpInsn(int i, Label label)
    {
        if ((label.status & 2) != 0 && label.position - code.length < -32768)
        {
            throw new UnsupportedOperationException();
        } else
        {
            code.putByte(i);
            label.put(this, code, code.length - 1);
            return;
        }
    }

    public void visitLabel(Label label)
    {
        label.resolve(this, code.length, code.data);
    }

    public void visitLdcInsn(Object obj)
    {
        obj = cw.newConstItem(obj);
        int i = ((Item) (obj)).index;
        if (((Item) (obj)).type == 5 || ((Item) (obj)).type == 6)
        {
            code.put12(20, i);
            return;
        }
        if (i >= 256)
        {
            code.put12(19, i);
            return;
        } else
        {
            code.put11(18, i);
            return;
        }
    }

    public void visitMaxs(int i, int j)
    {
        maxStack = i;
        maxLocals = j;
    }

    public void visitMethodInsn(int i, String s, String s1, String s2)
    {
        int j;
        boolean flag;
        if (i == 185)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        s = cw.newMethodItem(s, s1, s2, flag);
        j = ((Item) (s)).intVal;
        if (flag)
        {
            if (j == 0)
            {
                i = Type.getArgumentsAndReturnSizes(s2);
                s.intVal = i;
            } else
            {
                i = j;
            }
            code.put12(185, ((Item) (s)).index).put11(i >> 2, 0);
            return;
        } else
        {
            code.put12(i, ((Item) (s)).index);
            return;
        }
    }

    public void visitTypeInsn(int i, String s)
    {
        s = cw.newClassItem(s);
        code.put12(i, ((Item) (s)).index);
    }

    public void visitVarInsn(int i, int j)
    {
        if (j < 4 && i != 169)
        {
            if (i < 54)
            {
                i = (i - 21 << 2) + 26 + j;
            } else
            {
                i = (i - 54 << 2) + 59 + j;
            }
            code.putByte(i);
            return;
        }
        if (j >= 256)
        {
            code.putByte(196).put12(i, j);
            return;
        } else
        {
            code.put11(i, j);
            return;
        }
    }
}
