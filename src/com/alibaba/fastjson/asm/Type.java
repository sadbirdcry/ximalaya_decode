// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.asm;


public class Type
{

    public static final int ARRAY = 9;
    public static final int BOOLEAN = 1;
    public static final Type BOOLEAN_TYPE = new Type(1, null, 0x5a000501, 1);
    public static final int BYTE = 3;
    public static final Type BYTE_TYPE = new Type(3, null, 0x42000501, 1);
    public static final int CHAR = 2;
    public static final Type CHAR_TYPE = new Type(2, null, 0x43000601, 1);
    public static final int DOUBLE = 8;
    public static final Type DOUBLE_TYPE = new Type(8, null, 0x44030302, 1);
    public static final int FLOAT = 6;
    public static final Type FLOAT_TYPE = new Type(6, null, 0x46020201, 1);
    public static final int INT = 5;
    public static final Type INT_TYPE = new Type(5, null, 0x49000001, 1);
    public static final int LONG = 7;
    public static final Type LONG_TYPE = new Type(7, null, 0x4a010102, 1);
    public static final int OBJECT = 10;
    public static final int SHORT = 4;
    public static final Type SHORT_TYPE = new Type(4, null, 0x53000701, 1);
    public static final int VOID = 0;
    public static final Type VOID_TYPE = new Type(0, null, 0x56050000, 1);
    private final char buf[];
    private final int len;
    private final int off;
    private final int sort;

    private Type(int i, char ac[], int j, int k)
    {
        sort = i;
        buf = ac;
        off = j;
        len = k;
    }

    public static int getArgumentsAndReturnSizes(String s)
    {
        int i;
        int j;
        boolean flag;
        flag = true;
        j = 1;
        i = 1;
_L7:
        int k;
        k = j + 1;
        j = s.charAt(j);
        if (j != ')') goto _L2; else goto _L1
_L1:
        k = s.charAt(k);
        if (k != 86) goto _L4; else goto _L3
_L3:
        j = 0;
_L5:
        return i << 2 | j;
_L4:
        if (k != 68)
        {
            j = ((flag) ? 1 : 0);
            if (k != 74)
            {
                continue; /* Loop/switch isn't completed */
            }
        }
        j = 2;
        if (true) goto _L5; else goto _L2
_L2:
        if (j == 'L')
        {
            do
            {
                j = k + 1;
                if (s.charAt(k) == ';')
                {
                    break;
                }
                k = j;
            } while (true);
            i++;
        } else
        if (j == 68 || j == 74)
        {
            i += 2;
            j = k;
        } else
        {
            i++;
            j = k;
        }
        if (true) goto _L7; else goto _L6
_L6:
    }

    public static Type getType(String s)
    {
        return getType(s.toCharArray(), 0);
    }

    private static Type getType(char ac[], int i)
    {
        int j = 1;
        boolean flag = true;
        switch (ac[i])
        {
        default:
            for (j = ((flag) ? 1 : 0); ac[i + j] != ';'; j++) { }
            break;

        case 86: // 'V'
            return VOID_TYPE;

        case 90: // 'Z'
            return BOOLEAN_TYPE;

        case 67: // 'C'
            return CHAR_TYPE;

        case 66: // 'B'
            return BYTE_TYPE;

        case 83: // 'S'
            return SHORT_TYPE;

        case 73: // 'I'
            return INT_TYPE;

        case 70: // 'F'
            return FLOAT_TYPE;

        case 74: // 'J'
            return LONG_TYPE;

        case 68: // 'D'
            return DOUBLE_TYPE;

        case 91: // '['
            for (; ac[i + j] == '['; j++) { }
            int k = j;
            if (ac[i + j] == 'L')
            {
                j++;
                do
                {
                    k = j;
                    if (ac[i + j] == ';')
                    {
                        break;
                    }
                    j++;
                } while (true);
            }
            return new Type(9, ac, i, k + 1);
        }
        return new Type(10, ac, i + 1, j - 1);
    }

    String getDescriptor()
    {
        return new String(buf, off, len);
    }

    public String getInternalName()
    {
        return new String(buf, off, len);
    }

    public int getSort()
    {
        return sort;
    }

}
