// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.asm;


public class ByteVector
{

    byte data[];
    int length;

    public ByteVector()
    {
        data = new byte[64];
    }

    public ByteVector(int i)
    {
        data = new byte[i];
    }

    private void enlarge(int i)
    {
        int j = data.length * 2;
        i = length + i;
        if (j > i)
        {
            i = j;
        }
        byte abyte0[] = new byte[i];
        System.arraycopy(data, 0, abyte0, 0, length);
        data = abyte0;
    }

    ByteVector put11(int i, int j)
    {
        int k = length;
        if (k + 2 > data.length)
        {
            enlarge(2);
        }
        byte abyte0[] = data;
        int l = k + 1;
        abyte0[k] = (byte)i;
        abyte0[l] = (byte)j;
        length = l + 1;
        return this;
    }

    ByteVector put12(int i, int j)
    {
        int l = length;
        if (l + 3 > data.length)
        {
            enlarge(3);
        }
        byte abyte0[] = data;
        int k = l + 1;
        abyte0[l] = (byte)i;
        i = k + 1;
        abyte0[k] = (byte)(j >>> 8);
        abyte0[i] = (byte)j;
        length = i + 1;
        return this;
    }

    public ByteVector putByte(int i)
    {
        int j = length;
        if (j + 1 > data.length)
        {
            enlarge(1);
        }
        data[j] = (byte)i;
        length = j + 1;
        return this;
    }

    public ByteVector putByteArray(byte abyte0[], int i, int j)
    {
        if (length + j > data.length)
        {
            enlarge(j);
        }
        if (abyte0 != null)
        {
            System.arraycopy(abyte0, i, data, length, j);
        }
        length = length + j;
        return this;
    }

    public ByteVector putInt(int i)
    {
        int k = length;
        if (k + 4 > data.length)
        {
            enlarge(4);
        }
        byte abyte0[] = data;
        int j = k + 1;
        abyte0[k] = (byte)(i >>> 24);
        k = j + 1;
        abyte0[j] = (byte)(i >>> 16);
        j = k + 1;
        abyte0[k] = (byte)(i >>> 8);
        abyte0[j] = (byte)i;
        length = j + 1;
        return this;
    }

    public ByteVector putShort(int i)
    {
        int j = length;
        if (j + 2 > data.length)
        {
            enlarge(2);
        }
        byte abyte0[] = data;
        int k = j + 1;
        abyte0[j] = (byte)(i >>> 8);
        abyte0[k] = (byte)i;
        length = k + 1;
        return this;
    }

    public ByteVector putUTF8(String s)
    {
        int l = s.length();
        int i = length;
        if (i + 2 + l > data.length)
        {
            enlarge(l + 2);
        }
        byte abyte0[] = data;
        int j = i + 1;
        abyte0[i] = (byte)(l >>> 8);
        i = j + 1;
        abyte0[j] = (byte)l;
        for (int k = 0; k < l;)
        {
            char c = s.charAt(k);
            if (c >= '\001' && c <= '\177')
            {
                abyte0[i] = (byte)c;
                k++;
                i++;
            } else
            {
                throw new UnsupportedOperationException();
            }
        }

        length = i;
        return this;
    }
}
