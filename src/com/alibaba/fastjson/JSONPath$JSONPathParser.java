// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;

import com.alibaba.fastjson.util.IOUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.alibaba.fastjson:
//            JSONPath, JSONPathException

static class next
{

    private char ch;
    private int level;
    private final String path;
    private int pos;

    static boolean isDigitFirst(char c)
    {
        return c == '-' || c == '+' || c >= '0' && c <= '9';
    }

    void accept(char c)
    {
        if (ch != c)
        {
            throw new JSONPathException((new StringBuilder()).append("expect '").append(c).append(", but '").append(ch).append("'").toString());
        }
        if (!isEOF())
        {
            next();
        }
    }

    next buildArraySegement(String s)
    {
        int ai1[];
        int i;
        i = 0;
        int k = 0;
        int k1 = s.length();
        char c = s.charAt(0);
        char c1 = s.charAt(k1 - 1);
        int i1 = s.indexOf(',');
        if (s.length() > 2 && c == '\'' && c1 == '\'')
        {
            if (i1 == -1)
            {
                return new t(s.substring(1, k1 - 1));
            }
            s = s.split(",");
            String as[] = new String[s.length];
            for (i = k; i < s.length; i++)
            {
                String s1 = s[i];
                as[i] = s1.substring(1, s1.length() - 1);
            }

            return new gement(as);
        }
        k = s.indexOf(':');
        if (i1 == -1 && k == -1)
        {
            return new ment(Integer.parseInt(s));
        }
        if (i1 != -1)
        {
            s = s.split(",");
            int ai[] = new int[s.length];
            for (; i < s.length; i++)
            {
                ai[i] = Integer.parseInt(s[i]);
            }

            return new ent(ai);
        }
        if (k == -1)
        {
            break MISSING_BLOCK_LABEL_444;
        }
        s = s.split(":");
        ai1 = new int[s.length];
        i = 0;
_L2:
        String s2;
        if (i >= s.length)
        {
            break MISSING_BLOCK_LABEL_308;
        }
        s2 = s[i];
        if (!s2.isEmpty())
        {
            break MISSING_BLOCK_LABEL_297;
        }
        if (i != 0)
        {
            break; /* Loop/switch isn't completed */
        }
        ai1[i] = 0;
_L3:
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        throw new UnsupportedOperationException();
        ai1[i] = Integer.parseInt(s2);
          goto _L3
        int j1 = ai1[0];
        int j;
        int l;
        if (ai1.length > 1)
        {
            j = ai1[1];
        } else
        {
            j = -1;
        }
        if (ai1.length == 3)
        {
            l = ai1[2];
        } else
        {
            l = 1;
        }
        if (j >= 0 && j < j1)
        {
            throw new UnsupportedOperationException((new StringBuilder()).append("end must greater than or equals start. start ").append(j1).append(",  end ").append(j).toString());
        }
        if (l <= 0)
        {
            throw new UnsupportedOperationException((new StringBuilder()).append("step must greater than zero : ").append(l).toString());
        } else
        {
            return new init>(j1, j, l);
        }
        throw new UnsupportedOperationException();
    }

    public >[] explain()
    {
        if (path == null || path.isEmpty())
        {
            throw new IllegalArgumentException();
        }
        > a>[] = new path[8];
        do
        {
            > > = readSegement();
            int i;
            if (> == null)
            {
                if (level == a>.length)
                {
                    return a>;
                } else
                {
                    > a>1[] = new level[level];
                    System.arraycopy(a>, 0, a>1, 0, level);
                    return a>1;
                }
            }
            i = level;
            level = i + 1;
            a>[i] = >;
        } while (true);
    }

    boolean isEOF()
    {
        return pos >= path.length();
    }

    void next()
    {
        String s = path;
        int i = pos;
        pos = i + 1;
        ch = s.charAt(i);
    }

    ch parseArrayAccess(boolean flag)
    {
        boolean flag4 = true;
        if (flag)
        {
            accept('[');
        }
        Object obj;
        Object obj1;
        Object obj2;
        Object obj3;
        String s;
        int i;
        boolean flag1;
        int j;
        boolean flag2;
        int k;
        boolean flag3;
        long l;
        if (ch == '?')
        {
            next();
            accept('(');
            if (ch == '@')
            {
                next();
                accept('.');
            }
            i = 1;
        } else
        {
            i = 0;
        }
        if (i == 0 && !IOUtils.firstIdentifier(ch)) goto _L2; else goto _L1
_L1:
        s = readName();
        skipWhitespace();
        if (i == 0 || ch != ')') goto _L4; else goto _L3
_L3:
        next();
        if (flag)
        {
            accept(']');
        }
        obj = new <init>(new (s));
_L9:
        return (() (obj));
_L4:
        if (flag && ch == ']')
        {
            next();
            return new <init>(new (s));
        }
        obj1 = readOp();
        skipWhitespace();
        if (obj1 == N || obj1 == TWEEN)
        {
            if (obj1 == TWEEN)
            {
                flag = flag4;
            } else
            {
                flag = false;
            }
            obj = readValue();
            if (!"and".equalsIgnoreCase(readName()))
            {
                throw new JSONPathException(path);
            }
            obj1 = readValue();
            if (obj == null || obj1 == null)
            {
                throw new JSONPathException(path);
            }
            if (JSONPath.isInt(obj.getClass()) && JSONPath.isInt(obj1.getClass()))
            {
                return new <init>(new ent(s, ((Number)obj).longValue(), ((Number)obj1).longValue(), flag));
            } else
            {
                throw new JSONPathException(path);
            }
        }
        if (obj1 == path || obj1 == )
        {
            if (obj1 == )
            {
                flag4 = true;
            } else
            {
                flag4 = false;
            }
            accept('(');
            obj = new ArrayList();
            ((List) (obj)).add(readValue());
            do
            {
                skipWhitespace();
                if (ch != ',')
                {
                    accept(')');
                    if (i != 0)
                    {
                        accept(')');
                    }
                    if (flag)
                    {
                        accept(']');
                    }
                    obj1 = ((List) (obj)).iterator();
                    flag1 = true;
                    flag2 = true;
                    i = 1;
                    while (((Iterator) (obj1)).hasNext()) 
                    {
                        obj2 = ((Iterator) (obj1)).next();
                        if (obj2 == null)
                        {
                            if (i != 0)
                            {
                                i = 0;
                            }
                        } else
                        {
                            obj2 = obj2.getClass();
                            flag3 = flag2;
                            k = i;
                            if (i != 0)
                            {
                                flag3 = flag2;
                                k = i;
                                if (obj2 != java/lang/Byte)
                                {
                                    flag3 = flag2;
                                    k = i;
                                    if (obj2 != java/lang/Short)
                                    {
                                        flag3 = flag2;
                                        k = i;
                                        if (obj2 != java/lang/Integer)
                                        {
                                            flag3 = flag2;
                                            k = i;
                                            if (obj2 != java/lang/Long)
                                            {
                                                flag3 = false;
                                                k = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            flag2 = flag3;
                            i = k;
                            if (flag1)
                            {
                                flag2 = flag3;
                                i = k;
                                if (obj2 != java/lang/String)
                                {
                                    flag1 = false;
                                    flag2 = flag3;
                                    i = k;
                                }
                            }
                        }
                    }
                    break;
                }
                next();
                ((List) (obj)).add(readValue());
            } while (true);
            if (((List) (obj)).size() == 1 && ((List) (obj)).get(0) == null)
            {
                if (flag4)
                {
                    return new <init>(new (s));
                } else
                {
                    return new <init>(new nit>(s));
                }
            }
            if (i != 0)
            {
                if (((List) (obj)).size() == 1)
                {
                    l = ((Number)((List) (obj)).get(0)).longValue();
                    if (flag4)
                    {
                        obj = nit>;
                    } else
                    {
                        obj = nit>;
                    }
                    return new <init>(new init>(s, l, ((init>) (obj))));
                }
                obj1 = new long[((List) (obj)).size()];
                for (i = 0; i < obj1.length; i++)
                {
                    obj1[i] = ((Number)((List) (obj)).get(i)).longValue();
                }

                return new <init>(new init>(s, ((long []) (obj1)), flag4));
            }
            if (flag1)
            {
                if (((List) (obj)).size() == 1)
                {
                    obj1 = (String)((List) (obj)).get(0);
                    if (flag4)
                    {
                        obj = init>;
                    } else
                    {
                        obj = init>;
                    }
                    return new <init>(new t(s, ((String) (obj1)), ((t) (obj))));
                } else
                {
                    obj1 = new String[((List) (obj)).size()];
                    ((List) (obj)).toArray(((Object []) (obj1)));
                    return new <init>(new t(s, ((String []) (obj1)), flag4));
                }
            }
            if (flag2)
            {
                obj1 = new Long[((List) (obj)).size()];
                for (i = 0; i < obj1.length; i++)
                {
                    obj2 = (Number)((List) (obj)).get(i);
                    if (obj2 != null)
                    {
                        obj1[i] = Long.valueOf(((Number) (obj2)).longValue());
                    }
                }

                return new <init>(new t(s, ((Long []) (obj1)), flag4));
            } else
            {
                throw new UnsupportedOperationException();
            }
        }
        if (ch != '\'' && ch != '"') goto _L6; else goto _L5
_L5:
        obj2 = readString();
        if (i != 0)
        {
            accept(')');
        }
        if (flag)
        {
            accept(']');
        }
        if (obj1 == accept)
        {
            return new <init>(new init>(s, ((String) (obj2)), false));
        }
        if (obj1 == IKE)
        {
            return new <init>(new init>(s, ((String) (obj2)), true));
        }
        obj = obj2;
        if (obj1 != init>)
        {
            if (obj1 != KE)
            {
                break MISSING_BLOCK_LABEL_1696;
            }
        }
        for (obj = obj2; ((String) (obj)).indexOf("%%") != -1; obj = ((String) (obj)).replaceAll("%%", "%")) { }
        if (obj1 == KE)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        i = ((String) (obj)).indexOf('%');
        if (i != -1) goto _L8; else goto _L7
_L7:
        if (obj1 == KE)
        {
            obj1 = KE;
        } else
        {
            obj1 = KE;
        }
_L10:
        return new <init>(new t(s, ((String) (obj)), ((t) (obj1))));
_L8:
        obj1 = ((String) (obj)).split("%");
        obj2 = null;
        if (i == 0)
        {
            if (((String) (obj)).charAt(((String) (obj)).length() - 1) == '%')
            {
                obj2 = new String[obj1.length - 1];
                System.arraycopy(obj1, 1, obj2, 0, obj2.length);
                obj1 = null;
                obj = null;
            } else
            {
                obj = obj1[obj1.length - 1];
                if (obj1.length > 2)
                {
                    obj2 = new String[obj1.length - 2];
                    System.arraycopy(obj1, 1, obj2, 0, obj2.length);
                    obj1 = obj;
                    obj = null;
                } else
                {
                    obj1 = obj;
                    obj = null;
                }
            }
        } else
        if (((String) (obj)).charAt(((String) (obj)).length() - 1) == '%')
        {
            obj2 = obj1;
            obj = null;
            obj1 = null;
        } else
        if (obj1.length == 1)
        {
            obj = obj1[0];
            obj1 = null;
        } else
        if (obj1.length == 2)
        {
            obj = obj1[0];
            obj1 = obj1[1];
        } else
        {
            obj = obj1[0];
            obj3 = obj1[obj1.length - 1];
            obj2 = new String[obj1.length - 2];
            System.arraycopy(obj1, 1, obj2, 0, obj2.length);
            obj1 = obj3;
        }
        return new <init>(new init>(s, ((String) (obj)), ((String) (obj1)), ((String []) (obj2)), flag));
_L6:
        if (isDigitFirst(ch))
        {
            l = readLongValue();
            if (i != 0)
            {
                accept(')');
            }
            if (flag)
            {
                accept(']');
            }
            return new <init>(new init>(s, l, ((init>) (obj1))));
        }
        if (ch == 'n' && "null".equals(readName()))
        {
            if (i != 0)
            {
                accept(')');
            }
            accept(']');
            if (obj1 == accept)
            {
                return new <init>(new nit>(s));
            }
            if (obj1 == nit>)
            {
                return new <init>(new (s));
            } else
            {
                throw new UnsupportedOperationException();
            }
        } else
        {
            throw new UnsupportedOperationException();
        }
_L2:
        j = pos;
        for (; ch != ']' && ch != '/' && !isEOF(); next()) { }
        if (flag)
        {
            i = pos - 1;
        } else
        if (ch == '/')
        {
            i = pos - 1;
        } else
        {
            i = pos;
        }
        obj1 = buildArraySegement(path.substring(j - 1, i));
        obj = obj1;
        if (flag)
        {
            obj = obj1;
            if (!isEOF())
            {
                accept(']');
                return ((accept) (obj1));
            }
        }
          goto _L9
        obj = obj2;
          goto _L10
    }

    protected long readLongValue()
    {
        int i = pos;
        if (ch == '+' || ch == '-')
        {
            next();
        }
        while (ch >= '0' && ch <= '9') 
        {
            next();
        }
        int j = pos;
        return Long.parseLong(path.substring(i - 1, j - 1));
    }

    String readName()
    {
        skipWhitespace();
        if (!IOUtils.firstIdentifier(ch))
        {
            throw new JSONPathException((new StringBuilder()).append("illeal jsonpath syntax. ").append(path).toString());
        }
        StringBuffer stringbuffer = new StringBuffer();
        do
        {
label0:
            {
                if (!isEOF())
                {
                    if (ch == '\\')
                    {
                        next();
                        stringbuffer.append(ch);
                        next();
                        continue;
                    }
                    if (IOUtils.isIdent(ch))
                    {
                        break label0;
                    }
                }
                if (isEOF() && IOUtils.isIdent(ch))
                {
                    stringbuffer.append(ch);
                }
                return stringbuffer.toString();
            }
            stringbuffer.append(ch);
            next();
        } while (true);
    }

    protected next readOp()
    {
        Object obj;
label0:
        {
label1:
            {
                obj = null;
                next next1;
                if (ch == '=')
                {
                    next();
                    obj = next;
                } else
                if (ch == '!')
                {
                    next();
                    accept('=');
                    obj = accept;
                } else
                if (ch == '<')
                {
                    next();
                    if (ch == '=')
                    {
                        next();
                        obj = next;
                    } else
                    {
                        obj = next;
                    }
                } else
                if (ch == '>')
                {
                    next();
                    if (ch == '=')
                    {
                        next();
                        obj = next;
                    } else
                    {
                        obj = next;
                    }
                }
                next1 = ((next) (obj));
                if (obj == null)
                {
                    obj = readName();
                    if (!"not".equalsIgnoreCase(((String) (obj))))
                    {
                        break label0;
                    }
                    skipWhitespace();
                    obj = readName();
                    if (!"like".equalsIgnoreCase(((String) (obj))))
                    {
                        break label1;
                    }
                    next1 = KE;
                }
                return next1;
            }
            if ("rlike".equalsIgnoreCase(((String) (obj))))
            {
                return IKE;
            }
            if ("in".equalsIgnoreCase(((String) (obj))))
            {
                return ;
            }
            if ("between".equalsIgnoreCase(((String) (obj))))
            {
                return TWEEN;
            } else
            {
                throw new UnsupportedOperationException();
            }
        }
        if ("like".equalsIgnoreCase(((String) (obj))))
        {
            return >;
        }
        if ("rlike".equalsIgnoreCase(((String) (obj))))
        {
            return >;
        }
        if ("in".equalsIgnoreCase(((String) (obj))))
        {
            return >;
        }
        if ("between".equalsIgnoreCase(((String) (obj))))
        {
            return N;
        } else
        {
            throw new UnsupportedOperationException();
        }
    }

    > readSegement()
    {
        if (level == 0 && path.length() == 1)
        {
            if (isDigitFirst(ch))
            {
                return new ment(ch - 48);
            }
            if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z')
            {
                return new t(Character.toString(ch));
            }
        }
          goto _L1
_L4:
        if (ch != '$') goto _L3; else goto _L2
_L2:
        next();
_L1:
        if (!isEOF())
        {
            skipWhitespace();
            if (ch == '@')
            {
                next();
                return stance;
            }
        } else
        {
            return null;
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (ch == '.' || ch == '/')
        {
            next();
            if (ch == '*')
            {
                if (!isEOF())
                {
                    next();
                }
                return t.instance;
            }
            if (isDigitFirst(ch))
            {
                return parseArrayAccess(false);
            }
            String s = readName();
            if (ch == '(')
            {
                next();
                if (ch == ')')
                {
                    if (!isEOF())
                    {
                        next();
                    }
                    if ("size".equals(s))
                    {
                        return stance;
                    } else
                    {
                        throw new UnsupportedOperationException();
                    }
                } else
                {
                    throw new UnsupportedOperationException();
                }
            } else
            {
                return new t(s);
            }
        }
        if (ch == '[')
        {
            return parseArrayAccess(true);
        }
        if (level == 0)
        {
            return new t(readName());
        } else
        {
            throw new UnsupportedOperationException();
        }
    }

    String readString()
    {
        char c = ch;
        next();
        int j = pos;
        for (; ch != c && !isEOF(); next()) { }
        String s = path;
        int i;
        if (isEOF())
        {
            i = pos;
        } else
        {
            i = pos - 1;
        }
        s = s.substring(j - 1, i);
        accept(c);
        return s;
    }

    protected Object readValue()
    {
        skipWhitespace();
        if (isDigitFirst(ch))
        {
            return Long.valueOf(readLongValue());
        }
        if (ch == '"' || ch == '\'')
        {
            return readString();
        }
        if (ch == 'n')
        {
            if ("null".equals(readName()))
            {
                return null;
            } else
            {
                throw new JSONPathException(path);
            }
        } else
        {
            throw new UnsupportedOperationException();
        }
    }

    public final void skipWhitespace()
    {
        while (ch < IOUtils.whitespaceFlags.length && IOUtils.whitespaceFlags[ch]) 
        {
            next();
        }
    }

    public t(String s)
    {
        path = s;
        next();
    }
}
