// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;

import com.alibaba.fastjson.util.TypeUtils;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

// Referenced classes of package com.alibaba.fastjson:
//            JSON, JSONObject

public class JSONArray extends JSON
    implements Serializable, Cloneable, List, RandomAccess
{

    private static final long serialVersionUID = 1L;
    protected transient Type componentType;
    private final List list;
    protected transient Object relatedArray;

    public JSONArray()
    {
        list = new ArrayList(10);
    }

    public JSONArray(int i)
    {
        list = new ArrayList(i);
    }

    public JSONArray(List list1)
    {
        list = list1;
    }

    public void add(int i, Object obj)
    {
        list.add(i, obj);
    }

    public boolean add(Object obj)
    {
        return list.add(obj);
    }

    public boolean addAll(int i, Collection collection)
    {
        return list.addAll(i, collection);
    }

    public boolean addAll(Collection collection)
    {
        return list.addAll(collection);
    }

    public void clear()
    {
        list.clear();
    }

    public Object clone()
    {
        return new JSONArray(new ArrayList(list));
    }

    public boolean contains(Object obj)
    {
        return list.contains(obj);
    }

    public boolean containsAll(Collection collection)
    {
        return list.containsAll(collection);
    }

    public boolean equals(Object obj)
    {
        return list.equals(obj);
    }

    public Object get(int i)
    {
        return list.get(i);
    }

    public BigDecimal getBigDecimal(int i)
    {
        return TypeUtils.castToBigDecimal(get(i));
    }

    public BigInteger getBigInteger(int i)
    {
        return TypeUtils.castToBigInteger(get(i));
    }

    public Boolean getBoolean(int i)
    {
        Object obj = get(i);
        if (obj == null)
        {
            return null;
        } else
        {
            return TypeUtils.castToBoolean(obj);
        }
    }

    public boolean getBooleanValue(int i)
    {
        Object obj = get(i);
        if (obj == null)
        {
            return false;
        } else
        {
            return TypeUtils.castToBoolean(obj).booleanValue();
        }
    }

    public Byte getByte(int i)
    {
        return TypeUtils.castToByte(get(i));
    }

    public byte getByteValue(int i)
    {
        Object obj = get(i);
        if (obj == null)
        {
            return 0;
        } else
        {
            return TypeUtils.castToByte(obj).byteValue();
        }
    }

    public Type getComponentType()
    {
        return componentType;
    }

    public Date getDate(int i)
    {
        return TypeUtils.castToDate(get(i));
    }

    public Double getDouble(int i)
    {
        return TypeUtils.castToDouble(get(i));
    }

    public double getDoubleValue(int i)
    {
        Object obj = get(i);
        if (obj == null)
        {
            return 0.0D;
        } else
        {
            return TypeUtils.castToDouble(obj).doubleValue();
        }
    }

    public Float getFloat(int i)
    {
        return TypeUtils.castToFloat(get(i));
    }

    public float getFloatValue(int i)
    {
        Object obj = get(i);
        if (obj == null)
        {
            return 0.0F;
        } else
        {
            return TypeUtils.castToFloat(obj).floatValue();
        }
    }

    public int getIntValue(int i)
    {
        Object obj = get(i);
        if (obj == null)
        {
            return 0;
        } else
        {
            return TypeUtils.castToInt(obj).intValue();
        }
    }

    public Integer getInteger(int i)
    {
        return TypeUtils.castToInt(get(i));
    }

    public JSONArray getJSONArray(int i)
    {
        Object obj = list.get(i);
        if (obj instanceof JSONArray)
        {
            return (JSONArray)obj;
        } else
        {
            return (JSONArray)toJSON(obj);
        }
    }

    public JSONObject getJSONObject(int i)
    {
        Object obj = list.get(i);
        if (obj instanceof JSONObject)
        {
            return (JSONObject)obj;
        } else
        {
            return (JSONObject)toJSON(obj);
        }
    }

    public Long getLong(int i)
    {
        return TypeUtils.castToLong(get(i));
    }

    public long getLongValue(int i)
    {
        Object obj = get(i);
        if (obj == null)
        {
            return 0L;
        } else
        {
            return TypeUtils.castToLong(obj).longValue();
        }
    }

    public Object getObject(int i, Class class1)
    {
        return TypeUtils.castToJavaBean(list.get(i), class1);
    }

    public Object getRelatedArray()
    {
        return relatedArray;
    }

    public Short getShort(int i)
    {
        return TypeUtils.castToShort(get(i));
    }

    public short getShortValue(int i)
    {
        Object obj = get(i);
        if (obj == null)
        {
            return 0;
        } else
        {
            return TypeUtils.castToShort(obj).shortValue();
        }
    }

    public java.sql.Date getSqlDate(int i)
    {
        return TypeUtils.castToSqlDate(get(i));
    }

    public String getString(int i)
    {
        return TypeUtils.castToString(get(i));
    }

    public Timestamp getTimestamp(int i)
    {
        return TypeUtils.castToTimestamp(get(i));
    }

    public int hashCode()
    {
        return list.hashCode();
    }

    public int indexOf(Object obj)
    {
        return list.indexOf(obj);
    }

    public boolean isEmpty()
    {
        return list.isEmpty();
    }

    public Iterator iterator()
    {
        return list.iterator();
    }

    public int lastIndexOf(Object obj)
    {
        return list.lastIndexOf(obj);
    }

    public ListIterator listIterator()
    {
        return list.listIterator();
    }

    public ListIterator listIterator(int i)
    {
        return list.listIterator(i);
    }

    public Object remove(int i)
    {
        return list.remove(i);
    }

    public boolean remove(Object obj)
    {
        return list.remove(obj);
    }

    public boolean removeAll(Collection collection)
    {
        return list.removeAll(collection);
    }

    public boolean retainAll(Collection collection)
    {
        return list.retainAll(collection);
    }

    public Object set(int i, Object obj)
    {
        if (i == -1)
        {
            list.add(obj);
            return null;
        }
        if (list.size() <= i)
        {
            for (int j = list.size(); j < i; j++)
            {
                list.add(null);
            }

            list.add(obj);
            return null;
        } else
        {
            return list.set(i, obj);
        }
    }

    public void setComponentType(Type type)
    {
        componentType = type;
    }

    public void setRelatedArray(Object obj)
    {
        relatedArray = obj;
    }

    public int size()
    {
        return list.size();
    }

    public List subList(int i, int j)
    {
        return list.subList(i, j);
    }

    public Object[] toArray()
    {
        return list.toArray();
    }

    public Object[] toArray(Object aobj[])
    {
        return list.toArray(aobj);
    }
}
