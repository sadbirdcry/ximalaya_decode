// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;


// Referenced classes of package com.alibaba.fastjson:
//            JSONPath

static class not
    implements not
{

    private final boolean not;
    private final String propertyName;
    private final long values[];

    public boolean apply(JSONPath jsonpath, Object obj, Object obj1, Object obj2)
    {
        jsonpath = ((JSONPath) (jsonpath.getPropertyValue(obj2, propertyName, false)));
        if (jsonpath == null)
        {
            return false;
        }
        if (jsonpath instanceof Number)
        {
            long l = ((Number)jsonpath).longValue();
            jsonpath = values;
            int j = jsonpath.length;
            for (int i = 0; i < j; i++)
            {
                if (jsonpath[i] == l)
                {
                    boolean flag;
                    if (!not)
                    {
                        flag = true;
                    } else
                    {
                        flag = false;
                    }
                    return flag;
                }
            }

        }
        return not;
    }

    public (String s, long al[], boolean flag)
    {
        propertyName = s;
        values = al;
        not = flag;
    }
}
