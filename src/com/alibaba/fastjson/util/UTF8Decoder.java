// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.util;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;

public class UTF8Decoder extends CharsetDecoder
{
    private static class Surrogate
    {

        static final boolean $assertionsDisabled;
        public static final int UCS4_MAX = 0x10ffff;
        public static final int UCS4_MIN = 0x10000;

        public static char high(int i)
        {
            if (!$assertionsDisabled && !neededFor(i))
            {
                throw new AssertionError();
            } else
            {
                return (char)(0xd800 | i - 0x10000 >> 10 & 0x3ff);
            }
        }

        public static char low(int i)
        {
            if (!$assertionsDisabled && !neededFor(i))
            {
                throw new AssertionError();
            } else
            {
                return (char)(0xdc00 | i - 0x10000 & 0x3ff);
            }
        }

        public static boolean neededFor(int i)
        {
            return i >= 0x10000 && i <= 0x10ffff;
        }

        static 
        {
            boolean flag;
            if (!com/alibaba/fastjson/util/UTF8Decoder.desiredAssertionStatus())
            {
                flag = true;
            } else
            {
                flag = false;
            }
            $assertionsDisabled = flag;
        }

        private Surrogate()
        {
        }
    }


    private static final Charset charset = Charset.forName("UTF-8");

    public UTF8Decoder()
    {
        super(charset, 1.0F, 1.0F);
    }

    private CoderResult decodeArrayLoop(ByteBuffer bytebuffer, CharBuffer charbuffer)
    {
        byte abyte0[];
        char ac[];
        int k;
        int l;
        int i2;
        int j2;
        abyte0 = bytebuffer.array();
        int i = bytebuffer.arrayOffset();
        i = bytebuffer.position() + i;
        k = bytebuffer.arrayOffset();
        i2 = bytebuffer.limit() + k;
        ac = charbuffer.array();
        int k1 = charbuffer.arrayOffset() + charbuffer.position();
        j2 = charbuffer.arrayOffset() + charbuffer.limit();
        int k2 = Math.min(i2 - i, j2 - k1);
        k = k1;
        do
        {
            int i1 = k;
            k = i;
            l = i1;
            if (i1 >= k1 + k2)
            {
                break;
            }
            k = i;
            l = i1;
            if (abyte0[i] < 0)
            {
                break;
            }
            ac[i1] = (char)abyte0[i];
            k = i1 + 1;
            i++;
        } while (true);
          goto _L1
_L5:
        int j;
        int j1;
        j = l + 1;
        ac[l] = (char)j1;
        k++;
_L3:
        l = j;
_L1:
        if (k >= i2)
        {
            break; /* Loop/switch isn't completed */
        }
        j1 = abyte0[k];
        if (j1 >= 0)
        {
            if (l >= j2)
            {
                return xflow(bytebuffer, k, i2, charbuffer, l, 1);
            }
            continue; /* Loop/switch isn't completed */
        }
        if (j1 >> 5 == -2)
        {
            if (i2 - k < 2 || l >= j2)
            {
                return xflow(bytebuffer, k, i2, charbuffer, l, 2);
            }
            byte byte0 = abyte0[k + 1];
            if (isMalformed2(j1, byte0))
            {
                return malformed(bytebuffer, k, charbuffer, l, 2);
            }
            j = l + 1;
            ac[l] = (char)(j1 << 6 ^ byte0 ^ 0xf80);
            k += 2;
        } else
        if (j1 >> 4 == -2)
        {
            if (i2 - k < 3 || l >= j2)
            {
                return xflow(bytebuffer, k, i2, charbuffer, l, 3);
            }
            byte byte1 = abyte0[k + 1];
            byte byte2 = abyte0[k + 2];
            if (isMalformed3(j1, byte1, byte2))
            {
                return malformed(bytebuffer, k, charbuffer, l, 3);
            }
            j = l + 1;
            ac[l] = (char)(j1 << 12 ^ byte1 << 6 ^ byte2 ^ 0x1f80);
            k += 3;
        } else
        if (j1 >> 3 == -2)
        {
            if (i2 - k < 4 || j2 - l < 2)
            {
                return xflow(bytebuffer, k, i2, charbuffer, l, 4);
            }
            j = abyte0[k + 1];
            int l1 = abyte0[k + 2];
            byte byte3 = abyte0[k + 3];
            j1 = (j1 & 7) << 18 | (j & 0x3f) << 12 | (l1 & 0x3f) << 6 | byte3 & 0x3f;
            if (isMalformed4(j, l1, byte3) || !Surrogate.neededFor(j1))
            {
                return malformed(bytebuffer, k, charbuffer, l, 4);
            }
            l1 = l + 1;
            ac[l] = Surrogate.high(j1);
            j = l1 + 1;
            ac[l1] = Surrogate.low(j1);
            k += 4;
        } else
        {
            return malformed(bytebuffer, k, charbuffer, l, 1);
        }
        if (true) goto _L3; else goto _L2
_L2:
        return xflow(bytebuffer, k, i2, charbuffer, l, 0);
        if (true) goto _L5; else goto _L4
_L4:
    }

    private static boolean isMalformed2(int i, int j)
    {
        return (i & 0x1e) == 0 || (j & 0xc0) != 128;
    }

    private static boolean isMalformed3(int i, int j, int k)
    {
        return i == -32 && (j & 0xe0) == 128 || (j & 0xc0) != 128 || (k & 0xc0) != 128;
    }

    private static boolean isMalformed4(int i, int j, int k)
    {
        return (i & 0xc0) != 128 || (j & 0xc0) != 128 || (k & 0xc0) != 128;
    }

    private static boolean isNotContinuation(int i)
    {
        return (i & 0xc0) != 128;
    }

    private static CoderResult lookupN(ByteBuffer bytebuffer, int i)
    {
        for (int j = 1; j < i; j++)
        {
            if (isNotContinuation(bytebuffer.get()))
            {
                return CoderResult.malformedForLength(j);
            }
        }

        return CoderResult.malformedForLength(i);
    }

    private static CoderResult malformed(ByteBuffer bytebuffer, int i, CharBuffer charbuffer, int j, int k)
    {
        bytebuffer.position(i - bytebuffer.arrayOffset());
        CoderResult coderresult = malformedN(bytebuffer, k);
        updatePositions(bytebuffer, i, charbuffer, j);
        return coderresult;
    }

    public static CoderResult malformedN(ByteBuffer bytebuffer, int i)
    {
        byte byte0 = 2;
        i;
        JVM INSTR tableswitch 1 4: default 32
    //                   1 40
    //                   2 103
    //                   3 108
    //                   4 151;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        throw new IllegalStateException();
_L2:
        i = bytebuffer.get();
        if (i >> 2 == -2)
        {
            if (bytebuffer.remaining() < 4)
            {
                return CoderResult.UNDERFLOW;
            } else
            {
                return lookupN(bytebuffer, 5);
            }
        }
        if (i >> 1 == -2)
        {
            if (bytebuffer.remaining() < 5)
            {
                return CoderResult.UNDERFLOW;
            } else
            {
                return lookupN(bytebuffer, 6);
            }
        } else
        {
            return CoderResult.malformedForLength(1);
        }
_L3:
        return CoderResult.malformedForLength(1);
_L4:
        byte byte1;
        i = bytebuffer.get();
        byte1 = bytebuffer.get();
        if (i == -32 && (byte1 & 0xe0) == 128) goto _L7; else goto _L6
_L6:
        i = byte0;
        if (!isNotContinuation(byte1)) goto _L8; else goto _L7
_L7:
        i = 1;
_L8:
        return CoderResult.malformedForLength(i);
_L5:
        i = bytebuffer.get() & 0xff;
        int j = bytebuffer.get() & 0xff;
        if (i > 244 || i == 240 && (j < 144 || j > 191) || i == 244 && (j & 0xf0) != 128 || isNotContinuation(j))
        {
            return CoderResult.malformedForLength(1);
        }
        if (isNotContinuation(bytebuffer.get()))
        {
            return CoderResult.malformedForLength(2);
        } else
        {
            return CoderResult.malformedForLength(3);
        }
    }

    static void updatePositions(Buffer buffer, int i, Buffer buffer1, int j)
    {
        buffer.position(i);
        buffer1.position(j);
    }

    private static CoderResult xflow(Buffer buffer, int i, int j, Buffer buffer1, int k, int l)
    {
        updatePositions(buffer, i, buffer1, k);
        if (l == 0 || j - i < l)
        {
            return CoderResult.UNDERFLOW;
        } else
        {
            return CoderResult.OVERFLOW;
        }
    }

    protected CoderResult decodeLoop(ByteBuffer bytebuffer, CharBuffer charbuffer)
    {
        return decodeArrayLoop(bytebuffer, charbuffer);
    }

}
