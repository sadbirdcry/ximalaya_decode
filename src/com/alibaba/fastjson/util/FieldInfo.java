// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.util;

import com.alibaba.fastjson.annotation.JSONField;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

// Referenced classes of package com.alibaba.fastjson.util:
//            TypeUtils, ParameterizedTypeImpl

public class FieldInfo
    implements Comparable
{

    private final Class declaringClass;
    private final Field field;
    private final Class fieldClass;
    private final Type fieldType;
    private boolean getOnly;
    private String label;
    private final Method method;
    private final String name;
    private int ordinal;
    private int serialzeFeatures;

    public FieldInfo(String s, Class class1, Class class2, Type type, Field field1)
    {
        this(s, class1, class2, type, field1, 0, 0);
    }

    public FieldInfo(String s, Class class1, Class class2, Type type, Field field1, int i, int j)
    {
        ordinal = 0;
        getOnly = false;
        label = "";
        name = s;
        declaringClass = class1;
        fieldClass = class2;
        fieldType = type;
        method = null;
        field = field1;
        ordinal = i;
        serialzeFeatures = j;
        if (field1 != null)
        {
            TypeUtils.setAccessible(field1);
        }
    }

    public FieldInfo(String s, Method method1, Field field1)
    {
        this(s, method1, field1, ((Class) (null)), ((Type) (null)));
    }

    public FieldInfo(String s, Method method1, Field field1, int i, int j)
    {
        this(s, method1, field1, i, j, null);
    }

    public FieldInfo(String s, Method method1, Field field1, int i, int j, String s1)
    {
        this(s, method1, field1, ((Class) (null)), ((Type) (null)), i, j);
        if (s1 != null && s1.length() > 0)
        {
            label = s1;
        }
    }

    public FieldInfo(String s, Method method1, Field field1, Class class1, Type type)
    {
        this(s, method1, field1, class1, type, 0, 0);
    }

    public FieldInfo(String s, Method method1, Field field1, Class class1, Type type, int i, int j)
    {
        ordinal = 0;
        getOnly = false;
        label = "";
        name = s;
        method = method1;
        field = field1;
        ordinal = i;
        serialzeFeatures = j;
        if (method1 != null)
        {
            TypeUtils.setAccessible(method1);
        }
        if (field1 != null)
        {
            TypeUtils.setAccessible(field1);
        }
        if (method1 != null)
        {
            if (method1.getParameterTypes().length == 1)
            {
                field1 = method1.getParameterTypes()[0];
                s = method1.getGenericParameterTypes()[0];
            } else
            {
                field1 = method1.getReturnType();
                s = method1.getGenericReturnType();
                getOnly = true;
            }
            declaringClass = method1.getDeclaringClass();
            method1 = field1;
        } else
        {
            method1 = field1.getType();
            s = field1.getGenericType();
            declaringClass = field1.getDeclaringClass();
        }
        if (class1 != null && method1 == java/lang/Object && (s instanceof TypeVariable))
        {
            field1 = getInheritGenericType(class1, (TypeVariable)s);
            if (field1 != null)
            {
                fieldClass = TypeUtils.getClass(field1);
                fieldType = field1;
                return;
            }
        }
        field1 = getFieldType(class1, type, s);
        if (field1 == s) goto _L2; else goto _L1
_L1:
        if (!(field1 instanceof ParameterizedType)) goto _L4; else goto _L3
_L3:
        s = TypeUtils.getClass(field1);
_L6:
        fieldType = field1;
        fieldClass = s;
        return;
_L4:
        if (field1 instanceof Class)
        {
            s = TypeUtils.getClass(field1);
            continue; /* Loop/switch isn't completed */
        }
_L2:
        s = method1;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public static Type getFieldType(Class class1, Type type, Type type1)
    {
        if (class1 != null && type != null) goto _L2; else goto _L1
_L1:
        return type1;
_L2:
        if (!(type1 instanceof GenericArrayType))
        {
            continue; /* Loop/switch isn't completed */
        }
        Type type2 = ((GenericArrayType)type1).getGenericComponentType();
        class1 = getFieldType(class1, type, type2);
        if (type2 != class1)
        {
            return Array.newInstance(TypeUtils.getClass(class1), 0).getClass();
        }
        continue; /* Loop/switch isn't completed */
        if (!TypeUtils.isGenericParamType(type)) goto _L1; else goto _L3
_L3:
        if (type1 instanceof TypeVariable)
        {
            ParameterizedType parameterizedtype = (ParameterizedType)TypeUtils.getGenericParamType(type);
            Class class2 = TypeUtils.getClass(parameterizedtype);
            TypeVariable typevariable = (TypeVariable)type1;
            for (int i = 0; i < class2.getTypeParameters().length; i++)
            {
                if (class2.getTypeParameters()[i].getName().equals(typevariable.getName()))
                {
                    return parameterizedtype.getActualTypeArguments()[i];
                }
            }

        }
        if (type1 instanceof ParameterizedType)
        {
            ParameterizedType parameterizedtype1 = (ParameterizedType)type1;
            Type atype[] = parameterizedtype1.getActualTypeArguments();
            int j = 0;
            boolean flag = false;
            for (; j < atype.length; j++)
            {
                Object obj = atype[j];
                if (!(obj instanceof TypeVariable))
                {
                    continue;
                }
                obj = (TypeVariable)obj;
                if (!(type instanceof ParameterizedType))
                {
                    continue;
                }
                ParameterizedType parameterizedtype2 = (ParameterizedType)type;
                for (int k = 0; k < class1.getTypeParameters().length; k++)
                {
                    if (class1.getTypeParameters()[k].getName().equals(((TypeVariable) (obj)).getName()))
                    {
                        atype[j] = parameterizedtype2.getActualTypeArguments()[k];
                        flag = true;
                    }
                }

            }

            if (flag)
            {
                return new ParameterizedTypeImpl(atype, parameterizedtype1.getOwnerType(), parameterizedtype1.getRawType());
            }
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    public static Type getInheritGenericType(Class class1, TypeVariable typevariable)
    {
        GenericDeclaration genericdeclaration = typevariable.getGenericDeclaration();
        Type type;
        do
        {
            type = class1.getGenericSuperclass();
            if (type == null)
            {
                return null;
            }
            if (type instanceof ParameterizedType)
            {
                class1 = (ParameterizedType)type;
                if (class1.getRawType() == genericdeclaration)
                {
                    TypeVariable atypevariable[] = genericdeclaration.getTypeParameters();
                    class1 = class1.getActualTypeArguments();
                    for (int i = 0; i < atypevariable.length; i++)
                    {
                        if (atypevariable[i] == typevariable)
                        {
                            return class1[i];
                        }
                    }

                    return null;
                }
            }
            class1 = TypeUtils.getClass(type);
        } while (type != null);
        return null;
    }

    public int compareTo(FieldInfo fieldinfo)
    {
        if (ordinal < fieldinfo.ordinal)
        {
            return -1;
        }
        if (ordinal > fieldinfo.ordinal)
        {
            return 1;
        } else
        {
            return name.compareTo(fieldinfo.name);
        }
    }

    public volatile int compareTo(Object obj)
    {
        return compareTo((FieldInfo)obj);
    }

    public String gerQualifiedName()
    {
        Member member = getMember();
        return (new StringBuilder()).append(member.getDeclaringClass().getName()).append(".").append(member.getName()).toString();
    }

    public Object get(Object obj)
        throws IllegalAccessException, InvocationTargetException
    {
        if (method != null)
        {
            return method.invoke(obj, new Object[0]);
        } else
        {
            return field.get(obj);
        }
    }

    public Annotation getAnnotation(Class class1)
    {
        Annotation annotation = null;
        if (method != null)
        {
            annotation = method.getAnnotation(class1);
        }
        Annotation annotation1 = annotation;
        if (annotation == null)
        {
            annotation1 = annotation;
            if (field != null)
            {
                annotation1 = field.getAnnotation(class1);
            }
        }
        return annotation1;
    }

    public Class getDeclaringClass()
    {
        return declaringClass;
    }

    public Field getField()
    {
        return field;
    }

    public Class getFieldClass()
    {
        return fieldClass;
    }

    public Type getFieldType()
    {
        return fieldType;
    }

    public String getFormat()
    {
        Object obj = (JSONField)getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        if (obj != null)
        {
            String s = ((JSONField) (obj)).format();
            obj = s;
            if (s.trim().length() == 0)
            {
                obj = null;
            }
            return ((String) (obj));
        } else
        {
            return null;
        }
    }

    public String getLabel()
    {
        return label;
    }

    public Member getMember()
    {
        if (method != null)
        {
            return method;
        } else
        {
            return field;
        }
    }

    public Method getMethod()
    {
        return method;
    }

    public String getName()
    {
        return name;
    }

    public int getSerialzeFeatures()
    {
        return serialzeFeatures;
    }

    public boolean isGetOnly()
    {
        return getOnly;
    }

    public void set(Object obj, Object obj1)
        throws IllegalAccessException, InvocationTargetException
    {
        if (method != null)
        {
            method.invoke(obj, new Object[] {
                obj1
            });
            return;
        } else
        {
            field.set(obj, obj1);
            return;
        }
    }

    public void setAccessible(boolean flag)
        throws SecurityException
    {
        if (method != null)
        {
            TypeUtils.setAccessible(method);
            return;
        } else
        {
            TypeUtils.setAccessible(field);
            return;
        }
    }

    public String toString()
    {
        return name;
    }
}
