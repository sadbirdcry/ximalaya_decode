// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.util;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Collection;

public class ASMUtils
{

    public static final String JAVA_VM_NAME = System.getProperty("java.vm.name");

    public ASMUtils()
    {
    }

    public static boolean checkName(String s)
    {
        for (int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);
            if (c < '\001' || c > '\177')
            {
                return false;
            }
        }

        return true;
    }

    public static String getDesc(Class class1)
    {
        if (class1.isPrimitive())
        {
            return getPrimitiveLetter(class1);
        }
        if (class1.isArray())
        {
            return (new StringBuilder()).append("[").append(getDesc(class1.getComponentType())).toString();
        } else
        {
            return (new StringBuilder()).append("L").append(getType(class1)).append(";").toString();
        }
    }

    public static String getDesc(Method method)
    {
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append("(");
        Class aclass[] = method.getParameterTypes();
        for (int i = 0; i < aclass.length; i++)
        {
            stringbuffer.append(getDesc(aclass[i]));
        }

        stringbuffer.append(")");
        stringbuffer.append(getDesc(method.getReturnType()));
        return stringbuffer.toString();
    }

    public static Type getFieldType(Class class1, String s)
    {
        try
        {
            class1 = class1.getField(s).getGenericType();
        }
        // Misplaced declaration of an exception variable
        catch (Class class1)
        {
            return null;
        }
        return class1;
    }

    public static Type getMethodType(Class class1, String s)
    {
        try
        {
            class1 = class1.getMethod(s, new Class[0]).getGenericReturnType();
        }
        // Misplaced declaration of an exception variable
        catch (Class class1)
        {
            return null;
        }
        return class1;
    }

    public static String getPrimitiveLetter(Class class1)
    {
        if (Integer.TYPE.equals(class1))
        {
            return "I";
        }
        if (Void.TYPE.equals(class1))
        {
            return "V";
        }
        if (Boolean.TYPE.equals(class1))
        {
            return "Z";
        }
        if (Character.TYPE.equals(class1))
        {
            return "C";
        }
        if (Byte.TYPE.equals(class1))
        {
            return "B";
        }
        if (Short.TYPE.equals(class1))
        {
            return "S";
        }
        if (Float.TYPE.equals(class1))
        {
            return "F";
        }
        if (Long.TYPE.equals(class1))
        {
            return "J";
        }
        if (Double.TYPE.equals(class1))
        {
            return "D";
        } else
        {
            throw new IllegalStateException((new StringBuilder()).append("Type: ").append(class1.getCanonicalName()).append(" is not a primitive type").toString());
        }
    }

    public static String getType(Class class1)
    {
        if (class1.isArray())
        {
            return (new StringBuilder()).append("[").append(getDesc(class1.getComponentType())).toString();
        }
        if (!class1.isPrimitive())
        {
            return class1.getName().replaceAll("\\.", "/");
        } else
        {
            return getPrimitiveLetter(class1);
        }
    }

    public static boolean isAndroid()
    {
        return isAndroid(JAVA_VM_NAME);
    }

    public static boolean isAndroid(String s)
    {
        if (s != null)
        {
            if ((s = s.toLowerCase()).contains("dalvik") || s.contains("lemur"))
            {
                return true;
            }
        }
        return false;
    }

    public static void parseArray(Collection collection, ObjectDeserializer objectdeserializer, DefaultJSONParser defaultjsonparser, Type type, Object obj)
    {
        obj = defaultjsonparser.getLexer();
        if (((JSONLexer) (obj)).token() == 8)
        {
            ((JSONLexer) (obj)).nextToken(16);
        }
        defaultjsonparser.accept(14, 14);
        int i = 0;
        do
        {
            collection.add(objectdeserializer.deserialze(defaultjsonparser, type, Integer.valueOf(i)));
            i++;
            if (((JSONLexer) (obj)).token() == 16)
            {
                ((JSONLexer) (obj)).nextToken(14);
            } else
            {
                defaultjsonparser.accept(15, 16);
                return;
            }
        } while (true);
    }

}
