// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.util;

import com.alibaba.fastjson.JSONException;
import java.io.Closeable;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;

public class IOUtils
{

    public static final char ASCII_CHARS[] = {
        '0', '0', '0', '1', '0', '2', '0', '3', '0', '4', 
        '0', '5', '0', '6', '0', '7', '0', '8', '0', '9', 
        '0', 'A', '0', 'B', '0', 'C', '0', 'D', '0', 'E', 
        '0', 'F', '1', '0', '1', '1', '1', '2', '1', '3', 
        '1', '4', '1', '5', '1', '6', '1', '7', '1', '8', 
        '1', '9', '1', 'A', '1', 'B', '1', 'C', '1', 'D', 
        '1', 'E', '1', 'F', '2', '0', '2', '1', '2', '2', 
        '2', '3', '2', '4', '2', '5', '2', '6', '2', '7', 
        '2', '8', '2', '9', '2', 'A', '2', 'B', '2', 'C', 
        '2', 'D', '2', 'E', '2', 'F'
    };
    public static final char DIGITS[] = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        'A', 'B', 'C', 'D', 'E', 'F'
    };
    static final char DigitOnes[] = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    };
    static final char DigitTens[] = {
        '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 
        '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', 
        '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', 
        '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', 
        '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', 
        '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', 
        '6', '6', '6', '6', '6', '6', '6', '6', '6', '6', 
        '7', '7', '7', '7', '7', '7', '7', '7', '7', '7', 
        '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', 
        '9', '9', '9', '9', '9', '9', '9', '9', '9', '9'
    };
    static final char digits[] = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 
        'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 
        'u', 'v', 'w', 'x', 'y', 'z'
    };
    public static final boolean firstIdentifierFlags[];
    public static final boolean identifierFlags[];
    public static final char replaceChars[];
    static final int sizeTable[] = {
        9, 99, 999, 9999, 0x1869f, 0xf423f, 0x98967f, 0x5f5e0ff, 0x3b9ac9ff, 0x7fffffff
    };
    public static final byte specicalFlags_doubleQuotes[];
    public static final byte specicalFlags_singleQuotes[];
    public static boolean whitespaceFlags[];

    public IOUtils()
    {
    }

    public static void close(Closeable closeable)
    {
        if (closeable == null)
        {
            break MISSING_BLOCK_LABEL_10;
        }
        closeable.close();
        return;
        closeable;
    }

    public static void decode(CharsetDecoder charsetdecoder, ByteBuffer bytebuffer, CharBuffer charbuffer)
    {
        try
        {
            bytebuffer = charsetdecoder.decode(bytebuffer, charbuffer, true);
            if (!bytebuffer.isUnderflow())
            {
                bytebuffer.throwException();
            }
            charsetdecoder = charsetdecoder.flush(charbuffer);
            if (!charsetdecoder.isUnderflow())
            {
                charsetdecoder.throwException();
            }
            return;
        }
        // Misplaced declaration of an exception variable
        catch (CharsetDecoder charsetdecoder)
        {
            throw new JSONException(charsetdecoder.getMessage(), charsetdecoder);
        }
    }

    public static boolean firstIdentifier(char c)
    {
        return c < firstIdentifierFlags.length && firstIdentifierFlags[c];
    }

    public static void getChars(byte byte0, int i, char ac[])
    {
        char c = '\0';
        int j = byte0;
        int k = i;
        if (byte0 < 0)
        {
            c = '-';
            j = -byte0;
            k = i;
        }
        do
        {
            byte0 = 52429 * j >>> 19;
            k--;
            ac[k] = digits[j - ((byte0 << 3) + (byte0 << 1))];
            if (byte0 == 0)
            {
                if (c != 0)
                {
                    ac[k - 1] = c;
                }
                return;
            }
            j = byte0;
        } while (true);
    }

    public static void getChars(int i, int j, char ac[])
    {
        char c;
        int l;
        int j1;
        if (i < 0)
        {
            int k = -i;
            c = '-';
            i = j;
            j = k;
        } else
        {
            c = '\0';
            int i1 = i;
            i = j;
            j = i1;
        }
        do
        {
            j1 = i;
            l = j;
            if (j < 0x10000)
            {
                break;
            }
            l = j / 100;
            j -= (l << 6) + (l << 5) + (l << 2);
            i--;
            ac[i] = DigitOnes[j];
            i--;
            ac[i] = DigitTens[j];
            j = l;
        } while (true);
        do
        {
            i = 52429 * l >>> 19;
            j1--;
            ac[j1] = digits[l - ((i << 3) + (i << 1))];
            if (i != 0)
            {
                l = i;
            } else
            {
                if (c != 0)
                {
                    ac[j1 - 1] = c;
                }
                return;
            }
        } while (true);
    }

    public static void getChars(long l, int i, char ac[])
    {
        char c;
        int k;
        int i1;
        long l1;
        if (l < 0L)
        {
            l = -l;
            c = '-';
        } else
        {
            c = '\0';
        }
        for (; l > 0x7fffffffL; l = l1)
        {
            l1 = l / 100L;
            int j = (int)(l - ((l1 << 6) + (l1 << 5) + (l1 << 2)));
            i--;
            ac[i] = DigitOnes[j];
            i--;
            ac[i] = DigitTens[j];
        }

        i1 = (int)l;
        k = i;
        for (i = i1; i >= 0x10000; i = i1)
        {
            i1 = i / 100;
            i -= (i1 << 6) + (i1 << 5) + (i1 << 2);
            k--;
            ac[k] = DigitOnes[i];
            k--;
            ac[k] = DigitTens[i];
        }

        do
        {
            j1 = 52429 * i >>> 19;
            k--;
            ac[k] = digits[i - ((j1 << 3) + (j1 << 1))];
            if (j1 != 0)
            {
                i = j1;
            } else
            {
                if (c != 0)
                {
                    ac[k - 1] = c;
                }
                return;
            }
        } while (true);
    }

    public static boolean isIdent(char c)
    {
        return c < identifierFlags.length && identifierFlags[c];
    }

    public static int stringSize(int i)
    {
        int j = 0;
        do
        {
            if (i <= sizeTable[j])
            {
                return j + 1;
            }
            j++;
        } while (true);
    }

    public static int stringSize(long l)
    {
        int i = 1;
        long l1 = 10L;
        for (; i < 19; i++)
        {
            if (l < l1)
            {
                return i;
            }
            l1 *= 10L;
        }

        return 19;
    }

    static 
    {
        firstIdentifierFlags = new boolean[256];
        char c = '\0';
        while (c < firstIdentifierFlags.length) 
        {
            if (c >= 65 && c <= 90)
            {
                firstIdentifierFlags[c] = true;
            } else
            if (c >= 'a' && c <= 'z')
            {
                firstIdentifierFlags[c] = true;
            } else
            if (c == '_')
            {
                firstIdentifierFlags[c] = true;
            }
            c++;
        }
        identifierFlags = new boolean[256];
        c = '\0';
        while (c < identifierFlags.length) 
        {
            if (c >= 65 && c <= 90)
            {
                identifierFlags[c] = true;
            } else
            if (c >= 'a' && c <= 'z')
            {
                identifierFlags[c] = true;
            } else
            if (c == '_')
            {
                identifierFlags[c] = true;
            } else
            if (c >= '0' && c <= '9')
            {
                identifierFlags[c] = true;
            }
            c++;
        }
        specicalFlags_doubleQuotes = new byte[256];
        specicalFlags_singleQuotes = new byte[256];
        replaceChars = new char[128];
        specicalFlags_doubleQuotes[0] = 4;
        specicalFlags_doubleQuotes[1] = 4;
        specicalFlags_doubleQuotes[2] = 4;
        specicalFlags_doubleQuotes[3] = 4;
        specicalFlags_doubleQuotes[4] = 4;
        specicalFlags_doubleQuotes[5] = 4;
        specicalFlags_doubleQuotes[6] = 4;
        specicalFlags_doubleQuotes[7] = 4;
        specicalFlags_doubleQuotes[8] = 1;
        specicalFlags_doubleQuotes[9] = 1;
        specicalFlags_doubleQuotes[10] = 1;
        specicalFlags_doubleQuotes[11] = 4;
        specicalFlags_doubleQuotes[12] = 1;
        specicalFlags_doubleQuotes[13] = 1;
        specicalFlags_doubleQuotes[34] = 1;
        specicalFlags_doubleQuotes[92] = 1;
        specicalFlags_singleQuotes[0] = 4;
        specicalFlags_singleQuotes[1] = 4;
        specicalFlags_singleQuotes[2] = 4;
        specicalFlags_singleQuotes[3] = 4;
        specicalFlags_singleQuotes[4] = 4;
        specicalFlags_singleQuotes[5] = 4;
        specicalFlags_singleQuotes[6] = 4;
        specicalFlags_singleQuotes[7] = 4;
        specicalFlags_singleQuotes[8] = 1;
        specicalFlags_singleQuotes[9] = 1;
        specicalFlags_singleQuotes[10] = 1;
        specicalFlags_singleQuotes[11] = 4;
        specicalFlags_singleQuotes[12] = 1;
        specicalFlags_singleQuotes[13] = 1;
        specicalFlags_singleQuotes[92] = 1;
        specicalFlags_singleQuotes[39] = 1;
        for (int i = 14; i <= 31; i++)
        {
            specicalFlags_doubleQuotes[i] = 4;
            specicalFlags_singleQuotes[i] = 4;
        }

        for (int j = 127; j <= 160; j++)
        {
            specicalFlags_doubleQuotes[j] = 4;
            specicalFlags_singleQuotes[j] = 4;
        }

        replaceChars[0] = '0';
        replaceChars[1] = '1';
        replaceChars[2] = '2';
        replaceChars[3] = '3';
        replaceChars[4] = '4';
        replaceChars[5] = '5';
        replaceChars[6] = '6';
        replaceChars[7] = '7';
        replaceChars[8] = 'b';
        replaceChars[9] = 't';
        replaceChars[10] = 'n';
        replaceChars[11] = 'v';
        replaceChars[12] = 'f';
        replaceChars[13] = 'r';
        replaceChars[34] = '"';
        replaceChars[39] = '\'';
        replaceChars[47] = '/';
        replaceChars[92] = '\\';
        whitespaceFlags = new boolean[256];
        whitespaceFlags[32] = true;
        whitespaceFlags[10] = true;
        whitespaceFlags[13] = true;
        whitespaceFlags[9] = true;
        whitespaceFlags[12] = true;
        whitespaceFlags[8] = true;
    }
}
