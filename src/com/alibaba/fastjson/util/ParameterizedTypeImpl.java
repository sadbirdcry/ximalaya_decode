// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class ParameterizedTypeImpl
    implements ParameterizedType
{

    private final Type actualTypeArguments[];
    private final Type ownerType;
    private final Type rawType;

    public ParameterizedTypeImpl(Type atype[], Type type, Type type1)
    {
        actualTypeArguments = atype;
        ownerType = type;
        rawType = type1;
    }

    public Type[] getActualTypeArguments()
    {
        return actualTypeArguments;
    }

    public Type getOwnerType()
    {
        return ownerType;
    }

    public Type getRawType()
    {
        return rawType;
    }
}
