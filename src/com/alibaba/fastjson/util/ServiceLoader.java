// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

// Referenced classes of package com.alibaba.fastjson.util:
//            IOUtils

public class ServiceLoader
{

    private static final String PREFIX = "META-INF/services/";
    private static final Set loadedUrls = new HashSet();

    public ServiceLoader()
    {
    }

    public static Set load(Class class1, ClassLoader classloader)
    {
        if (classloader == null)
        {
            return Collections.emptySet();
        }
        HashSet hashset = new HashSet();
        class1 = class1.getName();
        Object obj = (new StringBuilder()).append("META-INF/services/").append(class1).toString();
        class1 = new HashSet();
        try
        {
            obj = classloader.getResources(((String) (obj)));
            do
            {
                if (!((Enumeration) (obj)).hasMoreElements())
                {
                    break;
                }
                URL url = (URL)((Enumeration) (obj)).nextElement();
                if (!loadedUrls.contains(url.toString()))
                {
                    load(url, ((Set) (class1)));
                    loadedUrls.add(url.toString());
                }
            } while (true);
        }
        catch (IOException ioexception) { }
        for (class1 = class1.iterator(); class1.hasNext();)
        {
            String s = (String)class1.next();
            try
            {
                hashset.add(classloader.loadClass(s).newInstance());
            }
            catch (Exception exception) { }
        }

        return hashset;
    }

    public static void load(URL url, Set set)
        throws IOException
    {
        Object obj = null;
        url = url.openStream();
        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(url, "utf-8"));
_L1:
        obj = bufferedreader.readLine();
        if (obj == null)
        {
            IOUtils.close(bufferedreader);
            IOUtils.close(url);
            return;
        }
        int i = ((String) (obj)).indexOf('#');
        String s;
        s = ((String) (obj));
        if (i < 0)
        {
            break MISSING_BLOCK_LABEL_69;
        }
        s = ((String) (obj)).substring(0, i);
        obj = s.trim();
        if (((String) (obj)).length() != 0)
        {
            set.add(obj);
        }
          goto _L1
        Exception exception;
        exception;
        obj = url;
        set = bufferedreader;
        url = exception;
_L3:
        IOUtils.close(set);
        IOUtils.close(((java.io.Closeable) (obj)));
        throw url;
        url;
        set = null;
        continue; /* Loop/switch isn't completed */
        exception;
        set = null;
        obj = url;
        url = exception;
        if (true) goto _L3; else goto _L2
_L2:
    }

}
