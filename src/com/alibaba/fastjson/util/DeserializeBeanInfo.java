// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.util;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.annotation.JSONCreator;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

// Referenced classes of package com.alibaba.fastjson.util:
//            TypeUtils, FieldInfo

public class DeserializeBeanInfo
{

    private final Class clazz;
    private Constructor creatorConstructor;
    private Constructor defaultConstructor;
    private Method factoryMethod;
    private final List fieldList = new ArrayList();
    private int parserFeatures;
    private final List sortedFieldList = new ArrayList();

    public DeserializeBeanInfo(Class class1)
    {
        parserFeatures = 0;
        clazz = class1;
        parserFeatures = TypeUtils.getParserFeatures(class1);
    }

    public static DeserializeBeanInfo computeSetters(Class class1, Type type)
    {
        Object obj;
        DeserializeBeanInfo deserializebeaninfo;
        deserializebeaninfo = new DeserializeBeanInfo(class1);
        obj = getDefaultConstructor(class1);
        if (obj == null) goto _L2; else goto _L1
_L1:
        TypeUtils.setAccessible(((java.lang.reflect.AccessibleObject) (obj)));
        deserializebeaninfo.setDefaultConstructor(((Constructor) (obj)));
_L17:
        Method amethod1[];
        int j1;
        int j3;
        amethod1 = class1.getMethods();
        j3 = amethod1.length;
        j1 = 0;
_L4:
        Object obj2;
        Method method2;
        int i;
        int k;
        if (j1 >= j3)
        {
            break MISSING_BLOCK_LABEL_1052;
        }
        method2 = amethod1[j1];
        i = 0;
        k = 0;
        obj2 = method2.getName();
          goto _L3
_L6:
        j1++;
        if (true) goto _L4; else goto _L2
_L2:
        if (obj == null && !class1.isInterface() && !Modifier.isAbstract(class1.getModifiers()))
        {
label0:
            {
                Constructor constructor = getCreatorConstructor(class1);
                if (constructor == null)
                {
                    break label0;
                }
                TypeUtils.setAccessible(constructor);
                deserializebeaninfo.setCreatorConstructor(constructor);
                for (i = 0; i < constructor.getParameterTypes().length; i++)
                {
                    obj2 = constructor.getParameterAnnotations()[i];
                    obj = null;
                    int k1 = obj2.length;
                    k = 0;
label1:
                    do
                    {
label2:
                        {
                            type = ((Type) (obj));
                            if (k < k1)
                            {
                                type = obj2[k];
                                if (!(type instanceof JSONField))
                                {
                                    break label2;
                                }
                                type = (JSONField)type;
                            }
                            if (type == null)
                            {
                                throw new JSONException("illegal json creator");
                            }
                            break label1;
                        }
                        k++;
                    } while (true);
                    obj = constructor.getParameterTypes()[i];
                    obj2 = constructor.getGenericParameterTypes()[i];
                    Field field = TypeUtils.getField(class1, type.name());
                    k = type.ordinal();
                    k1 = SerializerFeature.of(type.serialzeFeatures());
                    deserializebeaninfo.add(new FieldInfo(type.name(), class1, ((Class) (obj)), ((Type) (obj2)), field, k, k1));
                }

                return deserializebeaninfo;
            }
label3:
            {
                Method method = getFactoryMethod(class1);
                if (method == null)
                {
                    break label3;
                }
                TypeUtils.setAccessible(method);
                deserializebeaninfo.setFactoryMethod(method);
                for (i = 0; i < method.getParameterTypes().length; i++)
                {
                    obj2 = method.getParameterAnnotations()[i];
                    obj = null;
                    int l1 = obj2.length;
                    k = 0;
label4:
                    do
                    {
label5:
                        {
                            type = ((Type) (obj));
                            if (k < l1)
                            {
                                type = obj2[k];
                                if (!(type instanceof JSONField))
                                {
                                    break label5;
                                }
                                type = (JSONField)type;
                            }
                            if (type == null)
                            {
                                throw new JSONException("illegal json creator");
                            }
                            break label4;
                        }
                        k++;
                    } while (true);
                    obj = method.getParameterTypes()[i];
                    obj2 = method.getGenericParameterTypes()[i];
                    Field field1 = TypeUtils.getField(class1, type.name());
                    k = type.ordinal();
                    l1 = SerializerFeature.of(type.serialzeFeatures());
                    deserializebeaninfo.add(new FieldInfo(type.name(), class1, ((Class) (obj)), ((Type) (obj2)), field1, k, l1));
                }

                return deserializebeaninfo;
            }
            throw new JSONException((new StringBuilder()).append("default constructor not found. ").append(class1).toString());
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (((String) (obj2)).length() < 4 || Modifier.isStatic(method2.getModifiers()) || !method2.getReturnType().equals(Void.TYPE) && !method2.getReturnType().equals(class1) || method2.getParameterTypes().length != 1) goto _L6; else goto _L5
_L5:
        JSONField jsonfield = (JSONField)method2.getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        obj = jsonfield;
        if (jsonfield == null)
        {
            obj = TypeUtils.getSupperMethodAnnotation(class1, method2);
        }
        if (obj == null) goto _L8; else goto _L7
_L7:
        if (!((JSONField) (obj)).deserialize()) goto _L6; else goto _L9
_L9:
        int j2;
        int l2;
        j2 = ((JSONField) (obj)).ordinal();
        l2 = SerializerFeature.of(((JSONField) (obj)).serialzeFeatures());
        i = j2;
        k = l2;
        if (((JSONField) (obj)).name().length() == 0) goto _L8; else goto _L10
_L10:
        deserializebeaninfo.add(new FieldInfo(((JSONField) (obj)).name(), method2, null, class1, type, j2, l2));
        TypeUtils.setAccessible(method2);
          goto _L6
_L8:
        if (!((String) (obj2)).startsWith("set")) goto _L6; else goto _L11
_L11:
        char c;
        c = ((String) (obj2)).charAt(3);
        Object obj1;
        if (Character.isUpperCase(c))
        {
            if (TypeUtils.compatibleWithJavaBean)
            {
                obj = TypeUtils.decapitalize(((String) (obj2)).substring(3));
            } else
            {
                obj = (new StringBuilder()).append(Character.toLowerCase(((String) (obj2)).charAt(3))).append(((String) (obj2)).substring(4)).toString();
            }
        } else
        {
label6:
            {
                if (c != '_')
                {
                    break label6;
                }
                obj = ((String) (obj2)).substring(4);
            }
        }
_L14:
        obj2 = TypeUtils.getField(class1, ((String) (obj)));
        obj1 = obj2;
        if (obj2 == null)
        {
            obj1 = obj2;
            if (method2.getParameterTypes()[0] == Boolean.TYPE)
            {
                obj1 = TypeUtils.getField(class1, (new StringBuilder()).append("is").append(Character.toUpperCase(((String) (obj)).charAt(0))).append(((String) (obj)).substring(1)).toString());
            }
        }
        j2 = i;
        l2 = k;
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_1021;
        }
        obj2 = (JSONField)((Field) (obj1)).getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        j2 = i;
        l2 = k;
        if (obj2 == null)
        {
            break MISSING_BLOCK_LABEL_1021;
        }
        i = ((JSONField) (obj2)).ordinal();
        k = SerializerFeature.of(((JSONField) (obj2)).serialzeFeatures());
        j2 = i;
        l2 = k;
        if (((JSONField) (obj2)).name().length() == 0)
        {
            break MISSING_BLOCK_LABEL_1021;
        }
        deserializebeaninfo.add(new FieldInfo(((JSONField) (obj2)).name(), method2, ((Field) (obj1)), class1, type, i, k));
          goto _L6
        if (c != 'f') goto _L13; else goto _L12
_L12:
        obj = ((String) (obj2)).substring(3);
          goto _L14
_L13:
        if (((String) (obj2)).length() < 5 || !Character.isUpperCase(((String) (obj2)).charAt(4))) goto _L6; else goto _L15
_L15:
        obj = TypeUtils.decapitalize(((String) (obj2)).substring(3));
          goto _L14
        deserializebeaninfo.add(new FieldInfo(((String) (obj)), method2, null, class1, type, j2, l2));
        TypeUtils.setAccessible(method2);
          goto _L6
        Field afield[] = class1.getFields();
        int k3 = afield.length;
        int j = 0;
        while (j < k3) 
        {
            Field field2 = afield[j];
            if (!Modifier.isStatic(field2.getModifiers()))
            {
                Iterator iterator = deserializebeaninfo.getFieldList().iterator();
                boolean flag = false;
                do
                {
                    if (!iterator.hasNext())
                    {
                        break;
                    }
                    if (((FieldInfo)iterator.next()).getName().equals(field2.getName()))
                    {
                        flag = true;
                    }
                } while (true);
                if (!flag)
                {
                    int l = 0;
                    int i2 = 0;
                    String s2 = field2.getName();
                    JSONField jsonfield2 = (JSONField)field2.getAnnotation(com/alibaba/fastjson/annotation/JSONField);
                    String s = s2;
                    if (jsonfield2 != null)
                    {
                        int k2 = jsonfield2.ordinal();
                        int i3 = SerializerFeature.of(jsonfield2.serialzeFeatures());
                        s = s2;
                        l = k2;
                        i2 = i3;
                        if (jsonfield2.name().length() != 0)
                        {
                            s = jsonfield2.name();
                            i2 = i3;
                            l = k2;
                        }
                    }
                    deserializebeaninfo.add(new FieldInfo(s, null, field2, class1, type, l, i2));
                }
            }
            j++;
        }
        Method amethod[] = class1.getMethods();
        int i1 = amethod.length;
        j = 0;
        do
        {
            if (j >= i1)
            {
                break;
            }
            Method method1 = amethod[j];
            String s1 = method1.getName();
            if (s1.length() >= 4 && !Modifier.isStatic(method1.getModifiers()) && s1.startsWith("get") && Character.isUpperCase(s1.charAt(3)) && method1.getParameterTypes().length == 0 && (java/util/Collection.isAssignableFrom(method1.getReturnType()) || java/util/Map.isAssignableFrom(method1.getReturnType()) || java/util/concurrent/atomic/AtomicBoolean == method1.getReturnType() || java/util/concurrent/atomic/AtomicInteger == method1.getReturnType() || java/util/concurrent/atomic/AtomicLong == method1.getReturnType()))
            {
                JSONField jsonfield1 = (JSONField)method1.getAnnotation(com/alibaba/fastjson/annotation/JSONField);
                if (jsonfield1 != null && jsonfield1.name().length() > 0)
                {
                    s1 = jsonfield1.name();
                } else
                {
                    s1 = (new StringBuilder()).append(Character.toLowerCase(s1.charAt(3))).append(s1.substring(4)).toString();
                }
                if (deserializebeaninfo.getField(s1) == null)
                {
                    deserializebeaninfo.add(new FieldInfo(s1, method1, null, class1, type));
                    TypeUtils.setAccessible(method1);
                }
            }
            j++;
        } while (true);
        return deserializebeaninfo;
        if (true) goto _L17; else goto _L16
_L16:
    }

    public static Constructor getCreatorConstructor(Class class1)
    {
        class1 = class1.getDeclaredConstructors();
        int j = class1.length;
        for (int i = 0; i < j; i++)
        {
            Constructor constructor = class1[i];
            if ((JSONCreator)constructor.getAnnotation(com/alibaba/fastjson/annotation/JSONCreator) != null)
            {
                if (false)
                {
                    throw new JSONException("multi-json creator");
                } else
                {
                    return constructor;
                }
            }
        }

        return null;
    }

    public static Constructor getDefaultConstructor(Class class1)
    {
        Object obj = null;
        if (!Modifier.isAbstract(class1.getModifiers())) goto _L2; else goto _L1
_L1:
        return ((Constructor) (obj));
_L2:
        int i;
        int j;
        obj = class1.getDeclaredConstructors();
        j = obj.length;
        i = 0;
_L10:
        Constructor constructor;
        if (i >= j)
        {
            break MISSING_BLOCK_LABEL_141;
        }
        constructor = obj[i];
        if (constructor.getParameterTypes().length != 0) goto _L4; else goto _L3
_L3:
        if (constructor != null || !class1.isMemberClass() || Modifier.isStatic(class1.getModifiers())) goto _L6; else goto _L5
_L5:
        Constructor aconstructor[];
        aconstructor = class1.getDeclaredConstructors();
        j = aconstructor.length;
        i = 0;
_L9:
        if (i >= j) goto _L6; else goto _L7
_L7:
        Constructor constructor1;
        constructor1 = aconstructor[i];
        if (constructor1.getParameterTypes().length != 1)
        {
            break; /* Loop/switch isn't completed */
        }
        obj = constructor1;
        if (constructor1.getParameterTypes()[0].equals(class1.getDeclaringClass())) goto _L1; else goto _L8
_L8:
        i++;
          goto _L9
          goto _L1
_L4:
        i++;
          goto _L10
_L6:
        return constructor;
        constructor = null;
          goto _L3
    }

    public static Method getFactoryMethod(Class class1)
    {
        Method amethod[];
        int i;
        int j;
        amethod = class1.getDeclaredMethods();
        j = amethod.length;
        i = 0;
_L3:
        Method method;
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        method = amethod[i];
          goto _L1
_L5:
        i++;
        if (true) goto _L3; else goto _L2
_L1:
        if (!Modifier.isStatic(method.getModifiers()) || !class1.isAssignableFrom(method.getReturnType()) || (JSONCreator)method.getAnnotation(com/alibaba/fastjson/annotation/JSONCreator) == null) goto _L5; else goto _L4
_L4:
        if (false)
        {
            throw new JSONException("multi-json creator");
        } else
        {
            return method;
        }
_L2:
        return null;
    }

    public boolean add(FieldInfo fieldinfo)
    {
label0:
        {
            Iterator iterator = fieldList.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                FieldInfo fieldinfo1 = (FieldInfo)iterator.next();
                if (!fieldinfo1.getName().equals(fieldinfo.getName()) || fieldinfo1.isGetOnly() && !fieldinfo.isGetOnly())
                {
                    continue;
                }
                if (!fieldinfo1.getFieldClass().isAssignableFrom(fieldinfo.getFieldClass()))
                {
                    break label0;
                }
                fieldList.remove(fieldinfo1);
                break;
            } while (true);
            fieldList.add(fieldinfo);
            sortedFieldList.add(fieldinfo);
            Collections.sort(sortedFieldList);
            return true;
        }
        return false;
    }

    public Class getClazz()
    {
        return clazz;
    }

    public Constructor getCreatorConstructor()
    {
        return creatorConstructor;
    }

    public Constructor getDefaultConstructor()
    {
        return defaultConstructor;
    }

    public Method getFactoryMethod()
    {
        return factoryMethod;
    }

    public FieldInfo getField(String s)
    {
        for (Iterator iterator = fieldList.iterator(); iterator.hasNext();)
        {
            FieldInfo fieldinfo = (FieldInfo)iterator.next();
            if (fieldinfo.getName().equals(s))
            {
                return fieldinfo;
            }
        }

        return null;
    }

    public List getFieldList()
    {
        return fieldList;
    }

    public int getParserFeatures()
    {
        return parserFeatures;
    }

    public List getSortedFieldList()
    {
        return sortedFieldList;
    }

    public void setCreatorConstructor(Constructor constructor)
    {
        creatorConstructor = constructor;
    }

    public void setDefaultConstructor(Constructor constructor)
    {
        defaultConstructor = constructor;
    }

    public void setFactoryMethod(Method method)
    {
        factoryMethod = method;
    }
}
