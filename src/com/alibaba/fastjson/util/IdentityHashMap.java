// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.util;


public class IdentityHashMap
{
    protected static final class Entry
    {

        public final int hashCode;
        public final Object key;
        public final Entry next;
        public Object value;

        public Entry(Object obj, Object obj1, int i, Entry entry)
        {
            key = obj;
            value = obj1;
            next = entry;
            hashCode = i;
        }
    }


    public static final int DEFAULT_TABLE_SIZE = 1024;
    private final Entry buckets[];
    private final int indexMask;

    public IdentityHashMap()
    {
        this(1024);
    }

    public IdentityHashMap(int i)
    {
        indexMask = i - 1;
        buckets = new Entry[i];
    }

    public final Object get(Object obj)
    {
        int i = System.identityHashCode(obj);
        int j = indexMask;
        for (Entry entry = buckets[i & j]; entry != null; entry = entry.next)
        {
            if (obj == entry.key)
            {
                return entry.value;
            }
        }

        return null;
    }

    public boolean put(Object obj, Object obj1)
    {
        int i = System.identityHashCode(obj);
        int j = i & indexMask;
        for (Entry entry = buckets[j]; entry != null; entry = entry.next)
        {
            if (obj == entry.key)
            {
                entry.value = obj1;
                return true;
            }
        }

        obj = new Entry(obj, obj1, i, buckets[j]);
        buckets[j] = ((Entry) (obj));
        return false;
    }

    public int size()
    {
        int i = 0;
        int j = 0;
        for (; i < buckets.length; i++)
        {
            for (Entry entry = buckets[i]; entry != null;)
            {
                entry = entry.next;
                j++;
            }

        }

        return j;
    }
}
