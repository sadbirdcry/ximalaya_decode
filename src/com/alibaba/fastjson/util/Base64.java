// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.util;

import java.util.Arrays;

public class Base64
{

    public static final char CA[];
    public static final int IA[];

    public Base64()
    {
    }

    public static byte[] decodeFast(String s)
    {
        boolean flag = false;
        int l = s.length();
        if (l == 0)
        {
            return new byte[0];
        }
        int k1 = l - 1;
        int i;
        for (i = 0; i < k1 && IA[s.charAt(i) & 0xff] < 0; i++) { }
        for (; k1 > 0 && IA[s.charAt(k1) & 0xff] < 0; k1--) { }
        byte abyte0[];
        int k;
        int i1;
        int l1;
        int k3;
        if (s.charAt(k1) == '=')
        {
            if (s.charAt(k1 - 1) == '=')
            {
                k = 2;
            } else
            {
                k = 1;
            }
        } else
        {
            k = 0;
        }
        i1 = (k1 - i) + 1;
        if (l > 76)
        {
            int ai[];
            int k2;
            int i3;
            int j3;
            int l3;
            int i4;
            if (s.charAt(76) == '\r')
            {
                l = i1 / 78;
            } else
            {
                l = 0;
            }
            l1 = l << 1;
        } else
        {
            l1 = 0;
        }
        k3 = ((i1 - l1) * 6 >> 3) - k;
        abyte0 = new byte[k3];
        l3 = k3 / 3;
        i1 = 0;
        l = 0;
        do
        {
            if (l >= l3 * 3)
            {
                break;
            }
            ai = IA;
            i3 = i + 1;
            i = ai[s.charAt(i)];
            ai = IA;
            k2 = i3 + 1;
            i3 = ai[s.charAt(i3)];
            ai = IA;
            j3 = k2 + 1;
            i4 = ai[s.charAt(k2)];
            ai = IA;
            k2 = j3 + 1;
            i = i4 << 6 | (i << 18 | i3 << 12) | ai[s.charAt(j3)];
            i3 = l + 1;
            abyte0[l] = (byte)(i >> 16);
            l = i3 + 1;
            abyte0[i3] = (byte)(i >> 8);
            i3 = l + 1;
            abyte0[l] = (byte)i;
            i = k2;
            l = i3;
            if (l1 > 0)
            {
                j3 = i1 + 1;
                i1 = j3;
                i = k2;
                l = i3;
                if (j3 == 19)
                {
                    i = k2 + 2;
                    i1 = 0;
                    l = i3;
                }
            }
        } while (true);
        if (l < k3)
        {
            int i2 = i;
            int j1 = 0;
            i = ((flag) ? 1 : 0);
            do
            {
                int j2 = i;
                if (i2 > k1 - k)
                {
                    break;
                }
                int l2 = IA[s.charAt(i2)];
                i = j2 + 1;
                j1 = l2 << 18 - j2 * 6 | j1;
                i2++;
            } while (true);
            k = 16;
            for (int j = l; j < k3; j++)
            {
                abyte0[j] = (byte)(j1 >> k);
                k -= 8;
            }

        }
        return abyte0;
    }

    public static byte[] decodeFast(String s, int i, int j)
    {
        boolean flag = false;
        if (j == 0)
        {
            return new byte[0];
        }
        int i1;
        for (i1 = (i + j) - 1; i < i1 && IA[s.charAt(i)] < 0; i++) { }
        for (; i1 > 0 && IA[s.charAt(i1)] < 0; i1--) { }
        byte abyte0[];
        byte byte0;
        int k;
        int j1;
        int i3;
        if (s.charAt(i1) == '=')
        {
            if (s.charAt(i1 - 1) == '=')
            {
                byte0 = 2;
            } else
            {
                byte0 = 1;
            }
        } else
        {
            byte0 = 0;
        }
        k = (i1 - i) + 1;
        if (j > 76)
        {
            int ai[];
            int i2;
            int k2;
            int l2;
            int j3;
            int k3;
            if (s.charAt(76) == '\r')
            {
                j = k / 78;
            } else
            {
                j = 0;
            }
            j1 = j << 1;
        } else
        {
            j1 = 0;
        }
        i3 = ((k - j1) * 6 >> 3) - byte0;
        abyte0 = new byte[i3];
        j3 = i3 / 3;
        k = 0;
        j = 0;
        do
        {
            if (j >= j3 * 3)
            {
                break;
            }
            ai = IA;
            k2 = i + 1;
            i = ai[s.charAt(i)];
            ai = IA;
            i2 = k2 + 1;
            k2 = ai[s.charAt(k2)];
            ai = IA;
            l2 = i2 + 1;
            k3 = ai[s.charAt(i2)];
            ai = IA;
            i2 = l2 + 1;
            i = k3 << 6 | (i << 18 | k2 << 12) | ai[s.charAt(l2)];
            k2 = j + 1;
            abyte0[j] = (byte)(i >> 16);
            j = k2 + 1;
            abyte0[k2] = (byte)(i >> 8);
            k2 = j + 1;
            abyte0[j] = (byte)i;
            i = i2;
            j = k2;
            if (j1 > 0)
            {
                l2 = k + 1;
                k = l2;
                i = i2;
                j = k2;
                if (l2 == 19)
                {
                    i = i2 + 2;
                    k = 0;
                    j = k2;
                }
            }
        } while (true);
        if (j < i3)
        {
            int k1 = i;
            int l = 0;
            i = ((flag) ? 1 : 0);
            do
            {
                int l1 = i;
                if (k1 > i1 - byte0)
                {
                    break;
                }
                int j2 = IA[s.charAt(k1)];
                i = l1 + 1;
                l = j2 << 18 - l1 * 6 | l;
                k1++;
            } while (true);
            byte0 = 16;
            i = j;
            j = byte0;
            for (; i < i3; i++)
            {
                abyte0[i] = (byte)(l >> j);
                j -= 8;
            }

        }
        return abyte0;
    }

    public static byte[] decodeFast(char ac[], int i, int j)
    {
        boolean flag = false;
        if (j == 0)
        {
            return new byte[0];
        }
        int i1;
        for (i1 = (i + j) - 1; i < i1 && IA[ac[i]] < 0; i++) { }
        for (; i1 > 0 && IA[ac[i1]] < 0; i1--) { }
        byte abyte0[];
        byte byte0;
        int k;
        int j1;
        int i3;
        if (ac[i1] == '=')
        {
            if (ac[i1 - 1] == '=')
            {
                byte0 = 2;
            } else
            {
                byte0 = 1;
            }
        } else
        {
            byte0 = 0;
        }
        k = (i1 - i) + 1;
        if (j > 76)
        {
            int ai[];
            int i2;
            int k2;
            int l2;
            int j3;
            int k3;
            if (ac[76] == '\r')
            {
                j = k / 78;
            } else
            {
                j = 0;
            }
            j1 = j << 1;
        } else
        {
            j1 = 0;
        }
        i3 = ((k - j1) * 6 >> 3) - byte0;
        abyte0 = new byte[i3];
        j3 = i3 / 3;
        k = 0;
        j = 0;
        do
        {
            if (j >= j3 * 3)
            {
                break;
            }
            ai = IA;
            k2 = i + 1;
            i = ai[ac[i]];
            ai = IA;
            i2 = k2 + 1;
            k2 = ai[ac[k2]];
            ai = IA;
            l2 = i2 + 1;
            k3 = ai[ac[i2]];
            ai = IA;
            i2 = l2 + 1;
            i = k3 << 6 | (i << 18 | k2 << 12) | ai[ac[l2]];
            k2 = j + 1;
            abyte0[j] = (byte)(i >> 16);
            j = k2 + 1;
            abyte0[k2] = (byte)(i >> 8);
            k2 = j + 1;
            abyte0[j] = (byte)i;
            i = i2;
            j = k2;
            if (j1 > 0)
            {
                l2 = k + 1;
                k = l2;
                i = i2;
                j = k2;
                if (l2 == 19)
                {
                    i = i2 + 2;
                    k = 0;
                    j = k2;
                }
            }
        } while (true);
        if (j < i3)
        {
            int k1 = i;
            int l = 0;
            i = ((flag) ? 1 : 0);
            do
            {
                int l1 = i;
                if (k1 > i1 - byte0)
                {
                    break;
                }
                int j2 = IA[ac[k1]];
                i = l1 + 1;
                l = j2 << 18 - l1 * 6 | l;
                k1++;
            } while (true);
            byte0 = 16;
            i = j;
            j = byte0;
            for (; i < i3; i++)
            {
                abyte0[i] = (byte)(l >> j);
                j -= 8;
            }

        }
        return abyte0;
    }

    static 
    {
        CA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();
        IA = new int[256];
        Arrays.fill(IA, -1);
        int j = CA.length;
        for (int i = 0; i < j; i++)
        {
            IA[CA[i]] = i;
        }

        IA[61] = 0;
    }
}
