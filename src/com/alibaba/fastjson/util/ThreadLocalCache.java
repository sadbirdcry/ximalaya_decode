// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.util;

import java.lang.ref.SoftReference;
import java.nio.charset.CharsetDecoder;

// Referenced classes of package com.alibaba.fastjson.util:
//            UTF8Decoder

public class ThreadLocalCache
{

    static final boolean $assertionsDisabled;
    public static final int BYTES_CACH_INIT_SIZE = 1024;
    public static final int BYTES_CACH_INIT_SIZE_EXP = 10;
    public static final int BYTES_CACH_MAX_SIZE = 0x20000;
    public static final int BYTES_CACH_MAX_SIZE_EXP = 17;
    public static final int CHARS_CACH_INIT_SIZE = 1024;
    public static final int CHARS_CACH_INIT_SIZE_EXP = 10;
    public static final int CHARS_CACH_MAX_SIZE = 0x20000;
    public static final int CHARS_CACH_MAX_SIZE_EXP = 17;
    private static final ThreadLocal bytesBufLocal = new ThreadLocal();
    private static final ThreadLocal charsBufLocal = new ThreadLocal();
    private static final ThreadLocal decoderLocal = new ThreadLocal();

    public ThreadLocalCache()
    {
    }

    private static char[] allocate(int i)
    {
        if (i > 0x20000)
        {
            return new char[i];
        } else
        {
            char ac[] = new char[getAllocateLengthExp(10, 17, i)];
            charsBufLocal.set(new SoftReference(ac));
            return ac;
        }
    }

    private static byte[] allocateBytes(int i)
    {
        if (i > 0x20000)
        {
            return new byte[i];
        } else
        {
            byte abyte0[] = new byte[getAllocateLengthExp(10, 17, i)];
            bytesBufLocal.set(new SoftReference(abyte0));
            return abyte0;
        }
    }

    public static void clearBytes()
    {
        bytesBufLocal.set(null);
    }

    public static void clearChars()
    {
        charsBufLocal.set(null);
    }

    private static int getAllocateLengthExp(int i, int j, int k)
    {
        if (!$assertionsDisabled && 1 << j < k)
        {
            throw new AssertionError();
        }
        if (k >>> i <= 0)
        {
            return 1 << i;
        } else
        {
            return 1 << 32 - Integer.numberOfLeadingZeros(k - 1);
        }
    }

    public static byte[] getBytes(int i)
    {
        byte abyte0[] = (SoftReference)bytesBufLocal.get();
        if (abyte0 == null)
        {
            abyte0 = allocateBytes(i);
        } else
        {
            byte abyte1[] = (byte[])abyte0.get();
            if (abyte1 == null)
            {
                return allocateBytes(i);
            }
            abyte0 = abyte1;
            if (abyte1.length < i)
            {
                return allocateBytes(i);
            }
        }
        return abyte0;
    }

    public static char[] getChars(int i)
    {
        char ac[] = (SoftReference)charsBufLocal.get();
        if (ac == null)
        {
            ac = allocate(i);
        } else
        {
            char ac1[] = (char[])ac.get();
            if (ac1 == null)
            {
                return allocate(i);
            }
            ac = ac1;
            if (ac1.length < i)
            {
                return allocate(i);
            }
        }
        return ac;
    }

    public static CharsetDecoder getUTF8Decoder()
    {
        CharsetDecoder charsetdecoder = (CharsetDecoder)decoderLocal.get();
        Object obj = charsetdecoder;
        if (charsetdecoder == null)
        {
            obj = new UTF8Decoder();
            decoderLocal.set(obj);
        }
        return ((CharsetDecoder) (obj));
    }

    static 
    {
        boolean flag;
        if (!com/alibaba/fastjson/util/ThreadLocalCache.desiredAssertionStatus())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        $assertionsDisabled = flag;
    }
}
