// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.util;


// Referenced classes of package com.alibaba.fastjson.util:
//            UTF8Decoder

private static class 
{

    static final boolean $assertionsDisabled;
    public static final int UCS4_MAX = 0x10ffff;
    public static final int UCS4_MIN = 0x10000;

    public static char high(int i)
    {
        if (!$assertionsDisabled && !neededFor(i))
        {
            throw new AssertionError();
        } else
        {
            return (char)(0xd800 | i - 0x10000 >> 10 & 0x3ff);
        }
    }

    public static char low(int i)
    {
        if (!$assertionsDisabled && !neededFor(i))
        {
            throw new AssertionError();
        } else
        {
            return (char)(0xdc00 | i - 0x10000 & 0x3ff);
        }
    }

    public static boolean neededFor(int i)
    {
        return i >= 0x10000 && i <= 0x10ffff;
    }

    static 
    {
        boolean flag;
        if (!com/alibaba/fastjson/util/UTF8Decoder.desiredAssertionStatus())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        $assertionsDisabled = flag;
    }

    private ()
    {
    }
}
