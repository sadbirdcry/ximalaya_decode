// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.util;

import com.alibaba.fastjson.JSON;
import java.security.AccessController;
import java.security.ProtectionDomain;

public class ASMClassLoader extends ClassLoader
{

    private static ProtectionDomain DOMAIN = (ProtectionDomain)AccessController.doPrivileged(new _cls1());

    public ASMClassLoader()
    {
        super(getParentClassLoader());
    }

    public ASMClassLoader(ClassLoader classloader)
    {
        super(classloader);
    }

    static ClassLoader getParentClassLoader()
    {
        ClassLoader classloader;
        classloader = Thread.currentThread().getContextClassLoader();
        if (classloader == null)
        {
            break MISSING_BLOCK_LABEL_24;
        }
        classloader.loadClass(com/alibaba/fastjson/JSON.getName());
        return classloader;
        ClassNotFoundException classnotfoundexception;
        classnotfoundexception;
        return com/alibaba/fastjson/JSON.getClassLoader();
    }

    public Class defineClassPublic(String s, byte abyte0[], int i, int j)
        throws ClassFormatError
    {
        return defineClass(s, abyte0, i, j, DOMAIN);
    }

    public boolean isExternalClass(Class class1)
    {
        ClassLoader classloader;
        classloader = class1.getClassLoader();
        class1 = this;
        if (classloader == null)
        {
            return false;
        }
_L3:
        if (class1 != null)
        {
            if (class1 == classloader)
            {
                return false;
            }
        } else
        {
            return true;
        }
        if (true) goto _L2; else goto _L1
_L2:
        class1 = class1.getParent();
        if (true) goto _L3; else goto _L1
_L1:
    }


    private class _cls1
        implements PrivilegedAction
    {

        public Object run()
        {
            return com/alibaba/fastjson/util/ASMClassLoader.getProtectionDomain();
        }

        _cls1()
        {
        }
    }

}
