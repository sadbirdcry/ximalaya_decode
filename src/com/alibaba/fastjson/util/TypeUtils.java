// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.alibaba.fastjson.asm.ASMException;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.JSONScanner;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.parser.deserializer.FieldDeserializer;
import com.alibaba.fastjson.serializer.SerializerFeature;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.AccessControlException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

// Referenced classes of package com.alibaba.fastjson.util:
//            Base64, FieldInfo

public class TypeUtils
{

    public static boolean compatibleWithJavaBean = true;
    private static ConcurrentMap mappings;
    private static boolean setAccessibleEnable;

    public TypeUtils()
    {
    }

    public static void addBaseClassMappings()
    {
        mappings.put("byte", Byte.TYPE);
        mappings.put("short", Short.TYPE);
        mappings.put("int", Integer.TYPE);
        mappings.put("long", Long.TYPE);
        mappings.put("float", Float.TYPE);
        mappings.put("double", Double.TYPE);
        mappings.put("boolean", Boolean.TYPE);
        mappings.put("char", Character.TYPE);
        mappings.put("[byte", [B);
        mappings.put("[short", [S);
        mappings.put("[int", [I);
        mappings.put("[long", [J);
        mappings.put("[float", [F);
        mappings.put("[double", [D);
        mappings.put("[boolean", [Z);
        mappings.put("[char", [C);
        mappings.put(java/util/HashMap.getName(), java/util/HashMap);
    }

    public static void addClassMapping(String s, Class class1)
    {
        String s1 = s;
        if (s == null)
        {
            s1 = class1.getName();
        }
        mappings.put(s1, class1);
    }

    public static Object cast(Object obj, Class class1, ParserConfig parserconfig)
    {
        if (obj != null) goto _L2; else goto _L1
_L1:
        Object obj1 = null;
_L4:
        return obj1;
_L2:
        Map map;
        if (class1 == null)
        {
            throw new IllegalArgumentException("clazz is null");
        }
        obj1 = obj;
        if (class1 == obj.getClass())
        {
            continue; /* Loop/switch isn't completed */
        }
        if (!(obj instanceof Map))
        {
            break MISSING_BLOCK_LABEL_84;
        }
        obj1 = obj;
        if (class1 == java/util/Map)
        {
            continue; /* Loop/switch isn't completed */
        }
        map = (Map)obj;
        if (class1 != java/lang/Object)
        {
            break; /* Loop/switch isn't completed */
        }
        obj1 = obj;
        if (!map.containsKey(JSON.DEFAULT_TYPE_KEY)) goto _L4; else goto _L3
_L3:
        return castToJavaBean((Map)obj, class1, parserconfig);
        if (class1.isArray())
        {
            if (obj instanceof Collection)
            {
                obj1 = (Collection)obj;
                int i = 0;
                obj = Array.newInstance(class1.getComponentType(), ((Collection) (obj1)).size());
                for (obj1 = ((Collection) (obj1)).iterator(); ((Iterator) (obj1)).hasNext();)
                {
                    Array.set(obj, i, cast(((Iterator) (obj1)).next(), class1.getComponentType(), parserconfig));
                    i++;
                }

                return obj;
            }
            if (class1 == [B)
            {
                return castToBytes(obj);
            }
        }
        obj1 = obj;
        if (!class1.isAssignableFrom(obj.getClass()))
        {
            if (class1 == Boolean.TYPE || class1 == java/lang/Boolean)
            {
                return castToBoolean(obj);
            }
            if (class1 == Byte.TYPE || class1 == java/lang/Byte)
            {
                return castToByte(obj);
            }
            if (class1 == Short.TYPE || class1 == java/lang/Short)
            {
                return castToShort(obj);
            }
            if (class1 == Integer.TYPE || class1 == java/lang/Integer)
            {
                return castToInt(obj);
            }
            if (class1 == Long.TYPE || class1 == java/lang/Long)
            {
                return castToLong(obj);
            }
            if (class1 == Float.TYPE || class1 == java/lang/Float)
            {
                return castToFloat(obj);
            }
            if (class1 == Double.TYPE || class1 == java/lang/Double)
            {
                return castToDouble(obj);
            }
            if (class1 == java/lang/String)
            {
                return castToString(obj);
            }
            if (class1 == java/math/BigDecimal)
            {
                return castToBigDecimal(obj);
            }
            if (class1 == java/math/BigInteger)
            {
                return castToBigInteger(obj);
            }
            if (class1 == java/util/Date)
            {
                return castToDate(obj);
            }
            if (class1 == java/sql/Date)
            {
                return castToSqlDate(obj);
            }
            if (class1 == java/sql/Timestamp)
            {
                return castToTimestamp(obj);
            }
            if (class1.isEnum())
            {
                return castToEnum(obj, class1, parserconfig);
            }
            if (java/util/Calendar.isAssignableFrom(class1))
            {
                parserconfig = castToDate(obj);
                if (class1 == java/util/Calendar)
                {
                    obj = Calendar.getInstance();
                } else
                {
                    try
                    {
                        obj = (Calendar)class1.newInstance();
                    }
                    // Misplaced declaration of an exception variable
                    catch (Object obj)
                    {
                        throw new JSONException((new StringBuilder()).append("can not cast to : ").append(class1.getName()).toString(), ((Throwable) (obj)));
                    }
                }
                ((Calendar) (obj)).setTime(parserconfig);
                return obj;
            }
            if (obj instanceof String)
            {
                obj = (String)obj;
                if (((String) (obj)).length() == 0)
                {
                    return null;
                }
                if (class1 == java/util/Currency)
                {
                    return Currency.getInstance(((String) (obj)));
                }
            }
            throw new JSONException((new StringBuilder()).append("can not cast to : ").append(class1.getName()).toString());
        }
        if (true) goto _L4; else goto _L5
_L5:
    }

    public static Object cast(Object obj, ParameterizedType parameterizedtype, ParserConfig parserconfig)
    {
        Object obj1;
label0:
        {
label1:
            {
                obj1 = parameterizedtype.getRawType();
                if (obj1 == java/util/Set || obj1 == java/util/HashSet || obj1 == java/util/TreeSet || obj1 == java/util/List || obj1 == java/util/ArrayList)
                {
                    Type type = parameterizedtype.getActualTypeArguments()[0];
                    if (obj instanceof Iterable)
                    {
                        Iterator iterator;
                        if (obj1 == java/util/Set || obj1 == java/util/HashSet)
                        {
                            parameterizedtype = new HashSet();
                        } else
                        if (obj1 == java/util/TreeSet)
                        {
                            parameterizedtype = new TreeSet();
                        } else
                        {
                            parameterizedtype = new ArrayList();
                        }
                        iterator = ((Iterable)obj).iterator();
                        do
                        {
                            obj = parameterizedtype;
                            if (!iterator.hasNext())
                            {
                                break;
                            }
                            parameterizedtype.add(cast(iterator.next(), type, parserconfig));
                        } while (true);
                        break label1;
                    }
                }
                if (obj1 != java/util/Map && obj1 != java/util/HashMap)
                {
                    break label0;
                }
                Type type1 = parameterizedtype.getActualTypeArguments()[0];
                Type type2 = parameterizedtype.getActualTypeArguments()[1];
                if (!(obj instanceof Map))
                {
                    break label0;
                }
                parameterizedtype = new HashMap();
                for (obj = ((Map)obj).entrySet().iterator(); ((Iterator) (obj)).hasNext(); parameterizedtype.put(cast(((java.util.Map.Entry) (obj1)).getKey(), type1, parserconfig), cast(((java.util.Map.Entry) (obj1)).getValue(), type2, parserconfig)))
                {
                    obj1 = (java.util.Map.Entry)((Iterator) (obj)).next();
                }

                obj = parameterizedtype;
            }
            return obj;
        }
        if ((obj instanceof String) && ((String)obj).length() == 0)
        {
            return null;
        }
        if (parameterizedtype.getActualTypeArguments().length == 1 && (parameterizedtype.getActualTypeArguments()[0] instanceof WildcardType))
        {
            return cast(obj, ((Type) (obj1)), parserconfig);
        } else
        {
            throw new JSONException((new StringBuilder()).append("can not cast to : ").append(parameterizedtype).toString());
        }
    }

    public static Object cast(Object obj, Type type, ParserConfig parserconfig)
    {
        if (obj == null)
        {
            obj = null;
        } else
        {
            if (type instanceof Class)
            {
                return cast(obj, (Class)type, parserconfig);
            }
            if (type instanceof ParameterizedType)
            {
                return cast(obj, (ParameterizedType)type, parserconfig);
            }
            if ((obj instanceof String) && ((String)obj).length() == 0)
            {
                return null;
            }
            if (!(type instanceof TypeVariable))
            {
                throw new JSONException((new StringBuilder()).append("can not cast to : ").append(type).toString());
            }
        }
        return obj;
    }

    public static BigDecimal castToBigDecimal(Object obj)
    {
        if (obj == null)
        {
            return null;
        }
        if (obj instanceof BigDecimal)
        {
            return (BigDecimal)obj;
        }
        if (obj instanceof BigInteger)
        {
            return new BigDecimal((BigInteger)obj);
        }
        obj = obj.toString();
        if (((String) (obj)).length() == 0)
        {
            return null;
        } else
        {
            return new BigDecimal(((String) (obj)));
        }
    }

    public static BigInteger castToBigInteger(Object obj)
    {
        if (obj == null)
        {
            return null;
        }
        if (obj instanceof BigInteger)
        {
            return (BigInteger)obj;
        }
        if ((obj instanceof Float) || (obj instanceof Double))
        {
            return BigInteger.valueOf(((Number)obj).longValue());
        }
        obj = obj.toString();
        if (((String) (obj)).length() == 0)
        {
            return null;
        } else
        {
            return new BigInteger(((String) (obj)));
        }
    }

    public static Boolean castToBoolean(Object obj)
    {
        boolean flag = true;
        if (obj == null)
        {
            return null;
        }
        if (obj instanceof Boolean)
        {
            return (Boolean)obj;
        }
        if (obj instanceof Number)
        {
            if (((Number)obj).intValue() != 1)
            {
                flag = false;
            }
            return Boolean.valueOf(flag);
        }
        if (obj instanceof String)
        {
            String s = (String)obj;
            if (s.length() == 0)
            {
                return null;
            }
            if ("true".equalsIgnoreCase(s))
            {
                return Boolean.TRUE;
            }
            if ("false".equalsIgnoreCase(s))
            {
                return Boolean.FALSE;
            }
            if ("1".equals(s))
            {
                return Boolean.TRUE;
            }
            if ("0".equals(s))
            {
                return Boolean.FALSE;
            }
            if ("null".equals(s) || "NULL".equals(s))
            {
                return null;
            }
        }
        throw new JSONException((new StringBuilder()).append("can not cast to boolean, value : ").append(obj).toString());
    }

    public static Byte castToByte(Object obj)
    {
        if (obj != null)
        {
            if (obj instanceof Number)
            {
                return Byte.valueOf(((Number)obj).byteValue());
            }
            if (obj instanceof String)
            {
                obj = (String)obj;
                if (((String) (obj)).length() != 0 && !"null".equals(obj) && !"NULL".equals(obj))
                {
                    return Byte.valueOf(Byte.parseByte(((String) (obj))));
                }
            } else
            {
                throw new JSONException((new StringBuilder()).append("can not cast to byte, value : ").append(obj).toString());
            }
        }
        return null;
    }

    public static byte[] castToBytes(Object obj)
    {
        if (obj instanceof byte[])
        {
            return (byte[])(byte[])obj;
        }
        if (obj instanceof String)
        {
            return Base64.decodeFast((String)obj);
        } else
        {
            throw new JSONException((new StringBuilder()).append("can not cast to int, value : ").append(obj).toString());
        }
    }

    public static Character castToChar(Object obj)
    {
        if (obj == null)
        {
            return null;
        }
        if (obj instanceof Character)
        {
            return (Character)obj;
        }
        if (obj instanceof String)
        {
            String s = (String)obj;
            if (s.length() == 0)
            {
                return null;
            }
            if (s.length() != 1)
            {
                throw new JSONException((new StringBuilder()).append("can not cast to byte, value : ").append(obj).toString());
            } else
            {
                return Character.valueOf(s.charAt(0));
            }
        } else
        {
            throw new JSONException((new StringBuilder()).append("can not cast to byte, value : ").append(obj).toString());
        }
    }

    public static Date castToDate(Object obj)
    {
        if (obj == null)
        {
            return null;
        }
        if (obj instanceof Calendar)
        {
            return ((Calendar)obj).getTime();
        }
        if (obj instanceof Date)
        {
            return (Date)obj;
        }
        if (obj instanceof Number)
        {
            return new Date(((Number)obj).longValue());
        }
        long l;
        if (obj instanceof String)
        {
            String s = (String)obj;
            if (s.indexOf('-') != -1)
            {
                if (s.length() == JSON.DEFFAULT_DATE_FORMAT.length())
                {
                    obj = JSON.DEFFAULT_DATE_FORMAT;
                } else
                if (s.length() == 10)
                {
                    obj = "yyyy-MM-dd";
                } else
                if (s.length() == "yyyy-MM-dd HH:mm:ss".length())
                {
                    obj = "yyyy-MM-dd HH:mm:ss";
                } else
                {
                    obj = "yyyy-MM-dd HH:mm:ss.SSS";
                }
                obj = new SimpleDateFormat(((String) (obj)));
                try
                {
                    obj = ((SimpleDateFormat) (obj)).parse(s);
                }
                // Misplaced declaration of an exception variable
                catch (Object obj)
                {
                    throw new JSONException((new StringBuilder()).append("can not cast to Date, value : ").append(s).toString());
                }
                return ((Date) (obj));
            }
            if (s.length() == 0)
            {
                return null;
            }
            l = Long.parseLong(s);
        } else
        {
            l = -1L;
        }
        if (l < 0L)
        {
            throw new JSONException((new StringBuilder()).append("can not cast to Date, value : ").append(obj).toString());
        } else
        {
            return new Date(l);
        }
    }

    public static Double castToDouble(Object obj)
    {
        if (obj != null)
        {
            if (obj instanceof Number)
            {
                return Double.valueOf(((Number)obj).doubleValue());
            }
            if (obj instanceof String)
            {
                obj = obj.toString();
                if (((String) (obj)).length() != 0 && !"null".equals(obj) && !"NULL".equals(obj))
                {
                    return Double.valueOf(Double.parseDouble(((String) (obj))));
                }
            } else
            {
                throw new JSONException((new StringBuilder()).append("can not cast to double, value : ").append(obj).toString());
            }
        }
        return null;
    }

    public static Object castToEnum(Object obj, Class class1, ParserConfig parserconfig)
    {
        int i = 0;
        if (!(obj instanceof String))
        {
            break MISSING_BLOCK_LABEL_29;
        }
        obj = (String)obj;
        if (((String) (obj)).length() == 0)
        {
            return null;
        }
        return Enum.valueOf(class1, ((String) (obj)));
        int j;
        int k;
        if (!(obj instanceof Number))
        {
            break MISSING_BLOCK_LABEL_141;
        }
        j = ((Number)obj).intValue();
        obj = ((Object) ((Object[])(Object[])class1.getMethod("values", new Class[0]).invoke(null, new Object[0])));
        k = obj.length;
        while (i < k) 
        {
            int l;
            try
            {
                parserconfig = (Enum)obj[i];
                l = parserconfig.ordinal();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                throw new JSONException((new StringBuilder()).append("can not cast to : ").append(class1.getName()).toString(), ((Throwable) (obj)));
            }
            if (l != j)
            {
                i++;
            } else
            {
                return parserconfig;
            }
        }
        throw new JSONException((new StringBuilder()).append("can not cast to : ").append(class1.getName()).toString());
    }

    public static Float castToFloat(Object obj)
    {
        if (obj != null)
        {
            if (obj instanceof Number)
            {
                return Float.valueOf(((Number)obj).floatValue());
            }
            if (obj instanceof String)
            {
                obj = obj.toString();
                if (((String) (obj)).length() != 0 && !"null".equals(obj) && !"NULL".equals(obj))
                {
                    return Float.valueOf(Float.parseFloat(((String) (obj))));
                }
            } else
            {
                throw new JSONException((new StringBuilder()).append("can not cast to float, value : ").append(obj).toString());
            }
        }
        return null;
    }

    public static Integer castToInt(Object obj)
    {
        if (obj == null)
        {
            return null;
        }
        if (obj instanceof Integer)
        {
            return (Integer)obj;
        }
        if (obj instanceof Number)
        {
            return Integer.valueOf(((Number)obj).intValue());
        }
        if (obj instanceof String)
        {
            obj = (String)obj;
            if (((String) (obj)).length() == 0)
            {
                return null;
            }
            if ("null".equals(obj))
            {
                return null;
            }
            if ("null".equals(obj) || "NULL".equals(obj))
            {
                return null;
            } else
            {
                return Integer.valueOf(Integer.parseInt(((String) (obj))));
            }
        } else
        {
            throw new JSONException((new StringBuilder()).append("can not cast to int, value : ").append(obj).toString());
        }
    }

    public static Object castToJavaBean(Object obj, Class class1)
    {
        return cast(obj, class1, ParserConfig.getGlobalInstance());
    }

    public static Object castToJavaBean(Map map, Class class1, ParserConfig parserconfig)
    {
        Object obj1;
        int i = 0;
        if (class1 != java/lang/StackTraceElement)
        {
            break MISSING_BLOCK_LABEL_88;
        }
        Object obj;
        try
        {
            class1 = (String)map.get("className");
            parserconfig = (String)map.get("methodName");
            obj = (String)map.get("fileName");
            map = (Number)map.get("lineNumber");
        }
        // Misplaced declaration of an exception variable
        catch (Map map)
        {
            throw new JSONException(map.getMessage(), map);
        }
        if (map != null)
        {
            i = map.intValue();
        }
        return new StackTraceElement(class1, parserconfig, ((String) (obj)), i);
        obj = map.get(JSON.DEFAULT_TYPE_KEY);
        if (!(obj instanceof String))
        {
            break MISSING_BLOCK_LABEL_180;
        }
        obj = (String)obj;
        obj1 = loadClass(((String) (obj)));
        if (obj1 != null)
        {
            break MISSING_BLOCK_LABEL_163;
        }
        throw new ClassNotFoundException((new StringBuilder()).append(((String) (obj))).append(" not found").toString());
        if (!obj1.equals(class1))
        {
            return castToJavaBean(map, ((Class) (obj1)), parserconfig);
        }
        if (class1.isInterface())
        {
            if (map instanceof JSONObject)
            {
                map = (JSONObject)map;
            } else
            {
                map = new JSONObject(map);
            }
            return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[] {
                class1
            }, map);
        }
        ParserConfig parserconfig1;
        parserconfig1 = parserconfig;
        if (parserconfig != null)
        {
            break MISSING_BLOCK_LABEL_240;
        }
        parserconfig1 = ParserConfig.getGlobalInstance();
        parserconfig = parserconfig1.getFieldDeserializers(class1);
        class1 = class1.getDeclaredConstructor(new Class[0]);
        if (!class1.isAccessible())
        {
            class1.setAccessible(true);
        }
        class1 = ((Class) (class1.newInstance(new Object[0])));
        parserconfig = parserconfig.entrySet().iterator();
_L1:
        Object obj2;
        Method method;
        do
        {
            if (!parserconfig.hasNext())
            {
                break MISSING_BLOCK_LABEL_418;
            }
            obj1 = (java.util.Map.Entry)parserconfig.next();
            obj2 = (String)((java.util.Map.Entry) (obj1)).getKey();
            obj1 = (FieldDeserializer)((java.util.Map.Entry) (obj1)).getValue();
        } while (!map.containsKey(obj2));
        obj2 = map.get(obj2);
        method = ((FieldDeserializer) (obj1)).getMethod();
        if (method == null)
        {
            break MISSING_BLOCK_LABEL_395;
        }
        method.invoke(class1, new Object[] {
            cast(obj2, method.getGenericParameterTypes()[0], parserconfig1)
        });
          goto _L1
        ((FieldDeserializer) (obj1)).getField().set(class1, cast(obj2, ((FieldDeserializer) (obj1)).getFieldType(), parserconfig1));
          goto _L1
        return class1;
    }

    public static Long castToLong(Object obj)
    {
        if (obj != null) goto _L2; else goto _L1
_L1:
        return null;
_L2:
        Object obj1;
        if (obj instanceof Number)
        {
            return Long.valueOf(((Number)obj).longValue());
        }
        if (!(obj instanceof String))
        {
            break MISSING_BLOCK_LABEL_112;
        }
        obj1 = (String)obj;
        if (((String) (obj1)).length() == 0 || "null".equals(obj1) || "NULL".equals(obj1)) goto _L1; else goto _L3
_L3:
        long l = Long.parseLong(((String) (obj1)));
        return Long.valueOf(l);
        NumberFormatException numberformatexception;
        numberformatexception;
        JSONScanner jsonscanner = new JSONScanner(((String) (obj1)));
        if (jsonscanner.scanISO8601DateIfMatch(false))
        {
            obj1 = jsonscanner.getCalendar();
        } else
        {
            obj1 = null;
        }
        jsonscanner.close();
        if (obj1 != null)
        {
            return Long.valueOf(((Calendar) (obj1)).getTimeInMillis());
        }
        throw new JSONException((new StringBuilder()).append("can not cast to long, value : ").append(obj).toString());
    }

    public static Short castToShort(Object obj)
    {
        if (obj != null)
        {
            if (obj instanceof Number)
            {
                return Short.valueOf(((Number)obj).shortValue());
            }
            if (obj instanceof String)
            {
                obj = (String)obj;
                if (((String) (obj)).length() != 0 && !"null".equals(obj) && !"NULL".equals(obj))
                {
                    return Short.valueOf(Short.parseShort(((String) (obj))));
                }
            } else
            {
                throw new JSONException((new StringBuilder()).append("can not cast to short, value : ").append(obj).toString());
            }
        }
        return null;
    }

    public static java.sql.Date castToSqlDate(Object obj)
    {
        if (obj == null)
        {
            return null;
        }
        if (obj instanceof Calendar)
        {
            return new java.sql.Date(((Calendar)obj).getTimeInMillis());
        }
        if (obj instanceof java.sql.Date)
        {
            return (java.sql.Date)obj;
        }
        if (obj instanceof Date)
        {
            return new java.sql.Date(((Date)obj).getTime());
        }
        long l;
        if (obj instanceof Number)
        {
            l = ((Number)obj).longValue();
        } else
        {
            l = 0L;
        }
        if (obj instanceof String)
        {
            String s = (String)obj;
            if (s.length() == 0)
            {
                return null;
            }
            l = Long.parseLong(s);
        }
        if (l <= 0L)
        {
            throw new JSONException((new StringBuilder()).append("can not cast to Date, value : ").append(obj).toString());
        } else
        {
            return new java.sql.Date(l);
        }
    }

    public static String castToString(Object obj)
    {
        if (obj == null)
        {
            return null;
        } else
        {
            return obj.toString();
        }
    }

    public static Timestamp castToTimestamp(Object obj)
    {
        if (obj == null)
        {
            return null;
        }
        if (obj instanceof Calendar)
        {
            return new Timestamp(((Calendar)obj).getTimeInMillis());
        }
        if (obj instanceof Timestamp)
        {
            return (Timestamp)obj;
        }
        if (obj instanceof Date)
        {
            return new Timestamp(((Date)obj).getTime());
        }
        long l;
        if (obj instanceof Number)
        {
            l = ((Number)obj).longValue();
        } else
        {
            l = 0L;
        }
        if (obj instanceof String)
        {
            String s = (String)obj;
            if (s.length() == 0)
            {
                return null;
            }
            l = Long.parseLong(s);
        }
        if (l <= 0L)
        {
            throw new JSONException((new StringBuilder()).append("can not cast to Date, value : ").append(obj).toString());
        } else
        {
            return new Timestamp(l);
        }
    }

    public static void clearClassMapping()
    {
        mappings.clear();
        addBaseClassMappings();
    }

    public static List computeGetters(Class class1, Map map)
    {
        return computeGetters(class1, map, true);
    }

    public static List computeGetters(Class class1, Map map, boolean flag)
    {
        LinkedHashMap linkedhashmap;
        Method amethod[];
        int k;
        int l1;
        linkedhashmap = new LinkedHashMap();
        amethod = class1.getMethods();
        l1 = amethod.length;
        k = 0;
_L3:
        String s1;
        Object obj3;
        int i;
        int j;
        if (k >= l1)
        {
            break; /* Loop/switch isn't completed */
        }
        obj3 = amethod[k];
        s1 = ((Method) (obj3)).getName();
        j = 0;
        i = 0;
          goto _L1
_L5:
        k++;
        if (true) goto _L3; else goto _L2
_L1:
        if (Modifier.isStatic(((Method) (obj3)).getModifiers()) || ((Method) (obj3)).getReturnType().equals(Void.TYPE) || ((Method) (obj3)).getParameterTypes().length != 0 || ((Method) (obj3)).getReturnType() == java/lang/ClassLoader || ((Method) (obj3)).getName().equals("getMetaClass") && ((Method) (obj3)).getReturnType().getName().equals("groovy.lang.MetaClass")) goto _L5; else goto _L4
_L4:
        Object obj;
        obj = (JSONField)((Method) (obj3)).getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        if (obj == null)
        {
            obj = getSupperMethodAnnotation(class1, ((Method) (obj3)));
        }
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_1551;
        }
        if (!((JSONField) (obj)).serialize()) goto _L5; else goto _L6
_L6:
        String s;
        Object obj1;
        int l;
        int i1;
        i1 = ((JSONField) (obj)).ordinal();
        l = SerializerFeature.of(((JSONField) (obj)).serialzeFeatures());
        if (((JSONField) (obj)).name().length() == 0)
        {
            break MISSING_BLOCK_LABEL_281;
        }
        obj1 = ((JSONField) (obj)).name();
        s = ((String) (obj1));
        if (map == null) goto _L8; else goto _L7
_L7:
        s = (String)map.get(obj1);
        if (s == null) goto _L5; else goto _L8
_L8:
        linkedhashmap.put(s, new FieldInfo(s, ((Method) (obj3)), null, i1, l, ((JSONField) (obj)).label()));
          goto _L5
        j = i1;
        i = l;
        if (((JSONField) (obj)).label().length() == 0)
        {
            break MISSING_BLOCK_LABEL_1551;
        }
        s = ((JSONField) (obj)).label();
        j = i1;
        i = l;
_L44:
        if (!s1.startsWith("get")) goto _L10; else goto _L9
_L9:
        if (s1.length() < 4 || s1.equals("getClass")) goto _L5; else goto _L11
_L11:
        char c;
        c = s1.charAt(3);
        Field field;
        JSONField jsonfield;
        int j1;
        if (Character.isUpperCase(c))
        {
            if (compatibleWithJavaBean)
            {
                obj = decapitalize(s1.substring(3));
            } else
            {
                obj = (new StringBuilder()).append(Character.toLowerCase(s1.charAt(3))).append(s1.substring(4)).toString();
            }
        } else
        if (c == '_')
        {
            obj = s1.substring(4);
        } else
        {
            if (c != 'f')
            {
                continue; /* Loop/switch isn't completed */
            }
            obj = s1.substring(3);
        }
_L27:
        if (isJSONTypeIgnore(class1, ((String) (obj)))) goto _L5; else goto _L12
_L12:
        field = ParserConfig.getField(class1, ((String) (obj)));
        if (field == null)
        {
            break MISSING_BLOCK_LABEL_522;
        }
        jsonfield = (JSONField)field.getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        if (jsonfield == null)
        {
            break MISSING_BLOCK_LABEL_522;
        }
        if (!jsonfield.serialize()) goto _L5; else goto _L13
_L13:
        j = jsonfield.ordinal();
        i = SerializerFeature.of(jsonfield.serialzeFeatures());
        if (jsonfield.name().length() == 0) goto _L15; else goto _L14
_L14:
        obj1 = jsonfield.name();
        obj = obj1;
        if (map == null) goto _L15; else goto _L16
_L16:
        obj = (String)map.get(obj1);
        if (obj == null) goto _L5; else goto _L15
_L15:
        if (jsonfield.label().length() != 0)
        {
            s = jsonfield.label();
        }
        obj1 = obj;
        if (map == null) goto _L18; else goto _L17
_L17:
        obj1 = (String)map.get(obj);
        if (obj1 == null) goto _L5; else goto _L18
_L18:
        linkedhashmap.put(obj1, new FieldInfo(((String) (obj1)), ((Method) (obj3)), field, j, i, s));
_L10:
        if (!s1.startsWith("is") || s1.length() < 3) goto _L5; else goto _L19
_L19:
        c = s1.charAt(2);
        if (Character.isUpperCase(c))
        {
            if (compatibleWithJavaBean)
            {
                obj = decapitalize(s1.substring(2));
            } else
            {
                obj = (new StringBuilder()).append(Character.toLowerCase(s1.charAt(2))).append(s1.substring(3)).toString();
            }
        } else
        {
            if (c != '_')
            {
                continue; /* Loop/switch isn't completed */
            }
            obj = s1.substring(3);
        }
_L29:
        obj1 = ParserConfig.getField(class1, ((String) (obj)));
        field = ((Field) (obj1));
        if (obj1 == null)
        {
            field = ParserConfig.getField(class1, s1);
        }
        obj1 = obj;
        l = j;
        j1 = i;
        s1 = s;
        if (field == null)
        {
            break MISSING_BLOCK_LABEL_837;
        }
        jsonfield = (JSONField)field.getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        obj1 = obj;
        l = j;
        j1 = i;
        s1 = s;
        if (jsonfield == null)
        {
            break MISSING_BLOCK_LABEL_837;
        }
        if (!jsonfield.serialize()) goto _L5; else goto _L20
_L20:
        i = jsonfield.ordinal();
        j = SerializerFeature.of(jsonfield.serialzeFeatures());
        if (jsonfield.name().length() == 0) goto _L22; else goto _L21
_L21:
        obj1 = jsonfield.name();
        obj = obj1;
        if (map == null) goto _L22; else goto _L23
_L23:
        obj = (String)map.get(obj1);
        if (obj == null) goto _L5; else goto _L22
_L22:
        obj1 = obj;
        l = i;
        j1 = j;
        s1 = s;
        if (jsonfield.label().length() != 0)
        {
            s1 = jsonfield.label();
            j1 = j;
            l = i;
            obj1 = obj;
        }
        obj = obj1;
        if (map == null) goto _L25; else goto _L24
_L24:
        obj = (String)map.get(obj1);
        if (obj == null) goto _L5; else goto _L25
_L25:
        linkedhashmap.put(obj, new FieldInfo(((String) (obj)), ((Method) (obj3)), field, l, j1, s1));
          goto _L5
        if (s1.length() < 5 || !Character.isUpperCase(s1.charAt(4))) goto _L5; else goto _L26
_L26:
        obj = decapitalize(s1.substring(3));
          goto _L27
        if (c != 'f') goto _L5; else goto _L28
_L28:
        obj = s1.substring(2);
          goto _L29
_L2:
        Field afield[];
        afield = class1.getFields();
        l1 = afield.length;
        i = 0;
_L31:
        Field field1;
        if (i >= l1)
        {
            break MISSING_BLOCK_LABEL_1332;
        }
        field1 = afield[i];
        if (!Modifier.isStatic(field1.getModifiers()))
        {
            break; /* Loop/switch isn't completed */
        }
_L33:
        i++;
        if (true) goto _L31; else goto _L30
_L30:
        Object obj2;
        obj3 = (JSONField)field1.getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        j = 0;
        k = 0;
        s = field1.getName();
        obj2 = null;
        obj = s;
        obj1 = obj2;
        if (obj3 == null)
        {
            break MISSING_BLOCK_LABEL_1263;
        }
        if (!((JSONField) (obj3)).serialize()) goto _L33; else goto _L32
_L32:
        l = ((JSONField) (obj3)).ordinal();
        int k1 = SerializerFeature.of(((JSONField) (obj3)).serialzeFeatures());
        if (((JSONField) (obj3)).name().length() != 0)
        {
            s = ((JSONField) (obj3)).name();
        }
        obj = s;
        j = l;
        k = k1;
        obj1 = obj2;
        if (((JSONField) (obj3)).label().length() != 0)
        {
            obj1 = ((JSONField) (obj3)).label();
            k = k1;
            j = l;
            obj = s;
        }
        s = ((String) (obj));
        if (map == null) goto _L35; else goto _L34
_L34:
        s = (String)map.get(obj);
        if (s == null) goto _L33; else goto _L35
_L35:
        if (!linkedhashmap.containsKey(s))
        {
            linkedhashmap.put(s, new FieldInfo(s, null, field1, j, k, ((String) (obj1))));
        }
          goto _L33
        map = new ArrayList();
        class1 = (JSONType)class1.getAnnotation(com/alibaba/fastjson/annotation/JSONType);
        if (class1 == null)
        {
            break MISSING_BLOCK_LABEL_1531;
        }
        class1 = class1.orders();
        if (class1 == null || class1.length != linkedhashmap.size()) goto _L37; else goto _L36
_L36:
        k = 1;
        l = class1.length;
        j = 0;
_L42:
        i = k;
        if (j >= l) goto _L39; else goto _L38
_L38:
        if (linkedhashmap.containsKey(class1[j])) goto _L41; else goto _L40
_L40:
        i = 0;
          goto _L39
_L41:
        j++;
          goto _L42
_L37:
        i = 0;
_L39:
        if (i != 0)
        {
            j = class1.length;
            for (i = 0; i < j; i++)
            {
                map.add((FieldInfo)linkedhashmap.get(class1[i]));
            }

        } else
        {
            for (class1 = linkedhashmap.values().iterator(); class1.hasNext(); map.add((FieldInfo)class1.next())) { }
            if (flag)
            {
                Collections.sort(map);
            }
        }
        return map;
        i = 0;
        class1 = null;
        if (true) goto _L39; else goto _L43
_L43:
        s = null;
          goto _L44
    }

    public static String decapitalize(String s)
    {
        while (s == null || s.length() == 0 || s.length() > 1 && Character.isUpperCase(s.charAt(1)) && Character.isUpperCase(s.charAt(0))) 
        {
            return s;
        }
        s = s.toCharArray();
        s[0] = Character.toLowerCase(s[0]);
        return new String(s);
    }

    public static Class getClass(Type type)
    {
        if (type.getClass() == java/lang/Class)
        {
            return (Class)type;
        }
        if (type instanceof ParameterizedType)
        {
            return getClass(((ParameterizedType)type).getRawType());
        } else
        {
            return java/lang/Object;
        }
    }

    public static Class getCollectionItemClass(Type type)
    {
        if (type instanceof ParameterizedType)
        {
            type = ((ParameterizedType)type).getActualTypeArguments()[0];
            if (type instanceof Class)
            {
                Class class1 = (Class)type;
                type = class1;
                if (!Modifier.isPublic(class1.getModifiers()))
                {
                    throw new ASMException("can not create ASMParser");
                }
            } else
            {
                throw new ASMException("can not create ASMParser");
            }
        } else
        {
            type = java/lang/Object;
        }
        return type;
    }

    public static Field getField(Class class1, String s)
    {
        Field afield[] = class1.getDeclaredFields();
        int j = afield.length;
        for (int i = 0; i < j; i++)
        {
            Field field = afield[i];
            if (s.equals(field.getName()))
            {
                return field;
            }
        }

        class1 = class1.getSuperclass();
        if (class1 != null && class1 != java/lang/Object)
        {
            return getField(class1, s);
        } else
        {
            return null;
        }
    }

    public static Type getGenericParamType(Type type)
    {
        while ((type instanceof ParameterizedType) || !(type instanceof Class)) 
        {
            return type;
        }
        return getGenericParamType(((Class)type).getGenericSuperclass());
    }

    public static JSONType getJSONType(Class class1)
    {
        return (JSONType)class1.getAnnotation(com/alibaba/fastjson/annotation/JSONType);
    }

    public static int getParserFeatures(Class class1)
    {
        class1 = (JSONType)class1.getAnnotation(com/alibaba/fastjson/annotation/JSONType);
        if (class1 == null)
        {
            return 0;
        } else
        {
            return Feature.of(class1.parseFeatures());
        }
    }

    public static int getSerializeFeatures(Class class1)
    {
        class1 = (JSONType)class1.getAnnotation(com/alibaba/fastjson/annotation/JSONType);
        if (class1 == null)
        {
            return 0;
        } else
        {
            return SerializerFeature.of(class1.serialzeFeatures());
        }
    }

    public static JSONField getSupperMethodAnnotation(Class class1, Method method)
    {
        int i;
        int l;
        class1 = class1.getInterfaces();
        l = class1.length;
        i = 0;
_L11:
        if (i >= l) goto _L2; else goto _L1
_L1:
        Method amethod[];
        int j;
        int i1;
        amethod = class1[i].getMethods();
        i1 = amethod.length;
        j = 0;
_L5:
        Object obj;
        if (j >= i1)
        {
            continue; /* Loop/switch isn't completed */
        }
        obj = amethod[j];
          goto _L3
_L7:
        j++;
        if (true) goto _L5; else goto _L4
_L4:
        continue; /* Loop/switch isn't completed */
_L3:
        if (!((Method) (obj)).getName().equals(method.getName()) || ((Method) (obj)).getParameterTypes().length != method.getParameterTypes().length) goto _L7; else goto _L6
_L6:
        int k = 0;
_L10:
        if (k >= ((Method) (obj)).getParameterTypes().length)
        {
            break MISSING_BLOCK_LABEL_160;
        }
        if (((Method) (obj)).getParameterTypes()[k].equals(method.getParameterTypes()[k])) goto _L9; else goto _L8
_L8:
        k = 0;
_L12:
        if (k != 0)
        {
            obj = (JSONField)((Method) (obj)).getAnnotation(com/alibaba/fastjson/annotation/JSONField);
            if (obj != null)
            {
                return ((JSONField) (obj));
            }
        }
          goto _L7
_L9:
        k++;
          goto _L10
        i++;
          goto _L11
_L2:
        return null;
        k = 1;
          goto _L12
    }

    public static boolean isGenericParamType(Type type)
    {
        if (type instanceof ParameterizedType)
        {
            return true;
        }
        if (type instanceof Class)
        {
            return isGenericParamType(((Class)type).getGenericSuperclass());
        } else
        {
            return false;
        }
    }

    private static boolean isJSONTypeIgnore(Class class1, String s)
    {
        JSONType jsontype = (JSONType)class1.getAnnotation(com/alibaba/fastjson/annotation/JSONType);
        if (jsontype == null) goto _L2; else goto _L1
_L1:
        String as1[] = jsontype.includes();
        if (as1.length <= 0) goto _L4; else goto _L3
_L3:
        int i = 0;
_L11:
        if (i >= as1.length) goto _L6; else goto _L5
_L5:
        if (!s.equals(as1[i])) goto _L8; else goto _L7
_L7:
        return false;
_L8:
        i++;
        continue; /* Loop/switch isn't completed */
_L6:
        return true;
_L4:
        String as[] = jsontype.ignores();
        for (int j = 0; j < as.length; j++)
        {
            if (s.equals(as[j]))
            {
                return true;
            }
        }

_L2:
        if (class1.getSuperclass() == java/lang/Object || class1.getSuperclass() == null || !isJSONTypeIgnore(class1.getSuperclass(), s)) goto _L7; else goto _L9
_L9:
        return true;
        if (true) goto _L11; else goto _L10
_L10:
    }

    public static Class loadClass(String s)
    {
        if (s != null && s.length() != 0) goto _L2; else goto _L1
_L1:
        Class class3 = null;
_L4:
        return class3;
_L2:
        Class class1;
        class1 = (Class)mappings.get(s);
        class3 = class1;
        if (class1 != null) goto _L4; else goto _L3
_L3:
        if (s.charAt(0) == '[')
        {
            return Array.newInstance(loadClass(s.substring(1)), 0).getClass();
        }
        if (s.startsWith("L") && s.endsWith(";"))
        {
            return loadClass(s.substring(1, s.length() - 1));
        }
        class3 = class1;
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        class3 = class1;
        if (classloader == null)
        {
            break MISSING_BLOCK_LABEL_128;
        }
        class3 = class1;
        class1 = classloader.loadClass(s);
        class3 = class1;
        addClassMapping(s, class1);
        return class1;
        Throwable throwable;
        throwable;
        Class class2;
        try
        {
            class2 = Class.forName(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return class3;
        }
        class3 = class2;
        addClassMapping(s, class2);
        return class2;
    }

    static void setAccessible(AccessibleObject accessibleobject)
    {
        while (!setAccessibleEnable || accessibleobject.isAccessible()) 
        {
            return;
        }
        try
        {
            accessibleobject.setAccessible(true);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (AccessibleObject accessibleobject)
        {
            setAccessibleEnable = false;
        }
    }

    public static Type unwrap(Type type)
    {
        Object obj = type;
        if (type instanceof GenericArrayType)
        {
            Type type1 = ((GenericArrayType)type).getGenericComponentType();
            if (type1 == Byte.TYPE)
            {
                obj = [B;
            } else
            {
                obj = type;
                if (type1 == Character.TYPE)
                {
                    return [C;
                }
            }
        }
        return ((Type) (obj));
    }

    static 
    {
        compatibleWithJavaBean = false;
        setAccessibleEnable = true;
        String s = System.getProperty("fastjson.compatibleWithJavaBean");
        if (!"true".equals(s)) goto _L2; else goto _L1
_L1:
_L4:
        mappings = new ConcurrentHashMap();
        addBaseClassMappings();
        return;
_L2:
        try
        {
            if ("false".equals(s))
            {
                compatibleWithJavaBean = false;
            }
        }
        catch (Throwable throwable) { }
        if (true) goto _L4; else goto _L3
_L3:
    }
}
