// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;


public class JSONException extends RuntimeException
{

    private static final long serialVersionUID = 1L;

    public JSONException()
    {
    }

    public JSONException(String s)
    {
        super(s);
    }

    public JSONException(String s, Throwable throwable)
    {
        super(s, throwable);
    }
}
