// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.SerializeWriter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;

// Referenced classes of package com.alibaba.fastjson:
//            JSONStreamContext, JSONException

public class JSONWriter
    implements Closeable, Flushable
{

    private JSONStreamContext context;
    private JSONSerializer serializer;
    private SerializeWriter writer;

    public JSONWriter(Writer writer1)
    {
        writer = new SerializeWriter(writer1);
        serializer = new JSONSerializer(writer);
    }

    private void afterWriter()
    {
        if (context != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        context.getState();
        JVM INSTR tableswitch 1001 1005: default 48
    //                   1001 71
    //                   1002 64
    //                   1003 71
    //                   1004 78
    //                   1005 85;
           goto _L3 _L4 _L5 _L4 _L6 _L7
_L7:
        break MISSING_BLOCK_LABEL_85;
_L5:
        break; /* Loop/switch isn't completed */
_L3:
        char c = '\uFFFF';
_L9:
        if (c != -1)
        {
            context.setState(c);
            return;
        }
        if (true) goto _L1; else goto _L8
_L8:
        c = '\u03EB';
          goto _L9
_L4:
        c = '\u03EA';
          goto _L9
_L6:
        c = '\u03ED';
          goto _L9
        c = '\uFFFF';
          goto _L9
    }

    private void beforeWrite()
    {
        if (context != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        switch (context.getState())
        {
        default:
            return;

        case 1001: 
        case 1004: 
            break;

        case 1002: 
            writer.write(':');
            return;

        case 1003: 
            writer.write(',');
            return;

        case 1005: 
            writer.write(',');
            break; /* Loop/switch isn't completed */
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    private void beginStructure()
    {
        int i = context.getState();
        switch (i)
        {
        case 1003: 
        default:
            throw new JSONException((new StringBuilder()).append("illegal state : ").append(i).toString());

        case 1002: 
            writer.write(':');
            // fall through

        case 1001: 
        case 1004: 
            return;

        case 1005: 
            writer.write(',');
            break;
        }
    }

    private void endStructure()
    {
        context = context.getParent();
        if (context != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        context.getState();
        JVM INSTR tableswitch 1001 1005: default 60
    //                   1001 95
    //                   1002 76
    //                   1003 60
    //                   1004 83
    //                   1005 90;
           goto _L3 _L4 _L5 _L3 _L6 _L7
_L4:
        break MISSING_BLOCK_LABEL_95;
_L5:
        break; /* Loop/switch isn't completed */
_L3:
        char c = '\uFFFF';
_L9:
        if (c != -1)
        {
            context.setState(c);
            return;
        }
        if (true) goto _L1; else goto _L8
_L8:
        c = '\u03EB';
          goto _L9
_L6:
        c = '\u03ED';
          goto _L9
_L7:
        c = '\uFFFF';
          goto _L9
        c = '\u03EA';
          goto _L9
    }

    public void close()
        throws IOException
    {
        writer.close();
    }

    public void config(SerializerFeature serializerfeature, boolean flag)
    {
        writer.config(serializerfeature, flag);
    }

    public void endArray()
    {
        writer.write(']');
        endStructure();
    }

    public void endObject()
    {
        writer.write('}');
        endStructure();
    }

    public void flush()
        throws IOException
    {
        writer.flush();
    }

    public void startArray()
    {
        if (context != null)
        {
            beginStructure();
        }
        context = new JSONStreamContext(context, 1004);
        writer.write('[');
    }

    public void startObject()
    {
        if (context != null)
        {
            beginStructure();
        }
        context = new JSONStreamContext(context, 1001);
        writer.write('{');
    }

    public void writeEndArray()
    {
        endArray();
    }

    public void writeEndObject()
    {
        endObject();
    }

    public void writeKey(String s)
    {
        writeObject(s);
    }

    public void writeObject(Object obj)
    {
        beforeWrite();
        serializer.write(obj);
        afterWriter();
    }

    public void writeObject(String s)
    {
        beforeWrite();
        serializer.write(s);
        afterWriter();
    }

    public void writeStartArray()
    {
        startArray();
    }

    public void writeStartObject()
    {
        startObject();
    }

    public void writeValue(Object obj)
    {
        writeObject(obj);
    }
}
