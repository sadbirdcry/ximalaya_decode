// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;

import com.alibaba.fastjson.serializer.JSONSerializable;
import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.SerializeWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.alibaba.fastjson:
//            JSON

public class JSONPObject
    implements JSONSerializable
{

    private String function;
    private final List parameters;

    public JSONPObject()
    {
        parameters = new ArrayList();
    }

    public JSONPObject(String s)
    {
        parameters = new ArrayList();
        function = s;
    }

    public void addParameter(Object obj)
    {
        parameters.add(obj);
    }

    public String getFunction()
    {
        return function;
    }

    public List getParameters()
    {
        return parameters;
    }

    public void setFunction(String s)
    {
        function = s;
    }

    public String toJSONString()
    {
        return null;
    }

    public String toString()
    {
        return JSON.toJSONString(this);
    }

    public void write(JSONSerializer jsonserializer, Object obj, Type type, int i)
        throws IOException
    {
        obj = jsonserializer.getWriter();
        ((SerializeWriter) (obj)).write(function);
        ((SerializeWriter) (obj)).write('(');
        for (i = 0; i < parameters.size(); i++)
        {
            if (i != 0)
            {
                ((SerializeWriter) (obj)).write(',');
            }
            jsonserializer.write(parameters.get(i));
        }

        ((SerializeWriter) (obj)).write(')');
    }
}
