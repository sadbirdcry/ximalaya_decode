// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.alibaba.fastjson;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.alibaba.fastjson:
//            JSON, JSONArray, JSONException

public class JSONObject extends JSON
    implements Serializable, Cloneable, InvocationHandler, Map
{

    private static final int DEFAULT_INITIAL_CAPACITY = 16;
    private static final long serialVersionUID = 1L;
    private final Map map;

    public JSONObject()
    {
        this(16, false);
    }

    public JSONObject(int i)
    {
        this(i, false);
    }

    public JSONObject(int i, boolean flag)
    {
        if (flag)
        {
            map = new LinkedHashMap(i);
            return;
        } else
        {
            map = new HashMap(i);
            return;
        }
    }

    public JSONObject(Map map1)
    {
        map = map1;
    }

    public JSONObject(boolean flag)
    {
        this(16, flag);
    }

    public void clear()
    {
        map.clear();
    }

    public Object clone()
    {
        Object obj;
        if (map instanceof LinkedHashMap)
        {
            obj = new LinkedHashMap(map);
        } else
        {
            obj = new HashMap(map);
        }
        return new JSONObject(((Map) (obj)));
    }

    public boolean containsKey(Object obj)
    {
        return map.containsKey(obj);
    }

    public boolean containsValue(Object obj)
    {
        return map.containsValue(obj);
    }

    public Set entrySet()
    {
        return map.entrySet();
    }

    public boolean equals(Object obj)
    {
        return map.equals(obj);
    }

    public Object get(Object obj)
    {
        return map.get(obj);
    }

    public BigDecimal getBigDecimal(String s)
    {
        return TypeUtils.castToBigDecimal(get(s));
    }

    public BigInteger getBigInteger(String s)
    {
        return TypeUtils.castToBigInteger(get(s));
    }

    public Boolean getBoolean(String s)
    {
        s = ((String) (get(s)));
        if (s == null)
        {
            return null;
        } else
        {
            return TypeUtils.castToBoolean(s);
        }
    }

    public boolean getBooleanValue(String s)
    {
        s = ((String) (get(s)));
        if (s == null)
        {
            return false;
        } else
        {
            return TypeUtils.castToBoolean(s).booleanValue();
        }
    }

    public Byte getByte(String s)
    {
        return TypeUtils.castToByte(get(s));
    }

    public byte getByteValue(String s)
    {
        s = ((String) (get(s)));
        if (s == null)
        {
            return 0;
        } else
        {
            return TypeUtils.castToByte(s).byteValue();
        }
    }

    public byte[] getBytes(String s)
    {
        s = ((String) (get(s)));
        if (s == null)
        {
            return null;
        } else
        {
            return TypeUtils.castToBytes(s);
        }
    }

    public Date getDate(String s)
    {
        return TypeUtils.castToDate(get(s));
    }

    public Double getDouble(String s)
    {
        return TypeUtils.castToDouble(get(s));
    }

    public double getDoubleValue(String s)
    {
        s = ((String) (get(s)));
        if (s == null)
        {
            return 0.0D;
        } else
        {
            return TypeUtils.castToDouble(s).doubleValue();
        }
    }

    public Float getFloat(String s)
    {
        return TypeUtils.castToFloat(get(s));
    }

    public float getFloatValue(String s)
    {
        s = ((String) (get(s)));
        if (s == null)
        {
            return 0.0F;
        } else
        {
            return TypeUtils.castToFloat(s).floatValue();
        }
    }

    public int getIntValue(String s)
    {
        s = ((String) (get(s)));
        if (s == null)
        {
            return 0;
        } else
        {
            return TypeUtils.castToInt(s).intValue();
        }
    }

    public Integer getInteger(String s)
    {
        return TypeUtils.castToInt(get(s));
    }

    public JSONArray getJSONArray(String s)
    {
        s = ((String) (map.get(s)));
        if (s instanceof JSONArray)
        {
            return (JSONArray)s;
        } else
        {
            return (JSONArray)toJSON(s);
        }
    }

    public JSONObject getJSONObject(String s)
    {
        s = ((String) (map.get(s)));
        if (s instanceof JSONObject)
        {
            return (JSONObject)s;
        } else
        {
            return (JSONObject)toJSON(s);
        }
    }

    public Long getLong(String s)
    {
        return TypeUtils.castToLong(get(s));
    }

    public long getLongValue(String s)
    {
        s = ((String) (get(s)));
        if (s == null)
        {
            return 0L;
        } else
        {
            return TypeUtils.castToLong(s).longValue();
        }
    }

    public Object getObject(String s, Class class1)
    {
        return TypeUtils.castToJavaBean(map.get(s), class1);
    }

    public Short getShort(String s)
    {
        return TypeUtils.castToShort(get(s));
    }

    public short getShortValue(String s)
    {
        s = ((String) (get(s)));
        if (s == null)
        {
            return 0;
        } else
        {
            return TypeUtils.castToShort(s).shortValue();
        }
    }

    public java.sql.Date getSqlDate(String s)
    {
        return TypeUtils.castToSqlDate(get(s));
    }

    public String getString(String s)
    {
        s = ((String) (get(s)));
        if (s == null)
        {
            return null;
        } else
        {
            return s.toString();
        }
    }

    public Timestamp getTimestamp(String s)
    {
        return TypeUtils.castToTimestamp(get(s));
    }

    public int hashCode()
    {
        return map.hashCode();
    }

    public Object invoke(Object obj, Method method, Object aobj[])
        throws Throwable
    {
        obj = method.getParameterTypes();
        if (obj.length == 1)
        {
            if (method.getName().equals("equals"))
            {
                return Boolean.valueOf(equals(aobj[0]));
            }
            if (method.getReturnType() != Void.TYPE)
            {
                throw new JSONException("illegal setter");
            }
            obj = (JSONField)method.getAnnotation(com/alibaba/fastjson/annotation/JSONField);
            Object obj1;
            if (obj != null && ((JSONField) (obj)).name().length() != 0)
            {
                obj = ((JSONField) (obj)).name();
            } else
            {
                obj = null;
            }
            obj1 = obj;
            if (obj == null)
            {
                obj = method.getName();
                if (!((String) (obj)).startsWith("set"))
                {
                    throw new JSONException("illegal setter");
                }
                obj = ((String) (obj)).substring(3);
                if (((String) (obj)).length() == 0)
                {
                    throw new JSONException("illegal setter");
                }
                obj1 = (new StringBuilder()).append(Character.toLowerCase(((String) (obj)).charAt(0))).append(((String) (obj)).substring(1)).toString();
            }
            map.put(obj1, aobj[0]);
            return null;
        }
        if (obj.length != 0)
        {
            break MISSING_BLOCK_LABEL_461;
        }
        if (method.getReturnType() == Void.TYPE)
        {
            throw new JSONException("illegal getter");
        }
        obj = (JSONField)method.getAnnotation(com/alibaba/fastjson/annotation/JSONField);
        if (obj != null && ((JSONField) (obj)).name().length() != 0)
        {
            obj = ((JSONField) (obj)).name();
        } else
        {
            obj = null;
        }
        aobj = ((Object []) (obj));
        if (obj != null) goto _L2; else goto _L1
_L1:
        obj = method.getName();
        if (!((String) (obj)).startsWith("get")) goto _L4; else goto _L3
_L3:
        obj = ((String) (obj)).substring(3);
        if (((String) (obj)).length() == 0)
        {
            throw new JSONException("illegal getter");
        }
        aobj = (new StringBuilder()).append(Character.toLowerCase(((String) (obj)).charAt(0))).append(((String) (obj)).substring(1)).toString();
_L2:
        return TypeUtils.cast(map.get(((Object) (aobj))), method.getGenericReturnType(), ParserConfig.getGlobalInstance());
_L4:
        if (!((String) (obj)).startsWith("is"))
        {
            break; /* Loop/switch isn't completed */
        }
        obj = ((String) (obj)).substring(2);
        if (((String) (obj)).length() == 0)
        {
            throw new JSONException("illegal getter");
        }
        aobj = (new StringBuilder()).append(Character.toLowerCase(((String) (obj)).charAt(0))).append(((String) (obj)).substring(1)).toString();
        if (true) goto _L2; else goto _L5
_L5:
        if (((String) (obj)).startsWith("hashCode"))
        {
            return Integer.valueOf(hashCode());
        }
        if (((String) (obj)).startsWith("toString"))
        {
            return toString();
        } else
        {
            throw new JSONException("illegal getter");
        }
        throw new UnsupportedOperationException(method.toGenericString());
    }

    public boolean isEmpty()
    {
        return map.isEmpty();
    }

    public Set keySet()
    {
        return map.keySet();
    }

    public volatile Object put(Object obj, Object obj1)
    {
        return put((String)obj, obj1);
    }

    public Object put(String s, Object obj)
    {
        return map.put(s, obj);
    }

    public void putAll(Map map1)
    {
        map.putAll(map1);
    }

    public Object remove(Object obj)
    {
        return map.remove(obj);
    }

    public int size()
    {
        return map.size();
    }

    public Collection values()
    {
        return map.values();
    }
}
