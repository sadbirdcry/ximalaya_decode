// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.sina.weibo.sdk.auth.sso;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.text.TextUtils;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuth;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.exception.WeiboDialogException;
import com.sina.weibo.sdk.utils.LogUtil;
import com.sina.weibo.sdk.utils.SecurityHelper;
import java.util.Iterator;
import java.util.List;

public class SsoHandler
{

    private static final String DEFAULT_SINA_WEIBO_PACKAGE_NAME = "com.sina.weibo";
    private static final String DEFAULT_WEIBO_REMOTE_SSO_SERVICE_NAME = "com.sina.weibo.remotessoservice";
    private static final int REQUEST_CODE_SSO_AUTH = 32973;
    private static final String TAG = "Weibo_SSO_login";
    private Activity mAuthActivity;
    private WeiboAuthListener mAuthListener;
    private ServiceConnection mConnection;
    private int mSSOAuthRequestCode;
    private WeiboAuth mWeiboAuth;

    public SsoHandler(Activity activity, WeiboAuth weiboauth)
    {
        mConnection = new _cls1();
        mAuthActivity = activity;
        mWeiboAuth = weiboauth;
    }

    private boolean bindRemoteSSOService(Context context, String s)
    {
        String s1;
        boolean flag;
label0:
        {
            flag = true;
            if (!TextUtils.isEmpty(s))
            {
                s1 = s;
                if (!s.trim().equals(""))
                {
                    break label0;
                }
            }
            s1 = "com.sina.weibo";
        }
        s = new Intent("com.sina.weibo.remotessoservice");
        s.setPackage(s1);
        if (!context.bindService(s, mConnection, 1))
        {
            flag = context.bindService(new Intent("com.sina.weibo.remotessoservice"), mConnection, 1);
        }
        return flag;
    }

    public static ComponentName isServiceExisted(Context context, String s)
    {
        context = ((ActivityManager)context.getSystemService("activity")).getRunningServices(0x7fffffff).iterator();
        ComponentName componentname;
        do
        {
            if (!context.hasNext())
            {
                return null;
            }
            componentname = ((android.app.ActivityManager.RunningServiceInfo)context.next()).service;
        } while (!componentname.getPackageName().equals(s) || !componentname.getClassName().equals((new StringBuilder(String.valueOf(s))).append(".business.RemoteSSOService").toString()));
        return componentname;
    }

    private boolean startSingleSignOn(String s, String s1)
    {
        boolean flag = true;
        Intent intent = new Intent();
        intent.setClassName(s, s1);
        intent.putExtras(mWeiboAuth.getAuthInfo().getAuthBundle());
        intent.putExtra("_weibo_command_type", 3);
        intent.putExtra("_weibo_transaction", String.valueOf(System.currentTimeMillis()));
        if (!SecurityHelper.validateAppSignatureForIntent(mAuthActivity, intent))
        {
            return false;
        }
        try
        {
            mAuthActivity.startActivityForResult(intent, mSSOAuthRequestCode);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            flag = false;
        }
        return flag;
    }

    public void authorize(int i, WeiboAuthListener weiboauthlistener, String s)
    {
        mSSOAuthRequestCode = i;
        mAuthListener = weiboauthlistener;
        if (!bindRemoteSSOService(mAuthActivity.getApplicationContext(), s) && mWeiboAuth != null)
        {
            mWeiboAuth.anthorize(mAuthListener);
        }
    }

    public void authorize(WeiboAuthListener weiboauthlistener)
    {
        authorize(32973, weiboauthlistener, null);
    }

    public void authorize(WeiboAuthListener weiboauthlistener, String s)
    {
        authorize(32973, weiboauthlistener, s);
    }

    public void authorizeCallBack(int i, int j, Intent intent)
    {
        LogUtil.d("Weibo_SSO_login", (new StringBuilder("requestCode: ")).append(i).append(", resultCode: ").append(j).append(", data: ").append(intent).toString());
        if (i != mSSOAuthRequestCode) goto _L2; else goto _L1
_L1:
        if (j != -1) goto _L4; else goto _L3
_L3:
        if (SecurityHelper.checkResponseAppLegal(mAuthActivity, intent)) goto _L5; else goto _L2
_L2:
        return;
_L5:
        String s = intent.getStringExtra("error");
        Object obj = s;
        if (s == null)
        {
            obj = intent.getStringExtra("error_type");
        }
        if (obj != null)
        {
            if (((String) (obj)).equals("access_denied") || ((String) (obj)).equals("OAuthAccessDeniedException"))
            {
                LogUtil.d("Weibo_SSO_login", "Login canceled by user.");
                mAuthListener.onCancel();
                return;
            }
            String s1 = intent.getStringExtra("error_description");
            intent = ((Intent) (obj));
            if (s1 != null)
            {
                intent = (new StringBuilder(String.valueOf(obj))).append(":").append(s1).toString();
            }
            LogUtil.d("Weibo_SSO_login", (new StringBuilder("Login failed: ")).append(intent).toString());
            mAuthListener.onWeiboException(new WeiboDialogException(intent, j, s1));
            return;
        }
        intent = intent.getExtras();
        obj = Oauth2AccessToken.parseAccessToken(intent);
        if (obj != null && ((Oauth2AccessToken) (obj)).isSessionValid())
        {
            LogUtil.d("Weibo_SSO_login", (new StringBuilder("Login Success! ")).append(((Oauth2AccessToken) (obj)).toString()).toString());
            mAuthListener.onComplete(intent);
            return;
        } else
        {
            LogUtil.d("Weibo_SSO_login", "Failed to receive access token by SSO");
            mWeiboAuth.anthorize(mAuthListener);
            return;
        }
_L4:
        if (j == 0)
        {
            if (intent != null)
            {
                LogUtil.d("Weibo_SSO_login", (new StringBuilder("Login failed: ")).append(intent.getStringExtra("error")).toString());
                mAuthListener.onWeiboException(new WeiboDialogException(intent.getStringExtra("error"), intent.getIntExtra("error_code", -1), intent.getStringExtra("failing_url")));
                return;
            } else
            {
                LogUtil.d("Weibo_SSO_login", "Login canceled by user.");
                mAuthListener.onCancel();
                return;
            }
        }
        if (true) goto _L2; else goto _L6
_L6:
    }






    private class _cls1
        implements ServiceConnection
    {

        final SsoHandler this$0;

        public void onServiceConnected(ComponentName componentname, IBinder ibinder)
        {
            ibinder = com.sina.sso.RemoteSSO.Stub.asInterface(ibinder);
            try
            {
                componentname = ibinder.getPackageName();
                ibinder = ibinder.getActivityName();
                mAuthActivity.getApplicationContext().unbindService(mConnection);
                if (!startSingleSignOn(componentname, ibinder))
                {
                    mWeiboAuth.anthorize(mAuthListener);
                }
                return;
            }
            // Misplaced declaration of an exception variable
            catch (ComponentName componentname)
            {
                componentname.printStackTrace();
            }
        }

        public void onServiceDisconnected(ComponentName componentname)
        {
            mWeiboAuth.anthorize(mAuthListener);
        }

        _cls1()
        {
            this$0 = SsoHandler.this;
            super();
        }
    }

}
