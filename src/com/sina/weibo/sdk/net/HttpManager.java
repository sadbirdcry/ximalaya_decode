// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.sina.weibo.sdk.net;

import android.graphics.Bitmap;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.exception.WeiboHttpException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Iterator;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

// Referenced classes of package com.sina.weibo.sdk.net:
//            WeiboParameters, NetStateManager

class HttpManager
{
    private static class MySSLSocketFactory extends SSLSocketFactory
    {

        SSLContext sslContext;

        private TrustManager[] createTrustManagers(KeyStore keystore)
            throws KeyStoreException, NoSuchAlgorithmException
        {
            if (keystore == null)
            {
                throw new IllegalArgumentException("Keystore may not be null");
            } else
            {
                TrustManagerFactory trustmanagerfactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                trustmanagerfactory.init(keystore);
                return trustmanagerfactory.getTrustManagers();
            }
        }

        public Socket createSocket()
            throws IOException
        {
            return sslContext.getSocketFactory().createSocket();
        }

        public Socket createSocket(Socket socket, String s, int i, boolean flag)
            throws IOException, UnknownHostException
        {
            return sslContext.getSocketFactory().createSocket(socket, s, i, flag);
        }

        public MySSLSocketFactory(KeyStore keystore)
            throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException
        {
            super(keystore);
            sslContext = SSLContext.getInstance("TLS");
            keystore = createTrustManagers(keystore);
            sslContext.init(null, keystore, null);
        }
    }


    private static final String BOUNDARY = getBoundry();
    private static final int BUFFER_SIZE = 8192;
    private static final int CONNECTION_TIMEOUT = 5000;
    private static final String END_MP_BOUNDARY = (new StringBuilder("--")).append(BOUNDARY).append("--").toString();
    private static final String HTTP_METHOD_GET = "GET";
    private static final String HTTP_METHOD_POST = "POST";
    private static final String MP_BOUNDARY = (new StringBuilder("--")).append(BOUNDARY).toString();
    private static final String MULTIPART_FORM_DATA = "multipart/form-data";
    private static final int SOCKET_TIMEOUT = 20000;
    private static SSLSocketFactory sSSLSocketFactory;

    HttpManager()
    {
    }

    private static void buildParams(OutputStream outputstream, WeiboParameters weiboparameters)
        throws WeiboException
    {
        Object obj;
        Object obj1;
        obj = weiboparameters.keySet();
        obj1 = ((Set) (obj)).iterator();
_L4:
        if (((Iterator) (obj1)).hasNext()) goto _L2; else goto _L1
_L1:
        obj = ((Set) (obj)).iterator();
_L7:
        if (!((Iterator) (obj)).hasNext())
        {
            outputstream.write((new StringBuilder("\r\n")).append(END_MP_BOUNDARY).toString().getBytes());
            return;
        }
          goto _L3
_L2:
        try
        {
            String s = (String)((Iterator) (obj1)).next();
            if (weiboparameters.get(s) instanceof String)
            {
                StringBuilder stringbuilder = new StringBuilder(100);
                stringbuilder.setLength(0);
                stringbuilder.append(MP_BOUNDARY).append("\r\n");
                stringbuilder.append("content-disposition: form-data; name=\"").append(s).append("\"\r\n\r\n");
                stringbuilder.append(weiboparameters.get(s)).append("\r\n");
                outputstream.write(stringbuilder.toString().getBytes());
            }
        }
        // Misplaced declaration of an exception variable
        catch (OutputStream outputstream)
        {
            throw new WeiboException(outputstream);
        }
          goto _L4
_L3:
        Object obj2;
        obj1 = (String)((Iterator) (obj)).next();
        obj2 = weiboparameters.get(((String) (obj1)));
        if (!(obj2 instanceof Bitmap)) goto _L6; else goto _L5
_L5:
        StringBuilder stringbuilder1 = new StringBuilder();
        stringbuilder1.append(MP_BOUNDARY).append("\r\n");
        stringbuilder1.append("content-disposition: form-data; name=\"").append(((String) (obj1))).append("\"; filename=\"file\"\r\n");
        stringbuilder1.append("Content-Type: application/octet-stream; charset=utf-8\r\n\r\n");
        outputstream.write(stringbuilder1.toString().getBytes());
        obj1 = (Bitmap)obj2;
        obj2 = new ByteArrayOutputStream();
        ((Bitmap) (obj1)).compress(android.graphics.Bitmap.CompressFormat.PNG, 100, ((OutputStream) (obj2)));
        outputstream.write(((ByteArrayOutputStream) (obj2)).toByteArray());
        outputstream.write("\r\n".getBytes());
          goto _L7
_L6:
        if (!(obj2 instanceof ByteArrayOutputStream)) goto _L7; else goto _L8
_L8:
        StringBuilder stringbuilder2 = new StringBuilder();
        stringbuilder2.append(MP_BOUNDARY).append("\r\n");
        stringbuilder2.append("content-disposition: form-data; name=\"").append(((String) (obj1))).append("\"; filename=\"file\"\r\n");
        stringbuilder2.append("Content-Type: application/octet-stream; charset=utf-8\r\n\r\n");
        outputstream.write(stringbuilder2.toString().getBytes());
        obj1 = (ByteArrayOutputStream)obj2;
        outputstream.write(((ByteArrayOutputStream) (obj1)).toByteArray());
        outputstream.write("\r\n".getBytes());
        ((ByteArrayOutputStream) (obj1)).close();
          goto _L7
    }

    private static String getBoundry()
    {
        StringBuffer stringbuffer;
        int i;
        stringbuffer = new StringBuffer();
        i = 1;
_L2:
        long l;
        if (i >= 12)
        {
            return stringbuffer.toString();
        }
        l = System.currentTimeMillis() + (long)i;
        if (l % 3L != 0L)
        {
            break; /* Loop/switch isn't completed */
        }
        stringbuffer.append((char)(int)l % 9);
_L3:
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        if (l % 3L == 1L)
        {
            stringbuffer.append((char)(int)(l % 26L + 65L));
        } else
        {
            stringbuffer.append((char)(int)(l % 26L + 97L));
        }
          goto _L3
        if (true) goto _L2; else goto _L4
_L4:
    }

    private static HttpClient getNewHttpClient()
    {
        Object obj;
        try
        {
            obj = new BasicHttpParams();
            HttpProtocolParams.setVersion(((HttpParams) (obj)), HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(((HttpParams) (obj)), "UTF-8");
            Object obj1 = new SchemeRegistry();
            ((SchemeRegistry) (obj1)).register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            ((SchemeRegistry) (obj1)).register(new Scheme("https", getSSLSocketFactory(), 443));
            obj1 = new ThreadSafeClientConnManager(((HttpParams) (obj)), ((SchemeRegistry) (obj1)));
            HttpConnectionParams.setConnectionTimeout(((HttpParams) (obj)), 5000);
            HttpConnectionParams.setSoTimeout(((HttpParams) (obj)), 20000);
            obj = new DefaultHttpClient(((org.apache.http.conn.ClientConnectionManager) (obj1)), ((HttpParams) (obj)));
        }
        catch (Exception exception)
        {
            return new DefaultHttpClient();
        }
        return ((HttpClient) (obj));
    }

    private static SSLSocketFactory getSSLSocketFactory()
    {
        InputStream inputstream;
        InputStream inputstream1;
        inputstream1 = null;
        inputstream = null;
        if (sSSLSocketFactory != null)
        {
            break MISSING_BLOCK_LABEL_62;
        }
        InputStream inputstream2 = com/sina/weibo/sdk/net/HttpManager.getResourceAsStream("cacert.cer");
        inputstream = inputstream2;
        inputstream1 = inputstream2;
        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
        inputstream = inputstream2;
        inputstream1 = inputstream2;
        keystore.load(inputstream2, null);
        inputstream = inputstream2;
        inputstream1 = inputstream2;
        sSSLSocketFactory = new MySSLSocketFactory(keystore);
        Exception exception;
        Exception exception1;
        if (inputstream2 != null)
        {
            try
            {
                inputstream2.close();
            }
            catch (IOException ioexception1) { }
        }
        return sSSLSocketFactory;
        exception1;
        inputstream1 = inputstream;
        exception1.printStackTrace();
        inputstream1 = inputstream;
        sSSLSocketFactory = MySSLSocketFactory.getSocketFactory();
        if (inputstream != null)
        {
            try
            {
                inputstream.close();
            }
            catch (IOException ioexception) { }
        }
        if (false)
        {
        } else
        {
            break MISSING_BLOCK_LABEL_62;
        }
        exception;
        if (inputstream1 != null)
        {
            try
            {
                inputstream1.close();
            }
            catch (IOException ioexception2) { }
        }
        throw exception;
    }

    public static String openUrl(String s, String s1, WeiboParameters weiboparameters)
        throws WeiboException
    {
        return readRsponse(requestHttpExecute(s, s1, weiboparameters));
    }

    private static String readRsponse(HttpResponse httpresponse)
        throws WeiboException
    {
        Object obj = null;
        if (httpresponse != null) goto _L2; else goto _L1
_L1:
        return ((String) (obj));
_L2:
        ByteArrayOutputStream bytearrayoutputstream;
        obj = httpresponse.getEntity();
        bytearrayoutputstream = new ByteArrayOutputStream();
        Object obj1 = ((HttpEntity) (obj)).getContent();
        Object obj2;
        obj2 = obj1;
        obj = obj1;
        Header header = httpresponse.getFirstHeader("Content-Encoding");
        httpresponse = ((HttpResponse) (obj1));
        if (header == null)
        {
            break MISSING_BLOCK_LABEL_92;
        }
        httpresponse = ((HttpResponse) (obj1));
        obj2 = obj1;
        obj = obj1;
        if (header.getValue().toLowerCase().indexOf("gzip") <= -1)
        {
            break MISSING_BLOCK_LABEL_92;
        }
        obj2 = obj1;
        obj = obj1;
        httpresponse = new GZIPInputStream(((InputStream) (obj1)));
        obj2 = httpresponse;
        obj = httpresponse;
        obj1 = new byte[8192];
_L4:
        obj2 = httpresponse;
        obj = httpresponse;
        int i = httpresponse.read(((byte []) (obj1)));
        if (i != -1)
        {
            break MISSING_BLOCK_LABEL_157;
        }
        obj2 = httpresponse;
        obj = httpresponse;
        obj1 = new String(bytearrayoutputstream.toByteArray(), "UTF-8");
        obj = obj1;
        if (httpresponse != null)
        {
            try
            {
                httpresponse.close();
            }
            // Misplaced declaration of an exception variable
            catch (HttpResponse httpresponse)
            {
                httpresponse.printStackTrace();
                return ((String) (obj1));
            }
            return ((String) (obj1));
        }
        continue; /* Loop/switch isn't completed */
        obj2 = httpresponse;
        obj = httpresponse;
        bytearrayoutputstream.write(((byte []) (obj1)), 0, i);
        if (true) goto _L4; else goto _L3
_L3:
        httpresponse;
        obj = obj2;
_L8:
        throw new WeiboException(httpresponse);
        httpresponse;
_L6:
        if (obj != null)
        {
            try
            {
                ((InputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        throw httpresponse;
        httpresponse;
        obj = null;
        if (true) goto _L6; else goto _L5
_L5:
        httpresponse;
        obj = null;
        if (true) goto _L8; else goto _L7
_L7:
        if (true) goto _L1; else goto _L9
_L9:
    }

    private static HttpResponse requestHttpExecute(String s, String s1, WeiboParameters weiboparameters)
    {
        Object obj;
        HttpClient httpclient;
        int i;
        try
        {
            httpclient = getNewHttpClient();
            httpclient.getParams().setParameter("http.route.default-proxy", NetStateManager.getAPN());
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            throw new WeiboException(s);
        }
        obj = null;
        if (!s1.equals("GET")) goto _L2; else goto _L1
_L1:
        weiboparameters = new HttpGet((new StringBuilder(String.valueOf(s))).append("?").append(weiboparameters.encodeUrl()).toString());
_L5:
        s = httpclient.execute(weiboparameters);
        i = s.getStatusLine().getStatusCode();
        if (i == 200)
        {
            break MISSING_BLOCK_LABEL_302;
        }
        throw new WeiboHttpException(readRsponse(s), i);
_L2:
        if (!s1.equals("POST"))
        {
            break MISSING_BLOCK_LABEL_278;
        }
        s = new HttpPost(s);
        s1 = new ByteArrayOutputStream();
        if (!weiboparameters.hasBinaryData()) goto _L4; else goto _L3
_L3:
        s.setHeader("Content-Type", (new StringBuilder("multipart/form-data; boundary=")).append(BOUNDARY).toString());
        buildParams(s1, weiboparameters);
_L6:
        s.setEntity(new ByteArrayEntity(s1.toByteArray()));
        s1.close();
        weiboparameters = s;
          goto _L5
_L4:
        obj = weiboparameters.get("content-type");
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_265;
        }
        if (!(obj instanceof String))
        {
            break MISSING_BLOCK_LABEL_265;
        }
        weiboparameters.remove("content-type");
        s.setHeader("Content-Type", (String)obj);
_L7:
        s1.write(weiboparameters.encodeUrl().getBytes("UTF-8"));
          goto _L6
        s.setHeader("Content-Type", "application/x-www-form-urlencoded");
          goto _L7
        weiboparameters = ((WeiboParameters) (obj));
        if (!s1.equals("DELETE")) goto _L5; else goto _L8
_L8:
        weiboparameters = new HttpDelete(s);
          goto _L5
        return s;
    }

}
