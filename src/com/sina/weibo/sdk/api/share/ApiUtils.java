// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.sina.weibo.sdk.api.share;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.Signature;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.sina.weibo.sdk.utils.LogUtil;
import com.sina.weibo.sdk.utils.MD5;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class ApiUtils
{
    public static class WeiboInfo
    {

        public String packageName;
        public int supportApi;

        public String toString()
        {
            return (new StringBuilder("WeiboInfo: PackageName = ")).append(packageName).append(", supportApi = ").append(supportApi).toString();
        }

        public WeiboInfo()
        {
        }
    }


    public static final int BUILD_INT = 10350;
    public static final int BUILD_INT_VER_2_2 = 10351;
    public static final int BUILD_INT_VER_2_3 = 10352;
    public static final int BUILD_INT_VER_2_5 = 10353;
    private static final String TAG = "ApiUtils";
    private static final String WEIBO_IDENTITY_ACTION = "com.sina.weibo.action.sdkidentity";
    private static final Uri WEIBO_NAME_URI = Uri.parse("content://com.sina.weibo.sdkProvider/query/package");
    private static final String WEIBO_SIGN = "18da2bf10352443a00a5e046d9fca6bd";

    public ApiUtils()
    {
    }

    public static boolean containSign(Signature asignature[], String s)
    {
        if (asignature != null && s != null)
        {
            int j = asignature.length;
            int i = 0;
            while (i < j) 
            {
                if (s.equals(MD5.hexdigest(asignature[i].toByteArray())))
                {
                    LogUtil.d("ApiUtils", "check pass");
                    return true;
                }
                i++;
            }
        }
        return false;
    }

    public static boolean isWeiboAppSupportAPI(int i)
    {
        return i >= 10350;
    }

    public static WeiboInfo queryWeiboInfo(Context context)
    {
        WeiboInfo weiboinfo1 = queryWeiboInfoByProvider(context);
        WeiboInfo weiboinfo = weiboinfo1;
        if (weiboinfo1 == null)
        {
            weiboinfo = queryWeiboInfoByFile(context);
        }
        return weiboinfo;
    }

    private static WeiboInfo queryWeiboInfoByAsset(Context context, String s)
    {
        if (context != null && s != null) goto _L2; else goto _L1
_L1:
        context = null;
_L5:
        return context;
_L2:
        InputStream inputstream1;
        byte abyte0[];
        Context context1 = context.createPackageContext(s, 2);
        abyte0 = new byte[1024];
        inputstream1 = context1.getAssets().open("weibo_for_sdk.json");
        InputStream inputstream = inputstream1;
        StringBuilder stringbuilder = new StringBuilder();
_L4:
        inputstream = inputstream1;
        int i = inputstream1.read(abyte0, 0, 1024);
        if (i != -1)
        {
            break MISSING_BLOCK_LABEL_104;
        }
        inputstream = inputstream1;
        if (TextUtils.isEmpty(stringbuilder.toString()))
        {
            break MISSING_BLOCK_LABEL_94;
        }
        inputstream = inputstream1;
        boolean flag = validateWeiboSign(context, s);
        if (flag)
        {
            break; /* Loop/switch isn't completed */
        }
        WeiboInfo weiboinfo;
        if (inputstream1 != null)
        {
            try
            {
                inputstream1.close();
            }
            // Misplaced declaration of an exception variable
            catch (Context context) { }
        }
        return null;
        inputstream = inputstream1;
        stringbuilder.append(new String(abyte0, 0, i));
        if (true) goto _L4; else goto _L3
        context;
_L14:
        inputstream = inputstream1;
        context.printStackTrace();
        if (inputstream1 != null)
        {
            try
            {
                inputstream1.close();
            }
            // Misplaced declaration of an exception variable
            catch (Context context) { }
        }
        return null;
_L3:
        inputstream = inputstream1;
        i = (new JSONObject(stringbuilder.toString())).optInt("support_api", -1);
        inputstream = inputstream1;
        weiboinfo = new WeiboInfo();
        inputstream = inputstream1;
        weiboinfo.packageName = s;
        inputstream = inputstream1;
        weiboinfo.supportApi = i;
        context = weiboinfo;
        if (inputstream1 != null)
        {
            try
            {
                inputstream1.close();
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                return weiboinfo;
            }
            return weiboinfo;
        }
          goto _L5
        context;
        inputstream1 = null;
_L13:
        inputstream = inputstream1;
        context.printStackTrace();
        if (inputstream1 != null)
        {
            try
            {
                inputstream1.close();
            }
            // Misplaced declaration of an exception variable
            catch (Context context) { }
        }
        break MISSING_BLOCK_LABEL_142;
        context;
        inputstream1 = null;
_L11:
        inputstream = inputstream1;
        context.printStackTrace();
        if (inputstream1 != null)
        {
            try
            {
                inputstream1.close();
            }
            // Misplaced declaration of an exception variable
            catch (Context context) { }
        }
        break MISSING_BLOCK_LABEL_142;
        context;
        inputstream1 = null;
_L9:
        inputstream = inputstream1;
        LogUtil.e("ApiUtils", context.getMessage());
        if (inputstream1 != null)
        {
            try
            {
                inputstream1.close();
            }
            // Misplaced declaration of an exception variable
            catch (Context context) { }
        }
        break MISSING_BLOCK_LABEL_142;
        context;
        inputstream = null;
_L7:
        if (inputstream != null)
        {
            try
            {
                inputstream.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s) { }
        }
        throw context;
        context;
        if (true) goto _L7; else goto _L6
_L6:
        context;
        if (true) goto _L9; else goto _L8
_L8:
        context;
        if (true) goto _L11; else goto _L10
_L10:
        context;
        if (true) goto _L13; else goto _L12
_L12:
        context;
        inputstream1 = null;
          goto _L14
    }

    private static WeiboInfo queryWeiboInfoByFile(Context context)
    {
        Object obj = new Intent("com.sina.weibo.action.sdkidentity");
        ((Intent) (obj)).addCategory("android.intent.category.DEFAULT");
        obj = context.getPackageManager().queryIntentServices(((Intent) (obj)), 0);
        if (obj == null || ((List) (obj)).isEmpty())
        {
            return null;
        }
        obj = ((List) (obj)).iterator();
        Object obj1;
        do
        {
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    return null;
                }
                obj1 = (ResolveInfo)((Iterator) (obj)).next();
            } while (((ResolveInfo) (obj1)).serviceInfo == null || ((ResolveInfo) (obj1)).serviceInfo.applicationInfo == null || ((ResolveInfo) (obj1)).serviceInfo.applicationInfo.packageName == null || ((ResolveInfo) (obj1)).serviceInfo.applicationInfo.packageName.length() == 0);
            obj1 = queryWeiboInfoByAsset(context, ((ResolveInfo) (obj1)).serviceInfo.applicationInfo.packageName);
        } while (obj1 == null);
        return ((WeiboInfo) (obj1));
    }

    public static WeiboInfo queryWeiboInfoByPackage(Context context, String s)
    {
        if (context != null && s != null) goto _L2; else goto _L1
_L1:
        Object obj = null;
_L4:
        return ((WeiboInfo) (obj));
_L2:
        WeiboInfo weiboinfo;
        weiboinfo = queryWeiboInfoByAsset(context, s);
        obj = weiboinfo;
        if (weiboinfo != null) goto _L4; else goto _L3
_L3:
        context = queryWeiboInfoByProvider(context);
        if (context == null)
        {
            break; /* Loop/switch isn't completed */
        }
        obj = context;
        if (s.equals(((WeiboInfo) (context)).packageName)) goto _L4; else goto _L5
_L5:
        return null;
    }

    private static WeiboInfo queryWeiboInfoByProvider(Context context)
    {
        Object obj = context.getContentResolver();
        Cursor cursor = ((ContentResolver) (obj)).query(WEIBO_NAME_URI, null, null, null, null);
        if (cursor != null) goto _L2; else goto _L1
_L1:
        if (cursor != null)
        {
            cursor.close();
        }
        context = null;
_L11:
        return context;
_L2:
        obj = cursor;
        int i = cursor.getColumnIndex("support_api");
        obj = cursor;
        int j = cursor.getColumnIndex("package");
        obj = cursor;
        if (!cursor.moveToFirst()) goto _L4; else goto _L3
_L3:
        obj = cursor;
        Object obj1 = cursor.getString(i);
        obj = cursor;
        i = Integer.parseInt(((String) (obj1)));
_L5:
        obj = cursor;
        String s = cursor.getString(j);
        obj = cursor;
        if (TextUtils.isEmpty(s))
        {
            break; /* Loop/switch isn't completed */
        }
        obj = cursor;
        if (!validateWeiboSign(context, s))
        {
            break; /* Loop/switch isn't completed */
        }
        obj = cursor;
        obj1 = new WeiboInfo();
        obj = cursor;
        obj1.packageName = s;
        obj = cursor;
        obj1.supportApi = i;
        context = ((Context) (obj1));
        if (cursor != null)
        {
            cursor.close();
            return ((WeiboInfo) (obj1));
        }
        continue; /* Loop/switch isn't completed */
        NumberFormatException numberformatexception;
        numberformatexception;
        obj = cursor;
        numberformatexception.printStackTrace();
        i = -1;
        if (true) goto _L5; else goto _L4
        context;
        cursor = null;
_L9:
        obj = cursor;
        context.printStackTrace();
        obj = cursor;
        LogUtil.e("ApiUtils", context.getMessage());
        if (cursor != null)
        {
            cursor.close();
        }
_L6:
        return null;
        context;
        obj = null;
_L7:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        throw context;
_L4:
        if (cursor != null)
        {
            cursor.close();
        }
          goto _L6
        context;
          goto _L7
        context;
        if (true) goto _L9; else goto _L8
_L8:
        if (true) goto _L11; else goto _L10
_L10:
    }

    public static boolean validateWeiboSign(Context context, String s)
    {
        try
        {
            context = context.getPackageManager().getPackageInfo(s, 64);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        return containSign(((PackageInfo) (context)).signatures, "18da2bf10352443a00a5e046d9fca6bd");
    }

}
