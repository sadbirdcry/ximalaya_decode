// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.sina.weibo.sdk.api.share;


// Referenced classes of package com.sina.weibo.sdk.api.share:
//            BaseRequest, BaseResponse

public interface IWeiboHandler
{
    public static interface Request
    {

        public abstract void onRequest(BaseRequest baserequest);
    }

    public static interface Response
    {

        public abstract void onResponse(BaseResponse baseresponse);
    }

}
