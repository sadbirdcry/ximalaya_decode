// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.loopj.android.http;

import android.content.Context;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpVersion;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.SyncBasicHttpContext;

// Referenced classes of package com.loopj.android.http:
//            RetryHandler, MySSLSocketFactory, RequestParams, ResponseHandlerInterface, 
//            RequestHandle, AsyncHttpRequest, RangeFileAsyncHttpResponseHandler, PreemtiveAuthorizationHttpRequestInterceptor

public class AsyncHttpClient
{
    private static class InflatingEntity extends HttpEntityWrapper
    {

        public InputStream getContent()
            throws IOException
        {
            return new GZIPInputStream(wrappedEntity.getContent());
        }

        public long getContentLength()
        {
            return -1L;
        }

        public InflatingEntity(HttpEntity httpentity)
        {
            super(httpentity);
        }
    }


    public static final int DEFAULT_MAX_CONNECTIONS = 10;
    public static final int DEFAULT_MAX_RETRIES = 5;
    public static final int DEFAULT_RETRY_SLEEP_TIME_MILLIS = 1500;
    public static final int DEFAULT_SOCKET_BUFFER_SIZE = 8192;
    public static final int DEFAULT_SOCKET_TIMEOUT = 10000;
    public static final String ENCODING_GZIP = "gzip";
    public static final String HEADER_ACCEPT_ENCODING = "Accept-Encoding";
    public static final String LOG_TAG = "AsyncHttpClient";
    public static final String VERSION = "1.4.5";
    private final Map clientHeaderMap;
    private final DefaultHttpClient httpClient;
    private boolean isUrlEncodingEnabled;
    private int maxConnections;
    private final Map requestMap;
    private ExecutorService threadPool;
    private int timeout;

    public AsyncHttpClient()
    {
        this(false, 80, 443);
    }

    public AsyncHttpClient(int i)
    {
        this(false, i, 443);
    }

    public AsyncHttpClient(int i, int j)
    {
        this(false, i, j);
    }

    public AsyncHttpClient(SchemeRegistry schemeregistry)
    {
        maxConnections = 10;
        timeout = 10000;
        isUrlEncodingEnabled = true;
        BasicHttpParams basichttpparams = new BasicHttpParams();
        ConnManagerParams.setTimeout(basichttpparams, timeout);
        ConnManagerParams.setMaxConnectionsPerRoute(basichttpparams, new ConnPerRouteBean(maxConnections));
        ConnManagerParams.setMaxTotalConnections(basichttpparams, 10);
        HttpConnectionParams.setSoTimeout(basichttpparams, timeout);
        HttpConnectionParams.setConnectionTimeout(basichttpparams, timeout);
        HttpConnectionParams.setTcpNoDelay(basichttpparams, true);
        HttpConnectionParams.setSocketBufferSize(basichttpparams, 8192);
        HttpProtocolParams.setVersion(basichttpparams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setUserAgent(basichttpparams, String.format("android-async-http/%s (http://loopj.com/android-async-http)", new Object[] {
            "1.4.5"
        }));
        schemeregistry = new ThreadSafeClientConnManager(basichttpparams, schemeregistry);
        threadPool = Executors.newCachedThreadPool();
        requestMap = new WeakHashMap();
        clientHeaderMap = new HashMap();
        new SyncBasicHttpContext(new BasicHttpContext());
        httpClient = new DefaultHttpClient(schemeregistry, basichttpparams);
        httpClient.addRequestInterceptor(new _cls1());
        httpClient.addResponseInterceptor(new _cls2());
        httpClient.addRequestInterceptor(new _cls3(), 0);
        httpClient.setHttpRequestRetryHandler(new RetryHandler(5, 1500));
    }

    public AsyncHttpClient(boolean flag, int i, int j)
    {
        this(getDefaultSchemeRegistry(flag, i, j));
    }

    private HttpEntityEnclosingRequestBase addEntityToRequestBase(HttpEntityEnclosingRequestBase httpentityenclosingrequestbase, HttpEntity httpentity)
    {
        if (httpentity != null)
        {
            httpentityenclosingrequestbase.setEntity(httpentity);
        }
        return httpentityenclosingrequestbase;
    }

    public static void allowRetryExceptionClass(Class class1)
    {
        if (class1 != null)
        {
            RetryHandler.addClassToWhitelist(class1);
        }
    }

    public static void blockRetryExceptionClass(Class class1)
    {
        if (class1 != null)
        {
            RetryHandler.addClassToBlacklist(class1);
        }
    }

    private static SchemeRegistry getDefaultSchemeRegistry(boolean flag, int i, int j)
    {
        if (flag)
        {
            Log.d("AsyncHttpClient", "Beware! Using the fix is insecure, as it doesn't verify SSL certificates.");
        }
        int k = i;
        if (i < 1)
        {
            k = 80;
            Log.d("AsyncHttpClient", "Invalid HTTP port number specified, defaulting to 80");
        }
        i = j;
        if (j < 1)
        {
            i = 443;
            Log.d("AsyncHttpClient", "Invalid HTTPS port number specified, defaulting to 443");
        }
        SSLSocketFactory sslsocketfactory;
        SchemeRegistry schemeregistry;
        if (flag)
        {
            sslsocketfactory = MySSLSocketFactory.getFixedSocketFactory();
        } else
        {
            sslsocketfactory = SSLSocketFactory.getSocketFactory();
        }
        schemeregistry = new SchemeRegistry();
        schemeregistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), k));
        schemeregistry.register(new Scheme("https", sslsocketfactory, i));
        return schemeregistry;
    }

    public static String getUrlWithQueryString(boolean flag, String s, RequestParams requestparams)
    {
        if (flag)
        {
            s = s.replaceAll(" ", "%20");
        }
        Object obj = s;
        if (requestparams != null)
        {
            requestparams = requestparams.getParamString().trim();
            obj = s;
            if (!requestparams.equals(""))
            {
                obj = s;
                if (!requestparams.equals("?"))
                {
                    obj = (new StringBuilder()).append(s);
                    if (s.contains("?"))
                    {
                        s = "&";
                    } else
                    {
                        s = "?";
                    }
                    s = ((StringBuilder) (obj)).append(s).toString();
                    obj = (new StringBuilder()).append(s).append(requestparams).toString();
                }
            }
        }
        return ((String) (obj));
    }

    private HttpEntity paramsToEntity(RequestParams requestparams, ResponseHandlerInterface responsehandlerinterface)
    {
        HttpEntity httpentity = null;
        if (requestparams != null)
        {
            try
            {
                httpentity = requestparams.getEntity(responsehandlerinterface);
            }
            // Misplaced declaration of an exception variable
            catch (RequestParams requestparams)
            {
                if (responsehandlerinterface != null)
                {
                    responsehandlerinterface.sendFailureMessage(0, null, null, requestparams);
                    return null;
                } else
                {
                    requestparams.printStackTrace();
                    return null;
                }
            }
        }
        return httpentity;
    }

    public static void silentCloseInputStream(InputStream inputstream)
    {
        if (inputstream == null)
        {
            break MISSING_BLOCK_LABEL_8;
        }
        inputstream.close();
        return;
        inputstream;
        Log.w("AsyncHttpClient", "Cannot close input stream", inputstream);
        return;
    }

    public static void silentCloseOutputStream(OutputStream outputstream)
    {
        if (outputstream == null)
        {
            break MISSING_BLOCK_LABEL_8;
        }
        outputstream.close();
        return;
        outputstream;
        Log.w("AsyncHttpClient", "Cannot close output stream", outputstream);
        return;
    }

    public void addHeader(String s, String s1)
    {
        synchronized (clientHeaderMap)
        {
            clientHeaderMap.put(s, s1);
        }
        return;
        s;
        map;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void cancelAllRequests(boolean flag)
    {
        Iterator iterator = requestMap.values().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            Object obj = (List)iterator.next();
            if (obj != null)
            {
                obj = ((List) (obj)).iterator();
                while (((Iterator) (obj)).hasNext()) 
                {
                    ((RequestHandle)((Iterator) (obj)).next()).cancel(flag);
                }
            }
        } while (true);
        requestMap.clear();
    }

    public void cancelRequests(Context context, boolean flag)
    {
        Object obj = (List)requestMap.get(context);
        if (obj != null)
        {
            for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext(); ((RequestHandle)((Iterator) (obj)).next()).cancel(flag)) { }
            requestMap.remove(context);
        }
    }

    public void clearBasicAuth()
    {
        httpClient.getCredentialsProvider().clear();
    }

    public RequestHandle delete(Context context, String s, ResponseHandlerInterface responsehandlerinterface)
    {
        s = new HttpDelete(URI.create(s).normalize());
        return sendRequest(httpClient, null, s, null, responsehandlerinterface, context);
    }

    public RequestHandle delete(Context context, String s, Header aheader[], RequestParams requestparams, ResponseHandlerInterface responsehandlerinterface)
    {
        s = new HttpDelete(getUrlWithQueryString(isUrlEncodingEnabled, s, requestparams));
        if (aheader != null)
        {
            s.setHeaders(aheader);
        }
        return sendRequest(httpClient, null, s, null, responsehandlerinterface, context);
    }

    public RequestHandle delete(Context context, String s, Header aheader[], ResponseHandlerInterface responsehandlerinterface)
    {
        s = new HttpDelete(URI.create(s).normalize());
        if (aheader != null)
        {
            s.setHeaders(aheader);
        }
        return sendRequest(httpClient, null, s, null, responsehandlerinterface, context);
    }

    public RequestHandle delete(String s, ResponseHandlerInterface responsehandlerinterface)
    {
        return delete(null, s, responsehandlerinterface);
    }

    public RequestHandle get(Context context, String s, RequestParams requestparams, ResponseHandlerInterface responsehandlerinterface)
    {
        return sendRequest(httpClient, null, new HttpGet(getUrlWithQueryString(isUrlEncodingEnabled, s, requestparams)), null, responsehandlerinterface, context);
    }

    public RequestHandle get(Context context, String s, ResponseHandlerInterface responsehandlerinterface)
    {
        return get(context, s, null, responsehandlerinterface);
    }

    public RequestHandle get(Context context, String s, Header aheader[], RequestParams requestparams, ResponseHandlerInterface responsehandlerinterface)
    {
        s = new HttpGet(getUrlWithQueryString(isUrlEncodingEnabled, s, requestparams));
        if (aheader != null)
        {
            s.setHeaders(aheader);
        }
        return sendRequest(httpClient, null, s, null, responsehandlerinterface, context);
    }

    public RequestHandle get(String s, RequestParams requestparams, ResponseHandlerInterface responsehandlerinterface)
    {
        return get(null, s, requestparams, responsehandlerinterface);
    }

    public RequestHandle get(String s, ResponseHandlerInterface responsehandlerinterface)
    {
        return get(null, s, null, responsehandlerinterface);
    }

    public HttpClient getHttpClient()
    {
        return httpClient;
    }

    public int getMaxConnections()
    {
        return maxConnections;
    }

    public int getTimeout()
    {
        return timeout;
    }

    public RequestHandle head(Context context, String s, RequestParams requestparams, ResponseHandlerInterface responsehandlerinterface)
    {
        return sendRequest(httpClient, null, new HttpHead(getUrlWithQueryString(isUrlEncodingEnabled, s, requestparams)), null, responsehandlerinterface, context);
    }

    public RequestHandle head(Context context, String s, ResponseHandlerInterface responsehandlerinterface)
    {
        return head(context, s, null, responsehandlerinterface);
    }

    public RequestHandle head(Context context, String s, Header aheader[], RequestParams requestparams, ResponseHandlerInterface responsehandlerinterface)
    {
        s = new HttpHead(getUrlWithQueryString(isUrlEncodingEnabled, s, requestparams));
        if (aheader != null)
        {
            s.setHeaders(aheader);
        }
        return sendRequest(httpClient, null, s, null, responsehandlerinterface, context);
    }

    public RequestHandle head(String s, RequestParams requestparams, ResponseHandlerInterface responsehandlerinterface)
    {
        return head(null, s, requestparams, responsehandlerinterface);
    }

    public RequestHandle head(String s, ResponseHandlerInterface responsehandlerinterface)
    {
        return head(null, s, null, responsehandlerinterface);
    }

    public boolean isUrlEncodingEnabled()
    {
        return isUrlEncodingEnabled;
    }

    public RequestHandle post(Context context, String s, RequestParams requestparams, ResponseHandlerInterface responsehandlerinterface)
    {
        return post(context, s, paramsToEntity(requestparams, responsehandlerinterface), null, responsehandlerinterface);
    }

    public RequestHandle post(Context context, String s, HttpEntity httpentity, String s1, ResponseHandlerInterface responsehandlerinterface)
    {
        return sendRequest(httpClient, null, addEntityToRequestBase(new HttpPost(URI.create(s).normalize()), httpentity), s1, responsehandlerinterface, context);
    }

    public RequestHandle post(Context context, String s, Header aheader[], RequestParams requestparams, String s1, ResponseHandlerInterface responsehandlerinterface)
    {
        s = new HttpPost(URI.create(s).normalize());
        if (requestparams != null)
        {
            s.setEntity(paramsToEntity(requestparams, responsehandlerinterface));
        }
        if (aheader != null)
        {
            s.setHeaders(aheader);
        }
        return sendRequest(httpClient, null, s, s1, responsehandlerinterface, context);
    }

    public RequestHandle post(Context context, String s, Header aheader[], HttpEntity httpentity, String s1, ResponseHandlerInterface responsehandlerinterface)
    {
        s = addEntityToRequestBase(new HttpPost(URI.create(s).normalize()), httpentity);
        if (aheader != null)
        {
            s.setHeaders(aheader);
        }
        return sendRequest(httpClient, null, s, s1, responsehandlerinterface, context);
    }

    public RequestHandle post(String s, RequestParams requestparams, ResponseHandlerInterface responsehandlerinterface)
    {
        return post(null, s, requestparams, responsehandlerinterface);
    }

    public RequestHandle post(String s, ResponseHandlerInterface responsehandlerinterface)
    {
        return post(null, s, null, responsehandlerinterface);
    }

    public RequestHandle put(Context context, String s, RequestParams requestparams, ResponseHandlerInterface responsehandlerinterface)
    {
        return put(context, s, paramsToEntity(requestparams, responsehandlerinterface), null, responsehandlerinterface);
    }

    public RequestHandle put(Context context, String s, HttpEntity httpentity, String s1, ResponseHandlerInterface responsehandlerinterface)
    {
        return sendRequest(httpClient, null, addEntityToRequestBase(new HttpPut(URI.create(s).normalize()), httpentity), s1, responsehandlerinterface, context);
    }

    public RequestHandle put(Context context, String s, Header aheader[], HttpEntity httpentity, String s1, ResponseHandlerInterface responsehandlerinterface)
    {
        s = addEntityToRequestBase(new HttpPut(URI.create(s).normalize()), httpentity);
        if (aheader != null)
        {
            s.setHeaders(aheader);
        }
        return sendRequest(httpClient, null, s, s1, responsehandlerinterface, context);
    }

    public RequestHandle put(String s, RequestParams requestparams, ResponseHandlerInterface responsehandlerinterface)
    {
        return put(null, s, requestparams, responsehandlerinterface);
    }

    public RequestHandle put(String s, ResponseHandlerInterface responsehandlerinterface)
    {
        return put(null, s, null, responsehandlerinterface);
    }

    public void removeHeader(String s)
    {
        synchronized (clientHeaderMap)
        {
            clientHeaderMap.remove(s);
        }
        return;
        s;
        map;
        JVM INSTR monitorexit ;
        throw s;
    }

    protected RequestHandle sendRequest(DefaultHttpClient defaulthttpclient, HttpContext httpcontext, HttpUriRequest httpurirequest, String s, ResponseHandlerInterface responsehandlerinterface, Context context)
    {
        if (s != null)
        {
            httpurirequest.setHeader("Content-Type", s);
        }
        responsehandlerinterface.setRequestHeaders(httpurirequest.getAllHeaders());
        responsehandlerinterface.setRequestURI(httpurirequest.getURI());
        defaulthttpclient = new AsyncHttpRequest(defaulthttpclient, httpcontext, httpurirequest, responsehandlerinterface);
        threadPool.submit(defaulthttpclient);
        s = new RequestHandle(defaulthttpclient);
        if (context != null)
        {
            httpcontext = (List)requestMap.get(context);
            defaulthttpclient = httpcontext;
            if (httpcontext == null)
            {
                defaulthttpclient = new LinkedList();
                requestMap.put(context, defaulthttpclient);
            }
            if (responsehandlerinterface instanceof RangeFileAsyncHttpResponseHandler)
            {
                ((RangeFileAsyncHttpResponseHandler)responsehandlerinterface).updateRequestHeaders(httpurirequest);
            }
            defaulthttpclient.add(s);
            defaulthttpclient = defaulthttpclient.iterator();
            do
            {
                if (!defaulthttpclient.hasNext())
                {
                    break;
                }
                if (((RequestHandle)defaulthttpclient.next()).shouldBeGarbageCollected())
                {
                    defaulthttpclient.remove();
                }
            } while (true);
        }
        return s;
    }

    public void setAuthenticationPreemptive(boolean flag)
    {
        if (flag)
        {
            httpClient.addRequestInterceptor(new PreemtiveAuthorizationHttpRequestInterceptor(), 0);
            return;
        } else
        {
            httpClient.removeRequestInterceptorByClass(com/loopj/android/http/PreemtiveAuthorizationHttpRequestInterceptor);
            return;
        }
    }

    public void setBasicAuth(String s, String s1)
    {
        setBasicAuth(s, s1, false);
    }

    public void setBasicAuth(String s, String s1, AuthScope authscope)
    {
        setBasicAuth(s, s1, authscope, false);
    }

    public void setBasicAuth(String s, String s1, AuthScope authscope, boolean flag)
    {
        s1 = new UsernamePasswordCredentials(s, s1);
        CredentialsProvider credentialsprovider = httpClient.getCredentialsProvider();
        s = authscope;
        if (authscope == null)
        {
            s = AuthScope.ANY;
        }
        credentialsprovider.setCredentials(s, s1);
        setAuthenticationPreemptive(flag);
    }

    public void setBasicAuth(String s, String s1, boolean flag)
    {
        setBasicAuth(s, s1, null, flag);
    }

    public void setCookieStore(CookieStore cookiestore)
    {
        httpClient.setCookieStore(cookiestore);
    }

    public void setEnableRedirects(final boolean enableRedirects)
    {
        httpClient.setRedirectHandler(new _cls4());
    }

    public void setMaxConnections(int i)
    {
        int j = i;
        if (i < 1)
        {
            j = 10;
        }
        maxConnections = j;
        ConnManagerParams.setMaxConnectionsPerRoute(httpClient.getParams(), new ConnPerRouteBean(maxConnections));
    }

    public void setMaxRetriesAndTimeout(int i, int j)
    {
        httpClient.setHttpRequestRetryHandler(new RetryHandler(i, j));
    }

    public void setProxy(String s, int i)
    {
        s = new HttpHost(s, i);
        httpClient.getParams().setParameter("http.route.default-proxy", s);
    }

    public void setProxy(String s, int i, String s1, String s2)
    {
        httpClient.getCredentialsProvider().setCredentials(new AuthScope(s, i), new UsernamePasswordCredentials(s1, s2));
        s = new HttpHost(s, i);
        httpClient.getParams().setParameter("http.route.default-proxy", s);
    }

    public void setSSLSocketFactory(SSLSocketFactory sslsocketfactory)
    {
        httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", sslsocketfactory, 443));
    }

    public void setThreadPool(ThreadPoolExecutor threadpoolexecutor)
    {
        threadPool = threadpoolexecutor;
    }

    public void setTimeout(int i)
    {
        int j = i;
        if (i < 1000)
        {
            j = 10000;
        }
        timeout = j;
        HttpParams httpparams = httpClient.getParams();
        ConnManagerParams.setTimeout(httpparams, timeout);
        HttpConnectionParams.setSoTimeout(httpparams, timeout);
        HttpConnectionParams.setConnectionTimeout(httpparams, timeout);
    }

    public void setURLEncodingEnabled(boolean flag)
    {
        isUrlEncodingEnabled = flag;
    }

    public void setUserAgent(String s)
    {
        HttpProtocolParams.setUserAgent(httpClient.getParams(), s);
    }


    private class _cls1
        implements HttpRequestInterceptor
    {

        final AsyncHttpClient this$0;

        public void process(HttpRequest httprequest, HttpContext httpcontext)
        {
            if (!httprequest.containsHeader("Accept-Encoding"))
            {
                httprequest.addHeader("Accept-Encoding", "gzip");
            }
            httpcontext = clientHeaderMap;
            httpcontext;
            JVM INSTR monitorenter ;
            String s;
            for (Iterator iterator = clientHeaderMap.keySet().iterator(); iterator.hasNext(); httprequest.addHeader(s, (String)clientHeaderMap.get(s)))
            {
                s = (String)iterator.next();
                if (httprequest.containsHeader(s))
                {
                    Header header = httprequest.getFirstHeader(s);
                    Log.d("AsyncHttpClient", String.format("Headers were overwritten! (%s | %s) overwrites (%s | %s)", new Object[] {
                        s, clientHeaderMap.get(s), header.getName(), header.getValue()
                    }));
                }
            }

            break MISSING_BLOCK_LABEL_180;
            httprequest;
            httpcontext;
            JVM INSTR monitorexit ;
            throw httprequest;
            httpcontext;
            JVM INSTR monitorexit ;
        }

        _cls1()
        {
            this$0 = AsyncHttpClient.this;
            super();
        }
    }


    private class _cls2
        implements HttpResponseInterceptor
    {

        final AsyncHttpClient this$0;

        public void process(HttpResponse httpresponse, HttpContext httpcontext)
        {
            httpcontext = httpresponse.getEntity();
            if (httpcontext != null) goto _L2; else goto _L1
_L1:
            Header header;
            return;
_L2:
            if ((header = httpcontext.getContentEncoding()) != null)
            {
                HeaderElement aheaderelement[] = header.getElements();
                int j = aheaderelement.length;
                int i = 0;
                while (i < j) 
                {
                    if (aheaderelement[i].getName().equalsIgnoreCase("gzip"))
                    {
                        httpresponse.setEntity(new InflatingEntity(httpcontext));
                        return;
                    }
                    i++;
                }
            }
            if (true) goto _L1; else goto _L3
_L3:
        }

        _cls2()
        {
            this$0 = AsyncHttpClient.this;
            super();
        }
    }


    private class _cls3
        implements HttpRequestInterceptor
    {

        final AsyncHttpClient this$0;

        public void process(HttpRequest httprequest, HttpContext httpcontext)
            throws HttpException, IOException
        {
            httprequest = (AuthState)httpcontext.getAttribute("http.auth.target-scope");
            CredentialsProvider credentialsprovider = (CredentialsProvider)httpcontext.getAttribute("http.auth.credentials-provider");
            httpcontext = (HttpHost)httpcontext.getAttribute("http.target_host");
            if (httprequest.getAuthScheme() == null)
            {
                httpcontext = credentialsprovider.getCredentials(new AuthScope(httpcontext.getHostName(), httpcontext.getPort()));
                if (httpcontext != null)
                {
                    httprequest.setAuthScheme(new BasicScheme());
                    httprequest.setCredentials(httpcontext);
                }
            }
        }

        _cls3()
        {
            this$0 = AsyncHttpClient.this;
            super();
        }
    }


    private class _cls4 extends DefaultRedirectHandler
    {

        final AsyncHttpClient this$0;
        final boolean val$enableRedirects;

        public boolean isRedirectRequested(HttpResponse httpresponse, HttpContext httpcontext)
        {
            return enableRedirects;
        }

        _cls4()
        {
            this$0 = AsyncHttpClient.this;
            enableRedirects = flag;
            super();
        }
    }

}
