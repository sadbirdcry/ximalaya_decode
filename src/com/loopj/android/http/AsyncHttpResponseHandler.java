// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.loopj.android.http;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.util.ByteArrayBuffer;

// Referenced classes of package com.loopj.android.http:
//            ResponseHandlerInterface, AsyncHttpClient

public abstract class AsyncHttpResponseHandler
    implements ResponseHandlerInterface
{
    private static class ResponderHandler extends Handler
    {

        private final AsyncHttpResponseHandler mResponder;

        public void handleMessage(Message message)
        {
            mResponder.handleMessage(message);
        }

        ResponderHandler(AsyncHttpResponseHandler asynchttpresponsehandler)
        {
            mResponder = asynchttpresponsehandler;
        }
    }


    protected static final int BUFFER_SIZE = 4096;
    protected static final int CANCEL_MESSAGE = 6;
    public static final String DEFAULT_CHARSET = "UTF-8";
    protected static final int FAILURE_MESSAGE = 1;
    protected static final int FINISH_MESSAGE = 3;
    private static final String LOG_TAG = "AsyncHttpResponseHandler";
    protected static final int PROGRESS_MESSAGE = 4;
    protected static final int RETRY_MESSAGE = 5;
    protected static final int START_MESSAGE = 2;
    protected static final int SUCCESS_MESSAGE = 0;
    private final Handler handler = new ResponderHandler(this);
    private Header requestHeaders[];
    private URI requestURI;
    private String responseCharset;
    private Boolean useSynchronousMode;

    public AsyncHttpResponseHandler()
    {
        responseCharset = "UTF-8";
        useSynchronousMode = Boolean.valueOf(false);
        requestURI = null;
        requestHeaders = null;
        postRunnable(null);
    }

    public String getCharset()
    {
        if (responseCharset == null)
        {
            return "UTF-8";
        } else
        {
            return responseCharset;
        }
    }

    public Header[] getRequestHeaders()
    {
        return requestHeaders;
    }

    public URI getRequestURI()
    {
        return requestURI;
    }

    byte[] getResponseData(HttpEntity httpentity)
        throws IOException
    {
        InputStream inputstream;
        long l;
        long l1;
        int i = 4096;
        if (httpentity == null)
        {
            break MISSING_BLOCK_LABEL_177;
        }
        inputstream = httpentity.getContent();
        if (inputstream == null)
        {
            break MISSING_BLOCK_LABEL_177;
        }
        l1 = httpentity.getContentLength();
        if (l1 > 0x7fffffffL)
        {
            throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");
        }
        byte abyte0[];
        int j;
        if (l1 > 0L)
        {
            i = (int)l1;
        }
        httpentity = new ByteArrayBuffer(i);
        abyte0 = new byte[4096];
        i = 0;
_L2:
        j = inputstream.read(abyte0);
        if (j == -1)
        {
            break; /* Loop/switch isn't completed */
        }
        if (Thread.currentThread().isInterrupted())
        {
            break; /* Loop/switch isn't completed */
        }
        i += j;
        httpentity.append(abyte0, 0, j);
        if (l1 <= 0L)
        {
            l = 1L;
        } else
        {
            l = l1;
        }
        sendProgressMessage(i, (int)l);
        if (true) goto _L2; else goto _L1
_L1:
        try
        {
            AsyncHttpClient.silentCloseInputStream(inputstream);
            return httpentity.toByteArray();
        }
        // Misplaced declaration of an exception variable
        catch (HttpEntity httpentity)
        {
            System.gc();
        }
        break MISSING_BLOCK_LABEL_167;
        httpentity;
        AsyncHttpClient.silentCloseInputStream(inputstream);
        throw httpentity;
        throw new IOException("File too large to fit into available memory");
        return null;
    }

    public boolean getUseSynchronousMode()
    {
        return useSynchronousMode.booleanValue();
    }

    protected void handleMessage(Message message)
    {
        switch (message.what)
        {
        default:
            return;

        case 0: // '\0'
            message = ((Message) ((Object[])(Object[])message.obj));
            if (message != null && message.length >= 3)
            {
                onSuccess(((Integer)message[0]).intValue(), (Header[])(Header[])message[1], (byte[])(byte[])message[2]);
                return;
            } else
            {
                Log.e("AsyncHttpResponseHandler", "SUCCESS_MESSAGE didn't got enough params");
                return;
            }

        case 1: // '\001'
            message = ((Message) ((Object[])(Object[])message.obj));
            if (message != null && message.length >= 4)
            {
                onFailure(((Integer)message[0]).intValue(), (Header[])(Header[])message[1], (byte[])(byte[])message[2], (Throwable)message[3]);
                return;
            } else
            {
                Log.e("AsyncHttpResponseHandler", "FAILURE_MESSAGE didn't got enough params");
                return;
            }

        case 2: // '\002'
            onStart();
            return;

        case 3: // '\003'
            onFinish();
            return;

        case 4: // '\004'
            message = ((Message) ((Object[])(Object[])message.obj));
            if (message != null && message.length >= 2)
            {
                try
                {
                    onProgress(((Integer)message[0]).intValue(), ((Integer)message[1]).intValue());
                    return;
                }
                // Misplaced declaration of an exception variable
                catch (Message message)
                {
                    Log.e("AsyncHttpResponseHandler", "custom onProgress contains an error", message);
                }
                return;
            } else
            {
                Log.e("AsyncHttpResponseHandler", "PROGRESS_MESSAGE didn't got enough params");
                return;
            }

        case 5: // '\005'
            message = ((Message) ((Object[])(Object[])message.obj));
            if (message != null && message.length == 1)
            {
                onRetry(((Integer)message[0]).intValue());
                return;
            } else
            {
                Log.e("AsyncHttpResponseHandler", "RETRY_MESSAGE didn't get enough params");
                return;
            }

        case 6: // '\006'
            onCancel();
            return;
        }
    }

    protected Message obtainMessage(int i, Object obj)
    {
        return handler.obtainMessage(i, obj);
    }

    public void onCancel()
    {
        Log.d("AsyncHttpResponseHandler", "Request got cancelled");
    }

    public abstract void onFailure(int i, Header aheader[], byte abyte0[], Throwable throwable);

    public void onFinish()
    {
    }

    public void onProgress(int i, int j)
    {
        int k;
        if (j > 0)
        {
            k = (i / j) * 100;
        } else
        {
            k = -1;
        }
        Log.v("AsyncHttpResponseHandler", String.format("Progress %d from %d (%d%%)", new Object[] {
            Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k)
        }));
    }

    public void onRetry(int i)
    {
        Log.d("AsyncHttpResponseHandler", String.format("Request retry no. %d", new Object[] {
            Integer.valueOf(i)
        }));
    }

    public void onStart()
    {
    }

    public abstract void onSuccess(int i, Header aheader[], byte abyte0[]);

    protected void postRunnable(Runnable runnable)
    {
label0:
        {
            boolean flag;
            if (Looper.myLooper() == null)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            if (runnable != null)
            {
                if (!flag)
                {
                    break label0;
                }
                handler.post(runnable);
            }
            return;
        }
        runnable.run();
    }

    public final void sendCancelMessage()
    {
        sendMessage(obtainMessage(6, null));
    }

    public final void sendFailureMessage(int i, Header aheader[], byte abyte0[], Throwable throwable)
    {
        sendMessage(obtainMessage(1, ((Object) (new Object[] {
            Integer.valueOf(i), aheader, abyte0, throwable
        }))));
    }

    public final void sendFinishMessage()
    {
        sendMessage(obtainMessage(3, null));
    }

    protected void sendMessage(Message message)
    {
        if (getUseSynchronousMode())
        {
            handleMessage(message);
        } else
        if (!Thread.currentThread().isInterrupted())
        {
            handler.sendMessage(message);
            return;
        }
    }

    public final void sendProgressMessage(int i, int j)
    {
        sendMessage(obtainMessage(4, ((Object) (new Object[] {
            Integer.valueOf(i), Integer.valueOf(j)
        }))));
    }

    public void sendResponseMessage(HttpResponse httpresponse)
        throws IOException
    {
        StatusLine statusline;
        byte abyte0[];
label0:
        {
            if (!Thread.currentThread().isInterrupted())
            {
                statusline = httpresponse.getStatusLine();
                abyte0 = getResponseData(httpresponse.getEntity());
                if (!Thread.currentThread().isInterrupted())
                {
                    if (statusline.getStatusCode() < 300)
                    {
                        break label0;
                    }
                    sendFailureMessage(statusline.getStatusCode(), httpresponse.getAllHeaders(), abyte0, new HttpResponseException(statusline.getStatusCode(), statusline.getReasonPhrase()));
                }
            }
            return;
        }
        sendSuccessMessage(statusline.getStatusCode(), httpresponse.getAllHeaders(), abyte0);
    }

    public final void sendRetryMessage(int i)
    {
        sendMessage(obtainMessage(5, ((Object) (new Object[] {
            Integer.valueOf(i)
        }))));
    }

    public final void sendStartMessage()
    {
        sendMessage(obtainMessage(2, null));
    }

    public final void sendSuccessMessage(int i, Header aheader[], byte abyte0[])
    {
        sendMessage(obtainMessage(0, ((Object) (new Object[] {
            Integer.valueOf(i), aheader, abyte0
        }))));
    }

    public void setCharset(String s)
    {
        responseCharset = s;
    }

    public void setRequestHeaders(Header aheader[])
    {
        requestHeaders = aheader;
    }

    public void setRequestURI(URI uri)
    {
        requestURI = uri;
    }

    public void setUseSynchronousMode(boolean flag)
    {
        useSynchronousMode = Boolean.valueOf(flag);
    }
}
